export BRANCH=main
export DATABASES_DICTIONARY_BRANCH=dictionary

traefik:
	docker run -d --restart always --network traefik_webgateway -p 8081:8080 -p 80:80 -v /var/run/docker.sock:/var/run/docker.sock --name traefik traefik:v2.5.0 --providers.docker=true --providers.docker.network=traefik_webgateway --api.dashboard=true --api.insecure=true

up:
	docker compose --profile core --profile publishers -f docker/docker-compose.yml up -d --build

down:
	docker compose --profile core --profile publishers -f docker/docker-compose.yml down

pull:
	docker compose --profile core --profile publishers -f docker/docker-compose.yml pull

clean:
	docker system prune --volumes

ups:
	docker compose --profile core -f docker/docker-compose.yml up -d

downs:
	docker compose --profile core -f docker/docker-compose.yml down

db_down:
	docker rm -f docker-sok.mysql.intra-1 docker-clickhouse.intra-1

db_up:
	docker compose -f docker/docker-compose.yml up -d sok.mysql.intra clickhouse.intra

clean_branches:
	docker rmi -f `docker images | grep 'mt-\|cp-\|vt-\|kot-\|bt-\|vid-\|af-\|ui-\|dev-|\dictionary-'`
