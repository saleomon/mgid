#!/bin/sh
mvn test -Dlog4j.configurationFile=file:src/main/resources/log_ci/log4j2.properties ${MVN_COMMAND}

if [ -f target/surefire-reports/testng-failed.xml ];
then
  exec mvn test -Dlog4j.configurationFile=file:src/main/resources/log_ci/log4j2.properties -DVIDEO_RECORDING=enabled -Dsurefire.suiteXmlFiles=target/surefire-reports/testng-failed.xml
fi