insert into widget_statistics_shows_rtb_summ (hour_timestamp,
                                         uid,
                                         composite,
                                         site,
                                         campaign,
                                         country,
                                         device,
                                         os,
                                         browser,
                                         traffic_type,
                                         traffic_source,
                                         wages_rtb,
                                         wages_usd,
                                         wages_publisher_currency,
                                         revenue_rtb,
                                         spent_campaign,
                                         cost,
                                         implversion)
VALUES  (toUnixTimestamp(toDateTime(today())),     439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11),
        (toUnixTimestamp(toDateTime(today())),     439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11),
        (toUnixTimestamp(toDateTime(today())),     439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11),
        (toUnixTimestamp(toDateTime(today())),     439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11),
        (toUnixTimestamp(toDateTime(yesterday())), 439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11),
        (toUnixTimestamp(toDateTime(yesterday())), 439, 535, 136, 1, 40, 'tablet', 38, 33, 'Direct', 't1.tv.com', 8126840, 0.00812684, 0.00812684, 1309936, 1443854, 1443854, 11)