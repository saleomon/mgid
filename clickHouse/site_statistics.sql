INSERT INTO site_statistics (hour_timestamp,
                                   site,
                                   os,
                                   device,
                                   country,
                                   traffic_source,
                                   traffic_type,
                                   page_views)
values (toUnixTimestamp(toDateTime(today())),            1, 38, 'tablet',  40, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday())),        1,  5, 'desktop', 39, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-1)),      1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-2)),      1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-3)),      1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-4)),      1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-5)),      1,  5, 'desktop', 39, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-6)),      1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-7)),      1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-8)),      1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-9)),      1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-10)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-11)),     1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-12)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-13)),     1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-14)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-15)),     1,  5, 'desktop', 39, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-16)),     1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-17)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-18)),     1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-19)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-20)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-21)),     1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-22)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-23)),     1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-24)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-25)),     1,  5, 'desktop', 39, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-26)),     1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-27)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-28)),     1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-29)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-30)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday()-31)),     1, 10, 'mobile',  38, 'testAutoSource.tv.com',  'Direct',  2),
       (toUnixTimestamp(toDateTime(yesterday()-32)),     1, 49, 'desktop', 37, 'testAutoSource1.tv.com', 'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-33)),     1, 24, 'mobile',  36, 'testAutoSource.tv.com',  'Organic', 3),
       (toUnixTimestamp(toDateTime(yesterday()-34)),     1,  3, 'tablet',  35, 'testAutoSource.tv.com',  'Direct',  4),
       (toUnixTimestamp(toDateTime(yesterday())),     2027, 72, 'tablet',   1, 'mgid.com',               'Direct',  3),
       (toUnixTimestamp(toDateTime(yesterday())),     2013, 72, 'tablet',   1, 'mgid.com',               'Direct',  3);
