FROM maven:3-openjdk-14-slim

ENV DEBIAN_FRONTEND noninteractive
ENV MAVEN_OPTS "-Djava.awt.headless=true"
ENV SELENOID_HOST selenoid

WORKDIR /usr/src/tests

RUN apt-get update -qq && \
    apt-get install -yqq --no-install-recommends nodejs npm && \
    npm config set unsafe-perm true && \
    npm install -g allure-commandline --save-dev && \
    apt-get -yqq purge npm && \
    apt-get autoremove -yqq && \
    apt-get -yqq clean && \
    rm -rf /var/lib/apt/lists/* /root/.npm

COPY pom.xml ./

RUN mvn --batch-mode dependency:go-offline && \
    mvn --batch-mode test-compile

COPY . .

ENTRYPOINT ["/usr/src/tests/docker/entrypoint.sh"]
