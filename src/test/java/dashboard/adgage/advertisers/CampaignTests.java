package dashboard.adgage.advertisers;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

public class CampaignTests extends TestBase {
    public CampaignTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADGAGE;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    /**
     * Create/check/edit/check/delete Product company
     * <p>NIO</p>
     */
    @Test
    public void createContentCampaign() {
        log.info("Test is started");
        authDashAndGo("advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setCampaignType("content")
                .setTargetingSwitcher(false)
                .setSubnetType(Subnets.SubnetType.SCENARIO_ADGAGE)
                .createCampaignSimple();

        log.info("Check campaign after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "Check campaign after creating");

        log.info("Check shows only for this language value");
        softAssert.assertNotEquals(operationMySql.getgPartners1().selectShowsOnlyForThisLanguageValue(pagesInit.getCampaigns().getCampaignId()), 1,"FAIL - show only for this language");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(true)
                .setTargetingSwitcher(false)
                .setDynamicRetargeting(false)
                .setIsEnabledSensors(true)
                .setTargetingOption("os")
                .setLimitType("clicks_limits")
                .setIsUseSchedule(true)
                .setIsEnableUtms(true)
                .setCampaignUtm("pmac")
                .setCustomUtm("geo={geo_region}")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterEditing(), "Check campaign after editing");

        log.info("Delete campaign");
        authDashAndGo("advertisers");
        pagesInit.getCampaigns().deleteCampaign(pagesInit.getCampaigns().getCampaignId());
        authDashAndGo("advertisers");
        //adgage delete?
//        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedRestoreCampaign(pagesInit.getCampaigns().getCampaignId()), "Delete campaign");
        softAssert.assertAll();
        log.info("Test is finished");
    }



}
