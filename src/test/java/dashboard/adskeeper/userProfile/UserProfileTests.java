package dashboard.adskeeper.userProfile;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.userProfile.locators.UserProfileLocators.ADVERTISERS_TAB;
import static pages.dash.userProfile.locators.UserProfileLocators.PUBLISHERS_TAB;


public class UserProfileTests extends TestBase {
    public UserProfileTests(){
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    private String clientLogin = "sok.autotest.payouts.ukraine_ak@mgid.com";
    public static final String SUNDAY_US = "SUNDAY";
    public static final String MONDAY_US = "MONDAY";

    public void goToUserProfile(String clientLogin) {
        authDashAndGo(clientLogin, "profile/users");
        helpersInit.getBaseHelper().waitForReadyPage();
    }

    public void goToLink(SelenideElement element) {
        element.click();
        helpersInit.getBaseHelper().waitForReadyPage();
    }


    @Feature("Clients profile settings")
    @Story("Check first day of the week in user settings. Adskeeper.")
    @Description("Check default first day in user profile interface. \n" +
            "     <ul>\n" +
            "      <li>Checking what is for Adskeeper subnet client by default, the starting day of the calendar is set in the profile - Sunday</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2757\">Ticket KOT-2757</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Check default day in user profile. Adskeeper.")
    public void checkDefaultFirstDayOfTheWeekInProfile() {
        log.info("Test is started");
        goToUserProfile(clientLogin);
        Assert.assertEquals(pagesInit.getUserProfile().getCurrentFirstDayOfTheWeek(), SUNDAY_US);
        log.info("Test is finished");
    }

    @Feature("Clients profile settings")
    @Story("Change first day of the week in user settings. Adskeeper.")
    @Description("Change and check first day in user profile and checking it in calendar. \n" +
            "     <ul>\n" +
            "      <li>Change default day from Sunday to Monday in user profile.</li>\n" +
            "      <li>Checking that in calendar's in Advertise and Publishers first day in calendar is equal with user profile.</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2757\">Ticket KOT-2757</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Check changed first day in calendar. Adskeeper.")
    public void checkChangingDefaultDayOfCalendar(){
        try {
            log.info("Test is started");
            goToUserProfile(clientLogin);
            pagesInit.getUserProfile().selectAnotherStartDayOfTheWeek("mon");
            pagesInit.getUserProfile().saveSettings();
            softAssert.assertEquals(pagesInit.getUserProfile().getCurrentFirstDayOfTheWeek(), MONDAY_US, "Day from profile is incorrect!");
            goToLink(ADVERTISERS_TAB);
            softAssert.assertTrue(MONDAY_US.toLowerCase().contains(pagesInit.getUserProfile().checkFirstCalendarDayInPublishersAndAdvertInterfaces().toLowerCase()), "Day from user profile and from calendar are difference!");
            goToLink(PUBLISHERS_TAB);
            softAssert.assertTrue(MONDAY_US.toLowerCase().contains(pagesInit.getUserProfile().checkFirstCalendarDayInPublishersAndAdvertInterfaces().toLowerCase()), "Day from user profile and from calendar are difference!");
            softAssert.assertAll();
            log.info("Test is finished");
        }
        finally {
            operationMySql.getClients().updateClientsDashboardFirstDayOfTheWeek("sun", "3007");
        }
    }

    @Feature("Clients profile settings")
    @Story("Automatic emails of cloaking block | NOTIFICATIONS dashboard")
    @Description("Check notification checkbox due blocking by cloaking\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52024\">Ticket TA-52024</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check notification due blocking by cloaking")
    public void checkNotificationDueBlockingByCloaking(){
        log.info("Test is started");
        goToUserProfile(clientLogin);
        pagesInit.getUserProfile().enableDisableNotificationCloakingCheckbox(true);
        pagesInit.getUserProfile().saveSettings();

        goToUserProfile(clientLogin);
        softAssert.assertTrue(pagesInit.getUserProfile().checkStateNotificationCloakingCheckbox(true));

        pagesInit.getUserProfile().enableDisableNotificationCloakingCheckbox(false);
        pagesInit.getUserProfile().saveSettings();

        goToUserProfile(clientLogin);
        softAssert.assertTrue(pagesInit.getUserProfile().checkStateNotificationCloakingCheckbox(false));

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
