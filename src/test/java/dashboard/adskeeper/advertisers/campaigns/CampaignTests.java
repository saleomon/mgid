package dashboard.adskeeper.advertisers.campaigns;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

public class CampaignTests extends TestBase {
    public CampaignTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    @Epic("Create campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Create and edit campaign form")
    @Description("Check that 'Rotate in MGID network' checkbox is enabled by default" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52205\">Ticket TA-52205</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(dataProvider = "campaignTypesForCheckRotateInMgidForAdskeeperCampaigns",
            description = "Check that 'Rotate in MGID network' checkbox is enabled for Adskeeper campaign")
    public void checkRotateInMgidForAdskeeperCampaigns(String campaignTypeValue) {
        log.info("Test is started");
        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create " + campaignTypeValue + helpersInit.getBaseHelper().randomNumbersString(4))
                .setCampaignType(campaignTypeValue)
                .setBlockBeforeShow(false)
                .setUseTargeting(false)
                .selectLocationTarget("include", "Italy")
                .setSubnetType(subnetId)
                .setIsUseSchedule(false)
                .setIsEnabledSensors(false)
                .createCampaignWithId();
        log.info("Check campaign after creation");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypesForCheckRotateInMgidForAdskeeperCampaigns() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"push"}
        };
    }

    @Epic("Create campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Create and edit campaign form")
    @Description("Check that 'Rotate in MGID network' checkbox is enabled by default" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52205\">Ticket TA-52205</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that 'Rotate in MGID network' checkbox is enabled for Adskeeper search_feed campaign")
    public void checkRotateInMgidForAdskeeperSearchFeedCampaign() {
        log.info("Test is started");
        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp search_feed " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setCampaignType("search_feed")
                .setBlockBeforeShow(false)
                .setUseTargeting(false)
                .selectLocationTarget("include", "Italy")
                .setSubnetType(subnetId)
                .setIsUseSchedule(false)
                .setIsEnabledSensors(false)
                .setBlockBeforeShow(false)
                .setCampaignKeyword("rotateInMgid")
                .createCampaignSimple();
        log.info("Check campaign after creation");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network!");
        log.info("Test is finished");
    }

    @Epic("Copy campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Copy campaign in dashboard and edit campaign form in cab")
    @Description("Check that 'Rotate in MGID network' checkbox is enabled in cloned campaign" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52205\">Ticket TA-52205</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that 'Rotate in MGID network' checkbox is enabled in cloned campaign")
    public void checkCopyCampaignWithRotateInMgid() {
        log.info("Test is started");
        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers");
        pagesInit.getCampaigns().copyCampaign(2047);
        String campaignName = pagesInit.getCampaigns().getCampaignNameFromInput();
        pagesInit.getCampaigns().saveCampaign();
        authDashAndGo("advertisers");
        String campaignId = pagesInit.getCampaigns().getCampaignIdByName(campaignName);
        log.info("Check campaign after cloning");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network after cloning!");
        log.info("Test is finished");
    }

    @Epic("Create campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Create and edit campaign form")
    @Description("Check that 'Rotate in MGID network' checkbox is enabled for duplicated Push-campaign" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52410\">Ticket TA-52410</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that 'Rotate in MGID network' checkbox is enabled for Adskeeper duplicated Push-campaign")
    public void checkRotateInMgidForAdskeeperCampaignsWithPushDuplicate() {
        log.info("Test is started");
        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers/add");
        log.info("Create campaign");
        String campaignTypeValue = "product";
        pagesInit.getCampaigns().setCampaignName("Test camp " + campaignTypeValue + helpersInit.getBaseHelper().randomNumbersString(4))
                .setCampaignType(campaignTypeValue)
                .setBlockBeforeShow(false)
                .setUseTargeting(false)
                .selectLocationTarget("include", "Italy")
                .setSubnetType(subnetId)
                .setIsUseSchedule(false)
                .setIsEnabledSensors(false)
                .setUsePushDuplicate(true)
                .createCampaignWithId();
        log.info("Check campaign after creation");
        pagesInit.getCampaigns().getCampaignIdFromListByName(pagesInit.getCampaigns().getCampaignName() + " push");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network!");
        log.info("Test is finished");
    }
}