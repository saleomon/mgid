package dashboard.adskeeper.advertisers.campaigns;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

public class BlockUnblockCampaignTests extends TestBase {

    public BlockUnblockCampaignTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }
    @Feature("Compliant")
    @Story("Compliant Adskeeper | Type B")
    @Description("Check unblocking campaigns without teasers with type b" +
            "<ul>\n" +
            "   <li>Check unblock campaign without option compliant at teaser</li>\n" +
            "   <li>Check unblock campaign without option compliant at teaser and teaser with landing b value</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51932\">Ticket TA-51932</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24526\">Ticket TA-24526</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblocking campaigns without teasers with type b", dataProvider = "dataUnblockCampaignWithoutTeasersWithTypeB")
    public void checkUnblockCampaignWithoutTeasersWithTypeB(String campaignId, String message) {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), arrayCountries);
        authDashAndGo("advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign(campaignId);

        log.info("Check popup message");
        softAssert.assertTrue(pagesInit.getCampaigns().checkPopupMessage(message), "Unblock by icon");

        authDashAndGo("advertisers");
        log.info("Check icon visibility");
        softAssert.assertTrue(pagesInit.getCampaigns().checkVisibilityUnblockIcon(campaignId), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataUnblockCampaignWithoutTeasersWithTypeB() {
        return new Object[][]{
                {"1271", "Campaign you’re trying to unblock is not RAC/Autocontrol/IAP compliant"},
                {"1274", "The teasers of the campaign you are trying to unlock are not compliant"},
        };
    }

    @Feature("Compliant")
    @Story("Compliant Adskeeper | Type B")
    @Description("Check unblocking campaigns with teasers with type b" +
            "<ul>\n" +
            "   <li>Check unblock without compliant geo</li>\n" +
            "   <li>Check unblock with compliant option and teaser with landing b type</li>\n" +
            "   <li>Check unblock with teaser with ad type b</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51932\">Ticket TA-51932</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24526\">Ticket TA-24526</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblocking campaigns with teasers with type b", dataProvider = "dataUnblockCampaignWithTeasersWithTypeB")
    public void checkUnblockCampaignWithTeasersWithTypeB(String campaignId) {
        log.info("Test is started " + campaignId);
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), arrayCountries);
        authDashAndGo("advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign(campaignId);

        authDashAndGo("advertisers");
        log.info("Check icon visibility");
        Assert.assertFalse(pagesInit.getCampaigns().checkVisibilityUnblockIcon(campaignId), "Check icon visibility ");

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataUnblockCampaignWithTeasersWithTypeB() {
        return new Object[][]{
                {"1270"},
                {"1272"},
        };
    }
}
