package dashboard.adskeeper.advertisers.campaigns;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;

public class CampaignCertificateTests  extends TestBase {

    public CampaignCertificateTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Проверка загрузки сертификата больше 5 мб</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Загрузка сертификата больше 5 мб")
    public void checkUploadOversizeCertificate() {
        log.info("Test is started");
        int campaignId = 1206;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Italy");

        log.info("Upload certificate");
        authDashAndGo("advertisers/edit/campaign_id/1206");
        pagesInit.getCampaigns().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_size.jpeg");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("MAXIMUM ALLOWED SIZE FOR FILE 5MB"), "Check campaign after editing");

        log.info("Test is finished");
    }

    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Проверка удаления сертификата при утвержденном тизере</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка удаления сертификата при утвержденном тизере")
    public void checkDeletingCertificateWithApprovedTeaser() {
        log.info("Test is started");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1204, "Italy");

        authDashAndGo( "advertisers/edit/campaign_id/1204");
        log.info("Delete certificate");
        pagesInit.getCampaigns().deleteCertificate();

        log.info("Check displaying deleted certificate");
        authDashAndGo("advertisers/edit/campaign_id/1204");
        Assert.assertTrue(pagesInit.getCampaigns().checkDisplayingUploadedCertificate("winter.jpg"), "Check campaign after editing");

        log.info("Test is finished");
    }

    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Проверка лимита количества загрузки сертификатов</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка лимита количества загрузки сертификатов")
    public void checkAllowedAmountOfUploadedCertificate() {
        log.info("Test is started");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1207, "Italy");

        authDashAndGo( "advertisers/edit/campaign_id/1207");
        log.info("Upload certificate");
        pagesInit.getCampaigns().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_png.png");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("LIMIT OF UPLOAD FILES EXCEEDED. LIMIT 20"), "Check campaign after editing");

        log.info("Test is finished");
    }
}
