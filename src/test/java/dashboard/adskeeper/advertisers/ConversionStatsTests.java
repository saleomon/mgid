package dashboard.adskeeper.advertisers;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

public class ConversionStatsTests extends TestBase {
    public ConversionStatsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    /**
     * Вывод сабайди в интерфейс Conversions statistics
     * <p> NIO </>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51809">TA-51809</a>
     */
    @Test(dataProvider = "campaignsForFilterSubuid")
    public void checkUidSubuidFilterThroughtCampaignType(int campaignId, boolean state) {
        log.info("Test is started");

        authDashAndGo("advertisers/sensors-stat/id/" + campaignId);

        log.info("Check filter presence");
        Assert.assertEquals(pagesInit.getConversionStats().checkVisibilityOfFilterBySubid(), state, "FAIL - table checking");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignsForFilterSubuid() {
        return new Object[][]{
                {1155, true},
                {1156, false},
                {1157, true},
                {1158, true},
        };
    }
}