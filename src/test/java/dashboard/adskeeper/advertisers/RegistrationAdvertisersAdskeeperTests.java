package dashboard.adskeeper.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.*;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.adskeeperLink;

public class RegistrationAdvertisersAdskeeperTests extends TestBase {

    public RegistrationAdvertisersAdskeeperTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    /**
     * Create client with role Advertiser
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerAdvertiserAdskeeper() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersBySubject(NEW_ADVERTISER_LOGIN_ADSKEEPER);
        log.info("Let's Register new advertiser!");
        open(adskeeperLink + "/user/signup");
        pagesInit.getSignUp().registerNewClient(NEW_ADVERTISER_LOGIN_ADSKEEPER);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextAdskeeper),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(adskeeperLink + helpersInit.getBaseHelper().extractUrls(operationMySql.getMailPull().getBodyFromMailByClientEmail(
                NEW_ADVERTISER_LOGIN_ADSKEEPER)).get(0).substring(adskeeperLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_ADVERTISER_LOGIN_ADSKEEPER, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopupForAdskeeper(true);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkLoginOnPage(NEW_ADVERTISER_LOGIN_ADSKEEPER),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Restore password")
    @Feature("Password restoring in Adskeeper")
    @Story("Main dashboard page")
    @Description("Check password restoring in Adskeeper dashboard and login with new password")
    @Owner("AIA")
    @Test(description = "Check password restoring in Adskeeper dashboard and login with new password")
    public void checkRestorePasswordAdskeeper() {
        log.info("Test is started");
        String userLogin = "restore-password.client@adskeeper.com";
        operationMySql.getMailPull().getCountLettersByClientEmail(userLogin);
        open(adskeeperLink + "/user/restore-password");
        pagesInit.getSignUp().restorePassword(userLogin);
        open(adskeeperLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(userLogin)).get(0).substring("https://dashboard.adskeeper.co.uk".length()));
        pagesInit.getSignUp().createPassword(NEW_RESTORED_PASSWORD);
        pagesInit.getSignUp().signInDash(userLogin, NEW_RESTORED_PASSWORD);
        Assert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(userLogin),
                "FAIL -> Login after password restoring is failed!");
        log.info("Test is finished");
    }
}
