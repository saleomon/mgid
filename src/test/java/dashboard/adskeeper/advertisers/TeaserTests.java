package dashboard.adskeeper.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static core.service.constantTemplates.ConstantsInit.*;
import static core.service.constantTemplates.ConstantsInit.NIO;

public class TeaserTests extends TestBase {
    public TeaserTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }
    private static final String limitError = "The %s campaign has reached its limit on the number of teasers. The available limit within the campaign is %s teasers. Clean up teasers that don't get impressions.";

    public void goToCreateTeaser(int campaignId) {
        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    @Epic(TEASERS)
    @Feature(AUTOGENERATION_TITLES)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "     <li>g_partners_1.campaign_types = product, g_partners_1.languages_id = 1 </li>" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52162\">Ticket TA-52162</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check displayed autogenerate title button Adskeeper")
    public void checkDisplayingAutogenerateTitleButton() {
        log.info("Test is started");

        authDashAndGo("advertisers/add-teaser-goods/campaign_id/1408");
        Assert.assertFalse(pagesInit.getTeaserClass().isDisplayedGetAutogenerateTitleButton(), "FAIL - button visible");
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 teaser which one dropped and second is blocked</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo NIO https://jira.mgid.com/browse/CP-3033 @Test(description = "Check create teaser over limit")
    public void checkCreateTeaserOverLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1429;
        int limit = 2;

        goToCreateTeaser(campaignId);
        softAssert.assertEquals(pagesInit.getTeaserClass()
                .setPriceOfClick("15")
                .useDiscount(true)
                .createTeaser(), 0, "Fail - teaser created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(String.format(limitError, campaignId, limit)),
                "FAIL -> Check teaser settings in list!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
