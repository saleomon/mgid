package dashboard.adskeeper.publishers;

import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextAdskeeper;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.adskeeperLink;

public class RegistrationPublishersAdskeeperTests extends TestBase {

    public RegistrationPublishersAdskeeperTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    /**
     * Create client with role Publisher
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerPublisherAdskeeper() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersBySubject(NEW_PUBLISHER_LOGIN_ADSKEEPER);
        log.info("Let's Register new publisher!");
        open(adskeeperLink + "/user/signup");
        pagesInit.getSignUp().registerNewClient(NEW_PUBLISHER_LOGIN_ADSKEEPER);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextAdskeeper),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(adskeeperLink + helpersInit.getBaseHelper().extractUrls(operationMySql.getMailPull().getBodyFromMailByClientEmail(
                NEW_PUBLISHER_LOGIN_ADSKEEPER)).get(0).substring(adskeeperLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_PUBLISHER_LOGIN_ADSKEEPER, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopupForAdskeeper(false);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkLoginOnPage(NEW_PUBLISHER_LOGIN_ADSKEEPER),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertEquals(operationMySql.getClients().getAdDarknessCheckboxInNewClient(NEW_PUBLISHER_LOGIN_ADSKEEPER), "0" ,
                "FAIL -> flag can_change_ad_darkness = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
