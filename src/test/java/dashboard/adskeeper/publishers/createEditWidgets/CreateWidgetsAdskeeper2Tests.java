package dashboard.adskeeper.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static testData.project.ClientsEntities.WEBSITE_ADSKEEPER_WAGES_ID;

/*
 * 1. SIDEBAR
 *      - SIDEBAR_LIGHT
 *      - SIDEBAR_DARK
 *
 * 2. MOBILE
 *
 * 3. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 *      - IN_SITE_NOTIFICATION_MEDIA
 *      - IN_SITE_NOTIFICATION_CHAT
 *
 * 4. EXIT_POP_UP
 *
 * 5. MOBILE_EXIT
 */
public class CreateWidgetsAdskeeper2Tests extends TestBase {

    public CreateWidgetsAdskeeper2Tests() {
        subnetId = SubnetType.SCENARIO_ADSKEEPER;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_ADSKEEPER_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_LIGHT")
    @Test
    public void createSidebarLightWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_DARK")
    @Test
    public void createSidebarDarkWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.MOBILE, SubTypes.NONE")
    @Test
    public void createMobileWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN/IN_SITE_NOTIFICATION_MEDIA/IN_SITE_NOTIFICATION_CHAT")
    @Test(dataProvider = "inSiteNotificationSubType")
    public void createInSiteNotificationWidget(WidgetTypes.SubTypes subType) {
        log.info("Test is started: " + subType);
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_SITE_NOTIFICATION, subType);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished: " + subType);
    }

    @DataProvider
    public Object[][] inSiteNotificationSubType(){
        return new Object[][]{
                {WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN},
                {WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MEDIA},
                {WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_CHAT}
        };
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.EXIT_POP_UP, SubTypes.NONE")
    @Test
    public void createPopUpWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.MOBILE_EXIT, SubTypes.NONE")
    @Test
    public void createMobileExitWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
