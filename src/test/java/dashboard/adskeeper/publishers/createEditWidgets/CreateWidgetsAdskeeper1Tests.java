package dashboard.adskeeper.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static testData.project.ClientsEntities.WEBSITE_ADSKEEPER_WAGES_ID;

/*
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_CARD_MEDIA
 *      - UNDER_ARTICLE_LIGHT
 *      - UNDER_ARTICLE_DARK
 *
 * 2. HEADER
 *      - HEADER_LIGHT
 *      - HEADER_DARK
 *
 * 3. IN_ARTICLE
 *      - IN_ARTICLE_CAROUSEL
 *      - IN_ARTICLE_CARD_MEDIA
 *
 * 4. FEED
 *
 * - checkButtonAddAnotherWidget
 */
public class CreateWidgetsAdskeeper1Tests extends TestBase {

    public CreateWidgetsAdskeeper1Tests() {
        subnetId = SubnetType.SCENARIO_ADSKEEPER;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_ADSKEEPER_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARD_MEDIA")
    @Test
    public void createUnderArticleCardMedia() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARD_MEDIA);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT")
    @Test
    public void createUnderArticleLightWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after edit");

        authDashAndGo("publisher/widgets/site/" + WEBSITE_ADSKEEPER_WAGES_ID);
        softAssert.assertTrue(pagesInit.getWidgetClass().deleteWidget(), "fail delete widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_DARK")
    @Test
    public void createUnderArticleDarkWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.HEADER, SubTypes.HEADER_LIGHT")
    @Test
    public void createHeaderLightWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_LIGHT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.HEADER, SubTypes.HEADER_DARK")
    @Test
    public void createHeaderDarkWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_DARK);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void createInArticleCarouselBlackWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CARD_MEDIA")
    @Test
    public void createInArticleCardMedia() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CARD_MEDIA);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Types.FEED, SubTypes.NONE")
    @Test
    public void createFeedBlackWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget ADSKEEPER in Dashboard")
    @Description("Проверка, что при нажатии на кнопку создания нового виджета - открывается интерфейс конструктора без ошибок <a href='https://youtrack.dt00.net/issue/VT-21895'>VT-21895</a>")
    @Test
    public void checkButtonAddAnotherWidget() {
        log.info("Test is started");
        goToCreateWidget();
        Assert.assertTrue(pagesInit.getWidgetClass().checkButtonAddAnotherWidget(), "FAIL -> button doesn't work correctly");
        log.info("Test is finished");
    }
}
