package dashboard.adskeeper.publishers;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.publisher.variables.PayoutsVariables.subjectLetterAdskeeper;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_UKRAINE_LOGIN;
import static testData.project.RoleIdsDash.ADMIN_ADSKEEPER_ROLE;

public class AdskeeperPayoutsTests extends TestBase {

    public AdskeeperPayoutsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        clientLogin = CLIENTS_PAYOUTS_LOGIN;
    }

    private final String clientId = "2002";

    /**
     * Add wallet - ACH bank-to-bank transfers and check wallet params
     * <p>Author RKO/Moved by AIA</p>
     */
    @Test
    public void addAndCheckAchBankTransferPurse() {
        log.info("Test is started");
        log.info("* Preconditions: delete clients current type purses *");
        operationMySql.getClientsPurses().deletePurses(clientId, "175");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method ACH bank-to-bank transfer *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().addAchTransferPurse("175", "ACH", "adskeeper");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayOutType("175", "ACH", "adskeeper"),
                "FAIL -> after creating!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().editAchTransferPurse("175", "ACH", "adskeeper");
        log.info("* Get confirmation link from mail and go it *");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayOutType("175", "ACH", "adskeeper"),
                "FAIL -> after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления и редактирования кошелька PayPal в дешборде
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22013">Ticket TA-22013</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addAndEditPayPalPurseTest() {
        log.info("Test is started");
        log.info("* Preconditions: delete clients current type purses *");
        operationMySql.getClientsPurses().deletePurses(clientId, "169");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method PayPal *");
        pagesInit.getPayoutsClass().addPurse("PayPal", "");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("PayPal"),
                "FAIL -> after creating!");
        softAssert.assertEquals(pagesInit.getPayoutsClass().getPurseId("PayPal"), "169",
                "FAIL -> check Id after creating!");
        helpersInit.getBaseHelper().refreshCurrentPage();
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPurse("PayPal", "");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("PayPal"),
                "FAIL -> Purse type after editing!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayPalPurseValue(),
                "FAIL -> Purse value after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления и редактирования кошелька Webmoney WMZ в дешборде
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22013">Ticket TA-22013</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addAndEditWebmoneyPurseTest() {
        log.info("Test is started");
        log.info("* Preconditions: delete clients current type purses *");
        operationMySql.getClientsPurses().deletePurses(clientId, "11");
        authDashAndGo("publisher/payouts");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        log.info("* Add payment method Webmoney USD *");
        pagesInit.getPayoutsClass().addPurse("Webmoney", "Z431071031918");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Webmoney"),
                "FAIL -> after creating!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPurse("Webmoney", "Z573184336595");
        log.info("* Get confirmation link from mail and go it *");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Webmoney"),
                "FAIL -> after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check "Hold/Resume payments" button
     * <p>Author AIA</p>
     */
    @Test(dependsOnMethods = "addAndEditPayPalPurseTest")
    public void checkHoldResumeButton() {
        log.info("Test is started");
        log.info("* Enable privilege 'publisher/display_hold_resume_button' *");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_ADSKEEPER_ROLE,
                "publisher/display_hold_resume_button");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_ADSKEEPER_ROLE,
                "publisher/payout-hold-or-resume");
        log.info("* Insert payments *");
        operationMySql.getPayments().pushDumpToTable("mysql/partners/payments/paymentsAdskeeper.sql");

        authDashAndGo("publisher/payouts");
        log.info("* Check pause payments *");
        pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldPaymentsConfirmationPopupText(),
                "FAIL -> pause payments!");
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                "CURRENTLY YOUR PAYMENTS ARE ON HOLD; CLICK \"RESUME PAYMENTS\" TO START GETTING PAID."),
                "FAIL -> pause payments message!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkResumePaymentsButton(),
                "FAIL -> resume payments button!");
        log.info("* Check payments status *");
        authDashAndGo("publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymentsStatus("Rejected"),
                "FAIL -> statuses!");
        log.info("* Check resume payments *");
        pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkResumePaymentsConfirmationPopupText(),
                "FAIL -> pause payments!");
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldPaymentsButton(),
                "FAIL -> resume payments!");
        log.info("* Disable privilege 'publisher/display_hold_resume_button' *");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_ADSKEEPER_ROLE,
                "publisher/display_hold_resume_button");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_ADSKEEPER_ROLE,
                "publisher/payout-hold-or-resume");
        log.info("* Check that 'Hold/Resume payments' button is not displayed on page 'Payuots' *");
        authDashAndGo("publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldResumePaymentsButtonInClient(false),
                "FAIL -> check privileges");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления и редактирования кошелька Paxum в дешборде
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24114">Ticket TA-24114</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addAndEditPaxumPurseTest() {
        log.info("Test is started");
        log.info("* Preconditions: delete clients current type purses *");
        operationMySql.getClientsPurses().deletePurses(clientId, "178");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method Paxum *");
        pagesInit.getPayoutsClass().addPurse("Paxum", "");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paxum"),
                "FAIL -> after adding Paxum purse!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPurse("Paxum", "");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paxum"),
                "FAIL -> after editing Paxum purse!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add wallet Paymaster24 IDAA, USD (id=248) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-28487">Ticket TA-28487</a>
     * <p>Author AIA</p>
     */
    //todo https://jira.mgid.com/browse/KOT-3324 @Test
    public void addAndCheckPaymaster24PurseAdskeeper() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "248");
        authDashAndGo("publisher/payouts");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        log.info("* Add payment method Paymaster24 MGID Asia, USD*");
        pagesInit.getPayoutsClass().addPaymaster24Purse("Paymaster24", "Z431071031918");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after creating!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after creating!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPaymaster24("Paymaster24", "Z573184336595");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after editing!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add wallet Payoneer IDAA, USD (195) and check wallet params
     * Cases
     * <ul>
     *     <li>1. Add purse</li>
     *     <li>2. Edit purse</li>
     *     <li>3. Check purse with disabled rights</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51751">Ticket TA-51751</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addAndCheckPayoneerPurseAdskeeper() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "195");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        log.info("* Enable rights *");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, 1, "publisher/can_select_payoneer_my_purse");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method Payoneer IDAA, USD *");
        pagesInit.getPayoutsClass().addPurse("Payoneer", "");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Payoneer"),
                "FAIL -> after creating!");
        softAssert.assertEquals(pagesInit.getPayoutsClass().getPurseId("Payoneer"), "195",
                "FAIL -> check Id after creating!");
        helpersInit.getBaseHelper().refreshCurrentPage();
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPurse("Payoneer", "");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Payoneer"),
                "FAIL -> Purse type after editing!");
        pagesInit.getPayoutsClass().openEditPursePopup();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoneerPurseValue(),
                "FAIL -> Purse value after editing!");
        log.info("Disable rights");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, 1, "publisher/can_select_payoneer_my_purse");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Payoneer"),
                "FAIL -> Check purse with disable right!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check if available wallet Payoneer IDAA, USD (195) with disabled rights
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51751">Ticket TA-51751</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkPayoneerPurseAdskeeperWithoutRights() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "195");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, 1, "publisher/can_select_payoneer_my_purse");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method Payoneer IDAA, USD *");
        pagesInit.getPayoutsClass().openAddPursePopup();
        Assert.assertFalse(pagesInit.getPayoutsClass().checkPurseTypeIsAvailable("Payoneer"),
                "FAIL -> Payoneer purse type is available with disabled rights!");
        log.info("Test is finished");
    }

    /**
     * Check if non USD Adskeeper client can add wallet Payoneer IDAA, USD (195)
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51751">Ticket TA-51751</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkPayoneerPurseAdskeeperForNonUsdClient() {
        log.info("Test is started");
        log.info("Check 'EUR' client can't add 'Payoneer' payment method");
        authDashAndGo("sok.autotest.payment.packages.eur@adskeeper.com", "publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkAddNewPaymentMethodButtonIsDisabled(),
                "FAIL -> Adskeeper EUR client can add payment method!");
        log.info("Check 'RUB' client can't add 'Payoneer' payment method");
        authDashAndGo("testClient_2033","publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkAddNewPaymentMethodButtonIsDisabled(),
                "FAIL -> Adskeeper RUB client can add payment method!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add wallet Capitalist IDAA, USD (201) and check wallet params
     * Cases
     * <ul>
     *     <li>1. Add purse</li>
     *     <li>2. Edit purse</li>
     *     <li>3. Check purse with disabled rights</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51756">Ticket TA-51756</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addAndCheckCapitalistPurseAdskeeper() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "201");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        log.info("* Enable rights *");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, 1, "publisher/can_select_capitalist_purse");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method Capitalist IDAA, USD *");
        pagesInit.getPayoutsClass().addPurse("Capitalist", "");
        log.info("* Get confirmation link from mail and go it *");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Capitalist"),
                "FAIL -> after creating!");
        softAssert.assertEquals(pagesInit.getPayoutsClass().getPurseId("Capitalist"), "201",
                "FAIL -> check Id after creating!");
        helpersInit.getBaseHelper().refreshCurrentPage();
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterAdskeeper);
        pagesInit.getPayoutsClass().editPurse("Capitalist", "");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterAdskeeper)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Capitalist"),
                "FAIL -> Purse type after editing!");
        pagesInit.getPayoutsClass().openEditPursePopup();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkCapitalistPurseValue(),
                "FAIL -> Purse value after editing!");
        log.info("Disable rights");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, 1, "publisher/can_select_capitalist_purse");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Capitalist"),
                "FAIL -> Check purse with disable right!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check if available wallet Capitalist IDAA, USD (201) without rights
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51756">Ticket TA-51756</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkCapitalistPurseAdskeeperWithoutRights() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "201");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, 1, "publisher/can_select_capitalist_purse");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method Capitalist IDAA, USD *");
        pagesInit.getPayoutsClass().openAddPursePopup();
        Assert.assertFalse(pagesInit.getPayoutsClass().checkPurseTypeIsAvailable("Capitalist"),
                "FAIL -> Capitalist purse type is available with disabled rights!");
        log.info("Test is finished");
    }

    /**
     * Check if non USD Adskeeper client can add wallet Capitalist IDAA, USD (201)
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51756">Ticket TA-51756</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkCapitalistPurseAdskeeperForNonUsdClient() {
        log.info("Test is started");
        log.info("Check 'EUR' client can't add 'Capitalist' payment method");
        authDashAndGo("sok.autotest.payment.packages.eur@adskeeper.com", "publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkAddNewPaymentMethodButtonIsDisabled(),
                "FAIL -> Adskeeper EUR client can add payment method Capitalist!");
        log.info("Check 'RUB' client can't add 'Capitalist' payment method");
        authDashAndGo("testClient_2033","publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkAddNewPaymentMethodButtonIsDisabled(),
                "FAIL -> Adskeeper RUB client can add payment method Capitalist!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description("Валидация добавления кошелька WMZ для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить WMZ кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Добавление кошелька Webmoney для клиента из Украины для подсети Adskeeper")
    public void forbiddenToAddWebMoneyWalletFromUkraine() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN,"publisher/payouts");
        log.info("* Add payment method Webmoney USD*");
        pagesInit.getPayoutsClass().addPurse("Webmoney", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "PLEASE USE A DIFFERENT PAYMENT METHOD."),
                "FAIL -> WMZ wallet is added correctly!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description ("Валидация добавления кошелька Paymaster24 для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить Paymaster24 кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    //todo https://jira.mgid.com/browse/KOT-3324 @Test (description = "Добавление кошелька Paymaster24 для клиента из Украины для подсети Adskeeper")
    public void forbiddenToAddWalletPaymaster24FromUkraine(){
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN,"publisher/payouts");
        log.info("* Add payment method Paymaster24 USD*");
        pagesInit.getPayoutsClass().addPaymaster24PurseUkraine("Paymaster24", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "PLEASE USE A DIFFERENT PAYMENT METHOD."),
                "FAIL -> Paymaster24 wallet is added correctly!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
