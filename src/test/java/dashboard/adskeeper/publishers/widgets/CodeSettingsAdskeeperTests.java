package dashboard.adskeeper.publishers.widgets;

import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.open;
import static testData.project.ClientsEntities.WEBSITE_ADSKEEPER_WAGES_DOMAIN;
import static testData.project.ClientsEntities.WEBSITE_ADSKEEPER_WAGES_ID;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;

public class CodeSettingsAdskeeperTests extends TestBase {

    private int widgetId;
    private String hearerWidgetLink;
    private String widgetCodeForClientLink;
    private final int webSiteId = WEBSITE_ADSKEEPER_WAGES_ID;
    private final String siteDomain = WEBSITE_ADSKEEPER_WAGES_DOMAIN;
    private final String subnetName = SUBNET_ADSKEEPER_NAME;

    private CodeSettingsAdskeeperTests() {
        subnetId = SubnetType.SCENARIO_ADSKEEPER;
    }

    private void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + webSiteId);
    }

    /**
     * <p>Создание нового виджета и проверка AMP кода при создании</p>
     * <p>Создание нового виджета и проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(priority = -1)
    public void createWidgetAndCheckCodeAfterCreated_dash(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass()
                .setIsCloseCodePopup(false)
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT)
                .saveWidgetAndGetId();
        widgetId = pagesInit.getWidgetClass().getWidgetId();

        log.info("Check AMP-code after creating widget");
        softAssert.assertFalse(
                pagesInit.getWidgetClass().checkAMPCodeAfterCreatingWidget(subnetName, siteDomain, webSiteId),
                "FAIL -> AMP code after creating in " + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода в списке виджетов dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "createWidgetAndCheckCodeAfterCreated_dash")
    public void checkCodeInWidgetsList_dash(){
        log.info("Test is started");
        authDashAndGo("publisher/widgets/site/" + webSiteId);
        softAssert.assertFalse(
                pagesInit.getWidgetClass().checkAMPCodeInWidgetsList(widgetId, webSiteId, siteDomain, subnetName),
                "FAIL -> AMP code in Dashboard widgets list" + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода в списке виджетов cab</p>
     * <p>проверка что Instant Articles код отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     * @see <a href="https://jira.mgid.com/browse/VT-27845">VT-27845</a>
     */
    @Test(dependsOnMethods = "createWidgetAndCheckCodeAfterCreated_dash")
    public void checkCodeInWidgetsList_cab(){
        log.info("Test is started");
        log.info("Check AMP-code in widgets list Cab");
        authCabAndGo("wages/informers-code/id/" + widgetId);

        hearerWidgetLink = pagesInit.getCabInformersCode().getLinkForHeaderWidget();
        widgetCodeForClientLink = pagesInit.getCabInformersCode().getLinkWidgetCodeForClient();

        softAssert.assertTrue(
                pagesInit.getCabInformersCode().checkAMPCodeForWidgetsInCab(widgetId,
                        siteDomain, webSiteId, subnetName),
                "FAIL -> AMP code in Cab! " + subnetName + "!");

        String jsc = "jsc.adskeeper.co.uk";
        softAssert.assertTrue(
                pagesInit.getCabInformersCode().checkInstantArticlesWidgetsInCab(widgetId,
                        siteDomain, webSiteId, jsc),
                "FAIL -> Instant Articles code in Cab! " + subnetName + "!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Get widget link for client) in dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkWithHash_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for authorized client -> WidgetCodeForClient");
        authDashAndGo("");
        authDashAndGo(hearerWidgetLink);
        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Header widget code) in dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkForHearerWidget_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for authorized client -> LinkForHeaderWidget");
        authDashAndGo("");
        authDashAndGo(widgetCodeForClientLink);
        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Get widget link for client) in dash (unauthorized client)</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkWithHash_unauthorizedClient_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for unauthorized client");

        open(widgetCodeForClientLink);

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Header widget code) in dash(unauthorized client)</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkForHearerWidget_unauthorizedClient_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for unauthorized client");

        open(hearerWidgetLink);

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
