package dashboard.adskeeper.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Collections;

import static testData.project.ClientsEntities.WEBSITE_ADSKEEPER_WAGES_ID;

public class AfterCreatedSettingsAdskeeperTests extends TestBase {

    public AfterCreatedSettingsAdskeeperTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_ADSKEEPER_WAGES_ID);
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Adskeeper")
    @Description("In-site notification settings after created: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24512\">TA-24512</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/KOT-1692\">KOT-1692</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "check in-site notification settings after create from dashboard")
    public void checkInSiteNotificationWidgetSettings(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.NONE);

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getCabCompositeSettings()
                .setCategoryPlatform("158");

        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkCategoryPlatform(), "FAIL -> checkCategoryPlatform");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getSelectedSourceType(), "16", "FAIL -> sourceType");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(true), "FAIL -> Place reservation");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), "274", "FAIL -> checkPushProvider");
        softAssert.assertEquals(operationMySql.getGblocks().getAnimation(pagesInit.getWidgetClass().getWidgetId()), "2", "FAIL -> animation in DB");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(pagesInit.getWidgetClass().getWidgetId()), Collections.singletonList("28"), "FAIL -> getBundlesGBlocks");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Adskeeper")
    @Description("Create SIDEBAR_LIGHT widget after this edit him to In-site notification widget and check settings: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24512\">TA-24512</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "Create SIDEBAR_LIGHT widget after this edit him to In-site notification")
    public void checkInSiteNotificationWidgetSettingsEditOtherTypeOnInSiteNotification(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.NONE);

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getCabCompositeSettings()
                .setCategoryPlatform("158");

        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkCategoryPlatform(), "FAIL -> checkCategoryPlatform");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getSelectedSourceType(), "16", "FAIL -> sourceType");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), "274", "FAIL -> checkPushProvider");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(pagesInit.getWidgetClass().getWidgetId()), Collections.singletonList("28"), "FAIL -> getBundlesGBlocks");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Adskeeper")
    @Description("Create In-site notification widget and after this edit him to SIDEBAR_LIGHT widget and check settings: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24512\">TA-24512</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "Create In-site notification widget after this edit him to SIDEBAR_LIGHT")
    public void checkInSiteNotificationWidgetSettingsEditOnOtherType(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.NONE);

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        pagesInit.getWidgetClass().saveWidgetSettings();
        int g_blocks = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getCabCompositeSettings()
                .setCategoryPlatform("120");

        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkCategoryPlatform(), "FAIL -> checkCategoryPlatform");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getSelectedSourceType(), "11", "FAIL -> sourceType");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), null, "FAIL -> getPushProviderId");
        softAssert.assertFalse(operationMySql.getBundlesGBlocks().selectIsExistBundlesId(g_blocks),"FAIL -> getBundlesGBlocks");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Adskeeper")
    @Description("Create Mobile exit widget widget and check settings: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24356\">TA-24356</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "Create Mobile exit widget widget and check settings")
    public void checkMobileExitWidgetSettings(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);

        log.info("get product id and go on Product Part");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());

        log.info("check 'mobile-exit' base settings");
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getSelectedValInMobileDoubleClick(), "1", "Fail -> Mobile doubleclick != default");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
