package dashboard.adskeeper.publishers;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;

public class AdskeeperBlockedContentTests extends TestBase {

    public AdskeeperBlockedContentTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Test
    public void widgetStand_checkCloseIcon_blockTeaserForWidget(){
        log.info("Test is started");
        int widgetId = 79;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_blocked_content"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserOnStand(), "FAIL -> show block teaser");
        authDashAndGo(pagesInit.getWidgetClass().getLinkOnBlockTeaser());

        softAssert.assertTrue(pagesInit.getBlockedContent().getTeaserTitle().contains(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                 "FAIL -> block teaser doesn't wright");
        pagesInit.getBlockedContent().chooseBlockerType("widget");
        pagesInit.getBlockedContent().blockedPopupSave();
        helpersInit.getBaseHelper().closePopup();

        pagesInit.getBlockedContent().chooseWidgetInFilter(widgetId);
        softAssert.assertEquals(pagesInit.getBlockedContent().getTeaserBlockedType(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                "Blocked widgets",
                "FAIL -> don't correct blocked type");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
