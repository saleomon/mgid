package dashboard.adskeeper.publishers.widgetOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Collections;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.publisher.locators.CreateEditWidgetLocators.LIKE_POST_BLOCK;
import static testData.project.ClientsEntities.ADSKEEPER_WIDGET_IN_SITE_NOTIFICATION_ID;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;

/*
 * 1. HEADER
 *      - HEADER_LIGHT
 *      - HEADER_DARK
 *
 * 2. FEED
 *
 * 3. EXIT_POP_UP
 *
 * 4. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 *      - IN_SITE_NOTIFICATION_CHAT
 *      - IN_SITE_NOTIFICATION_MEDIA
 *
 */
public class WidgetsOnStandAdskeeperScreenshot2Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandAdskeeperScreenshot2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        clientLogin = "testEmail43@ex.ua";
        subnetName = SUBNET_ADSKEEPER_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_LIGHT")
    @Test
    public void standHeaderLightCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(355, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_LIGHT,
                "header", "adskeeper-amp-header-light",
                "standHeaderLightCheckScreenshot", "standHeaderLightCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_DARK")
    @Test
    public void standHeaderDarkCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(355, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_DARK,
                "header", "adskeeper-amp-header-dark",
                "standHeaderDarkCheckScreenshot", "standHeaderDarkCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.FEED, SubTypes.NONE")
    @Test
    public void standFeedCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(358, WidgetTypes.Types.FEED, WidgetTypes.SubTypes.FEED_MAIN,
                "feed", "adskeeper-amp-feed",
                "standFeedCheckScreenshot", "standFeedCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.EXIT_POP_UP, SubTypes.NONE")
    @Test
    public void standExitPopUpCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 356;
        log.info("ShadowDomEnabled = 0");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("exit-pop-up"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().mouseOutEvent();
        serviceInit.getServicerMock().countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standExitPopUpCheckScreenshot.png")), "FAIL -> screen simple");
        serviceInit.getServicerMock().deleteCookie("AdskeeperStorage", "local-apache.com");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("exit-pop-up"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().mouseOutEvent();
        serviceInit.getServicerMock().countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standExitPopUpCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN/IN_SITE_NOTIFICATION_CHAT/IN_SITE_NOTIFICATION_MEDIA")
    @Test(dataProvider = "inSiteNotificationSubTypes")
    public void standInSiteNotificationCheckScreenshot(WidgetTypes.SubTypes subTypes, String screen) {
        log.info("Test is started: " + screen);
        int widgetId = 357;
        log.info("ShadowDomEnabled = 0");
        operationMySql.getTickersComposite().updateShadowDomEnabled(0, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, subTypes);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        serviceInit.getServicerMock().countDownLatchAwait();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + screen + ".png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        serviceInit.getServicerMock().countDownLatchAwait();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + screen + ".png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished: " + screen);
    }

    @DataProvider
    public Object[][] inSiteNotificationSubTypes(){
        return new Object[][]{
                {WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN, "standInSiteNotificationMainCheckScreenshot"},
                {WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_CHAT, "standInSiteNotificationChatCheckScreenshot"}
        };
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MEDIA and check work reRun function" +
            "we are waiting show 2 widgets after 70s when we closed one widget")
    @Test(description = "Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MEDIA")
    public void standInSiteNotificationMediaCheckScreenshotAndCheckReRunFunction() {
        int widgetId = 357;
        String responseBody = "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser 9\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}]";
        log.info("Test is started");
        operationMySql.getTickersComposite().updateShadowDomEnabled(0, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MEDIA)
                .chooseRows(2)
                .switchRerunAdsTumbler(true)
                .saveWidgetSettings();

        log.info("ShadowDomEnabled = 0");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass()
                .clickCloseIconInSiteNotification();
        serviceInit.getServicerMock()
                .setWidgetIds(ADSKEEPER_WIDGET_IN_SITE_NOTIFICATION_ID)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(Collections.singletonList(responseBody))
                .countDownLatchAwait(70);

        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().addIgnoreElement(LIKE_POST_BLOCK).takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInSiteNotificationMediaCheckScreenshotAndCheckReRunFunction.png")), "FAIL -> ShadowDomEnabled=0");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.DEFAULT)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .clickCloseIconInSiteNotification();
        serviceInit.getServicerMock()
                .setWidgetIds(ADSKEEPER_WIDGET_IN_SITE_NOTIFICATION_ID)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(Collections.singletonList(responseBody))
                .countDownLatchAwait(70);

        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().addIgnoreElementByCoords(locatorForScreen, locatorForMediaElements).takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInSiteNotificationMediaCheckScreenshotAndCheckReRunFunction.png")), "FAIL -> ShadowDomEnabled=1");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
