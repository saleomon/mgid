package dashboard.adskeeper.publishers.widgetOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import org.testng.annotations.Test;
import pages.dash.publisher.logic.Widget;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;

/*
 * 1. MOBILE_EXIT
 *
 * 2. MOBILE
 *      - TOASTER
 *      - DRAGDOWN
 *      - Interstitial
 */
public class WidgetsOnStandAdskeeperScreenshotMobileTests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandAdskeeperScreenshotMobileTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        clientLogin = "testEmail43@ex.ua";
        subnetName = SUBNET_ADSKEEPER_NAME;
    }

    @BeforeMethod
    public void setMobileEmulation(){
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("Types.MOBILE_EXIT, SubTypes.NONE")
    @Owner("RKO")
    @Test(description = "Types.MOBILE_EXIT, SubTypes.NONE")
    public void standMobileExitCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 359;
        log.info("ShadowDomEnabled = 0");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("exit"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standMobileExitCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("exit"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standMobileExitCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("Types.MOBILE, SubTypes.NONE, MobileWidgetType.TOASTER")
    @Owner("RKO")
    @Test(description = "Types.MOBILE, SubTypes.NONE, MobileWidgetType.TOASTER")
    public void standMobileToasterCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 360;
        log.info("ShadowDomEnabled = 0");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .chooseMobileWidgetType(Widget.MobileWidgetType.TOASTER)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("toaster"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().waitShowToasterWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standMobileToasterCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("toaster"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .waitShowToasterWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standMobileToasterCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("Types.MOBILE, SubTypes.NONE, MobileWidgetType.DRAGDOWN")
    @Owner("RKO")
    @Test(description = "Types.MOBILE, SubTypes.NONE, MobileWidgetType.DRAGDOWN")
    public void standMobileDragDownCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 398;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .chooseMobileWidgetType(Widget.MobileWidgetType.DRAGDOWN)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("drag-down"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        sleep(5000);
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .waitShowDragdownWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(2000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standMobileDragDownCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("check work sequence Toaster/DragDown/Interstitial for one widget <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work sequence Toaster/DragDown/Interstitial for one widget")
    public void mobileWidgetWithCustomTypeToasterDragDownInterstitial() {
        log.info("Test is started");
        int widgetId = 509;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setShowAfterInteraction("2")
                .chooseMobileWidgetType(Widget.MobileWidgetType.INTERSTITIAL,
                        Widget.MobileWidgetType.DRAGDOWN,
                        Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("toaster-drag-down-interstitial"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);

        log.info("show toaster");
        softAssert.assertTrue(pagesInit.getWidgetClass().setShadowDom(true).isShowMobileContainer(), "FAIL -> AdWidgetContainer toaster");
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeToasterDragDownInterstitial-toaster.png")), "FAIL -> screen toaster");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();

        log.info("show dragDown");
        sleep(6000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer dragDown");
        sleep(2000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeToasterDragDownInterstitial-dragDown-0.png")), "FAIL -> screen dragDown");

        refresh();
        log.info("show interstitial");
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial first click (link)");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-empty.png")), "FAIL -> screen interstitial");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-0.png")), "FAIL -> screen interstitial");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("check work sequence Toaster/Interstitial for one widget <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work sequence Toaster/Interstitial for one widget")
    public void mobileWidgetWithCustomTypeToasterInterstitial() {
        log.info("Test is started");
        int widgetId = 510;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setShowAfterInteraction("2")
                .chooseMobileWidgetType(Widget.MobileWidgetType.INTERSTITIAL,
                        Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("toaster-interstitial"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);

        log.info("show toaster");
        softAssert.assertTrue(pagesInit.getWidgetClass().setShadowDom(true).isShowMobileContainer(), "FAIL -> AdWidgetContainer toaster");
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeToasterInterstitial-toaster.png")), "FAIL -> screen toaster");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();

        log.info("show dragDown");
        sleep(6000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer dragDown");

        refresh();
        log.info("show interstitial");
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial first click (link)");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-empty.png")), "FAIL -> screen interstitial empty");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-0.png")), "FAIL -> screen interstitial");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("check work sequence DragDown/Interstitial for one widget <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work sequence DragDown/Interstitial for one widget")
    public void mobileWidgetWithCustomTypeDragDownInterstitial() {
        log.info("Test is started");
        int widgetId = 511;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setShowAfterInteraction("2")
                .chooseMobileWidgetType(
                        Widget.MobileWidgetType.INTERSTITIAL,
                        Widget.MobileWidgetType.DRAGDOWN
                );
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("drag-down-interstitial"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);

        log.info("show toaster");
        softAssert.assertFalse(pagesInit.getWidgetClass().setShadowDom(true).isShowMobileContainer(), "FAIL -> AdWidgetContainer toaster");

        log.info("show dragDown");
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer dragDown");
        sleep(2000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-dragDown.png")), "FAIL -> screen dragDown");

        refresh();
        log.info("show interstitial");
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial first click (link)");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-empty.png")), "FAIL -> screen interstitial");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeDragDownInterstitial-interstitial-0.png")), "FAIL -> screen interstitial");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Story("Mobile widget")
    @Description("check work sequence Interstitial/Toaster/DragDown for one widget <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work sequence Interstitial/Toaster/DragDown for one widget")
    public void mobileWidgetWithCustomTypeInterstitialToasterDragDown() {
        log.info("Test is started");
        int widgetId = 512;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setShowAfterInteraction("2")
                .chooseMobileWidgetType(Widget.MobileWidgetType.INTERSTITIAL,
                        Widget.MobileWidgetType.DRAGDOWN,
                        Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("interstitial-toaster-drag-down"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(1);

        log.info("show interstitial");
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial first click (link)");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeInterstitialToasterDragDown-interstitial-empty.png")), "FAIL -> screen interstitial first click");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        pagesInit.getWidgetClass().setShadowDom(true).waitMobileContainer();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeInterstitialToasterDragDown-interstitial.png")), "FAIL -> screen interstitial second click");

        pagesInit.getWidgetClass().clickMobileContinueButton();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer interstitial click 'Continue'");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeInterstitialToasterDragDown-interstitial-click-continue.png")), "FAIL -> screen interstitial click 'Continue'");

        log.info("show toaster");
        sleep(7000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer toaster");
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeInterstitialToasterDragDown-toaster.png")), "FAIL -> screen toaster");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();

        log.info("show dragDown");
        sleep(6000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer dragDown");
        sleep(2000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "mobileWidgetWithCustomTypeInterstitialToasterDragDown-dragDown-0.png")), "FAIL -> screen dragDown");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
