package dashboard.adskeeper.publishers.widgetOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.Screenshot;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.locators.CreateEditWidgetLocators.LIKE_POST_BLOCK;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;

/*
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_LIGHT
 *      - UNDER_ARTICLE_DARK
 *      - UNDER_ARTICLE_CARD_MEDIA
 *
 * 2. SIDEBAR
 *      - SIDEBAR_LIGHT
 *      - SIDEBAR_DARK
 *
 * 3. IN_ARTICLE
 *      - IN_ARTICLE_CAROUSEL
 *      - IN_ARTICLE_CARD_MEDIA
 */
public class WidgetsOnStandAdskeeperScreenshot1Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandAdskeeperScreenshot1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        clientLogin = "testEmail43@ex.ua";
        subnetName = SUBNET_ADSKEEPER_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT")
    @Test
    public void standUnderArticleLightCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(362, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT,
                "under-article-light", "adskeeper-amp-under-article-light",
                "standUnderArticleLightCheckScreenshot", "standUnderArticleLightCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARD_MEDIA")
    @Test
    public void standUnderArticleCardMediaCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 394;
        log.info("ShadowDomEnabled = 0");
        operationMySql.getTickersComposite().updateShadowDomEnabled(0, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARD_MEDIA);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("under-article-card-media"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();

        softAssert.assertTrue(serviceInit.getScreenshotService()
                .compareScreenshots(serviceInit.getScreenshotService().addIgnoredElement(LIKE_POST_BLOCK).takeScreenshot(WebDriverRunner.getWebDriver(), locatorForWholePage),
                        serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standUnderArticleCardMediaCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("under-article-card-media"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService()
                .compareScreenshots(serviceInit.getScreenshotService().addIgnoreElementByCoords(locatorForScreen, locatorForMediaElements).takeScreenshot(WebDriverRunner.getWebDriver(), locatorForWholePage),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standUnderArticleCardMediaCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_DARK")
    @Test
    public void standUnderArticleDarkCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(352, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK,
                "under-article-dark", "adskeeper-amp-under-article-dark",
                "standUnderArticleDarkCheckScreenshot", "standUnderArticleDarkCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_LIGHT")
    @Test
    public void standSidebarLightCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(353, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT,
                "sidebar", "adskeeper-amp-sidebar-light",
                "standSidebarLightCheckScreenshot", "standSidebarLightCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_DARK")
    @Test
    public void standSidebarDarkCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(353, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK,
                "sidebar", "adskeeper-amp-sidebar-dark",
                "standSidebarDarkCheckScreenshot", "standSidebarDarkCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void standInArticleCarouselCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 354;
        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("in-article"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Screenshot actualScreen = serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen);
        Screenshot expectedScreen1 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInArticleCarouselCheckScreenshot.png");
        Screenshot expectedScreen2 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInArticleCarouselCheckScreenshot-1.png");
        softAssert.assertTrue((serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen1) ||
                serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen2)), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("adskeeper-amp-in-article-carousel"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + "standInArticleCarouselCheckScreenshot.png")), "FAIL -> screen amp");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CARD_MEDIA")
    @Test
    public void standInArticleCardMediaCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 395;
        log.info("ShadowDomEnabled = 0");
        operationMySql.getTickersComposite().updateShadowDomEnabled(0, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CARD_MEDIA);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("in-article-card-media"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService()
                .compareScreenshots(
                        serviceInit.getScreenshotService()
                                .addIgnoredElement(LIKE_POST_BLOCK)
                                .takeScreenshot(WebDriverRunner.getWebDriver(), locatorForWholePage),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInArticleCardMediaCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("in-article-card-media"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService()
                .compareScreenshots(serviceInit.getScreenshotService().addIgnoreElementByCoords(locatorForScreen, locatorForMediaElements).takeScreenshot(WebDriverRunner.getWebDriver(), locatorForWholePage),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInArticleCardMediaCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
