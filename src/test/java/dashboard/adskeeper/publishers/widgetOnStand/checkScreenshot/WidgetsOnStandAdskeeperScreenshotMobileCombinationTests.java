package dashboard.adskeeper.publishers.widgetOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.dash.publisher.logic.Widget;
import ru.yandex.qatools.ashot.Screenshot;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;

public class WidgetsOnStandAdskeeperScreenshotMobileCombinationTests extends ScreenshotWidgetStandCheckingTemplate {

    private final int mobileExitId = 516;
    private final int popUpId = 518;
    private final int inSiteNotificationId = 519;

    public WidgetsOnStandAdskeeperScreenshotMobileCombinationTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        clientLogin = "sok.autotest@mgid.com";
        subnetName = SUBNET_ADSKEEPER_NAME;
    }

    @BeforeClass
    public void createDataWidgets(){
        goToCreateWidget(mobileExitId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().saveWidgetSettings();

        int mobileToasterId = 517;
        goToCreateWidget(mobileToasterId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setToasterInactivityTime("11")
                .chooseMobileWidgetType(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        goToCreateWidget(popUpId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().saveWidgetSettings();

        goToCreateWidget(inSiteNotificationId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        int feedId = 520;
        goToCreateWidget(feedId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.FEED_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        int mobileDragDownId = 525;
        goToCreateWidget(mobileDragDownId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .chooseMobileWidgetType(Widget.MobileWidgetType.DRAGDOWN);
        pagesInit.getWidgetClass().saveWidgetSettings();
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .setSubnetId(subnetId);

        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Exit and Toaster -> set Inactivity for toaster and wait 10s(show exit), " +
            "and check toaster doesn't show")
    @Owner("RKO")
    @Test(description = "Check work combination Exit and Toaster with inactivity")
    public void checkWorkCombinationMobileExitAndMobileToaster() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("exit-toaster"))
                .setWidgetIds(mobileExitId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(11000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationMobileExitAndMobileToaster.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Exit and Toaster -> set Inactivity for toaster + scroll(for toaster), " +
            "and check show toaster")
    @Owner("RKO")
    @Test(description = "Check work combination Exit and Toaster with scroll")
    public void checkWorkCombinationMobileExitAndMobileToaster1() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("exit-toaster"))
                .setWidgetIds(mobileExitId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        pagesInit.getWidgetClass().waitShowToasterWidget();
        sleep(10000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationMobileExitAndMobileToaster1.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Exit and PopUp")
    @Owner("RKO")
    @Test(description = "Check work combination Exit and PopUp")
    public void checkWorkCombinationMobileExitAndPopUpWidget() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("pop-up-exit"))
                .setWidgetIds(popUpId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        pagesInit.getWidgetClass().mouseOutEvent();
        sleep(10000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationMobileExitAndPopUpWidget.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Feed and PopUp")
    @Owner("RKO")
    @Test(description = "Check work combination Feed and PopUp")
    public void checkWorkCombinationFeedAndPopUpWidget() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("feed-pop-up"))
                .setWidgetIds(popUpId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        pagesInit.getWidgetClass().mouseOutEvent();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationFeedAndPopUpWidget.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Exit and DragDown")
    @Owner("RKO")
    @Test(description = "Check work combination Exit and DragDown with inactivity")
    public void checkWorkCombinationMobileExitAndMobileDragDown() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("exit-drag-down"))
                .setWidgetIds(mobileExitId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(10000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationMobileExitAndMobileDragDown.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination PopUp and DragDown")
    @Owner("RKO")
    @Test(description = "Check work combination PopUp and DragDown")
    public void checkWorkCombinationPopUpAndMobileDragDown() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("pop-up-drag-down"))
                .setWidgetIds(popUpId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        sleep(6000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        pagesInit.getWidgetClass().mouseOutEvent();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationPopUpAndMobileDragDown.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination InSiteNotification and DragDown")
    @Owner("RKO")
    @Test(description = "Check work combination InSiteNotification and DragDown")
    public void checkWorkCombinationInSiteNotificationAndMobileDragDown() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification-drag-down"))
                .setWidgetIds(inSiteNotificationId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        sleep(6000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);

        Screenshot actualScreen = serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver());
        Screenshot expectedScreen1 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationInSiteNotificationAndMobileDragDown.png");
        Screenshot expectedScreen2 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationInSiteNotificationAndMobileDragDown-1.png");
        Assert.assertTrue((serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen1) ||
                serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen2)));
        log.info("Test is finished");
    }
}
