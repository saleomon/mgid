package dashboard.adskeeper.publishers.widgetOnStand.shadow;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.EndPoints.widgetTemplateUrl;

/*
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_LIGHT
 *      - UNDER_ARTICLE_DARK
 *      - UNDER_ARTICLE_CARD_MEDIA
 *
 * 2. SIDEBAR:
 *      - SIDEBAR_LIGHT
 *      - SIDEBAR_DARK
 *
 * 3. IN_ARTICLE
 *      - IN_ARTICLE_CAROUSEL
 *      - IN_ARTICLE_CARD_MEDIA
 *
 */
public class WidgetsOnStandAdskeeperShadow1Tests extends TestBase {

    public WidgetsOnStandAdskeeperShadow1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("testEmail43@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }


    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT")
    @Test
    public void editAndSaveWidgetForStandUnderArticleLight() {
        int widgetId = 149;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_DARK")
    @Test
    public void editAndSaveWidgetForStandUnderArticleDark() {
        int widgetId = 149;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARD_MEDIA")
    @Test
    public void editAndSaveWidgetForStandUnderArticleCardMedia() {
        int widgetId = 149;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARD_MEDIA);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARD_MEDIA);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARD_MEDIA));
        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "16px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#0010101", "FAIL: TITLE -> color");

        log.info("check Image styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");

        log.info("mgbottom_media");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("position"), "absolute", "FAIL: mgbottom_media -> position");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("bottom"), "0px", "FAIL: mgbottom_media -> bottom");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("left"), "10px", "FAIL: mgbottom_media -> left");

        log.info("mcmore style");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreStyle("text-align"), "left", "FAIL: mcmore -> text-align");

        log.info("mcmore-link style");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("display"), "inline-block", "FAIL: mcmore-link -> display");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMcMoreLinkStyle("color")), "#3267e8", "FAIL: mcmore-link -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("font-size"), "14px", "FAIL: mcmore-link -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("margin-bottom"), "5px", "FAIL: mcmore-link -> margin-bottom");

        log.info("like icon");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWorkLikeIcon(), "FAIL -> like icon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_LIGHT")
    @Test
    public void editAndSaveWidgetForStandSidebarLight() {
        int widgetId = 150;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_sidebar_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_DARK")
    @Test
    public void editAndSaveWidgetForStandSidebarDark() {
        int widgetId = 150;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_sidebar_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void editAndSaveWidgetForStandInArticleCarouselBlack() {
        int widgetId = 151;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CARD_MEDIA")
    @Test
    public void editAndSaveWidgetForStandInArticleCardMedia() {
        int widgetId = 151;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CARD_MEDIA);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CARD_MEDIA);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CARD_MEDIA));
        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "16px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#0010101", "FAIL: TITLE -> color");

        log.info("check Image styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");

        log.info("mgbottom_media");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("position"), "absolute", "FAIL: mgbottom_media -> position");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("bottom"), "0px", "FAIL: mgbottom_media -> bottom");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("left"), "10px", "FAIL: mgbottom_media -> left");

        log.info("mcmore style");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreStyle("text-align"), "left", "FAIL: mcmore -> text-align");

        log.info("mcmore-link style");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("display"), "inline-block", "FAIL: mcmore-link -> display");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMcMoreLinkStyle("color")), "#3267e8", "FAIL: mcmore-link -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("font-size"), "14px", "FAIL: mcmore-link -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcMoreLinkStyle("margin-bottom"), "5px", "FAIL: mcmore-link -> margin-bottom");

        log.info("like icon");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWorkLikeIcon(), "FAIL -> like icon");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
