package dashboard.adskeeper.publishers.widgetOnStand.shadow;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static testData.project.EndPoints.widgetTemplateUrl;

public class WidgetsOnStandAdskeeperShadowMobileTests extends TestBase {

    public WidgetsOnStandAdskeeperShadowMobileTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("testEmail43@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.MOBILE_EXIT, SubTypes.NONE; MobileEmulation(isMobileDevice = true)\n" +
            "<ul>\n" +
            "<li>Виджет должен показываться после бездействия пользователя 10 сек</li>\n" +
            "<li>Виджет должен отображаться только под мобильным</li>\n" +
            "<li>Theme должно менять цвет рамки</li>\n" +
            "<li>Popup title (дефолтное значение - Advertisement)</li>\n" +
            "</ul>")
    @Test
    public void editAndSaveWidgetForMobileExit() {
        int widgetId = 157;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_mobile_exit_on_mob_device_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        helpersInit.getBaseHelper().timerStart();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        softAssert.assertTrue(helpersInit.getBaseHelper().finishAndCheckTimer(10, 2), "FAIL -> timer false, widget doesn't show after 10 seconds");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE), "FAIL -> checkWidgetInStand");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
