package dashboard.adskeeper.publishers.widgetOnStand.shadow;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.adskeeperLogo;

/*
 * 1. HEADER
 *      - HEADER_LIGHT
 *      - HEADER_DARK
 *
 * 2. EXIT_POP_UP
 *
 * 3. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 *      - IN_SITE_NOTIFICATION_MEDIA
 *      - IN_SITE_NOTIFICATION_CHAT
 *
 * 4. MOBILE_EXIT: (editAndSaveWidgetForMobileExit_valid_desktopDevice)
 *
 * 5. feed
 */
public class WidgetsOnStandAdskeeperShadow2Tests extends TestBase {
    public WidgetsOnStandAdskeeperShadow2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("testEmail43@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.HEADER, SubTypes.HEADER_LIGHT")
    @Test
    public void editAndSaveWidgetForStandHeaderLight() {
        int widgetId = 152;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_LIGHT);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_LIGHT));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.HEADER, SubTypes.HEADER_DARK")
    @Test
    public void editAndSaveWidgetForStandHeaderDark() {
        int widgetId = 152;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_DARK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_DARK);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_DARK));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.EXIT_POP_UP, SubTypes.NONE")
    @Test
    public void editAndSaveWidgetForPopup() {
        int widgetId = 153;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_popup_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().mouseOutEvent();
        serviceInit.getServicerMock().countDownLatchAwait();

        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.EXIT_POP_UP, WidgetTypes.SubTypes.NONE));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.MOBILE_EXIT, SubTypes.NONE")
    @Test
    public void editAndSaveWidgetForMobileExit_valid_desktopDevice() {
        int widgetId = 154;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_mobile_exit_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        helpersInit.getBaseHelper().timerStart();
        serviceInit.getServicerMock().countDownLatchAwait(15);
        Assert.assertFalse(helpersInit.getBaseHelper().finishAndCheckTimer(10, 2), "FAIL -> timer false, widget after 10 seconds");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN \nПроверяем:\n -время отображения\n -стили блока 'title'\n -виджет исчезает после клика по иконке 'close'\n -check widget position(top/bottom \n" +
            "<ul>\n" +
            "<li>Новый формат виджета в дашборд Adskeeper \"In-site Notification <a href=\"https://jira.mgid.com/browse/TA-23614\">TA-23614</a></li>\n" +
            "<li>Расширение возможностей для формата виджета in-site notifications <a href=\"https://jira.mgid.com/browse/TA-24158\">TA-24158</a></li>\n" +
            "<li>Доработки виджета in-site notification <a href=\"https://jira.mgid.com/browse/TA-24512\">TA-24512</a></li>\n" +
            "</ul>")
    @Test
    public void editAndCheckWorkInSiteNotificationMainWidgetOnStand() {
        int widgetId = 155;
        String cssPosition;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass()
                .setRerunAds(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_site_notification_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "13px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#414a59", "FAIL: TITLE -> color");

        log.info("check widget position (top = 10px & bottom = 540px) | (top = 540px & bottom = 10px)");
        cssPosition = pagesInit.getWidgetClass().getPosition().equals("top") ? "bottom" : "top";
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgBoxStyle(pagesInit.getWidgetClass().getPosition()), "10px", "FAIL -> widget position size");
        softAssert.assertNotEquals(pagesInit.getWidgetClass().getMgBoxStyle(cssPosition), "10px", "FAIL -> widget position auto");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN));

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass().clickCloseIconInSiteNotification();
        serviceInit.getServicerMock().setWidgetIds(widgetId)
                .countDownLatchAwait(70);

        // get reRun flag
        // if true -> count requests to servicer = 2
        // if false -> count requests to servicer = 1
        int countRequestToServicer = 1;
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), countRequestToServicer, "FAIL -> size != " + countRequestToServicer);

        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MEDIA <a href=\"https://jira.mgid.com/browse/TA-51707\">TA-51707</a>")
    @Test
    public void editAndCheckWorkInSiteNotificationMediaWidgetOnStand() {
        int widgetId = 155;
        String cssPosition;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MEDIA);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MEDIA);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_site_notification_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "14px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE -> color");

        log.info("check Image styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("border-radius"), "50%", "FAIL: .mcimg -> border-radius");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "500", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-size"), "12px", "FAIL: DOMAIN -> font-size");

        log.info("mcdomain_now");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainNowStyle("position"), "relative", "FAIL: DOMAIN_now -> position");

        log.info("mcdomain_icon");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainIconStyle("max-height"), "20px", "FAIL: mcdomain_icon -> max-height");

        log.info("mgbottom_media");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("position"), "absolute", "FAIL: mgbottom_media -> position");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("bottom"), "6px", "FAIL: mgbottom_media -> bottom");
        softAssert.assertEquals(pagesInit.getWidgetClass().getButtomMediaStyle("left"), "13px", "FAIL: mgbottom_media -> left");

        log.info("like icon");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWorkLikeIcon(), "FAIL -> like icon");

        log.info("check widget position (top = 10px & bottom = 540px) | (top = 540px & bottom = 10px)");
        cssPosition = pagesInit.getWidgetClass().getPosition().equals("top") ? "bottom" : "top";
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgBoxStyle(pagesInit.getWidgetClass().getPosition()), "10px", "FAIL -> widget position size");
        softAssert.assertNotEquals(pagesInit.getWidgetClass().getMgBoxStyle(cssPosition), "10px", "FAIL -> widget position auto");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MEDIA));

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass().clickCloseIconInSiteNotification();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMgBox(), "FAIL -> mgbox is show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_CHAT <a href=\"https://jira.mgid.com/browse/TA-51707\">TA-51707</a>")
    @Test
    public void editAndCheckWorkInSiteNotificationChatWidgetOnStand() {
        int widgetId = 155;
        String cssPosition;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_CHAT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_CHAT);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_site_notification_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("check TextElements styles");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTextElementsStyle("background-color")), "#8d8da1", "FAIL: TextElements -> background-color");

        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "14px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE -> color");

        log.info("check Image styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("border-radius"), "50%", "FAIL: .mcimg -> border-radius");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("border"), "3px solid rgb(141, 141, 161)", "FAIL: .mcimg -> border");

        log.info("check widget position (top = 10px & bottom = 540px) | (top = 540px & bottom = 10px)");
        cssPosition = pagesInit.getWidgetClass().getPosition().equals("top") ? "bottom" : "top";
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgBoxStyle(pagesInit.getWidgetClass().getPosition()), "10px", "FAIL -> widget position size");
        softAssert.assertNotEquals(pagesInit.getWidgetClass().getMgBoxStyle(cssPosition), "10px", "FAIL -> widget position auto");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_CHAT));

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass().clickCloseIconInSiteNotification();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMgBox(), "FAIL -> mgbox is show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.FEED, SubTypes.NONE (infinite-scroll = true)")
    @Test
    public void editAndSaveWidgetForFeed() {
        int widgetId = 156;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.NONE)
                .setInfiniteScroll(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.NONE);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_feed_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.NONE), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 45), "FAIL -> infinite scroll");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css(Shadow DOM)")
    @Description("Types.FEED, SubTypes.NONE (infinite-scroll = false)")
    @Test
    public void editAndSaveWidgetForStandFeed_infiniteScroll_false() {
        int widgetId = 156;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.FEED_MAIN)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.FEED_MAIN);

        serviceInit.getServicerMock().setStandName(setStand("adskeeper_feed_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.FEED, WidgetTypes.SubTypes.NONE), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "277"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "277"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "199", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "248", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
