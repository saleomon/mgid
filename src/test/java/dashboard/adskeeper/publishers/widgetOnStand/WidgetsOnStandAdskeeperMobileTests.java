package dashboard.adskeeper.publishers.widgetOnStand;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.dash.publisher.logic.Widget;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.ClientsEntities.ADSKEEPER_WIDGET_MOBILE_EXIT_ID;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.adskeeperLogo;


public class WidgetsOnStandAdskeeperMobileTests extends TestBase {

    public WidgetsOnStandAdskeeperMobileTests() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "mobile-widgets/" + Subnets.SUBNET_ADSKEEPER_NAME + "/" +  stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .setSubnetId(subnetId)
                .getPathToWidgetsJsonData(widgetDataAdskeeper);

        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("Types.MOBILE_EXIT, SubTypes.NONE; MobileEmulation(isMobileDevice = true)\n" +
            "<ul>\n" +
            "<li>Виджет должен показываться после бездействия пользователя 10 сек</li>\n" +
            "<li>Виджет должен отображаться только под мобильным</li>\n" +
            "<li>Theme должно менять цвет рамки</li>\n" +
            "<li>Popup title (дефолтное значение - Advertisement)</li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "check work on stand Mobile Exit Widget")
    public void editAndSaveWidgetForMobileExit() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_MOBILE_EXIT_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("exit"))
                .setWidgetIds(ADSKEEPER_WIDGET_MOBILE_EXIT_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        helpersInit.getBaseHelper().timerStart();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        softAssert.assertTrue(helpersInit.getBaseHelper().finishAndCheckTimer(10, 2), "FAIL -> timer false, widget doesn't show after 10 seconds");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.MOBILE_EXIT, WidgetTypes.SubTypes.NONE), "FAIL -> checkWidgetInStand");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Toaster after scroll <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget after scroll")
    public void toasterWidgetWithScroll() {
        log.info("Test is started");
        int toasterId = 278;
        goToCreateWidget(toasterId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("toaster"))
                .setWidgetIds(toasterId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().waitShowToasterWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "400", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Toaster after 8s inactivity <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget after 8s inactivity")
    public void toasterWidgetShowAfter8sInactivity() {
        log.info("Test is started");
        int toasterId = 498;
        goToCreateWidget(toasterId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("toaster-with-inactivity"))
                .setWidgetIds(toasterId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        sleep(1000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "400", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Toaster after 2s inactivity <a href='https://jira.mgid.com/browse/TA-52234'>TA-52234</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget after 2s inactivity")
    public void toasterWidgetShowAfter2sInactivity() {
        log.info("Test is started");
        int toasterId = 513;
        goToCreateWidget(toasterId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .setToasterInactivityTime("2")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("toaster-with-inactivity-2s"))
                .setWidgetIds(toasterId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(2);
        sleep(1000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "400", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Toaster -> show widget + refresh page + show -> without Close icon <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget refresh without Close icon")
    public void toasterWidgetRefreshWithoutIconClose() {
        int widgetId = 507;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("step#1: wait 8s and widget should be visible");
        serviceInit.getServicerMock().setStandName(setStand("toaster-refresh-without-close"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("step#2: refresh page and wait 8s and widget should be visible");
        refresh();
        sleep(9000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        log.info("step#3: refresh page and wait 8s and widget should NOT be visible (FrequencyCappingImpressions=2)");
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Toaster -> show widget + Close icon + refresh page -> widget show after capping 1 min <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster with capping after click icon Close")
    public void toasterWidgetRefreshWithIconClose() {
        int widgetId = 508;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("step#1: wait 8s and widget should be visible");
        serviceInit.getServicerMock().setStandName(setStand("toaster-refresh-with-close"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("step#2: click 'close' icon " +
                "refresh page " +
                "wait 8s and widget should NOT be visible " +
                "wait 60s and widget should NOT visible " +
                "scroll widget down-up and widget visible");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        sleep(30000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        sleep(30000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");

        log.info("step#3: click 'close' icon " +
                "refresh page " +
                "wait 8s and widget should NOT be visible " +
                "wait 60s and widget should NOT be visible " +
                "scroll widget down-up and widget NOT be visible");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");
        sleep(60000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #5");

        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("DragDown widget -> check work " +
            "widget should not be show in the first 5s" +
            "widget should show after 5s and scroll down and after scroll up")
    @Owner("RKO")
    @Test(description = "check work dragDown widget")
    public void dragDownWidget() {
        log.info("Test is started");
        int dragDownId = 279;
        goToCreateWidget(dragDownId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .reSaveMobileWidget(Widget.MobileWidgetType.DRAGDOWN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("dragDown"))
                .setWidgetIds(dragDownId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        log.info("check -> widget doesn't show after 1s load");
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        sleep(1000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("check -> widget show after 5s loaded");
        sleep(2000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        sleep(1000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        log.info("check other settings");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "400", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#159417", "FAIL: DOMAIN -> color");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Interstitial after 2 clicks links on the site <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work Interstitial after 2 clicks links on the site")
    public void interstitialWidget() {
        log.info("Test is started");
        int interstitialId = 280;
        goToCreateWidget(interstitialId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setShowAfterInteraction("2")
                .reSaveMobileWidget(Widget.MobileWidgetType.INTERSTITIAL);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("interstitial"))
                .setWidgetIds(interstitialId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        softAssert.assertTrue(pagesInit.getWidgetClass().getAdMobileContainerAttribute("class").contains("interstitial"), "FAIL -> AdWidgetContainer");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), adskeeperLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "400", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#159417", "FAIL: DOMAIN -> color");

        log.info("adwidget-continue-cation");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContinueButton());
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonText(), "Continue");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMobileContinueButtonStyle("background")), "#27387a");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMobileContinueButtonStyle("color")), "#fff");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("height"), "60px");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("font-size"), "28px");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("font-family"), "Verdana, sans-serif");

        log.info("check work button 'Continue' click it ");
        pagesInit.getWidgetClass().clickMobileContinueButton();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css"), @Story(value = "Mobile widget")})
    @Description("check work Interstitial after custom (4) clicks links on the site <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work Interstitial custom (4) clicks links on the site")
    public void interstitialWidgetWithCustomShowAfterInteraction() {
        log.info("Test is started");
        int interstitialWithCustomShowId = 497;
        goToCreateWidget(interstitialWithCustomShowId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setShowAfterInteraction("4")
                .reSaveMobileWidget(Widget.MobileWidgetType.INTERSTITIAL);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("interstitial-for-custom-show"))
                .setWidgetIds(interstitialWithCustomShowId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(3);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");

        softAssert.assertTrue(pagesInit.getWidgetClass().getAdMobileContainerAttribute("class").contains("interstitial"), "FAIL -> AdWidgetContainer");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
