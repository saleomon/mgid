package dashboard.adskeeper.publishers.widgetOnStand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataAdskeeper;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.widgetTemplateUrl;

/*
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_LIGHT
 *      - UNDER_ARTICLE_DARK
 *
 * 2. SIDEBAR:
 *      - SIDEBAR_LIGHT
 *      - SIDEBAR_DARK
 *
 * 3. IN_ARTICLE
 *      - IN_ARTICLE_CAROUSEL
 *
 */
public class WidgetsOnStandAdskeeper1Tests extends TestBase {

    public WidgetsOnStandAdskeeper1Tests() {
        subnetId = SubnetType.SCENARIO_ADSKEEPER;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataAdskeeper);
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }


    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT")
    @Test
    public void editAndSaveWidgetForStandUnderArticleLight() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_under_article"))
                .setWidgetIds(ADSKEEPER_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(3);
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.adskeeper.co.uk")), "FAIL -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT), "FAIL -> check base settings");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_DARK")
    @Test
    public void editAndSaveWidgetForStandUnderArticleDark() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_under_article"))
                .setWidgetIds(ADSKEEPER_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_DARK));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_LIGHT")
    @Test
    public void editAndSaveWidgetForStandSidebarLight() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_sidebar"))
                .setWidgetIds(ADSKEEPER_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_LIGHT));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_DARK")
    @Test
    public void editAndSaveWidgetForStandSidebarDark() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_sidebar"))
                .setWidgetIds(ADSKEEPER_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_DARK));
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void editAndSaveWidgetForStandInArticleCarouselBlack() {
        log.info("Test is started");
        goToCreateWidget(ADSKEEPER_WIDGET_IN_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        serviceInit.getServicerMock().setStandName(setStand("adskeeper_in_article"))
                .setWidgetIds(ADSKEEPER_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL));
        log.info("Test is finished");
    }
}
