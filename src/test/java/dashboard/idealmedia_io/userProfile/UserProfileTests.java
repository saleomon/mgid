package dashboard.idealmedia_io.userProfile;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.userProfile.locators.UserProfileLocators.ADVERTISERS_TAB;
import static pages.dash.userProfile.locators.UserProfileLocators.PUBLISHERS_TAB;

public class UserProfileTests extends TestBase {
    public UserProfileTests(){
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    private String clientLogin = "sok.autotest.payouts.ukraine_2@mgid.com";
    public static final String MONDAY_UKR = "ПОНЕДІЛОК";
    public static final String SUNDAY_UKR = "НЕДІЛЯ";

    public void goToUserProfile(String clientLogin) {
        authDashAndGo(clientLogin, "profile/users");
        helpersInit.getBaseHelper().waitForReadyPage();
    }

    public void goToLink(SelenideElement element) {
        element.click();
        helpersInit.getBaseHelper().waitForReadyPage();
    }

    @Feature("Clients profile settings")
    @Story("Check first day of the week in user settings. Idealmedia.io.")
    @Description("Check default first day in user profile interface. \n" +
            "     <ul>\n" +
            "      <li>Checking what is for Idealmedia subnet client by default, the starting day of the calendar is set in the profile - Monday</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2757\">Ticket KOT-2757</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Check default day in user profile. Idealmedia.")
    public void checkDefaultFirstDayOfTheWeekInProfile() {
        log.info("Test is started");
        goToUserProfile(clientLogin);
        Assert.assertEquals(pagesInit.getUserProfile().getCurrentFirstDayOfTheWeek(), MONDAY_UKR);
        log.info("Test is finished");
    }

    @Feature("Clients profile settings")
    @Story("Change first day of the week in user settings. Idealmedia.io.")
    @Description("Change and check first day in user profile and checking it in calendar. \n" +
            "     <ul>\n" +
            "      <li>Change default day from Monday to Sunday in user profile.</li>\n" +
            "      <li>Checking that in calendar's in Advertise and Publishers first day in calendar is equal with user profile.</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2757\">Ticket KOT-2757</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Check changed first day in calendar. Idealmedia.")
    public void checkChangingDefaultDayOfCalendar(){
        try {
            log.info("Test is started");
            goToUserProfile(clientLogin);
            pagesInit.getUserProfile().selectAnotherStartDayOfTheWeek("sun");
            softAssert.assertEquals(pagesInit.getUserProfile().getCurrentFirstDayOfTheWeek(), SUNDAY_UKR, "Day from profile is incorrect!");
            goToLink(ADVERTISERS_TAB);
            softAssert.assertTrue(SUNDAY_UKR.toLowerCase().contains(pagesInit.getUserProfile().checkFirstCalendarDayInPublishersAndAdvertInterfaces().toLowerCase()), "Day from user profile and from calendar are difference!");
            goToLink(PUBLISHERS_TAB);
            softAssert.assertTrue(SUNDAY_UKR.toLowerCase().contains(pagesInit.getUserProfile().checkFirstCalendarDayInPublishersAndAdvertInterfaces().toLowerCase()), "Day from user profile and from calendar are difference!");
            softAssert.assertAll();
            log.info("Test is finished");
        }
        finally {
            operationMySql.getClients().updateClientsDashboardFirstDayOfTheWeek("mon", "3004");
        }
    }
}
