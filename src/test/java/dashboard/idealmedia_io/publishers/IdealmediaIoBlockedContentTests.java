package dashboard.idealmedia_io.publishers;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;

public class IdealmediaIoBlockedContentTests extends TestBase {

    public IdealmediaIoBlockedContentTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Test
    public void widgetStandCheckCloseIconBlockTeaserForWidget(){
        log.info("Test is started");
        int widgetId = 80;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_blocked_content"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserOnStand(), "FAIL -> show block teaser");
        authDashAndGo(pagesInit.getWidgetClass().getLinkOnBlockTeaser());

        softAssert.assertEquals(pagesInit.getWidgetClass().getBlockedTeaserTitle(),
                pagesInit.getBlockedContent().getTeaserTitle(), "FAIL -> block teaser doesn't wright");
        pagesInit.getBlockedContent().chooseBlockerType("widget");
        pagesInit.getBlockedContent().blockedPopupSave();
        helpersInit.getBaseHelper().closePopup();

        pagesInit.getBlockedContent().chooseWidgetInFilter(widgetId);
        softAssert.assertEquals(pagesInit.getBlockedContent().getTeaserBlockedType(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                "Заблоковані інформери",
                "FAIL -> don't correct blocked type");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
