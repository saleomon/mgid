package dashboard.idealmedia_io.publishers.widgetOnStand.shadow;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.idealmediaIoLogo;

public class WidgetsOnStandIdealmediaIoShadow2Tests extends TestBase {

    public WidgetsOnStandIdealmediaIoShadow2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("testEmail44@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    /**
     * idealmedia_carousel
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24449">TA-24449</a>
     * <p>RKO</p>
     */
    @Test
    public void editAndSaveWidgetForStandCarousel() {
        int widgetId = 245;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_carousel_main_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_carousel_main_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_carousel_vertical
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24449">TA-24449</a>
     * <p>RKO</p>
     */
    @Test
    public void editAndSaveWidgetForStandCarouselVertical() {
        int widgetId = 162;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_carousel_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_carousel_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_smart_main, idealmedia_smart_blur
     * <ul>check</ul>
     * <li>infinite-scroll = true</li>
     * <p>RKO</p>
     */
    @Test(dataProvider = "smartType")
    public void editAndSaveWidgetForStandSmartMain_infiniteScroll_true(WidgetTypes.SubTypes type) {
        int widgetId = 163;
        log.info("Test is started: " + type);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, type)
                .setInfiniteScroll(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, type);

        serviceInit.getServicerMock().setStandName(setStand("idealmedia_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwaitWithoutTearDown();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 45), "FAIL -> infinite scroll");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_smart_shadow"))
                .setServicerCountDown(3)
                .navigate()
                .scrollToSlow()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 30), "FAIL -> amp infinite scroll");
        softAssert.assertAll();
        log.info("Test is finished: " + type);
    }

    @DataProvider
    public Object[][] smartType(){
        return new Object[][]{
                {WidgetTypes.SubTypes.SMART_MAIN},
                {WidgetTypes.SubTypes.SMART_BLUR}
        };
    }

    /**
     * idealmedia_smart_main
     * <ul>check</ul>
     * <li>infinite-scroll = false</li>
     * <li>base settings</li>
     * <p>RKO</p>
     */
    @Test
    public void editAndSaveWidgetForStandSmartMain_infiniteScroll_false() {
        int widgetId = 163;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);

        serviceInit.getServicerMock().setStandName(setStand("idealmedia_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "202"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "313"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "204"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "313"), "FAIL: .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: DOMAIN -> color");


        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL AMP  -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "202"), "FAIL AMP: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "313"), "FAIL AMP: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "204"), "FAIL AMP: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "313"), "FAIL AMP: .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL AMP: DOMAIN -> color");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_smart_blur
     * <ul>check</ul>
     * <li>infinite-scroll = false</li>
     * <li>base settings</li>
     * <p>RKO</p>
     */
    @Test
    public void editAndSaveWidgetForStandSmartBlur_infiniteScroll_false() {
        int widgetId = 163;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR);

        serviceInit.getServicerMock().setStandName(setStand("idealmedia_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "200", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL -> blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL -> blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL -> blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> blur row4");


        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL AMP: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL AMP: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL AMP: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL AMP: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "200", 2), "FAIL AMP: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP : TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL AMP: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL AMP:  TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL AMP: DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL  AMP-> blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL AMP -> blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL AMP -> blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL AMP -> blur row4");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_header_rectangular
     */
    @Test
    public void editAndSaveWidgetForStandHeaderRectangular() {
        int widgetId = 164;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_header_square
     */
    @Test
    public void editAndSaveWidgetForStandHeaderSquare() {
        int widgetId = 164;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "fail -> widget don't check");
        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
