package dashboard.idealmedia_io.publishers.widgetOnStand;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.fffColor;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.*;

/**
 * 1. sidebar-widget
 *      - sidebar-widget-cards
 *      - sidebar-widget-rectangular
 *      - sidebar-widget-square-small
 *      - sidebar-widget-impact
 *      - sidebar-widget-blur
 *      - sidebar-widget-frame
 * 2. banner
 *      - banner-470x325
 */
public class WidgetsOnStandIdealmediaIo3Tests extends TestBase {

    public WidgetsOnStandIdealmediaIo3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    /**
     * idealmedia_sidebar_cards
     */
    @Test
    public void editAndSaveWidgetForStandSidebarCards() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_sidebar_rectangular
     */
    @Test
    public void editAndSaveWidgetForStandSidebarRectangular() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_sidebar_square_small
     */
    @Test
    public void editAndSaveWidgetForStandSidebarSquareSmall() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_sidebar_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_sidebar_impact
     */
    @Test
    public void editAndSaveWidgetForStandSidebarImpact() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_sidebar"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "267.281px", "FAIL: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL: .mcimg -> object-fit");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#000", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-size"), "14px", "FAIL: DOMAIN -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainTopStyle("color")), "#2a71b6", "FAIL: DOMAIN -> color");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("background")), "#2a71b6", "FAIL: MglBtnStyle -> background");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "УЗНАТЬ БОЛЬШЕ", "FAIL -> MglBtnStyle: text");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_sidebar"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "267.281px", "FAIL AMP : .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL AMP: .mcimg -> object-fit");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL AMP: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#000", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-family"), "Arial, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-weight"), "700", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-size"), "14px", "FAIL AMP: DOMAIN -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainTopStyle("color")), "#2a71b6", "FAIL AMP: DOMAIN -> color");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL AMP:  MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("background")), "#2a71b6", "FAIL AMP: MglBtnStyle -> background");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "УЗНАТЬ БОЛЬШЕ", "FAIL  AMP -> MglBtnStyle: text");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_sidebar_blur
     */
    @Test
    public void editAndSaveWidgetForStandSidebarBlur() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_sidebar"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "267.281px", "FAIL: .mcimg -> height");


        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkBlurEffectOnMouseHover(), "FAIL: blur effect on mouse hover");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_sidebar"))
                .setWidgetIds(IDEALMEDIA_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "267.281px", "FAIL: .mcimg -> height");


        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL AMP: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL AMP: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkBlurEffectOnMouseHover(), "FAIL AMP: blur effect on mouse hover");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_sidebar_frame
     */
    @Test
    public void editAndSaveWidgetForStandSidebarFrame() {
        log.info("Test is started");
        int widgetId = 537;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia-sidebar-frame"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "246.125px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        log.info("TITLE (.mctitle>a): " + convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")));
        softAssert.assertTrue(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")).matches(fffColor), "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertTrue(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")).matches(fffColor), "FAIL: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSidebarFrameHover(), "FAIL: blur effect on mouse hover");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia-amp-sidebar-frame"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoIdealmediaIoLinkInStand(), idealmediaIoLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "246.125px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL AMP: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL AMP : DOMAIN -> font-weight");
        softAssert.assertTrue(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")).matches(fffColor), "FAIL AMP: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSidebarFrameHover(), "FAIL AMP: blur effect on mouse hover");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_banner
     */
    @Test
    public void editAndSaveWidgetForStandBanner() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_BANNER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_banner"))
                .setWidgetIds(IDEALMEDIA_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_banner"))
                .setWidgetIds(IDEALMEDIA_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * banner-470x325
     */
    @Test
    public void editAndSaveWidgetForStandBanner_470x325() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_BANNER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_banner"))
                .setWidgetIds(IDEALMEDIA_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        //header (logo, show_name)
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerLogoImg(), idealmediaIoBannerLogo, "FAIL: logo");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBannerCampaignName(), "FAIL: show_name");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerWidgetTitle(), "Sponsored", "FAIL: widget title");

        //check button '.mglbtn'
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-family"), "\"Times New Roman\"", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-weight"), "700", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-size"), "11px", "FAIL: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getIdealMglBtnStyle("color")), "#1f2224", "FAIL: MglBtnStyle -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnText(), "Узнать больше", "FAIL: MglBtnStyle -> text");

        //TITLE (.mctitle>a)
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "15px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE(.mctitle>a) -> font-color");

        //.mcimg
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "182.641px", "FAIL: .mcimg -> height");


        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_banner"))
                .setWidgetIds(IDEALMEDIA_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();

        //header (logo, show_name)
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerLogoImg(), idealmediaIoBannerLogo, "FAIL AMP: logo");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBannerCampaignName(), "FAIL AMP: show_name");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerWidgetTitle(), "Sponsored", "FAIL AMP: widget title");

        //check button '.mglbtn'
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-family"), "\"Times New Roman\"", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-weight"), "700", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnStyle("font-size"), "11px", "FAIL AMP: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getIdealMglBtnStyle("color")), "#1f2224", "FAIL AMP: MglBtnStyle -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getIdealMglBtnText(), "Узнать больше", "FAIL AMP: MglBtnStyle -> text");

        //TITLE (.mctitle>a)
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "15px", "FAIL AMP: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        //.mcimg
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "182.641px", "FAIL AMP: .mcimg -> height");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
