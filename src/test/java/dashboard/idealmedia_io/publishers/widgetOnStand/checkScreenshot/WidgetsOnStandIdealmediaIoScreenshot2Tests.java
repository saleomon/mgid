package dashboard.idealmedia_io.publishers.widgetOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.Screenshot;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_IDEALMEDIA_IO_NAME;

/*
 * 1. CAROUSEL
 *      - CAROUSEL_MAIN
 *      - CAROUSEL_VERTICAL
 *
 * 2. SMART
 *      - SMART_MAIN
 *      - SMART_BLUR
 *
 * 3. HEADER
 *      - HEADER_RECTANGULAR
 *      - HEADER_SQUARE
 */
public class WidgetsOnStandIdealmediaIoScreenshot2Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandIdealmediaIoScreenshot2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        clientLogin = "testEmail50@ex.ua";
        subnetName = SUBNET_IDEALMEDIA_IO_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.CAROUSEL, SubTypes.CAROUSEL_MAIN")
    @Test
    public void standCarouselMainCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(460, WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN,
                "carousel-main", "idealmedia-amp-carousel",
                "standCarouselMainCheckScreenshot", "standCarouselMainCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.CAROUSEL, SubTypes.CAROUSEL_VERTICAL")
    @Test
    public void standCarouselVerticalCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 214;
        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("carousel"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Screenshot actualScreen = serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen);
        Screenshot expectedScreen1 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standCarouselVerticalCheckScreenshot.png");
        Screenshot expectedScreen2 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standCarouselVerticalCheckScreenshot-1.png");
        softAssert.assertTrue((serviceInit.getScreenshotService().setDiffSizeTrigger(15).compareScreenshots(actualScreen, expectedScreen1) ||
                serviceInit.getScreenshotService().setDiffSizeTrigger(15).compareScreenshots(actualScreen, expectedScreen2)), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia-amp-carousel-vertical"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + "standCarouselVerticalCheckScreenshot.png")), "FAIL -> screen amp");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SMART, SubTypes.SMART_MAIN")
    @Test
    public void standSmartMainCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(215, WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN,
                "smart", "idealmedia-amp-smart-main",
                "standSmartMainCheckScreenshot", "standSmartMainCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SMART, SubTypes.SMART_BLUR")
    @Test
    public void standSmartBlurCheckScreenshot2() {
        log.info("Test is started");
        int widgetId = 215;
        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("smart"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Screenshot actualScreen = serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen);
        Screenshot expectedScreen1 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standSmartBlurCheckScreenshot.png");
        Screenshot expectedScreen2 = serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standSmartBlurCheckScreenshot-1.png");
        softAssert.assertTrue((serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen1) ||
                serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen2)), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia-amp-smart-blur"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + "standSmartBlurCheckScreenshot.png")), "FAIL -> screen amp");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_RECTANGULAR")
    @Test
    public void standHeaderRectangularCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(216, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR,
                "header", "idealmedia-amp-header-rectangular",
                "standHeaderRectangularCheckScreenshot", "standHeaderRectangularCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_SQUARE")
    @Test
    public void standHeaderSquareCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(216, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE,
                "header", "idealmedia-amp-header-square",
                "standHeaderSquareCheckScreenshot", "standHeaderSquareCheckScreenshot");
        log.info("Test is finished");
    }
}
