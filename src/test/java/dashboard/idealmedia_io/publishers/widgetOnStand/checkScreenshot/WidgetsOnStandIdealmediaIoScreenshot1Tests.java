package dashboard.idealmedia_io.publishers.widgetOnStand.checkScreenshot;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_IDEALMEDIA_IO_NAME;


/*
 * Check widgets on stand with screenshot
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_CARDS
 *      - UNDER_ARTICLE_RECTANGULAR
 *      - UNDER_ARTICLE_SQUARE
 *
 * 2. IN_ARTICLE
 *      - IN_ARTICLE_MAIN
 *      - IN_ARTICLE_IMPACT
 *      - IN_ARTICLE_DOUBLE_PICTURE
 *
 * 3. TEXT_ON_IMAGE
 */
public class WidgetsOnStandIdealmediaIoScreenshot1Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandIdealmediaIoScreenshot1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        clientLogin = "testEmail50@ex.ua";
        subnetName = SUBNET_IDEALMEDIA_IO_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS")
    @Test
    public void standUnderArticleCardsCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(211, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS,
                "under-article", "idealmedia-amp-under-article-cards",
                "standUnderArticleCardsCheckScreenshot", "standUnderArticleCardsCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_RECTANGULAR")
    @Test
    public void standUnderArticleRectangularCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(211, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR,
                "under-article", "idealmedia-amp-under-article-rectangular",
                "standUnderArticleRectangularCheckScreenshot", "standUnderArticleRectangularCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE")
    @Test
    public void standUnderArticleSquareCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(211, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE,
                "under-article", "idealmedia-amp-under-article-square",
                "standUnderArticleSquareCheckScreenshot", "standUnderArticleSquareCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_MAIN")
    @Test
    public void standInArticleMainCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(212, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN,
                "in-article", "idealmedia-amp-in-article-main",
                "standInArticleMainCheckScreenshot", "standInArticleMainCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_IMPACT")
    @Test
    public void standInArticleImpactCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(212, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT,
                "in-article", "idealmedia-amp-in-article-impact",
                "standInArticleImpactCheckScreenshot", "standInArticleImpactCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_DOUBLE_PICTURE")
    @Test
    public void standInArticleDoublePictureCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(212, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE,
                "in-article", "idealmedia-amp-in-article-double-picture",
                "standInArticleDoublePictureCheckScreenshot", "standInArticleDoublePictureCheckScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.TEXT_ON_IMAGE, SubTypes.NONE")
    @Test
    public void standTextOnImageCheckScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(213, WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE,
                "text-on-image", "idealmedia-amp-text-on-image",
                "standTextOnImageCheckScreenshot", "standTextOnImageCheckScreenshot");
        log.info("Test is finished");
    }
}
