package dashboard.idealmedia_io.publishers.widgetOnStand.checkScreenshot;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_IDEALMEDIA_IO_NAME;

/*
 * 1. SIDEBAR
 *      - SIDEBAR_CARDS
 *      - SIDEBAR_RECTANGULAR
 *      - SIDEBAR_SQUARE_SMALL
 *      - SIDEBAR_IMPACT
 *      - SIDEBAR_BLUR
 *      - SIDEBAR_FRAME
 *
 * 2. BANNER
 *      - BANNER_470x325
 */
public class WidgetsOnStandIdealmediaIoScreenshot3Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandIdealmediaIoScreenshot3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        clientLogin = "testEmail50@ex.ua";
        subnetName = SUBNET_IDEALMEDIA_IO_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_CARDS")
    @Test
    public void standSidebarCardsScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS,
                "sidebar", "idealmedia-amp-sidebar-cards",
                "standSidebarCardsScreenshot", "standSidebarCardsScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR")
    @Test
    public void standSidebarRectangularScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR,
                "sidebar", "idealmedia-amp-sidebar-rectangular",
                "standSidebarRectangularScreenshot", "standSidebarRectangularScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_SQUARE_SMALL")
    @Test
    public void standSidebarSquareSmallScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL,
                "sidebar", "idealmedia-amp-sidebar-square-small",
                "standSidebarSquareSmallScreenshot", "standSidebarSquareSmallScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_IMPACT")
    @Test
    public void standSidebarImpactScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT,
                "sidebar", "idealmedia-amp-sidebar-impact",
                "standSidebarImpactScreenshot", "standSidebarImpactScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_BLUR")
    @Test
    public void standSidebarBlurScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR,
                "sidebar", "idealmedia-amp-sidebar-blur",
                "standSidebarBlurScreenshot", "standSidebarBlurScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_FRAME")
    @Test
    public void standSidebarFrameScreenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(217, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME,
                "sidebar", "idealmedia-amp-sidebar-frame",
                "standSidebarFrameScreenshot", "standSidebarFrameScreenshot");
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) Idealmedia.Io -> check screenshot")
    @Description("Types.BANNER, SubTypes.BANNER_470x325")
    @Test
    public void standBanner470x325Screenshot() {
        log.info("Test is started");
        standHelperCheckScreenshot(218, WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325,
                "banner", "idealmedia-amp-banner-470-325",
                "standBanner470x325Screenshot", "standBanner470x325Screenshot");
        log.info("Test is finished");
    }
}
