package dashboard.idealmedia_io.publishers.widgetOnStand;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static core.helpers.BaseHelper.convertRgbaToHex;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

/*
 * 1. under-article-widget
 *      - under-article-widget-cards
 *      - under-article-widget-rectangular
 *      - under-article-widget-square
 *
 * 2. in-article
 *      - in-article-main
 *      - in-article-double-picture
 *      - in-article-impact
 *
 * 3. text-on-image
 *
 */
public class WidgetsOnStandIdealmediaIo1Tests extends TestBase {

    public WidgetsOnStandIdealmediaIo1Tests() {
        subnetId = SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }


    /**
     * idealmedia_under_article_cards
     */
    @Test
    public void editAndSaveWidgetForStandUnderArticleCards() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.idealmedia.io")), "FAIL -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(3);
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.idealmedia.io")), "FAIL AMP -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "fail -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_under_article_rectangular
     */
    @Test
    public void editAndSaveWidgetForStandUnderArticleRectangular() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_under_article_square
     */
    @Test
    public void editAndSaveWidgetForStandUnderArticleSquare() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_under_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_in_article_main
     */
    @Test
    public void editAndSaveWidgetForStandInArticleMain() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_IN_ARTICLE_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_in_article_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_in_article_with_detailed"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .setAutoplacement("amp")
                .checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_in_article_double_picture
     */
    @Test
    public void editAndSaveWidgetForStandInArticleDoublePicture() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_IN_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_in_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkAutoplacementOnStand(), "FAIL -> checkAutoplacementOnStand");

        log.info(".mg-button>svg");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton_Svg("fill")), "#fff", "FAIL: .mg-button>svg -> font-color");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info(".mcimg-inner");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("width"), "75px", "FAIL: .mcimg-inner -> width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("height"), "75px", "FAIL: .mcimg-inner -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("float"), "left", "FAIL: .mcimg-inner -> float");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "250px", "FAIL: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL: .mcimg -> object-fit");

        log.info("mg-button");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton("background-color")), "#004e6db", "FAIL: .mg-button -> background-color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("font-size"), "25px", "FAIL: .mg-button -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("position"), "absolute", "FAIL: .mg-button -> position");


        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_in_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();
        softAssert.assertTrue(pagesInit.getWidgetClass().setAutoplacement("amp").checkAutoplacementOnStand(), "FAIL  AMP-> checkAutoplacementOnStand");

        log.info(".mg-button>svg");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton_Svg("fill")), "#fff", "FAIL AMP: .mg-button>svg -> font-color");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL AMP: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info(".mcimg-inner");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("width"), "75px", "FAIL AMP: .mcimg-inner -> width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("height"), "75px", "FAIL AMP: .mcimg-inner -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("float"), "left", "FAIL AMP: .mcimg-inner -> float");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "250px", "FAIL AMP: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL AMP: .mcimg -> object-fit");

        log.info("mg-button");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton("background-color")), "#004e6db", "FAIL AMP: .mg-button -> background-color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("font-size"), "25px", "FAIL AMP: .mg-button -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("position"), "absolute", "FAIL AMP: .mg-button -> position");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_in_article_impact
     */
    @Test
    public void editAndSaveWidgetForStandInArticleImpact() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_IN_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_in_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkAutoplacementOnStand(), "FAIL -> checkAutoplacementOnStand");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "500", "FAIL: DOMAIN -> font-weight");

        log.info("check TITLE '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "600", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("overflow"), "visible", "FAIL: TITLE -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("text-overflow"), "clip", "FAIL: TITLE -> text-overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("display"), "contents", "FAIL: TITLE -> display");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("max-height"), "52px", "FAIL: TITLE -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-line-clamp"), "none", "FAIL: TITLE -> webkit-line-clamp");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-box-orient"), "horizontal", "FAIL: TITLE -> webkit-box-orient");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE -> font-size");

        log.info("check DESCRIPTION '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("overflow"), "hidden", "FAIL: DESCRIPTION -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("max-height"), "38px", "FAIL: DESCRIPTION -> max-height");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "600", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "УЗНАТЬ БОЛЬШЕ", "FAIL -> MglBtnStyle: text");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_in_article"))
                .setWidgetIds(IDEALMEDIA_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();
        softAssert.assertTrue(pagesInit.getWidgetClass().setAutoplacement("amp").checkAutoplacementOnStand(), "FAIL AMP -> checkAutoplacementOnStand");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "500", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-size"), "15px", "FAIL AMP: DOMAIN -> font-size");

        log.info("check TITLE '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "600", "FAIL AMP: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "20px", "FAIL AMP: TITLE -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("overflow"), "visible", "FAIL AMP: TITLE -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("text-overflow"), "clip", "FAIL AMP: TITLE -> text-overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("display"), "contents", "FAIL AMP: TITLE -> display");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("max-height"), "52px", "FAIL AMP: TITLE -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-line-clamp"), "none", "FAIL AMP: TITLE -> webkit-line-clamp");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-box-orient"), "horizontal", "FAIL AMP: TITLE -> webkit-box-orient");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL AMP: TITLE -> font-size");

        log.info("check DESCRIPTION '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("overflow"), "hidden", "FAIL AMP: DESCRIPTION -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("font-size"), "17px", "FAIL AMP: DESCRIPTION -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("max-height"), "38px", "FAIL AMP: DESCRIPTION -> max-height");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "600", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL AMP: MglBtnStyle -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "УЗНАТЬ БОЛЬШЕ", "FAIL AMP -> MglBtnStyle: text");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * idealmedia_text_on_image
     */
    @Test
    public void editAndSaveWidgetForStandTextOnImage() {
        log.info("Test is started");
        goToCreateWidget(IDEALMEDIA_WIDGET_TEXT_ON_IMAGE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("idealmedia_text_on_image"))
                .setWidgetIds(IDEALMEDIA_WIDGET_TEXT_ON_IMAGE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("idealmedia_amp_text_on_image"))
                .setWidgetIds(IDEALMEDIA_WIDGET_TEXT_ON_IMAGE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
