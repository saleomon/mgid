package dashboard.idealmedia_io.publishers;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.advertiser.variables.TeaserVariables.*;
import static testData.project.ClientsEntities.CLIENTS_VIDEO_LOGIN;
import static testData.project.EndPoints.idealmediaIoLink;

public class ExchangeNewsTests extends TestBase {
    public ExchangeNewsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        clientLogin = CLIENTS_VIDEO_LOGIN;
    }

    @BeforeMethod
    public void initializeVariables() {
        pagesInit.getWidgetClass().setSubnetId(subnetId);
    }

    /**
     * Не обновляется дата в поле news_1.when_change при редактировании новостей
     * Case: редактирование новости в дашборде и проверка обновления времени
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27410">Ticket TA-27410</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkTimeWhenChangedAfterEditingNews() {
        log.info("Test is started");
        String newsId = "2006";
        operationMySql.getNews1().updateActiveStatus(newsId);
        String whenChangeTimeAtStart = operationMySql.getNews1().getWhenChange(newsId);

        authDashAndGo("publisher/teasers-news/campaign_id/2001");
        log.info("Edit news");
        pagesInit.getNewsClass().openNewsEditInterface(newsId);
        pagesInit.getNewsClass().isGenerateNewValues(true).editNews();
        String whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        Assert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed after editing!");

        log.info("Test is finished");
    }

    /**
     * Check import news in Dashboard
     *
     * @see <a href=https://jira.mgid.com/browse/TA-51819">Ticket TA-51819</a>
     * <p>Author AIA</p>
     */
    // todo AIA @Test
    public void checkImportNewsIdealmedia() {
        log.info("Test is started");
        int campaignID = 2003;
        authDashAndGo("publisher/teasers-news/campaign_id/" + campaignID);
        pagesInit.getNewsClass().importNews();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(successImportTextForIdealmedia),
                "FAIL -> import message!");
        open(idealmediaIoLink + "publisher/teasers-news/campaign_id/" + campaignID + "/search/" + autocreativeNewsTitle);
        softAssert.assertTrue(pagesInit.getNewsClass().checkImportedNews(),
                "FAIL -> Import news!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Создание и редактирование новости с неразрывными пробелами заголовке и описании\n" +
            "     <ul>\n" +
            "      <li>в форме создания новости в дешборде</li>\n" +
            "      <li>в интерфейсе списка новостей в дешборде</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test
    public void createNewsWithNbsp() {
        log.info("Test is started");
        int campaignID = 2003;
        String originalStringValue = "Test&nbsp;news&nbsp;with&nbsp;No-Break&nbsp;Space";
        String checkStringValue = "Test news with No-Break Space";

        authDashAndGo("publisher/add-teaser-news/campaign_id/" + campaignID);
        pagesInit.getNewsClass().createNews(originalStringValue);

        Assert.assertTrue(pagesInit.getNewsClass().checkNewsWithNbsp(checkStringValue),
                "FAIL -> Check for CREATED news in LIST Interface!");

        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Редактирование новости с неразрывными пробелами заголовке и описании\n" +
            "     <ul>\n" +
            "      <li>в форме редактироваания новости в дешборде</li>\n" +
            "      <li>в интерфейсе списка новостей в дешборде</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test
    public void editNewsWithNbsp() {
        log.info("Test is started");
        String campaignId = "2001";
        String newsId = "2014";
        String checkStringValue = "Test news 2014";

        authDashAndGo("publisher/teasers-news/campaign_id/" + campaignId + "/search/" + newsId);
        log.info("Edit news");
        pagesInit.getNewsClass().openNewsEditInterface(newsId);
        pagesInit.getNewsClass().reSaveNewsWithNbsp();

        Assert.assertTrue(pagesInit.getNewsClass().checkNewsWithNbsp(checkStringValue),
                "FAIL -> Check for EDITED news in LIST Interface!");

        log.info("Test is finished");
    }
}
