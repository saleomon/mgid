package dashboard.idealmedia_io.publishers;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.publisher.variables.PayoutsVariables.subjectLetterIdealmedioIo;
import static testData.project.ClientsEntities.*;

public class IdealmediaIoPayoutsTests extends TestBase {

    public IdealmediaIoPayoutsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        clientLogin = "sok.autotest.payouts_im_io@mgid.com";
    }

    private final String clientId = "2030";


    /**
     * Add wallet Paymaster24 (WMZ), USD (id=249) and check wallet params
     *
     * @see <a href "https://youtrack.mgid.com/issue/TA-50001">Ticket TA-50001</a>
     * <p>Author AIA</p>
     */
    //todo https://jira.mgid.com/browse/KOT-3324 @Test
    public void addAndCheckPaymaster24PurseIdealmediaIo() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "249");
        authDashAndGo("publisher/payouts");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterIdealmedioIo);
        log.info("* Add payment method Paymaster24 (WMZ), USD *");
        pagesInit.getPayoutsClass().addPaymaster24Purse("Paymaster24", "Z431071031918");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterIdealmedioIo)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after creating!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after creating!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterIdealmedioIo);
        pagesInit.getPayoutsClass().editPaymaster24("Paymaster24", "Z573184336595");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterIdealmedioIo)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after editing!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description("Валидация добавления кошелька WMZ для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить WMZ кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Добавление кошелька Webmoney для клиента из Украины для зеркала Idealmedia.io")
    public void forbiddenToAddWebMoneyWalletFromUkraine() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_IDEALMEDIA_IO_EMAIL,"publisher/payouts");
        log.info("* Add payment method Webmoney USD*");
        pagesInit.getPayoutsClass().addPurse("Webmoney", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "БУДЬ ЛАСКА, СКОРИСТАЙТЕСЬ ІНШИМ МЕТОДОМ ОПЛАТИ."),
                "FAIL -> Message is incorrect!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description ("Валидация добавления кошелька Paymaster24 для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить Paymaster24 кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    //todo https://jira.mgid.com/browse/KOT-3324 @Test (description = "Добавление кошелька Paymaster24 для клиента из Украины для зеркала Idealmedia.io")
    public void forbiddenToAddWalletPaymaster24FromUkraine(){
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN,"publisher/payouts");
        log.info("* Add payment method Paymaster24 USD*");
        pagesInit.getPayoutsClass().addPaymaster24PurseUkraine("Paymaster24", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "БУДЬ ЛАСКА, СКОРИСТАЙТЕСЬ ІНШИМ МЕТОДОМ ОПЛАТИ."),
                "FAIL -> Message is incorrect!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
