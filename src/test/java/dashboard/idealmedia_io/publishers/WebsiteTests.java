package dashboard.idealmedia_io.publishers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static core.service.constantTemplates.ConstantsInit.AIA;

public class WebsiteTests extends TestBase {
    public WebsiteTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    @Epic("Publisher Website")
    @Feature("Automatically generate the ads.txt file right after creation of a new domain")
    @Story("Websites list")
    @Description("Check adding ads.txt after creating of the website" +
            "* @see <a href=\"https://jira.mgid.com/browse/TA-52423\">Ticket TA-52423</a>")
    @Owner(AIA)
    @Test(description = "Automatically generate the ads.txt file right after creation of a new domain")
    public void checkAdsTxtAfterAddingWebSiteAndActivate() {
        log.info("Test is started");
        log.info("create new site");
        authDashAndGo("publisher/add-site");
        pagesInit.getWebsiteClass().addWebSiteIdealmedia();
        authCabAndGo("wages/sites?id=" + pagesInit.getWebsiteClass().getSiteId());
        pagesInit.getCabWebsites().editTierInline();
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getWebsiteClass().getSiteId() + "/filters/%252Fid%252F" + pagesInit.getWebsiteClass().getSiteId());
        pagesInit.getCabWebsites()
                .setTier("5")
                .chooseTier()
                .editAdvertisersDomain()
                .saveEditWebsite();
        pagesInit.getCabWebsites().approveWebSiteIdealmedia(Integer.parseInt(pagesInit.getWebsiteClass().getSiteId()));
        log.info("check ads.txt in list interface in cab");
        authCabAndGo("wages/sites?id=" + pagesInit.getWebsiteClass().getSiteId());
        Assert.assertTrue(pagesInit.getCabWebsites().checkEditAdsTxtIcon(pagesInit.getWebsiteClass().getSiteId()),
                "FAIL -> edit ads.txt icon!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
