package dashboard.idealmedia_io.publishers.createEditWidgets;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;

/**
 * 1. in-article:
 *      - in-article-main
 *      - in-article-impact
 *      - in-article-double-picture
 *
 * 2. carousel-super:
 *      - carousel-super-main
 *      - carousel-super-vertical
 */
public class CreateWidgetsIdealmediaIo3Tests extends TestBase {

    public CreateWidgetsIdealmediaIo3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }


    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
    }

    public void goToCreateWidget(){
        int siteId = 64;
        authDashAndGo("testEmail37@ex.ua", "publisher/add-widget/site/" + siteId);
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - In-Article Widget, FORMAT: MAIN
     * <p>RKO</p>
     */
    @Test
    public void createInArticleMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - In-Article Widget, FORMAT: IMPACT
     * <p>RKO</p>
     */
    @Test
    public void createInArticleImpactWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - In-Article Widget, FORMAT: in-article-double-picture
     * <p>RKO</p>
     */
    @Test
    public void createInArticleDoublePictureWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - "carousel-super", "carousel-super-main" Widget
     * <p>RKO</p>
     */
    @Test
    public void createCarouselMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - "carousel-super", "carousel-super-vertical" Widget
     * <p>RKO</p>
     */
    @Test
    public void createCarouselVerticalWidget() {
        log.info("Test is started");
        authDashAndGo("testEmaileqt@ex.ua", "publisher/add-widget/site/" + 3);
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.CAROUSEL, WidgetTypes.SubTypes.CAROUSEL_VERTICAL);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super-vertical doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super-vertical doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: carousel-super-vertical doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
