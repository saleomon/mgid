package dashboard.idealmedia_io.publishers.createEditWidgets;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;

/*
 * 1. banner
 *     - banner-728x90_2
 *     - banner-470x325
 *
 * 2. text-on-image
 *
 * 3. header-widget
 *      - header-widget-rectangular
 *      - header-widget-square
 */
public class CreateWidgetsIdealmediaIo4Tests extends TestBase {

    public CreateWidgetsIdealmediaIo4Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }


    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
    }

    public void goToCreateWidget(){
        int siteId = 64;
        authDashAndGo("testEmail37@ex.ua", "publisher/add-widget/site/" + siteId);
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - IAB Display Standard Ad Unit(Banner) Widget
     * <p>RKO</p>
     */
    @Test
    public void createBannerWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_728x90_2);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - banner-470x325
     * <p>RKO</p>
     */
    @Test
    public void createBanner__banner_470x325() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check after CREATE");
        softAssert.assertEquals(operationMySql.getTickersComposite().getCtaRequired(pagesInit.getWidgetClass().getWidgetId()), "1", "FAIL: tickers_composite.cta_required !=1");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - text-on-image
     * <p>RKO</p>
     */
    @Test
    public void createTextOnImageWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.TEXT_ON_IMAGE, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Header Widget, FORMAT: RECTANGULAR
     * RKO
     */
    @Test
    public void createHeaderRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Header Widget, FORMAT: SQUARE
     * RKO
     */
    @Test
    public void createHeaderSquareWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
