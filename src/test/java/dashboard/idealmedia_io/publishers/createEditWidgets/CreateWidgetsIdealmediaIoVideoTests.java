package dashboard.idealmedia_io.publishers.createEditWidgets;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testData.project.publishers.VideoCfgData;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;

import static core.helpers.BaseHelper.stringToJson;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;
import static testData.project.ClientsEntities.*;

/*
 * 1. Instream video
 * 2. Outstream Over video
 */
public class CreateWidgetsIdealmediaIoVideoTests extends TestBase {

    VideoCfgData videoCfgData;
    public CreateWidgetsIdealmediaIoVideoTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
        videoCfgData = new VideoCfgData(subnetId);
    }

    @BeforeMethod
    public void initializeVariables() {
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
    }

    public void goToCreateWidget() {
        authDashAndGo(CLIENTS_VIDEO_LOGIN,"publisher/add-widget/site/" + WEBSITE_IDEALMEDIA_FOR_VIDEO_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget IDEALMEDIA in Dashboard")
    @Description("VIDEO::check privilege work 'can_manipulate_with_video'")
    @Owner(RKO)
    @Test(description = "VIDEO::check privilege work 'can_manipulate_with_video'")
    @Privilege(name = "default/publisher/can_manipulate_with_video")
    public void checkVideoIsAvailableForClient() {
        log.info("Test is started");
        authDashAndGo("testEmail83@ex.ua", "publisher/add-widget/site/" + 146);
        Assert.assertFalse(pagesInit.getWidgetClass().videoTumblerIsDisplayed());
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget IDEALMEDIA in Dashboard")
    @Description("VIDEO::InStream")
    @Owner(RKO)
    @Test(description = "VIDEO::InStream")
    public void createInStreamWidget() {
        log.info("Test is started");
        goToCreateWidget();

        log.info("create widget");
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertNotNull(pagesInit.getWidgetClass().createVideoWidget(VideoFormat.INSTREAM, true, false),
                "FAIL -> Video Instream Desktop widget was not created!");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.INSTREAM, true, false), "FAIL -> checkVideoWidget");

        log.info("edit widget");
        pagesInit.getWidgetClass().editVideoWidget(VideoFormat.INSTREAM, true, true);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.INSTREAM, true, true), "FAIL -> checkVideoWidget after edit");

        int compositeId = pagesInit.getWidgetClass().getWidgetId();
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(
                        pagesInit.getWidgetClass().getWidgetId()).stream().anyMatch(i -> i.equals("vr")),
                "FAIL -> VR placement type is absent");

        authCabAndGo("wages/widgets/?c_id=" + compositeId);
        authCabAndGo("wages/informers-manage-video-cfg/id/" + pagesInit.getCabWidgetsList().getVideoPartId(compositeId));
        softAssert.assertEquals(pagesInit.getCabInformersManageVideoCfg().getVideoCfgJson(),
                stringToJson(videoCfgData.generateInstreamCfgJson(  compositeId, true, true)), "FAIL -> videoCfg(json) check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget IDEALMEDIA in Dashboard")
    @Description("VIDEO::OutStream")
    @Owner(RKO)
    @Test(description = "VIDEO::OutStream")
    public void createOutStreamOverWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertNotNull(pagesInit.getWidgetClass().createVideoWidget(VideoFormat.OUTSTREAM, false, true),
                "FAIL -> Video Outstream 'Over' Desktop widget was not created!");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.OUTSTREAM, false, true), "FAIL -> checkVideoWidget");

        log.info("edit widget");
        pagesInit.getWidgetClass().editVideoWidget(VideoFormat.OUTSTREAM, true, true);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.OUTSTREAM, true, true), "FAIL -> checkVideoWidget after edit");

        int compositeId = pagesInit.getWidgetClass().getWidgetId();
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(
                        compositeId).stream().noneMatch(i -> i.equals("vr")),
                "FAIL -> VR placement type is present");
        authCabAndGo("wages/widgets/?c_id=" + compositeId);
        authCabAndGo("wages/informers-manage-video-cfg/id/" + pagesInit.getCabWidgetsList().getVideoPartId(compositeId));
        softAssert.assertEquals(pagesInit.getCabInformersManageVideoCfg().getVideoCfgJson(),
                stringToJson(videoCfgData.generateOutstreamCfgJson(  compositeId, true, true)), "FAIL -> videoCfg(json) check");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
