package dashboard.idealmedia_io.publishers.createEditWidgets;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;

/*
 * 1. smart:
 *      - smart-main
 *      - smart-blur
 * 
 * 2. under-article-widget:
 *      - under-article-widget-cards
 *      - under-article-widget-rectangular
 *      - under-article-widget-square
 *
 */
public class CreateWidgetsIdealmediaIo1Tests extends TestBase {
    private final int siteId = 64;

    public CreateWidgetsIdealmediaIo1Tests() {
        subnetId = SubnetType.SCENARIO_IDEALMEDIA_IO;
    }


    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
    }

    public void goToCreateWidget(){
        authDashAndGo("testEmail37@ex.ua", "publisher/add-widget/site/" + siteId);
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Smart, FORMAT: smart-main
     * <p>RKO</p>
     */
    @Test
    public void createSmartMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Smart, FORMAT: smart-blur
     * <p>RKO</p>
     */
    @Test
    public void createSmartBlurWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Under Article Widget, FORMAT: CARDS
     * <p>RKO</p>
     */
    @Test
    public void createUnderArticleCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");

        authDashAndGo("publisher/widgets/site/" + siteId);
        softAssert.assertTrue(pagesInit.getWidgetClass().deleteWidget(), "FAIL: createUnderArticleCardsWidget doesn't delete");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Under Article Widget, FORMAT: RECTANGULAR
     * <p>RKO</p>
     */
    @Test
    public void createUnderArticleRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Under Article Widget, FORMAT: SQUARE
     * <p>RKO</p>
     */
    @Test
    public void createUnderArticleSquareWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
