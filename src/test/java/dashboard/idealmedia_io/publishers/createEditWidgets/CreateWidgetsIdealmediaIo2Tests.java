package dashboard.idealmedia_io.publishers.createEditWidgets;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataIdealmediaIo;

/**
 * 1. sidebar-widget
 *      - sidebar-widget-cards
 *      - sidebar-widget-rectangular
 *      - sidebar-widget-square-small
 *      - sidebar-widget-square-impact
 *      - sidebar-widget-blur
 *      - sidebar-widget-frame
 *
 */
public class CreateWidgetsIdealmediaIo2Tests extends TestBase {

    public CreateWidgetsIdealmediaIo2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }


    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setSubnetId(subnetId);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataIdealmediaIo);
    }

    public void goToCreateWidget(){
        int siteId = 64;
        authDashAndGo("testEmail37@ex.ua", "publisher/add-widget/site/" + siteId);
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: CARDS
     * RKO
     */
    @Test
    public void createSidebarCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: RECTANGULAR
     * RKO
     */
    @Test
    public void createSidebarRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: SMALL
     * RKO
     */
    @Test
    public void createSidebarSmallWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: sidebar-widget-impact
     */
    @Test
    public void createSidebarImpactWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: sidebar-widget-blur
     */
    @Test
    public void createSidebarBlurWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создаём/редактируем/проверяем/удаляем - Sidebar Widget, FORMAT: sidebar-widget-frame
     */
    @Test
    public void createSidebarFrameWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
