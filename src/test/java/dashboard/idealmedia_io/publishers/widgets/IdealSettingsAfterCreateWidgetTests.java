package dashboard.idealmedia_io.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Collections;

import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.goodWidgetSettingUrl;

public class IdealSettingsAfterCreateWidgetTests extends TestBase {

    public IdealSettingsAfterCreateWidgetTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Idealmedia.Io")
    @Description("Check save news categories block on his created: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-27518\">TA-27518</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "Check save news categories block on his created")
    public void checkSetNewsCategoryOnCreateWidget(){
        log.info("Test is started");
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_IDEALMEDIA_WAGES_ID);

        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().setCategoriesBlock();
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getWidgetId());
        String newsLink = pagesInit.getCabWidgetsList().getLinkEditNewsSubWidget();
        authCabAndGo(newsLink);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> categoryFilter_Only UI");
        softAssert.assertEquals(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").size(), pagesInit.getWidgetClass().getCategoriesList().size(), "FAIL -> not equals categories (on create)");

        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType("0");
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getCategoryTumblerStatus(), "off", "FAIL -> category tumbler != off");

        log.info("turn on category tumbler and set new category");
        pagesInit.getWidgetClass().turnOnCategoryTumbler(true);
        pagesInit.getWidgetClass().setCategoriesBlock();
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        authCabAndGo(newsLink);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> categoryFilter_Only UI");
        softAssert.assertEquals(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").size(), pagesInit.getWidgetClass().getCategoriesList().size(), "FAIL -> not equals categories (on edit)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Idealmedia.Io")
    @Description("Impact settings after created: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-23615\">TA-23615</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24426\">TA-24426</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24413\">TA-24413</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "check Impact settings after create from dashboard")
    public void checkImpactWidgetSettings(){
        log.info("Test is started");
        authDashAndGo(CLIENTS_EMAIL, "publisher/add-widget/site/" + 57);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().switchMonetizationTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(1);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("checkImpactWidgetSettings: " + pagesInit.getWidgetClass().getWidgetId());
        int g_blocks = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());

        log.info("check news amount on disabled");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisabled_NewsAmount(), "FAIL -> news amount isn't disabled");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(true), "FAIL -> Place reservation");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");

        log.info("get product id and go on Product Part - check 'brandformance' base settings");
        authCabAndGo(goodWidgetSettingUrl + g_blocks);

        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCampaignTierFilterValue(), "Fail -> tier filter doesn't has default value 0 (All)");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().descriptionRequiredIsDisabled(), "FAIL -> description Required is disabled");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().isSelectedCustomCheckboxInCategoryFilter("Content promotions"), "Fail -> 'Content promotions' - is selected");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(pagesInit.getWidgetClass().getWidgetId()), Collections.singletonList("29"), "FAIL -> bundles");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
