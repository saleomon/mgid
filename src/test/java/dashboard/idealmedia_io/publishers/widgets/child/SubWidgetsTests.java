package dashboard.idealmedia_io.publishers.widgets.child;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.exchangeWidgetSettingsUrl;

public class SubWidgetsTests extends TestBase {

    private SubWidgetsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    @Feature("SubWidget")
    @Story("Subwidget Idealmedia_io")
    @Description("create new subWidget and check copy settings from parent widget: " +
            "- is_original_title <a href='https://jira.mgid.com/browse/TA-51942'>TA-51942</a>")
    @Test
    public void createSubwidgetAndCheckCloneSettings() {
        log.info("Test is started");
        String widgetId = "396";
        int tickersId;

        authDashAndGo("testEmail65@ex.ua", "publisher/widgets/site/104");
        log.info("Create subwidget with rule traffic_types");
        pagesInit.getWidgetClass().createSubwidget(widgetId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("Check created rule");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCreatedRule(pagesInit.getWidgetClass().getCloneId()), "False - saved settings not meet with required");
        tickersId = operationMySql.getTickers().getTickersId(pagesInit.getWidgetClass().getCloneId());

        authCabAndGo(exchangeWidgetSettingsUrl + tickersId);
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkOriginalTitlesOnlyCheckbox(true), "FAIL -> is_original_title switch_on in UI");
        softAssert.assertEquals(operationMySql.getTickers().getIsOriginalTitle(tickersId), 1, "FAIL -> is_original_title switch_on in DB");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
