package dashboard.idealmedia_io.publishers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextIdealmediaIO;
import static pages.dash.signup.variables.SignUpPageVariables.subjectRegistrationLetterIdealmediaIO;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.idealmediaIoLink;

public class RegistrationPublishersIdealmediaIOTests extends TestBase {

    public RegistrationPublishersIdealmediaIOTests() {
        subnetId = Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO;
    }

    /**
     * Create client with role Publisher
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerPublisherIdealmedia() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersBySubject(subjectRegistrationLetterIdealmediaIO);
        log.info("Let's Register new publisher!");
        open(idealmediaIoLink + "/user/signup");
        pagesInit.getSignUp().registerNewClient(NEW_PUBLISHER_LOGIN_IDEALMEDIA_IO);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextIdealmediaIO),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(idealmediaIoLink + helpersInit.getBaseHelper().extractUrls(operationMySql.getMailPull().getBodyFromMail(
                subjectRegistrationLetterIdealmediaIO)).get(0).substring(idealmediaIoLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_PUBLISHER_LOGIN_IDEALMEDIA_IO, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSelfRegisterPopupIdealmedia(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getWebsiteClass().setupProfileInPopupIdealmedia();
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkLoginOnPage(NEW_PUBLISHER_LOGIN_IDEALMEDIA_IO),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertEquals(operationMySql.getClients().getAdDarknessCheckboxInNewClient(NEW_PUBLISHER_LOGIN_IDEALMEDIA_IO), "0" ,
                "FAIL -> flag can_change_ad_darkness = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Restore password")
    @Feature("Password restoring in Idealmedia")
    @Story("Main dashboard page")
    @Description("Check password restoring in Idealmedia dashboard and login with new password")
    @Owner("AIA")
    @Test(description = "Check password restoring in Idealmedia dashboard and login with new password")
    public void checkRestorePasswordIdealmedia() {
        log.info("Test is started");
        String userLogin = "restore-password.client@idealmedia.io";
        operationMySql.getMailPull().getCountLettersByClientEmail(userLogin);
        open(idealmediaIoLink + "/user/restore-password");
        pagesInit.getSignUp().restorePassword(userLogin);
        open(idealmediaIoLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(userLogin)).get(0).substring("https://dashboard.idealmedia.io".length()));
        pagesInit.getSignUp().createPassword(NEW_RESTORED_PASSWORD);
        pagesInit.getSignUp().signInDash(userLogin, NEW_RESTORED_PASSWORD);
        Assert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(userLogin),
                "FAIL -> Login after password restoring is failed!");
        log.info("Test is finished");
    }
}
