package dashboard.mgid.userProfile;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.userProfile.locators.UserProfileLocators.ADVERTISERS_TAB;
import static pages.dash.userProfile.locators.UserProfileLocators.PUBLISHERS_TAB;

public class UserProfileTests extends TestBase {
        public UserProfileTests(){
            subnetId = Subnets.SubnetType.SCENARIO_MGID;
        }

    private String clientLogin = "sok.autotest.payouts.ukraine_mg@mgid.com";
    private String clientLoginTeaserManipulation = "testemail1000@ex.ua";
    public static final String SUNDAY_US = "SUNDAY";
    public static final String MONDAY_US = "MONDAY";

    public void goToUserProfile(String clientLogin) {
        authDashAndGo(clientLogin, "profile/users");
        helpersInit.getBaseHelper().waitForReadyPage();
    }

    public void goToLink(SelenideElement element) {
        element.scrollTo().click();
        helpersInit.getBaseHelper().waitForReadyPage();
    }


    @Feature("Clients profile settings")
    @Story("Check first day of the week in user settings. MGID.")
    @Description("Check default first day in user profile interface. \n" +
            "     <ul>\n" +
            "      <li>Checking what is for MGID subnet client by default, the starting day of the calendar is set in the profile - Sunday</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2757\">Ticket KOT-2757</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Check default day in user profile. MGID.")
    public void checkDefaultFirstDayOfTheWeekInProfile() {
        log.info("Test is started");
        goToUserProfile(clientLogin);
        Assert.assertEquals(pagesInit.getUserProfile().getCurrentFirstDayOfTheWeek(), SUNDAY_US);
        log.info("Test is finished");
    }

    @Feature("Clients profile settings")
    @Story("Automatic emails of cloaking block | NOTIFICATIONS dashboard")
    @Description("Check notification checkbox due blocking by cloaking\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52024\">Ticket TA-52024</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check notification due blocking by cloaking")
    public void checkNotificationDueBlockingByCloaking(){
        log.info("Test is started");
        goToUserProfile(clientLoginTeaserManipulation);
        pagesInit.getUserProfile().enableDisableNotificationCloakingCheckbox(true);
        pagesInit.getUserProfile().saveSettings();

        goToUserProfile(clientLoginTeaserManipulation);
        softAssert.assertTrue(pagesInit.getUserProfile().checkStateNotificationCloakingCheckbox(true));

        pagesInit.getUserProfile().enableDisableNotificationCloakingCheckbox(false);
        pagesInit.getUserProfile().saveSettings();

        goToUserProfile(clientLoginTeaserManipulation);
        softAssert.assertTrue(pagesInit.getUserProfile().checkStateNotificationCloakingCheckbox(false));

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
