package dashboard.mgid.advertisers;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.sorted.SortedType;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.randomNumbersInt;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.OthersData.LINK_TO_RESOURCES_FILES;

public class SelectiveBiddingNew1Tests extends TestBase {
    public SelectiveBiddingNew1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    private void goToSelectiveBidding(int id, String...link){
        String partOfLink = link.length > 0 ? link[0]: "";
        String login = "testemail1000@ex.ua";
        authDashAndGo(login, "advertisers/campaign-quality-analysis/v2/" + id + partOfLink);
        sleep(5);
        waitForAjax();
        checkErrors();
    }

    private void goToCampaignFilters(int id,int filterId){
        authCabAndGo("goodhits/campaigns-filters-v2/filter/" + filterId + "/pid/" + id);
        waitForAjax();
        checkErrors();
    }

    /**
     * Check sort data by providers layer
     * <p> NIO </p>
     */
    @Test
    public void checkSortColumnsProviders() {
        log.info("Test is started");
        log.info("Prepare test data");
        String[] randomColumn = {"CPC", "Spent", "Clicks"};
        String choosenElement = randomColumn[randomNumbersInt(randomColumn.length)];
        SortedType.SortType type = new SortedType().randomPeriod();
        log.info("Column, type - " + choosenElement + ", " + type);
        goToSelectiveBidding(1111);

        Assert.assertTrue(pagesInit.getSelectiveBinding2().checkSortedSelectiveProviders(type,
                choosenElement), choosenElement + ", " + type);
        log.info("Test is finished");
    }
    /**
     * Check filter by status
     * <p> NIO </p>
     */
    @Test
    public void checkStatusFilter() {
        log.info("Test is started");
        int allSources;
        int sourcesWithNeededStatus;
        goToSelectiveBidding(1113);

        log.info("Pre conditions steps");
        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("block");
        goToSelectiveBidding(1113);

        pagesInit.getSelectiveBinding2().chooseRandomProvider();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("unblock");

        log.info("Filter by blocked");
        goToSelectiveBidding(1113);

        pagesInit.getSelectiveBinding2().filterByStatus("Blocked");
        pagesInit.getSelectiveBinding2().expandAllProviders(2);

        log.info("Get data's and check - blocked");
        allSources = pagesInit.getSelectiveBinding2().getNumberOfAllSources();
        sourcesWithNeededStatus = pagesInit.getSelectiveBinding2().getNumberOfBlockedSources();
        Assert.assertEquals(allSources, sourcesWithNeededStatus);

        log.info("Filter by active");
        goToSelectiveBidding(1113);

        pagesInit.getSelectiveBinding2().filterByStatus("Active");
        pagesInit.getSelectiveBinding2().expandAllProviders(1);

        log.info("Get data's and check - active");
        allSources = pagesInit.getSelectiveBinding2().getNumberOfAllSources();
        sourcesWithNeededStatus = pagesInit.getSelectiveBinding2().getNumberOfUnblockedSources();
        Assert.assertEquals(allSources, sourcesWithNeededStatus);

        log.info("Test is finished");
    }

    /**
     * Check filter by coefficient
     * <p> NIO </p>
     */
    @Test
    public void checkCoefficientFilter() {
        log.info("Test is started");
        int allSources;
        int providersWithNeededCoefficient;
        goToSelectiveBidding(1112);


        log.info("Filter by initial");
        pagesInit.getSelectiveBinding2().filterByCoefficient("Initial");

        log.info("Get data's and check - initial");
        pagesInit.getSelectiveBinding2().expandAllProviders(2);
        allSources = pagesInit.getSelectiveBinding2().getNumberOfAllProviders();
        providersWithNeededCoefficient = pagesInit.getSelectiveBinding2().getNumberOfDisplayedCoefIcons();
        Assert.assertNotEquals(allSources, providersWithNeededCoefficient, "first");

        log.info("Filter by changed");
        goToSelectiveBidding(1112);

        pagesInit.getSelectiveBinding2().filterByCoefficient("Changed");
        pagesInit.getSelectiveBinding2().expandAllProviders(1);

        log.info("Get data's and check - changed");
        allSources = pagesInit.getSelectiveBinding2().getNumberOfAllProviders();
        providersWithNeededCoefficient = pagesInit.getSelectiveBinding2().getNumberOfDisplayedCoefIcons();
        Assert.assertEquals(allSources, providersWithNeededCoefficient, "second");

        log.info("Test is finished");
    }

    /**
     * Check import option source data for filter only
     * <p> NIO </p>
     */
    @Test
    public void checkImportOptionForOnlyFilter() {
        log.info("Test is started");
        int activeSourcesInterface;
        int activeElementsAdd = 3; // 2 - active sources, 1 - provider
        int activeElementsReplace = 5; // 3 - active sources, 2 - provider
        int activeElementsDelete = 2; // 1 - active sources, 1 - provider
        goToSelectiveBidding(1004);
        log.info("Import add sources");
        goToCampaignFilters(1004, 1);
        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_1.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("add");

        log.info("Check added elements");
        goToSelectiveBidding(1004);

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsAdd, activeSourcesInterface, "Add");

        log.info("Import replace sources");
        goToCampaignFilters(1004, 1);

        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_2.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("replace");

        log.info("Check added elements");
        goToSelectiveBidding(1004);
        pagesInit.getSelectiveBinding2().expandAllProviders(2);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsReplace, activeSourcesInterface, "Replace");

        log.info("Import delete sources");
        goToCampaignFilters(1004, 1);
        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_3.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("delete");

        log.info("Check added elements");
        goToSelectiveBidding(1004);
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsDelete, activeSourcesInterface, "Deletion");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check import option source data for filter only
     * <p> NIO </p>
     */
    @Test
    public void checkImportOptionForExceptFilter() {
        log.info("Test is started");
        int activeSourcesInterface;
        int activeElementsAdd = 2; // 1 - active sources, 1 - provider
        int activeElementsReplace = 5; // 3 - active sources, 2 - provider
        int activeElementsDelete = 3; // 1 - active sources, 1 - provider

        log.info("Import add sources");
        goToSelectiveBidding(1005);
        goToCampaignFilters(1005, 2);

        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_1.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("add");

        log.info("Check added elements");
        goToSelectiveBidding(1005);
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsAdd, activeSourcesInterface, "add");

        log.info("Import replace sources");
        goToCampaignFilters(1005, 2);

        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_2.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("replace");

        log.info("Check added elements");
        goToSelectiveBidding(1005);
        pagesInit.getSelectiveBinding2().expandAllProviders(2);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsReplace, activeSourcesInterface, "replace");

        log.info("Import delete sources");
        goToCampaignFilters(1005, 2);

        pagesInit.getFilterBlockedPublishers().chooseFileForImport(LINK_TO_RESOURCES_FILES + "campaign_filter_active_3.txt");
        pagesInit.getFilterBlockedPublishers().selectImportOptionsAndSave("delete");

        log.info("Check added elements");
        goToSelectiveBidding(1005);

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        activeSourcesInterface = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        softAssert.assertEquals(activeElementsDelete, activeSourcesInterface, "delete");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
