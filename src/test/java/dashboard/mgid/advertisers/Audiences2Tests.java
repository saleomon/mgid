package dashboard.mgid.advertisers;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.*;

public class Audiences2Tests extends TestBase {
    /**
     * Add Audience with existing target: valid case ( add/check(ui) )
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test
    public void createAudience_WithTargets() {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("targets")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience");

        pagesInit.getAudiences().clearData().setAudienceType("targets").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add Audience with existing target(not valid case): create audience with same targets
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test
    public void createAudience_WithTargets_NotValid() {
        List<Map.Entry<String, String>> pairList = new ArrayList<>();
        pairList.add(new AbstractMap.SimpleEntry<>("reached", "1"));
        pairList.add(new AbstractMap.SimpleEntry<>("reached", "1"));

        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNull(pagesInit.getAudiences()
                .setAudienceType("targets")
                .setTargetsData(pairList)
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Duplicate targets"), "FAIL -> valid message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * EventTarget: doesn't valid case
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test(dataProvider = "eventTargetValue")
    public void createAudience_WithEventTarget_NotValid(String value, String message) {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNull(pagesInit.getAudiences()
                .setAudienceType("event")
                .setTargetEvent(value)
                .createAudienceWithTarget(), "FAIL -> audience is create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(message), "FAIL -> valid message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] eventTargetValue() {
        return new Object[][]{
                {"clickdb", "A goal with this identity already exists"},
                {"click db", "Invalid characters entered"},
                {"", "Fill all fields with appropriate values"}
        };
    }

    /**
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     * VisitedTarget: doesn't valid case
     * <p>RKO</p>
     */
    @Test(dataProvider = "visitedValue")
    public void createAudience_WithVisited_NotValid(String pagesToVisit, String domainForTracking, String... messages) {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNull(pagesInit.getAudiences()
                .setAudienceType("visited")
                .setPagesToVisit(pagesToVisit)
                .setDomainForTracking(domainForTracking)
                .createAudienceWithTarget(), "FAIL -> audience is create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(messages), "FAIL -> valid message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] visitedValue() {
        return new Object[][]{
                {"", "", "Incorrect domain", "The input value must be a number bigger than 0"},
                {"-23", "test", "Incorrect domain", "The input value must be a number bigger than 0"}
        };
    }

    /**
     * UrlTarget: doesn't valid case
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test(dataProvider = "urlValue")
    public void createAudience_WithUrl_NotValid(LinkedHashMap<String, String> urlData, String... messages) {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNull(pagesInit.getAudiences()
                .setAudienceType("url")
                .setUrlData(urlData)
                .createAudienceWithTarget(), "FAIL -> audience is create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(messages), "FAIL -> valid message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] urlValue() {
        return new Object[][]{
                {new LinkedHashMap<String, String>() {
                    {
                        put("starts", "https://test");
                        put("ends", "@");
                        put("contain", ".com");
                    }
                },
                        "The input value can only contain letters, numbers and special characters '+', '='. '-', '.', '/','_', '&'."
                },
                {new LinkedHashMap<String, String>() {
                    {
                        put("starts", "");
                        put("ends", "@");
                        put("contain", "@");
                    }
                },
                        "You didn't specify a target URL."
                },
                {new LinkedHashMap<String, String>() {
                    {
                        put("starts", "123");
                        put("ends", ".com");
                        put("contain", "test");
                    }
                },
                        "'123' is not a valid uri"
                },
                {new LinkedHashMap<String, String>() {
                    {
                        put("starts", "");
                        put("ends", "");
                        put("contain", "");
                    }
                },
                        "You didn't specify a target URL."
                }
        };
    }

    /**
     * Add audience (name/description): doesn't valid case
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test(dataProvider = "nameDescriptionValue")
    public void createAudience_NameDescription_NotValid(String name, String description, String... messages) {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNull(pagesInit.getAudiences()
                .setAudienceType("visited")
                .setName(name)
                .setDescription(description)
                .createAudienceWithTarget(), "FAIL -> audience is create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(messages), "FAIL -> valid message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] nameDescriptionValue() {
        return new Object[][]{
                {
                        "testHGvbjsnflksmdfgdmsfgfashkdf",
                        "testHGvbjsnflksmdfgdmsfgfashkdfaslkdfnsadgaisdflcvoxcvasdtasddbchdnaiasdnchzvzxcbcxzjchg122",
                        "Value exceeds limit", "Value exceeds limit"
                },
                {
                        "",
                        "",
                        "Value is required and can't be empty"
                },
                {
                        "Audience name cxz",
                        "test",
                        "You already have a step with the same name. Step names should be unique."
                }
        };
    }

    /**
     * Check delete targets
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51713">TA-51713</a>
     */
    @Test
    public void checkDeleteTargets() {
        log.info("Test is started");

        log.info("delete all targets but one");
        authDashAndGo("advertisers/audience-edit/audience_id/1100");
        pagesInit.getAudiences().deleteAllTargets();
        pagesInit.getAudiences().saveAudience();

        log.info("check saved options");
        authDashAndGo("advertisers/audience-edit/audience_id/1100");
        Assert.assertEquals(pagesInit.getAudiences().getAmountOfTargets(), 1, "Fail - different amount");

        log.info("Test is finished");
    }
}
