package dashboard.mgid.advertisers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.dash.advertiser.logic.ConversionStats;
import core.helpers.sorted.SortedType;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.*;

import static java.util.Map.entry;
import static pages.dash.advertiser.logic.ConversionStats.ConversionStages;
import static pages.dash.advertiser.logic.ConversionStats.ConversionStages.*;
import static pages.dash.advertiser.variables.ConversionStatsVariables.*;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;

public class ConversionStatsTests extends TestBase {
    public ConversionStatsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }
    String[] filter1 = {"teaser", "ticker"};
    String[] filter2 = {"country", "region", "connection_type", "browser"/*, "device_os"*/};

    /**
     * ClickHouse. Статистика по Конверсиям товарной кампани. Interest, all, tickers, devices
     * <ul>
     * <li>Проверка таблицы</li>
     * <li>Проверка общих даных</li>
     * <li>Проверка даных графика</li>
     * </ul>
     * <p> NIO
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22255">TA-22255</a>
     */
    @Test
    public void checkInterestStageByWidgetUid() {
        log.info("Test is started");
        authDashAndGo("advertisers/sensors-stat/id/1000");
        log.info("Choose interest stage with tickers for all widgets");
        pagesInit.getConversionStats().chooseStageRadio(INTEREST);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy("ticker");

        log.info("Get table data statistics from interface");
        pagesInit.getConversionStats().getSumColumnsTableData();
        pagesInit.getConversionStats().getConversionChartData();
        pagesInit.getConversionStats().getConversionPriceChartData();

        log.info("Check table statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsClicksFromTable(mapAllWidgetsInterest), "FAIL - checkClicksTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionFromTable(mapAllWidgetsInterest), "FAIL - checkConversionsTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCostFromTable(mapAllWidgetsInterest), "FAIL - checkConversionCostTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCountFromTable(mapAllWidgetsInterest), "FAIL - checkConversionCountTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRevenueFromTable(mapAllWidgetsInterest), "FAIL - checkRevenueTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRoiFromTable(mapAllWidgetsInterest), "FAIL - checkRoiTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsSpentFromTable(mapAllWidgetsInterest), "FAIL - checkSpentTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsEpcFromTable(mapAllWidgetsInterest), "FAIL - checkEpcTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter1(mapAllWidgetsInterest), "FAIL - checkFilter1");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter2(mapAllWidgetsInterest), "FAIL - checkFilter2");
        softAssert.assertTrue(pagesInit.getConversionStats().checkProfitTable(mapAllWidgetsInterest), "FAIL - checkSpentTable");

        log.info("Check summary statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetSummaryData(listSumarryDataInterest), "FAIL - summary checking");

        log.info("Choose chart statistics for past 7 days");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(4);
        log.info("Check conversion count and conversion cost at chart statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionChart(chartDataInterest), "FAIL - chart checking");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * ClickHouse. Статистика по Конверсиям товарной кампани. Buy, all, tickers, devices
     * <ul>
     * <li>Проверка таблицы</li>
     * <li>Проверка общих даных</li>
     * <li>Проверка даных графика</li>
     * </ul>
     * <p> NIO
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22255">TA-22255</a>
     */
    @Test
    public void checkBuyStageByWidgetUid() {
        log.info("Test is started");
        authDashAndGo("advertisers/sensors-stat/id/1000");
        log.info("Choose interest stage with tickers for all widgets");
        pagesInit.getConversionStats().chooseStageRadio(BUY);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy("teaser");

        log.info("Get table data statistics from interface");
        pagesInit.getConversionStats().getSumColumnsTableData();
        pagesInit.getConversionStats().getConversionChartData();
        pagesInit.getConversionStats().getConversionPriceChartData();

        log.info("Check table statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsClicksFromTable(mapAllWidgetsBuy), "FAIL - checkClicksTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionFromTable(mapAllWidgetsBuy), "FAIL - checkConversionsTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCostFromTable(mapAllWidgetsBuy), "FAIL - checkConversionCostTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCountFromTable(mapAllWidgetsBuy), "FAIL - checkConversionCountTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRevenueFromTable(mapAllWidgetsBuy), "FAIL - checkRevenueTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRoiFromTable(mapAllWidgetsBuy), "FAIL - checkRoiTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsSpentFromTable(mapAllWidgetsBuy), "FAIL - checkSpentTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsEpcFromTable(mapAllWidgetsBuy), "FAIL - checkEpcTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter1(mapAllWidgetsBuy), "FAIL - checkFilter1");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter2(mapAllWidgetsBuy), "FAIL - checkFilter2");

        log.info("Check summary statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetSummaryData(listSumarryDatatBuy), "FAIL - summary checking");

        log.info("Choose chart statistics for this week");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(6);
        log.info("Check conversion count and conversion cost at chart statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionChart(chartDataBuy), "FAIL - chart checking");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * ClickHouse. Статистика по Конверсиям товарной кампани. Decision, all, tickers, devices
     * <ul>
     * <li>Проверка таблицы</li>
     * <li>Проверка общих даных</li>
     * <li>Проверка даных графика</li>
     * </ul>
     * <p> NIO
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22255">TA-22255</a>
     */
    @Test
    public void checkDecisionStageByWidgetUid() {
        log.info("Test is started");
        authDashAndGo("advertisers/sensors-stat/id/1000");
        log.info("Choose interest stage with tickers for all widgets");
        pagesInit.getConversionStats().chooseStageRadio(DECISION);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy("ticker");

        log.info("Get table data statistics from interface");
        pagesInit.getConversionStats().getSumColumnsTableData();
        pagesInit.getConversionStats().getConversionChartData();
        pagesInit.getConversionStats().getConversionPriceChartData();

        log.info("Check table statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsClicksFromTable(mapAllWidgetsDecision), "FAIL - checkClicksTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionFromTable(mapAllWidgetsDecision), "FAIL - checkConversionsTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCostFromTable(mapAllWidgetsDecision), "FAIL - checkConversionCostTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsConversionCountFromTable(mapAllWidgetsDecision), "FAIL - checkConversionCountTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRevenueFromTable(mapAllWidgetsDecision), "FAIL - checkRevenueTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsRoiFromTable(mapAllWidgetsDecision), "FAIL - checkRoiTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsSpentFromTable(mapAllWidgetsDecision), "FAIL - checkSpentTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkEqualsEpcFromTable(mapAllWidgetsDecision), "FAIL - checkEpcTable");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter1(mapAllWidgetsDecision), "FAIL - checkFilter1");
        softAssert.assertTrue(pagesInit.getConversionStats().checkFilter2(mapAllWidgetsDecision), "FAIL - checkFilter2");

        log.info("Check summary statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetSummaryData(listSumarryDatatDecision), "FAIL - summary checking");

        log.info("Choose chart statistics for past 30 days");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(7);
        log.info("Check conversion count and conversion cost at chart statistics");
        softAssert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionChart(chartDataDecision), "FAIL - chart checking");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * ClickHouse. Статистика по Конверсиям товарной кампани. Таблица с даними
     * <ul>
     * <li>Проверка interest, with с рандомными фильтрами 1, 2</li>
     * <li>Проверка buy, without с рандомными фильтрами 1, 2</li>
     * <li>Проверка decision, all с рандомными фильтрами 1, 2</li>
     * </ul>
     * <p> NIO
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22255">TA-22255</a>
     */
    @Test(dataProvider = "stageOptions")
    public void checkStagesWithRandomFilters(ConversionStats.ConversionStages stageConversion, String stateConversion) {
        log.info("Test is started");
        Integer campaignId = 1000;
        String filter1Val;
        String filter2Val;
        List<List<String>> dataDb;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1000, "Australia", "Canada", "Italy");
        authDashAndGo("advertisers/sensors-stat/id/" + 1000);

        log.info("Choose stage with random filters");
        pagesInit.getConversionStats().chooseStageRadio(stageConversion);
        pagesInit.getConversionStats().chooseTypeOfCondition(stateConversion);
        pagesInit.getConversionStats().selectFirstFilterBy(filter1Val = helpersInit.getBaseHelper().getRandomFromArray(filter1));
        pagesInit.getConversionStats().selectSecondFilterBy(filter2Val = helpersInit.getBaseHelper().getRandomFromArray(filter2));
        log.info("Choosen filter 1 - " + filter1Val);
        log.info("Choosen filter 2 - " + filter2Val);
        dataDb = operationCh.getConversionQueryBuilder().selectFromConversionStats(campaignId, stageConversion.getConversionVal(), stateConversion, filter1Val, filter2Val);
        log.info("Check table statistics");
        Assert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionTable(dataDb), "FAIL - table checking");
        Assert.assertTrue(pagesInit.getConversionStats().checkSecondColumnOfConversionStatsTable(dataDb, null), "FAIL - second column checking");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] stageOptions() {
        return new Object[][]{
                {INTEREST, "with"},
                {BUY, "without"},
                {DECISION, "all"}
        };
    }

    /**
     * ClickHouse. Статистика по Конверсиям товарной кампани. Таблица с даними
     * <ul>
     * <li>Проверка работы сортировки даных по колонкам</li>
     * </ul>
     * <p> NIO
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22255">TA-22255</a>
     */
    @Test
    public void checkingSortColumns() {
        ConversionStages conversionStageIndex = ConversionStages.getRandomConversionStage();
        String filter1Val = helpersInit.getBaseHelper().getRandomFromArray(filter1);
        String filter2Val = helpersInit.getBaseHelper().getRandomFromArray(filter2);
        log.info("Test is started");
        authDashAndGo("advertisers/sensors-stat/id/1000");
        log.info("Choose stage, conversion state, filter 1, filter 2");
        pagesInit.getConversionStats().chooseStageRadio(conversionStageIndex);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy(filter1Val);
        pagesInit.getConversionStats().selectSecondFilterBy(filter2Val);
        log.info(String.format("Choosen filter 1 - %s, filter 2 - %s, conversionState - %s, conversionStage - %s",
                filter1Val, filter2Val, "all", conversionStageIndex.getConversionVal()));

        log.info("Check sort action");
        Assert.assertTrue(helpersInit.getSortedHelper().checkSorted(new SortedType().randomPeriod()), "FAIL - sort failed");
        log.info("Test is finished");
    }

    /**
     * Дашборд. Интерфейс статистики по конверсиям. Детализировать разбивку до уровня тип устройства + OS
     * <p> NIO </>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27441">TA-27441</a>
     */
    @Test(dataProvider = "dataForExport")
    public void checkCsvConversionStatistics(String filter1, String filter2, ConversionStats.ConversionStages stage, Map<String, String> map) {
        log.info("Test is started");

        authDashAndGo("advertisers/sensors-stat/id/1000");
        log.info("Choose stage, conversion state, filter 1, filter 2");
        pagesInit.getConversionStats().chooseStageRadio(stage);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy(filter1);
        pagesInit.getConversionStats().selectSecondFilterBy(filter2);

        softAssert.assertTrue(serviceInit.getExportFileTableStructure().loadExportedFileDash(), "load");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile(pagesInit.getConversionStats().getFilterName1(), map.get("id1"), map.get("id2")), "Widget UID");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile(pagesInit.getConversionStats().getFilterName2(), map.get("filter1"), map.get("filter2")), "Country");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Clicks", map.get("clicks1"), map.get("clicks2")), "Clicks");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Spent, €", map.get("spent1"), map.get("spent2")), "Spent, €");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversions count", map.get("convCount1"), map.get("convCount2")), "Conversions count");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversion price, €", map.get("convCost1"), map.get("convCost2")), "Conversion price, €");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversion, %", map.get("conv1"), map.get("conv2")), "Conversion, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("ROI, %", map.get("roi1"), map.get("roi2")), "ROI, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Revenue, €", map.get("revenue1"), map.get("revenue2")), "Revenue, €");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("EPC, ¢", map.get("epc1"), map.get("epc2")), "EPC, ¢");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Profit, €", map.get("profit1"), map.get("profit2")), "Profit, €");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForExport() {
        return new Object[][]{
                {"ticker", "country", INTEREST.getConversionStage("interest"),
                        Map.ofEntries(
                                entry("id1", "0"), entry("id2", "2"),
                                entry("filter1", "Canada"), entry("filter2", "Australia"),
                                entry("clicks1", "15"), entry("clicks2", "12"),
                                entry("spent1", "0.36"), entry("spent2", "0.20"),
                                entry("convCount1", "3"), entry("convCount2", "2"),
                                entry("convCost1", "0.12"), entry("convCost2", "0.10"),
                                entry("conv1", "20.00"), entry("conv2", "16.67"),
                                entry("roi1", "12112.68"), entry("roi2", "20500.00"),
                                entry("revenue1", "43.00"), entry("revenue2", "41.00"),
                                entry("epc1", "286.67"), entry("epc2", "341.67"),
                                entry("profit1", "42.65"), entry("profit2", "40.80")
                        )},
                {"teaser", "device_os", BUY.getConversionStage("buy"),
                        Map.ofEntries(
                                entry("id1", "1001"), entry("id2", "1001"),
                                entry("filter1", "mobile Android 6.xx"), entry("filter2", "desktop Other Desktop OS"),
                                entry("clicks1", "31"), entry("clicks2", "21"),
                                entry("spent1", "0.68"), entry("spent2", "0.47"),
                                entry("convCount1", "1"), entry("convCount2", "2"),
                                entry("convCost1", "0.68"), entry("convCost2", "0.23"),
                                entry("conv1", "3.23"), entry("conv2", "9.52"),
                                entry("roi1", "12259.97"), entry("roi2", "12446.35"),
                                entry("revenue1", "83.00"), entry("revenue2", "58.00"),
                                entry("epc1", "267.74"), entry("epc2", "276.19"),
                                entry("profit1", "82.32"), entry("profit2", "57.53"),
                                entry("prevStage1", "25.00"), entry("prevStage2", "200.00")
                        )},
        };
    }

    /**
     * Checking statistics of conversions grouped by Traffic type
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51786">Ticket TA-51786</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkConversionStatisticsByTrafficType() {
        log.info("Test is started");
        Integer campaignId = 2005;
        String filter1Val = "teaser";
        String filter2Val = "adv_traffic_type";
        List<List<String>> dataDb;
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/sensors-stat/id/" + campaignId);
        pagesInit.getConversionStats().selectFirstFilterBy(filter1Val);
        pagesInit.getConversionStats().selectSecondFilterBy(filter2Val);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        dataDb = operationCh.getConversionQueryBuilder().selectFromConversionStats(campaignId, "all", "all", filter1Val, filter2Val);
        log.info("Check table statistics");
        Assert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionTable(dataDb),
                "FAIL - statistics by Traffic Type!");
        Assert.assertTrue(pagesInit.getConversionStats().checkSecondColumnOfConversionStatsTable(dataDb, null), "FAIL - second column checking");
        log.info("Test is finished");
    }

    /**
     * Вывод сабайди в интерфейс Conversions statistics
     * <p> NIO </>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51809">TA-51809</a>
     */
    @Test
    public void checkStagesWithUidSubuidFilter() {
        log.info("Test is started");
        Integer campaignId = 1150;
        String filter1Val;
        String filter2Val;
        List<List<String>> dataDb;
        authDashAndGo("testemail1000@ex.ua", "advertisers/sensors-stat/id/" + campaignId);
        log.info("Choose stage with random filters");
        pagesInit.getConversionStats().chooseStageRadio(INTEREST);
        pagesInit.getConversionStats().chooseTypeOfCondition("with");
        pagesInit.getConversionStats().selectFirstFilterBy(filter1Val = helpersInit.getBaseHelper().getRandomFromArray(new String[] {"teaser"}));
        pagesInit.getConversionStats().selectSecondFilterBy(filter2Val = helpersInit.getBaseHelper().getRandomFromArray(new String[] {"uid_subuid"}));
        log.info("Choosen filter 1 - " + filter1Val);
        log.info("Choosen filter 2 - " + filter2Val);
        dataDb = operationCh.getConversionQueryBuilder().selectFromConversionStats(campaignId, INTEREST.getConversionVal(), "with", filter1Val, filter2Val);
        log.info("Check table statistics");
        Assert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionTable(dataDb), "FAIL - table checking");
        Assert.assertTrue(pagesInit.getConversionStats().checkSecondColumnOfConversionStatsTable(dataDb, null), "FAIL - second column checking");
        log.info("Test is finished");
    }

    /**
     * Вывод сабайди в интерфейс Conversions statistics
     * <p> NIO </>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51809">TA-51809</a>
     */
    @Test(dataProvider = "csvStatisticsUidSubuid")
    public void checkCsvStatisticsUidSubuid(String filter1, String filter2, ConversionStats.ConversionStages stage, Map<String,String> map) {
        log.info("Test is started");
        int campaignId = 1150;
        authDashAndGo("testemail1000@ex.ua", "advertisers/sensors-stat/id/" + campaignId);

        log.info("Choose stage, conversion state, filter 1, filter 2");
        pagesInit.getConversionStats().chooseStageRadio(stage);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        pagesInit.getConversionStats().selectFirstFilterBy(filter1);
        pagesInit.getConversionStats().selectSecondFilterBy(filter2);

        softAssert.assertTrue(serviceInit.getExportFileTableStructure().loadExportedFileDash(), "load");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile(pagesInit.getConversionStats().getFilterName1(), map.get("id1"), map.get("id2")), "Widget UID");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile(pagesInit.getConversionStats().getFilterName2(), map.get("filter1"), map.get("filter2")), "Country");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Clicks", map.get("clicks1"), map.get("clicks2")), "Clicks");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Spent, €", map.get("spent1"), map.get("spent2")), "Spent, €");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversions count", map.get("convCount1"), map.get("convCount2")), "Conversions count");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversion price, €", map.get("convCost1"), map.get("convCost2")), "Conversion price, €");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversion, %", map.get("conv1"), map.get("conv2")), "Conversion, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("ROI, %", map.get("roi1"), map.get("roi2")), "ROI, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Conversions  from prev. stage, %", map.get("convPrev1"), map.get("convPrev2")), "ROI, %");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] csvStatisticsUidSubuid() {
        return new Object[][]{
                {"teaser", "uid_subuid", BUY.getConversionStage("buy"),
                        Map.ofEntries(
                                entry("id1", "1146"), entry("id2", "1146"),
                                entry("filter1", "1007s1"), entry("filter2", "1009s0"),
                                entry("clicks1", "8"), entry("clicks2", "8"),
                                entry("spent1", "0.15"), entry("spent2", "0.13"),
                                entry("convCount1", "0"), entry("convCount2", "0"),
                                entry("convCost1", "0.00"), entry("convCost2", "0.00"),
                                entry("conv1", "0.00"), entry("conv2", "0.00"),
                                entry("roi1", "0.00"), entry("roi2", "0.00"),
                                entry("convPrev1", "0.00"), entry("convPrev2", "0.00")
                        )}
        };
    }

    /**
     * Вывод сабайди в интерфейс Conversions statistics
     * <p> NIO </>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51809">TA-51809</a>
     */
    @Test(dataProvider = "campaignsForFilterSubuid")
    public void checkUidSubuidFilterThroughtCampaignType(int campaignId, boolean state) {
        log.info("Test is started");

        authDashAndGo("testemail1000@ex.ua", "advertisers/sensors-stat/id/" + campaignId);

        log.info("Check filter presence");
        Assert.assertEquals(pagesInit.getConversionStats().checkVisibilityOfFilterBySubid(), state, "FAIL - table checking");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignsForFilterSubuid() {
        return new Object[][]{
                {1151, true},
                {1152, false},
                {1153, true},
                {1154, true}
        };
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление среза на ценовой диапазон девайса в интерфейсы статистики")
    @Description("Проверка интерфейса статистики конверсий по ценовым диапазонам девайсов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51877\">Ticket TA-51877</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка интерфейса статистики конверсий по ценовым диапазонам девайсов")
    public void checkConversionStatisticsByPhonePrices() {
        log.info("Test is started");
        Integer campaignId = 2005;
        String filter1Val = "teaser";
        String filter2Val = "phones_price_range";
        List<List<String>>  dataDb;
        List<String> trafficTypeValues = Arrays.asList("Undefined", "From 150$ to 200$", "From 200$ to 250$", "From 300$ to 350$",
                "From 350$ to 400$", "From 400$ to 500$", "From 500$ to 600$", "From 600$ to 700$", "From 700$ and more");

        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers/sensors-stat/id/" + campaignId);

        pagesInit.getConversionStats().selectFirstFilterBy(filter1Val);
        pagesInit.getConversionStats().selectSecondFilterBy(filter2Val);
        pagesInit.getConversionStats().chooseTypeOfCondition("all");
        dataDb = operationCh.getConversionQueryBuilder().selectFromConversionStats(campaignId, "all", "all", filter1Val, filter2Val);
        log.info("Check table statistics");
        Assert.assertTrue(pagesInit.getConversionStats().checkStatisticsWidgetWithConversionTable(dataDb),
                "FAIL - statistics by 'Phone price ranges'!");
        Assert.assertTrue(pagesInit.getConversionStats().checkSecondColumnOfConversionStatsTable(null, trafficTypeValues),
                "FAIL - Second column in statistics by 'Phone price ranges'!");
        log.info("Test is finished");
    }
}