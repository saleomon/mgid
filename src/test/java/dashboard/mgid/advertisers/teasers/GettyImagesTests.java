package dashboard.mgid.advertisers.teasers;

import io.qameta.allure.*;
import libs.devtools.CloudinaryMock;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.*;
import static testData.project.ClientsEntities.*;

public class GettyImagesTests extends TestBase {
    String clientLogin = "testEmail1002@ex.ua";

    private final String teaser = "Teaser";
    private final String gettyImages = "Getty Images";
    private final String createInterface = "Create Interface Dash";
    private final String editInterface = "Edit Interface Dash";

    String notificationNoImages1 = "No results found for the keywords.\n" +
            " Try to use different keywords or check for spelling errors.";


    public void goToCreateTeaser(int campaignId) {
        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    public void goToCurrentTeaser(int campaignId, int teaserId) {
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Stories({@Story(TEASER_CREATE_DASH_INTERFACE), @Story(TEASER_EDIT_DASH_INTERFACE)})
    @Description("Getty images| Dashboard | Images Gallery\n" +
            "     <ul>\n" +
            "      <li>Create/Edit teaser with image using Getty Images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51868\">Ticket TA-51868</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Create/Edit teaser with image using Getty Images", dataProvider = "createTeaserGettyTypeOfBlockView")
    public void createTeaserProduct(String typeOfFile, String requestGetty, String responseGettyFile, CloudinaryMock.ResponseWithImages responseCloudinaryCreate, CloudinaryMock.ResponseWithImages responseCloudinaryEdit) {
        log.info("Test is started");
        log.info("create teaser");
        goToCreateTeaser(CAMPAIGN_MGID_CONTENT);

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestGetty, responseGettyFile);
        pagesInit.getTeaserClass().openStockGettyImageForm();
        pagesInit.getTeaserClass().selectTypeOfFormatTeaserViewBlock(typeOfFile);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        if(typeOfFile.equalsIgnoreCase("video")) serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*get-stock-video-download-url*", "responseStockVideoUrl2");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(responseCloudinaryCreate, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getTeaserClass().openUploadImageForm();

        pagesInit.getTeaserClass().useDiscount(true).setNeedToEditImage(false).setPriceOfClick("5").setNeedToEditCategory(true).createTeaser();

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(CAMPAIGN_MGID_CONTENT, pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(pagesInit.getTeaserClass().getTeaserId()), "FAIL -> checkTeaserSettingsInListInterface");

        operationMySql.getgHits1().updateBlockedOption(pagesInit.getTeaserClass().getTeaserId(), 1);

        goToCurrentTeaser(CAMPAIGN_MGID_CONTENT, pagesInit.getTeaserClass().getTeaserId());

        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestGetty, responseGettyFile);
        pagesInit.getCabTeasers().openStockGettyImageForm();
        pagesInit.getTeaserClass().selectTypeOfFormatTeaserViewBlock(typeOfFile);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        if(typeOfFile.equalsIgnoreCase("video")) serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*get-stock-video-download-url*", "responseStockVideoUrl1");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(responseCloudinaryEdit, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);
        serviceInit.getCloudinaryMock().countDownLatchAwait();
        pagesInit.getTeaserClass().openUploadImageForm();

        pagesInit.getTeaserClass().useTitle(true).isGenerateNewValues(true).setNeedToEditImage(false).editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(pagesInit.getTeaserClass().getTeaserId());

        log.info("check teaser settings in edit interface after edit him");
        goToCurrentTeaser(CAMPAIGN_MGID_CONTENT,  pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] createTeaserGettyTypeOfBlockView() {
        return new Object[][]{
                {"static", "*get-stock-images*", "responseGettyImages", HAPPINESS, GEERAF},
                {"video", "*get-stock-videos*", "responseGettyVideos", MANAGERS_RESET_TRUE, HAPPY_WOMAN}
        };
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("Getty images| Dashboard | Images Gallery\n" +
            "     <ul>\n" +
            "      <li>Check possibility of search images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51868\">Ticket TA-51868</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check possibility of search images")
    public void checkEditGettyImageSearchPhrase() {
        log.info("Test is started");
        String searchPhrase = "Yippee Ki-Yay";

        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("Choose category");
        pagesInit.getTeaserClass().openStockGettyImageForm();
        pagesInit.getTeaserClass().deleteGettyImageSearchPhrases();
        pagesInit.getTeaserClass().inputGettyImageSearchPhrase(searchPhrase);

        Assert.assertEquals(pagesInit.getTeaserClass().getCategoryNameAtGettyImagesSearchForm(), searchPhrase);
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("Getty images| Dashboard | Images Gallery\n" +
            "     <ul>\n" +
            "      <li>Check message of missing images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51868\">Ticket TA-51868</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check message of missing images")
    public void checkDisplayedMessageNoResults() {
        log.info("Test is started");
        String searchPhrase = "Салко";

        goToCreateTeaser(CAMPAIGN_MGID_SEARCH_FEED);

        log.info("Choose category");
        pagesInit.getTeaserClass().openStockGettyImageForm();
        pagesInit.getTeaserClass().deleteGettyImageSearchPhrases();

        log.info("edit created teaser");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*get-stock-images*", "responseGettyImages_noImages");
        pagesInit.getTeaserClass().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        Assert.assertEquals(pagesInit.getTeaserClass().getStockGalleryNotification(), notificationNoImages1);
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("Check filtering by and pagination\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52214\">Ticket TA-52214</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
//    @Test (description = "Check filtering by and pagination")
    @Flaky
    // toDo NIO  https://jira.mgid.com/browse/CP-2299 Zhynzher
    public void checkFilteringOfGettyAndPaginationVideo() {
        log.info("Test is started");

        String fileFormat = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"video", "static"});
        String requestName = fileFormat.equalsIgnoreCase("video") ? "*get-stock-videos*":"*get-stock-images*";
        String responseFile = fileFormat.equalsIgnoreCase("video") ? "responseGettyVideos":"responseGettyImages";
        String sortBy = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"most_popular", "newest"});

        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("Open form");
        pagesInit.getTeaserClass().openStockGettyImageForm();

        log.info("Choose type of format media content");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestName, responseFile);
        pagesInit.getTeaserClass().selectTypeOfFormatTeaserViewBlock(fileFormat);
        pagesInit.getTeaserClass().selectSortFilesForViewBlockBy(sortBy);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        log.info("Check parameters in requests");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl(sortBy), sortBy, "Fail - error sortBy");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl("page=1"), "page=1", "Fail - error page=1");
        softAssert.assertEquals(pagesInit.getTeaserClass().getActiveGettyImageMediaPage(), "1", "Fail - active pag 1");

        log.info("Action with pagination");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestName, responseFile);
        pagesInit.getTeaserClass().clickNextPageOfGalleryFiles();
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        log.info("Check parameter in requests and active page");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl("page=2"), "page=2", "Fail - error page=2");
        softAssert.assertEquals(pagesInit.getTeaserClass().getActiveGettyImageMediaPage(), "2", "Fail - active pag 2");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| inline editing\n" +
            "     <ul>\n" +
            "      <li>Check inline editing</li>\n" +
            "      <li>Check forwarding category</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51866\">Ticket TA-51866</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check inline editing forwarding with static media")
    public void checkInlineEditingWithGettyVideo() {
        log.info("Test is started");
        String searchPhrase = "Морквинка";
        int teaserId = 1374;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/1261/search/" + teaserId);

        log.info("Choose category");
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().openStockGettyImageForm();

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*get-stock-videos*", "responseGettyVideos");
        pagesInit.getTeaserClass().selectTypeOfFormatTeaserViewBlock("video");
        pagesInit.getTeaserClass().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*get-stock-video-download-url*", "responseStockVideoUrl1");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait();
        serviceInit.getCloudinaryMock().countDownLatchAwait();

        pagesInit.getTeaserClass().saveSettingsEdit();

        softAssert.assertTrue(pagesInit.getTeaserClass().isDisplayedTeaserVideoBlock(), "Fail - isDisplayedTeaserImage");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
