package dashboard.mgid.advertisers.teasers;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.dash.advertiser.logic.Teasers;
import testBase.TestBase;

import static testData.project.ClientsEntities.CAMPAIGN_MGID_PUSH;
import static testData.project.ClientsEntities.TEASER_PUSH_1_APPROVE;

public class TeaserPushTests extends TestBase {

    public void goToCreateTeaser(int campaignId){
        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    public void goToCurrentTeaser(int campaignId, int teaserId) {
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
    }

    /**
     * Создание /редактирование /удаление Push-тизера
     * RKO
     */
    @Test
    public void createTeaserPush() {
        log.info("Test is started");
        log.info("create teaser");
        goToCreateTeaser(CAMPAIGN_MGID_PUSH);
        pagesInit.getTeaserClass().useDescription(true).setPriceOfClick("5").createTeaser();

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(CAMPAIGN_MGID_PUSH, pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(pagesInit.getTeaserClass().getTeaserId()), "FAIL -> checkTeaserSettingsInListInterface");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ACTIVE), "FAIL -> teaser on moderation status");

        operationMySql.getgHits1().updateBlockedOption(pagesInit.getTeaserClass().getTeaserId(), 1);
        log.info("edit teaser");
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().useTitle(true).isGenerateNewValues(true).editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(pagesInit.getTeaserClass().getTeaserId());

        log.info("check teaser settings in edit interface after edit him");
        goToCurrentTeaser(CAMPAIGN_MGID_PUSH, pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface");

        log.info("reject and delete teaser");
        authCabAndGo("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_PUSH + "&id=" + pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().rejectAndDeleteTeaser(pagesInit.getTeaserClass().getTeaserId()), "FAIL -> rejectAndDeleteTeaser");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/TA-23991
     * Дашборд. Страница списка тизеров. Вывести пуш иконку, call to action, description и прокидывать их в экспорт.
     * Для РК типа PUSH нужно выводить push иконк
     * Проверяем что push campaign image отображается
     * RKO
     */
    @Test
    public void showDescriptionInEditFormForProductTeaser(){
        log.info("Test is started");
        goToCurrentTeaser(CAMPAIGN_MGID_PUSH, TEASER_PUSH_1_APPROVE);
        Assert.assertTrue(pagesInit.getTeaserClass().showPushCampaignImageInListInterface(), "FAIL -> description input is displayed for product teaser");
        log.info("Test is finished");
    }
}
