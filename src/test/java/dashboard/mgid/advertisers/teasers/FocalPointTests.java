package dashboard.mgid.advertisers.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.*;

public class FocalPointTests extends TestBase {
    String clientLogin = "testEmail1000@ex.ua";

    public void goToCurrentTeaser(int campaignId, int teaserId) {
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 0</li>\n" +
            "      <li>g_hits_1.blocked=1</li>\n" +
            "      <li>\"clVideoEffects\": [\"x_-5\", \"y_-5\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check displayed edit or default button")
    public void checkDisplayedEditOrDefaultButton() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1463;

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        softAssert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointEditButton(), "FAIL - isDisplayedFocalPointEditButton");
        softAssert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointDefaultButton(), "Fail - isDisplayedFocalPointDefaultButton");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 0</li>\n" +
            "      <li>g_hits_1.blocked=1</li>\n" +
            "      <li>\"clVideoEffects\": []</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check displayed manual button")
    public void checkDisplayedManualButton() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1464;

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        Assert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointManualButton(), "FAIL - isDisplayedFocalPointManualButton");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked=1</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check cancel button settings focal point")
    public void checkCancelButtonFocalPoint() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1465;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getTeaserClass().cancelManualFocalPoint();
        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        Assert.assertFalse(options.contains("clVideoEffects"), "Fail - clVideoEffects");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo NIO getty problem tech consultation 21.12.22 @Test (description = "Check focal point, reset crop to default, getty parameter")
    public void checkFocalPointFocusAndResetCropToDefault() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1466;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        pagesInit.getTeaserClass().enableCropFormat("3-2");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_DASH_SET_FOCAL, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().setFocalPoint(130, 114).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        softAssert.assertTrue(pagesInit.getTeaserClass().isActiveCropVideo("16-9"), "Fail - isActiveCrop");

        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertTrue(options.contains("clVideoEffects"), "Fail - clVideoEffects");
        softAssert.assertTrue(options.contains("x_35"), "Fail - x_35");
        softAssert.assertTrue(options.contains("y_256"), "Fail - y_256");
        softAssert.assertEquals(operationMySql.getgHits1().getGettyImagesFlag(teaserId), "4556",  "Fail - getGettyImagesFlag");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 1</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>\"clVideoEffects\": [\"x_-5\", \"y_-5\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check button to default settings focal point")
    public void checkButtonToDefaultSettingsFocalPoint() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1467;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_DASH_SET_FOCAL, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().clickFocalPointDefaultButton();
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        Assert.assertTrue(options.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>g_hits_1.options \"clEffects\": [\"e_sharpen:100\", \"c_fill\", \"f_jpg\", \"q_auto:good\", \"g_xy_center\", \"x_236\", \"y_299\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state focal point with static to focal point with video")
    public void checkChangeStaticFocalPointToVideoFocalPoint() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1473;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_DASH_SET_VIDEO, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().loadImage("managers_croped.mp4");
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_DASH_SET_FOCAL, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().setFocalPoint(130, 114).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertFalse(options.contains("e_sharpen"), "Fail - e_sharpen");
        softAssert.assertFalse(options.contains("f_jpg"), "Fail - f_jpg");
        softAssert.assertFalse(options.contains("g_xy_center"), "Fail - g_xy_center");
        softAssert.assertFalse(options.contains("x_236"), "Fail - x_236");
        softAssert.assertFalse(options.contains("y_299"), "Fail - y_299");
        softAssert.assertTrue(options.contains("clVideoEffects"), "Fail - clVideoEffects");
        softAssert.assertTrue(options.contains("x_35"), "Fail - x_35");
        softAssert.assertTrue(options.contains("y_256"), "Fail - y_256");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>g_hits_1.options \"clVideoEffects\": [\"x_-5\", \"y_-5\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state focal point with video to focal point with static")
    public void checkChangeVideoFocalPointToStaticFocalPoint() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1468;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        pagesInit.getTeaserClass().loadImage("mount.jpg");
        pagesInit.getTeaserClass().setFocalPoint(130, 114).setFocalPoint(true);
        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertTrue(options.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");
        softAssert.assertTrue(options.contains("clEffects"), "Fail - clEffects");
        softAssert.assertTrue(options.contains("e_sharpen"), "Fail - e_sharpen");
        softAssert.assertTrue(options.contains("f_jpg"), "Fail - f_jpg");
        softAssert.assertTrue(options.contains("g_xy_center"), "Fail - g_xy_center");
        softAssert.assertTrue(options.contains("x_375"), "Fail - x_375");
        softAssert.assertTrue(options.contains("y_463"), "Fail - y_463");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check message of editing focal point not completed")
    public void checkMessageOfEditingFocalPointNotCompletedVideo() {
        log.info("Test is started");
        int campaignId = 1413;
        int teaserId = 1469;

        goToCurrentTeaser(campaignId, teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().setFocalPoint(130, 114).setFocalPoint(false);
        pagesInit.getTeaserClass().saveSettingsEdit();
        pagesInit.getTeaserClass().getMessageFromPreviewBlock();

        Assert.assertEquals(pagesInit.getTeaserClass().getMessageFromPreviewBlock(),"PLEASE DISABLE FOCAL POINT FIRST", "Fail - getMessageFromPreviewBlock");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Set focal point edit teaser")
    public void checkEditionOfFocalPointEditTeaser() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5007;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().loadImage("sky.jpg");
        pagesInit.getTeaserClass().setFocalPoint(133, 144).setFocalPoint(true);
        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_681\""), "x_681");
        softAssert.assertTrue(options.contains("\"y_699\""), "y_699");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Check cancel focal point option for teaser edit")
    public void checkCancelOfFocalPointEdits() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5007;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().loadImage("mount.jpg");
        pagesInit.getTeaserClass().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getTeaserClass().cancelManualFocalPoint();
        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");
        softAssert.assertAll();
        log.info("Test finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Check edit focal point of rejected teaser")
    public void checkEditingFocalPointOfRejectedTeaser() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5008;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().setFocalPoint(130, 110).setFocalPoint(true);

        softAssert.assertTrue(pagesInit.getTeaserClass().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_246\""), "x_246");
        softAssert.assertTrue(options.contains("\"y_313\""), "y_313");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Check set default settings for focal point, teasers on moderation")
    public void checkSetToDefaultSettingsOfFocalPointModerationStatus() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5009;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().clickFocalPointDefaultButton();

        softAssert.assertTrue(pagesInit.getTeaserClass().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getTeaserClass().saveSettingsEdit();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Check displayed manual button for focal point settings")
    public void checkDisplayedManualButtonForApprovedAndBlockedTeaser() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5010;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        Assert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointManualButton(), "FAIL - isDisplayedFocalPointManualButton");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.blocked = 1</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52185\">Ticket TA-52185</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test (description = "Check displayed manual or default button for focal point settings")
    public void checkDisplayedManualOrDefaultButtonForApprovedAndBlockedTeaser() {
        log.info("Test is started");
        String clientLogin = "testEmail5000@ex.ua";
        int campaignId = 5003;
        int teaserId = 5011;

        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        softAssert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointManualButton(), "FAIL - isDisplayedFocalPointManualButton");
        softAssert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointDefaultButton(), "Fail - isDisplayedFocalPointDefaultButton");
        softAssert.assertAll();
        log.info("Test is finished");
    }

}
