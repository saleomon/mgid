package dashboard.mgid.advertisers.teasers;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.dash.advertiser.logic.Teasers;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods.YESTERDAY;
import static core.service.constantTemplates.ConstantsInit.*;
import static pages.dash.advertiser.variables.TeaserVariables.*;
import static testData.project.ClientsEntities.*;

public class TeaserTests extends TestBase {
    private static final String login = "testemail1000@ex.ua";
    private static final String limitError = "The %s campaign has reached its limit on the number of teasers. The available limit within the campaign is %s teasers. Clean up teasers that don't get impressions.";

    public void goToCreateTeaser(int campaignId) {
        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    public void goToCreateTeaser(String clientsLogin, int campaignId) {
        authDashAndGo(clientsLogin, "advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    public void goToCurrentTeaser(int campaignId, int teaserId) {
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
    }

    public void goToCurrentTeaser(String clientsLogin, int campaignId, int teaserId) {
        authDashAndGo(clientsLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
    }

    @Epic(TEASERS)
    @Feature(TITLE_AND_DESCRIPTION)
    @Stories({@Story(TEASER_EDIT_DASH_INTERFACE), @Story(TEASER_CREATE_DASH_INTERFACE)})
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check create/edit/delete teaser</>" +
            "   <li>Allow the use of special characters  ™, ®, ° in titles</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52409\">Ticket TA-52409</a></li>\n" +
            "</ul>")
    @Owner(RKO)
    @Test(description = "Check create/edit/delete teaser")
    public void createTeaserProduct() {
        log.info("Test is started");
        log.info("create teaser");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);
        pagesInit.getTeaserClass().setPriceOfClick("15")
                .useDiscount(true)
                .setTitle("™ New and Sign ® dash")
                .createTeaser();

        int teaserId = pagesInit.getTeaserClass().getTeaserId();
        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        softAssert.assertTrue(optionValues.contains("e_sharpen:100"), "FAIL - effects e_sharpen");
        softAssert.assertTrue(optionValues.contains("c_fill"), "FAIL - effects c_fill");
        softAssert.assertTrue(optionValues.contains("f_jpg"), "FAIL - effects f_jpg");
        softAssert.assertTrue(optionValues.contains("g_faces:auto"), "FAIL - effects g_faces");

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().editCategoryInline("116");
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(CAMPAIGN_MGID_PRODUCT, teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(teaserId), "FAIL -> checkTeaserSettingsInListInterface");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ACTIVE), "FAIL -> teaser on moderation status");

        operationMySql.getgHits1().updateBlockedOption(teaserId, 1);

        log.info("edit teaser");
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass()
                .useTitle(true)
                .isGenerateNewValues(true)
                .setTitle("Sign ® and ° Celsius")
                .editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(teaserId);

        log.info("check teaser settings in edit interface after edit him");
        goToCurrentTeaser(CAMPAIGN_MGID_PRODUCT, teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface");

        log.info("reject and delete teaser");
        authCabAndGo("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_PRODUCT + "&id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().rejectAndDeleteTeaser(teaserId), "FAIL -> rejectAndDeleteTeaser");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Создание, Редактирование вывод анимированных тизеров в дашборде")
    @Description("Создание, Редактирование тизера з gif\n" +
            "     <ul>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Создание, Редактирование тизера з gif")
    public void createTeaserWithAnimation() {
        log.info("Test is started");
        log.info("create teaser");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);
        pagesInit.getTeaserClass().useDiscount(true)
                .setPriceOfClick("5")
                .setImage("car_driving.gif")
                .createTeaser();

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(CAMPAIGN_MGID_PRODUCT, pagesInit.getTeaserClass().getTeaserId());
        operationMySql.getgHits1().updateBlockedOption(pagesInit.getTeaserClass().getTeaserId(), 1);

        log.info("edit teaser");
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface 1");

        pagesInit.getTeaserClass()
                .useTitle(true)
                .isGenerateNewValues(true)
                .setImage("space_journey.gif")
                .setNeedToEditImage(true)
                .editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(pagesInit.getTeaserClass().getTeaserId());

        log.info("check teaser settings in edit interface after edit him");
        goToCurrentTeaser(CAMPAIGN_MGID_PRODUCT, pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface 2");

        softAssert.assertAll();

        log.info("Test is finished");
    }


    /**
     * Story to PI-1986: дать пользователям дэша возможность добавлять тизеры в CPM-кампанию
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51757">TA-51757</a>
     */
    @Test
    public void createTeaserCpm() {
        log.info("Test is started");
        log.info("create teaser");
        goToCreateTeaser(CAMPAIGN_MGID_CPM);
        pagesInit.getTeaserClass().useDescription(false).setPriceOfClick("5").createTeaser();

        goToCurrentTeaser(CAMPAIGN_MGID_CPM, pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ON_MODERATION), "FAIL -> teaser should be on moderation status");

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getCabTeasers().editCategoryInline("132");
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("check teaser settings in list interface");
        goToCurrentTeaser(CAMPAIGN_MGID_CPM, pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(pagesInit.getTeaserClass().getTeaserId()), "FAIL -> checkTeaserSettingsInListInterface");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ACTIVE), "FAIL -> teaser on moderation status");

        operationMySql.getgHits1().updateBlockedOption(pagesInit.getTeaserClass().getTeaserId(), 1);
        log.info("edit teaser");
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().useTitle(true).isGenerateNewValues(true).editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(pagesInit.getTeaserClass().getTeaserId());

        log.info("check teaser settings in edit interface after edit him");
        goToCurrentTeaser(CAMPAIGN_MGID_CPM, pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getTeaserClass().openTeasersEditInterface(pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInEditInterface(), "FAIL -> checkTeaserSettingsInEditInterface");

        log.info("reject and delete teaser");
        authCabAndGo("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_CPM + "&id=" + pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().rejectAndDeleteTeaser(pagesInit.getTeaserClass().getTeaserId()), "FAIL -> rejectAndDeleteTeaser");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23991">https://youtrack.mgid.com/issue/TA-23991</a>
     * Дашборд. Страница списка тизеров. Вывести пуш иконку, call to action, description и прокидывать их в экспорт.
     * Для всех типов РК, кроме PUSH, не давать редактировать описание (скрывать блок description в попапе редактирования тизера - <a href="http://prntscr.com/sxzupv">http://prntscr.com/sxzupv</a>)
     * Проверяем что блок description не отображается при редактировании
     * RKO
     */
    @Test
    public void showDescriptionInEditFormForProductTeaser() {
        log.info("Test is started");
        goToCurrentTeaser(CAMPAIGN_MGID_PRODUCT, 10);
        pagesInit.getTeaserClass().openTeasersEditInterface(10);
        Assert.assertTrue(pagesInit.getTeaserClass().showDescriptionInEditFormForProductTeaser(), "FAIL -> description input is displayed for product teaser");
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23991">https://youtrack.mgid.com/issue/TA-23991</a>
     * Дашборд. Страница списка тизеров. Вывести пуш иконку, call to action, description и прокидывать их в экспорт.
     * Если у тизера есть call to action текст g_hits_1.call_to_action, то выводить его с новой строки под описанием
     * Проверяем что call to action отображается в списке тизеров
     * RKO
     */
    @Test
    public void showCallToAction() {
        log.info("Test is started");
        int brandCampaignId = 13;
        int brandTeaserId = 22;
        String callToAction = "call to action m1 go !!!";
        String description = "Description m1";

        goToCurrentTeaser(brandCampaignId, brandTeaserId);
        pagesInit.getTeaserClass()
                .setDescription(description)
                .setCallToAction(callToAction);
        Assert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(brandTeaserId), "FAIL -> call to action fill is incorrect");
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.dt00.net/issue/MT-196">https://youtrack.dt00.net/issue/MT-196</a>
     * Добавить в экспорт вывод типа тизера ad_types
     * <p>
     * <a href="https://youtrack.mgid.com/issue/TA-23991">https://youtrack.mgid.com/issue/TA-23991</a>
     * Дашборд. Страница списка тизеров. Вывести пуш иконку, call to action, description и прокидывать их в экспорт.
     * <p>
     * проверка экспорта csv, сверка заголовков и кастомных полей
     * <p>
     * RKO
     */
    @Test
    public void checkDataStatByListTeasers() {
        log.info("Test is started");
        try {
            String[] headers = {"ID", "Status", "Title", "Description", "Call to Action", "Category", "Ranking", "CPC", "vCTR, %", "Imps today", "Imps yesterday", "Imps total",
                    "Clicks today", "Clicks yesterday", "Clicks total", "Spent today", "Spent yesterday", "Spent total"};
            int brandCampaignId = 13;
            int brandTeaserId = 22;
            goToCurrentTeaser(brandCampaignId, brandTeaserId);

            softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                    .setCustomRowNumber(1)
                    .loadExportedFileDash(), "FAIL -> exported file has some error");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers), "FAIL -> don't correct all column headers");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Status", pagesInit.getTeaserClass().getStatusTeaser()), "FAIL -> don't equal column 'Status'");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Title", pagesInit.getTeaserClass().getTitleTeaser()), "FAIL -> don't equal column 'Title'");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPC", pagesInit.getTeaserClass().getCpcTeaser()), "FAIL -> don't equal column 'CPC'");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Ranking", pagesInit.getTeaserClass().getTypeTeaser()), "FAIL -> don't equal column 'Ranking'");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Call to Action", pagesInit.getTeaserClass().getCallToActionTeaser()), "FAIL -> don't equal column 'Call to Action'");
            softAssert.assertAll();
        } finally {
            serviceInit.getExportFileTableStructure().closeAllFile();
        }
        log.info("Test is finished");
    }

    /**
     * Создание тизера с незаполненными обязательными полями
     * <p>NIO</p>
     */
    @Test
    public void checkFieldsValidation() {
        log.info("Test is started");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);
        pagesInit.getTeaserClass().setDomain("").setPriceOfClick("5").createTeaser();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkErrorSelectionField("url"), "FAIL -> url");
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getTeaserClass().resetAllVariables();

        log.info("Check title validation");
        pagesInit.getTeaserClass().setTitle("").setPriceOfClick("5").createTeaser();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkErrorSelectionField("title"), "FAIL -> title");
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getTeaserClass().resetAllVariables();

        log.info("Check image link validation");
        pagesInit.getTeaserClass().setImage("").setPriceOfClick("5").createTeaser();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkErrorSelectionField("image_link"), "FAIL -> image link");
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getTeaserClass().resetAllVariables();

        log.info("Check price of click validation");
        pagesInit.getTeaserClass().setPriceOfClick("").createTeaser();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkErrorSelectionField("price_of_click-visible"), "FAIL -> price_of_click-visible");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * покрыта работа блока 'TEASER REACH' - изменение цены в зависимости от уровня 'teaser reach'
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.dt00.net/issue/TA-22564">TA-22564</a>
     */
    @Test
    public void priceEngineCheckChangeLevelPrice() {
        log.info("Test is started");
        goToCreateTeaser(21);
        pagesInit.getTeaserClass().getPriceForTeaserReachBlock();

        log.info("check 'high' icon in 'TEASER REACH' -> check change price");
        pagesInit.getTeaserClass().highCpcIconClick();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachBlockAfterChangeLevel("high"), "'high' icon -> isn't work");

        log.info("check 'Very high' icon in 'TEASER REACH' -> check change price");
        pagesInit.getTeaserClass().veryHighCpcIconClick();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachBlockAfterChangeLevel("very_high"), "'Very high' icon -> isn't work");

        log.info("check 'low' icon in 'TEASER REACH' -> check change price");
        pagesInit.getTeaserClass().lowCpcIconClick();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachBlockAfterChangeLevel("low"), "'low' icon -> isn't work");

        log.info("check 'Very low' icon in 'TEASER REACH' -> check change price");
        pagesInit.getTeaserClass().veryLowCpcIconClick();
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachBlockAfterChangeLevel("very_low"), "'Very low' icon -> isn't work");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка того, что тизеры заблокированные по расписанию нельзя переблокироватьс помощью массовой блокировки
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24031">Ticket TA-24031</a>
     * Author AIA
     */
    @Test
    public void checkReBlockingForbiddanceForTeaserBlockedByScheduleDash() {
        log.info("Test is started");
        authDashAndGo("advertisers/teasers-goods/campaign_id/2001/search/2003");
        pagesInit.getTeaserClass().reblockTeaserInDashByMassAction();
        Assert.assertEquals(helpersInit.getMessageHelper().getMessageDash(),
                "IT IS FORBIDDEN TO RE-BLOCK TEASERS BLOCKED BY A SCHEDULE");
        log.info("Test is finished");
    }

    /**
     * Проверка статуса тизеров(с определенными категориями) при изменении гео кампании:
     * <ul>
     *     <li>Изменение гео с гео Spain, Canada на 'Ukraine', 'Canada'</li>
     *     <li>Проверка статуса тизеров</li>
     *     <li>Изменение гео с гео 'Ukraine', 'Canada' на Spain, Canada</li>
     *     <li>Проверка статуса тизеров</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24494">TA-24494</a>
     */
    @Test
    public void checkModerationStatusTeaserWithGeoSpainCampaign() {
        log.info("Test is started");

        log.info("Check teaser status. Geo Spain, Canada -> 'Latvia', 'Canada'");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1040, "Canada", "Spain");
        authDashAndGo("advertisers/edit/campaign_id/1040");
        pagesInit.getCampaigns().selectLocationTarget("include", "Latvia", "Canada");
        pagesInit.getCampaigns().saveCampaign();

        authDashAndGo("advertisers/teasers-goods/campaign_id/1040/search/1026");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ACTIVE), "Geo Spain, Canada -> 'Latvia', 'Canada'");

        log.info("Check teaser status. Geo 'Latvia', 'Canada' -> 'Spain', 'Canada'");
        authDashAndGo("advertisers/edit/campaign_id/1040");
        pagesInit.getCampaigns().selectLocationTarget("include", "Canada", "Spain");
        pagesInit.getCampaigns().saveCampaign();
        authDashAndGo("advertisers/teasers-goods/campaign_id/1040/search/1026");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ON_MODERATION), "Geo 'Latvia', 'Canada' -> 'Spain', 'Canada' 1026");
        authDashAndGo("advertisers/teasers-goods/campaign_id/1040/search/1027");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ON_MODERATION), "Geo 'Latvia', 'Canada' -> 'Spain', 'Canada' 1027");
        authDashAndGo("advertisers/teasers-goods/campaign_id/1040/search/1028");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ON_MODERATION), "Geo 'Latvia', 'Canada' -> 'Spain', 'Canada' 1028");
        authDashAndGo("advertisers/teasers-goods/campaign_id/1040/search/1029");
        softAssert.assertTrue(pagesInit.getTeaserClass().teaserStatus(Teasers.Statuses.ACTIVE), "Geo 'Latvia', 'Canada' -> 'Spain', 'Canada' 1029");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка валидации на размер загружаемого изображения(интерфейс создания/редактирования):
     * <ul>
     *     <li>Проверка валидации по высоте</li>
     *     <li>Проверка валидации по ширине</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24494">TA-24494</a>
     */
    @Test
    public void checkImageSizeValidation() {
        log.info("Test is started");

        authDashAndGo("testEmail1002@ex.ua", "advertisers/add-teaser-goods/campaign_id/1041");
        pagesInit.getTeaserClass().loadImage("492_327_validation1.jpeg");
        log.info("Check error message about HEIGHT");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("MINIMUM EXPECTED HEIGHT FOR IMAGE '492_327_VALIDATION1.JPEG' SHOULD BE '328' BUT '327' DETECTED"), "geeraf_740x380");

        authDashAndGo("advertisers/add-teaser-goods/campaign_id/1041");
        pagesInit.getTeaserClass().loadImage("491_328_validation2.jpeg");
        log.info("Check error message about WIDTH");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("MINIMUM EXPECTED WIDTH FOR IMAGE '491_328_VALIDATION2.JPEG' SHOULD BE '492' BUT '491' DETECTED"), "aurora_500x400");

        authDashAndGo("advertisers/add-teaser-goods/campaign_id/1041");
        pagesInit.getTeaserClass().loadImage("leo.gif");
        log.info("Check error message about size");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("MAXIMUM ALLOWED SIZE FOR FILE - 5MB. CHOSEN FILE - 7.33MB"), "leo.gif");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Story to PI-1986: дать пользователям дэша возможность добавлять тизеры в CPM-кампанию
     * <p>Should be:<p/>
     * <ul>
     *     <li>THE PRICE OF 1000 VIEWABLE IMPRESSIONS visible</li>
     *     <li>Apply CPM for all regions visible</li>
     *     <li>TEASER REACH hidden</li>
     *     <li>price engine hidden</li>
     * </ul>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51757">TA-51757</a>
     * <p>NIO</p>
     */
    @Test
    public void checkDisplayedElementsFormForCpmTeaser() {
        log.info("Test is started");

        goToCreateTeaser(CAMPAIGN_MGID_CPM);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().addIgnoredElement(By.className("user")).takeScreenshot(WebDriverRunner.getWebDriver(), $("[id=ghits_new_add]")),
                serviceInit.getScreenshotService().getExpectedScreenshot("checkDisplayedElementsFormForCpmTeaser.png")
        ));

        log.info("Test is finished");
    }

    /**
     * Story to PI-1986: дать пользователям дэша возможность добавлять тизеры в CPM-кампанию
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51757">TA-51757</a>
     * <p>NIO</p>
     */
    @Test
    public void checkDisplayedTeasersForCpmTeaser() {
        log.info("Test is started");

        goToCurrentTeaser(CAMPAIGN_MGID_CPM, 1120);
        Assert.assertTrue(pagesInit.getTeaserClass().checkVisibilityTeaser());

        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Создание, Редактирование вывод анимированных тизеров в дашборде")
    @Description("Check preview teaser with gif\n" +
            "     <ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51863\">Ticket TA-51863</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test(description = "Check preview teaser with gif")
    public void checkPreviewTeaserAnimation() {
        log.info("Test is started");
        authDashAndGo("testEmail1000@ex.ua", "advertisers/teasers-goods/campaign_id/1222/search/1181");
        open(pagesInit.getTeaserClass().getLinkForPreviewTeaserInWidget());

        Assert.assertTrue(pagesInit.getTeaserClass().isDisplayedImageFrame(),
                "FAIL -> Displaying teaser!");
        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Создание, Редактирование вывод анимированных тизеров в дашборде")
    @Description("Check preview teaser with video\n" +
            "     <ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52144\">Ticket TA-52144</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test(description = "Check preview teaser with video")
    public void checkPreviewTeaserVideo() {
        log.info("Test is started");
        String expectedVideoLink = "http://local-dashboard.mgid.com/imgh/video/upload/ar_16:9,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4";

        authDashAndGo("testEmail1000@ex.ua", "advertisers/teasers-goods/campaign_id/1222/search/1298");
        open(pagesInit.getTeaserClass().getLinkForPreviewTeaserInWidget());

        Assert.assertEquals(pagesInit.getTeaserClass().getVideoLink(), expectedVideoLink,
                "FAIL -> Displaying teaser!");
        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Создание и редактирование тизера в кабе с middot в середине тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактирование тизера в кабе с middot в середине тайтла и дескрипшена")
    public void checkCreateEditTeaserWithMiddotInTheMiddleOfTitleAndDescriptionInDashboard() {
        log.info("Test is started");
        int campaignId = 2014;
        log.info("Create teaser");
        goToCreateTeaser(CLIENTS_PAYOUTS_LOGIN, campaignId);
        pagesInit.getTeaserClass().useDiscount(true)
                .setTitle("Title with one middot·in the middle!")
                .setDescription("Description with one middot·the middle!")
                .setPriceOfClick("5")
                .createTeaser();
        int teaserId = pagesInit.getTeaserClass().getTeaserId();
        log.info("Approve teaser");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().setLandingAndAdTypeAndApproveTeaser();

        log.info("Check teaser settings in list interface");
        goToCurrentTeaser(CLIENTS_PAYOUTS_LOGIN, campaignId, teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTitleInTeasersListInterface(),
                "FAIL -> Title with middot has failed check in teasers list after creating!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkDescriptionInTeasersListInterface(),
                "FAIL -> Description with middot has failed check in teasers list after creating!");

        operationMySql.getgHits1().updateBlockedOption(teaserId, 1);

        log.info("Edit teaser");
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass()
                .useTitle(true)
                .setTitle("Title·with·middots·the·middle!")
                .setDescription("Description·with·middots·the·middle!")
                .editTeaser();
        operationMySql.getgHits1().updateTeaserSendApprove(teaserId);

        log.info("Check teaser settings in edit interface after editing");
        goToCurrentTeaser(CLIENTS_PAYOUTS_LOGIN, campaignId, teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().checkTitleInTeasersEditInterface(),
                "FAIL -> Title with middot has failed check in edit after editing!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkDescriptionInTeasersEditInterface(),
                "FAIL -> Description with middot has failed check in edit interface after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Проверка ошибки при создании тизера в дашборде с middot в конце тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка ошибки при создании тизера в дашборде с middot в конце тайтла и дескрипшена")
    public void checkCreateTeaserWithMiddotAtTheEndOfTitleAndDescriptionInDashboard() {
        log.info("Test is started");
        int campaignId = 2014;
        log.info("Create teaser");
        goToCreateTeaser(CLIENTS_PAYOUTS_LOGIN, campaignId);
        pagesInit.getTeaserClass().useDiscount(true)
                .setTitle("Title with middot at the end·")
                .setDescription("Description with middot at the end·")
                .setPriceOfClick("5")
                .createTeaser();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Title with middot has failed check in teasers list after creating!");

        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Проверка ошибки при редактировании тизера в дашборде с middot в конце тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка ошибки при редактировании тизера в дашборде с middot в конце тайтла и дескрипшена")
    public void     checkEditTeaserWithMiddotAtTheEndOfTitleAndDescriptionInDashboard() {
        log.info("Test is started");
        int campaignId = 2014;
        int teaserId = 2008;
        log.info("Create teaser");
        goToCurrentTeaser(CLIENTS_PAYOUTS_LOGIN, campaignId, teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().useDiscount(true)
                .useTitle(true)
                .setTitle("Title with middot at the end·")
                .setDescription("Description with middot at the end·")
                .editTeaser();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Title with middot has failed check in teasers list after creating!");

        log.info("Test is finished");
    }

    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to WT-933 | Метрики ежедневной статы по тизеру")
    @Description("Checking reach metrics in teasers daily statistic\n" +
            "<ul>\n" +
            "   <li>1. Check column names</>" +
            "   <li>1. Check columns values</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51963\">Ticket TA-51963</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for teasers list")
    public void checkReachMetricsForTeasersDailyStatistics() {
        log.info("Test is started");
        int teaserId = 2005;
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/teaser-goods-stat/id/" + teaserId);
        pagesInit.getCampaigns().selectPeriodInCalendar(YESTERDAY);
        helpersInit.getBaseHelper().refreshCurrentPage();
        String[] headers = {"DATE", "IMPS", "VIEWABLE IMPRESSIONS", "VIEWABILITY, %", "CLICKS",
                "SPENT, $", "CPC, ¢", "CTR, %", "CPM, $", "REACH", "CPR, $", "FREQUENCY"};
        softAssert.assertTrue(pagesInit.getTeaserClass().checkShowAllColumnNames(headers),
                "FAIL -> Column header names are incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(2, "3000"),
                "FAIL -> Impressions (or Ad Requests) value is incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(3, "2000"),
                "FAIL -> Viewable impressions value is incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(4, "0.67"),
                "FAIL -> Viewability, % value is incorrect and should be calculated by formula 'Impression / Requests'!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(5, "20"),
                "FAIL -> Clicks value is incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(6, "3.00"),
                "FAIL -> Spent value is incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(7, "15.0"),
                "FAIL -> CPC should be calculated by formula CPC = Spend)/Clicks!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(8, "0.67"),
                "FAIL -> CTR should be calculated by formula CTR = 100%*Clicks/Impressions where Impressions is ad_requests!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(9, "1.0"),
                "FAIL -> CPM should be calculated by formula CPM = (1000*Spend)/Impressions where Impressions is ad_requests!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(10, "385"),
                "FAIL -> Reach value is incorrect!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(11, "7.79"),
                "FAIL -> CPR should be calculated by formula CPR = (Spend/Reach)*1000!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStatValues(12, "7.79"),
                "FAIL -> Frequency should be calculated by formula 'Impressions/Reach'!");
        softAssert.assertFalse(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStat("vCTR"),
                "FAIL -> Table still contains 'vCTR' column for Reach type stat!");
        softAssert.assertFalse(pagesInit.getTeaserClass().checkReachMetricsInTeasersDailyStat("vCPM"),
                "FAIL -> Table still contains 'vCPM' column for Reach type stat!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Manual GIF Upload")
    @Story("Manual GIF Upload. Internal")
    @Description("Check displaying images at teasers preview\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52032\">Ticket TA-52032</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check displaying images at teasers preview")
    public void checkDisplayingGifsAtTeaserPreview() {
        log.info("Test is started");
        int amountOfGifs = 3;

        goToCurrentTeaser(1000, 1198);
        open(pagesInit.getTeaserClass().getLinkForPreview());

        Assert.assertEquals(pagesInit.getTeaserClass().checkAmountOfImageBlocks(), amountOfGifs);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] autoModerationStateDueChangingCategory() {
        return new Object[][]{
                {1250, "145", "0"},
                {1251, "103", "0"}
        };
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Закрити можливість додавання gif у Push тизерах")
    @Description("Check possibility to create and edit push teaser with gif\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52114\">Ticket TA-52114</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check possibility to create and edit push teaser with gif")
    public void createPushTeaserWithAnimation() {
        log.info("Test is started");

        int campaignId = 1266;
        int teaserId = 1266;

        log.info("Check load gif create interface");
        goToCreateTeaser(login, campaignId);
        pagesInit.getTeaserClass().loadImage("car_driving.gif");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("ANIMATED IMAGES FOR PUSH CAMPAIGNS ARE NOT ALLOWED"), "FAIL - create teaser");

        log.info("Check load gif edit interface");
        operationMySql.getgHits1().updateBlockedOption(teaserId, 1);
        goToCurrentTeaser(campaignId, teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        pagesInit.getTeaserClass().loadImage("car_driving.gif");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("ANIMATED IMAGES FOR PUSH CAMPAIGNS ARE NOT ALLOWED"), "FAIL - edit teaser");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("FEATURE | Creative Safety Ratings. MVP")
    @Story("Story to BT-4039 Вывод рейтинга в интерфейс тизеров")
    @Description("Check 'Creative Safety Ratings' icons and titles in teaser list\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52127\">Ticket TA-52127</a></li>\n" +
            "   <li><p>AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check 'Creative Safety Ratings' icons and titles in teaser list")
    public void checkCsrIconsInList() {
        log.info("Test is started");
        int campaignId = 2030;
        int teaserMediumId = 2028;
        int teaserHighId = 2029;
        authDashAndGo("csr-client-2@mgid.com", "advertisers/teasers-goods/campaign_id/" + campaignId);
        softAssert.assertTrue(pagesInit.getTeaserClass().checkCsrMediumIconIsDisplayed(),
                "FAIL -> Creative safety rating Medium icon is not visible!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkCsrHighIconIsDisplayed(),
                "FAIL -> Creative safety rating High icon is not visible!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrMediumTitle(),
                String.format(teaserCsrMediumTitle, teaserMediumId),
                "FAIL -> Creative safety rating Medium title is incorrect!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrHighTitle(),
                String.format(teaserCsrHighTitle, teaserHighId),
                "FAIL -> Creative safety rating High title is incorrect!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Creative Safety Ratings")
    @Feature("FEATURE | Creative Safety Ratings. MVP")
    @Story("Story to 4039 Ричметр при создании тизера с учетом рейтинга, " +
            "CSR. Interface changes and more")
    @Description("Check 'CSR' attributes in create teaser form\n" +
            "<ul>\n" +
            "   <li>Check CSR text and title</li>\n" +
            "   <li>Check CSR reach meter</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52128\">Ticket TA-52128</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check 'CSR' attributes in create teaser form", dataProvider = "campaignsWithCsr")
    public void checkCsrAttributesInCreateTeaserForm(int campaignId, String campaignType) {
        log.info("Test is started");
        String[] countries = helpersInit.getBaseHelper().getRandomValuesFromList(operationMySql.getMaxmindGeoipCountries().getListCountries(), 1);
        log.info("Selected country is - " + countries[0]);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, countries[0]);
        authDashAndGo("csr-client-2@mgid.com", "advertisers/add-teaser-goods/campaign_id/" + campaignId);
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrTitle(), "INCREASE YOUR POTENTIAL AUDIENCE REACH!",
                "FAIL -> Incorrect CSR title!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrText(),
                "Please consider most of our publishers prefer High Safety creatives to be displayed; " +
                        "hence Medium Safety creatives get less potential reach.\n\nRead more",
                "FAIL -> Incorrect CSR text!");
        softAssert.assertTrue(pagesInit.getTeaserClass().checkCsrReachMeterIsDisplayed(),
                "FAIL -> CSR reach meter is not present!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrLinksValue("High Safety"), teaserCsrHighLink,
                "FAIL -> High Safety link is incorrect for " + campaignType + " campaign!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrLinksValue("Medium Safety"), teaserCsrMediumLink,
                "FAIL -> Medium Safety link is incorrect for " + campaignType + " campaign!");
        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrLinksValue("Read more"), teaserCsrReadMoreLink,
                "FAIL -> Read more link is incorrect for " + campaignType + " campaign!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignsWithCsr() {
        return new Object[][]{
                {2030, "product"},
                {2031, "push"},
                {2032, "content"},
                {2033, "search_feed"}
        };
    }

    @Feature("FEATURE | Creative Safety Ratings. MVP")
    @Story("Story to 4039 Ричметр при создании тизера с учетом рейтинга")
    @Description("Check creating teaser with 'CSR'\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52128\">Ticket TA-52128</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check creating teaser with 'CSR'")
    public void checkCsrCreateTeaser() {
        log.info("Test is started");

        goToCreateTeaser("csr-client-2@mgid.com", 2031);
        pagesInit.getTeaserClass()
                .setPriceOfClick("5")
                .setTitle("Title with CSR!")
                .useDescription(true)
                .setDescription("Description with CSR!")
                .createTeaser();
        Assert.assertTrue(pagesInit.getTeaserClass().checkTeaserSettingsInListInterface(pagesInit.getTeaserClass().getTeaserId()),
                "FAIL -> Check teaser settings in list!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(CSR)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("Check 'CSR' Reach Meter value in create teaser form\n" +
            "<ul>\n" +
            "   <li>Check CSR Reach Meter value</li>\n" +
            "   <li>Check CSR Reach Meter note text</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52165\">Ticket TA-52165</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check 'CSR' Reach Meter value and note in create teaser form", dataProvider = "checkCsrReachMeterValue")
    public void checkCsrReachMeter(int campaignId, String reachMeterValue, boolean noteIsDisplayed) {
        log.info("Test is started");
        authDashAndGo("csr-client-2@mgid.com", "advertisers/add-teaser-goods/campaign_id/" + campaignId);

        softAssert.assertEquals(pagesInit.getTeaserClass().getCsrReachMeterValue(), reachMeterValue,
                "FAIL -> Reach Meter value!");
        softAssert.assertEquals(pagesInit.getTeaserClass().checkCsrReachMeterNoteIsDisplayed("Please note, that the value of the increase is based on our mathematical model, the actual data may differ from the presented."),
                noteIsDisplayed,
                "FAIL -> Reach Meter note text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkCsrReachMeterValue() {
        return new Object[][]{
                {2036, "5", true},
                {2037, "14", false},
        };
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 teaser which one dropped and second is blocked</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo NIO https://jira.mgid.com/browse/CP-3033 @Test(description = "Check create teaser over limit")
    public void checkCreateTeaserOverLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1425;
        int limit = 2;

        goToCreateTeaser(login, campaignId);
        softAssert.assertEquals(pagesInit.getTeaserClass()
                .setPriceOfClick("15")
                .useDiscount(true)
                .createTeaser(), 0, "Fail - teaser created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(String.format(limitError, campaignId, limit)),
                "FAIL -> Check teaser settings in list!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 2 teaser which one dropped</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check create teaser with max range of limit")
    public void checkCreateTeaserWithMaxLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1432;

        goToCreateTeaser(login, campaignId);
        softAssert.assertNotEquals(pagesInit.getTeaserClass()
                .setPriceOfClick("15")
                .useDiscount(true)
                .createTeaser(), 0, "Fail - teaser created");

        goToCurrentTeaser(campaignId, pagesInit.getTeaserClass().getTeaserId());
        softAssert.assertTrue(pagesInit.getTeaserClass().isTeaserDisplayed(),
                "FAIL -> Check teaser settings in list!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 regular teaser</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check create teaser with exceeded limit")
    public void checkCreateTeaserWithExceededLimit() {
        log.info("Test is started");
        int campaignId = 1427;
        String campaignName = "Limit for amount of teasers 3";
        int limit = 2;

        goToCreateTeaser(login, campaignId);
        softAssert.assertEquals(pagesInit.getTeaserClass()
                .setPriceOfClick("15")
                .useDiscount(true)
                .createTeaser(), 0, "Fail - teaser created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(String.format(limitError, campaignName, limit)),
                "FAIL -> Check teaser settings in list!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
