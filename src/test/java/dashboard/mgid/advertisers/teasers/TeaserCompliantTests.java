package dashboard.mgid.advertisers.teasers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PUSH;

public class TeaserCompliantTests extends TestBase {
    @Feature("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check default compliant after create teaser\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(dataProvider = "campaignTypes", description = "Check default compliant option after create teaser")
    public void createTeaserProduct(int campaignId, boolean isSetDescription, boolean isNeedToEditCategory) {
        log.info("Test is started");
        log.info("create teaser");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, arrayCountries);
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, AUTOCONTROL.getCompliantType(), IAP.getCompliantType(), RAC.getCompliantType(), ZPL.getCompliantType(), POLAND_REG_COMPLIANT.getCompliantType());

        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);

        pagesInit.getTeaserClass().setPriceOfClick("10").
                setDescription("Some description")
                .useDescription(isSetDescription)
                .createTeaser();

        log.info("check compliant");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getTeaserClass().getTeaserId());
        pagesInit.getCabTeasers().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(5), "Fail - size");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(AUTOCONTROL, IAP, RAC, ZPL, POLAND_REG_COMPLIANT), "Fail - types");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypes() {
        return new Object[][]{
                {CAMPAIGN_MGID_PUSH, true, true},
                {CAMPAIGN_MGID_PRODUCT, false, false}
        };
    }

    @Feature("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check unblock teaser with no flag compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblock teaser with no flag compliant")
    public void checkUnblockingWithoutCompliantFlag() {
        log.info("Test is started");
        int campaignId = 1284;
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Spain");
        operationMySql.getgHits1().updateBlockedOption(1303, 1);
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, AUTOCONTROL.getCompliantType());

        authDashAndGo("testemail1000@ex.ua","advertisers/teasers-goods/campaign_id/" + campaignId);
        pagesInit.getTeaserClass().unBlockTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("TEASER YOU’RE TRYING TO UNBLOCK IS NOT AUTOCONTROL COMPLIANT"), "FAIL - Spain");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Italy");
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, IAP.getCompliantType());

        authDashAndGo("testemail1000@ex.ua","advertisers/teasers-goods/campaign_id/" + campaignId);
        pagesInit.getTeaserClass().unBlockTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("TEASER YOU’RE TRYING TO UNBLOCK IS NOT IAP COMPLIANT"), "FAIL - Italy");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Romania");
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, RAC.getCompliantType());

        authDashAndGo("testemail1000@ex.ua","advertisers/teasers-goods/campaign_id/" + campaignId);
        pagesInit.getTeaserClass().unBlockTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                "TEASER YOU’RE TRYING TO UNBLOCK IS NOT RAC COMPLIANT"), "FAIL - Romania");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Poland");
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, POLAND_REG_COMPLIANT.getCompliantType());

        authDashAndGo("testemail1000@ex.ua","advertisers/teasers-goods/campaign_id/" + campaignId);
        pagesInit.getTeaserClass().unBlockTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("TEASER YOU’RE TRYING TO UNBLOCK IS NOT POLAND REG COMPLIANT"), "FAIL - Poland");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
