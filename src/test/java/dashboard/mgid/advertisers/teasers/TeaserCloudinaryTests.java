package dashboard.mgid.advertisers.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static libs.devtools.CloudinaryMock.ResponseWithImages.*;
import static core.service.constantTemplates.ConstantsInit.TEASERS;


public class TeaserCloudinaryTests extends TestBase {
    String clientLogin = "testEmail1002@ex.ua";

    @Feature("Cloudinary")
    @Story("Image | Dashboard")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Create teaser with static image and check options field</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51780\">Ticket TA-51780</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Create teaser with static image and check options field")
    public void editTeaserInlineDashboard() {
        log.info("Test is started");
        int campaignId = 1041;
        int teaserId = 1128;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        log.info("Create teaser with response from cloudinary");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(STONEHENGE, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getTeaserClass().isGenerateNewValues(false)
                .setNeedToEditImage(true)
                .setImage("mount.jpg")
                .editTeaser();

        log.info("check teaser settings in edit interface after edit him");
        operationMySql.getgHits1().updateTeaserModerationStatus(teaserId, 0);
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        softAssert.assertTrue(optionValues.contains("\"clEffects\": [\"e_sharpen:100\", \"c_fill\", \"g_faces:auto\", \"f_jpg\", \"q_auto:good\"]"), "FAIL - effects cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary")
    @Story("MP4 | Dashboard.")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Edit teaser with video and check displaying video tag at teaser list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52145\">Ticket TA-52145</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Edit teaser with video and check displaying video tag at teaser list")
    public void editVideoTeaserDashboard() {
        log.info("Test is started");
        int campaignId = 1041;
        int teaserId = 1312;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        log.info("Create teaser with response from cloudinary");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getTeaserClass().isGenerateNewValues(false)
                .setNeedToEditImage(true)
                .setImage("managers_croped.mov")
                .editTeaser();

        log.info("check teaser settings in edit interface after edit him");
        Assert.assertFalse(pagesInit.getTeaserClass().isDisplayedEditTeaserPopup(), "Fail - popup is displayed");

        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().getVideoLinkFromTeaserList().contains("/imgh/video/upload/ar_16:9,c_fill,w_680/local.s3.eu-central-1.amazonaws.com"), "Fail - options");

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);

        softAssert.assertTrue(options.contains("\"clEffects\": []"), "Fail - options");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "Fail - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_1:1"), "Fail - link ar_1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_3:2"), "Fail - link ar_3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_16:9"), "Fail - link ar_16:9");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature("Image Extensions")
    @Story("Edit Interface Dash")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Edit teaser with animation and check displaying video tag at teaser list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52145\">Ticket TA-52145</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Edit teaser with animation and check displaying video tag at teaser list")
    public void editTeaserWithAnimationAndCheckVideoTag() {
        log.info("Test is started");
        int campaignId = 1041;
        int teaserId = 1373;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        log.info("Create teaser with response from cloudinary");

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*img-cloudinary-load*");
        pagesInit.getTeaserClass().loadImage("space_journey.gif");
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getTeaserClass().isGenerateNewValues(false)
                .setNeedToEditImage(false)
                .setImage("managers_croped.mov")
                .editTeaser();

        log.info("check teaser settings in edit interface after edit him");
        Assert.assertFalse(pagesInit.getTeaserClass().isDisplayedEditTeaserPopup(), "Fail - popup is displayed");

        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        softAssert.assertTrue(pagesInit.getTeaserClass().getVideoLinkFromTeaserList().contains("/imgh/video/upload/ar_16:9,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/happy_woman.mp4"), "Fail - getVideoLinkFromTeaserList");

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);

        log.info("options - " + options);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);
        softAssert.assertTrue(options.contains("\"clEffects\": []"), "Fail - options");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "Fail - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_1:1"), "Fail - link ar_1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_3:2"), "Fail - link ar_3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_16:9"), "Fail - link ar_16:9");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "Fail - getVideoLinksValue create");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "Fail - getAnimationValue create");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary")
    @Story("Image | Dashboard")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check crop buttons 1:1, 3:2, 16:9</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51780\">Ticket TA-51780</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Check crop buttons 1:1, 3:2, 16:9")
    public void checkCropFunctionalityDashboard() {
        log.info("Test is started");
        int campaignId = 1041;
        int teaserId = 1129;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        log.info("Check default crop");
        softAssert.assertTrue(pagesInit.getTeaserClass().isActiveCrop("16-9"), "FAIL - 16-9");

        log.info("Check crop 3-2");
        pagesInit.getTeaserClass().enableCropFormat("3-2");
        softAssert.assertTrue(pagesInit.getTeaserClass().isActiveCrop("3-2"), "FAIL - 3-2");

        log.info("Check crop 1-1");
        pagesInit.getTeaserClass().enableCropFormat("1-1");
        softAssert.assertTrue(pagesInit.getTeaserClass().isActiveCrop("1-1"), "FAIL - 1-1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary")
    @Story("Image | Dashboard")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check disable invalid crops</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51780\">Ticket TA-51780</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Check disable invalid crops")
    public void checkDisabledInvalidCrop() {
        log.info("Test is started");
        int campaignId = 1041;
        int teaserId = 1130;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);

        log.info("Check default crop");
        Assert.assertTrue(pagesInit.getTeaserClass().isDisableCrop("1-1"));

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature("Image Extensions")
    @Story("Create Interface Dash")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Create teaser with animation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52225\">Ticket TA-52225</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Create teaser with animation")
    public void createTeaserWithAnomation() {
        log.info("Test is started");
        int campaignId = 1041;
        authDashAndGo(clientLogin, "advertisers/add-teaser-goods/campaign_id/" + campaignId);
        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getTeaserClass().loadImage("space_journey.gif");
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getTeaserClass().setNeedToEditImage(false)
                .setDomain("https://testsitegoodsmg.com")
                .setPriceOfClick("5")
                .createTeaser();

        int teaserId = pagesInit.getTeaserClass().getTeaserId();

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        log.info("Check created teaser");
        pagesInit.getCabTeasers().openPopupEditVideoInline();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkImageVisibility(), "FAIL - image visibility");

        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);

        softAssert.assertFalse(operationMySql.getgHits1().getOptionsValue(teaserId).contains("clEffects"), "Fail - options");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue create");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com"), "FAIL - link create");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoLinksValue create");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "FAIL - getAnimationValue create");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary")
    @Story("MP4 | Dashboard.")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Create teaser with video and check displaying video tag at teaser list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52145\">Ticket TA-52145</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Create teaser with video and check displaying video tag at teaser list")
    public void createTeaserWithCloudinaryVideo() {
        log.info("Test is started");
        int campaignId = 1303;
        authDashAndGo(clientLogin, "advertisers/add-teaser-goods/campaign_id/" + campaignId);
        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getTeaserClass().loadImage("managers_croped.mov");
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        Assert.assertNotEquals(pagesInit.getTeaserClass().setDomain("https://testsitegoodsmg.com")
                .setNeedToEditImage(false)
                .setPriceOfClick("5")
                .createTeaser(), 0, "Fail - teaser didn't created");

        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + pagesInit.getTeaserClass().getTeaserId());

        softAssert.assertTrue(pagesInit.getTeaserClass().getVideoLinkFromTeaserList().contains("/imgh/video/upload/ar_16:9,c_fill,w_680/local.s3.eu-central-1.amazonaws.com"), "Fail - options");

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getTeaserClass().getTeaserId());
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(pagesInit.getTeaserClass().getTeaserId());

        softAssert.assertFalse(options.contains("clEffects"), "Fail - options");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(pagesInit.getTeaserClass().getTeaserId()), 1, "Fail - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_1:1"), "Fail - link ar_1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_3:2"), "Fail - link ar_3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_16:9"), "Fail - link ar_16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains("ar_16:9"), "Fail - link ar_16:9");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Feature("Cloudinary")
    @Story("MP4 | Dashboard.")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check message of validation of video file extension</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52145\">Ticket TA-52145</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Check message of validation of video file extension")
    public void checkMessageFailExtensionValidation() {
        log.info("Test is started");
        int campaignId = 1303;
        authDashAndGo(clientLogin, "advertisers/add-teaser-goods/campaign_id/" + campaignId);
        log.info("Create teaser with response from cloudinary");
        pagesInit.getTeaserClass().loadImage("managers_croped.avi");
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("FILE 'MANAGERS_CROPED.AVI' HAS A FALSE EXTENSION"));

        log.info("Test is finished");
    }

    @Feature("Focal Point")
    @Story("Интеграция Cloudinary. Этап3. Focal Point")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Set manual focal point and check saved data to g_hits_1.options</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Set manual focal point and check saved data to g_hits_1.options create")
    public void checkFocalPointValuesCreateTeaser() {
        log.info("Test is started");
        authDashAndGo(clientLogin, "advertisers/add-teaser-goods/campaign_id/1261");

        pagesInit.getTeaserClass()
                .useManualFocalPoint(true)
                .setFocalPoint(144, 133)
                .setPriceOfClick("5")
                .createTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getTeaserClass().getTeaserId());

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_268\""), "x_268");
        softAssert.assertTrue(options.contains("\"y_275\""), "y_275");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Focal Point")
    @Story("Cloudinary integration. Stage 3. Focal Point")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check manual focal point cancel for create teaser</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52184\">Ticket TA-52184</a></li>\n" +
            "</ul>")
    @Owner(value="OHV")
    @Test(description = "Check manual focal point cancel for create teaser")
    public void checkFocalPointValuesCancelCreateTeaser() {
        log.info("Test is started");
        authDashAndGo(clientLogin, "advertisers/add-teaser-goods/campaign_id/1261");

        pagesInit.getTeaserClass().loadImage("mount.jpg");
        pagesInit.getTeaserClass()
                .useManualFocalPoint(true)
                .indicateFocalPoint(144, 133, false);

        pagesInit.getTeaserClass().cancelManualFocalPoint();

        pagesInit.getTeaserClass()
                .useManualFocalPoint(false)
                .setPriceOfClick("5")
                .setNeedToEditImage(false)
                .createTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getTeaserClass().getTeaserId());

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");
        softAssert.assertAll();
        log.info("Test finished");
    }

    @Story("Интеграция Cloudinary. Этап3. Focal Point")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Set manual focal point and check saved data to g_hits_1.options</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Set manual focal point and check saved data to g_hits_1.options edit")
    public void checkFocalPointValuesEditTeaser() {
        log.info("Test is started");
        int teaserId = 1264;
        authDashAndGo(clientLogin, "advertisers/teasers-goods/campaign_id/1261/search/" + teaserId);

        pagesInit.getTeaserClass().openTeasersEditInterface(teaserId);
        softAssert.assertFalse(pagesInit.getTeaserClass().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");

        pagesInit.getTeaserClass().isGenerateNewValues(false)
                .useManualFocalPoint(true)
                .setImage("mount.jpg")
                .setFocalPoint(144, 133)
                .setNeedToEditImage(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_411\""), "x_411");
        softAssert.assertTrue(options.contains("\"y_414\""), "y_414");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
