package dashboard.mgid.advertisers;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.sorted.SortedType;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.advertiser.variables.SelectiveBiddingVariables.*;
import static core.helpers.BaseHelper.randomNumbersInt;
import static core.helpers.BaseHelper.waitForAjax;

public class SelectiveBiddingNew2Tests extends TestBase {
    private static final String login = "testemail1000@ex.ua";
    public SelectiveBiddingNew2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    private void goToSelectiveBidding(int id, String...link){
        String partOfLink = link.length > 0 ? link[0]: "";
        authDashAndGo(login, "advertisers/campaign-quality-analysis/v2/" + id + partOfLink);
        sleep(5);
        waitForAjax();
    }

    /**
     * Check unblock sources by mass action
     * <p> NIO </p>
     */
    @Test
    public void checkUnblockSourcesMassAction() {
        log.info("Test is started");
        goToSelectiveBidding(1114);
        log.info("Pre conditions steps");
        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("block");

        log.info("Choose sources and unblock its");
        goToSelectiveBidding(1114);

        pagesInit.getSelectiveBinding2().expandAllProviders(3);
        pagesInit.getSelectiveBinding2().chooseRandomProvider();
        pagesInit.getSelectiveBinding2().chooseRandomSources();
        log.info("Get amount of marked sources");
        int chosenSources = pagesInit.getSelectiveBinding2().getNumberOfSelectedSources();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("unblock");

        log.info("Get amount of unblocked sources");
        goToSelectiveBidding(1114);

        pagesInit.getSelectiveBinding2().expandAllProviders(3);
        int unblockedSources = pagesInit.getSelectiveBinding2().getNumberOfUnblockedElements();
        log.info("Compare marked with unblocked - " + chosenSources + " == " + unblockedSources);
        Assert.assertEquals(chosenSources, unblockedSources, "Compare choosen and unblocked sources");
        log.info("Test is finished");
    }

    /**
     * Check block sources by mass action
     * <p> NIO </p>
     */
    @Test
    public void checkBlockSourcesMassAction() {
        log.info("Test is started");
        goToSelectiveBidding(1110);

        log.info("Pre conditions steps");
        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("unblock");

        log.info("Choose sources and unblock its");
        goToSelectiveBidding(1110);

        pagesInit.getSelectiveBinding2().expandAllProviders(3);
        pagesInit.getSelectiveBinding2().chooseRandomProvider();
        pagesInit.getSelectiveBinding2().chooseRandomSources();
        log.info("Get amount of marked sources");
        int chosenSources = pagesInit.getSelectiveBinding2().getNumberOfSelectedSources();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("block");

        log.info("Get amount of unblocked sources");
        goToSelectiveBidding(1110);

        pagesInit.getSelectiveBinding2().expandAllProviders(3);
        int unblockedSources = pagesInit.getSelectiveBinding2().getNumberOfBlockedElements();
        log.info("Compare marked with unblocked - " + chosenSources + " == " + unblockedSources);
        Assert.assertEquals(chosenSources, unblockedSources, "Compare choosen and unblocked sources");
        log.info("Test is finished");
    }

    /**
     * Check unblock/block sources by providers
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockByProvider() {
        log.info("Test is started");
        int amountOfActiveSources;
        int amountOfAllSources;
        authDashAndGo(login, "advertisers/campaign-quality-analysis/v2/1006?search=AdMaven");
        goToSelectiveBidding(1006, "?search=AdMaven");

        log.info("Pre conditions steps");
        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("block");

        log.info("Unblock sources");
        goToSelectiveBidding(1006, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().clickBlockUnblockProviderButton();
        amountOfActiveSources = pagesInit.getSelectiveBinding2().getNumberOfUnblockedSources();
        amountOfAllSources = pagesInit.getSelectiveBinding2().getNumberOfAllSources();
        log.info("Check amount all and unblocked sources - " + amountOfAllSources + " == " + amountOfActiveSources);
        softAssert.assertEquals(amountOfActiveSources, amountOfAllSources, "Compare amount of  active and all sources");

        log.info("Block sources");
        goToSelectiveBidding(1006, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().clickBlockUnblockProviderButton();
        amountOfActiveSources = pagesInit.getSelectiveBinding2().getNumberOfBlockedSources();
        amountOfAllSources = pagesInit.getSelectiveBinding2().getNumberOfAllSources();
        log.info("Check amount all and blocked sources - " + amountOfAllSources + " == " + amountOfActiveSources);
        softAssert.assertEquals(amountOfActiveSources, amountOfAllSources, "Compare amount of  blocked and all sources");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check unblock/block sources by source's checkboxes
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockBySources() {
        log.info("Test is started");
        int amountOfChangedSources;
        int amountOfChosenElements = 3;
        goToSelectiveBidding(1115, "?search=AdMaven");

        log.info("Pre conditions steps");
        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("block");

        log.info("Unblock sources");
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().unblockRandomSources(amountOfChosenElements);
        goToSelectiveBidding(1115, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        amountOfChangedSources = pagesInit.getSelectiveBinding2().getNumberOfUnblockedSources();
        softAssert.assertEquals(amountOfChosenElements, amountOfChangedSources, "Check amount unblocked sources");
        log.info("Unblock all sources");
        goToSelectiveBidding(1115, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().selectAllProviders();
        pagesInit.getSelectiveBinding2().clickBlockUnblockButtonMass("unblock");
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().blockRandomSources(amountOfChosenElements);
        goToSelectiveBidding(1115, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        amountOfChangedSources = pagesInit.getSelectiveBinding2().getNumberOfBlockedElements();
        softAssert.assertEquals(amountOfChosenElements, amountOfChangedSources, "Check amount blocked sources");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Change coefficient by providers
     * <p> NIO </p>
     */
    @Test
    public void changeCoefficientByProvider() {
        log.info("Test is started");
        String randomPrice = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(1.5, 10.0);
        int amountOfAllElements;
        int amountOfElementsWithNewPrice;
        goToSelectiveBidding(1006, "?search=AmplyMedia");


        log.info("Change price by provider to " + randomPrice);
        pagesInit.getSelectiveBinding2().changePriceByProvider(randomPrice);
        log.info("Get values and check price by provider");
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        amountOfAllElements = pagesInit.getSelectiveBinding2().getNumberOfAllSources();
        amountOfElementsWithNewPrice = pagesInit.getSelectiveBinding2().getAllSourcesWithPrice(randomPrice);
        log.info("Check price by provider " + amountOfAllElements + " == " + amountOfElementsWithNewPrice);
        Assert.assertEquals(amountOfAllElements, amountOfElementsWithNewPrice, "Different amount");
        log.info("Test is finished");
    }

    /**
     * Change coefficient by sources
     * <p> NIO </p>
     */
    @Test
    public void changeCoefficientBySources() {
        log.info("Test is started");
        String randomPrice = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(1.5, 10.0);
        int amountOfElementsWithNeededPrice = 1;
        int amountOfElementsWithNewPrice;
        goToSelectiveBidding(1116, "?search=AmplyMedia");


        log.info("Change price by provider to " + randomPrice);
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().changePriceBySource(randomPrice);
        log.info("Get values and check price by provider");
        amountOfElementsWithNewPrice = pagesInit.getSelectiveBinding2().getAllSourcesWithPrice(randomPrice);
        log.info("Check price by provider " + amountOfElementsWithNeededPrice + " == " + amountOfElementsWithNewPrice);
        Assert.assertEquals(amountOfElementsWithNeededPrice, amountOfElementsWithNewPrice, "Different amount");
        log.info("Test is finished");
    }

    /**
     * Check min and max coefficient of sources
     * <p> NIO </p>
     */
    @Test
    public void checkMaxMinCoefficient() {
        log.info("Test is started");
        String outOfMinPrice = "0.05";
        String outOfMaxPrice = "11.5";
        String amountOfElementsWithNewPrice;
        goToSelectiveBidding(1006, "?search=AmplyMedia");


        log.info("Change price by provider to " + outOfMinPrice);
        pagesInit.getSelectiveBinding2().expandAllProviders(1);
        pagesInit.getSelectiveBinding2().changePriceBySource(outOfMinPrice);
        log.info("Get values and check price by provider");
        amountOfElementsWithNewPrice = pagesInit.getSelectiveBinding2().getSourceCoefficient(0);
        softAssert.assertTrue(amountOfElementsWithNewPrice.contains("0.1"), "Min range different with required");

        log.info("Change price by provider to " + outOfMaxPrice);
        pagesInit.getSelectiveBinding2().changePriceBySource(outOfMaxPrice);
        log.info("Get values and check price by provider");
        amountOfElementsWithNewPrice = pagesInit.getSelectiveBinding2().getSourceCoefficient(0);
        softAssert.assertTrue(amountOfElementsWithNewPrice.contains("10.0"), "Max range different with required");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check displayed source data with value set before
     * <p> NIO </p>
     */
    @Test
    public void checkSourcesData() {
        log.info("Test is started");
        List<String> sourcesDataInt;
        goToSelectiveBidding(1006, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);

        log.info("Get data's and check - clicks");
        sourcesDataInt = pagesInit.getSelectiveBinding2().getSourcesDataFromColumn("clicks");
        softAssert.assertEquals(sourcesDataIntClicks, sourcesDataInt, "clicks");

        log.info("Get data's and check - spent");
        sourcesDataInt = pagesInit.getSelectiveBinding2().getSourcesDataFromColumn("spent");
        softAssert.assertEquals(sourcesDataIntSpent, sourcesDataInt, "spent");

        log.info("Get data's and check - cpc");
        sourcesDataInt = pagesInit.getSelectiveBinding2().getSourcesDataFromColumn("cpc");
        softAssert.assertEquals(sourcesDataIntCpc, sourcesDataInt, "cpc");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check displayed providers data match with displayed sources data
     * <p> NIO </p>
     */
    @Test
    public void checkProviderData() {
        log.info("Test is started");
        int providersDataInt;
        int sourcesDataInt;
        double providersDataDouble;
        double sourcesDataDouble;
        goToSelectiveBidding(1006, "?search=AdMaven");

        pagesInit.getSelectiveBinding2().expandAllProviders(1);

        log.info("Get data's and check - clicks");
        providersDataInt = pagesInit.getSelectiveBinding2().getProviderColumnDataInt("clicks");
        sourcesDataInt = pagesInit.getSelectiveBinding2().getSumSourcesColumnDataInt("clicks");
        softAssert.assertEquals(providersDataInt, sourcesDataInt, "clicks");

        log.info("Get data's and check - spent");
        providersDataDouble = pagesInit.getSelectiveBinding2().getProviderColumnDataDouble("spent");
        sourcesDataDouble = pagesInit.getSelectiveBinding2().getSourcesColumnDataDouble("spent");
        softAssert.assertEquals(providersDataDouble, sourcesDataDouble, "spent");

        log.info("Get data's and check - cpc");
        providersDataDouble = pagesInit.getSelectiveBinding2().getProviderColumnDataDouble("cpc");
        sourcesDataDouble = pagesInit.getSelectiveBinding2().getSourcesColumnDataDoubleAveradge("cpc");
        softAssert.assertEquals(providersDataDouble, sourcesDataDouble, 0.1, "cpc");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check sort data by sources layer
     * <p> NIO </p>
     */
    @Test
    public void checkSortColumnsSources() {
        log.info("Test is started");
        log.info("Prepare test data");
        String[] randomColumn = {"CPC", "Spent" , "Clicks"};
        String choosenElement = randomColumn[randomNumbersInt(randomColumn.length)];
        SortedType.SortType type = new SortedType().randomPeriod();
        log.info("Column, type - " + choosenElement + ", " + type);
        goToSelectiveBidding(1006, "?search=AmplyMedia");

        Assert.assertTrue(pagesInit.getSelectiveBinding2().checkSortedSelectiveSources(type,
                choosenElement));
        log.info("Test is finished");
    }
}
