package dashboard.mgid.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

public class IdenfyTests extends TestBase {

    private final String idenfyNotificationText = "To access the full functionality, you need to confirm your identity. Click here to confirm.";
    private final String idenfyPopupIndividualClientText = "In order to continue working, you need to confirm your identity. Are you ready to be verified?\n" +
            "To confirm the verification, the iDenfy service is used";
    private final String idenfyPopupLegalEntityText = "You are registered as a Legal Entity. You need to sign a contract. To sign the contract, contact a personal manager.\n" +
            "- For Advertisers sokgoodhits, sokgoodhits@ex.ua\n" +
            "CLOSE";

    private final String idenfyProfileLegalEntityText = "You are registered as a Legal Entity. You need to sign a contract. To sign the contract, contact a personal manager.\n" +
            "- For Advertisers sokgoodhits,";

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Advertisers. Add Dashboard iframe and API")
    @Description("Check Idenfy statuses in clients profile\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52167\">Ticket TA-52167</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check Idenfy statuses in clients profile", dataProvider = "idenfyClientsStatuses")
    public void checkIdenfyStatuses(String clientLogin, String idenfyStatus) {
        log.info("Test is started");
        authDashAndGo(clientLogin, "profile/users");
        pagesInit.getUserProfile().goToProfileIdenfyTab();
        Assert.assertEquals(pagesInit.getUserProfile().getIdenfyVerificationStatus(), "Verification status: " + idenfyStatus,
                "FAIL -> Idenfy status " + idenfyStatus);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] idenfyClientsStatuses() {
        return new Object[][]{
                {"idenfy-client-2@mgid.com", "Approved"},
                {"idenfy-client-3@mgid.com", "Denied"},
                {"idenfy-client-4@mgid.com", "Suspected"},
                {"idenfy-client-5@mgid.com", "Verification time has expired."},
                {"idenfy-client-6@mgid.com", "In progress"},
        };
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Advertisers. Add Dashboard iframe and API")
    @Description("Check that payment systems is available for approved by Idenfy clients\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52167\">Ticket TA-52167</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check that payment systems is available for approved by Idenfy clients")
    public void checkPaymentsIsAvailableForIdenfyApprovedClient() {
        log.info("Test is started");
        authDashAndGo("idenfy-client-2@mgid.com", "");
        pagesInit.getCampaigns().clickAddFundsButton();
        Assert.assertTrue(pagesInit.getCampaigns().paymentsIsAvailable(),
                "FAIL -> Payments are unavailable for Idenfy Approved client!");
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. 'Need verification' message in the Dashboard")
    @Description("Check that 'Need verification' message is displayed for not approved by Idenfy individual clients\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52489\">Ticket TA-52489</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    //todo mvv
    //@Test(dataProvider = "idenfyStatuses", description = "Check that 'Need verification' message is displayed for not approved by Idenfy individual clients")
    public void checkIdenfyNeedVerificationMessageForIndividual(String verifiedStatus) {
        log.info("Test is started");
        operationMySql.getKycIdenfy().updateStatus(2071, verifiedStatus);
        operationMySql.getClients().updateClientsCanVerifyIdenfy(2071, "1");
        authDashAndGo("idenfy-client-13@mgid.com", "");
        softAssert.assertEquals(pagesInit.getCampaigns().getNotificationText(),
                idenfyNotificationText,
                "FAIL -> Need verification message is not present for Individual client with verified status - '" + verifiedStatus + "'");
        pagesInit.getCampaigns().clickAddFundsButton();
        softAssert.assertEquals(pagesInit.getCampaigns().getIdenfyPopupText(), idenfyPopupIndividualClientText,
                "FAIL -> Idenfy confirmation popup is not appeared for " + verifiedStatus + " client!");
        pagesInit.getCampaigns().closeIdenfyPopup();
        pagesInit.getCampaigns().clickHereLinkInNotification();
        softAssert.assertTrue(pagesInit.getCampaigns().isStartVerificationButtonDisplayed(),
                "FAIL -> 'START VERIFICATION' button is not displayed for " + verifiedStatus + " client!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] idenfyStatuses() {
        return new Object[][] {
                {"DENIED"},
                {"SUSPECTED"},
                {"EXPIRED"}
        };
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. 'Need verification' message in the Dashboard")
    @Description("Check that payment systems is available for Individual client without 'Can Verify Idenfy' flag\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52489\">Ticket TA-52489</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that payment systems is available for Individual client without 'Can Verify Idenfy' flag")
    public void checkPaymentsIsAvailableForIndividualWithoutCanVerifyIdenfy() {
        log.info("Test is started");
        authDashAndGo("idenfy-client-15@mgid.com", "");
        softAssert.assertFalse(pagesInit.getCampaigns().checkNotificationIsDisplayed(),
                "FAIL -> 'Need verification' message is present for Individual client without 'Can verify Idenfy' flag!");
        pagesInit.getCampaigns().clickAddFundsButton();
        softAssert.assertTrue(pagesInit.getCampaigns().paymentsIsAvailable(),
                "FAIL -> Payments are unavailable for client without 'Can verify Idenfy' flag!");
        log.info("Test is finished");
        softAssert.assertAll();
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. 'Need verification' message in the Dashboard")
    @Description("Check that 'Need verification' message is displayed for not approved by Idenfy Legal entity clients\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52489\">Ticket TA-52489</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    //todo MVV
    //@Test(description = "Check that payment systems is available for approved by Idenfy Legal entity clients")
    public void checkIdenfyNeedVerificationMessageForLegalEntity() {
        log.info("Test is started");
        authDashAndGo("idenfy-client-14@mgid.com", "");
        Assert.assertEquals(pagesInit.getCampaigns().getNotificationText(),
                idenfyNotificationText,
        "FAIL -> Need verification message is not present for Legal Entity client!");
        pagesInit.getCampaigns().clickAddFundsButton();
        softAssert.assertEquals(pagesInit.getCampaigns().getIdenfyPopupText(), idenfyPopupLegalEntityText,
                "FAIL -> Idenfy confirmation popup is not appeared for Legal entity client by 'ADD FUNDS' button!");
        pagesInit.getCampaigns().closeIdenfyPopup();
        pagesInit.getCampaigns().clickHereLinkInNotification();
        softAssert.assertEquals(pagesInit.getCampaigns().getIdenfyProfileText(), idenfyProfileLegalEntityText,
                "FAIL -> Idenfy profile text is not valid for Legal entity client by link 'here'!");
        operationMySql.getClients().updateClientsCanVerifyIdenfy(2072, "0");
        authDashAndGo("idenfy-client-14@mgid.com", "");
        softAssert.assertFalse(pagesInit.getCampaigns().checkNotificationIsDisplayed(),
                "FAIL -> 'Need verification' message is present for Individual client without 'Can verify Idenfy' flag!");
        pagesInit.getCampaigns().clickAddFundsButton();
        softAssert.assertTrue(pagesInit.getCampaigns().paymentsIsAvailable(),
                "FAIL -> Payments are unavailable for Legal entity client without 'Can verify Idenfy' flag!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}