package dashboard.mgid.advertisers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.dash.advertiser.logic.DetailedGoodStats;
import testBase.TestBase;
import testData.project.Subnets;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static pages.dash.advertiser.logic.DetailedGoodStats.StatsSection.*;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;

public class DetailedGoodStatsTest extends TestBase {
    String startDay = LocalDate.now(ZoneId.of("Pacific/Honolulu")).minusDays(4).toString();

    public DetailedGoodStatsTest() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    /**
     * Дашборд. Интерфейс поденной статистики. Выводить sources вместо uid.
     *
     * <p> NIO </p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24037">TA-24037</a>
     */
    @Test(dataProvider = "statisticsData")
    public void checkInterestStageByWidgetUid(List<String> sectionsValue, int clicks, double spent, double percent, DetailedGoodStats.StatsSection section) {
        log.info("Test is started");

        authDashAndGo("advertisers/detailed-goods-stat/id/1093/date/" + startDay);
        log.info("Choose interest stage with tickers for all widgets");
        pagesInit.getDetailedGoodStats().chooseStatBy(section);


        log.info("Get table data statistics from interface");
        pagesInit.getDetailedGoodStats().getSumColumnsTableData();

        log.info("Check table statistics");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getClicksTable(), clicks, "FAIL - checkClicksTable");
        softAssert.assertTrue(helpersInit.getBaseHelper().comparisonData(pagesInit.getDetailedGoodStats().getPercentTable(), percent, 0.01), "FAIL - checkPercentTable");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getSpentTable(), spent, "FAIL - checkSpentTable");
        softAssert.assertTrue(pagesInit.getDetailedGoodStats().getSliceTable().containsAll(sectionsValue), "FAIL - checkSectionValueTable");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] statisticsData() {
        return new Object[][]{
                {Arrays.asList("2", "0", "15"), 18, 0.51, 100.00, DOMAIN},
                {Arrays.asList("Canada-Saskatchewan", "Australia-Victoria"), 18, 0.51, 100.00, REGION},
                {Arrays.asList("Canada", "Australia"), 18, 0.51, 100.00, COUNTRY},
                {Arrays.asList("mobile Android 6.xx", "desktop Other Desktop OS"), 6, 0.18, 34.63, OS},
                {Arrays.asList("Safari", "Firefox"), 18, 0.51, 100.00, BROWSER},
                {Arrays.asList("Non Mobile Connections", "Wi-Fi", "Carrier"), 18, 0.51, 100.00, CONNECTION_TYPE},
        };
    }

    /**
     * Checking daily statistics by Traffic type
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51786">Ticket TA-51786</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkDailyStatisticsByTrafficType() {
        log.info("Test is started");
        String startDay = LocalDate.now(ZoneId.of("Pacific/Honolulu")).minusDays(3).toString();
        int campaignId = 2005;
        List<String> trafficTypeValues = Arrays.asList("Social", "Referral", "Direct", "Organic");
        int clicks = 37;
        double spent = 0.72;
        double percent = 100.00;
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/detailed-goods-stat/id/" + campaignId +"/date/" + startDay);
        log.info("Choose tab with statistics by Traffic type");
        pagesInit.getDetailedGoodStats().chooseStatBy(ADV_TRAFFIC_TYPE);
        log.info("Get table data statistics from interface");
        pagesInit.getDetailedGoodStats().getSumColumnsTableData();

        log.info("Check table statistics");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getClicksTable(), clicks,
                "FAIL - Clicks checking");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getPercentTable(), percent,
                "FAIL - Percent checking");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getSpentTable(), spent,
                "FAIL - Spent checking");
        softAssert.assertTrue(pagesInit.getDetailedGoodStats().getSliceTable().containsAll(trafficTypeValues),
                "FAIL - Section Value checking");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление среза на ценовой диапазон девайса в интерфейсы статистики")
    @Description("Проверка интерфейса детальной статистики по ценовым диапазонам девайсов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51877\">Ticket TA-51877</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка интерфейса детальной статистики по ценовым диапазонам девайсов")
    public void checkDailyStatisticsByPhonePrices() {
        log.info("Test is started");
        String startDay = LocalDate.now(ZoneId.of("Pacific/Honolulu")).minusDays(3).toString();
        int campaignId = 2005;
        List<String> trafficTypeValues = Arrays.asList("Undefined", "From 150$ to 200$", "From 200$ to 250$", "From 300$ to 350$",
                "From 350$ to 400$", "From 400$ to 500$", "From 500$ to 600$", "From 600$ to 700$", "From 700$ and more");
        int clicks = 37;
        double spent = 0.74;
        double percent = 100.01;

        authDashAndGo("sok.autotest.payouts@mgid.com", "advertisers/detailed-goods-stat/id/" + campaignId + "/date/" + startDay);
        log.info("Choose tab with statistics by 'Phone price ranges'");
        pagesInit.getDetailedGoodStats().chooseStatBy(PHONE_PRICE_RANGES);

        log.info("Get table data statistics from interface");
        pagesInit.getDetailedGoodStats().getSumColumnsTableData();

        log.info("Check table statistics");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getClicksTable(), clicks,
                "FAIL - Clicks checking");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getPercentTable(), percent,
                "FAIL - Percent checking");
        softAssert.assertEquals(pagesInit.getDetailedGoodStats().getSpentTable(), spent,
                "FAIL - Spent checking");
        softAssert.assertTrue(pagesInit.getDetailedGoodStats().getSliceTable().containsAll(trafficTypeValues),
                "FAIL - Section Value checking");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
