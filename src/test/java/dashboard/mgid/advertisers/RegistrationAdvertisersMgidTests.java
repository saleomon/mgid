package dashboard.mgid.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextMgid;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.mgidLink;

public class RegistrationAdvertisersMgidTests extends TestBase {

    /**
     * Create client with role Advertiser
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerAdvertiserMgid() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_ADVERTISER_LOGIN_MGID);
        log.info("Let's register new Advertiser!");
        open(mgidLink + "/user/signup");
        pagesInit.getSignUp().registerAdvertiser(NEW_ADVERTISER_LOGIN_MGID);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_ADVERTISER_LOGIN_MGID)).get(0).substring(mgidLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_ADVERTISER_LOGIN_MGID, NEW_CLIENT_PASSWORD);
         softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopup();
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(NEW_ADVERTISER_LOGIN_MGID),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check sign in be blocked client
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27223">Ticket TA-27223</a>
     * <p>Author NIO</p>
     */
    @Test
    public void checkBlockClientsAuthorization() {
        log.info("Test is started");
        open(mgidLink + "/auth/sign-in");
        pagesInit.getSignUp().signInDash("masstestEmail1126@ex.ua", CLIENTS_PASSWORD);
        log.info("Check message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDashNew("Your account has been blocked. Please contact your administrator"), "ERROR - message not shown");
        log.info("Check displaying authorization form");
        softAssert.assertTrue(pagesInit.getSignUp().isShowLoginDash(), "ERROR - login form not shown");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check restore password by blocked client
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27223">Ticket TA-27223</a>
     * <p>Author NIO</p>
     */
    @Test
    public void checkRestorePasswordByBlockedClient() {
        log.info("Test is started");
        open(mgidLink + "/user/restore-password");

        log.info("Restore password");
        pagesInit.getSignUp().restorePassword("masstestEmail1126@ex.ua");

        log.info("Check displaying authorization form");
        Assert.assertTrue(pagesInit.getSignUp().waitVisibilityShowLoginDash(), "ERROR - login form not shown");
        log.info("Test is finished");
    }

    /**
     * Check sign out client when he become blocked
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27223">Ticket TA-27223</a>
     * <p>Author NIO</p>
     */
    @Test
    public void checkSignOutClientWhileBecomeBlocked() {
        log.info("Test is started");
        log.info("Sign in");
        authDashAndGo("masstestEmail1127@ex.ua", "");
        softAssert.assertFalse(pagesInit.getSignUp().isShowLoginDash(), "ERROR - login form is shown");

        log.info("Mark client as blocked");
        operationMySql.getClients().updateClientsGodmodeBlock("1", "1127");

        log.info("Check displaying authorization form");
        open(mgidLink + "/advertisers/add");
        softAssert.assertFalse(pagesInit.getSignUp().isShowProfileDash(), "ERROR - login form not shown");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Registration")
    @Feature("Register legal entity client")
    @Story("Dashboard")
    @Description("Check register legal entity client - advertiser" +
            "Except registration from RU and BY countries")
    @Owner("AIA")
    @Test(description = "Check register legal entity client - advertiser")
    public void registerAdvertiserMgidLegalEntity() {
        log.info("Test is started");
        List<String> forbiddenCountries = new ArrayList<>(Arrays.asList("RU", "BY"));
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_ADVERTISER_LEGAL_ENTITY_MGID);
        log.info("Let's register new Advertiser!");
        open(mgidLink + "/user/signup");
        pagesInit.getSignUp().registerAdvertiser(NEW_ADVERTISER_LEGAL_ENTITY_MGID);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_ADVERTISER_LEGAL_ENTITY_MGID)).get(0).substring(mgidLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
        pagesInit.getSignUp().signInDash(NEW_ADVERTISER_LEGAL_ENTITY_MGID, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopupLegalEntity(forbiddenCountries);
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(NEW_ADVERTISER_LEGAL_ENTITY_MGID),
                "FAIL -> Company login on the page isn`t correct!");
        open(mgidLink + "/profile/users");
        softAssert.assertTrue(pagesInit.getCampaigns().checkProfileOfLegalEntity(NEW_ADVERTISER_LEGAL_ENTITY_MGID),
                "FAIL -> Check user profile!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Restore password")
    @Feature("Password restoring in MGID")
    @Story("Main dashboard page")
    @Description("Check password restoring in MGID dashboard and login with new password")
    @Owner("AIA")
    @Test(description = "Check password restoring in MGID dashboard and login with new password")
    public void checkRestorePasswordMgid() {
        log.info("Test is started");
        String userLogin = "restore-password.client@mgid.com";
        operationMySql.getMailPull().getCountLettersByClientEmail(userLogin);
        open(mgidLink + "/user/restore-password");
        pagesInit.getSignUp().restorePassword(userLogin);
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(userLogin)).get(0).substring("https://dashboard.mgid.com".length()));
        pagesInit.getSignUp().createPassword(NEW_RESTORED_PASSWORD);
        pagesInit.getSignUp().signInDash(userLogin, NEW_RESTORED_PASSWORD);
        Assert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(userLogin),
                "FAIL -> Login after password restoring is failed!");
        log.info("Test is finished");
    }
}
