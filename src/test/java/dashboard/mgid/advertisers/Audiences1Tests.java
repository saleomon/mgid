package dashboard.mgid.advertisers;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.sorted.SortedType;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;

import static testData.project.ClientsEntities.CLIENT_FOR_DISABLE_403_ONLY;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;

public class Audiences1Tests extends TestBase {

    public Audiences1Tests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    /**
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     * Дашборд. Добавить страницу управления аудиториями [список аудиторий, статистика]
     * Проверка работы права
     * <p>RKO</p>
     */
    @Test(priority = -5)
    @Privilege(name = "default/advertisers/audiences")
    public void workRightCanSeeAudience() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/audiences");
            authDashAndGo(CLIENT_FOR_DISABLE_403_ONLY,"advertisers/audiences");
            Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("ACCESS DENIED OR INVALID REQUEST. PLEASE, CHECK THE LINK FOR CONSISTENCY."), "FAIL -> audiences interface is show");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/audiences");
        }
        log.info("Test is finished");
    }

    /**
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     * Дашборд. Добавить страницу управления аудиториями [список аудиторий, статистика]
     * Проверка работы сортировк по всем обязательным полям (колонку берём рандомную)
     * <p>RKO</p>
     */
    @Test
    public void checkSortInInterface() {
        log.info("Test is started");
        authDashAndGo("advertisers/audiences");
        Assert.assertTrue(helpersInit.getSortedHelper().checkSorted(new SortedType().randomPeriod()), "FAIL -> sort doesn't work");
        log.info("Test is finished");
    }

    /**
     * Add Audience with EventTarget: valid case ( add/edit/check(ui|db) /delete )
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test
    public void createAudience_WithEventTarget() {
        log.info("Test is started");
        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("event")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check created target in db");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistEventTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getTargetEvent(),
                pagesInit.getAudiences().isFulfilmentOfAllConditions()
        ), "FAIL -> check db");

        log.info("check and edit audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience create");
        softAssert.assertEquals(pagesInit.getAudiences().getTargetJsCode(),
                "MgSensor.invoke('" + pagesInit.getAudiences().getTargetEvent() + "');",
                "FAIL -> targetJsCode create");
        pagesInit.getAudiences().clearData().setAudienceType("event").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");
        softAssert.assertEquals(pagesInit.getAudiences().getTargetJsCode(),
                "MgSensor.invoke('" + pagesInit.getAudiences().getTargetEvent() + "');",
                "FAIL -> targetJsCode edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add Audience with VisitedTarget: valid case ( add/check(ui|db) )
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test
    public void createAudience_WithVisited() {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("visited")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check created target in db");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistVisitedTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getPagesToVisit(),
                pagesInit.getAudiences().getDomainForTracking()
        ), "FAIL -> check db create");

        log.info("check audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience create");

        pagesInit.getAudiences().clearData().setAudienceType("visited").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistVisitedTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getPagesToVisit(),
                pagesInit.getAudiences().getDomainForTracking()
        ), "FAIL -> check db edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add Audience with UrlTarget: valid case ( add/check(ui|db) )
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23921">TA-23921</a>
     */
    @Test
    public void createAudience_WithUrl() {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("url")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check created target in db");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistUrlTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getUrlData()
        ), "FAIL -> check db");

        log.info("check audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience");

        pagesInit.getAudiences().clearData().setAudienceType("url").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistUrlTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getUrlData()
        ), "FAIL -> check db edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add Audience with Ad Clickers: valid case ( add/check(ui|db) )
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51713">TA-51713</a>
     */
    @Test
    public void createAudience_WithClickers() {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("ad_clickers")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check created target in db");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistClickersTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getClickersData()
        ), "FAIL -> check db");

        log.info("check audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience");
        softAssert.assertTrue(pagesInit.getAudiences().checkClickersTarget(), "FAIL -> check audience target");

        pagesInit.getAudiences().clearData().setAudienceType("ad_clickers").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistClickersTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getClickersData()
        ), "FAIL -> check db edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add Audience with Ad Viewers: valid case ( add/check(ui|db) )
     * <p>RKO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51713">TA-51713</a>
     */
    @Test
    public void createAudience_WithViewers() {
        log.info("Test is started");

        log.info("create audience with target");
        authDashAndGo("advertisers/audience-add");
        softAssert.assertNotNull(pagesInit.getAudiences()
                .setAudienceType("ad_viewers")
                .createAudienceWithTarget(), "FAIL -> audience doesn't create");

        log.info("check created target in db");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistViewersTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getViewersData()
        ), "FAIL -> check db");

        log.info("check audience");
        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience");
        softAssert.assertTrue(pagesInit.getAudiences().checkViewersTarget(), "FAIL -> check audience target");

        pagesInit.getAudiences().clearData().setAudienceType("ad_viewers").editAudience();

        authDashAndGo("advertisers/audience-edit/audience_id/" + pagesInit.getAudiences().getAudienceId());
        softAssert.assertTrue(pagesInit.getAudiences().checkAudience(), "FAIL -> check audience edit");
        softAssert.assertTrue(operationMySql.getSensorsTargets().isExistViewersTarget(
                pagesInit.getAudiences().getName(),
                pagesInit.getAudiences().getDescription(),
                pagesInit.getAudiences().getViewersData()
        ), "FAIL -> check db edit");

        log.info("delete audience");
        authDashAndGo("advertisers/audiences");
        softAssert.assertTrue(pagesInit.getAudiences().deleteAudience(pagesInit.getAudiences().getAudienceId()), "FAIL -> delete audience");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
