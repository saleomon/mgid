package dashboard.mgid.advertisers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.advertiser.locators.SelectiveBiddingLocators.COEFFICITNT_LABEL;
import static pages.dash.advertiser.variables.SelectiveBiddingVariables.campaignId;
import static core.helpers.BaseHelper.isEqualsLists;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;

public class SelectiveBidding1Tests extends TestBase {
    private final String login = "testemail1000@ex.ua";

    public void goToPlatform(String id_company) {
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + id_company);
        COEFFICITNT_LABEL.shouldBe(visible);
    }

    /**
     * Check block unblock sources with active subSource. Button blocking
     *
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockSubSource() {
        log.info("Test is started");
        int startState;

        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1003");
        startState = pagesInit.getSelectiveBidding1().getAmountActiveSubSource();

        pagesInit.getSelectiveBidding1().blockUnblockSubSource("unblock");
        softAssert.assertNotEquals(startState, pagesInit.getSelectiveBidding1().getAmountActiveSubSource(), "block1");

        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1003");
        startState = pagesInit.getSelectiveBidding1().getAmountActiveSubSource();
        pagesInit.getSelectiveBidding1().blockUnblockSubSource("block");
        softAssert.assertNotEquals(startState, pagesInit.getSelectiveBidding1().getAmountActiveSubSource(), "block2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check block unblock sources with active subSource and witn exclude whole uid option. Button blocking
     *
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockSourceWithTurnOnSubSourceAndGetPopUpWithExcludeWholeUid() {
        log.info("Test is started");
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/1012/search/1001");

        log.info("Block source with disagree block whole uid");
        pagesInit.getSelectiveBidding1().blockUnblockSource("block");
        pagesInit.getSelectiveBidding1().excludeSpecificSubIds(false);

        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkblockUnblockSource("unblock"), "unblock state 1");
        authDashAndGo("advertisers/campaign-quality-analysis/id/1012/search/1001");

        log.info("Block source with agree block whole uid");
        pagesInit.getSelectiveBidding1().blockUnblockSource("block");
        pagesInit.getSelectiveBidding1().excludeSpecificSubIds(true);

        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkblockUnblockSource("block"), "unblock");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Change Coefficient for Sources with subSources
     * <ul>
     *     <li>without change price subSource</li>
     *     <li>with change price subSource</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/BT-782">BT-782</a>
     * <p> NIO </p>
     */
    @Test
    public void changeCoefficientSourceWithSubSource() {
        log.info("Test is started");
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1000");

        pagesInit.getSelectiveBidding1().changeCoefficientSource("1.7");
        pagesInit.getSelectiveBidding1().changeCoefficientSubSource("1.3");
        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1000");

        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.7"), "should be present 1.7");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.3"), "should be present 1.3");

        pagesInit.getSelectiveBidding1().changeCoefficientSourceForAll("1.9");

        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1000");
        softAssert.assertFalse(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.7"), "should not to be present 1.7");
        softAssert.assertFalse(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.3"), "should not to be present 1.3");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.9"), "should be present 1.9");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Change Coefficient for Sources with subSources. Mass action
     * <ul>
     *     <li>change coefficient for subSource</li>
     *     <li>change coefficient for source</li>
     * </ul>
     *
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * <p> NIO </p>
     */
    @Test
    public void changeCoefficientSourceWithSubSourceInMassAction() {
        log.info("Test is started");
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1003");
        pagesInit.getSelectiveBidding1().changeCoefficientSubSourceMass("1.4");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.4"), "should be present 1.4");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1"), "should be present 1");

        pagesInit.getSelectiveBidding1().changeCoefficientSourceMass("1.6");
        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1003");

        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.6"), "should be present 1.6");
        softAssert.assertFalse(pagesInit.getSelectiveBidding1().checkVisibilitySubSourceWithCoefficient("1.4"), "should not to be present 1.4");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check link functionality with options 'Вы хотите выбрать все n виджетов ?'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21653">TA-21653</a>
     * <p> NIO </p>
     */
    @Test
    public void checkFunctionChooseAllCheckBox() {
        log.info("Test is started");
        goToPlatform("1123");
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkFunctionChooseAllCheckBox());
        log.info("Test is finished");
    }

    /**
     * Check displaying row Reach and tips for it
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21664">TA-21664</a>
     * <p> NIO </p>
     */
    @Test
    public void checkReachBlock() {
        log.info("Test is started");
        goToPlatform(campaignId);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkReachBlock());
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignProvider() {
        return new Object[][]{
                {campaignId},
                {campaignId + " "},
                {campaignId + "-"},
                {campaignId + "?"},
                {campaignId + "&"},
                {campaignId + "-!!@"},
                {campaignId + "bla bla"},
                {" " + campaignId + " "},
        };
    }

    /**
     * Check 500 error witn incorrect link
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21957">TA-21957</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "campaignProvider")
    public void checkRedirectWithInvalidCampaignIdTest(String campaignId) {
        log.info("Test is started");
        goToPlatform(campaignId);
        log.info("Test is finished");
    }

    /**
     * Check source block during the subSource blocking
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22047">TA-22047</a>
     * @see <a href="https://youtrack.mgid.com/issue/BT-1801">BT-1801</a>
     * <p> NIO </p>
     */
    @Test
    public void checkInfluenceOfBlockSubSourceOnSourceTest() {
        log.info("Test is started");
        int carrentActiveSources;
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/1010/search/1002");
        carrentActiveSources = pagesInit.getSelectiveBidding1().getAmountActiveSubSource();
        pagesInit.getSelectiveBidding1().blockUnblockSubSource("block");
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/1010/search/1002");
        softAssert.assertNotEquals(carrentActiveSources, pagesInit.getSelectiveBidding1().getAmountActiveSubSource(), "check active sources");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().isActiveSource(), "check source activity");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Block source with checking edits at cab
     * <p> NIO </p>
     */
    @Test
    public void checkWorkTumbler() {
        log.info("Test is started");
        String[] sources = new String[]{"1001"};
        goToPlatform("1122");
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/1122/search/1001");
        pagesInit.getSelectiveBidding1().blockUnblockSource("unblock");
        authCabAndGo("goodhits/campaigns-filters/filter/2/pid/1122");
        softAssert.assertTrue(pagesInit.getFilterBlockedPublishers().isMarkedWidgetInFilter(sources), "unblock");

        goToPlatform("1122");
        pagesInit.getSelectiveBidding1().blockUnblockSource("block");
        pagesInit.getSelectiveBidding1().excludeSpecificSubIds(true);
        authCabAndGo("goodhits/campaigns-filters/filter/2/pid/1122");
        softAssert.assertFalse(pagesInit.getFilterBlockedPublishers().isMarkedWidgetInFilter(sources), "block");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Change coefficient with check edits at cab. Mass action
     * <p> NIO </p>
     */
    @Test
    public void checkWorkCheckboxForCoefficient() {
        log.info("Test is started");
        goToPlatform("1121");
        pagesInit.getSelectiveBidding1().changeCoefficientSourceMass("1.8");
        authCabAndGo("goodhits/analysis-quality-platforms/id/1121?&widget_status=all");
        Assert.assertTrue(pagesInit.getFilterBlockedPublishers().checkCoefficientCab("1.8"));
        log.info("Test is finished");
    }

    /**
     * Check edit coefficient source and subSource through import file. Min and max range. Mass action
     *
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataCoefficientMassAction")
    public void checkMaxMinCoefficientMassAction(String typeSource, String koef_value, String id_company, String subSource1, String subSource2) {
        log.info("Test is started");
        goToPlatform(id_company);
        pagesInit.getSelectiveBidding1().importSources("sourcesCoefficient1.csv");
        goToPlatform(id_company);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkMaxMinCoefficientInMassActionInSubPlatform(koef_value, typeSource, subSource1, subSource2));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataCoefficientMassAction() {
        return new Object[][]{
                {"source", "10.0", "1015", "1988", "7"},
                {"source", "0.1", "1015", "1988", "7"},
                {"subSource", "0.1", "1016", "1988", "2"},
                {"subSource", "10.0", "1016", "1988", "2"},
        };
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Selective bidding interface")
    @Description("Import block list from file\n" +
            "<ul\n" +
            "    <li>Check successfully import</li>\n" +
            "    <li>Check message</li>\n" +
            "    <li>Check email</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52104\">Ticket TA-52104</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Import block list from file")
    public void checkBlockListImportSuccessfully() {
        log.info("Test is started");
        List<String> widgetsList = Arrays.asList("2001", "2002", "2003");
        String letterToManager = "Client 2001 added blocklist in campaign <a href='http://local-admin.mgid.com/cab/goodhits/campaigns/id/%s" +
                "' target='_blank'>%s</a>";
        String campaignId = "2026";
        String subjectLetter = "Blocklist imported";
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + campaignId);
        pagesInit.getSelectiveBidding1().importBlockList("blocklist.csv");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("BLOCKLIST SUCCESSFULLY IMPORTED"),
                "FAIL -> Success message!");
        softAssert.assertTrue(isEqualsLists(
                        pagesInit.getSelectiveBidding1().getWidgetsListFromPage(), widgetsList),
                "FAIL -> Widgets from page are not match with widgets from file!");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Blocked"),
                "FAIL -> Widgets from 'Block list' are not Blocked!");
        softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMail(subjectLetter).substring(0, 133),
                String.format(letterToManager, campaignId, campaignId),
                "FAIL - Check email to manager!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Selective bidding interface")
    @Description("Import block list from file\n" +
            "<ul\n" +
            "    <li>Check failed import</li>\n" +
            "    <li>Check messages</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52104\">Ticket TA-52104</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Import block list from file - validations.")
    public void checkBlockListImportWithWrongFiles() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/2026");
        pagesInit.getSelectiveBidding1().importBlockList("blocklist_incorrect.csv");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("WIDGETS WITH THESE UIDS DO NOT EXIST"),
                "FAIL -> Wrong widgets message for not exist UIDS!");
        pagesInit.getSelectiveBidding1().importBlockList("blocklist_empty.csv");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("AN EMPTY FILE IS NOT ALLOWED"),
                "FAIL -> Wrong widgets message for empty file!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Selective bidding interface")
    @Description("Check rights\n" +
            "<ul\n" +
            "    <li>Check that Import button is not displayed with disabled rights!</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52104\">Ticket TA-52104</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that Import button is not displayed with disabled rights!")
    @Privilege(name="default/advertisers/campaign-quality-analysis-import-blacklist")
    public void checkBlockListImportRights() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/campaign-quality-analysis-import-blacklist");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/2026");
            Assert.assertFalse(pagesInit.getSelectiveBidding1().checkImportBlockListIsDisplayed(),
                    "FAIL -> Import block list button is displayed with disabled privilege!");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/campaign-quality-analysis-import-blacklist");
        }
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Selective bidding interface")
    @Description("Check new push campaign\n" +
            "<ul\n" +
            "    <li>Check that Import button is not displayed for push v2!</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52104\">Ticket TA-52104</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that Import button is not displayed for new push campaign!")
    public void checkBlockListImportForNewPushCampaign() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/2025");
        Assert.assertFalse(pagesInit.getSelectiveBidding1().checkImportBlockListIsDisplayed(),
                "FAIL -> Import block list button is displayed for push v2 campaign!");
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Selective bidding interface")
    @Description("Check block list importing over existed widgets list\n" +
            "<ul\n" +
            "    <li>Check that previously added widgets gets disabled after import block list</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52104\">Ticket TA-52104</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check block list importing over existed widgets list!")
    public void checkBlockListImportForPreviouslyAddedWidget() {
        log.info("Test is started");
        List<String> widgetsList = Arrays.asList("2001", "2002", "2003");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/2027");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Active"),
                "FAIL -> Widgets from 'Block list' are not 'Active'!");
        pagesInit.getSelectiveBidding1().importBlockList("blocklist.csv");
        softAssert.assertTrue(isEqualsLists(widgetsList,
                        pagesInit.getSelectiveBidding1().getWidgetsListFromPage()),
                "FAIL -> Failed block list import!");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Blocked"),
                "FAIL -> Widgets from 'Block list' are not 'Blocked'!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Copy campaign in dash")
    @Description("Check copy block list when clone campaign\n" +
            "<ul\n" +
            "    <li>Check that block list copied to campaign recipient</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52086\">Ticket TA-52086</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that block list copied to campaign recipient!")
    public void checkCopyCampaignWithBlockList() {
        log.info("Test is started");
        List<String> widgetsList = Arrays.asList("2001", "2002", "2003");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers");
        pagesInit.getCampaigns().copyCampaignWithBlockList(2028);
        String campaignName = pagesInit.getCampaigns().getCampaignNameFromInput();
        pagesInit.getCampaigns().saveCampaign();
        authDashAndGo("advertisers");
        String campaignId = pagesInit.getCampaigns().getCampaignIdByName(campaignName);
        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId);
        softAssert.assertTrue(isEqualsLists(
                        pagesInit.getSelectiveBidding1().getWidgetsListFromPage(), widgetsList),
                "FAIL -> Widgets in recipient campaign are not match with widgets from donor campaign!");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Blocked"),
                "FAIL -> Widgets from 'Block list' are not Blocked!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Copy campaign in dash")
    @Description("Check clone campaign without block list\n" +
            "<ul\n" +
            "    <li>Check that block list is not copied to campaign recipient when checkbox is disabled</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52086\">Ticket TA-52086</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that block list is not copied to campaign recipient when checkbox is disabled!")
    public void checkCopyCampaignWithoutBlockList() {
        log.info("Test is started");
        List<String> widgetsList = Arrays.asList("2001", "2002", "2003");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers");
        pagesInit.getCampaigns().isNeedToCopyBlocklist(false);
        pagesInit.getCampaigns().copyCampaignWithBlockList(2028);
        String campaignName = pagesInit.getCampaigns().getCampaignNameFromInput();
        pagesInit.getCampaigns().saveCampaign();
        authDashAndGo("advertisers");
        String campaignId = pagesInit.getCampaigns().getCampaignIdByName(campaignName);
        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId);
        softAssert.assertFalse(isEqualsLists(
                        pagesInit.getSelectiveBidding1().getWidgetsListFromPage(), widgetsList),
                "FAIL -> Block list has copied from recipient campaign to donor campaign with disabled checkbox!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Black/white lists / UID's optimization")
    @Story("Blacklist. Copy campaign in dash")
    @Description("Check rights\n" +
            "<ul\n" +
            "    <li>Check that copy block list checkbox is not displayed with disabled rights</li>\n" +
            "    <li>@see <a href=\"https://jira.mgid.com/browse/TA-52086\">Ticket TA-52086</a></li>\n" +
            "    <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that copy block list checkbox is not displayed with disabled rights!")
    @Privilege(name="default/advertisers/campaign-quality-analysis-import-blacklist")
    public void checkBlockListCopyWithCampaignRights() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/can_copy_black_list_when_copy_campaign");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers");
            pagesInit.getCampaigns().clickCopyCampaignIcon(2028);
            Assert.assertFalse(pagesInit.getCampaigns().checkCopyBlockListCheckboxIsDisplayed(),
                    "FAIL -> Copy block list checkbox is displayed with disabled rights!");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/can_copy_black_list_when_copy_campaign");
        }
    }

    /**
     * Check edit coefficient source and subSource through import file. Not valid data. Inline
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataNotValidCoefficient")
    public void checkNotValidCoefficient(String typeSource, String koef_value, String id_company, String old_value) {
        log.info("Test is started");
        goToPlatform(id_company);
        pagesInit.getSelectiveBidding1().importSources("sourcesCoefficient1.csv");
        goToPlatform(id_company);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkNotValidCoefficientInSubPlatform(koef_value, old_value, typeSource));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataNotValidCoefficient() {
        return new Object[][]{
                {"source", "10.01", "1009", "10.0"},
                {"source", "0.0", "1009", "0.1"},
                {"subSource", "0.00", "1009", "0.1"},
                {"subSource", "10.1", "1009", "10.0"},
        };
    }
}
