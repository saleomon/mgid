package dashboard.mgid.advertisers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

public class MaxCpcTest extends TestBase {
    private final String clientLogin = "sok.autotest.payouts.ukraine@mgid.com";
    private final int campaignId = 3002;
    private final int teaserId = 3002;

    public void goToCreateTeaser(String clientLogin, int campaignId) {
        authDashAndGo(clientLogin,"advertisers/add-teaser-goods/campaign_id/" + campaignId);
    }

    public void goToGlobeInterface(String clientLogin, int teaserId) {
        authDashAndGo(clientLogin,"goods/settings/id/" + teaserId);
    }

    @Feature("Максимизация CPC в системе")
    @Story("Max CPC. Dashboard")
    @Description("Валидация установки максимального CPC в Дешборде.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности создания тизера для РК типа `push`</li>\n" +
            "      <li>Проверка валидации работает при установке рендомных значений в рамках конкретных ГЕО для тизера. В данном случае выбрано все ГЕО USA</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/BT-4065\">Ticket BT-4065</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (description = "Валидация установки CPC большего чем разрешено в рамках системи в Дешборде MGID")
    public void checkValidationMaxCpcInTeaserCreationInterfaceForRandomGeo() {
            log.info("Test is started");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "United States");
            goToCreateTeaser(clientLogin, campaignId);
            Assert.assertEquals(pagesInit.getTeaserClass()
                    .useDiscount(false)
                    .setDomain("https://test-t.com/testmaxcpc")
                    .useDescription(true)
                    .setDescription("Some descriptions")
                    .setPriceOfClick("400")
                    .createTeaser(), 0, "FAIL -> teaser doesn't create");
            log.info("Check error message max cpc validation");
        Assert.assertEquals(helpersInit.getMessageHelper().getMessageDash(),
                "THE PRICE FOR ONE OR MORE REGIONS IS HIGHER THAN ACCEPTABLE",
                "FAIL -> Price more than expected is accepted");
            log.info("Test is finished");
        }

}
