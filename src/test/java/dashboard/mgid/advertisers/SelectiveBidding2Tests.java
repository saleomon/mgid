package dashboard.mgid.advertisers;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.advertiser.locators.SelectiveBiddingLocators.COEFFICITNT_LABEL;
import static pages.dash.advertiser.variables.SelectiveBiddingVariables.campaignId;

public class SelectiveBidding2Tests extends TestBase {
    private final String login = "testemail1000@ex.ua";

    public void goToPlatform(String id_company) {
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + id_company);
        COEFFICITNT_LABEL.shouldBe(visible);
    }

    /**
     * Check 'search' by source and subsource
     * @see <a href="https://youtrack.mgid.com/issue/BT-735">BT-735</a>
     * <p> NIO </p>
     */
    @Test
    public void checkSearch() {
        log.info("Test is started");
        goToPlatform(campaignId);
        log.info("Search by sub source");
        pagesInit.getSelectiveBidding1().filterBySourceSubSource("1988");
        log.info("Check search result");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkDisplayedSource("1988"), "subSource");
        log.info("Search by source");
        pagesInit.getSelectiveBidding1().filterBySourceSubSource("1001");
        log.info("Check search result");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkDisplayedSubSource("1001"), "source");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check import sources with add and replace option
     * <p> NIO </p>
     */
    @Test
    public void checkImportPlatforms() {
        log.info("Test is started");
        String[] mass_platforms = new String[]{"2", "1", "1001", "1002"};
        String[] mass_koef_1 = new String[]{"1.20", "1.30", "1.40", "1.50"};
        String[] mass_koef_2 = new String[]{"2.10", "2.20", "2.30", "2.20"};

        goToPlatform(campaignId);
        pagesInit.getSelectiveBidding1().importSources("sourcesCoefficient1.csv");
        authCabAndGo("goodhits/analysis-quality-platforms/id/" + campaignId + "?widget_status=all");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkCoefficientInCab(mass_platforms, mass_koef_1), "mass_koef_1");
        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId);
        pagesInit.getSelectiveBidding1().importSources("sourceCoefficient2.csv");
        authCabAndGo("goodhits/analysis-quality-platforms/id/" + campaignId + "?widget_status=all");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkCoefficientInCab(mass_platforms, mass_koef_2), "mass_koef_2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check filter by status
     * <p> NIO </p>
     */
    @Test
    public void checkFiltersStatus() {
        log.info("Test is started");
        goToPlatform(campaignId);
        pagesInit.getSelectiveBidding1().filterByStatus("Active");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Active"), "Active");
        pagesInit.getSelectiveBidding1().filterByStatus("Blocked");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterStatus("Blocked"), "Blocked");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check filter by coefficient
     * <p> NIO </p>
     */
    @Test
    public void checkFiltersCoefficient() {
        log.info("Test is started");
        goToPlatform(campaignId);
        pagesInit.getSelectiveBidding1().filterByCoefficient("changed");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterCoefficient("changed"), "changed");
        pagesInit.getSelectiveBidding1().filterByCoefficient("initial");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkFilterCoefficient("initial"), "initial");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check edit coefficient source and subSource through import file. Min and max range
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataCoefficientImport")
    public void checkEditCoefficientInImport(String sourceType, String link, String value, String subSource) {
        String widgetId = "1001";
        log.info("Test is started - " + link);
        goToPlatform("1011");
        Assert.assertTrue(pagesInit.getSelectiveBidding1().editCoefficientInImport(link, value, widgetId, sourceType, subSource));
        log.info("Test is finished - " + link);
    }

    @DataProvider
    public Object[][] dataCoefficientImport() {
        return new Object[][]{
                {"source", "sourceCoefficient_min_range.csv", "0.1", "17"},
                {"source", "sourceCoefficient_max_range.csv", "10.0", "17"},
                {"subSource", "subSourceCoefficient_min_range.csv", "0.1", "17"},
                {"subSource", "subSourceCoefficient_max_range.csv", "10.0", "17"},
        };
    }

    /**
     * Check edit coefficient source and subSource through import file. Not valid values
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataCoefficientImportNotValid")
    public void checkEditCoefficientInImportNotValid(String sourceType, String link, String subSource) {
        log.info("Test is started - " + link);
        String widgetId = "1001";
        goToPlatform(campaignId);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().editSourceInImportWithNotValidCoefficient(link, widgetId, sourceType, subSource), "logic");
        Assert.assertEquals( helpersInit.getMessageHelper().getMessageDash(), "IT IS NOT ALLOWED TO DECREASE THE COEFFICIENT TO LESS THAN 0.1", "message");
        log.info("Test is finished - " + link);
    }

    @DataProvider
    public Object[][] dataCoefficientImportNotValid() {
        return new Object[][]{
                {"source", "sourceCoefficient_invalid_bottom.csv", "17"},
                {"source", "sourceCoefficient_invalid_top.csv", "17"},
                {"source", "sourceCoefficient_invalid_spec_symbols.csv", "17"},
                {"subSource", "subSourceCoefficient_invalid_bottom.csv", "17"},
                {"subSource", "subSourceCoefficient_invalid_top.csv", "17"},
                {"subSource", "subSourceCoefficient_invalid_spec_symbols.csv", "17"},
        };
    }

    /**
     * Check edit coefficient source and subSource through import file. Min and max range. Inline
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataCoefficient")
    public void checkMaxMinCoefficient(String typeSource, String koef_value, String id_company) {
        log.info("Test is started");
        goToPlatform(id_company);
        pagesInit.getSelectiveBidding1().importSources("sourcesCoefficient1.csv");
        goToPlatform(id_company);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkMaxMinCoefficientInSubPlatform(koef_value, typeSource));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataCoefficient() {
        return new Object[][]{
                {"source", "10.0", "1009"},
                {"source", "0.1", "1009"},
                {"subSource", "10.0", "1014"},
                {"subSource", "0.1", "1014"},
        };
    }

    /**
     * Check edit coefficient source and subSource through import file. Not valid data. Mass action
     * @see <a href="https://youtrack.dt00.net/issue/BT-446">BT-446</a>
     * <p> NIO </p>
     */
    @Test(dataProvider = "dataNotValidCoefficientMassAction")
    public void checkNotValidCoefficientMassAction(String typeSource, String koef_value, String old_value, String subSource1, String subSource2) {
        log.info("Test is started");
        goToPlatform(campaignId);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkNotValidCoefficientInMassActionInSubPlatform(koef_value, old_value, typeSource, subSource1, subSource2));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataNotValidCoefficientMassAction() {
        return new Object[][]{
                {"source", "10,01", "10.0", "16", "17"},
                {"source", "0,00", "0.1", "16", "17"},
                {"source", "s89%^&09()", "0.1", "16", "17"},
                {"subSource", "10,01", "10.0", "16", "17"},
                {"subSource", "0,00", "0.1", "16", "17"},
                {"subSource", "s89%^&09()", "0.1", "16", "17"}
        };
    }

    /**
     * Check block unblock sources. Mass action
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21997">TA-21997</a>
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockSourceWithMassAction() {
        log.info("Test is started");
        goToPlatform(campaignId);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkBlockUnblockSourcesInMassAction());
        log.info("Test is finished");
    }

    /**
     * Check block unblock subSources. Mass action
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21997">TA-21997</a>
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockSubSourceWithMassAction() {
        log.info("Test is started");
        goToPlatform(campaignId);
        Assert.assertTrue(pagesInit.getSelectiveBidding1().checkBlockUnblockSubSourcesInMassAction());
        log.info("Test is finished");
    }

    /**
     * Check block unblock sources with active subSource. Mass action
     * @see <a href="https://youtrack.dt00.net/issue/BT-796">BT-796</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21997">TA-21997</a>
     * <p> NIO </p>
     */
    @Test
    public void checkBlockUnblockSourceWithTurnOnSubSource() {
        log.info("Test is started");
        String startState;
        authDashAndGo(login, "advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1001");
        pagesInit.getSelectiveBidding1().blockUnblockSourceMassAction("block");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkblockUnblockSource("block"), "block");

        authDashAndGo("advertisers/campaign-quality-analysis/id/" + campaignId + "/search/1001");
        pagesInit.getSelectiveBidding1().blockUnblockSourceMassAction("unblock");
        softAssert.assertTrue(pagesInit.getSelectiveBidding1().checkblockUnblockSource("unblock"), "unblock");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
