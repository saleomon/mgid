package dashboard.mgid.advertisers.campaign;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.List;

import static pages.dash.advertiser.variables.AddEditCampaignVariables.keyword;
import static pages.dash.advertiser.variables.AddEditCampaignVariables.messageUaRotation;
import static core.helpers.BaseHelper.randomNumbersInt;
import static core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods.YESTERDAY;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

public class Campaign2Tests extends TestBase {
    public Campaign2Tests() {
        subnetId = SCENARIO_MGID;
    }

    /**
     * create push duplicate campaign
     *
     * <p>NIO</p>
     */
    @Test
    public void createCampaignWithPushDuplicate() {
        log.info("Test is started");
        authDashAndGo("advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test duplicate " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setUsePushDuplicate(true)
                .setSubnetType(SCENARIO_MGID)
                .setUseTargeting(true)
                .setTargetingOption("location", "os", "browser", "browserLanguage", "audience")
                .createCampaignWithId();
        String campaignDuplicateName = pagesInit.getCampaigns().getCampaignName() + " push";
        pagesInit.getCampaigns().setCampaignUtm(campaignDuplicateName.replace(" ", "+"))
                .setCustomUtm("mgclid={click_id}&" + pagesInit.getCampaigns().getCustomUtm());
        pagesInit.getCampaigns().getCampaignIdFromListByName(pagesInit.getCampaigns().getCampaignName() + " push");
        log.info("Check campaign after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "FAIL -Check campaign after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check displaying tab and switcher button for google tag manager
     * <p>NIO</p>
     */
    @Test
    public void checkGoogleTagManagerConversion() {
        log.info("Test is started");
        authDashAndGo("advertisers/edit/campaign_id/1003");
        log.info("Create campaign");
        pagesInit.getCampaigns().setGoogleTagManagerConversion();
        authDashAndGo("advertisers/edit/campaign_id/1003");
        Assert.assertTrue(pagesInit.getCampaigns().checkSavedConversion("manager"), "FAIL -Check campaign after creating");
        log.info("Test is finished");
    }

    /**
     * Check displaying tab and switcher button for google analytics
     * <p>NIO</p>
     */
    @Test
    public void checkGoogleAnalyticsConversion() {
        log.info("Test is started");
        authDashAndGo("advertisers/edit/campaign_id/1003");
        log.info("Create campaign");
        pagesInit.getCampaigns().setGoogleAnalyticsConversion();
        Assert.assertTrue(pagesInit.getCampaigns().checkSavedConversion("analytics"), "FAIL -Check campaign after creating");
        log.info("Test is finished");
    }

    /**
     * <p>Ограничение при редактировании РК на гео включая Bьетнам с категориями
     * "Currencies", "Options", "Stocks and Bonds", "Casino and gambling"</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27204">Ticket TA-27204</a>
     * <p>NIO</p>
     */
    @Test
    public void checkCreateCampaignWithGeoVietnamAndForbiddenCategories() {
        log.info("Test is started");
        String[] categories =  {"254", "149", "148", "105"};
        String category = categories[randomNumbersInt(categories.length)];
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setTargetingSwitcher(true)
                .setCampaignType("product")
                .setSubnetType(SCENARIO_MGID)
                .setCampaignCategory(category)
                .selectLocationTarget("exclude", "Italy")
                .fillFirstBlockCreateCampaign();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Category is not available for the selected region"));
        log.info("Test is finished");
    }

    /**
     * <p>Ограничение при редактировании РК на гео включая Bьетнам с категориями
     * "Currencies", "Options", "Stocks and Bonds", "Casino and gambling"</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27204">Ticket TA-27204</a>
     * <p>NIO</p>
     */
    @Test
    public void checkEditCampaignWithGeoVietnamAndForbiddenCategories() {
        log.info("Test is started");
        String[] categories = {"254", "149", "148", "105"};
        String category = categories[randomNumbersInt(categories.length)];
        authDashAndGo("advertisers/edit/campaign_id/1047");
        log.info("Edit campaign");
        pagesInit.getCampaigns().setIsNeedToEdit(false).clearAllSettings()
                .setCampaignCategory(category)
                .setBlockBeforeShow(false)
                .setIsEnableUtms(false)
                .setIsUseSchedule(false)
                .editCampaign();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Category is not available for the selected region"));
        log.info("Test is finished");
    }

    /**
     * <p>Добавление keyword для кампаний типа Search Feed в cab/dash</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51738">Ticket TA-51738</a>
     * <p>NIO</p>
     */
    @Test
    public void checkDisableKeywordField() {
        log.info("Test is started");
        authDashAndGo("advertisers/edit/campaign_id/1105");
        log.info("Check field on disabling");
        Assert.assertTrue(pagesInit.getCampaigns().isDisableKeywordField());
        log.info("Test is finished");
    }

    /**
     * <p>Добавление keyword для кампаний типа Search Feed в cab/dash</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51738">Ticket TA-51738</a>
     * <p>NIO</p>
     */
    @Test
    public void checkMaxAmountOfSymbolsForKeyword() {
        log.info("Test is started");
        authDashAndGo("advertisers/edit/campaign_id/1106");
        log.info("Check field on disabling");
        pagesInit.getCampaigns().setIsNeedToEdit(false)
                .setCampaignKeyword(keyword)
                .setIsEnableUtms(false)
                .setLimitType(null)
                .setIsUseSchedule(false)
                .editCampaign();

        softAssert.assertTrue(pagesInit.getCampaigns().isDisplayedCampaign("Search Ads"), "campaign isn't displayed");

        authDashAndGo("advertisers/edit/campaign_id/1106");
        softAssert.assertNotEquals(pagesInit.getCampaigns().getKeyword(), keyword, "FAIL - assertNotEquals");
        softAssert.assertTrue(keyword.contains(pagesInit.getCampaigns().getKeyword()),"FAIL - assertTrue");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Reach meter metamorphosis for "Traffic type" items excluding
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51822">Ticket TA-51822</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkReachMeterForTrafficTypeTargetingExclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");

        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 950");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 94.2857%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 800");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 91.5714%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 650");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 88.8571%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 500");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 86.1429%;");

        log.info("Test is finished");
    }

    /**
     * Check Reach meter metamorphosis for "Traffic type" items including
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51822">Ticket TA-51822</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkReachMeterForTrafficTypeTargetingInclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");

        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("150");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 40.3571%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("300");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 49.4286%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("450");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 55.1429%;");
        pagesInit.getCampaigns().selectTrafficTypeTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("600");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 60.8571%;");

        log.info("Test is finished");
    }

    @Epic("Reachmeter")
    @Feature("Reachmeter for Interests targeting")
    @Story("Dashboard. Creating campaign form")
    @Description("Check reachmeter is reacts on targeting by Interests\n" +
            "<ul>\n" +
            "   <li>Exclude targeting</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52293\">Ticket TA-52293</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check reachmeter is reacts on targeting by Interests exclude")
    public void checkReachMeterForInterestsTargetingExclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectInterestsTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 950");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 94.2857%;");
        log.info("Test is finished");
    }

    @Epic("Reachmeter")
    @Feature("Reachmeter for Interests targeting")
    @Story("Dashboard. Creating campaign form")
    @Description("Check reachmeter is reacts on targeting by Interests\n" +
            "<ul>\n" +
            "   <li>Include targeting</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52293\">Ticket TA-52293</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check reachmeter is reacts on targeting by Interests include")
    public void checkReachMeterForInterestsTargetingInclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectInterestsTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("150");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 40.3571%;");
        log.info("Test is finished");
    }

    @Epic("Reachmeter")
    @Feature("Reachmeter for Push traffic")
    @Story("Reachmeter. Targeting for push")
    @Description("Check reachmeter is reacts on targeting by Phone price ranges\n" +
            "<ul>\n" +
            "   <li>Include targeting</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52002\">Ticket TA-52002</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check reachmeter is reacts on targeting by Phone price ranges include")
    public void checkReachMeterForPhonePriceTargetingIncludeInPushCampaign() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns().chooseCampaignType("push");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("150");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 40.3571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("300");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 49.4286%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("450");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 55.1429%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("600");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 60.8571%;");

        log.info("Test is finished");
    }

    @Epic("Reachmeter")
    @Feature("Reachmeter for Push traffic")
    @Story("Reachmeter. Targeting for push")
    @Description("Check reachmeter is reacts on targeting by Phone price ranges\n" +
            "<ul>\n" +
            "   <li>Exclude targeting</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52002\">Ticket TA-52002</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check reachmeter is reacts on targeting by Phone price ranges exclude")
    public void checkReachMeterForPhonePriceTargetingExcludeInPushCampaign() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");

        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 950");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 94.2857%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 800");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 91.5714%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 650");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 88.8571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 500");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 86.1429%;");

        log.info("Test is finished");
    }

    /**
     * Check location targeting by Cities
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51845">Ticket TA-51845</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkTargetingByCities() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns()
                .setBlockBeforeShow(false)
                .setTargetingSwitcher(false)
                .setIsEnabledSensors(false)
                .setSubnetType(SCENARIO_MGID)
                .createCampaignWithCityTargeting();

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCampaigns().checkSelectedCityTargets(),
                "Check location targeting by cities");

        log.info("Test is finished");
    }

    @Story("Валидация макросов")
    @Description("Валидация макросов {gdpr} и {gdpr_consent}\n" +
            "<ul>\n" +
            "   <li>при создании кампании</li>\n" +
            "   <li>при редактировании кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51808\">Ticket TA-51808</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактировании кампании с макросами {gdpr} и {gdpr_consent}")
    public void checkCreateCampaignWithGdprMacros() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Сampaign with GDPR macros " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setSubnetType(SCENARIO_MGID)
                .setTargetingOption("location")
                .setCustomUtm("gdpr={gdpr}&gdpr_consent={gdpr_consent}")
                .setIsEnabledSensors(false)
                .createCampaignWithId();

        log.info("Check campaign tags after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkUtms(),
                "FAIL -> Check UTM macroses {gdpr} и {gdpr_consent} after creating!");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Сampaign with GDPR macros " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setSubnetType(SCENARIO_MGID)
                .setTargetingOption("location")
                .setCampaignUtm("pmac")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .setCustomUtm("gdpr_consent={gdpr_consent}&gdpr={gdpr}")
                .setIsEnabledSensors(false)
                .editCampaign();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkUtms(),
                "FAIL -> Check UTM macroses {gdpr} и {gdpr_consent} after editing!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка контекстного таргетинга при:\n" +
            "<ul>\n" +
            "   <li>создании продуктовой кампании</li>\n" +
            "   <li>редактировании продуктовой кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51893\">Ticket TA-51893</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    //todo AIA Flaky test, need to investigate
    // @Test(description = "Создание и редактировании продуктовой кампании с блоком контекстного таргетинга")
    public void createCampaignWithContextTargetingInDash() {
        log.info("Test is started");
        String languageForContext = helpersInit.getBaseHelper().getRandomFromList(
                operationMySql.getGlobalSettings().getContextTargetingLanguageIds());

        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp with context " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setCampaignLanguage(languageForContext)
                .setSubnetType(SCENARIO_MGID)
                .setTargetingOption("context", "sentiments")
                .setIsEnabledSensors(false)
                .createCampaignWithId();

        log.info("Check campaign after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelectedTargets(),
                "FAIL -> Check context targeting");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setUseTargeting(true)
                .setTargetingOption("context", "sentiments")
                .setIsUseSchedule(false)
                .setLimitType("clicks_limits")
                .setCampaignUtm("pmac")
                .setCustomUtm("geo={geo_region}")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelectedTargets(),
                "Check campaign after editing");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка отсутствия блока контекстного таргетинга для языков не из списка:\n" +
            "<ul>\n" +
            "   <li>Английский\n" +
            "   <li>Русский\n" +
            "   <li>Итальянский\n" +
            "   <li>Украинский\n" +
            "   <li>Индонезийский\n" +
            "   <li>Тайский\n" +
            "   <li>Вьетнамский\n" +
            "   <li>Испанский</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51893\">Ticket TA-51893</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание кампании с языком, для которого недоступен контекстный таргетинг",
            dataProvider = "campaignTypes")
    public void checkCreatingCampaignForLanguagesWithoutContextTargeting(String campaignType) {
        log.info("Test is started");
        List<String> languagesForContext = operationMySql.getGlobalSettings().getContextTargetingLanguageIds();

        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns()
                .chooseCampaignType(campaignType)
                .chooseCampaignLanguage(pagesInit.getCampaigns().chooseLanguageWithoutContext(languagesForContext));

        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedContextTargetingTab(),
                "FAIL -> Context targeting is present for language - "
                        + pagesInit.getCampaigns().getCampaignLanguage()
                        + " in campaign type - "
                        + campaignType + "!");
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedSentimentsTargetingTab(),
                "FAIL -> Sentiments targeting is present for language - "
                        + pagesInit.getCampaigns().getCampaignLanguage()
                        + " in campaign type - "
                        + campaignType + "!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypes() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"search_feed"}
        };
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка блока контекстного таргетинга в кампаниях разных типов")
    @Test(description = "Проверка контекстного таргетинга в кампаниях разных типов", dataProvider = "campaignTypes")
    public void checkCreatingCampaignsForLanguagesWithContextTargeting(String campaignType) {
        log.info("Test is started");
        String languageForContext = helpersInit.getBaseHelper().getRandomFromList(
                operationMySql.getGlobalSettings().getContextTargetingLanguageIds());

        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns()
                .chooseCampaignType(campaignType)
                .chooseCampaignLanguage(languageForContext);

        softAssert.assertTrue(pagesInit.getCampaigns().isDisplayedContextTargetingTab(),
                "FAIL -> Context targeting is not present for language - "
                        + pagesInit.getCampaigns().getCampaignLanguage()
                        + " in campaign type - " + campaignType + "!");
        softAssert.assertTrue(pagesInit.getCampaigns().isDisplayedSentimentsTargetingTab(),
                "FAIL -> Sentiments targeting is not present for language - "
                        + pagesInit.getCampaigns().getCampaignLanguage()
                        + " in campaign type - " + campaignType + "!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Отключение возможности запуска пуш рк для новых самозарегов")
    @Description("Проверка отсутствия возможности создания пуш рк для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка отсутствия возможности создания пуш рк для новых самозарегов")
    public void checkAbsencePushCampaignForSelfRegisterClients() {
        log.info("Test is started");
        authDashAndGo("sok.self-reg@mgid.com", "advertisers/add");

        log.info("Check available campaign types");
        Assert.assertEquals(pagesInit.getCampaigns().checkCampaignTypeIsPresent("push"), 0,
                "FAIL -> Push campaign is available for self register client!");

        log.info("Test is finished");
    }

    @Story("Отключение возможности запуска пуш рк для новых самозарегов")
    @Description("Проверка отсутствия возможности создания дубля пуш рк при создании продуктовой кампании для новых " +
            "самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка отсутствия возможности создания пуш рк для новых самозарегов")
    public void checkAbsenceDuplicatePushCampaignForSelfRegisterClients() {
        log.info("Test is started");
        authDashAndGo("sok.self-reg@mgid.com", "advertisers/add");

        log.info("Create product campaign");
        pagesInit.getCampaigns().fillCampaignName("Check Duplicate Push Campaign")
                .chooseCampaignType("product")
                .chooseCampaignLanguage("1")
                .chooseCampaignCategory("100")
                .confirmCampaignGeneralSettings();

        Assert.assertFalse(pagesInit.getCampaigns().checkDuplicatePushCampaignIsDisplayed(),
                "FAIL -> Duplicate Push campaign is available for self register client!");

        log.info("Test is finished");
    }

    @Story("Флаг управления созданием пуш-рк для НКС")
    @Description("Проверка возможности создания пуш рк для новых самозарегов с разрешенным флагом\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка возможности создания пуш рк для новых самозарегов с разрешенным флагом")
    public void checkCanCreatePushCampaignForSelfRegisterClientsWithFlag() {
        log.info("Test is started");
        authDashAndGo("self-reg-for-push-with-1-dollar-spend@mgid.com", "advertisers/add");

            log.info("Check available campaign types");
            Assert.assertEquals(pagesInit.getCampaigns().checkCampaignTypeIsPresent("push"), 1,
                    "FAIL -> Push campaign is available for self register client!");

        log.info("Test is finished");
    }

    @Story("Ограничение дневных спендов для новых саморегов")
    @Description("Проверка остановки кампании после достижения лимита трат в $500 для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51890\">Ticket TA-51890</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка остановки кампании после достижения лимита трат в $500 для новых самозарегов")
    public void checkBlockedCampaignsAfterLimitOfDaySpendingForSelfRegisteredClients() {
        log.info("Test is started");
        authDashAndGo("self-reg-for-check-limit@mgid.com", "advertisers");
        softAssert.assertEquals(pagesInit.getCampaigns().getCampaignStatus(), "Active",
                "FAIL -> Campaign STATUS by limit for self registered client!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkLimitIconIsDisplayed(),
                "FAIL -> Campaign ICON by limit for self registered client!");
        softAssert.assertTrue(pagesInit.getCampaigns().getCampaignStatusTitle().contains(
                "Account has reached its daily limit. Please contact your manager to change the limit. The campaign has been paused since "),
                "FAIL -> Campaign TITLE by limit for self registered client!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Ограничение дневных спендов для новых саморегов")
    @Description("Проверка статуса кампании до достижения лимита трат в $500 для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51890\">Ticket TA-51890</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка статуса кампании до достижения лимита трат в $500 для новых самозарегов")
    public void checkCampaignsStatusBeforeLimitOfDaySpendingForSelfRegisteredClients() {
        log.info("Test is started");
        authDashAndGo("self-reg-before-day-limit@mgid.com", "advertisers");
        softAssert.assertEquals(pagesInit.getCampaigns().getCampaignStatus(), "Active",
                "FAIL -> Campaign STATUS before limit for self registered client!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkLimitIconIsDisplayed(),
                "FAIL -> Campaign ICON before limit for self registered client!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление таргета на стоимость телефонов")
    @Description("Проверка права для отображения таргета на ценовой диапазон устройств при создании кампании</li>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51876\">Ticket TA-51876</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    //todo AIA need to discuss checking privileges in dash @Test(description = "Проверка права для отображения таргетинга на ценовой диапазон устройств при создании кампании")
    public void checkPhonesPricesTargetingInDashPrivilege() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/can_use_phones_price_range_targeting");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
            Assert.assertFalse(pagesInit.getCampaigns().checkPhonePriceRangeTargetingIsDisplayed(),
                    "FAIL -> Phone price range is displayed with disabled privilege!");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/can_use_phones_price_range_targeting");
        }
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Реакция ричметра на добавление ценовой категории девайса в таргеты")
    @Description("Проверка отрисовки ричметра при исключении диапазона цен из таргетинга\n" +
                "<ul>\n" +
                "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51878\">Ticket TA-51878</a></li>\n" +
                "   <li><p>Author AIA</p></li>\n" +
                "<ul>\n")
    @Test(description = "Проверка отрисовки ричметра при исключении диапазона цен из таргетинга")
    public void checkReachMeterForPhonePricesTargetingExclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");

        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 950");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 94.2857%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 800");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 91.5714%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 650");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 88.8571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 500");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 86.1429%;");

        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Реакция ричметра на добавление ценовой категории девайса в таргеты")
    @Description("Проверка отрисовки ричметра при включении диапазона цен в таргетинг\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51878\">Ticket TA-51878</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Проверка отрисовки ричметра при включении диапазона цен в таргетинг")
    public void checkReachMeterForPhonePricesTargetingInclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("150");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 40.3571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("300");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 49.4286%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("450");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 55.1429%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("600");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 60.8571%;");

        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for campaigns list\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for campaigns list")
    public void checkReachMetricsForCampaignsListStatistics() {
        log.info("Test is started");
        int campaignId = 2013;
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers");
        pagesInit.getCampaigns().selectPeriodInCalendar(YESTERDAY);
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetrics("CPM, $"),
                "FAIL -> Table doesn't contain 'CPM, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsValues(campaignId, 10, "1.00"),
                "FAIL -> CPM should be calculated by formula  CPM = (1000*Spend)/Impressions where Impressions is ad_requests!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetrics("CTR, %"),
                "FAIL -> Table doesn't contain 'CTR, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsValues(campaignId, 11, "0.67"),
                "FAIL -> CTR should be calculated by formula  CTR = 100%*Clicks/Impressions where Impressions is ad_requests!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetrics("vCTR"),
                "FAIL -> Table still contains 'vCTR' column for Reach type stat!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetrics("vCPM"),
                "FAIL -> Table still contains 'vCPM' column for Reach type stat!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for campaigns list in exported file.\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for campaigns list in exported file.")
    public void checkReachMetricsForCampaignsListExport() {
        log.info("Test is started");
        try {
            String[] headers = {"Information", "Status", "Impressions", "Viewable impressions", "Viewability, %", "Clicks",
                    "Spent, $", "Avg. price per click, ¢", "Teasers", "CPM, $", "CTR, %", "Reach", "CPR, $", "Frequency"};

            authDashAndGo("client-with-reach-stat@mgid.com", "advertisers");
            pagesInit.getCampaigns().selectPeriodInCalendar(YESTERDAY);

            softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                            .setCustomRowNumber(1)
                            .loadExportedFileDash(),
                    "FAIL -> There is some error in exported file!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                    "FAIL -> Some of column headers have incorrect names!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "0.67"),
                    "FAIL -> 'CTR' value is incorrect!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPM, $", "1.0"),
                    "FAIL -> 'CPM' value is incorrect!");
            softAssert.assertAll();
        } finally {
            serviceInit.getExportFileTableStructure().closeAllFile();
        }
        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for daily campaigns statistic\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for daily campaigns statistic")
    public void checkReachMetricsForDailyCampaignStatistics() {
        log.info("Test is started");
        int campaignId = 2013;
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/daily-stat/type/goods/id/" + campaignId);
        pagesInit.getCampaigns().selectPeriodInCalendar(YESTERDAY);
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStat("CPM, $"),
                "FAIL -> Table doesn't contain 'CPM, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStatValues(8, "1.00"),
                "FAIL -> CPM should be calculated by formula  CPM = (1000*Spend)/Impressions where Impressions is ad_requests!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStat("CTR, %"),
                "FAIL -> Table doesn't contain 'CTR, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStatValues(7, "0.67"),
                "FAIL -> CTR should be calculated by formula CTR = 100%*Clicks/Impressions where Impressions is ad_requests!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStat("vCTR"),
                "FAIL -> Table still contains 'vCTR' column for Reach type stat!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetricsInDailyCampaignStat("vCPM"),
                "FAIL -> Table still contains 'vCPM' column for Reach type stat!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for daily campaigns statistic in exported file.\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for daily campaigns statistic in exported file.")
    public void checkReachMetricsForCampaignsDailyStatExport() {
        log.info("Test is started");
        try {
            String[] headers = {"Date",	"Impressions", "Viewable impressions", "Viewability, %", "Clicks", "Spent, $",
                    "Av. CPC, ¢", "CTR, %", "CPM, $", "Reach", "CPR, $", "Frequency"};

            int campaignId = 2013;
            authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/daily-stat/type/goods/id/" + campaignId);
            pagesInit.getCampaigns().selectPeriodInCalendar(YESTERDAY);
            softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                            .setCustomRowNumber(1)
                            .loadExportedFileDash(),
                    "FAIL -> There is some error in exported file!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                    "FAIL -> Some of column headers have incorrect names!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "0.67"),
                    "FAIL -> 'CTR' value is incorrect!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPM, $", "1.0"),
                    "FAIL -> 'CPM' value is incorrect!");
            softAssert.assertAll();
        } finally {
            serviceInit.getExportFileTableStructure().closeAllFile();
        }
        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for teasers list\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for teasers list")
    public void checkReachMetricsForTeasersListStatistics() {
        log.info("Test is started");
        int campaignId = 2013;
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/teasers-goods/campaign_id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInTeasersListStat("CPM, $"),
                "FAIL -> Table doesn't contain 'CPM, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInTeasersListStatValues(10, "1.00"),
                "FAIL -> CPM should be calculated by formula  CPM = (1000*Spend)/Impressions where Impressions is ad_requests!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInTeasersListStat("CTR, %"),
                "FAIL -> Table doesn't contain 'CTR, %' column for Reach type stat!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkReachMetricsInTeasersListStatValues(14, "0.667"),
                "FAIL -> CTR should be calculated by formula  CTR = 100%*Clicks/Impressions where Impressions is ad_requests!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetricsInTeasersListStat("vCTR"),
                "FAIL -> Table still contains 'vCTR' column for Reach type stat!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkReachMetricsInTeasersListStat("vCPM"),
                "FAIL -> Table still contains 'vCPM' column for Reach type stat!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Изменение метрики vCPM на CPM для CPM рк")
    @Feature("Завершение задачи по выводу метрик для CPM рк")
    @Story("Story to BT-4557 vCPM to CPM for CPM-campaign")
    @Story("Story to WT-933 | изменение метрики vCTR для CPM кампаний")
    @Description("Checking reach metrics for teasers list in exported file.\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51961\">Ticket TA-51961</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51962\">Ticket TA-51962</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Checking reach metrics for teasers list in exported file.")
    public void checkReachMetricsForTeasersListExport() {
        log.info("Test is started");
        try {
            String[] headers = {"ID", "Status", "Title", "Description", "Call to Action", "Category", "CPC",
                    "CTR, %", "Impressions", "Viewable impressions today", "Viewable impressions yesterday",
                    "Viewable impressions total", "Viewability, %", "Clicks today", "Clicks yesterday", "Clicks total",
                    "Spent today", "Spent yesterday", "Spent total", "CPM, $", "Reach",	"CPR, $", "Frequency"};

            int teaserId = 2005;
            int campaignId = 2013;
            authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);

            softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                            .setCustomRowNumber(1)
                            .loadExportedFileDash(),
                    "FAIL -> There is some error in exported file!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                    "FAIL -> Some of column headers have incorrect names!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "0.67"),
                    "FAIL -> 'CTR' value is incorrect!");
            softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPM, $", "1.0"),
                    "FAIL -> 'CPM' value is incorrect!");
            softAssert.assertAll();
        } finally {
            serviceInit.getExportFileTableStructure().closeAllFile();
        }
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Dash")
    @Description("Check message at create interface:" +
            "<ul>\n" +
            "   <li>Language != Ukraine, choose targeting Ukraine</li>\n" +
            "   <li>Targeting == Ukraine, choose language  not Ukrainian</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51948\">Ticket TA-51948</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "check message at create interface")
    public void checkMessageAtCreateInterface() {
        log.info("Test is started");
        log.info("Create campaign");
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        pagesInit.getCampaigns()
                .chooseCampaignLanguage("2");

        log.info("Check message language != Ukraine, targeting all");
        softAssert.assertTrue(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - first check");
        log.info("Check message language != Ukraine, targeting include Ukraine");
        pagesInit.getCampaigns()
                .chooseCampaignLanguage("2")
                .selectLocationTarget("include", "Ukraine");
        softAssert.assertTrue(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - second check");

        log.info("Check targeting include Ukraine, message language != Ukraine");
        authDashAndGo("advertisers/add");
        pagesInit.getCampaigns()
                .selectLocationTarget("include", "Ukraine");
        pagesInit.getCampaigns().chooseCampaignLanguage("2");
        softAssert.assertTrue(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - third check");

        log.info("Check message language != Ukraine, targeting exclude Ukraine");
        authDashAndGo("advertisers/add");
        pagesInit.getCampaigns().chooseCampaignLanguage("2").selectLocationTarget("exclude", "Ukraine");
        softAssert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - fourth check");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
