package dashboard.mgid.advertisers.campaign;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static pages.dash.advertiser.variables.AddEditCampaignVariables.messageUaRotation;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

public class Campaign1Tests extends TestBase {
    public Campaign1Tests() {
        subnetId = SCENARIO_MGID;
    }

    /**
     * Create/check/edit/check/delete Product company
     * Check that 'Rotate in Adskeeper network' checkbox is enabled by default after campaign creating and editing
     * <p>NIO</p>
     */
    @Test
    public void createProductCampaign() {
        log.info("Test is started");
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setTargetingSwitcher(false)
                .setSubnetType(SCENARIO_MGID)
                .createCampaignSimple();

        log.info("Check campaign after creation");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign editing!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "Check campaign after creating");

        log.info("Check shows only for this language value");
        softAssert.assertNotEquals(operationMySql.getgPartners1().selectShowsOnlyForThisLanguageValue(pagesInit.getCampaigns().getCampaignId()), 1, "FAIL - show only for this language");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(true)
                .setTargetingSwitcher(false)
                .setDynamicRetargeting(false)
                .setIsEnabledSensors(true)
                .setTargetingOption("os")
                .setLimitType("clicks_limits")
                .setIsUseSchedule(true)
                .setIsEnableUtms(true)
                .setCampaignUtm("pmac")
                .setCustomUtm("geo={geo_region}")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign editing!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterEditing(), "Check campaign after editing");

        log.info("Delete campaign");
        authDashAndGo("advertisers");
        pagesInit.getCampaigns().deleteCampaign(pagesInit.getCampaigns().getCampaignId());
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedRestoreCampaign(pagesInit.getCampaigns().getCampaignName()), "Delete campaign");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Create/check/edit/check/delete Content company
     * Check that 'Rotate in Adskeeper network' checkbox is enabled by default after campaign creating
     * <p>NIO</p>
     */
    @Test
    public void createContentCampaign() {
        log.info("Test is started");
        authDashAndGo("advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create content " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setCampaignType("content")
                .setSubnetType(SCENARIO_MGID)
                .setTargetingSwitcher(false)
                .createCampaignSimple();

        log.info("Check campaign after creation");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign creating!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "Check campaign after creating");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(true)
                .setTargetingSwitcher(false)
                .setDynamicRetargeting(false)
                .setIsEnabledSensors(true)
                .setLimitType("clicks_limits")
                .setIsUseSchedule(true)
                .setIsEnableUtms(true)
                .setTargetingOption("os")
                .setCampaignUtm("pmac")
                .setCustomUtm("tsuc")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign editing!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterEditing(), "Check campaign after editing");

        log.info("Delete campaign");
        authDashAndGo("advertisers");
        pagesInit.getCampaigns().deleteCampaign(pagesInit.getCampaigns().getCampaignId());
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedRestoreCampaign(pagesInit.getCampaigns().getCampaignName()), "Delete campaign");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Create/check/edit/check/delete Push company
     * Check that 'Rotate in Adskeeper network' checkbox is enabled by default after campaign creating
     * <p>NIO</p>
     */
    @Test
    public void createPushCampaign() {
        log.info("Test is started");
        authDashAndGo("advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create content " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setCampaignType("push")
                .setSubnetType(SCENARIO_MGID)
                .setTargetingSwitcher(false)
                .createCampaignSimple();

        log.info("Check campaign after creation");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign editing!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "Check campaign after creating");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(true)
                .setTargetingSwitcher(false)
                .setDynamicRetargeting(false)
                .setIsEnabledSensors(true)
                .setLimitType("clicks_limits")
                .setIsUseSchedule(true)
                .setTargetingOption("location")
                .setCampaignUtm("pmac")
                .setCustomUtm("tsuc")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateAdskeeperSubnet(true),
                "FAIL -> Check Rotate in Adskeeper network after campaign editing!");

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterEditing(), "Check campaign after editing");

        log.info("Delete campaign");
        authDashAndGo("advertisers");
        pagesInit.getCampaigns().deleteCampaign(pagesInit.getCampaigns().getCampaignId());
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedRestoreCampaign(pagesInit.getCampaigns().getCampaignName()), "Delete campaign");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Create/check/edit/check/delete Search feed company
     * <p>NIO</p>
     */
    @Test
    public void createSearchFeedCampaign() {
        log.info("Test is started");
        authDashAndGo("advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create content " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setSubnetType(SCENARIO_MGID)
                .setCampaignType("search_feed")
                .setCampaignKeyword("crazy baboon и ворчун alert('Executing JS') '-prompt()-'")
                .setTargetingSwitcher(false)
                .createCampaignSimple();

        log.info("Check campaign after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterCreating(), "FAIL -Check campaign after creating");

        log.info("Check shows only for this language value");
        softAssert.assertEquals(operationMySql.getgPartners1().selectShowsOnlyForThisLanguageValue(pagesInit.getCampaigns().getCampaignId()), 1, "FAIL - show only for this language");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(true)
                .setTargetingSwitcher(false)
                .setDynamicRetargeting(false)
                .setTargetingOption("browser")
                .setIsEnabledSensors(true)
                .setLimitType("clicks_limits")
                .setIsUseSchedule(true)
                .setIsEnableUtms(true)
                .setCampaignUtm("pmac")
                .setCustomUtm("tsuc")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkCampaignSettingsAfterEditing(), "FAIL - Check campaign after editing");

        log.info("Delete campaign");
        authDashAndGo("advertisers");
        pagesInit.getCampaigns().deleteCampaign(pagesInit.getCampaigns().getCampaignId());
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedRestoreCampaign(pagesInit.getCampaigns().getCampaignName()), "FAIL - Delete campaign");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Story("Ограничение дневных спендов для новых саморегов")
    @Description("Проверка статуса кампании до достижения лимита трат в $500 для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51890\">Ticket TA-51890</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка статуса кампании до достижения лимита трат в $500 для новых самозарегов")
    public void checkCampaignsStatusBeforeLimitOfDaySpendingForSelfRegisteredClients() {
        log.info("Test is started");
        authDashAndGo("self-reg-before-day-limit@mgid.com", "advertisers");
        softAssert.assertEquals(pagesInit.getCampaigns().getCampaignStatus(), "Active",
                "FAIL -> Campaign STATUS before limit for self registered client!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkLimitIconIsDisplayed(),
                "FAIL -> Campaign ICON before limit for self registered client!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление таргета на ценовой диапазон устройств")
    @Description("Проверка таргета на ценовой диапазон устройств при:\n" +
            "<ul>\n" +
            "   <li>создании продуктовой кампании</li>\n" +
            "   <li>редактировании продуктовой кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51876\">Ticket TA-51876</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактировании продуктовой кампании с блоком таргетинга на ценовой диапазон телефонов")
    public void createCampaignWithPhonesPricesTargetingInDash() {
        log.info("Test is started");

        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test campaign " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setBlockBeforeShow(false)
                .setSubnetType(SCENARIO_MGID)
                .setTargetingOption("phonePriceRange")
                .setIsEnabledSensors(false)
                .createCampaignWithId();

        log.info("Check campaign after creation");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelectedTargets(),
                "FAIL -> Check targeting after creation!");

        log.info("edit campaign");
        pagesInit.getCampaigns().clearAllSettings()
                .setCampaignName("Test campaign " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setUseTargeting(true)
                .setTargetingOption("phonePriceRange")
                .setIsUseSchedule(false)
                .setLimitType("clicks_limits")
                .setCampaignUtm("pmac")
                .setCustomUtm("geo={geo_region}")
                .setSourcesUtm("ruos")
                .setMediumUtm("idem")
                .editCampaign();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelectedTargets(),
                "FAIL -> Check targeting after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление таргета на стоимость телефонов")
    @Description("Проверка права для отображения таргета на ценовой диапазон устройств при создании кампании</li>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51876\">Ticket TA-51876</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    //todo AIA need to discuss checking privileges in dash @Test(description = "Проверка права для отображения таргетинга на ценовой диапазон устройств при создании кампании")
    public void checkPhonesPricesTargetingInDashPrivilege() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/can_use_phones_price_range_targeting");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
            Assert.assertFalse(pagesInit.getCampaigns().checkPhonePriceRangeTargetingIsDisplayed(),
                    "FAIL -> Phone price range is displayed with disabled privilege!");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/can_use_phones_price_range_targeting");
        }
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Реакция ричметра на добавление ценовой категории девайса в таргеты")
    @Description("Проверка отрисовки ричметра при исключении диапазона цен из таргетинга\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51878\">Ticket TA-51878</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Проверка отрисовки ричметра при исключении диапазона цен из таргетинга")
    public void checkReachMeterForPhonePricesTargetingExclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");

        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 950");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 94.2857%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 800");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 91.5714%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 650");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 88.8571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("exclude");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("1 500");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 86.1429%;");

        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Реакция ричметра на добавление ценовой категории девайса в таргеты")
    @Description("Проверка отрисовки ричметра при включении диапазона цен в таргетинг\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51878\">Ticket TA-51878</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "<ul>\n")
    @Test(description = "Проверка отрисовки ричметра при включении диапазона цен в таргетинг")
    public void checkReachMeterForPhonePricesTargetingInclude() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("2 100");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 97%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("150");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 40.3571%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("300");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 49.4286%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("450");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 55.1429%;");
        pagesInit.getCampaigns().selectPhonePriceTargeting("include");
        pagesInit.getCampaigns().checkReachMeterImpressionsCounter("600");
        pagesInit.getCampaigns().checkReachMeterPercent("width: 60.8571%;");

        log.info("Test is finished");
    }


    @Story("Флаг управления созданием пуш-рк для НКС")
    @Description("Проверка возможности создания пуш рк для новых самозарегов с разрешенным флагом\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка возможности создания пуш рк для новых самозарегов с разрешенным флагом")
    public void checkCanCreatePushCampaignForSelfRegisterClientsWithFlag() {
        log.info("Test is started");
        authDashAndGo("self-reg-for-push-with-1-dollar-spend@mgid.com", "advertisers/add");

        log.info("Check available campaign types");
        Assert.assertEquals(pagesInit.getCampaigns().checkCampaignTypeIsPresent("push"), 1,
                "FAIL -> Push campaign is available for self register client!");

        log.info("Test is finished");
    }

    @Story("Ограничение дневных спендов для новых саморегов")
    @Description("Проверка остановки кампании после достижения лимита трат в $500 для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51890\">Ticket TA-51890</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка остановки кампании после достижения лимита трат в $500 для новых самозарегов")
    public void checkBlockedCampaignsAfterLimitOfDaySpendingForSelfRegisteredClients() {
        log.info("Test is started");
        authDashAndGo("self-reg-for-check-limit@mgid.com", "advertisers");
        softAssert.assertEquals(pagesInit.getCampaigns().getCampaignStatus(), "Active",
                "FAIL -> Campaign STATUS by limit for self registered client!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkLimitIconIsDisplayed(),
                "FAIL -> Campaign ICON by limit for self registered client!");
        softAssert.assertTrue(pagesInit.getCampaigns().getCampaignStatusTitle().contains(
                "Account has reached its daily limit. Please contact your manager to change the limit. The campaign has been paused since "),
                "FAIL -> Campaign TITLE by limit for self registered client!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Dash")
    @Description("Check message at edit interface:" +
            "<ul>\n" +
            "   <li>Language != Ukraine, choose targeting Ukraine</li>\n" +
            "   <li>Targeting == Ukraine, choose language  not Ukrainian</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51948\">Ticket TA-51948</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "check message at edit interface")
    public void checkMessageAtEditInterface() {
        log.info("Test is started");
        int campaignIdWithUkraineLanguage = 1234;
        int campaignIdWithoutUkraineLanguage = 1233;
        log.info("Create campaign");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(campaignIdWithUkraineLanguage, "Ukraine");

        log.info("Check message language != Ukraine, targeting include Ukraine");
        authDashAndGo("advertisers/edit/campaign_id/" + campaignIdWithoutUkraineLanguage);
        softAssert.assertTrue(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - first edit check");

        log.info("Check message language != Ukraine, targeting exclude Ukraine");
        authDashAndGo("advertisers/edit/campaign_id/" + campaignIdWithoutUkraineLanguage);
        pagesInit.getCampaigns()
                .selectLocationTarget("exclude", "Ukraine");
        softAssert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - second edit check");

        log.info("Check message language == Ukraine, targeting exclude Ukraine");
        authDashAndGo("advertisers/edit/campaign_id/" + campaignIdWithUkraineLanguage);
        softAssert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - third edit check");

        log.info("Check message language == Ukraine, targeting include Ukraine");
        authDashAndGo("advertisers/edit/campaign_id/" + campaignIdWithUkraineLanguage);
        pagesInit.getCampaigns().selectLocationTarget("include", "Ukraine");
        softAssert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - fourth edit check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Dash")
    @Description("Check message with canRotateUa flag" +
            "<ul>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51948\">Ticket TA-51948</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check message with canRotateUa flag")
    public void checkMessageAtEditInterfaceWithCanRotateUaFlag() {
        log.info("Test is started");
        int campaignIdWithoutUkraineLanguage = 1232;
        log.info("Create campaign");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");

        log.info("Check message language with rotate flag");
        authDashAndGo("testemail1000@ex.ua", "advertisers/edit/campaign_id/" + campaignIdWithoutUkraineLanguage);
        Assert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - rotate flag");
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Dash")
    @Description("Check privilege" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51948\">Ticket TA-51948</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Privilege(name = "can_see_ukraine_law_alert_msg")
    @Test(description = "check privilege can_see_ukraine_law_alert_msg")
    public void checkPrivilegeCanRotateUa() {
        try {
            log.info("Test is started");
            log.info("Create campaign");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "advertisers/can_see_ukraine_law_alert_msg");

            authDashAndGo("advertisers/add");
            pagesInit.getCampaigns()
                    .chooseCampaignLanguage("2");

            Assert.assertFalse(pagesInit.getCampaigns().checkWarningAboutRotationInUkraine(messageUaRotation), "Fail - first check");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "advertisers/can_see_ukraine_law_alert_msg");
            log.info("Test is finished");
        }
    }

    @Epic("Limits")
    @Feature("Limits. Daily can be equal to overall")
    @Story("Creating campaign form")
    @Description("Check error message when general limit is less then daily for campaign types:" +
            "- product" +
            "- content" +
            "- push")
    @Owner("AIA")
    @Test(dataProvider = "campaignTypes", description = "Check error message when general limit is less then daily")
    public void checkLimitsValidation(String campaignType) {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Сampaign with equal limits " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setSubnetType(SCENARIO_MGID)
                .setCampaignType(campaignType)
                .setUseTargeting(false)
                .setIsUseSchedule(false)
                .setIsEnabledSensors(false)
                .setTargetingSwitcher(false)
                .setIsEnableUtms(false)
                .setDailyLimit("500")
                .setOverallLimit("499")
                .createCampaign();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("THE OVERALL BUDGET SHOULD BE GREATER THAN THE DAILY BUDGET"),
                "FAIL -> Limits error message!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypes() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"push"}
        };
    }

    @Epic("Limits")
    @Feature("Limits. Daily can be equal to overall")
    @Story("Creating campaign form")
    @Description("Check error message when general limit is less then daily for search_feed campaign")
    @Owner("AIA")
    @Test(description = "Check error message when general limit is less then daily for search_feed campaign")
    public void checkLimitsValidationForSearchFeedCampaign() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Сampaign with equal limits " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setSubnetType(SCENARIO_MGID)
                .setCampaignType("search_feed")
                .setCampaignKeyword("limit")
                .setUseTargeting(false)
                .setIsUseSchedule(false)
                .setIsEnabledSensors(false)
                .setTargetingSwitcher(false)
                .setIsEnableUtms(false)
                .setDailyLimit("500")
                .setOverallLimit("499")
                .createCampaign();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("THE OVERALL BUDGET SHOULD BE GREATER THAN THE DAILY BUDGET"),
                "FAIL -> Limits error message!");
        log.info("Test is finished");
    }
}
