package dashboard.mgid.advertisers.campaign;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.Arrays;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

public class CampaignCertificateTests extends TestBase {
    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Добавление сертификата</li>\n" +
            "   <li>Редактирование сертификата</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Добавление, редактирование сертификата")
    public void createProductCampaign() {
        log.info("Test is started");
        String categoryId = helpersInit.getBaseHelper().getRandomFromList(operationMySql.getgCategory().getCategoriesAllowedLoadCertificate("product"));
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setTargetingSwitcher(true)
                .setBlockBeforeShow(false)
                .setCertificate(LINK_TO_RESOURCES_IMAGES + "sky.jpg")
                .setTargetingOption("location")
                .selectLocationTarget("include", "Italy")
                .setCampaignCategory(categoryId)
                .setSubnetType(SCENARIO_MGID)
                .createCampaignSimple();

        log.info("Check campaign after editing");
        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCampaigns().checkDisplayingUploadedCertificate("sky.jpg"), "Check campaign after editing 1");

        pagesInit.getCampaigns().deleteCertificate();
        pagesInit.getCampaigns().saveCampaign();

        authDashAndGo("advertisers/edit/campaign_id/" + pagesInit.getCampaigns().getCampaignId());
        softAssert.assertFalse(pagesInit.getCampaigns().checkDisplayingUploadedCertificate("sky.jpg"), "Check campaign after editing 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Отображение блока загрузки при одном з не подходящих условий - тип кампании</li>\n" +
            "   <li>Отображение блока загрузки при одном з не подходящих условий - категория</li>\n" +
            "   <li>Отображение блока загрузки при одном з не подходящих условий - гео</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Отображение блока загрузки при отсутствии одного из обязательных условий для этого блока")
    public void checkVisibilityCertificateBlockWithDiferentConditions() {
        log.info("Test is started");

        String categoryIdDisallowedUploading = helpersInit.getBaseHelper().getRandomFromList(operationMySql.getgCategory().getCategoriesDisallowLoadCertificate("push"));
        String categoryIdAllowedUploading = helpersInit.getBaseHelper().getRandomFromList(operationMySql.getgCategory().getCategoriesAllowedLoadCertificate("product"));
        String countryNameDisallowedUploading = helpersInit.getBaseHelper().getRandomFromList(Arrays.asList("Poland", "Canada", "Latvia", "Germany", "USA"));
        String countryNameAllowedUploading = helpersInit.getBaseHelper().getRandomFromList(Arrays.asList("Italy", "Ukraine", "Spain", "Romania", "Czech Republic", "Vietnam"));
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Choose campaign settings " + categoryIdDisallowedUploading);
        pagesInit.getCampaigns().chooseCampaignType("push")
                .chooseCampaignCategory(categoryIdDisallowedUploading)
                .selectLocationTarget("include", countryNameAllowedUploading);

        log.info("Check displaying certificate");
        softAssert.assertFalse(pagesInit.getCampaigns().checkDisplayingBlockUploadCertificate(), "First case");

        log.info("Choose campaign settings " + countryNameDisallowedUploading);
        pagesInit.getCampaigns().chooseCampaignType("product")
                .chooseCampaignCategory(categoryIdAllowedUploading)
                .selectLocationTarget("include", countryNameDisallowedUploading);

        log.info("Check displaying certificate");
        softAssert.assertFalse(pagesInit.getCampaigns().checkDisplayingBlockUploadCertificate(), "Second case");

        log.info("Choose campaign settings search_feed");
        categoryIdAllowedUploading = helpersInit.getBaseHelper().getRandomFromList(operationMySql.getgCategory().getCategoriesAllowedLoadCertificate("search_feed"));
        pagesInit.getCampaigns().chooseCampaignType("search_feed")
                .chooseCampaignCategory(categoryIdAllowedUploading)
                .selectLocationTarget("include", countryNameAllowedUploading);
        log.info("Check displaying certificate");
        softAssert.assertFalse(pagesInit.getCampaigns().checkDisplayingBlockUploadCertificate(), "Third case");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Сертификаты. Кампании. Дешборд")
    @Description("Доработка по сертификатам и производителям продуктов | Dashbord\n" +
            "<ul>\n" +
            "   <li>Проверка загрузки сертификатов с одинаковым названием</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51900\">Ticket TA-51900</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Загрузки сертификатов с одинаковым названием")
    public void checkUploadCertificatesWithTheSameName() {
        log.info("Test is started");
        int campaignId = 1205;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Italy");

        log.info("Upload certificate");
        authDashAndGo("testemail1000@ex.ua", "advertisers/edit/campaign_id/" + campaignId);
        pagesInit.getCampaigns().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_png.png");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("CERTIFICATE FILE WITH THIS NAME ALREADY EXIST"), "Check campaign after editing");

        log.info("Test is finished");
    }
}
