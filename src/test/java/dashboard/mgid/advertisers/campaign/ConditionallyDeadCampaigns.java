package dashboard.mgid.advertisers.campaign;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

public class ConditionallyDeadCampaigns extends TestBase {
    public ConditionallyDeadCampaigns() {
        subnetId = SCENARIO_MGID;
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check filter:\n" +
        "<ul>\n" +
            "<li>1) Filter by 'Deleted' value - deleted campaign is present in list</li>\n" +
            "<li>2) Filter by 'All but deleted' value - deleted campaign is not present in list</li>\n" +
            "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
            "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check filter job")
    public void checkConditionallyDeletedCampaignsByFilterDeleted() {
        log.info("Test is started");
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers");
        pagesInit.getCampaigns().filterCampaignsByCustomValue("deleted");
        softAssert.assertTrue(pagesInit.getCampaigns().isDisplayedCampaign("For check dead campaigns"),
                "FAIL -> Deleted campaign is not displayed by filter 'Deleted'!");
        pagesInit.getCampaigns().filterCampaignsByCustomValue("allButDeleted");
        softAssert.assertFalse(pagesInit.getCampaigns().isDisplayedCampaign("For check dead campaigns"),
                "FAIL -> Deleted campaign is displayed by filter 'All but deleted'!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check icons:\n" +
            "<ul>\n" +
                "<li>1) Check title for 'Teaser' icon</li>\n" +
                "<li>2) Check that 'Statistic' icon is present</li>\n" +
                "<li>3) Check that 'Selective bidding' icon is present</li>\n" +
                "<li>4) Check that 'Copy campaign' icon is not present</li>\n" +
                "<li>5) Check that 'Restore campaign' icon is not present</li>\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
                "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check action icons for deleted campaign")
    public void checkConditionallyDeletedCampaignsAvailableIcons() {
        log.info("Test is started");
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers");
        pagesInit.getCampaigns().filterCampaignsByCustomValue("deleted");
        softAssert.assertTrue(pagesInit.getCampaigns().checkTeaserIconTitle("To be able to view the ads, please first restore the cаmpaign by clicking the restore icon."),
                "FAIL -> Teaser`s icon title!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkStatisticsIconIsDisplayed(),
                "FAIL -> Statistics icon is not present!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelectiveBiddingIconIsDisplayed(),
                "FAIL -> Selective bidding icon is not present!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkCopyCampaignIconIsDisplayed(),
                "FAIL -> Copy campaign icon is present!");
        softAssert.assertFalse(pagesInit.getCampaigns().checkRestoreCampaignIconIsDisplayed(),
                "FAIL -> Restore campaign icon is present!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check Selected bidding availability\n" +
            "<ul>\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
                "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check Selected bidding is available for 'dead' campaign")
    public void checkConditionallyDeletedCampaignsSelectiveBiddingIsAvailable() {
        log.info("Test is started");
        authDashAndGo("client-with-reach-stat@mgid.com", "advertisers/campaign-quality-analysis/id/" + 2015);
        Assert.assertEquals(pagesInit.getSelectiveBidding1().getSelectiveBiddingWidgetUID(), "4",
                "FAIL -> Selective bidding is unavailable for dead campaign!");
        log.info("Test is finished");
    }
}
