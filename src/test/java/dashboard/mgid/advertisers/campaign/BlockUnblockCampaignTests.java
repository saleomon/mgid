package dashboard.mgid.advertisers.campaign;

import org.testng.annotations.Test;
import testBase.TestBase;

public class BlockUnblockCampaignTests extends TestBase {
    public String login = "testemail1000@ex.ua";


    /**
     * Запрет на снятие блока с кампаний ГЕО Италия со статусом IAP NOT Compliant
     * <ul>
     *  <li>Создаем кампанию с настройками гео только Италия без опции IAP NOT Compliant</li>
     *  <li>Проверяем разблокировку кампании</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24526">TA-24526</a>
     *<p>NIO</p>
     */
    @Test
    public void checkUnblockCampaignWithGeoItalyOnly() {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1035, "Italy");
        authDashAndGo(login, "advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign("1035");

        log.info("Check popup message");
        softAssert.assertTrue(pagesInit.getCampaigns().checkPopupMessage("Campaign you’re trying to unblock is not IAP compliant"), "Unblock by icon");

        authDashAndGo("advertisers");
        log.info("Check icon visibility");
        softAssert.assertTrue(pagesInit.getCampaigns().checkVisibilityUnblockIcon("1035"), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Запрет на снятие блока с кампаний ГЕО Италия со статусом IAP NOT Compliant
     * <ul>
     *  <li>Создаем кампанию с настройками гео Италия и другими странами без опции IAP NOT Compliant</li>
     *  <li>Проверяем разблокировку кампании</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24526">TA-24526</a>
     *<p>NIO</p>
     */
    @Test
    public void checkUnblockCampaignWithGeoItalyAndOthers() {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1035, "Italy", "Egypt", "India");
        authDashAndGo(login, "advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign("1035");

        log.info("Check popup message");
        softAssert.assertTrue(pagesInit.getCampaigns().checkPopupMessage("Campaign you’re trying to unblock is not IAP compliant"), "Unblock by icon");

        authDashAndGo("advertisers");
        log.info("Check icon visibility");
        softAssert.assertTrue(pagesInit.getCampaigns().checkVisibilityUnblockIcon("1035"), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Запрет на снятие блока с кампаний ГЕО Италия со статусом IAP NOT Compliant
     * <ul>
     *  <li>Создаем кампанию с настройками гео только Италия c опцией IAP Compliant</li>
     *  <li>Проверяем разблокировку кампании</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24526">TA-24526</a>
     *<p>NIO</p>
     */
    @Test
    public void checkUnblockCampaignWithGeoItalyWithIapCompliantOption() {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1036, "Italy");
        authDashAndGo(login,"advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign("1036");

        log.info("Check icon visibility");
        authDashAndGo("advertisers");
        softAssert.assertFalse(pagesInit.getCampaigns().checkVisibilityUnblockIcon("1036"), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Запрет на снятие блока с кампаний ГЕО Италия со статусом IAP NOT Compliant
     * <ul>
     *  <li>Создаем кампанию с настройками гео отличным от Италии c опцией IAP NOT Compliant</li>
     *  <li>Проверяем разблокировку кампании</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24526">TA-24526</a>
     *<p>NIO</p>
     */
    @Test
    public void checkUnblockCampaignWithGeoNotItalyWithoutIapCompliantOption() {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1037, "Ukraine", "Australia");
        authDashAndGo(login,"advertisers");
        log.info("Unblock campaign");
        pagesInit.getCampaigns().unBlockCampaign("1037");

        log.info("Check icon visibility");
        softAssert.assertFalse(pagesInit.getCampaigns().checkVisibilityUnblockIcon("1037"), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
