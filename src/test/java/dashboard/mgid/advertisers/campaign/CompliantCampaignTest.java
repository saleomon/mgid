package dashboard.mgid.advertisers.campaign;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.cab.products.logic.CabCampaigns.CompliantType.*;
import static core.helpers.BaseHelper.randomNumbersInt;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

public class CompliantCampaignTest extends TestBase {

    @Story("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check auto enabling checkboxes after created product campaign\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check auto enabling checkboxes after created product campaign")
    public void checkDefaultStateCheckboxesAfterCampaignCreationAllGeo() {
        log.info("Test is started");
        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setTargetingSwitcher(false)
                .setUseTargeting(false)
                .setBlockBeforeShow(false)
                .setSubnetType(SCENARIO_MGID)
                .createCampaignSimple();

        log.info("Check campaign after editing");
        authCabAndGo("goodhits/campaigns/?id=" + pagesInit.getCampaigns().getCampaignId());

        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(RAC, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT), "FAIL - selected compliant all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(4), "FAIL - amount of displayed countries");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check auto enabling checkboxes after created push campaign for custom region\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check auto enabling checkboxes after created push campaign for custom region")
    public void checkDefaultStateCheckboxesAfterCampaignCreationCustomGeo() {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};
        int randomIndex = randomNumbersInt(arrayCountries.length);


        authDashAndGo("testemail1000@ex.ua", "advertisers/add");
        log.info("Create campaign");
        pagesInit.getCampaigns().setCampaignName("Test camp create product " + helpersInit.getBaseHelper().randomNumbersString(4))
                .setCampaignType("push")
                .setTargetingSwitcher(true)
                .setTargetingOption("location")
                .selectLocationTarget("include", arrayCountries[randomIndex])
                .setBlockBeforeShow(false)
                .setSubnetType(SCENARIO_MGID)
                .createCampaignSimple();

        log.info("Check campaign after editing");
        authCabAndGo("goodhits/campaigns/?id=" + pagesInit.getCampaigns().getCampaignId());

        pagesInit.getCabCampaigns().openCompliantPopup();
        Assert.assertTrue(pagesInit.getCabCampaigns().checkCompliantCountryIcon(arrayCountries[randomIndex]), "FAIL - country icon - " + arrayCountries[randomIndex]);

        log.info("Test is finished");
    }

    @Story("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check influence on checkbox states after geo editing\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check influence on checkbox states after geo editing")
    public void checkInfluenceGeoEditingOnCheckboxStates() {
        log.info("Test is started");
        int campaignId = 1281;

        authDashAndGo("testemail1000@ex.ua", "advertisers/edit/campaign_id/" + campaignId);
        pagesInit.getCampaigns().setTargetingSwitcher(true).selectLocationTarget("include", "Spain", "Italy", "Romania", "Czech Republic", "Ukraine", "France", "Thailand", "Brazil", "Poland");
        pagesInit.getCampaigns().saveCampaign();


        log.info("Check all geo icon and selected checkboxes");
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(0), "FAIL - amount of displayed countries");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check unblock campaign with no flag compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblock campaign with no flag compliant")
    public void checkUnblockingWithoutCompliantFlag() {
        log.info("Test is started");
        String teaserId = "1292";

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(teaserId), "Spain");
        authDashAndGo("testemail1000@ex.ua","advertisers");
        pagesInit.getCampaigns().unBlockCampaign(teaserId);
        softAssert.assertEquals(pagesInit.getCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not Autocontrol compliant", "FAIL - Spain");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(teaserId), "Italy");
        authDashAndGo("testemail1000@ex.ua","advertisers");
        pagesInit.getCampaigns().unBlockCampaign(teaserId);
        softAssert.assertEquals(pagesInit.getCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not IAP compliant", "FAIL - Italy");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(teaserId), "Romania");
        authDashAndGo("testemail1000@ex.ua","advertisers");
        pagesInit.getCampaigns().unBlockCampaign(teaserId);
        softAssert.assertEquals(pagesInit.getCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not RAC compliant", "FAIL - Romania");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(teaserId), "Poland");
        authDashAndGo("testemail1000@ex.ua","advertisers");
        pagesInit.getCampaigns().unBlockCampaign(teaserId);
        softAssert.assertEquals(pagesInit.getCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not Poland Reg compliant", "FAIL - Poland");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
