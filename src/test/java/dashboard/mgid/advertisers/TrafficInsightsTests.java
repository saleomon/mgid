package dashboard.mgid.advertisers;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.sorted.SortedType;
import testBase.TestBase;

public class TrafficInsightsTests extends TestBase {

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Platforms")
    @Description("Check switching Platforms at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Desktop platforms</li>\n" +
            "   <li>Mobile & Tablets platforms</li>\n" +
            "   <li>All platforms</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: check switching Platforms")
    public void checkTrafficInsightsSwitchingPlatforms() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");
        softAssert.assertEquals(pagesInit.getTrafficInsights().getTrafficInsightsTitle(),
                "Traffic Insights (updates daily)",
                "FAIL -> Wrong title!");
        pagesInit.getTrafficInsights().enableDesktopPlatform();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_platform_desktop.png")),
                "FAIL -> Check screen 'Desktop platforms");

        pagesInit.getTrafficInsights().enableMobilePlatform();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_platform_mobile.png")),
                "FAIL -> Check screen 'Mobile platforms");

        pagesInit.getTrafficInsights().enableAllPlatform();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_platform_all.png")),
                "FAIL -> Check screen 'All platforms");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Traffic type")
    @Description("Check switching Traffic type at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Push traffic type</li>\n" +
            "   <li>Native traffic type</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: check switching Traffic type")
    public void checkTrafficInsightsSwitchingTrafficType() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");

        pagesInit.getTrafficInsights().enablePushTrafficType();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_traffic_type_push.png")),
                "FAIL -> Check screen 'Push traffic type!");

        pagesInit.getTrafficInsights().enableNativeTrafficType();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_traffic_type_native.png")),
                "FAIL -> Check screen 'Native traffic type!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Sorting by impressions")
    @Description("Check sorting table by impressions at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Sorting by ascending</li>\n" +
            "   <li>Sorting by descending</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: check sorting table by impressions")
    public void checkTrafficInsightsSortingImpressions() {
        log.info("Test is started");
        SortedType.SortType sortingType = new SortedType().randomPeriod();
        authDashAndGo("advertisers/traffic-insights");
        Assert.assertTrue(pagesInit.getTrafficInsights().checkSortedColumns(sortingType, "impressions"),
                "FAIL -> sorting Daily impressions by - " + sortingType);
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. CPC by campaign type")
    @Description("Check viewing 'Cost per click' by campaign type at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Product campaign</li>\n" +
            "   <li>Content campaign</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: viewing 'Cost per click' by campaign type")
    public void checkTrafficInsightsCampaignTypeCpcFilter() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");
        pagesInit.getTrafficInsights().selectCampaignTypeValue("content");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_campaign_type_cpc_filter_content.png")),
                "FAIL -> Check screen 'Pagination next'!");
        pagesInit.getTrafficInsights().selectCampaignTypeValue("product");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_pagination_previous.png")),
                "FAIL -> Check screen 'Pagination previous'!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Pagination")
    @Description("Check pagination at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Go to Next page</li>\n" +
            "   <li>Go to Previous page</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: pagination")
    public void checkTrafficInsightsPagination() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");
        pagesInit.getTrafficInsights().clickPaginationNextButton();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_pagination_next.png")),
                "FAIL -> Check screen 'Pagination next'!");
        pagesInit.getTrafficInsights().clickPaginationPreviousButton();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                        serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("trafficInsights/check_pagination_previous.png")),
                "FAIL -> Check screen 'Pagination previous'!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Filter")
    @Description("Check filtering by country at 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Filter by country</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: filtering by country")
    public void checkTrafficInsightsSearchByCountry() {
        log.info("Test is started");
        String countryName = "Ukraine";
        authDashAndGo("advertisers/traffic-insights");
        pagesInit.getTrafficInsights().searchCountry(countryName);
        softAssert.assertEquals(pagesInit.getTrafficInsights().getCountryListSizeAfterSearch(), 1,
                "FAIL -> County list size!");
        softAssert.assertEquals(pagesInit.getTrafficInsights().getCountryNameFromList(), countryName,
                "FAIL -> County list size!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Add campaign")
    @Description("Check add native campaign for Desktop platform by country from 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Add Native campaign for Desktop platform</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: check add native campaign for desktop by country")
    public void checkTrafficInsightsAddCampaignByCountryForDesktopAndNative() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");
        pagesInit.getTrafficInsights().enableDesktopPlatform();
        pagesInit.getTrafficInsights().enableNativeTrafficType();
        pagesInit.getTrafficInsights().addCampaignByCountry();
        softAssert.assertEquals(pagesInit.getCampaigns().getSelectedCampaignType(), "product",
                "FAIL -> Check Product campaign type!");
        softAssert.assertTrue(pagesInit.getTrafficInsights().checkGeoTargeting(),
                "FAIL -> Check geo targeting!");
        softAssert.assertEquals(pagesInit.getTrafficInsights().getPlatformTargeting(), "Desktop OS",
                "FAIL -> Check Desktop platform targeting!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Traffic Insights")
    @Story("Traffic Insights. Add campaign")
    @Description("Check add push campaign for Mobile platform by country from 'Traffic Insights' interface:" +
            "Cases:\n" +
            "<ul>\n" +
            "   <li>Add Push campaign for Mobile platform</li>\n" +
            "</ul>\n" +
            "Tickets:\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52194\">Ticket TA-51948</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/BT-775\">Ticket BT-775</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/WT-612\">Ticket WT-612</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "'Traffic Insights' interface: check add Push campaign for mobile by country")
    public void checkTrafficInsightsAddCampaignByCountryForMobileAndPush() {
        log.info("Test is started");
        authDashAndGo("advertisers/traffic-insights");
        pagesInit.getTrafficInsights().enableMobilePlatform();
        pagesInit.getTrafficInsights().enablePushTrafficType();
        pagesInit.getTrafficInsights().addCampaignByCountry();
        softAssert.assertEquals(pagesInit.getCampaigns().getSelectedCampaignType(), "push",
                "FAIL -> Check Push campaign type!");
        softAssert.assertTrue(pagesInit.getTrafficInsights().checkGeoTargeting(),
                "FAIL -> Check geo targeting!");
        softAssert.assertEquals(pagesInit.getTrafficInsights().getPlatformTargeting(), "Mobile OS",
                "FAIL -> Check Mobile platform targeting!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}