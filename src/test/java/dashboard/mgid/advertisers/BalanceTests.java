package dashboard.mgid.advertisers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

public class BalanceTests extends TestBase {

    @Description("Проверка возможности формирования pdf-инвойса в дашборде в интерфейсе 'История баланса'\n" +
            "     <ul>\n" +
            "      <li>Проверка того, что:</li>" +
            "      <li>1) в інтерфейсе биллинга есть кнопка формирования инвойса;</li>" +
            "      <li>2) при нажатии на кнопку скачивается файл с инвойсом;</li>" +
            "      <li>3) после скачивания файла он присутствует, и имеет формат *.pdf.</li>" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51974\">Ticket TA-51974</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Проверка возможности формирования pdf-инвойса в дашборде в интерфейсе 'История баланса'")
    public void checkPDFInvoiceGeneration() {
        log.info("Test is started");
        authDashAndGo("client-with-deposits@mgid.com", "/advertisers/balance");
        pagesInit.getAdvertisersBalance().generateInvoice();
        serviceInit.getDownloadedFiles().getDownloadedFile();

        log.info("Check downloaded file name");
        Assert.assertEquals(serviceInit.getDownloadedFiles().getFileName(), "export.pdf");

        log.info("Test is finished");
    }

    @Feature("Change Minimum Top-up Limit for UAH,RUB currencies")
    @Story("Story to BT-4405 | Увеличение минимальной суммы пополнения для гривны и рублей")
    @Description("Тест выполняет проверку того, что нельзя ввести сумму пополнения меньшую чем задано настройками \n" +
            "системы для валют UAH, RUB, USD\n" +
            "     <ul>\n" +
            "      <li>1. Проверка того, что при вводе суммы меньше минимальной срабатывает валидация</li>" +
            "      <li>2. Проверка того, что при выборе платежной системы появляется alert;</li>" +
            "      <li>3. Проверка того, что при вводе корректной суммы, выборе платежной системы и последующем " +
            "             изменении суммы на меньшую минимальной, срабатывает валидация.</li>" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-1995\">Ticket TA-51995</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Проверка минимальной суммы пополнения для валют UAH, RUB, USD", dataProvider = "minPaymentValueValidations")
    public void checkMinimumDepositAmount(String userLogin, String incorrectPaymentValue,
                                          String correctPaymentValue, String validationMessage) {
        log.info("Test is started");
        authDashAndGo(userLogin, "/advertisers");
        pagesInit.getCampaigns().clickAddFundsButton();
        pagesInit.getCampaigns().setPaymentSum(incorrectPaymentValue);
        log.info("Check validation error");
        Assert.assertTrue(pagesInit.getCampaigns().checkMinPaymentSumValidation(validationMessage),
                "FAIL -> Minimal payment sum validation is wrong!");
        Assert.assertTrue(pagesInit.getCampaigns().checkPaymentSystemsAlert(),
                "FAIL -> Incorrect payment alert!");
        Assert.assertTrue(pagesInit.getCampaigns().checkPaymentsSystemValidationAfterChangeSum(validationMessage, correctPaymentValue, incorrectPaymentValue),
                "FAIL -> Minimal payment sum validation is wrong after changing it from suitable value!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] minPaymentValueValidations() {
        return new Object[][]{
                {"adver-rub@mgid.com",           "9999.99", "10000", "The minimal payment sum is 10 000.00RUB"},
                {"adver-uah@mgid.com",           "2999.99",  "3000",  "The minimal payment sum is 3 000.00UAH"},
                {"client-with-deposits@mgid.com",  "99.99",   "100",      "The minimal payment sum is $100.00"}
        };
    }

    @Feature("Change Minimum Top-up Limit for UAH,RUB currencies")
    @Story("Story to BT-4405 | Увеличение минимальной суммы пополнения для гривны и рублей")
    @Description("Тест выполняет проверку того, что тестовый пользователь может ввести сумму пополнения меньшую чем " +
            "задано настройками системы\n" +
            "     <ul>\n" +
            "      <li>1. Проверка того, что для тестового пользователя, при вводе суммы меньше минимальной нет валидации</li>" +
            "      <li>2. Проверка того, что для тестового пользователя, при выборе платежной системы НЕ появляется alert;</li>" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-1995\">Ticket TA-51995</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Проверка минимальной суммы пополнения для тестового пользователя")
    public void checkMinimumDepositAmountForTestUser() {
        log.info("Test is started");
        authDashAndGo("is-test-user@mgid.com", "/advertisers");
        pagesInit.getCampaigns().clickAddFundsButton();
        pagesInit.getCampaigns().setPaymentSum("0.01");
        log.info("Check validation error");
        Assert.assertFalse(pagesInit.getCampaigns().checkMinPaymentSumValidation("The minimal payment sum is 3 000.00UAH"),
                "FAIL -> Minimal payment sum validation message is present for 'is_test' user!");
        Assert.assertFalse(pagesInit.getCampaigns().checkPaymentSystemsAlert(),
                "FAIL -> 'Incorrect value' alert is present for 'is_test' user!");
        log.info("Test is finished");
    }
}
