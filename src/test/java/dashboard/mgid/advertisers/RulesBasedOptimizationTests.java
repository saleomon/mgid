package dashboard.mgid.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.CliCommandsList.Cron.RULES_BASED_OPTIMIZATION;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.ClientsEntities.CLIENT_FOR_DISABLE_403_ONLY;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;

public class RulesBasedOptimizationTests extends TestBase {

    @Epic("Rules Based Optimization")
    @Feature("Check Rights on Optimization Rule - list")
    @Story("Publishers efficiency interface")
    @Description("Check Rights on Optimization Rule - list")
    @Owner("AIA")
    @Test(description = "Check Rights on Optimization Rule - list")
    public void checkRulesBasedOptimizationRights_List() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "rules-based-optimization/list");
            authDashAndGo(CLIENT_FOR_DISABLE_403_ONLY, "advertisers/campaign-quality-analysis/id/" + 2057);
            softAssert.assertFalse(pagesInit.getRulesBaseOptimization().checkDisplayingAddRuleButton(),
                    "FAIL -> List rules rights!");
            softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("ACCESS DENIED OR INVALID REQUEST. PLEASE, CHECK THE LINK FOR CONSISTENCY."),
                    "FAIL -> List rules rights - Access denied message!");
            softAssert.assertAll();
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "rules-based-optimization/list");
            log.info("Test is finished");
        }
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Rights on add Optimization Rule - Auto-blocking rule")
    @Story("Publishers efficiency interface")
    @Description("Check Rights on add Optimization Rule - Auto-blocking rule")
    @Owner("AIA")
    @Test(description = "Check Rights on add Optimization Rule - Auto-blocking rule")
    public void checkRulesBasedOptimizationRights_AutoBlocking() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "rules-based-optimization/manage");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2043);
            pagesInit.getRulesBaseOptimization().clickAddRule();
            Assert.assertFalse(pagesInit.getRulesBaseOptimization().autoBlockingRuleIsDisplayed(),
                    "FAIL -> Auto-blocking Rule rights!");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "rules-based-optimization/manage");
            log.info("Test is finished");
        }
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Rights on add Optimization Rule - fixed bid rule")
    @Story("Publishers efficiency interface")
    @Description("Check Rights on add Optimization Rule - fixed bid rule")
    @Owner("AIA")
    @Test(description = "Check Rights on add Optimization Rule - fixed bid rule")
    public void checkRulesBasedOptimizationRights_FixeBid() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "rules-based-optimization/manage-fixed-bid-rule");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2041);
            pagesInit.getRulesBaseOptimization().clickAddRule();
            Assert.assertFalse(pagesInit.getRulesBaseOptimization().fixedBidRuleIsDisplayed(),
                    "FAIL -> Dynamic Bid Rule rights!");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "rules-based-optimization/manage-fixed-bid-rule");
            log.info("Test is finished");
        }
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Rights on add Optimization Rule - dynamic bid rule")
    @Story("Publishers efficiency interface")
    @Description("Check Rights on add Optimization Rule - dynamic bid rule")
    @Owner("AIA")
    @Test(description = "Check Rights on add Optimization Rule - dynamic bid rule")
    public void checkRulesBasedOptimizationRights_DynamicBid() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE, "rules-based-optimization/manage-bid-rule");
            authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2040);
            pagesInit.getRulesBaseOptimization().clickAddRule();
            Assert.assertFalse(pagesInit.getRulesBaseOptimization().autoBiddingRuleIsDisplayed(),
                    "FAIL -> Dynamic Bid Rule rights!g");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE, "rules-based-optimization/manage-bid-rule");
            log.info("Test is finished");
        }
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Create | Edit | Delete Optimization Rule")
    @Story("Publishers efficiency interface")
    @Description("Create, Edit, Delete Optimization Rule - Auto Blocking")
    @Owner("AIA")
    @Test(description = "Create, Edit, Delete Optimization Rule - Auto Blocking", dataProvider = "rulesBasedType")
    public void checkRulesBasedOptimizationCreate_AutoBlocking(String ruleType) {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2043);
        pagesInit.getRulesBaseOptimization().createRule(ruleType);
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkRule(),
                "FAIL -> Rules based optimization after creating - " + ruleType + "!");
        pagesInit.getRulesBaseOptimization().editRule();
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkRule(),
                "FAIL -> Rules based optimization after editing - " + ruleType + "!");
        pagesInit.getRulesBaseOptimization().deleteRule();
        softAssert.assertFalse(pagesInit.getRulesBaseOptimization().checkRuleIsDisplayed(),
                "FAIL -> Rules based optimization deleting - " + ruleType + "!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] rulesBasedType() {
        return new Object[][] {
                {"autoBidding"},
                {"autoBlocking"},
                {"fixedBid"}
        };
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Add Optimization Rule Set")
    @Story("Publishers efficiency interface")
    @Description("Check Add Optimization Rule Set")
    @Owner("AIA")
    @Test(description = "Check Add Optimization Rule Set")
    public void checkRulesBasedOptimization_RulesSet() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2046);
        softAssert.assertEquals(pagesInit.getRulesBaseOptimization().getRuleSetSize(), 1,
                "FAIL -> Check Optimization Rule Set - default!");
        pagesInit.getRulesBaseOptimization().clickAddRule();
        pagesInit.getRulesBaseOptimization().addOptimizationRuleSet();
        softAssert.assertEquals(pagesInit.getRulesBaseOptimization().getRuleSetSize(), 4,
                "FAIL -> Check Optimization Rule Set!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Rules Based Optimization")
    @Feature("Check Blocking widget by Optimization Rule with Cron")
    @Story("Publishers efficiency interface")
    @Description("Check Blocking widget by Optimization Rule with Cron")
    @Owner("AIA")
    @Test(description = "Check Blocking widget by Optimization Rule with Cron")
    public void checkRulesBasedOptimization_Cron() {
        log.info("Test is started");
        String firstWidget = "2001";
        String secondWidget = "2002";
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2044);
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkWidgetSwitchersState(firstWidget, "on"),
                "FAIl -> Default state of first switcher!");
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkWidgetSwitchersState(secondWidget, "on"),
                "FAIL -> Default state of second switcher!");
        serviceInit.getDockerCli().runAndStopCron(RULES_BASED_OPTIMIZATION, "-vv");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2044);
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkWidgetSwitchersState(firstWidget, "off"),
                "FAIL -> State of first switcher after crones work!");
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkWidgetSwitchersState(secondWidget, "on"),
                "FAIL -> State of second switcher after crones work!");
        pagesInit.getRulesBaseOptimization().clickSwitcher(firstWidget);
        softAssert.assertEquals(pagesInit.getRulesBaseOptimization().getRulesBasedPopupText(),
                "Rule-based optimization is enabled for the current campaign. If you change the state of a sub-source manually, the rules will ignore it during optimization. You can change this at any time by clicking the lock icon next to the ON-OFF switch.\n" +
                        "OK\n" +
                        "CANCEL",
                "FAIL -> Check rules based popup text!");
        pagesInit.getRulesBaseOptimization().confirmPopup();
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkLockIsVisible(firstWidget),
                "FAIL -> Lock for first widget!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Rules Based Optimization")
    @Feature("Check copying of Optimization Rule from one campaign to another")
    @Story("Publishers efficiency interface")
    @Description("Copy optimization rule from one campaign to another.\n" +
            "Check that rule is appeared in recipient campaign.")
    @Owner("AIA")
    @Flaky
    @Test(description = "Check copying of Optimization Rule")
    public void checkRulesBasedOptimization_CopyRules() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2042);
        pagesInit.getRulesBaseOptimization().copyRules("2045");
        softAssert.assertEquals(pagesInit.getRulesBaseOptimization().getRulesCopySuccessPopupText(),
                "The rules were copied successfully and will be applied immediately",
                "FAIL -> Copy rules Confirm popup text!");
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2045);
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkRuleIsDisplayed(),
                "FAIL -> Rule has not copied!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Rules Based Optimization")
    @Feature("Check History Log for Optimization Rule")
    @Story("Publishers efficiency interface")
    @Description("Check filter in history log popup by:\n" +
            "<ul>" +
                "<li>- UID</li>\n" +
                "<li>- Action</li>\n" +
                "<li>- Date</li>\n" +
                "<li>@see <a href=\"https://jira.mgid.com/browse/TA-52331\">Ticket TA-52331</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check filter in history log popup")
    public void checkRulesBasedOptimization_HistoryLog() {
        log.info("Test is started");
        String uidValue = "2001";
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "advertisers/campaign-quality-analysis/id/" + 2048);
        pagesInit.getRulesBaseOptimization().openHistoryLog();
        pagesInit.getRulesBaseOptimization().filterHistoryLogByUid(uidValue);
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkFilterHistoryLogByUidResult(uidValue),
                "FAIl -> Filter by UID!");
        pagesInit.getRulesBaseOptimization().closeHistoryLogPopup();
        pagesInit.getRulesBaseOptimization().openHistoryLog();
        pagesInit.getRulesBaseOptimization().filterHistoryLogByAction();
        softAssert.assertTrue(pagesInit.getRulesBaseOptimization().checkFilterHistoryLogByActionResult(),
                "FAIl -> Filter by Action!");
        pagesInit.getRulesBaseOptimization().closeHistoryLogPopup();
        pagesInit.getRulesBaseOptimization().selectDate();
        pagesInit.getRulesBaseOptimization().openHistoryLog();
        softAssert.assertEquals(pagesInit.getRulesBaseOptimization().getFilterHistoryLogResultTableSize(), 1,
                "FAIl -> Filter by Date!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
