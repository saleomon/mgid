package dashboard.mgid.publishers.websiteStatistics;

import core.helpers.statCalendar.CalendarForStatistics;
import io.qameta.allure.*;
import org.testng.annotations.Test;
import testBase.TestBase;

public class PushStatisticsTests extends TestBase {

    @Epic("Audience Hub Statistics")
    @Feature("Push subscribers statistics")
    @Story("Dashboard - Publishers - Audience Hub Statistics by site")
    @Description("Check statistics in interface 'Audience Hub Statistics - Push subscribers'" +
            "(dashboard.mgid.com/publisher/audience-hub-statistics-push-subscribers/site_id/2028)\n" +
            "Filter statistics by random period in calendar and check:\n" +
            "     <ul>\n" +
            "     * <li>sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for 'Form Requests', 'Form Impressions', 'Users Subscribed'</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52655\">Ticket TA-52655</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check statistics in interface 'Audience Hub Statistics - Push subscribers'")
    public void checkStatisticsPushSubscribers() {
        log.info("Test is started");
        int websiteId = 2028;
        authDashAndGo("publisher-with-push-stat@mgid.com", "publisher/audience-hub-statistics-push-subscribers/site_id/" + websiteId);

        CalendarForStatistics.CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(getRandomPeriod);

        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pushFR_pushFI");
        pagesInit.getWebsiteStatisticsClass().getDataPushRequestsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPushImpressionsFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pushFI_pushSubscribe");
        pagesInit.getWebsiteStatisticsClass().getDataPushSubscribeFromGraph();

        pagesInit.getWebsiteStatisticsClass().getPushSubscribersDataColumnByHeader();

        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatByPushSubscribers();

        pagesInit.getWebsiteStatisticsClass().getTotalDataFromAudienceHubEventsClickhouse(websiteId);

        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushRequests(),"FAIL -> sum Push Request in table/graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushImpressions(), "FAIL -> sum Push Impressions table/graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushSubscribers(), "FAIL -> sum Push Subscribers in table/graph");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Audience Hub Statistics")
    @Feature("Push subscribers statistics")
    @Story("Dashboard - Publishers - Audience Hub Statistics by site")
    @Description("Check statistics in interface 'Audience Hub Statistics - Push notifications'" +
            "(dashboard.mgid.com/publisher/audience-hub-statistics-push-notifications/site_id/2028)\n" +
            "Filter statistics by random period in calendar and check:\n" +
            "     <ul>\n" +
            "     * <li>sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for 'Pushes Sent', 'Pushes Delivered', 'Pushes Clicked'</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52655\">Ticket TA-52655</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check statistics in interface 'Audience Hub Statistics - Push notifications'")
    public void checkStatisticsPushNotifications() {
        log.info("Test is started");
        int websiteId = 2028;
        authDashAndGo("publisher-with-push-stat@mgid.com", "publisher/audience-hub-statistics-push-notifications/site_id/" + websiteId);

        CalendarForStatistics.CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(getRandomPeriod);

        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pushSend_pushDelivered");
        pagesInit.getWebsiteStatisticsClass().getDataPushesSentFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPushesDeliveredFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pushDelivered_pushClick");
        pagesInit.getWebsiteStatisticsClass().getDataPushesClickedFromGraph();

        pagesInit.getWebsiteStatisticsClass().getPushNotificationsDataColumnByHeader();

        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatByPushNotifications();

        pagesInit.getWebsiteStatisticsClass().getTotalDataFromPushStatisticsEventsClickhouse("site-push.com");

        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushesSent(),"FAIL -> sum 'Pushes Sent' in table/graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushesDelivered(), "FAIL -> sum 'Pushes Delivered' table/graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTableForPushesClicked(), "FAIL -> sum 'Pushes Clicked' in table/graph");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
