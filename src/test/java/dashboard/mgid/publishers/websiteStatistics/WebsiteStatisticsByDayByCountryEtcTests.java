package dashboard.mgid.publishers.websiteStatistics;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import core.helpers.statCalendar.CalendarForStatistics;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

import java.time.LocalDate;
import java.time.ZoneId;

public class WebsiteStatisticsByDayByCountryEtcTests extends TestBase {

    @Epic("Dash Publisher Statistics")
    @Story("Dash Publisher Statistics Website by COUNTRY/SOURCE/Device")
    @Description("<ul>" +
            "<li>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</li>" +
            "<li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks</li>" +
            "<li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm, CPC, rCpm</li>" +
            "<li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, rCpm, visibilityRate</li>" +
            "<li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate</li>" +
            "<li>widget_statistics_summ; widget_clicks_goods; widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(dataProvider = "byStaticTabsWidgets", description = "check Publisher Statistics Website in dash by COUNTRY/SOURCE/Device")
    public void checkStatByStat(String tabName, String tabValue) {
        log.info("Test is started -> " + tabName);
        int websiteId = 1;
        authDashAndGo("publisher/site-stats-by-" + tabValue + "/site_id/" + websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из графика по параметрам - Wages, Clicks, ad_requests, rCPM, eCpm, CPC, impressions");
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_revenue");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataClicksFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("ad_requests_impressions");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWebsiteStatisticsClass().getDataCpcFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("ad_requests_r_cpm");
        pagesInit.getWebsiteStatisticsClass().getDataRcpmFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWebsiteStatisticsClass().getDataEcpmFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, Clicks");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(14), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTotalTableForPageImpressions(),"FAIL -> sum page views in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views with ads in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eCpm, CPC, rCpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkCpcInGraph(), "FAIL -> cpc in graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkRcpmInGraph(), "FAIL -> rCpm in graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkEcpmInGraph(), "FAIL -> eCpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, rCpm, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkEcpmInTable(), "FAIL -> eCpm in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkRcpmInTable(), "FAIL -> rCpm in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eRpm, CPC, CTR, rCpm, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalEcpm(), "FAIL -> eCpm total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCpc(), "FAIL -> cpc total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCtr(), "FAIL -> ctr total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalRcpm(), "FAIL -> rCpm total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished -> " + tabName);
    }

    @DataProvider
    public Object[][] byStaticTabsWidgets(){
        return new Object[][]{
                {"By Country stat", "country"},
                {"By Source stat", "traffic-type"},
                {"By Device stat", "device"},
        };
    }

    /**
     * Проверка статистики в интерфейсе BY DAY (dashboard.mgid.com/publisher/site-stats/site_id/1)
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, Clicks</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkStatByDay() {
        log.info("Test is started");
        authDashAndGo("publisher/site-stats/site_id/1");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.YESTERDAY;
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из графика по параметрам - Wages, Clicks, PageViews, rCPM, eCpm, CPC, viewsWithAds");
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_wages");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataClicksFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("ad_requests_to_page_views_with_ads");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWebsiteStatisticsClass().getDataCpcFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("page_views_with_ads_to_page_erpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageErpmFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeaderForStatByDay();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatByDay();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromClickHouse("site = 1");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, Clicks");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(14), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForPageImpressions(),"FAIL -> sum page views view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views with ads in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для page ErPM");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkPageErpmInGraph(), "FAIL -> page ErPM in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для page ErPM, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkPageErpmInTable(), "FAIL -> page ErPM in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для page ErPM, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalPageErpm(), "FAIL -> page ErPM total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalCpc(), "FAIL -> CPC total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalCtr(), "FAIL -> CTR total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test(dataProvider = "periodsByHours")
    public void checkStatByHours(CalendarPeriods period) {
        log.info("Test is started");

        authDashAndGo("publisher/site-stat-by-hours/site_id/60");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(period);
        pagesInit.getWebsiteStatisticsClass().loadDataIfPaginatorDisplayed();

        log.info("получаем стату из Graph по параметрам - Wages, Clicks, ad_requests, rCPM, eCpm, CPC, impressions");
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("page_views_with_ads_to_page_erpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageErpmFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_wages");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataClicksFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("page_views_to_page_views_with_ads");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWebsiteStatisticsClass().getDataCpcFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeaderForStatByDay();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatByDay();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromClickHouse("site = 60");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, Clicks");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(13), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForPageImpressions(),"FAIL -> sum page views view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views with ads in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для page ErPM");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkPageErpmInGraph(), "FAIL -> page ErPM in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для page ErPM, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkPageErpmInTable(), "FAIL -> page ErPM in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для page ErPM, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalPageErpm(), "FAIL -> page ErPM total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalCpc(), "FAIL -> CPC total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalCtr(), "FAIL -> CTR total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statByDay_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] periodsByHours(){

        ZoneId zoneId           = ZoneId.of("Pacific/Honolulu");
        LocalDate dayEnd        = LocalDate.now(zoneId).minusDays(1);
        LocalDate dayStart      = dayEnd.minusDays(1);

        return new Object[][]{
                //{CalendarPeriods.INTERVAL.setDateStart(dayStart).setDateEnd(dayEnd)},
                {CalendarPeriods.YESTERDAY}
        };
    }
}
