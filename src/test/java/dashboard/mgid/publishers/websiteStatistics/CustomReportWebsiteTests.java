package dashboard.mgid.publishers.websiteStatistics;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.statCalendar.CalendarForStatistics;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

import static core.helpers.BaseHelper.getRandomFromArray;

public class CustomReportWebsiteTests extends TestBase {

    private final int websiteId = 1;

    /**
     * Проверка отображения всех показателей и метрик в Конструкторе отчётов
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByAllParams() {
        log.info("Test is started");
        String[] reportParameters = {"date", "device", "traffic_type", "country", "os", "traffic_source"};
        String[] metrics = {"page_views", "visibility_rate", "wages", "cpc", "page_impressions", "clicks", "r_cpm", "ctr", "e_cpm"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, metrics);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(16), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(metrics), "FAIL -> sorted headers");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Дата" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, ViewsWithAds</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByDate() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] metrics = {"page_impressions", "e_cpm", "wages", "clicks"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, metrics);

        log.info("заполняем данными масивы page_impressions, e_cpm and wages");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataEcpmFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(5), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(metrics), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, ViewsWithAds");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views_with_ads in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для e_cpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkEcpmInGraph(), "FAIL -> e_cpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для e_cpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkEcpmInTable(), "FAIL -> e_cpm in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalEcpm(), "FAIL -> eCpm total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Тип устройства" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, Clicks</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для CPC</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CPC</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для CPC</li>
     * <li>5. что при фильтре по deviceType данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.dt00.net/issue/PAS-23125">PAS-23125</a>
     */
    @Test
    public void checkCustomSourceByDeviceType() {
        log.info("Test is started");

        String[] reportParameters = {"device"};
        String[] sortsHeaders = {"cpc", "clicks", "wages"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_WEEK;
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы clicks, wages, cpc");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataClicksFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWebsiteStatisticsClass().getDataCpcFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Clicks");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для CPC");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCpcInGraph(), "FAIL -> cpc in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CPC");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCpcInTable(), "FAIL -> cpc in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для CPC");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCpc(), "FAIL -> CPC total");

        log.info("Проверяем работу фильтра по device");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkWorkFilterByDimensionWithSelect(), "FAIL -> device filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Тип источника" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, ViewsWithAds</li>
     * <li>2. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CTR</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для CTR</li>
     * <li>4. что при фильтре по traffic_type данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByTrafficType() {
        log.info("Test is started");
        String[] reportParameters = {"traffic_type"};
        String[] sortsHeaders = {"page_impressions", "wages", "ctr", "clicks"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_SEVEN;
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы page_impressions, wages");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(5), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, ViewsWithAds");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views_with_ads in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CTR");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCtrInTable(), "FAIL -> CTR in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для CTR");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCtr(), "FAIL -> CTR total");

        log.info("Проверяем работу фильтра по traffic_type");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkWorkFilterByDimensionWithSelect(), "FAIL -> traffic_type filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Страна" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, ViewsWithAds</li>
     * <li>2. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для visibilityRate</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для visibilityRate</li>
     * <li>4. что при фильтре по country данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByCountry() {
        log.info("Test is started");
        int countryForFilter = getRandomFromArray(new int[]{35, 36, 37, 38, 39});
        String[] reportParameters = {"country"};
        String[] sortsHeaders = {"page_views", "visibility_rate", "wages", "page_impressions", "clicks"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_30_DAYS;
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы page_impressions, wages");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(6), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, ViewsWithAds");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum views_with_ads in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для visibilityRate");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalVisibilityRate(), "FAIL -> visibilityRate total");

        log.info("Проверяем работу фильтра по country");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkWorkFilterByDimensionWithSelect(countryForFilter), "FAIL -> country filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "ОС" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for ViewsWithAds</li>
     * <li>2. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для ctr</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для ctr</li>
     * <li>4. что при фильтре по OS данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByOs() {
        log.info("Test is started");
        int osForFilter = getRandomFromArray(new int[]{10, 49, 24});
        String[] reportParameters = {"os"};
        String[] sortsHeaders = {"page_impressions", "clicks", "ctr"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_30_DAYS;
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы page_impressions");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsWithAdsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("сравниваем основные параметры в графике, таблице и total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for ViewsWithAds");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum view_with_ads view in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для ctr");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCtrInTable(), "FAIL -> ctr in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для ctr");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCtr(), "FAIL -> ctr total");

        log.info("Проверяем работу фильтра по os");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkWorkFilterByDimensionWithSelect(osForFilter), "FAIL -> OS filter");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Источник" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для CPC</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CPC</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для CPC</li>
     * <li>5. что при фильтре по traffic_source данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByTrafficSource() {
        log.info("Test is started");
        String[] reportParameters = {"traffic_source"};
        String[] sortsHeaders = {"cpc", "clicks", "wages"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы clicks, wages, cpc");
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWebsiteStatisticsClass().getDataCpcFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeader();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("clicks_revenue");
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataClicksFromGraph();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(websiteId);

        log.info("сравниваем основные параметры в графике, таблице и total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для CPC");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCpcInGraph(), "FAIL -> cpc in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для cpc");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkCpcInTable(), "FAIL -> cpc in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для cpc");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().statBy_checkTotalCpc(), "FAIL -> cpc total");

        log.info("Проверяем работу фильтра по traffic_source");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkWorkFilterByDimensionWithInput(), "FAIL -> traffic_source filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка в табе "Фильтры" работы метрик с типами данных int, double на действия заданные в фильтре(математические действия сравнения)
     * Проверяем фильтр по математическим действиям на "page_views", "page_impressions", "clicks" и с плавающей точкой "wages", "cpc", "e_cpm"
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceOtherParams() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] sortsHeaders = {"page_views", "page_impressions", "visibility_rate", "clicks", "wages", "cpc", "ctr", "e_cpm"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        Assert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkMinOrMaxValueSelect("page_views", "page_impressions", "clicks")
                && pagesInit.getWebsiteStatisticsClass().checkMinOrMaxValueSelect("wages"));
        log.info("Test is finished");
    }

    /**
     * Проверка Создания/Редактирования/Удаления отчёта
     * <p>RKO</p>
     */
    @Test
    public void checkCreateEditDeleteCustomReport() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] sortsHeaders = {"page_views", "page_impressions"};
        goToCustomReportWebsite(websiteId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_WEEK;
        pagesInit.getWebsiteStatisticsClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("create custom report on choose params");
        pagesInit.getWebsiteStatisticsClass().createCustomReport();

        log.info("go to saved custom report and check him");
        goToSavedCustomReport();

        log.info("сравниваем основные параметры в графике, таблице и total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkCustomReportInListInterfaceAndGoHim(pagesInit.getWebsiteStatisticsClass().getCustomReportName()));
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().checkSizeHeadersInBaseTable(3), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("go to saved custom report and delete him");
        goToSavedCustomReport();
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().deleteCustomReport(pagesInit.getWebsiteStatisticsClass().getCustomReportName()), "FAIL -> delete custom report");

        log.info("Test is finished");
    }

    public void goToCustomReportWebsite(int websiteId) {
        authDashAndGo("publisher/site-stats-custom-report/site_id/" + websiteId);
    }

    public void goToSavedCustomReport() {
        authDashAndGo("publisher/site-stats-client-custom-reports/site_id/1");
    }
}
