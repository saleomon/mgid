package dashboard.mgid.publishers.websiteStatistics;

import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

public class WebsiteStatisticsInListTests extends TestBase {

    /**
     * Проверка статистики в интерфейсе список сайтов (dashboard.mgid.com/publisher)
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, IntExchange</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eRpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eRpm</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eRpm</li>
     * </ul>
     * <p>RKO</p>
     */
    //todo RKO @Test - problem with clickHouse
    public void checkStatInListWebSites() {
        log.info("Test is started");
        authDashAndGo("publisher");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_30_DAYS;
        pagesInit.getWebsiteStatisticsClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из графика по параметрам - PageView, PageErpm, Wages, PageImpressions(VIEWS WITH ADS)");
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pageViewsToWages");
        pagesInit.getWebsiteStatisticsClass().getDataPageViewsFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataWagesFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pageImpressionsToPageERpm");
        pagesInit.getWebsiteStatisticsClass().getDataErpmFromGraph();
        pagesInit.getWebsiteStatisticsClass().getDataPageImpressionsFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("intExchange");
        pagesInit.getWebsiteStatisticsClass().getDataIntExchangeFromGraph();
        pagesInit.getWebsiteStatisticsClass().switchParamInGraph("pageViewsToPageRpm");
        pagesInit.getWebsiteStatisticsClass().getDataPageRpmFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWebsiteStatisticsClass().getDataColumnByHeaderForWebsiteList();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWebsiteStatisticsClass().getTotalElementForStatInWebsiteList();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWebsiteStatisticsClass().getTotalDataFromClickHouse("");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, PageImpressions, ViewsWithAds, IntExchange");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkSumGraphSumTableTotalTableForPageImpressions(), "FAIL -> sum PagesViews in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkSumGraphSumTableTotalTableForViewsWithAds(), "FAIL -> sum viewsWithAds in table/graph/total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkSumGraphSumTableTotalTableForIntExchange(), "FAIL -> sum intExchange in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eRpm, pageRpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkErpmInGraph(), "FAIL -> eRpm in graph");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkPageRpmInGraph(), "FAIL -> pageRpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eRpm, pageRpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkErpmInTable(), "FAIL -> eRpm in table");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkPageRpmInTable(), "FAIL -> pageRpm in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eRpm, pageRpm");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkTotalErpm(), "FAIL -> eRpm total");
        softAssert.assertTrue(pagesInit.getWebsiteStatisticsClass().websiteList_checkTotalPageRpm(), "FAIL -> pageRpm total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
