package dashboard.mgid.publishers;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

import java.util.*;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.publisher.locators.DirectDemandLocators.IMAGE_IN_LIST_LABEL;

public class DirectDemandTests extends TestBase {

    private final String addForBlock = "add for block";

    private void goToDirectDemand(){
        authDashAndGo("testEmail71@ex.ua", "publisher/direct-demand");
    }

    public void goToCurrentAdd(String titleAdd) {
        authDashAndGo("testEmail71@ex.ua","publisher/direct-demand/search/" + titleAdd);
    }
    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("create new add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909, </a><a href='https://jira.mgid.com/browse/TA-52153'>TA-52153</a>")
    @Owner("RKO")
    @Test(description = "create new add")
    public void createAdd() {
        log.info("Test is started");
        log.info("create new Add");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        softAssert.assertNotEquals(pagesInit.getDirectDemand().createNewAdd(), 0, "FAIL -> create new add");

        log.info("check settings in list interface");
        goToCurrentAdd(pagesInit.getDirectDemand().getTitle());
        softAssert.assertTrue(pagesInit.getDirectDemand().checkNewAddInList(), "FAIL -> check add in list interface");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), IMAGE_IN_LIST_LABEL),
                serviceInit.getScreenshotService().getExpectedScreenshot("createAddScreenImageInList.png")), "FAIL -> screen in list");

        log.info("check settings in edit interface");
        pagesInit.getDirectDemand().clickEditAddIcon();
        softAssert.assertTrue(pagesInit.getDirectDemand().checkAddInEdit(), "FAIL -> check add in edit interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("edit add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909, </a><a href='https://jira.mgid.com/browse/TA-52153'>TA-52153</a>")
    @Owner("RKO")
    @Test(description = "edit add")
    public void editAdd() {
        log.info("Test is started");
        String addForEdit = "add for edit";
        goToCurrentAdd(addForEdit);
        pagesInit.getDirectDemand().clickEditAddIcon();
        softAssert.assertTrue(pagesInit.getDirectDemand()
                .setPicture("geeraf.png")
                .editAdd(), "FAIL -> edit add");

        log.info("check settings in list interface");
        goToCurrentAdd(pagesInit.getDirectDemand().getTitle());
        softAssert.assertTrue(pagesInit.getDirectDemand().checkNewAddInList(), "FAIL -> check add in list interface");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), IMAGE_IN_LIST_LABEL),
                serviceInit.getScreenshotService().getExpectedScreenshot("editAddScreenImageInList.png")), "FAIL -> screen in list");

        log.info("check settings in edit interface");
        pagesInit.getDirectDemand().clickEditAddIcon();
        softAssert.assertTrue(pagesInit.getDirectDemand().checkAddInEdit(), "FAIL -> check add in edit interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("delete add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909</a>")
    @Owner("RKO")
    @Test(description = "delete add")
    public void deleteAdd() {
        log.info("Test is started");
        String addForDelete = "add for delete";
        goToCurrentAdd(addForDelete);
        pagesInit.getDirectDemand().clickDeleteAddIcon();
        goToCurrentAdd(addForDelete);
        softAssert.assertFalse(pagesInit.getDirectDemand().isDisplayedAnyAdd(), "FAIL -> add is displayed after delete");
        softAssert.assertEquals(operationMySql.getDirectPublisherDemandAds().getDroped(addForDelete), 1, "FAIL -> db direct_publisher_demand_ads.droped != 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("block/unblock add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909</a>")
    @Owner("RKO")
    @Test(description = "block/unblock add")
    public void blockUnblockAdd() {
        log.info("Test is started");
        goToCurrentAdd(addForBlock);
        pagesInit.getDirectDemand().clickBlockUnBlockAddIcon();
        softAssert.assertEquals(pagesInit.getDirectDemand().getStatus(), "Blocked", "FAIL -> status add 'Blocked'");
        softAssert.assertEquals(pagesInit.getDirectDemand().getBlockUnblockAddStatus(), "Unblock", "FAIL -> icon title 'Unblock'");
        softAssert.assertEquals(operationMySql.getDirectPublisherDemandAds().getEnabled(addForBlock), 0, "FAIL -> db direct_publisher_demand_ads.enabled != 0");

        pagesInit.getDirectDemand().clickBlockUnBlockAddIcon();
        softAssert.assertEquals(pagesInit.getDirectDemand().getStatus(), "Active", "FAIL -> status add 'Active'");
        softAssert.assertEquals(pagesInit.getDirectDemand().getBlockUnblockAddStatus(), "Block", "FAIL -> icon title 'Block'");
        softAssert.assertEquals(operationMySql.getDirectPublisherDemandAds().getEnabled(addForBlock), 1, "FAIL -> db direct_publisher_demand_ads.enabled != 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("check validation URL for add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909</a>")
    @Owner("RKO")
    @Test(dataProvider = "dataValidationUrlAdd", description = "check validation URL for add")
    public void validationUrlAdd(String url, String message) {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        softAssert.assertEquals(pagesInit.getDirectDemand()
                .setUrl(url)
                .createNewAdd(), 0, "FAIL -> add id != 0");

        softAssert.assertEquals(pagesInit.getDirectDemand().getCustomAddError(), message);
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataValidationUrlAdd() {
        return new Object[][]{
                {"http://test.com", "Error: Wrong protocol"},
                {"https://f", "Invalid domain min length"}
        };
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("check validation Image for add <a href='https://jira.mgid.com/browse/KOT-2918'>KOT-2918</a>, <a href='https://jira.mgid.com/browse/KOT-2909'>KOT-2909</a>")
    @Owner("RKO")
    @Test(dataProvider = "dataValidationImageAdd", description = "check validation Image for add")
    public void validationImageAdd(String image, String message) {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        softAssert.assertEquals(pagesInit.getDirectDemand()
                .setPicture(image)
                .createNewAdd(), 0, "FAIL -> add id != 0");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(message));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataValidationImageAdd() {
        return new Object[][]{
                {"leo.gif", "MAXIMUM ALLOWED SIZE FOR FILE - 5MB. CHOSEN FILE - 7.33MB"},
                {"monochromeImage.jpg", "Minimum expected width for image 'monochromeImage.jpg' should be '492' but '400' detected,Minimum expected height for image 'monochromeImage.jpg' should be '328' but '225' detected"},
                {"492_327_validation1.jpeg", "Minimum expected height for image '492_327_validation1.jpeg' should be '328' but '327' detected"}
        };
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("filter search add By title, url, name <a href='https://jira.mgid.com/browse/KOT-2917'>KOT-2917, </a><a href='https://jira.mgid.com/browse/TA-52227'>TA-52227</a>")
    @Owner("RKO")
    @Test(dataProvider = "dataFilterSearchAdd", description = "filter search add")
    public void filterSearchAdd(String value) {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().fillValueInSearchFilter(value);
        softAssert.assertEquals(pagesInit.getDirectDemand().getAddsOnPage().size(), 1, "FAIL -> count adds on page");
        softAssert.assertEquals(pagesInit.getDirectDemand().getAddsOnPage().get(0).attr("data-teaser-id"), "3", "FAIL -> teaser id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataFilterSearchAdd() {
        return new Object[][]{
                {addForBlock},
                {"https://demandurl3.com"},
                {"stage 3"}
        };
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("filter status add <a href='https://jira.mgid.com/browse/KOT-2917'>KOT-2917</a>")
    @Owner("RKO")
    @Test(description = "filter status add")
    public void filterStatusAdd() {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().selectStatusFilter("Active");
        pagesInit.getDirectDemand().clickFilterButton();
        int activeAdds = pagesInit.getDirectDemand().getAddsOnPage().size();
        softAssert.assertTrue(pagesInit.getDirectDemand().getStatuses().stream().allMatch(i -> i.equals("Active")));

        pagesInit.getDirectDemand().selectStatusFilter("Blocked");
        pagesInit.getDirectDemand().clickFilterButton();
        int blockedAdds = pagesInit.getDirectDemand().getAddsOnPage().size();
        softAssert.assertTrue(pagesInit.getDirectDemand().getStatuses().stream().allMatch(i -> i.equals("Blocked")));

        pagesInit.getDirectDemand().selectStatusFilter("All");
        pagesInit.getDirectDemand().clickFilterButton();
        softAssert.assertEquals(pagesInit.getDirectDemand().getAddsOnPage().size(), (activeAdds + blockedAdds), "FAIL -> status 'All' wrong count adds");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("filter date created add <a href='https://jira.mgid.com/browse/KOT-2917'>KOT-2917</a>")
    @Owner("RKO")
    @Test(description = "filter date created add")
    public void filterDateCreatedAdd() {
        log.info("Test is started");
        log.info("Setting Preconditions");
        String formData = helpersInit.getBaseHelper().dateFormat(helpersInit.getBaseHelper().getCurrentDateWithDateOffset("Pacific/Honolulu", -4), ("yyyy-MM-dd HH:mm:ss"));
        operationMySql.getDirectPublisherDemandAds().updateCreatedAtTime(formData, 4);

        goToDirectDemand();

        CalendarPeriods period = CalendarPeriods.LAST_SEVEN;
        ArrayList<String> startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);
        sleep(1000);
        softAssert.assertEquals(pagesInit.getDirectDemand().getAddsOnPage().size(), 1, "FAIL -> count adds on page");
        softAssert.assertEquals(pagesInit.getDirectDemand().getAddsOnPage().get(0).attr("data-teaser-id"), "4", "FAIL -> teaser id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Add possibility to crop image after image upload")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Set manual focal point and check saved data to direct_publisher_demand_ads.cloudinary_effects Create action</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52155\">Ticket TA-52155</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Set manual focal point and check saved data to direct_publisher_demand_ads.cloudinary_effects create")
    public void checkFocalPointValuesCreateAdd() {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        pagesInit.getDirectDemand()
                .useManualFocalPoint(true)
                .setFocalPoint(144)
                .createNewAdd();

        String options = operationMySql.getDirectPublisherDemandAds().getCloudinaryEffects(pagesInit.getDirectDemand().getTitle());

        log.info("Cloudinary effects - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_325\""), "x_325");
        softAssert.assertTrue(options.contains("\"y_211\""), "y_211");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Add possibility to crop image after image upload")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Set manual focal point and check saved data to direct_publisher_demand_ads.cloudinary_effects Edit action</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52155\">Ticket TA-52155</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Set manual focal point and check saved data to direct_publisher_demand_ads.cloudinary_effects edit")
    public void checkFocalPointValuesEditAdd() {
        log.info("Test is started");
        String addForEdit = "add for fedit";
        goToCurrentAdd(addForEdit);
        pagesInit.getDirectDemand().clickEditAddIcon();
        pagesInit.getDirectDemand()
                .useManualFocalPoint(true)
                .setFocalPoint(124)
                .editAdd();

        String options = operationMySql.getDirectPublisherDemandAds().getCloudinaryEffects(pagesInit.getDirectDemand().getTitle());

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_284\""), "x_284");
        softAssert.assertTrue(options.contains("\"y_252\""), "y_252");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Add possibility to crop image after image upload")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check cancel edits of manual focal point</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52155\">Ticket TA-52155</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check cancel edits of manual focal point")
    public void checkFocalPointCancelEditOption() {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        pagesInit.getDirectDemand()
                .fillImageByLocal("happiness.jpeg");
        pagesInit.getDirectDemand().setFocalPoint(133).setFocalPoint(false);
        pagesInit.getDirectDemand().cancelManualFocalPoint();
        softAssert.assertFalse(pagesInit.getDirectDemand().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getDirectDemand()
                .setNeedToEditImage(false)
                .createNewAdd();

        String options = operationMySql.getDirectPublisherDemandAds().getCloudinaryEffects(pagesInit.getDirectDemand().getTitle());

        softAssert.assertFalse(options.contains("\"g_xy_center\""), "Fail - g_xy_center");
        softAssert.assertFalse(options.contains("\"x_2\""), "Fail - x_236");
        softAssert.assertFalse(options.contains("\"y_2\""), "Fail - y_299");
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Add possibility to crop image after image upload")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check reset data of manual focal point to default</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52155\">Ticket TA-52155</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check reset data of manual focal point to default")
    public void checkResetFocalPointDataToDefault() {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        pagesInit.getDirectDemand()
                .fillImageByLocal("happiness.jpeg");
        pagesInit.getDirectDemand().setFocalPoint(133).setFocalPoint(true);
        pagesInit.getDirectDemand().resetDataFocalPointToDefault();
        softAssert.assertFalse(pagesInit.getDirectDemand().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getDirectDemand()
                .setNeedToEditImage(false)
                .createNewAdd();

        String options = operationMySql.getDirectPublisherDemandAds().getCloudinaryEffects(pagesInit.getDirectDemand().getTitle());

        softAssert.assertFalse(options.contains("\"g_xy_center\""), "Fail - g_xy_center");
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Add possibility to crop image after image upload")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit data of manual focal point </>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52155\">Ticket TA-52155</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check edit data of manual focal point")
    public void checkEditFocalPointData() {
        log.info("Test is started");
        goToDirectDemand();
        pagesInit.getDirectDemand().clickAddNewCreativeButton();
        pagesInit.getDirectDemand()
                .fillImageByLocal("happiness.jpeg");
        pagesInit.getDirectDemand().setFocalPoint(133).setFocalPoint(true);
        pagesInit.getDirectDemand().editDataFocalPoint();
        Assert.assertTrue(pagesInit.getDirectDemand().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Export data in the Direct Demand Interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check export: all headers and check value for Ads(Id:4) </>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52154\">Ticket TA-52154</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check export functionality")
    public void checkCsvInListDirectDemand() {
        log.info("Test is started");
        String[] headers = {"ID", "Status", "Name", "Title", "Imps", "CTR, %", "Clicks/Goal", "Start date", "End date", "Websites"};

        goToDirectDemand();

        softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                .setCustomRowNumber(2)
                .loadExportedFileDash(), "FAIL -> load");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Status", "Blocked"), "FAIL -> Status");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Name", "stage 2"), "FAIL -> Name");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Title", "add for wewer"), "FAIL -> Title");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Imps", "0"), "FAIL -> Imps");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "0"), "FAIL -> CTR, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Clicks/Goal", "0/16"), "FAIL -> Clicks/Goal");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Websites", "testsite118.com"), "FAIL -> Websites");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Statistics by day for Publisher campaigns studio creative")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check stat by day (table date/table total/data db)</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52228\">Ticket TA-52228</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check stat by day (table date/table total/data db)")
    public void checkStatByDay() {
        log.info("Test is started");
        authDashAndGo("testEmail71@ex.ua","publisher/direct-demand-stats/ad_id/1");

        log.info("choose random period in calendar");
        CalendarPeriods getRandomPeriod = CalendarPeriods.THIS_WEEK;
        pagesInit.getDirectDemand().selectCalendarPeriodJs(getRandomPeriod);

        log.info("set data array (Impressions, Clicks, etc.) from column table");
        pagesInit.getDirectDemand().getDataColumnByHeaderForStatByDay();

        log.info("get locators TOTAL (Impressions, Clicks, etc.) by name<th>");
        pagesInit.getDirectDemand().getTotalElementForStatByDay();

        log.info("get from DB TOTAL for Impressions, Clicks, etc.");
        pagesInit.getDirectDemand().getTotalDataFromPublisherStatisticsClickHouse(430);

        log.info("Compare sum(Table) with Total(field) with sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks");
        softAssert.assertTrue(pagesInit.getDirectDemand().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/total");

        log.info("Check that cell data in Table count right for CTR");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkCtrInTable(), "FAIL -> ctr in table");

        log.info("Check TOTAL Table for CTR");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkTotalCtr(), "FAIL -> CTR total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Statistics by day for Publisher campaigns studio creative")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check export: all headers and check value for on day</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52228\">Ticket TA-52228</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check export: all headers and check value for on day")
    public void checkCsvStatByDay() {
        log.info("Test is started");
        String[] headers = {"Date", "Imps", "CTR, %", "Clicks"};

        authDashAndGo("testEmail71@ex.ua","publisher/direct-demand-stats/ad_id/1");

        log.info("Choose period in calendar");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_SEVEN;
        pagesInit.getDirectDemand().selectCalendarPeriodJs(getRandomPeriod);

        softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                .loadExportedFileDash(), "FAIL -> load");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Imps", "4"), "FAIL -> Imps");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "50"), "FAIL -> CTR, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Clicks", "2"), "FAIL -> Clicks/Goal");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Statistics by website for Publisher campaigns studio creative")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check stat by site (table date/table total/data db)</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52229\">Ticket TA-52229</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check stat by site (table date/table total/data db)")
    public void checkStatsBySite() {
        log.info("Test is started");
        authDashAndGo("testEmail71@ex.ua","publisher/direct-demand-stats-by-site/ad_id/4");

        log.info("choose random period in calendar");
        CalendarPeriods getRandomPeriod = CalendarPeriods.THIS_WEEK;
        pagesInit.getDirectDemand().selectCalendarPeriodJs(getRandomPeriod);

        log.info("set data array (Impressions, Clicks, etc.) from column table");
        pagesInit.getDirectDemand().getDataColumnByHeaderForStatByDay();

        log.info("get locators TOTAL (Impressions, Clicks, etc.) by name<th>");
        pagesInit.getDirectDemand().getTotalElementForStatByDay();

        log.info("get from DB TOTAL for Impressions, Clicks, etc.");
        pagesInit.getDirectDemand().getTotalDataFromPublisherStatisticsClickHouse(514, 515);

        log.info("Compare sum(Table) with Total(field) with sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks");
        softAssert.assertTrue(pagesInit.getDirectDemand().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/total");

        log.info("Check that cell data in Table count right for CTR");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkCtrInTable(), "FAIL -> ctr in table");

        log.info("Check TOTAL Table for CTR");
        softAssert.assertTrue(pagesInit.getDirectDemand().statByDay_checkTotalCtr(), "FAIL -> CTR total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("direct publisher demand")
    @Story("Statistics by website for Publisher campaigns studio creative")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check export: all headers and check value for on site</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52229\">Ticket TA-52229</a></li>\n" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "Check export: all headers and check value for on site")
    public void checkCsvStatBySite() {
        log.info("Test is started");
        String[] headers = {"Websites", "Imps", "CTR, %", "Clicks"};

        authDashAndGo("testEmail71@ex.ua","publisher/direct-demand-stats-by-site/ad_id/7");

        log.info("Choose period in calendar");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_SEVEN;
        pagesInit.getDirectDemand().selectCalendarPeriodJs(getRandomPeriod);

        softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                .loadExportedFileDash(), "FAIL -> load");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Imps", "30"), "FAIL -> Imps");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR, %", "36.66"), "FAIL -> CTR, %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Clicks", "11"), "FAIL -> Clicks/Goal");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
