package dashboard.mgid.publishers;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.Arrays;
import java.util.List;

import static pages.dash.publisher.variables.PayoutsVariables.*;
import static testData.project.ClientsEntities.*;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;

public class MgidPayoutsTests extends TestBase {

    public MgidPayoutsTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = CLIENTS_PAYOUTS_LOGIN;
    }

    private final String clientId = "2001";

    /**
     * Add wallet - ACH bank-to-bank transfers and check wallet params
     * <p>Author RKO/Moved by AIA
     */
    @Test
    public void addAndCheckAchBankTransferPurse() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "145");
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method ACH bank-to-bank transfer *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().addAchTransferPurse("btb-transfer", "ACH", "mgid");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayOutType("btb-transfer", "ACH", "mgid"),
                "FAIL -> added ACH bank-to-bank transfer");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().editAchTransferPurse("btb-transfer", "ACH", "mgid");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayOutType("btb-transfer", "ACH", "mgid"),
                "FAIL -> edited ACH bank-to-bank transfer");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления и редактирования кошелька PayPal в дешборде
     * Если у клиента не выбран регион выплат, тогда при добавлении кошелька PayPal с MGID должен создаваться кошелек
     * на валюту PayPal MGID Inc, USD (id=89)
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22013">Ticket TA-22013</a>
     * <p> Author AIA
     */
    @Test
    public void addAndEditPayPalPurseTest() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "89");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo("publisher/payouts");
        log.info("* Add payment method PayPal*");
        pagesInit.getPayoutsClass().addPurse("PayPal", "");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("PayPal"),
                "FAIL -> Check payout type after adding!");
        softAssert.assertEquals(pagesInit.getPayoutsClass().getPurseId("PayPal"), "89",
                "FAIL -> check Id after adding!");
        helpersInit.getBaseHelper().refreshCurrentPage();
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().editPurse("PayPal", "");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("PayPal"),
                "FAIL -> Purse type after editing!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayPalPurseValue(),
                "FAIL -> Purse value after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления и редактирования кошелька Webmoney WMZ в дешборде
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22013">Ticket TA-22013</a>
     * <p> Author AIA
     */
    @Test
    public void addAndEditWebmoneyPurseTest() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "83");
        authDashAndGo("publisher/payouts");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        log.info("* Add payment method Webmoney USD*");
        pagesInit.getPayoutsClass().addPurse("Webmoney", "Z431071031918");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Webmoney"));
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().editPurse("Webmoney", "Z573184336595");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Webmoney"));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check "Hold/Resume payments" button
     * <p>Author AIA</p>
     */
    @Test(dependsOnMethods = "addAndEditPayPalPurseTest")
    public void checkHoldResumeButton() {
        try {
            log.info("Test is started");
            log.info("Enable privilege 'publisher/display_hold_resume_button'");
            log.info("* Insert payments *");
            operationMySql.getPayments().pushDumpToTable("mysql/partners/payments/paymentsMgid.sql");
            authDashAndGo("publisher/payouts");
            log.info("* Check pause payments *");
            pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldPaymentsConfirmationPopupText(),
                    "FAIL -> pause payments!");
            helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
            softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                            "CURRENTLY YOUR PAYMENTS ARE ON HOLD; CLICK \"RESUME PAYMENTS\" TO START GETTING PAID."),
                    "FAIL -> pause payments message!");
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkResumePaymentsButton(),
                    "FAIL -> resume payments button!");
            log.info("* Check payments status *");
            authDashAndGo("publisher/payouts");
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymentsStatus("Rejected"),
                    "FAIL -> statuses!");
            log.info("* Check resume payments *");
            pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkResumePaymentsConfirmationPopupText(),
                    "FAIL -> pause payments!");
            helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldPaymentsButton(),
                    "FAIL -> resume payments!");
            log.info("Disable privilege 'publisher/display_hold_resume_button'");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE,
                    "publisher/display_hold_resume_button");
            log.info("Check that 'Hold/Resume payments' button is not displayed on page 'Payouts'");
            authDashAndGo("publisher/payouts");
            softAssert.assertTrue(pagesInit.getPayoutsClass().checkHoldResumePaymentsButtonInClient(false),
                    "FAIL -> check privileges");
            softAssert.assertAll();
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE,
                    "publisher/display_hold_resume_button");
        }
        log.info("Test is finished");
    }

    /**
     * Check available payout types for client with Euro currency
     *
     * @see <a href="https://docs.mgid.com/napravleniya/ploshhadki/zarabotok/zarabotok-pablisherov-publisher-currency/#i-2">
     * Publishers earnings</a>
     * @see <a href="https://docs.mgid.com/prilozheniya/dashboard-publishers-balancehistoryus/#i-3">
     * Payouts documentation</a>
     * Author AIA
     */
    @Test
    public void checkEuroClientPurses() {
        log.info("Test is started");
        List<String> pursesTypes = Arrays.asList("PAYPAL", "BANK TRANSFERS");
        List<String> bankTransfersTypes = Arrays.asList("TIPALTI BANK TRANSFER", "INTERNATIONAL BANK-TO-BANK TRANSFERS");

        authDashAndGo(CLIENTS_PAYOUTS_EURO_LOGIN, "publisher/payouts");
        log.info("* Check that Webmoney isn't available *");
        pagesInit.getPayoutsClass().openAddPursePopup();
        softAssert.assertFalse(pagesInit.getPayoutsClass().checkPursesTypes("WEBMONEY"),
                "FAIL -> euro purses set!");
        log.info("* Check that other types of payments are available *");
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getPayoutsClass().openAddPursePopup();
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPursesTypes(pursesTypes),
                "FAIL -> available purses types!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkBankTransfers(bankTransfersTypes),
                "FAIL -> available bank transfers types!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка добавления типа выплаты 'International bank-to-bank transfer' для стран со стандартным набором полей
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24024">Ticket TA-24024</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkInternationalB2bTransfers() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "160");
        authDashAndGo("publisher/payouts");

        log.info("* Add payment method International bank-to-bank transfer *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().addInternationalBankToBankTransfers(
                "btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutType(
                        "160", "btb-transfer", "International", "mgid"),
                "FAIL -> added International bank-to-bank transfer!");

        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().editInternationalTransferPurse(
                "btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));

        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutType(
                        "160", "btb-transfer", "International", "mgid"),
                "FAIL -> edited International bank-to-bank transfer!");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Проверка добавления типа выплаты 'International bank-to-bank transfer' для стран с дополнительными полей
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24024">Ticket TA-24024</a>
     * Author AIA
     */
    @Test
    public void checkInternationalB2bTransferForCustomCountries() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "160");
        authDashAndGo("publisher/payouts");

        log.info("* Add payment method International bank-to-bank transfer *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().addInternationalBankToBankTransfersForCustomCountries("btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));

        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutTypeForCustomCountries("btb-transfer", "International", "mgid"),
                "FAIL -> added International bank-to-bank transfer!");

        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().editInternationalTransferPurseForCustomCountries("btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));

        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutTypeForCustomCountries("btb-transfer", "International", "mgid"),
                "FAIL -> edited International bank-to-bank transfer!");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Проверка невозможности смены favorite wallet если у клиента есть заявки в статусах Approved и Pending
     * Cases
     * <ul>
     * <li>Подаём 2 заявки (approved, pending), проверяем, что нет возможности изменить favorite wallet</li>
     * <li>Одобряем одну заявку, изменяем кошелёк, проверяем предупреждающее сообщение и то, что кошелек не изменился</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21339">Ticket TA-21339</a>
     * <p>Author AIA</p>
     */
    @Test
    public void changeWalletInPayments() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_EURO_LOGIN, "publisher/payouts");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkChangeFavoritePurseDisabled(),
                "FAIL -> change favorite purse disabled!");
        authCabAndGo("wages/clients-payments?status=1&client_id=" + CLIENT_MGID_EURO_ID);
        pagesInit.getCabPublisherPayouts().checkPendingRequestsAndClearThem();
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().selectRandomFavoriteWallet();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(changeFavoriteWalletBlocked),
                "FAIL -> message change favorite wallet blocked!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента выбрано "Asia", при добавлении кошелька в дашборде "INTERNATIONAL BANK-TO-BANK TRANSFERS"
     * (InterTipalti) необходимо создавать кошелек на ФО InterTipalti MGID Asia (id=222)
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addInternationalB2bTransfersForAsiaRegion() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses("2015", "222");
        authDashAndGo("payments.asia.client.2015@mgid.com", "publisher/payouts");
        log.info("* Add payment method International bank-to-bank transfer Asia*");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().addInternationalBankToBankTransfers(
                "btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutType(
                        "222", "btb-transfer", "Asia", "mgid"),
                "FAIL -> added International bank-to-bank transfer Asia!");

        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo("publisher/payouts");
        pagesInit.getPayoutsClass().editInternationalTransferPurse(
                "btb-transfer", "International", "mgid");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkInternationalPayOutType(
                        "222", "btb-transfer", "Asia", "mgid"),
                "FAIL -> edited International bank-to-bank transfer Asia!");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента стоит регион выплат Asia, тогда при добавлении кошелька PayPal с MGID должен создаваться
     * кошелек на валюту PayPal MGID Asia, USD (id=223)
     * <p>Author AIA</p>
     */
    @Test
    public void addAndEditPayPalPurseForAsiaRegionTest() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses("2015", "223");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        authDashAndGo(CLIENTS_PAYOUTS_ASIA_LOGIN, "publisher/payouts");
        log.info("* Add payment method PayPal *");
        pagesInit.getPayoutsClass().addPurse("PayPal", "");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("PayPal"),
                "FAIL -> check type after adding!");
        softAssert.assertEquals(pagesInit.getPayoutsClass().getPurseId("PayPal"), "223",
                "FAIL -> check Id after adding!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add wallet Paymaster24 MGID Asia, USD (id=249) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-28487">Ticket TA-28487</a>
     * <p>Author AIA</p>
     */
    //todo https://jira.mgid.com/browse/KOT-3324 @Test
    public void addAndCheckPaymaster24PurseMgid() {
        log.info("Test is started");
        log.info("Preconditions: delete clients current type purses");
        operationMySql.getClientsPurses().deletePurses(clientId, "249");
        authDashAndGo("publisher/payouts");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        log.info("* Add payment method Paymaster24 MGID Asia, USD*");
        pagesInit.getPayoutsClass().addPaymaster24Purse("Paymaster24", "Z431071031918");
        log.info("Get confirmation link from mail and go it");
        String confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after creation *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after creating!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after creating!");
        log.info("* Edit payment method *");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetterMgid);
        pagesInit.getPayoutsClass().editPaymaster24("Paymaster24", "Z573184336595");
        log.info("Get confirmation link from mail and go it");
        confirmUrl = helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMail(subjectLetterMgid)).get(0);
        authDashAndGo(confirmUrl.substring(confirmUrl.indexOf("/profile")));
        log.info("* Check payment method after editing *");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPayoutType("Paymaster24"),
                "FAIL -> Payout type after editing!");
        softAssert.assertTrue(pagesInit.getPayoutsClass().checkPaymaster24Purse("Paymaster24"),
                "FAIL -> Paymaster24 requisites after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description("Валидация добавления кошелька WMZ для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить WMZ кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test(description = "Добавление кошелька Webmoney для клиента из Украины для подсети MGID")
    public void forbiddenToAddWebMoneyWalletFromUkraine() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN, "publisher/payouts");
        log.info("* Add payment method Webmoney USD*");
        pagesInit.getPayoutsClass().addPurse("Webmoney", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "PLEASE USE A DIFFERENT PAYMENT METHOD."),
                "FAIL -> WMZ wallet is added correctly!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Добавление кошельков для выплат в интерфейсе `Payouts` в Дешборде")
    @Description("Валидация добавления кошелька Paymaster24 для Украины.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности добавить Paymaster24 кошелек клиенту, если у кошелька стоит владелец из Украины</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/KOT-2009\">Ticket KOT-2009</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    //todo https://jira.mgid.com/browse/KOT-3324 @Test (description = "Добавление кошелька Paymaster24 для клиента из Украины для подсети MGID")
    public void forbiddenToAddWalletPaymaster24FromUkraine() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN, "publisher/payouts");
        log.info("* Add payment method Paymaster24 USD*");
        pagesInit.getPayoutsClass().addPaymaster24PurseUkraine("Paymaster24", "Z394313302416");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(
                        "PLEASE USE A DIFFERENT PAYMENT METHOD."),
                "FAIL -> Paymaster24 wallet is added correctly!!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
