package dashboard.mgid.publishers.widgets;

import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import static core.helpers.BaseHelper.getRandomFromArray;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_INT_EXCHANGE_ID;

public class PlacementConfigurationTests extends TestBase {

    public PlacementConfigurationTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    /**
     * <ul>
     * <li>Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id = 1, placements_count = 9)</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_1_placements_9() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 52);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().chooseColumns(3);
        pagesInit.getWidgetClass().chooseRows(3);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(6);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id = 1, placements_count = 1)</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_1_placements_1() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 52);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.IN_ARTICLE);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(1);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id = 1) RE-save old widget</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_4_widgetWithSetPlacements() {
        log.info("Test is started");
        authDashAndGo("publisher/edit-widget/id/" + 96);
        pagesInit.getWidgetClass().setIntExchangePlacementItems(Arrays.asList("1", "3", "5"));
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.singletonList("2"), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        authDashAndGo("publisher/edit-widget/id/" + 96);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.singletonList("2"), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23994">...</a>
     * Для виджетов с Type Mobile и IAB в поле tickers_composite_placement_configuration .type всегда записывается тип сотрудничества wages
     * Case
     * 1. choose website with cooperation type 'int_exchange'
     * 2. create widget (mobile/banner) with switch on int_exchange
     * 3. check tumbler 'int_exchange' true
     * 4. check tickers_composite_placement_configuration.type = int_exchange for all places
     * RKO
     */
    @Test
    public void checkWorkIntExchangeTumblerForMobileOrBannerWidgets() {
        log.info("Test is started");
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_INT_EXCHANGE_ID);

        WidgetTypes.Types[] type_mass = new WidgetTypes.Types[]{WidgetTypes.Types.MOBILE, WidgetTypes.Types.BANNER};
        pagesInit.getWidgetClass().chooseWidgetType(type_mass[new Random().nextInt(type_mass.length)]);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseAllPlacementWidgetByIntExchange();
        pagesInit.getWidgetClass().saveWidgetSettings();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkStateIntExchangeTumbler(true), "FAIL -> int_exchange tumbler doesn't switch on in ui-interface");
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(
                        pagesInit.getWidgetClass().getWidgetId()).stream().allMatch(i -> i.equals("int_exchange")),
                "FAIL -> placement type don't correct");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Выключать ВО для смартов по умолчанию, если сайт виджета относится к tier 3-6
     * Проверяем дефолтное состояние тумблера 'INTERNAL EXCHANGE' и состояние placement для сайтов с tier=1-2
     * @see <a href="https://jira.mgid.com/browse/TA-27372">TA-27372</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_1_and_2_default() {
        int[] sites = {83, 84};
        log.info("Test is started");
        authDashAndGo("testEmail54@ex.ua", "publisher");
        pagesInit.getWidgetClass().setIntExchangePlacementItems(Arrays.asList("13", "14", "15"));

        // check default state before save
        for(int site : sites) {
            authDashAndGo("publisher/add-widget/site/" + site);
            softAssert.assertTrue(pagesInit.getWidgetClass().checkStateIntExchangeTumbler(true), "FAIL -> state tumbler for site: " + site);
            softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> placement configuration for site: " + site);
        }

        // check default state after save
        int currentSite = getRandomFromArray(sites);
        authDashAndGo("publisher/add-widget/site/" + currentSite);
        pagesInit.getWidgetClass().saveWidgetSettings();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkStateIntExchangeTumbler(true), "FAIL -> state tumbler after save for site: " + currentSite);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> placement configuration after save for site: " + currentSite);

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Выключать ВО для смартов по умолчанию, если сайт виджета относится к tier 3-6
     * Проверяем дефолтное состояние тумблера 'INTERNAL EXCHANGE' и состояние placement для сайтов с tier=3-6,null
     * @see <a href="https://jira.mgid.com/browse/TA-27372">TA-27372</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_null_AND_3_between_6_default() {
        int[] sites = {82, 85, 86, 87, 88};
        log.info("Test is started");
        authDashAndGo("testEmail54@ex.ua", "publisher");

        // check default state before save
        for(int site : sites){
            authDashAndGo("publisher/add-widget/site/" + site);
            softAssert.assertTrue(pagesInit.getWidgetClass().checkStateIntExchangeTumbler(false), "FAIL -> state tumbler for site: " + site);
            softAssert.assertFalse(pagesInit.getWidgetClass().isPositionBlockDisplayed(), "FAIL -> displayed position block for site: " + site);
        }

        // check default state after save
        int currentSite = getRandomFromArray(sites);
        authDashAndGo("publisher/add-widget/site/" + currentSite);
        pagesInit.getWidgetClass().saveWidgetSettings();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkStateIntExchangeTumbler(false), "FAIL -> state tumbler after save for site: " + currentSite);
        softAssert.assertFalse(pagesInit.getWidgetClass().isPositionBlockDisplayed(), "FAIL -> displayed position block after save for site: " + currentSite);

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
