package dashboard.mgid.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.EndPoints;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.Collections;

public class PlacementConfiguration1Tests extends TestBase {

    public PlacementConfiguration1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    @Feature("Placement Configuration")
    @Story("Placement Configuration")
    @Description("1. Проверяем смену мест при создании и редактировании виджета с направлениями: int_exchange and video <a href=\"https://youtrack.mgid.com/issue/TA-24458\">TA-24458</a> \n" +
                 "2. Show amount of placements in a widget with booked for Direct Demand <a href=\"https://youtrack.mgid.com/issue/TA-52151\">TA-52151</a>")
    @Owner("RKO")
    @Test(description = "check placement int_exchange and video + Show amount of placements in a widget")
    public void placementConfigurationIntExchangeAndVideo() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and Video tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 5);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().switchVideoTumbler(true);
        pagesInit.getWidgetClass().enableVideoContentCheckboxes(true, false);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(5);
        pagesInit.getWidgetClass().choosePlacementByVideo();
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        log.info("edit PlacementConfiguration");
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(5);
        pagesInit.getWidgetClass().choosePlacementByVideo();
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("check after edit PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after edit widget");

        log.info("Show amount of placements in a widget");
        authCabAndGo(EndPoints.widgetsCustomIdUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Wages"), "9", "FAIL -> Show amount of placements: wages");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Internal exchange"), "5", "FAIL -> Show amount of placements: Internal exchange");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Video"), "1", "FAIL -> Show amount of placements: Video");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Не сохраняются изменения мест ВО в виджете из Дашборда</li>
     * <li>2. Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id != 1, placements_count = 3) </li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24458">TA-24458</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_4_disablesPlacements_1() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 1);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().chooseColumns(1);
        pagesInit.getWidgetClass().chooseRows(3);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(1);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.singletonList("1"), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.singletonList("1"), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Не сохраняются изменения мест ВО в виджете из Дашборда</li>
     * <li>2. Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id != 1, placements_count = 9) </li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24458">TA-24458</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_4_disablesPlacements_2() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 1);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().chooseColumns(3);
        pagesInit.getWidgetClass().chooseRows(3);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(4);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Arrays.asList("1", "2"), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Arrays.asList("1", "2"), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id != 1, placements_count = 1) </li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_4_disablesPlacements_0() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 1);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.IN_ARTICLE);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(1);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id != 1, placements_count = 9) - Without choose placements</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_WRONG_withoutPlacements() {
        log.info("Test is started");
        authDashAndGo("publisher/add-widget/site/" + 1);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().chooseColumns(3);
        pagesInit.getWidgetClass().chooseRows(3);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().clickSaveWidgetButton();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("At least one link should be enabled for “Internal exchange”"), "FAIL -> fail message");
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>Запрещать включать ВО на первых местах для T2-T6 сайтов (clients_sites.tier_id = 1, placements_count = 3)</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27472">TA-27472</a>
     * <p>RKO</p>
     */
    @Test
    public void placementConfiguration_tier_1_placements_3() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("publisher/add-widget/site/" + 52);
        pagesInit.getWidgetClass().chooseWidgetType(WidgetTypes.Types.UNDER_ARTICLE);
        pagesInit.getWidgetClass().chooseColumns(1);
        pagesInit.getWidgetClass().chooseRows(3);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(3);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement BEFORE create widget");
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Collections.emptyList(), "FAIL -> disable placement AFTER create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> PlacementConfiguration after create widget");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Placement Configuration")
    @Story("direct publisher demand")
    @Description("1. Add new widget E2E for site with cooperation type: wages, int_exchange, direct_demand and tier 1/3 <a href='https://jira.mgid.com/browse/VT-28251'>VT-28251</a> , <a href='https://jira.mgid.com/browse/VT-28193'>VT-28193</a>\n" +
                 "2. Show amount of placements in a widget with booked for Direct Demand <a href=\"https://youtrack.mgid.com/issue/TA-52151\">TA-52151</a>")
    @Owner("RKO")
    @Test(dataProvider = "dataPlacementConfigurationDirectPublisherDemand", description = "Add new widget E2E for site with cooperation type: wages, int_exchange, direct_demand and tier 1/3")
    public void placementConfigurationDirectPublisherDemandE2E(String tier, int siteId) {
        log.info("Test is started " + tier);
        authDashAndGo("testEmail71@ex.ua","publisher/add-widget/site/" + siteId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().switchDirectPublisherDemandTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByDemand(5);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByDemand(), "FAIL -> PlacementConfiguration after create widget");

        log.info("edit PlacementConfiguration");
        pagesInit.getWidgetClass().chooseRandomPlacementByDemand(5);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("check after edit PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByDemand(), "FAIL -> PlacementConfiguration after edit widget");

        log.info("Show amount of placements in a widget");
        authCabAndGo(EndPoints.widgetsCustomIdUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Wages"), "1", "FAIL -> Show amount of placements: wages");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Publisher campaigns studio"), "5", "FAIL -> Show amount of placements: Publisher campaigns studio");

        softAssert.assertAll();
        log.info("Test is finished " + tier);
    }

    @DataProvider
    public Object[][] dataPlacementConfigurationDirectPublisherDemand() {
        return new Object[][]{
                {"tier:1", 116},
                {"tier:3", 118}
        };
    }

    @Feature("Placement Configuration")
    @Story("direct publisher demand")
    @Description("Check disabled placements for site with cooperation type: wages, int_exchange, direct_demand and tier:3 <a href='https://jira.mgid.com/browse/VT-28251'>VT-28251</a> , <a href='https://jira.mgid.com/browse/VT-28193'>VT-28193</a>")
    @Test(description = "Check disabled placements for site with cooperation type: wages, int_exchange, direct_demand and tier:3")
    public void placementConfigurationDirectPublisherDemandCheckDisabledPlacements() {
        log.info("Test is started");
        log.info("turn on intExchange tumbler and choose placements");
        authDashAndGo("testEmail71@ex.ua","publisher/add-widget/site/" + 118);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledPlacement(),
                Arrays.asList("1", "2"), "FAIL -> disable placement IntExchange");

        pagesInit.getWidgetClass().switchDirectPublisherDemandTumbler(true);
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisabledDemandPlacement(),
                Collections.emptyList(), "FAIL -> disable placement DirectPublisherDemand");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Placement Configuration")
    @Story("direct publisher demand")
    @Description("1. Add new widget E2E for site with cooperation type: wages, int_exchange, direct_demand and tier 1/3 <a href='https://jira.mgid.com/browse/VT-28251'>VT-28251</a> , <a href='https://jira.mgid.com/browse/VT-28193'>VT-28193</a>\n" +
            "2. Show amount of placements in a widget with booked for Direct Demand <a href=\"https://youtrack.mgid.com/issue/TA-52151\">TA-52151</a>\n" +
            "3. Possibility to activate both internal exchange and direct demand in a widget <a href=\"https://youtrack.mgid.com/issue/TA-52135\">TA-52135</a>\n" +
            "4. Change the color of placements in a Widget Constructor for Direct Demand Placements. <a href=\"https://youtrack.mgid.com/issue/TA-52136\">TA-52136</a>")
    @Owner("RKO")
    @Test(dataProvider = "dataPlacementConfigurationDirectPublisherDemand", description = "Add new widget E2E with all types(wages+int_exc+video+DPD) for site with cooperation type: wages, video, int_exchange, direct_demand and tier 1/3")
    public void placementConfigurationTurnOnDirectPublisherDemandAndIntExchangeAndVideo(String tier, int siteId) {
        log.info("Test is started " + tier);
        authDashAndGo("testEmail71@ex.ua","publisher/add-widget/site/" + siteId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().switchIntExchangeTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(2);
        pagesInit.getWidgetClass().switchDirectPublisherDemandTumbler(true);
        pagesInit.getWidgetClass().chooseRandomPlacementByDemand(2);
        pagesInit.getWidgetClass().switchVideoTumbler(true);
        pagesInit.getWidgetClass().enableVideoContentCheckboxes(true, false);
        pagesInit.getWidgetClass().choosePlacementByVideo();
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after create PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> checkPlacementByIntExchange after create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByDemand(), "FAIL -> checkPlacementByDemand after create widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByVideo(), "FAIL -> checkPlacementByVideo after create widget");

        log.info("edit PlacementConfiguration");
        pagesInit.getWidgetClass().chooseRandomPlacementByIntExchange(1);
        pagesInit.getWidgetClass().chooseRandomPlacementByDemand(1);
        pagesInit.getWidgetClass().choosePlacementByVideo();
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("check after edit PlacementConfiguration");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByIntExchange(), "FAIL -> checkPlacementByIntExchange after edit widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByDemand(), "FAIL -> checkPlacementByDemand after edit widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByVideo(), "FAIL -> checkPlacementByVideo after edit widget");

        log.info("Show amount of placements in a widget");
        authCabAndGo(EndPoints.widgetsCustomIdUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Wages"), "3", "FAIL -> Show amount of placements: wages");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Publisher campaigns studio"), "1", "FAIL -> Show amount of placements: Publisher campaigns studio");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Internal exchange"), "1", "FAIL -> Show amount of placements: Internal exchange");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAmountOfPlacementForCustomWidget("Video"), "1", "FAIL -> Show amount of placements: Video");

        softAssert.assertAll();
        log.info("Test is finished " + tier);
    }
}
