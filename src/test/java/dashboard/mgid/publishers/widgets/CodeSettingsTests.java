package dashboard.mgid.publishers.widgets;

import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.open;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_DOMAIN;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.Subnets.SUBNET_MGID_NAME;

/*
 * AMP-code (cab/dash)
 * Instant Articles code (cab)
 */
public class CodeSettingsTests extends TestBase {

    private int widgetId;
    private String headerWidgetLink;
    private String widgetCodeForClientLink;
    private final int webSiteId = WEBSITE_MGID_WAGES_ID;
    private final String siteDomain = WEBSITE_MGID_WAGES_DOMAIN;
    private final String subnetName = SUBNET_MGID_NAME;

    private CodeSettingsTests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    private void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    /**
     * <p>Создание нового виджета и проверка AMP кода при создании</p>
     * <p>Создание нового виджета и проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(priority = -1)
    public void createWidgetAndCheckCodeAfterCreated_dash(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass()
                .setIsCloseCodePopup(false)
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN)
                .saveWidgetAndGetId();
        widgetId = pagesInit.getWidgetClass().getWidgetId();

        log.info("Check AMP-code after creating widget");
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkAMPCodeAfterCreatingWidget(subnetName, siteDomain, webSiteId),
                "FAIL -> AMP code after creating in " + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода в списке виджетов dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "createWidgetAndCheckCodeAfterCreated_dash")
    public void checkCodeInWidgetsList_dash(){
        log.info("Test is started");
        authDashAndGo("publisher/widgets/site/" + webSiteId);
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkAMPCodeInWidgetsList(widgetId, webSiteId, siteDomain, subnetName),
                "FAIL -> AMP code in Dashboard widgets list" + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода в списке виджетов cab</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "createWidgetAndCheckCodeAfterCreated_dash")
    public void checkCodeInWidgetsList_cab(){
        log.info("Test is started");
        log.info("Check AMP-code in widgets list Cab");
        authCabAndGo("wages/informers-code/id/" + widgetId);

        headerWidgetLink = pagesInit.getCabInformersCode().getLinkForHeaderWidget();
        widgetCodeForClientLink = pagesInit.getCabInformersCode().getLinkWidgetCodeForClient();

        softAssert.assertTrue(
                pagesInit.getCabInformersCode().checkAMPCodeForWidgetsInCab(widgetId,
                        siteDomain, webSiteId, subnetName),
                "FAIL -> AMP code in Cab! " + subnetName + "!");

        String jsc = "jsc.mgid.com";
        softAssert.assertTrue(
                pagesInit.getCabInformersCode().checkInstantArticlesWidgetsInCab(widgetId,
                        siteDomain, webSiteId, jsc),
                "FAIL -> Instant Articles code in Cab! " + subnetName + "!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Get widget link for client) in dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkWithHash_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for authorized client -> WidgetCodeForClient");
        authDashAndGo("");
        open(headerWidgetLink);
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkAMPCodeInAdditionalPlaces(
                        subnetName, widgetId, siteDomain, webSiteId),
                "FAIL -> AMP code by hash links authorized " + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Header widget code) in dash</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkForHearerWidget_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for authorized client -> LinkForHeaderWidget");
        authDashAndGo("");
        open(widgetCodeForClientLink);
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkAMPCodeInAdditionalPlaces(
                        subnetName, widgetId, siteDomain, webSiteId),
                "FAIL -> AMP code by hash links authorized " + subnetName + "!");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Get widget link for client) in dash (unauthorized client)</p>
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkWithHash_unauthorizedClient_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for unauthorized client");

        open(widgetCodeForClientLink);

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Проверка AMP кода (Header widget code) in dash(unauthorized client)</p>\
     * <p>проверка что Instant Articles код не отображается</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22396">TA-22396</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test(dependsOnMethods = "checkCodeInWidgetsList_cab")
    public void checkCodeByLinkForHearerWidget_unauthorizedClient_dash(){
        log.info("Test is started");
        log.info("Check AMP-code by links with hash for unauthorized client");

        open(headerWidgetLink);
        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowAmpCode(), "FAIL -> AMP-code");

        softAssert.assertFalse(
                pagesInit.getWidgetClass().isShowInstantArticlesCode(), "FAIL -> Instant Articles code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
