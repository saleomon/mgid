package dashboard.mgid.publishers.widgets.child;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.ClientsEntities.*;

public class ABTests extends TestBase {

    public ABTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23798">https://youtrack.mgid.com/issue/TA-23798</a>
     * Запрет на редактирование Parent виджета при активном A/B тестировании
     * - check disable edit parent widget and message due active child exist dash
     * - check disable edit parent widget and message due active child exist cab composite
     * - check disable edit parent widget and message due active child exist cab subwidget
     * NIO
     */
    @Test
    public void abTestWidgetCheckUnavailableEditParentWidgetWithActiveChild() {
        log.info("Test is started");
        authDashAndGo("publisher/edit-widget/id/" + MGID_WIDGET_IN_ARTICLE_ID_WITH_CHILD);
        log.info("check message dash");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("YOU CAN'T MAKE CHANGES TO THE WIDGET UNTIL A/B TESTING IS STOPPED"), "Trouble with message dash");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkDisableEditClass(), "Trouble with style that block edit widget dash");

        authCabAndGo("wages/informers-edit/type/composite/id/" + MGID_WIDGET_IN_ARTICLE_ID_WITH_CHILD);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("You can't make changes to the widget until A/B testing is stopped"), "Trouble with message");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDisabledSaveButton(), "Trouble with message cab composite");

        authCabAndGo("wages/widgets/?c_id=" + MGID_WIDGET_IN_ARTICLE_ID_WITH_CHILD);
        authCabAndGo(pagesInit.getCabWidgetsList().getLinkEditProductSubWidget());
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("You can't make changes to the widget until A/B testing is stopped"), "Trouble with message cab subwidget");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDisabledSaveButton(), "Trouble with message cab subwidget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * При создании виджета проставлять g_blocks.wrong_shows_enabled=9
     * <ul>
     * <li>Создание подвиджета</li>
     * <li>Сравнение значения поля подвиджета wrong_shows_enabled с дефолтным значением </li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24111">Ticket TA-24111</a>
     * <p>NIO</p>
     */
    @Test
    public void abTestWidgetWrongShowEnabled() {
        log.info("Test is started");
        String siteId = "1";
        int widgetId = 1000;
        authDashAndGo("publisher/widgets/site/" + siteId);
        log.info("Add clone widget");
        pagesInit.getWidgetClass().addAbTestWidget(widgetId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        log.info("Check g_blocks.wrong_shows_enabled clone field");
        Assert.assertEquals(
                operationMySql.getGblocks().getWrongShowsEnabled(pagesInit.getWidgetClass().getCloneId()),
                9,
                "FAIL -> wrong_shows_enabled != 9");
        log.info("Test is finished");
    }

    /**
     * Добавление возможности создания виджетов для A/B тестирования
     * <ul>
     * <li>Создание подвиджета</li>
     * <li>Проверка создания</li>
     * <li>Удаление клона</li>
     * <li>Проверка удаления</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-22429">Ticket VT-22429</a>
     * <p>NIO</p>
     */
    @Test
    public void abTestWidgetAddDeleteWidget() {
        log.info("Test is started");
        String siteId = "1001";
        int widgetId = 1002;
        authDashAndGo("publisher/widgets/site/" + siteId);
        log.info("Add clone widget");
        pagesInit.getWidgetClass().addAbTestWidget(widgetId);
        log.info("Check added cloned widget");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        log.info("Crone finished work");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCloneAttributesAtPopup(), "FAIL - cloned popup info");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCloneAttributesAtWidgetList(widgetId), "FAIL - cloned list info");
        log.info("Delete clone");
        pagesInit.getWidgetClass().deleteClonedWidget();
        log.info("Check deleted clone widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkDeletedClone(widgetId), "FAIL - delete cloned widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Змінити логіку при клонуванні інформерів з контрактом Month
     * <ul>
     *  <li>створення клону інформера</li>
     *  <li>перевірка ціни контракту клона з батьківським віджетом</li>
     *  </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/KOT-203">Ticket KOT-203</a>
     * <p>NIO</p>
     */
    @Test
    public void abTestWidgetContractPrice() {
        log.info("Test is started");
        int parentWidget = 26;
        String nameOfClone = "Subwidgets";
        String clientSites = "1";

        authDashAndGo("publisher/widgets/site/" + clientSites);
        log.info("add clone for widget");
        pagesInit.getWidgetClass().addAbTestWidget(parentWidget);
        authDashAndGo("publisher/widgets/site/" + clientSites);
        log.info("check clone creation");
        Assert.assertTrue(pagesInit.getWidgetClass().checkCreatedClone(parentWidget, nameOfClone), "Clone wasn't created");
        log.info("run consumer and cron");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getCloneId());
        log.info("check price of clone");
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkPriceOfClone(pagesInit.getWidgetClass().getCloneId()), "Price of clone similar with parent");
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Проверка клонирования настроек фыльтра площадок под виджету</li>
     * <li>2. Проверка клонирования настроек DFP-flag(partners.clients_sites_excluded_tags)</li>
     * <li>3. При создании чайлда, sources_id должно копироваться с парента, а не с сайта</li>
     * <li>4. Копировать g_blocks.spy_feed при создании чайлда: set g_blocks.spy_feed=0 after copy check  child = 0 </li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/KOT-8">Ticket KOT-8</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24140">TA-24140</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27224">TA-27224</a>
     * <p>NIO</p>
     */
    @Test
    public void abTestWidgetCheckCopySettings() {
        log.info("Test is started");
        int tickerCompositeId = 1001;
        int siteId = 1001;
        ArrayList<String> listOfSubSource = new ArrayList<>(Arrays.asList("2132", "12", "65432", "2145732"));

        authDashAndGo("publisher/widgets/site/" + tickerCompositeId);
        pagesInit.getWidgetClass().addAbTestWidget(tickerCompositeId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(operationMySql.getClientsSitesExcludedTags().isTickersCompositeIdHasDfpFlag(siteId, pagesInit.getWidgetClass().getCloneId().toString()), "FAIL -> DFP flag doesn't copy");

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSavedSubSources(listOfSubSource), "FAIL -> subSource doesn't copy");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().setSourcesId("8821").checkSourcesId(), "FAIL -> ui: g_blocks.sources_id");
        softAssert.assertEquals(operationMySql.getGblocks().getSpyFeed(tickerCompositeId), 0,"FAIL -> g_blocks.spy_feed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets cloning")
    @Feature("Accuracy of AB testing")
    @Story("Create A/B widget in dashboard")
    @Description("Check inheritance and not inheritance of antifraud widgets settings after cloning it\n" +
            "     <ul>\n" +
            "       <li>Turn on recalculate Native to Search rule - inherit\n" +
            "       <li>Transit indicator - set as default value in 5%\n" +
            "       <li>DC - do not inherit\n" +
            "       <li>Delay of clickability - do not inherit\n" +
            "       <li>Switcher DC/TRZ - do not inherit\n" +
            "       <li>Auto-balancer TRZ - do not inherit\n" +
            "       <li>Auto-balancer DC - do not inherit</li>" +
            "       <li>@see <a href \"https://jira.mgid.com/browse/TA-52068\">Ticket TA-52068</a></li>\n" +
            "     </ul>" +
            "   Preconditions are in dumps. Used tables and fields:" +
            "       - partners.g_blocks.use_tranz_page;" +
            "       - partners.tickers_composite_doubleclicks.parameters;" +
            "       - partners.tickers_composite_attributes.name and value;" +
            "       - partners.tickers_composite.clickable_delay;")
    @Owner("AIA")
    //todo AIA @Test(description = "Check correct inheritance of antifraud widgets settings after cloning it")
    public void checkPropertiesInheritanceAfterWidgetsCloning() {
        log.info("Test is started");
        int parentWidgetId = 2009;
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "publisher/widgets/site/2027");
        pagesInit.getWidgetClass().addAbTestWidget(parentWidgetId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        int cloneWidgetId = pagesInit.getWidgetClass().getCloneId();
        softAssert.assertEquals(operationMySql.getGblocks().getUseTranzPageByCompositeId(cloneWidgetId), null,
                "FAIL -> Use transit page!");
        softAssert.assertEquals(operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(parentWidgetId),
                operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(cloneWidgetId),
                "FAIL -> Double click!");
        softAssert.assertEquals(operationMySql.getTickersComposite().getClickableDelay(cloneWidgetId), 1,
                "FAIL -> Clickable delay!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_manual_rejection_native_to_search"),
                true,
                "FAIL -> Native to search!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_wages_optimization_switcher"),
                true,
                "FAIL -> Switcher DC/TRZ!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_transit_mode_auto_grades_enabled"),
                true,
                "FAIL -> Auto-balancing TRZ!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_wages_optimization_switcher"),
                true,
                "FAIL -> Auto-balancer DC!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets cloning")
    @Feature("Accuracy of AB testing")
    @Story("Create A/B widget in dashboard")
    @Description("Check inheritance and not inheritance of antifraud widgets settings after cloning it\n" +
            "     <ul>\n" +
            "       <li>Doubleclick settings\n" +
            "       <li>Trz settings(g_blocks.use_tranz_page)\n" +
            "       <li>Autobalancers settings\n" +
            "       <li>Liability\n" +
            "       <li>Anura autobalancer\n" +
            "       <li>Delay clicks\n" +
            "       <li>Switcher DC/TRZ</li>" +
            "       <li>Autobalancer TRZ</li>" +
            "       <li>Autobalancer DC </li>" +
            "       <li>AF redirect settings</li>" +
            "       <li>Recapcha trz</li>" +
            "       <li>Click sanctions (g_blocks.wrong_click_enabled)</li>" +
            "       <li>Source type</li>" +
            "       <li>Widget honeypot</li>" +
            "       <li>RTB</li>" +
            "       <li>N2s</li>" +
            "       <li>@see <a href \"https://jira.mgid.com/browse/AF-3634\">Ticket AF-3634</a></li>\n" +
            "     </ul>")
    @Owner("MVV")
    //todo MVV g_blocks.liability point
    //@Test(description = "Check correct inheritance of antifraud parameters in A/B widget")
    public void checkPropertiesInheritanceAfterWidgetsInABWidget(){
        log.info("Test is started");
        int parentWidgetId = 8001;
        authDashAndGo(CLIENTS_PAYOUTS_UKRAINE_LOGIN, "publisher/widgets/site/3001");
        pagesInit.getWidgetClass().addAbTestWidget(parentWidgetId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        int clonedWidgetId = pagesInit.getWidgetClass().getCloneId();
        List<String> parentAttributes = operationMySql.getTickersCompositeAttributes().getListOfAttributesValues(parentWidgetId);
        List<String> parentAttributesChild = operationMySql.getTickersCompositeAttributes().getListOfAttributesValues(clonedWidgetId);
        log.info("Checking g_blocks fields");
        softAssert.assertEquals("20", operationMySql.getGblocks().getUseTranzPageByCompositeId(clonedWidgetId),
                "FAIL -> Use transit page isn't equals");
        softAssert.assertEquals("1", operationMySql.getGblocks().getRtbEnabledByCompositeId(clonedWidgetId),
                "FAIL -> g_blocks.rtb_enabled isn't equals");
        softAssert.assertEquals("8384", operationMySql.getGblocks().getWrongClicksEnabledByCompositeId(clonedWidgetId),
                "FAIL -> g_blocks.wrong_clicks_enabled isn't equals");
        softAssert.assertEquals("1", operationMySql.getGblocks().getUseAfTransitPageByCompositeId(clonedWidgetId),
                "FAIL -> g_blocks.use_af_transit_page isn't equals");
        softAssert.assertEquals(0, operationMySql.getGblocks().getLiabilityByCompositeId(clonedWidgetId),
                "FAIL -> g_blocks.liability isn't equals");
        softAssert.assertEquals("autotest checking", operationMySql.getGblocks().getLiabilityCommentByCompositeId(clonedWidgetId),
                "FAIL -> g_blocks.liability_comment isn't equals");
        log.info("Checking tickers_composite fields");
        softAssert.assertEquals("2000", operationMySql.getTickersComposite().getActivateDelay(clonedWidgetId),
                "FAIL -> tickers_composite.activate_delay isn't equals");
        softAssert.assertEquals(11, operationMySql.getTickersComposite().getSourcesTypesId(clonedWidgetId),
                "FAIL -> tickers_composite.sources_types_id isn't equals");
        softAssert.assertEquals("medium", operationMySql.getTickersCompositeTransitRecaptcha().getTransitRecaptchaLevel(clonedWidgetId),
                "FAIL -> tickers_composite_transit_recaptcha.level isn't equals");
        softAssert.assertEquals(operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(parentWidgetId),
                operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(clonedWidgetId),
                "FAIL -> tickers_composite_doubleclicks.parameters isn't equals");
        softAssert.assertEquals(parentAttributes.toString(), parentAttributesChild.toString(),
                "FAIL -> tickers_composite_attributes values isn't equals");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
