package dashboard.mgid.publishers.widgets.child;

import io.qameta.allure.*;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;

public class SubWidgetsTests extends TestBase {

    /**
     * Dashboard. tickers_composite_relations. Добавить возможность задавать правила по traffic_types
     * <ul>
     * <li>1. Добавление правила для виджета и Проверка сохраненных параметров</li>
     * <li>2. Проверка клонирования настроек DFP-flag(partners.clients_sites_excluded_tags)</li>
     * <li>3. При создании чайлда, sources_id должно копироваться с парента, а не с сайта</li>
     * <li>4. Копировать g_blocks.spy_feed при создании чайлда: set g_blocks.spy_feed=1 after copy check  child = 1 </li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22986">Ticket TA-22986</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24140">TA-24140</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27224">TA-27224</a>
     * <p>NIO</p>
     */
    @Test
    public void creatingSubwidgetWithRuleTrafficTypes() {
        log.info("Test is started");
        int siteId = 1001;
        String widgetId = "1004";

        authDashAndGo("publisher/widgets/site/1001");
        log.info("Create subwidget with rule traffic_types");
        pagesInit.getWidgetClass().createSubwidget(widgetId);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("Check created rule");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCreatedRule(pagesInit.getWidgetClass().getCloneId()), "False - saved settings not meet with required");

        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(operationMySql.getClientsSitesExcludedTags().isTickersCompositeIdHasDfpFlag(siteId, pagesInit.getWidgetClass().getCloneId().toString()), "FAIL -> DFP flag doesn't copy");

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().setSourcesId("8821").checkSourcesId(), "FAIL -> ui: g_blocks.sources_id");
        softAssert.assertEquals(operationMySql.getGblocks().getSpyFeed(pagesInit.getWidgetClass().getCloneId()), 1,"FAIL -> g_blocks.spy_feed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets cloning")
    @Feature("Accuracy of AB testing")
    @Story("Widget property Inheritance")
    @Description("Check inheritance and not inheritance of antifraud widgets settings after cloning it\n" +
            "     <ul>\n" +
            "       <li>Turn on recalculate Native to Search rule - inherit\n" +
            "       <li>Transit indicator - set as default value in 5%\n" +
            "       <li>DC - do not inherit\n" +
            "       <li>Delay of clickability - do not inherit\n" +
            "       <li>Switcher DC/TRZ - do not inherit\n" +
            "       <li>Auto-balancer TRZ - do not inherit\n" +
            "       <li>Auto-balancer DC - do not inherit</li>" +
            "       <li>@see <a href \"https://jira.mgid.com/browse/TA-52068\">Ticket TA-52068</a></li>\n" +
            "     </ul>" +
            "   Preconditions are in dumps. Used tables and fields:" +
            "       - partners.g_blocks.use_tranz_page;" +
            "       - partners.tickers_composite_doubleclicks.parameters;" +
            "       - partners.tickers_composite_attributes.name and value;" +
            "       - partners.tickers_composite.clickable_delay;")
    @Owner("AIA")
    //todo AIA @Test(description = "Check correct inheritance of antifraud widgets settings after cloning it")
    public void checkPropertiesInheritanceAfterWidgetsCloning() {
        log.info("Test is started");
        int parentWidgetId = 2010;
        authDashAndGo(CLIENTS_PAYOUTS_LOGIN, "publisher/widgets/site/2027");
        pagesInit.getWidgetClass().createSubwidget(String.valueOf(parentWidgetId));
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
        int cloneWidgetId = pagesInit.getWidgetClass().getCloneId();
        softAssert.assertEquals(operationMySql.getGblocks().getUseTranzPageByCompositeId(parentWidgetId), null,
                "FAIL -> Use transit page!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(parentWidgetId),
                operationMySql.getTickersCompositeDoubleclick().getDoubleClickParameters(cloneWidgetId),
                "FAIL -> Double click!");
        softAssert.assertEquals(operationMySql.getTickersComposite().getClickableDelay(cloneWidgetId), 0,
                "FAIL -> Clickable delay!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_manual_rejection_native_to_search"),
                true,
                "FAIL -> Native to search!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_wages_optimization_switcher"),
                true,
                "FAIL -> Switcher DC/TRZ!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_transit_mode_auto_grades_enabled"),
                true,
                "FAIL -> Auto-balancing TRZ!");
        softAssert.assertNotEquals(operationMySql.getTickersCompositeAttributes().getAntifraudParameters(cloneWidgetId, "antifraud_doubleclick_balancer"),
                true,
                "FAIL -> Auto-balancer DC!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
