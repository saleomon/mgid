package dashboard.mgid.publishers.widgets.child;

import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;

public class AbTestsForSubwidgetTests extends TestBase {

    /**
     * <ul>
     * <li>1. Добавление Ab test for SubWidget и Проверка сохраненных параметров</li>
     * <li>2. Проверка клонирования настроек DFP-flag(partners.clients_sites_excluded_tags)</li>
     * <li>3. При создании чайлда, push_providers_id должно копироваться с парента, а не с сайта</li>
     * <li>4. Копировать g_blocks.spy_feed при создании чайлда: set g_blocks.spy_feed=2 after copy check  child = 1 </li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24140">TA-24140</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27224">TA-27224</a>
     * <p>RKO</p>
     */
    @Test
    public void creatingSubwidgetWithRuleTrafficTypes() {
        log.info("Test is started");
        int siteId = 31;
        authDashAndGo("testEmail31@ex.ua","publisher/widgets/site/" + siteId);
        log.info("Create AB test from subwidget");
        pagesInit.getWidgetClass().createAbTestFromSubwidget("59", "60");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("Check created rule");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSettingsAbTestFromSubwidget(
                pagesInit.getWidgetClass().getCloneId(),
                "Lesotho",
                "Direct"
                ),
                "False - saved settings not meet with required");

        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(operationMySql.getClientsSitesExcludedTags().isTickersCompositeIdHasDfpFlag(siteId, pagesInit.getWidgetClass().getCloneId().toString()), "FAIL -> DFP flag doesn't copy");

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getCloneId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().setPushProvider("132").checkPushProvider(), "FAIL -> ui: g_blocks.push_providers_id");
        softAssert.assertEquals(operationMySql.getGblocks().getSpyFeed(pagesInit.getWidgetClass().getCloneId()), 1,"FAIL -> g_blocks.spy_feed");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
