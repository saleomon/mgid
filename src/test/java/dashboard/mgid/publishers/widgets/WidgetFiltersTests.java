package dashboard.mgid.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.variables.ContractsData;
import testBase.TestBase;

import java.util.LinkedHashMap;

public class WidgetFiltersTests extends TestBase {

    public final int widgetWithRSContractId = 7004;
    public final int widgetWithMonthContractId = 7005;
    public final int widgetWithECpmContractId = 7006;
    public final int widgetWithCpmContractId = 7007;
    public final int widgetWithGuaranteedCpmContractId = 7010;
    public final int widgetWithGuaranteedECpmContractId = 7011;
    public final int widgetWithGuaranteedDailyContractId = 7009;
    public final int widgetWithGuaranteedMonthlyContractId = 7008;
    public final int widgetWithABTestParentId = 7012;
    public final int widgetWithABTestChildId = 7013;
    public final int widgetWithSubTestParentId = 7014;
    public final int widgetWithSubTestChildId = 7015;

    @Feature("Widget status filter")
    @Description("All but deleted filter doesn't work in the publisher dash <a href=\"https://youtrack.mgid.com/issue/TA-27460\">TA-27460</a>")
    @Test
    public void filter_ShowIsDeletedWidget() {
        LinkedHashMap<String, String> data_value_1 = new LinkedHashMap<>();
        data_value_1.put("84", "status-active");
        data_value_1.put("87", "status-active");

        LinkedHashMap<String, String> data_value_all = new LinkedHashMap<>();
        data_value_all.put("84", "status-active");
        data_value_all.put("87", "status-active");
        data_value_all.put("85", "status-blocked");
        data_value_all.put("86", "status-deleted");

        LinkedHashMap<String, String> data_child = new LinkedHashMap<>();
        data_child.put("88", "status-blocked");
        data_child.put("89", "status-deleted");
        data_child.put("87", "status-active");
        log.info("Test is started");
        authDashAndGo("testEmail34@ex.ua", "publisher/widgets/site/" + 59);

        log.info("check value = 1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetStatusFilter("1", data_value_1), "FAIL -> status filter = 1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkShowAllChildStatusWidget(87, data_child), "FAIL -> status filter = 1, check child");

        log.info("check value = all");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetStatusFilter("all", data_value_all), "FAIL -> status filter = all");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkShowAllChildStatusWidget(87, data_child), "FAIL -> status filter = all, check child");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check 'Contract type' filter in dashboard for basic contract types, <a href='https://jira.mgid.com/browse/TA-52041'>TA-52041</a>")
    @Test(dataProvider = "contractDataBasicTypes")
    public void contractTypeFilterBasicTypes(ContractsData.Contracts contractType, int widgetId) {
        log.info("Test is started");
        authDashAndGo("testEmail7003@test.com", "publisher/widgets/site/" + 7003);
        pagesInit.getWidgetClass().chooseWidgetContractFilter(contractType.getContractType());
        softAssert.assertEquals(pagesInit.getWidgetClass().getNumberOfDisplayedParentWidgets(), 1, "FAIL -> Wrong number of selected widgets");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetId, contractType.getContractType(), "Parent"), "FAIL -> Wrong contract type is displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] contractDataBasicTypes() {
        return new Object[][]{
                {ContractsData.Contracts.RS, widgetWithRSContractId},
                {ContractsData.Contracts.MONTH, widgetWithMonthContractId},
                {ContractsData.Contracts.ECPM, widgetWithECpmContractId},
                {ContractsData.Contracts.CPM, widgetWithCpmContractId},
        };
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check 'Contract type' filter in dashboard for guaranteed contract types, <a href='https://jira.mgid.com/browse/TA-52041'>TA-52041</a>")
    @Test(dataProvider = "contractDataGuaranteedTypes")
    public void contractTypeFilterGuaranteedTypes(ContractsData.Contracts contractType, ContractsData.GuaranteedContracts guaranteedContractType, int widgetId) {
        log.info("Test is started");
        authDashAndGo("testEmail7003@test.com", "publisher/widgets/site/" + 7003);
        pagesInit.getWidgetClass().chooseWidgetContractFilter(contractType.getContractType() + " " + guaranteedContractType.getGuaranteedContractType());
        softAssert.assertEquals(pagesInit.getWidgetClass().getNumberOfDisplayedParentWidgets(), 1, "FAIL -> Wrong number of selected widgets");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetId, contractType.getContractType() + " " + guaranteedContractType.getGuaranteedContractType(), "Parent"), "FAIL -> Wrong contract type is displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] contractDataGuaranteedTypes() {
        return new Object[][]{
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.CPM, widgetWithGuaranteedCpmContractId},
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.ECPM, widgetWithGuaranteedECpmContractId},
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.DAILY, widgetWithGuaranteedDailyContractId},
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.MONTHLY, widgetWithGuaranteedMonthlyContractId}
        };
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check 'Contract type' filter in dashboard for mixed contract type <a href='https://jira.mgid.com/browse/TA-52041'>TA-52041</a>")
    @Test
    public void contractTypeFilterForMixedTypeWithAbAndSubParts() {
        log.info("Test is started");
        authDashAndGo("testEmail7003@test.com", "publisher/widgets/site/" + 7004);
        pagesInit.getWidgetClass().chooseWidgetContractFilter(ContractsData.Contracts.MIXED.getContractType());
        softAssert.assertEquals(pagesInit.getWidgetClass().getNumberOfDisplayedParentWidgets(), 2, "FAIL -> Wrong number of selected parent widgets");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetWithABTestParentId, ContractsData.Contracts.MIXED.getContractType(), "Parent"), "FAIL -> Wrong contract type is displayed");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetWithSubTestParentId, ContractsData.Contracts.MIXED.getContractType(), "Parent"), "FAIL -> Wrong contract type is displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check 'Contract type' filter in dashboard for guaranteed contract widgets with AB and Sub parts, switching child/parent contract types <a href='https://jira.mgid.com/browse/TA-52041'>TA-52041</a>")
    @Test(dataProvider = "contractDataForWidgetsWithAbAndSubParts")
    public void contractTypeFilterForWidgetsWithAbAndSubParts(ContractsData.Contracts contractType, ContractsData.GuaranteedContracts guaranteedContractType, ContractsData.Contracts parentCopyContractType, int widgetParentId, int widgetChildId) {
        log.info("Test is started");
        authDashAndGo("testEmail7003@test.com", "publisher/widgets/site/" + 7004);
        log.info("Set contract filter to contract type that corresponds to child widget contract");
        pagesInit.getWidgetClass().chooseWidgetContractFilter(contractType.getContractType() + " " + guaranteedContractType.getGuaranteedContractType());
        pagesInit.getWidgetClass().showWidgetChild(widgetParentId);
        softAssert.assertEquals(pagesInit.getWidgetClass().getNumberOfDisplayedChildWidgets(), 2, "FAIL -> Wrong number of selected child widgets");
        log.info("Checking parent copy widget contract");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetParentId, parentCopyContractType.getContractType(), "Child"), "FAIL -> Wrong contract type is displayed");
        log.info("Checking child widget contract");
        softAssert.assertTrue(pagesInit.getWidgetClass().isContractTypeFromFilteredWidgetEqualsToSelectedType(widgetChildId, contractType.getContractType() + " " + guaranteedContractType.getGuaranteedContractType(), "Child"), "FAIL -> Wrong contract type is displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] contractDataForWidgetsWithAbAndSubParts() {
        return new Object[][]{
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.ECPM, ContractsData.Contracts.CPM, widgetWithABTestParentId, widgetWithABTestChildId},
                {ContractsData.Contracts.GUARANTEED, ContractsData.GuaranteedContracts.MONTHLY, ContractsData.Contracts.CPM, widgetWithSubTestParentId, widgetWithSubTestChildId}
        };
    }
}
