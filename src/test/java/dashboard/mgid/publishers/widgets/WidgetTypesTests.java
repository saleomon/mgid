package dashboard.mgid.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;
import java.util.List;

public class WidgetTypesTests extends TestBase {

    public final int siteWithoutCategoryFilter = 7006;
    public final int siteWithCategoryFilterFromImpactCategories = 7007;
    public final int siteWithCategoryFilterOutsideImpactCategories = 7008;
    public final String clientsLogin = "testEmail7004@test.com";

    @Story("Saving widget category filter after changes widget type/subtype")
    @Description("Saving impact widget type for widget from site without category filter <a href=\"https://jira.mgid.com/browse/TA-52050\">TA-52050</a>")
    @Test(description = "saving impact widget category for widget without category filter")
    public void savingImpactWidgetForSiteWithoutCategoryFilter() {
        log.info("Test is started");
        log.info("Creating simple Under-Article widget for site without activated CategoryFilter");
        authDashAndGo(clientsLogin, "publisher/add-widget/site/" + siteWithoutCategoryFilter);
        int createdWidgetTickerCompositeId = pagesInit.getWidgetClass().createSimpleWidget();
        int createdWidgetGBlocksId = operationMySql.getGblocks().getGBlocksId(createdWidgetTickerCompositeId);

        log.info("Changing widget type to In-Article with subtype Impact");
        authDashAndGo("publisher/edit-widget/id/" + createdWidgetTickerCompositeId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        softAssert.assertEquals(operationMySql.getgBlocksCategories().getCategoriesCountForWidget(createdWidgetGBlocksId), 99, "Wrong number of selected categories");
        softAssert.assertFalse(pagesInit.getWidgetClass().isAdOrLandingTypesSelectedForWidget(operationMySql.getgBlocksCategories().getAdTypesForWidget(createdWidgetGBlocksId)), "Ad or landing types are selected for a widget");
        softAssert.assertFalse(pagesInit.getWidgetClass().isAdOrLandingTypesSelectedForWidget(operationMySql.getgBlocksCategories().getLandingTypesForWidget(createdWidgetGBlocksId)), "Ad or landing types are selected for a widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Saving widget category filter after changes widget type/subtype")
    @Description("Saving impact widget type for widget from site with category from impact categories <a href=\"https://jira.mgid.com/browse/TA-52050\">TA-52050</a>")
    @Test(description = "saving impact widget category for widget with category filter from impact categories")
    public void savingImpactWidgetForSiteWithCategoryFromImpactCategory() {
        log.info("Test is started");
        log.info("Creating simple Under-Article widget for site with activated CategoryFilter from Impact categories");
        authDashAndGo(clientsLogin, "publisher/add-widget/site/" + siteWithCategoryFilterFromImpactCategories);
        int createdWidgetTickerCompositeId = pagesInit.getWidgetClass().createSimpleWidget();
        int createdWidgetGBlocksId = operationMySql.getGblocks().getGBlocksId(createdWidgetTickerCompositeId);

        log.info("Changing widget type to In-Article with subtype Impact");
        authDashAndGo("publisher/edit-widget/id/" + createdWidgetTickerCompositeId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        softAssert.assertEquals(operationMySql.getgBlocksCategories().getCategoriesCountForWidget(createdWidgetGBlocksId), 99, "Wrong number of selected categories");
        softAssert.assertEquals(operationMySql.getgBlocksCategories().isSelectedCategoryInWidget(createdWidgetGBlocksId, 100), "100", "Expected category isn't selected");
        List list = operationMySql.getgBlocksCategories().getAdAndLandingTypeForWidgetByCategoryID(createdWidgetGBlocksId, 100);
        softAssert.assertEquals(list.get(0), "r", "Wrong landing type is selected");
        softAssert.assertEquals(list.get(1), "pg,r", "Wrong ad type is selected");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Saving widget category filter after changes widget type/subtype")
    @Description("Saving impact widget type for widget from site with category outside impact categories <a href=\"https://jira.mgid.com/browse/TA-52050\">TA-52050</a>")
    @Test(description = "saving impact widget category for widget with category filter outside impact categories")
    public void savingImpactWidgetForSiteWithCategoryOutsideImpactCategory() {
        log.info("Test is started");
        log.info("Creating simple Under-Article widget for site with activated CategoryFilter outside Impact categories");
        authDashAndGo(clientsLogin, "publisher/add-widget/site/" + siteWithCategoryFilterOutsideImpactCategories);
        int createdWidgetTickerCompositeId = pagesInit.getWidgetClass().createSimpleWidget();
        int createdWidgetGBlocksId = operationMySql.getGblocks().getGBlocksId(createdWidgetTickerCompositeId);

        log.info("Changing widget type to In-Article with subtype Impact");
        authDashAndGo("publisher/edit-widget/id/" + createdWidgetTickerCompositeId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        softAssert.assertEquals(operationMySql.getgBlocksCategories().getCategoriesCountForWidget(createdWidgetGBlocksId), 99, "Wrong number of selected categories");
        softAssert.assertNotEquals(operationMySql.getgBlocksCategories().isSelectedCategoryInWidget(createdWidgetGBlocksId, 211), "211", "Wrong category is selected");
        softAssert.assertAll();
        log.info("Test is finished");
    }

}
