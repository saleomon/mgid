package dashboard.mgid.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StopWordsTests extends TestBase {

    private final String clientEmail = "testEmail73@ex.ua";

    @Feature("Stop Words")
    @Story("Stop Words Dash")
    @Description("add stop words e2e" +
            "Include check: " +
            "- add stop word 2 symbols " +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "add stop words e2e + check(add stop word 2 symbols)")
    public void addStopWords() {
        log.info("Test is started");
        ArrayList<String> listOfStopWords = new ArrayList<>(Arrays.asList("Fruit", "Rubber", "Ківі", "Їжак", "ґанокй", "Мʼята", "Різнотрав'я", "Єнот", "Ёлкаи"));

        int siteId = 121;
        authDashAndGo(clientEmail,"publisher/add-widget/site/" + siteId);
        pagesInit.getWidgetClass().setStopWords(new ArrayList<>(Arrays.asList("Fruit", "Rubber", "Ківі", "Їжак", "ґанокй", "Мʼята", "Різнотрав'я", "Єнот", "Ёлкаи", "hi")));
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertEquals(pagesInit.getWidgetClass().getStopWords(), listOfStopWords, "Check options");
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Dash")
    @Description("edit stop words e2e " +
            "Include check: " +
            "- try to add the same word " +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "edit stop words e2e + check(try to add the same word)")
    public void editStopWords() {
        log.info("Test is started");
        int widgetId = 438;
        ArrayList<String> listOfStopWords = new ArrayList<>(Arrays.asList("test", "fruit", "rest", "Fruit", "Rubber", "milk"));

        authDashAndGo(clientEmail,"publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setStopWords(new ArrayList<>(Arrays.asList("Fruit", "Rubber", "milk", "test")));
        softAssert.assertTrue(pagesInit.getWidgetClass().isDisplayedStopWordsError(), "FAIL -> error the same word");
        pagesInit.getWidgetClass().saveWidgetSettings();

        authDashAndGo("publisher/edit-widget/id/" + widgetId);
        softAssert.assertEquals(pagesInit.getWidgetClass().getStopWords(), listOfStopWords, "Check options cab");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Dash")
    @Description("Delete all stop words for 1 widget" +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "Delete all stop words for 1 widget")
    public void deleteAllStopWords() {
        log.info("Test is started");
        int widgetId = 439;
        authDashAndGo(clientEmail, "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().deleteStopWords();
        pagesInit.getWidgetClass().saveWidgetSettings();

        authDashAndGo("publisher/edit-widget/id/" + widgetId);
        Assert.assertEquals(pagesInit.getWidgetClass().getStopWords().size(), 0, "Check options");
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Dash")
    @Description("Widget have 3 words, delete 2 words and check 1 left" +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "Delete some stop words for 1 widget and check 1 left")
    public void deleteSomeStopWords() {
        log.info("Test is started");
        int widgetId = 440;
        authDashAndGo(clientEmail,"publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().deleteStopWords(2);
        pagesInit.getWidgetClass().saveWidgetSettings();

        authDashAndGo("publisher/edit-widget/id/" + widgetId);
        Assert.assertEquals(pagesInit.getWidgetClass().getStopWords(), new ArrayList<>(List.of("rest")), "Check options");
        log.info("Test is finished");
    }
}
