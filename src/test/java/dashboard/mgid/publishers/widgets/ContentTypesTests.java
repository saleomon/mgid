package dashboard.mgid.publishers.widgets;

import com.google.common.collect.ImmutableMap;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.Map;

import static com.codeborne.selenide.Selenide.refresh;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static core.helpers.BaseHelper.isConfirmDisplayed;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;

public class ContentTypesTests extends TestBase {

    public final String contentTypeWarning = "Removing ad types will likely affect CTR and eCPM of the ad unit. Please watch the widget performance closely.";

    public ContentTypesTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    @BeforeMethod
    public void initializeVariables() {
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    public void goToCreateWidget() {
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    /**
     * https://youtrack.dt00.net/issue/VT-21993
     * https://youtrack.dt00.net/issue/VT-22000
     * Дать возможность клиенту управлять типом тизеров в конструкторе информера Dashboard
     * Включает или отключает в ДБ один из adTypes, переходим в каб и проверяем
     * снимаем в кабе все adTypes, проставляем 2 рандомных и  сохраняем
     * переходим в ДБ и проверяем
     * RKO
     */
    @Test
    public void checkShowDarkNews() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().switchContentTypes();
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(pagesInit.getWidgetClass().getContentTypes()));
        pagesInit.getWidgetClass().setContentTypes(pagesInit.getCabCompositeSettings().switchOnRandomAdTypes(pagesInit.getWidgetClass().getContentTypes()));

        log.info("checkStateContentTypesInDash");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertTrue(pagesInit.getWidgetClass().checkStateContentTypesInDash());
        log.info("Test is finished");
    }

    /**
     * По умолчанию доступны все типы, ползунка в дешборде нет, переходим в каб и выбираем 3 рандомных типа, переходим в дб пересохраняем виджет, переходим в каб и проверяем что типы не слетели
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/MT-116">MT-116</a>
     * @see <a href="https://youtrack.mgid.com/issue/VT-22000">VT-22000</a>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21993">VT-21993</a>
     * @see <a href="https://youtrack.mgid.com/issue/VT-22001">VT-22001</a>
     */
    @Test
    public void checkShowDarkNewsAfterReSavingInDashboard() {
        Map<String, Boolean> adTypes = ImmutableMap.of(
                "pg", true,
                "r", true,
                "nc17", true,
                "nsfw", true,
                "pg13", true
        );

        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().setContentTypes(pagesInit.getCabCompositeSettings().switchOnRandomAdTypes(adTypes));

        log.info("checkStateContentTypesInDash");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass().checkStateContentTypesInDash(), "FAIL -> don't correct adType in Dash");
        pagesInit.getWidgetClass().reSaveWithEditWidget();

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(pagesInit.getWidgetClass().getContentTypes()), "FAIL -> don't correct adType in Cab");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создание и редактирование виджета со включенными content types. Проверка предупреждения
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-22006">VT-22006</a>
     */
    @Test
    public void editWidgetWithSwitchOnContentTypes() {
        log.info("Test is started");
        goToCreateWidget();
        log.info("выключаем все тумблеры в CONTENT TYPES и сохраняем виджет");
        pagesInit.getWidgetClass().switchOnOffAllContentTypes(false);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("включаем все тумблеры в CONTENT TYPES + получаем уведомление и сохраняем виджет");
        pagesInit.getWidgetClass().switchOnOffAllContentTypes(true);
        pagesInit.getWidgetClass().clickSaveWidgetButton();
        softAssert.assertTrue(
                helpersInit.getBaseHelper().checkDatasetContains(helpersInit.getBaseHelper().getPopupText(), contentTypeWarning),
                "FAIL -> contentTypeWarning");
        isConfirmDisplayed();
        helpersInit.getBaseHelper().closePopup();

        log.info("переходим в редактирование виджета + вкл/выкл 2 рандомных тумблера, записываем в мапку + сохраняем виджет и проверяем мапку");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().chooseRandomContentTypes(2);
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkContentTypesState(), "FAIL -> switch On 2 tumblers");

        log.info("переходим в редактирование виджета + вкл/выкл 1 рандомный тумблер, записываем в мапку + сохраняем виджет и проверяем мапку");
        pagesInit.getWidgetClass().chooseRandomContentTypes(1);
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkContentTypesState(), "FAIL -> switch On 2 tumblers");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Редактирование виджета с выключенными content types. Проверка отсутствия повторного предупреждения
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-22006">VT-22006</a>
     */
    @Test
    public void editWidgetWithSwitchOffContentTypes() {
        log.info("Test is started");

        Map<String, Boolean> tempMap;
        Map<String, Boolean> contentTypes;

        goToCreateWidget();
        log.info("получаем состояние всех тумблеров в CONTENT TYPES, сохраняем виджет и проверяем состояние");
        pagesInit.getWidgetClass().setContentTypesInMap();
        contentTypes = pagesInit.getWidgetClass().getContentTypes();
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkContentTypesState(), "FAIL -> check contentTypes after created");

        log.info("выключаем все тумблеры в CONTENT TYPES, записываем состояние во временную мапку tempMap");
        pagesInit.getWidgetClass().switchOnOffAllContentTypes(true);
        pagesInit.getWidgetClass().setContentTypesInMap();
        tempMap = pagesInit.getWidgetClass().getContentTypes();

        log.info("сохраняем виджет, получаем предупреждение, отклоняем его, проверяем состояние из tempMap");
        pagesInit.getWidgetClass().clickSaveWidgetButton();
        softAssert.assertTrue(
                helpersInit.getBaseHelper().checkDatasetContains(helpersInit.getBaseHelper().getPopupText(), contentTypeWarning),
                "FAIL -> contentTypeWarning");
        helpersInit.getBaseHelper().clickConfirmCancel();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .setContentTypes(tempMap)
                .checkContentTypesState(), "FAIL -> check contentTypes after show warning (contentTypes == tempMap)");

        log.info("обновляем страницу и проверяем что состояние тумблеров осталось прежднее из contentTypes");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .setContentTypes(contentTypes)
                .checkContentTypesState(), "FAIL -> check contentTypes after refresh (contentTypes == contentTypes)");

        log.info("отключаем все доступные тумблеры, записываем их состояние");
        refresh();
        pagesInit.getWidgetClass().switchOnOffAllContentTypes(true);
        pagesInit.getWidgetClass().setContentTypesInMap();

        log.info("сохраняем виджет, отклоняем предупреждением и сохраняем");
        pagesInit.getWidgetClass().clickSaveWidgetButton();
        helpersInit.getBaseHelper().clickConfirmCancel();
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .checkContentTypesState(), "FAIL -> check contentTypes after click confirm cancel warning (contentTypes == contentTypes)");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
