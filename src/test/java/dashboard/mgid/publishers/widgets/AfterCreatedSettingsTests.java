package dashboard.mgid.publishers.widgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.Collections;

import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.EndPoints.compositeEditUrl;
import static testData.project.EndPoints.goodWidgetSettingUrl;

/*
 * 1.Place multiple times on page:
 *      - checkPlaceMultipleTimesOnPageAfterCreated
 *      - checkCompositeCustomSettingsAfterCreate
 *      - placeMultipleTimesOnPage_changeTypeInCreatedWidget
 */
public class AfterCreatedSettingsTests extends TestBase {

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("Check settings(UI, db) after created new widget\n" +
            "<ul>\n" +
            "<li>1. sources_types по умолчанию при создании информера <a href=\"https://youtrack.mgid.com/issue/KT-785\">KT-785</a></li>\n" +
            "<li>2. Отключить функционал прямых ссылок по-умолчанию <a href=\"https://youtrack.mgid.com/issue/TA-23825\">TA-23825</a></li>\n" +
            "<li>3. Show sanction: set Sanctions as default <a href=\"https://jira.mgid.com/browse/AF-3117\">AF-3117</a></li>\n" +
            "<li>4. Проверка значения по умолчанию задержки кликабельности при создании информера <a href=\"https://youtrack.mgid.com/issue/VT-22205\">VT-22205</a></li>\n" +
            "<li>5. В настройках виджета добавить фильтр Campaign Tier <a href=\"https://youtrack.mgid.com/issue/TA-23366\">TA-23366</a></li>\n" +
            "<li>6. tickers_composite.use_place_reservation=1 <a href=\"https://jira.mgid.com/browse/TA-51682\">TA-51682</a></li>\n" +
            "<li>7. tickers_composite.adblock_change_styles=1</li>\n" +
            "<li>8. Drum Interval and Speed</li>\n" +
            "<li>9. Place multiple times on page ( allow_multiple_widgets = 1 )</li>\n" +
            "<li>10. Включение Shadow DOM по умолчанию для новых виджетов <a href=\"https://jira.mgid.com/browse/TA-51952\">TA-51952</a></li>\n" +
            "<li>11. Limit adblock integrations functionality for some widget types in the widget settings <a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a></li>\n" +
            "</ul>")
    @Test(description = "check UNDER_ARTICLE widget settings after created")
    public void checkCompositeCustomSettingsAfterCreate() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createSimpleWidget();
        authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getWidgetId());
        String goodsLink = pagesInit.getCabWidgetsList().getLinkEditProductSubWidget();
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getActiveDelayValue(), "2000", "FAIL -> active delay isn't equals 2000");
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().showDirectLinkIsSelected(), "FAIL -> 'Show direct links on hover' is selected !!! ");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getSelectedSourceType(), "11", "FAIL -> sourceType");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(true), "FAIL -> Place multiple times on page");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(true), "FAIL -> Place reservation");
        softAssert.assertEquals(
                operationMySql.getGblocks().getWrongShowsEnabled(pagesInit.getWidgetClass().getWidgetId()),
                305,
                "FAIL -> wrong_shows_enabled != 305");

        softAssert.assertEquals(
                operationMySql.getTickersComposite().getShadowDomEnabled(pagesInit.getWidgetClass().getWidgetId()),
                "1",
                "FAIL -> shadow_dom_enabled != 1");

        softAssert.assertEquals(
                operationMySql.getTickersComposite().getAdblockTemplate(pagesInit.getWidgetClass().getWidgetId()),
                null,
                "FAIL -> tickers_composite.adblock_template != null");

        authCabAndGo(goodsLink);
        log.info("Check 'Campaign tier filter' tooltip");
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getCampaignTierFilterTooltip(),
                "Important! Edit this filter for brand widgets only. In other cases, only after approval from analytics department.",
                "false -> tier tooltip");

        log.info("Check 'Campaign tier filter' default value");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCampaignTierFilterValue(),
                "false -> first tier all");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("check after created allow_multiple_widgets = 0")
    @Test(description = "check after created allow_multiple_widgets")
    public void placeMultipleTimesOnPageAfterCreated() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(false));
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("create widget and change his type on header and check allow_multiple_widgets = 0")
    @Test(description = "create widget and change his type on header and check allow_multiple_widgets")
    public void placeMultipleTimesOnPageChangeTypeInCreatedWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_728x90_1);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(false));
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("Impact settings after created: \n" +
            "<a href=\"https://jira.mgid.com/browse/TA-23615\">TA-23615</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24426\">TA-24426</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-24413\">TA-24413</a>\n" +
            "<a href=\"https://jira.mgid.com/browse/TA-52006\">TA-52006</a>")
    @Test(description = "check Impact settings after create from dashboard")
    public void checkImpactWidgetSettings(){
        log.info("Test is started");
        authDashAndGo("testEmail20@ex.ua","publisher/add-widget/site/" + 56);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);


        log.info("checkImpactWidgetSettings: " + pagesInit.getWidgetClass().getWidgetId());
        int g_blocks = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());

        log.info("check news amount on disabled");
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisabled_NewsAmount(), "FAIL -> news amount isn't disabled");

        log.info("get product id and go on Product Part - check 'brandformance' base settings");
        authCabAndGo(goodWidgetSettingUrl + g_blocks);

        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCampaignTierFilterValue(), "Fail -> tier filter doesn't has default value 0 (All)");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().descriptionRequiredIsDisabled(), "FAIL -> description Required is disabled");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().isSelectedCustomCheckboxInCategoryFilter("Content promotions"), "Fail -> 'Content promotions' - is selected");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().isSelectedCustomCheckboxInCategoryFilter("Product promotions"), "Fail -> 'Product promotions' - is selected");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(pagesInit.getWidgetClass().getWidgetId()), Collections.singletonList("29"), "FAIL -> bundles");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("check extends push provider from site after create widget <a href=\"https://youtrack.mgid.com/issue/TA-27217\">TA-27217</a>")
    @Test(description = "check extends push provider from site after create widget")
    public void checkExtendsPushProviderFromSiteAfterCreateWidget(){
        log.info("Test is started");
        authDashAndGo("testEmail29@ex.ua","publisher/add-widget/site/" + 50);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);

        log.info("go on composite settings");
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());

        softAssert.assertTrue(pagesInit.getCabCompositeSettings()
                .setCategoryPlatform("150")
                .checkCategoryPlatform(), "FAIL -> ui: category_platform");

        softAssert.assertTrue(pagesInit.getCabCompositeSettings()
                .setPushProvider("130")
                .checkPushProvider(), "FAIL -> ui: push_provider");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("check extends source id from site after create widget <a href=\"https://youtrack.mgid.com/issue/TA-27217\">TA-27217</a>")
    @Test(description = "check extends source id from site after create widget")
    public void checkExtendsSourceIdFromSiteAfterCreateWidget(){
        log.info("Test is started");
        authDashAndGo("testEmail29@ex.ua","publisher/add-widget/site/" + 51);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);

        log.info("go on composite settings");
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());

        softAssert.assertTrue(pagesInit.getCabCompositeSettings()
                .setCategoryPlatform("108")
                .checkCategoryPlatform(), "FAIL -> ui: category_platform");

        softAssert.assertTrue(pagesInit.getCabCompositeSettings()
                .setSourcesId("8820")
                .checkSourcesId(), "FAIL -> ui: sources_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Add \"In-site notification\" format to MGID dashboard: \n" +
            "<a href=\"https://jira.mgid.com/browse/VT-28177\">VT-28177</a>")
    @Owner("RKO")
    @Test(description = "check InSiteNotification settings after create from dashboard")
    public void checkInSiteNotificationMainWidgetSettings(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);

        log.info("widgetId: " + pagesInit.getWidgetClass().getWidgetId());

        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(pagesInit.getWidgetClass().getWidgetId()), 16, "FAIL -> sources_types_id");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), "1", "FAIL -> push_providers_id");

        log.info("go on composite settings");
        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> AdBlock template is disabled");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("Add \"In-site notification\" format to MGID dashboard: \n" +
            "<a href=\"https://jira.mgid.com/browse/VT-28177\">VT-28177</a>")
    @Test(description = "check In-site notification settings after edit Sidebar widget")
    public void checkInSiteNotificationMainSettingsAfterEditFromSidebar(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("widgetId: " + pagesInit.getWidgetClass().getWidgetId());

        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(pagesInit.getWidgetClass().getWidgetId()), 16, "FAIL -> sources_types_id");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), "1", "FAIL -> push_providers_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget settings after create from dashboard")
    @Story("Widget settings after create from dashboard Mgid")
    @Description("Add \"In-site notification\" format to MGID dashboard: \n" +
            "<a href=\"https://jira.mgid.com/browse/VT-28177\">VT-28177</a>")
    @Test(description = "check Sidebar settings after edit from In-site notification")
    public void checkInSiteNotificationMainSettingsAfterEditInSidebar(){
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);

        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        pagesInit.getWidgetClass().saveWidgetSettings();


        log.info("widgetId: " + pagesInit.getWidgetClass().getWidgetId());

        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(pagesInit.getWidgetClass().getWidgetId()), 11, "FAIL -> sources_types_id");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(pagesInit.getWidgetClass().getWidgetId()), null, "FAIL -> push_providers_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
