package dashboard.mgid.publishers;

import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;

public class BlockedContentTests extends TestBase {

    private final int widgetId = 78;

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Test
    public void widgetStand_checkCloseIcon_blockTeaserForWidget(){
        log.info("Test is started");
        goToCreateWidget(widgetId);

        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);

        serviceInit.getServicerMock().setStandName(setStand("mgid_blocked_content"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserOnStand(), "FAIL -> show block teaser");
        authDashAndGo(pagesInit.getWidgetClass().getLinkOnBlockTeaser());

        softAssert.assertTrue(pagesInit.getBlockedContent().getTeaserTitle().contains(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                 "FAIL -> block teaser doesn't wright");
        pagesInit.getBlockedContent().chooseBlockerType("widget");
        pagesInit.getBlockedContent().blockedPopupSave();
        helpersInit.getBaseHelper().closePopup();

        pagesInit.getBlockedContent().chooseWidgetInFilter(widgetId);
        softAssert.assertEquals(pagesInit.getBlockedContent().getTeaserBlockedType(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                "Blocked widgets",
                "FAIL -> don't correct blocked type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void widgetStand_checkCloseIcon_blockTeaserForWebsites(){
        log.info("Test is started");
        goToCreateWidget(widgetId);

        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);

        serviceInit.getServicerMock().setStandName(setStand("mgid_blocked_content"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserOnStand(2));
        authDashAndGo(pagesInit.getWidgetClass().getLinkOnBlockTeaser());

        softAssert.assertEquals(pagesInit.getWidgetClass().getBlockedTeaserTitle(),
                pagesInit.getBlockedContent().getTeaserTitle(), "FAIL -> block teaser doesn't wright");
        pagesInit.getBlockedContent().chooseBlockerType("site");
        pagesInit.getBlockedContent().blockedPopupSave();
        helpersInit.getBaseHelper().closePopup();

        softAssert.assertEquals(pagesInit.getBlockedContent().getTeaserBlockedType(pagesInit.getWidgetClass().getBlockedTeaserTitle()),
                "Blocked websites",
                "FAIL -> don't correct blocked type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
