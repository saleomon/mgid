package dashboard.mgid.publishers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.dash.publisher.variables.PayoutsVariables.identifyPaymentsPopupTextForIndividualClient;
import static pages.dash.publisher.variables.PayoutsVariables.identifyPaymentsPopupTextForLegalEntity;

public class KycForPublishersTests extends TestBase {

    private final String idenfyNotificationText = "To access the full functionality, you need to confirm your identity. Click here to confirm.";
    private final String idenfyProfileLegalEntityText = "You are registered as a Legal Entity. You need to sign a contract. To sign the contract, contact a personal manager.\n" +
            "- For Direct Website sokwages, sokwages@ex.ua";

    private final String idenfyAddChangePursePopupText = "You change/add payment details. Upon confirmation of these actions, your previous verification will become invalid. You will need to confirm your identity again. Are you sure you want to continue?";

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Profile page")
    @Description("Check KYC Notification for Publishers " +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(dataProvider = "dataForKycPublishersPayments", description = "Check KYC Notification for Publishers")
    public void checkKycNotificationForPublishers(String clientEmail) {
        log.info("Test is started");
        authDashAndGo(clientEmail, "publisher");
        Assert.assertEquals(pagesInit.getPayoutsClass().getNotificationText(), idenfyNotificationText,
                "FAIL -> notification!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForKycPublishersPayments() {
        return new Object[][] {
                {"idenfy-pub-individual@mgid.com"},
                {"idenfy-pub-legal@mgid.com"}
        };
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Profile page")
    @Description("Check KYC Notification for Publishers " +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(description = "Check Verification tab in Profile for Legal entity Publishers")
    public void checkKycForPublishersProfileVerificationTabLegalEntity() {
        log.info("Test is started");
        authDashAndGo("idenfy-pub-legal@mgid.com","profile/users");
        pagesInit.getUserProfile().goToProfileIdenfyTab();
        Assert.assertEquals(pagesInit.getPayoutsClass().getVerificationTabText(), idenfyProfileLegalEntityText,
                "FAIL -> Profile Verification Legal Entity Text!");
        log.info("Test is finished");
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Publisher payouts page")
    @Description("Check KYC confirmation popup for individual Publishers if change payment method" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(description = "Check KYC confirmation popup for individual Publishers if change payment method")
    public void checkKycForPublishersChangePaymentMethodIndividual() {
        log.info("Test is started");
        authDashAndGo("idenfy-pub-individual@mgid.com","publisher/payouts");
        pagesInit.getPayoutsClass().selectRandomFavoriteWallet();
        Assert.assertEquals(pagesInit.getPayoutsClass().getConfirmationPopupText(), identifyPaymentsPopupTextForIndividualClient,
                "FAIL -> Confirmation Idenfy popup!");
        log.info("Test is finished");
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Publishers payouts page")
    @Description("Check KYC alert popup for Legal entity Publishers if change payment method" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(description = "Check KYC confirmation popup for Legal entity Publishers if change payment method")
    public void checkKycForPublishersChangePaymentMethodLegalEntity() {
        log.info("Test is started");
        authDashAndGo("idenfy-pub-legal@mgid.com","publisher/payouts");
        pagesInit.getPayoutsClass().selectRandomFavoriteWallet();
        Assert.assertEquals(pagesInit.getPayoutsClass().getAlertPopupText(), identifyPaymentsPopupTextForLegalEntity,
                "FAIL -> Alert Idenfy popup!");
        log.info("Test is finished");
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Publishers payouts page")
    @Description("Check KYC confirmation popup for individual Publishers if resume payment" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(description = "Check KYC confirmation popup for individual Publishers if resume payment")
    public void checkKycForPublishersResumePaymentsIndividual() {
        log.info("Test is started");
        log.info("Enable privilege 'publisher/display_hold_resume_button'");
        authDashAndGo("idenfy-pub-individual@mgid.com","publisher/payouts");
        pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
        softAssert.assertEquals(pagesInit.getPayoutsClass().getConfirmationPopupText(), identifyPaymentsPopupTextForIndividualClient,
        "FAIL -> Confirmation Idenfy popup!");
        pagesInit.getPayoutsClass().confirmIdenfyPopup();
        softAssert.assertTrue(pagesInit.getCampaigns().isStartVerificationButtonDisplayed(),
        "FAIL -> 'START VERIFICATION' button is not displayed!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Payments payouts page")
    @Description("Check KYC confirmation popup for Legal entity Publishers if resume payment" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(description = "Check KYC confirmation popup for Legal entity Publishers if resume payment")
    public void checkKycForPublishersResumePaymentsLegalEntity() {
        log.info("Test is started");
        log.info("Enable privilege 'publisher/display_hold_resume_button'");
        authDashAndGo("idenfy-pub-legal@mgid.com","publisher/payouts");
        pagesInit.getPayoutsClass().clickHoldResumePaymentsButton();
        Assert.assertEquals(pagesInit.getPayoutsClass().getAlertPopupText(), identifyPaymentsPopupTextForLegalEntity,
        "FAIL -> Alert Idenfy popup!");

        log.info("Test is finished");
    }

    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Publishers payouts page")
    @Description("Check KYC text in Add/Edit popup for individual and legal entity Publishers if add payment method" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(dataProvider = "dataForKycPublishersPayments", description = "Check KYC text in Add/Edit popup for individual and legal entity Publishers if add payment method")
    public void checkKycForPublishersAddPaymentMethod(String clientEmail) {
        log.info("Test is started");
        authDashAndGo(clientEmail,"publisher/payouts");
        pagesInit.getPayoutsClass().openAddPursePopup();
        Assert.assertEquals(pagesInit.getPayoutsClass().getAddPursePopupText(), idenfyAddChangePursePopupText,
                "FAIL -> Add purse popup Idenfy text for client " + clientEmail);
        log.info("Test is finished");
    }


    @Epic("Dashboard. KYC for Publishers")
    @Feature("KYC Notification for Publishers")
    @Story("Publishers payouts page")
    @Description("Check KYC text in Add/Edit popup for individual and legal entity Publishers if edit payment method" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52653\">Ticket TA-52653</a>")
    @Owner("AIA")
    @Test(dataProvider = "dataForKycPublishersPayments", description = "Check KYC text in Add/Edit popup for individual and legal entity Publishers if edit payment method")
    public void checkKycForPublishersEditPaymentMethodIndividual(String clientEmail) {
        log.info("Test is started");
        authDashAndGo(clientEmail,"publisher/payouts");
        pagesInit.getPayoutsClass().openEditPursePopup();
        Assert.assertEquals(pagesInit.getPayoutsClass().getAddPursePopupText(), idenfyAddChangePursePopupText,
                "FAIL -> Confirmation Idenfy popup!");
        log.info("Test is finished");
    }
}

