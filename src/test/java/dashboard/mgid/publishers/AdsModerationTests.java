package dashboard.mgid.publishers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.helpers.sorted.SortedType;
import testBase.TestBase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AdsModerationTests extends TestBase {

    public void goToAdsModeration(int siteId) {
        authDashAndGo("testEmail60@ex.ua","publisher/ads-moderation/site/" + siteId);
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("filter status ads <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "filter status ads")
    public void filterStatusAds() {
        log.info("Test is started");
        goToAdsModeration(96);
        pagesInit.getAdsModeration().selectStatusFilter("active");
        pagesInit.getAdsModeration().clickFilterButton();
        int activeAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkStatusAds("active");

        pagesInit.getAdsModeration().selectStatusFilter("blocked");
        pagesInit.getAdsModeration().clickFilterButton();
        int blockedAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkStatusAds("blocked");

        pagesInit.getAdsModeration().selectStatusFilter("pending");
        pagesInit.getAdsModeration().clickFilterButton();
        int pendingAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkStatusAds("pending");

        pagesInit.getAdsModeration().selectStatusFilter("all");
        pagesInit.getAdsModeration().clickFilterButton();
        Assert.assertEquals(pagesInit.getAdsModeration().getAdsOnPage().size(), (activeAds + blockedAds + pendingAds));

        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Add search input for searching specific creatives by keywords")
    @Description("filter status ads <a href='https://jira.mgid.com/browse/TA-52149'>TA-52149</a>")
    @Test(description = "Add search input for searching specific creatives by keywords", dataProvider = "dataFilterSearchAdd")
    public void filterSearch(String searchValue, String id) {
        log.info("Test is started");
        goToAdsModeration(96);
        pagesInit.getAdsModeration().fillValueInSearchFilter(searchValue);
        pagesInit.getAdsModeration().clickFilterButton();

        softAssert.assertEquals(pagesInit.getAdsModeration().getAdsOnPage().size(), 1, "FAIL -> count adds on page");
        softAssert.assertEquals(pagesInit.getAdsModeration().getAdsOnPage().get(0).attr("id"), id, "FAIL -> teaser id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataFilterSearchAdd() {
        return new Object[][]{
                {"Teaser Title_30",         "row-30-15"},
                {"https://testsitemg3.com", "row-19-15"}
        };
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("filter status ads <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "filter status ads")
    public void filterCategoryAds() {
        log.info("Test is started");
        goToAdsModeration(96);
        pagesInit.getAdsModeration().selectCategoryFilter("103");
        pagesInit.getAdsModeration().clickFilterButton();
        int eventsAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkCategoryAds("Events");

        pagesInit.getAdsModeration().selectCategoryFilter("100");
        pagesInit.getAdsModeration().clickFilterButton();
        int automotiveAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkCategoryAds("Automotive");

        pagesInit.getAdsModeration().selectCategoryFilter("254");
        pagesInit.getAdsModeration().clickFilterButton();
        int currenciesAds = pagesInit.getAdsModeration().getAdsOnPage().size();
        pagesInit.getAdsModeration().checkCategoryAds("Currencies");

        pagesInit.getAdsModeration().selectCategoryFilter("all");
        pagesInit.getAdsModeration().clickFilterButton();
        Assert.assertEquals(pagesInit.getAdsModeration().getAdsOnPage().size(), (eventsAds + automotiveAds + currenciesAds));

        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("change status tumbler pending to blocked <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "change status tumbler pending to blocked")
    public void changeStatusTumblerPendingToBlocked() {
        log.info("Test is started");
        int adsId = 20;
        goToAdsModeration(96);
        pagesInit.getAdsModeration().switchAdsState(false, adsId);
        Assert.assertEquals(pagesInit.getAdsModeration().getStatusAds(adsId), "Blocked");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("change status tumbler pending to Active <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "change status tumbler pending to Active")
    public void changeStatusTumblerPendingToActive() {
        log.info("Test is started");
        int adsId = 26;
        goToAdsModeration(96);
        pagesInit.getAdsModeration().switchAdsState(true, adsId);
        Assert.assertEquals(pagesInit.getAdsModeration().getStatusAds(adsId), "Active");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("change status tumbler active to blocked <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "change status tumbler active to blocked")
    public void changeStatusTumblerActiveToBlocked() {
        log.info("Test is started");
        int adsId = 8;
        goToAdsModeration(96);
        pagesInit.getAdsModeration().switchAdsState(false, adsId);
        Assert.assertEquals(pagesInit.getAdsModeration().getStatusAds(adsId), "Blocked");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Dashboard | Create a moderation management interface")
    @Description("change status tumbler blocked to Active <a href='https://jira.mgid.com/browse/KOT-2930'>KOT-2930</a>")
    @Test(description = "change status tumbler blocked to Active")
    public void changeStatusTumblerBlockedToActive() {
        log.info("Test is started");
        int adsId = 41;
        goToAdsModeration(96);
        pagesInit.getAdsModeration().switchAdsState(true, adsId);
        Assert.assertEquals(pagesInit.getAdsModeration().getStatusAds(adsId), "Active");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Add sorting for columns: Title / Category / Status / Imps")
    @Description("check sort by IMPS <a href='https://jira.mgid.com/browse/TA-52148'>TA-52148</a>")
    @Test(description = "check sort by IMPS")
    public void checkSortByImps() {
        log.info("Test is started");
        goToAdsModeration(96);
        Assert.assertTrue(helpersInit.getSortedHelper().checkSorted(new SortedType().randomPeriod(), "imps"), "FAIL - sort failed");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Add sorting for columns: Title / Category / Status / Imps")
    @Description("check sort by Title <a href='https://jira.mgid.com/browse/TA-52148'>TA-52148</a>")
    @Test(description = "check sort by Title")
    public void checkSortByTitle() {
        log.info("Test is started");
        goToAdsModeration(96);
        Assert.assertTrue(helpersInit.getSortedHelper().checkSortedText(new SortedType().randomPeriod(), "title"), "FAIL - sort failed");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Add sorting for columns: Title / Category / Status / Imps")
    @Description("check sort by Category <a href='https://jira.mgid.com/browse/TA-52148'>TA-52148</a>")
    @Test(description = "check sort by Category")
    public void checkSortByCategory() {
        log.info("Test is started");
        goToAdsModeration(96);
        SortedType.SortType sort = new SortedType().randomPeriod();
        Object[] list = helpersInit.getSortedHelper().getSortedData(sort, "category");
        List<Object> l = Arrays.asList(list);
        Collections.replaceAll(l,"Automotive", "100");
        Collections.replaceAll(l,"Events", "103");
        Collections.replaceAll(l,"Currencies", "254");

        Assert.assertTrue(helpersInit.getSortedHelper().checkSortedData(sort, l.toArray()), "FAIL - sort failed");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Add sorting for columns: Title / Category / Status / Imps")
    @Description("check sort by status <a href='https://jira.mgid.com/browse/TA-52148'>TA-52148</a>")
    @Test(description = "check sort by status")
    public void checkSortByStatus() {
        log.info("Test is started");
        goToAdsModeration(96);
        Assert.assertTrue(helpersInit.getSortedHelper().checkSortedText(new SortedType().randomPeriod(), "status"), "FAIL - sort failed");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Ad comment")
    @Description("check add ad comment <a href='https://jira.mgid.com/browse/TA-52173'>TA-52173</a>")
    @Test(description = "check add ad comment")
    public void checkAddAdComment() {
        log.info("Test is started");
        goToAdsModeration(127);

        log.info("Check activity icon before add");
        softAssert.assertFalse(pagesInit.getAdsModeration().isActiveAdCommentIcon(), "Fail - icon is active create");

        log.info("Add comment");
        pagesInit.getAdsModeration().openAdsCommentPopup();
        pagesInit.getAdsModeration().setAdsComment("Husky").saveAdsComment();

        log.info("Check icon and comment after added it");
        softAssert.assertTrue(pagesInit.getAdsModeration().isActiveAdCommentIcon(), "Fail - icon isn't active create");
        pagesInit.getAdsModeration().openAdsCommentPopup();
        softAssert.assertEquals(pagesInit.getAdsModeration().getAdCommentValue(), "Husky", "Fail - Husky");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Ad comment")
    @Description("check edit ad comment <a href='https://jira.mgid.com/browse/TA-52173'>TA-52173</a>")
    @Test(description = "check edit ad comment")
    public void checkEditAdComment() {
        log.info("Test is started");

        goToAdsModeration(128);
        log.info("Edit comment");
        pagesInit.getAdsModeration().openAdsCommentPopup();
        pagesInit.getAdsModeration().editAdsComment("Ottawa").saveAdsComment();

        log.info("Check icon and comment after edit it");
        softAssert.assertTrue(pagesInit.getAdsModeration().isActiveAdCommentIcon(), "Fail - icon isn't active edit");
        pagesInit.getAdsModeration().openAdsCommentPopup();
        softAssert.assertEquals(pagesInit.getAdsModeration().getAdCommentValue(), "Ottawa", "Fail - Ottawa");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Ad comment")
    @Description("check cancel edit ad button <a href='https://jira.mgid.com/browse/TA-52173'>TA-52173</a>")
    @Test(description = "check cancel edit ad button")
    public void checkCancelButtonForAdComment() {
        log.info("Test is started");
        goToAdsModeration(129);

        log.info("Add comment and click cancel button");
        pagesInit.getAdsModeration().openAdsCommentPopup();
        pagesInit.getAdsModeration().setAdsComment("Husky").clickCancelButton();

        log.info("Check icon and comment after added it");
        Assert.assertFalse(pagesInit.getAdsModeration().isActiveAdCommentIcon(), "Fail - icon isn't active create");
        log.info("Test is finished");
    }
}
