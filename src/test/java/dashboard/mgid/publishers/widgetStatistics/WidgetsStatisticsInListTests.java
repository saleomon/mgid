package dashboard.mgid.publishers.widgetStatistics;

import org.testng.annotations.Test;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

public class WidgetsStatisticsInListTests extends TestBase {

    /**
     * Проверка статистики в интерфейсе список виджетов (dashboard.mgid.com/publisher/widgets/site/ + id(website))
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for wages, clicks, adRequests, impressions</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkStatInListWidgets() {
        log.info("Test is started");
        authDashAndGo("publisher/widgets/site/1");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_WEEK;
        pagesInit.getWidgetStatisticClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из графика по параметрам - Wages, Clicks, ad_requests, eCpm, CPC, impressions");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_clicks");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_wages");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWidgetStatisticClass().getDataCpcFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWidgetStatisticClass().getDataEcpmInGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWidgetStatisticClass().getDataColumnByHeaderInWidgetsList();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatInWidgetsList();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromPublisherStatisticsClickHouse(1);

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for wages, clicks, adRequests, impressions");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum adRequest");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkEcpmInGraph(), "FAIL -> eCpm in graph");

        //log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate");
        //softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkEcpmInTable(), "FAIL -> eCpm in table");
        //softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkCpcInTable(), "FAIL -> cpc in table");
        //softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkCtrInTable(), "FAIL -> ctr in table");
        //softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkTotalEcpm(), "FAIL -> eCpm total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkTotalCpc(), "FAIL -> cpc total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkTotalCtr(), "FAIL -> ctr total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().widgetList_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
