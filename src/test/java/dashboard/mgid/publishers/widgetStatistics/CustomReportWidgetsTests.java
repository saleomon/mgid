package dashboard.mgid.publishers.widgetStatistics;

import org.testng.*;
import org.testng.annotations.*;
import core.helpers.statCalendar.CalendarForStatistics;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import core.service.Retry;
import testBase.TestBase;

import static core.helpers.BaseHelper.getRandomFromArray;

public class CustomReportWidgetsTests extends TestBase {

    private final int widgetId = 1;

    /**
     * Проверка отображения всех показателей и метрик в Конструкторе отчётов
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByAllParams() {
        log.info("Test is started");
        String[] reportParameters = {"date", "device", "traffic_type", "pubsrcid", "country", "os", "traffic_source"};
        String[] metrics = {"ad_requests", "visibility_rate", "wages", "cpc", "impressions", "clicks", "r_cpm", "ctr", "e_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, metrics);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(17), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(metrics), "FAIL -> sorted headers");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Дата" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for AdRequest, Impressions</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByDate() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] metrics = {"ad_requests", "impressions", "e_cpm", "wages"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, metrics);

        log.info("заполняем данными масивы ad_requests and impressions");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(5), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(metrics), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for AdRequest, Impressions");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkEcpmInTable(), "FAIL -> e_cpm in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalEcpm(), "FAIL -> eCpm total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Тип устройства" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, Clicks</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для CPC</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CPC</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для CPC</li>
     * <li>5. что при фильтре по deviceType данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.dt00.net/issue/PAS-23125">PAS-23125</a>
     */
    @Test
    public void checkCustomSourceByDeviceType() {
        log.info("Test is started");

        String[] reportParameters = {"device"};
        String[] sortsHeaders = {"cpc", "clicks", "wages"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_WEEK;
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы clicks, wages, cpc");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWidgetStatisticClass().getDataCpcFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для CPC");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkCpcInGraph(), "FAIL -> cpc in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CPC");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkCpcInTable(), "FAIL -> cpc in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для CPC");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalCpc(), "FAIL -> CPC total");

        log.info("Проверяем работу фильтра по device");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithSelect(), "FAIL -> device filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Тип источника" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, AdRequest</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для rCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для rCpm</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для rCpm</li>
     * <li>5. что при фильтре по traffic_type данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test(retryAnalyzer = Retry.class)
    public void checkCustomSourceByTrafficType() {
        log.info("Test is started");
        String[] reportParameters = {"traffic_type"};
        String[] sortsHeaders = {"ad_requests", "wages", "r_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_SEVEN;
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы ad_requests, wages, r_cpm");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_r_cpm");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataRcpmFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_revenue");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, AdRequest");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для rCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkRcpmInGraph(), "FAIL -> r_cpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для rCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkRcpmInTable(), "FAIL -> r_cpm in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для rCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalRcpm(), "FAIL -> rCpm total");

        log.info("Проверяем работу фильтра по traffic_type");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithSelect(), "FAIL -> traffic_type filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Страна" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, AdRequest, Impressions</li>
     * <li>2. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для visibilityRate</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для visibilityRate</li>
     * <li>4. что при фильтре по country данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByCountry() {
        log.info("Test is started");
        int countryForFilter = getRandomFromArray(new int[]{35, 36, 37, 38, 39});
        String[] reportParameters = {"country"};
        String[] sortsHeaders = {"ad_requests", "visibility_rate", "wages", "impressions"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_30_DAYS;
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы ad_requests, visibility_rate, wages, impressions");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_revenue");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(5), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, AdRequest, Impressions");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalVisibilityRate(), "FAIL -> visibilityRate total");

        log.info("Проверяем работу фильтра по country");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithSelect(countryForFilter), "FAIL -> country filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "ОС" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Impressions, Clicks</li>
     * <li>2. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для ctr</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для ctr</li>
     * <li>4. что при фильтре по OS данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByOs() {
        log.info("Test is started");
        int osForFilter = getRandomFromArray(new int[]{10, 49, 24});
        String[] reportParameters = {"os"};
        String[] sortsHeaders = {"impressions", "clicks", "ctr"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_MONTH;
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы impressions and clicks");
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Impressions, Clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для CTR");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkCtrInTable(), "FAIL -> ctr in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для CTR");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalCtr(), "FAIL -> CTR total");

        log.info("Проверяем работу фильтра по os");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithSelect(osForFilter), "FAIL -> OS filter");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "Источник" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, AdRequest</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для rCpm</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для rCpm</li>
     * <li>4. что при фильтре по traffic_source данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceByTrafficSource() {
        log.info("Test is started");
        String[] reportParameters = {"traffic_source"};
        String[] sortsHeaders = {"ad_requests", "wages", "r_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы ad_requests, wages, r_cpm");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_r_cpm");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataRcpmFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_revenue");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, AdRequest");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для r_cpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkRcpmInGraph(), "FAIL -> r_cpm in graph");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для r_cpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalRcpm(), "FAIL -> r_cpm total");

        log.info("Проверяем работу фильтра по traffic_source");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithInput(), "FAIL -> traffic_source filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "SubId(pubsrcid)" с доп проверкой подсчёта данных в графике и таблице за определённый период + проверка отображения всех выбранных показателей в табе "Фильтры"
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, AdRequest</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для rCpm</li>
     * <li>3. что данные в TOTAL Table рассчитываются верно(согласно формуле) для rCpm</li>
     * <li>4. что при фильтре по pubsrcid данные выводяться верно</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceBySubId() {
        log.info("Test is started");
        String[] reportParameters = {"pubsrcid"};
        String[] sortsHeaders = {"ad_requests", "wages", "r_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("заполняем данными масивы ad_requests, wages, r_cpm");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_r_cpm");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataRcpmFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_revenue");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Проверяем количество отображаемых колонок и присутствие обязательных сортировок");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(4), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Clicks, etc.");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForAdRequest(), "FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages view in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для r_cpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkRcpmInGraph(), "FAIL -> r_cpm in graph");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для r_cpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalRcpm(), "FAIL -> r_cpm total");

        log.info("Проверяем работу фильтра по pubsrcid");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkWorkFilterByDimensionWithInput(), "FAIL -> pubsrcid filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка на показателе "SubId(pubsrcid)" невалидных значений
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * Появление ошибки валидации на не валидное значение
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceBySubIdValid() {
        log.info("Test is started");
        String[] reportParameters = {"pubsrcid"};
        String[] sortsHeaders = {"ad_requests", "wages", "r_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        Assert.assertEquals(pagesInit.getWidgetStatisticClass().customReport_getMessageForNotValidSubId(),
                "No data could be found",
                "FAIL -> doesn't correct message");
        log.info("Test is finished");
    }

    /**
     * Проверка в табе "Фильтры" работы метрик с типами данных int, double на действия заданные в фильтре(математические действия сравнения)
     * Проверяем фильтр по математическим действиям на "ad_requests", "impressions", "clicks" и с плавающей точкой "wages", "cpc", "e_cpm"
     * <p>RKO</p>
     */
    @Test
    public void checkCustomSourceOtherParams() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] sortsHeaders = {"ad_requests", "impressions", "visibility_rate", "clicks", "wages", "r_cpm", "cpc", "ctr", "e_cpm"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        Assert.assertTrue(pagesInit.getWidgetStatisticClass().checkMinOrMaxValueSelect("ad_requests", "impressions", "clicks")
                && pagesInit.getWidgetStatisticClass().checkMinOrMaxValueSelect("wages", "e_cpm"));
        log.info("Test is finished");
    }

    /**
     * Проверка Создания/Редактирования/Удаления отчёта
     * <p>RKO</p>
     */
    @Test
    public void checkCreateEditDeleteCustomReport() {
        log.info("Test is started");
        String[] reportParameters = {"date"};
        String[] sortsHeaders = {"ad_requests", "impressions"};
        goToCustomReportWidget(widgetId);

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.LAST_WEEK;
        pagesInit.getWidgetStatisticClass().customReport_choseRandomOptions(getRandomPeriod, reportParameters, sortsHeaders);

        log.info("create custom report on choose params");
        pagesInit.getWidgetStatisticClass().createCustomReport();

        log.info("go to saved custom report and check him");
        goToSavedCustomReport();

        log.info("сравниваем основные параметры в графике, таблице и total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkCustomReportInListInterfaceAndGoHim(pagesInit.getWidgetStatisticClass().getCustomReportName()));
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(3), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().customReport_checkShowAllSortedByHeaders(sortsHeaders), "FAIL -> sorted headers");

        log.info("go to saved custom report and delete him");
        goToSavedCustomReport();
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().deleteCustomReport(pagesInit.getWidgetStatisticClass().getCustomReportName()), "FAIL -> delete custom report");

        log.info("Test is finished");
    }

    public void goToCustomReportWidget(int widgetId) {
        authDashAndGo("publisher/custom-report/id/" + widgetId + "/type/composite");
    }

    public void goToSavedCustomReport() {
        authDashAndGo("publisher/client-custom-reports/id/1/type/composite");
    }
}
