package dashboard.mgid.publishers.widgetStatistics;

import io.qameta.allure.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.helpers.statCalendar.CalendarForStatistics;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;
import testBase.TestBase;

import java.time.LocalDate;
import java.time.ZoneId;

public class WidgetsStatisticsByDayByCountryEtcTests extends TestBase {

    @Epic("Dash Publisher Statistics")
    @Story("Dash Publisher Statistics Widgets by COUNTRY/SOURCE/SUBID/Device")
    @Description("<ul>" +
            "<li>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</li>" +
            "<li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks</li>" +
            "<li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm, CPC, rCpm</li>" +
            "<li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, rCpm, visibilityRate</li>" +
            "<li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate</li>" +
            "<li>widget_statistics_summ; widget_clicks_goods; widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(dataProvider = "byStaticTabsWidgets", description = "check Publisher Statistics Widgets in dash by COUNTRY/SOURCE/SUBID/Device")
    public void checkStatByStat(String tabName, String tabValue) {
        int widgetId = 1;
        log.info("Test is started -> " + tabName);
        authDashAndGo("publisher/widget-stat-by-" + tabValue + "/id/" + widgetId + "/type/composite");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = new CalendarForStatistics().randomPeriod();
        pagesInit.getWidgetStatisticClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из графика по параметрам - Wages, Clicks, ad_requests, rCPM, eCpm, CPC, impressions");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_impressions");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWidgetStatisticClass().getDataCpcFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_r_cpm");
        pagesInit.getWidgetStatisticClass().getDataRcpmFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWidgetStatisticClass().getDataEcpmInGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWidgetStatisticClass().getDataColumnByHeader();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatBy();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(widgetId);

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(15), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkSumGraphSumTableTotalTableForAdRequest(),"FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eCpm, CPC, rCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkCpcInGraph(), "FAIL -> cpc in graph");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkRcpmInGraph(), "FAIL -> rCpm in graph");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkEcpmInGraph(), "FAIL -> eCpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, rCpm, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkEcpmInTable(), "FAIL -> eCpm in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkRcpmInTable(), "FAIL -> rCpm in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalEcpm(), "FAIL -> eCpm total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalCpc(), "FAIL -> cpc total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalCtr(), "FAIL -> ctr total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalRcpm(), "FAIL -> rCpm total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statBy_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished -> " + tabName);
    }

    @DataProvider
    public Object[][] byStaticTabsWidgets(){
        return new Object[][]{
                {"By Country stat", "country"},
                {"By Source stat", "traffic-type"},
                {"By Device stat", "device"},
                {"By SubId stat", "subid"}

        };
    }

    /**
     * Проверка статистики в интерфейсе BY DAY (dashboard.mgid.com/publisher/widget-stat/id/ + widgetId + /type/composite)
     * <p>(contract_type='RS')Фильтруем статистику за рандомный период из календаря и проверяем:</p>
     * <ul>
     * <li>1. sum(Graph) == sum(Table) == Total(field) == sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks</li>
     * <li>2. что данные в Graph рассчитываются верно(согласно формуле) тдельно по каждому дню для eCpm</li>
     * <li>3. что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate</li>
     * <li>4. что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void checkStatByDay() {
        log.info("Test is started");
        authDashAndGo("publisher/widget-stat/id/1/type/composite");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        CalendarPeriods getRandomPeriod = CalendarPeriods.THIS_WEEK;
        pagesInit.getWidgetStatisticClass().selectCalendarPeriodJs(getRandomPeriod);

        log.info("получаем стату из Graph по параметрам - Wages, Clicks, ad_requests, rCPM, eCpm, CPC, impressions");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_wages");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_impressions");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWidgetStatisticClass().getDataCpcFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWidgetStatisticClass().getDataEcpmInGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWidgetStatisticClass().getDataColumnByHeaderForStatByDay();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatByDay();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromPublisherStatisticsClickHouse(1);

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(14), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForAdRequest(),"FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkEcpmInGraph(), "FAIL -> eCpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkEcpmInTable(), "FAIL -> eCpm in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalCpc(), "FAIL -> CPC total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalEcpm(), "FAIL -> eCpm total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalCtr(), "FAIL -> CTR total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test(dataProvider = "periodsByHours")
    public void checkStatByHours(CalendarPeriods period) {
        log.info("Test is started");

        authDashAndGo("publisher/widget-stat-by-hours/id/95");

        log.info("выбираем random период в календаре за который хотим вывести стату");
        pagesInit.getWidgetStatisticClass().selectCalendarPeriodJs(period);
        pagesInit.getWidgetStatisticClass().loadDataIfPaginatorDisplayed();

        log.info("получаем стату из Graph по параметрам - Wages, Clicks, ad_requests, rCPM, eCpm, CPC, impressions");
        pagesInit.getWidgetStatisticClass().switchParamInGraph("impressions_e_cpm");
        pagesInit.getWidgetStatisticClass().getDataEcpmInGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_wages");
        pagesInit.getWidgetStatisticClass().getDataWagesFromGraph();
        pagesInit.getWidgetStatisticClass().getDataClicksFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("ad_requests_impressions");
        pagesInit.getWidgetStatisticClass().getDataAdRequestsFromGraph();
        pagesInit.getWidgetStatisticClass().getDataImpressionsFromGraph();
        pagesInit.getWidgetStatisticClass().switchParamInGraph("clicks_cpc");
        pagesInit.getWidgetStatisticClass().getDataCpcFromGraph();

        log.info("заполняем даннми масивы (Wages, Clicks, etc.) из колонок таблицы");
        pagesInit.getWidgetStatisticClass().getDataColumnByHeaderForStatByDay();

        log.info("получаем локаторы TOTAL (Wages, Clicks, etc.) by name<th>");
        pagesInit.getWidgetStatisticClass().getTotalElementForStatByDay();

        log.info("получаем из базы TOTAL for Wages, Clicks, etc.");
        pagesInit.getWidgetStatisticClass().getTotalDataFromPublisherStatisticsClickHouse(95);

        log.info("Сравниваем сумму sum(Graph) c sum(Table) c Total(field) c sum(ClickHouse) for Wages, Impressions, AdRequest, Clicks");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().checkSizeHeadersInBaseTable(12), "FAIL -> header size");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForAdRequest(),"FAIL -> sum pages view in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForImpressions(), "FAIL -> sum impressions in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForClicks(), "FAIL -> sum clicks in table/graph/total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkSumGraphSumTableTotalTableForWages(), "FAIL -> sum wages in table/graph/total");

        log.info("Проверяем что данные в Graph рассчитываются верно(согласно формуле) для eCpm");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkEcpmInGraph(), "FAIL -> eCpm in graph");

        log.info("Проверяем что данные в столбцах Table рассчитываются верно(согласно формуле) (по строчно) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkEcpmInTable(), "FAIL -> eCpm in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkCpcInTable(), "FAIL -> cpc in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkCtrInTable(), "FAIL -> ctr in table");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkVisibilityRateInTable(), "FAIL -> visibilityRate in table");

        log.info("Проверяем что данные в TOTAL Table рассчитываются верно(согласно формуле) для eCpm, CPC, CTR, visibilityRate");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalCpc(), "FAIL -> CPC total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalEcpm(), "FAIL -> eCpm total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalCtr(), "FAIL -> CTR total");
        softAssert.assertTrue(pagesInit.getWidgetStatisticClass().statByDay_checkTotalVisibilityRate(), "FAIL -> VisibilityRate total");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] periodsByHours(){

        ZoneId zoneId           = ZoneId.of("Pacific/Honolulu");
        LocalDate dayEnd        = LocalDate.now(zoneId).minusDays(1);
        LocalDate dayStart      = dayEnd.minusDays(1);

        return new Object[][]{
                {CalendarPeriods.INTERVAL.setDateStart(dayStart).setDateEnd(dayEnd)},
                {CalendarPeriods.YESTERDAY}
        };
    }
}
