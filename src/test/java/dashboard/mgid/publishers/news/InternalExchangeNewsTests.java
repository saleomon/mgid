package dashboard.mgid.publishers.news;

import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static libs.mongoDb.exchange.News.insertNews;
import static libs.mongoDb.variables.NewsVariables.*;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.ClientsEntities.WEBSITE_FOR_INTERNAL_NEWS_MGID;
import static testData.project.RoleIdsDash.ADMIN_MGID_ROLE;

public class InternalExchangeNewsTests extends TestBase {
    public InternalExchangeNewsTests() {
        clientLogin = CLIENTS_PAYOUTS_LOGIN;
    }

    /**
     * Проверка работы фильтра Promo для новостей, которые сейчас промоутятся или промоутились ранее
     * <ul>
     *     <li>Проверяется отображение свитчера по праву</li>
     *     <li>Проверяется фильтрация новостей при включении свитчера</li>
     *     <li>Проверяется отображение свитчера по опции "Opportunity to promote news" в настройках сайта</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24067">Ticket TA-24067</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24261">Ticket TA-24261</a>
     * <p>Author AIA</p>
     */
    @Privilege(name = "publisher/promo-news")
    @Test
    public void checkInternalAdsPromotionalFilterTest() {
        log.info("Test is started");
        insertNews(newsItem1, newsItem2);
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(false, ADMIN_MGID_ROLE,
                "publisher/promo-news");
        authDashAndGo("publisher/internal-ads/site_id/" + WEBSITE_FOR_INTERNAL_NEWS_MGID);
        softAssert.assertFalse(pagesInit.getInternalAds().checkPromoFilterSwitcherIsDisplayed(),
                "FAIL -> Promo switcher is present with disabled rights!");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeDash(true, ADMIN_MGID_ROLE,
                "publisher/promo-news");
        authDashAndGo("publisher/internal-ads/site_id/" + WEBSITE_FOR_INTERNAL_NEWS_MGID);
        softAssert.assertTrue(pagesInit.getInternalAds().checkPromoFilterSwitcherIsDisplayed(),
                "FAIL -> Promo switcher isn't present with enabled rights!");
        softAssert.assertTrue(pagesInit.getInternalAds().checkPromoFilterSwitcherIsOff(),
                "FAIL -> Promo switcher disabled!");
        softAssert.assertEquals(pagesInit.getInternalAds().getNewsCount(), 2,
                "FAIL -> Count all news!");
        log.info("* Enable Promo-filter *");
        pagesInit.getInternalAds().enablePromoFilter();
        softAssert.assertTrue(pagesInit.getInternalAds().checkPromoFilterSwitcherIsOn(),
                "FAIL -> Promo switcher enabled!");
        softAssert.assertEquals(pagesInit.getInternalAds().getNewsCount(), 1,
                "FAIL -> Count only promo-news!");
        authCabAndGo("wages/sites-edit/id/" + WEBSITE_FOR_INTERNAL_NEWS_MGID);
        pagesInit.getCabWebsites().switchOpportunityToPromoteNews(false);
        authDashAndGo("publisher/internal-ads/site_id/" + WEBSITE_FOR_INTERNAL_NEWS_MGID);
        softAssert.assertFalse(pagesInit.getInternalAds().checkPromoFilterSwitcherIsDisplayed(),
                "FAIL -> Promo switcher is present with disabled settings!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка работы кнопок массовых действий для новостей внутреннего обмена
     * <ul>
     *     <li>Проверяется блокировка</li>
     *     <li>Проверяется разблокировка</li>
     *     <li>Проверяется удаление</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21994">Ticket TA-21994</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkMassActionButtonsForInternalAds() {
        log.info("Test is started");
        insertNews(newsItem3, newsItem4, newsItem5, newsItem6, newsItem7, newsItem8, newsItem9,
                newsItem10, newsItem11);
        authDashAndGo("publisher/internal-ads/site_id/" + WEBSITE_FOR_INTERNAL_NEWS_MGID);
        log.info("Block internal ads by mass action");
        pagesInit.getInternalAds().blockNewsByMassAction();
        softAssert.assertTrue(pagesInit.getInternalAds().checkNewsStatuses("Blocked"),
                "FAIL -> After blocking news!");
        pagesInit.getInternalAds().unblockNewsByMassAction();
        log.info("Unblock internal ads by mass action");
        softAssert.assertTrue(pagesInit.getInternalAds().checkNewsStatuses("Active"),
                "FAIL -> After activating news!");
        pagesInit.getInternalAds().deleteNewsByMassAction();
        log.info("Delete internal ads by mass action");
        softAssert.assertEquals(0, pagesInit.getInternalAds().getNewsCount(),
                "FAIL -> After deleting news!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
