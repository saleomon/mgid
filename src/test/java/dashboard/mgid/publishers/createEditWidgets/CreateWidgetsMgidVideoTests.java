package dashboard.mgid.publishers.createEditWidgets;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testData.project.publishers.VideoCfgData;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;

import static core.helpers.BaseHelper.stringToJson;
import static core.service.constantTemplates.ConstantsInit.AIA;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static testData.project.ClientsEntities.*;

public class CreateWidgetsMgidVideoTests extends TestBase {

    VideoCfgData videoCfgData = new VideoCfgData();


    public void goToCreateWidget() {
        authDashAndGo(CLIENTS_VIDEO_LOGIN, "publisher/add-widget/site/" + WEBSITE_MGID_FOR_VIDEO_ID);
    }

    public void goToEditWidget(int widgetID) {
        authDashAndGo(CLIENTS_VIDEO_LOGIN,"publisher/edit-widget/id/" + widgetID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("VIDEO::check privilege work 'can_manipulate_with_video'")
    @Owner(RKO)
    @Test(description = "VIDEO::check privilege work 'can_manipulate_with_video'")
    @Privilege(name = "default/publisher/can_manipulate_with_video")
    public void checkVideoIsAvailableForClient() {
        log.info("Test is started");
        authDashAndGo("testEmail82@ex.ua", "publisher/add-widget/site/" + 145);
        Assert.assertFalse(pagesInit.getWidgetClass().videoTumblerIsDisplayed());
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("VIDEO::InStream")
    @Owner(RKO)
    @Test(description = "VIDEO::InStream")
    public void createInStreamWidget() {
        log.info("Test is started");
        goToCreateWidget();

        log.info("create widget");
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertNotNull(pagesInit.getWidgetClass().createVideoWidget(VideoFormat.INSTREAM, true, false),
                "FAIL -> Video Instream Desktop widget was not created!");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.INSTREAM, true, false), "FAIL -> checkVideoWidget");

        log.info("edit widget");
        pagesInit.getWidgetClass().editVideoWidget(VideoFormat.INSTREAM, true, true);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.INSTREAM, true, true), "FAIL -> checkVideoWidget after edit");

        int compositeId = pagesInit.getWidgetClass().getWidgetId();
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(
                        pagesInit.getWidgetClass().getWidgetId()).stream().anyMatch(i -> i.equals("vr")),
                "FAIL -> VR placement type is absent");

        authCabAndGo("wages/widgets/?c_id=" + compositeId);
        authCabAndGo("wages/informers-manage-video-cfg/id/" + pagesInit.getCabWidgetsList().getVideoPartId(compositeId));
        softAssert.assertEquals(pagesInit.getCabInformersManageVideoCfg().getVideoCfgJson(),
                stringToJson(videoCfgData.generateInstreamCfgJson(  compositeId, true, true)), "FAIL -> videoCfg(json) check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("VIDEO::OutStream")
    @Owner(RKO)
    @Test(description = "VIDEO::OutStream")
    public void createOutStreamOverWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertNotNull(pagesInit.getWidgetClass().createVideoWidget(VideoFormat.OUTSTREAM, false, true),
                "FAIL -> Video Outstream 'Over' Desktop widget was not created!");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.OUTSTREAM, false, true), "FAIL -> checkVideoWidget");

        log.info("edit widget");
        pagesInit.getWidgetClass().editVideoWidget(VideoFormat.OUTSTREAM, true, true);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkVideoWidget(VideoFormat.OUTSTREAM, true, true), "FAIL -> checkVideoWidget after edit");

        int compositeId = pagesInit.getWidgetClass().getWidgetId();
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(
                        compositeId).stream().noneMatch(i -> i.equals("vr")),
                "FAIL -> VR placement type is present");
        authCabAndGo("wages/widgets/?c_id=" + compositeId);
        authCabAndGo("wages/informers-manage-video-cfg/id/" + pagesInit.getCabWidgetsList().getVideoPartId(compositeId));
        softAssert.assertEquals(pagesInit.getCabInformersManageVideoCfg().getVideoCfgJson(),
                stringToJson(videoCfgData.generateOutstreamCfgJson(  compositeId, true, true)), "FAIL -> videoCfg(json) check");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("VIDEO::Check video placements for Instream and Custom widget with manually changed styles" +
            "<a href='https://jira.mgid.com/browse/TA-24633'>TA-24633</a>")
    @Owner(AIA)
    @Test(description = "VIDEO::Check video placements for Instream and Custom widget with manually changed styles")
    public void checkVideoPlacementsForWidgetsWithManuallyChangedStyles() {
        log.info("Test is started");
        int widgetId = 2003;
        authCabAndGo("wages/informers-settings/id/" + widgetId);
        log.info("Enable 'Styles changed manually' flag");
        pagesInit.getCabEditCode().manuallyStyleSwitchOn(true);
        pagesInit.getCabEditCode().saveEditCodeInterface();
        log.info("Check video placement for 'Instream' widget");
        goToEditWidget(widgetId);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                        .setVideoPlacementItems(4)
                        .checkPlacementByVideo(),
                "FAIL -> Video placement for Instream video format widget!");
        log.info("Change video format to 'Custom'");
        authCabAndGo("/wages/informers-manage-video/id/" + widgetId);
        pagesInit.getCabInformersManageVideo().changeSkipTime();
        goToEditWidget(widgetId);
        authDashAndGo(CLIENTS_VIDEO_LOGIN, "publisher/edit-widget/id/" + widgetId);
        log.info("Check video placement for 'Custom' widget");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkPlacementByVideo(),
                "FAIL -> Video placement for 'Custom' video format widget!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
