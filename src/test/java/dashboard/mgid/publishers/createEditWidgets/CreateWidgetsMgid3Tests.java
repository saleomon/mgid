package dashboard.mgid.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;

/*
 * Author: RKO
 * 1. UNDER_ARTICLE:
 *      - UNDER_ARTICLE_RECTANGULAR
 *      - UNDER_ARTICLE_CARDS
 *      - UNDER_ARTICLE_PAIR_CARDS
 *      - UNDER_ARTICLE_LIGHT_CARDS
 *      - UNDER_ARTICLE_SQUARE
 *      - UNDER_ARTICLE_SQUARE_BLACK
 *      - UNDER_ARTICLE_MAIN
 */
public class CreateWidgetsMgid3Tests extends TestBase {

    public CreateWidgetsMgid3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_RECTANGULAR")
    @Test
    public void createUnderArticleRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());

        authDashAndGo("publisher/widgets/site/" + WEBSITE_MGID_WAGES_ID);
        softAssert.assertTrue(pagesInit.getWidgetClass().deleteWidget(), "FAIL -> delete widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS")
    @Test
    public void createUnderArticleCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS <a href=\"https://jira.mgid.com/browse/TA-51941\">TA-51941</a>")
    @Test
    public void createUnderArticlePairCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT_CARDS")
    @Test
    public void createUnderArticleLightCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE")
    @Test
    public void createUnderArticleSquareWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE_BLACK")
    @Test
    public void createUnderArticleBlackWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleBlackWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleBlackWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleBlackWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_MAIN")
    @Test
    public void createUnderArticleMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleMainWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleMainWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createUnderArticleMainWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

}
