package dashboard.mgid.publishers.createEditWidgets;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static testData.project.ClientsEntities.*;

public class CreateWidgetsMgidOtherTests extends TestBase {

    public CreateWidgetsMgidOtherTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget() {
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    /**
     * https://youtrack.dt00.net/issue/VT-22216
     * Проверка что значение задержки кликабельности не обнуляется после редактирования в дешборде
     * RKO
     */
    @Test
    public void checkActivateDelayValueAfterEditInDashboard() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().createSimpleWidget();
        pagesInit.getWidgetClass().reSaveWithEditWidget();
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        Assert.assertEquals(pagesInit.getCabCompositeSettings().getActiveDelayValue(), "2000");
        log.info("Test is finished");
    }

    /**
     * https://youtrack.dt00.net/issue/VT-21895
     * Ошибки при нажатии на кнопку создания нового виджета
     * Проверка, что при нажатии на кнопку создания нового виджета - открывается интерфейс конструктора без ошибок
     */
    @Test
    public void checkButtonAddAnotherWidget() {
        log.info("Test is started");
        goToCreateWidget();
        Assert.assertTrue(pagesInit.getWidgetClass().checkButtonAddAnotherWidget());
        log.info("Test is finished");
    }
}
