package dashboard.mgid.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;

/*
 * 1. SMART:
 *      - SMART_MAIN
 *      - SMART_BLUR
 *      - SMART_PLUS
 *
 * 2. HEADLINE
 *
 * 3. BANNER
 *      - BANNER_728x90_1
 *      - BANNER_300x250_4
 *      - BANNER_470x325
 *
 * 4. PASSAGE
 *
 */
public class CreateWidgetsMgid1Tests extends TestBase {

    public CreateWidgetsMgid1Tests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SMART, SubTypes.SMART_MAIN")
    @Test
    public void createSmartMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-main doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SMART, SubTypes.SMART_BLUR")
    @Test
    public void createSmartBlurWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-blur doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SMART, SubTypes.SMART_PLUS")
    @Test
    public void createSmartPlusWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-plus doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-plus doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: smart-plus doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.HEADLINE, SubTypes.NONE")
    @Test
    public void createHeadlineInPictureWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.BANNER, SubTypes.BANNER_728x90_1")
    @Test
    public void createBannerWidgetWithTheme() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_728x90_1);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.BANNER, SubTypes.BANNER_300x250_4\n" +
            "<ul>\n" +
            "<li>Возможность менять цвет кнопки для виджета формата 300x250_4 <a href=\"https://jira.mgid.com/browse/TA-23686\">TA-23686</a></li>\n" +
            "<li>Добавить опции в формат 300x250_blur_button <a href=\"https://jira.mgid.com/browse/TA-23856\">TA-23856</a></li>\n" +
            "<li>Опция \"BUTTON EFFECT\" для баннерного формата 300x250_blur_button <a href=\"https://jira.mgid.com/browse/TA-23998\">TA-23998</a></li>\n" +
            "</ul>")
    @Test
    public void createBannerWidgetWithButton() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.BANNER, SubTypes.BANNER_470x325")
    @Test
    public void createBannerBanner470x325() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check after CREATE");
        softAssert.assertEquals(operationMySql.getTickersComposite().getCtaRequired(pagesInit.getWidgetClass().getWidgetId()), "1", "FAIL: tickers_composite.cta_required !=1");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(subnetId), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.PASSAGE, SubTypes.NONE")
    @Test
    public void createPassageWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check before CREATE");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after CREATE");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL: doesn't check after EDIT");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
