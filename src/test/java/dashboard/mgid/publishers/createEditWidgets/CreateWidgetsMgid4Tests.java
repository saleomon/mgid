package dashboard.mgid.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;


/*
 * 1. SIDEBAR:
 *      - SIDEBAR_CARDS
 *      - SIDEBAR_RECTANGULAR_2
 *      - SIDEBAR_RECTANGULAR
 *      - SIDEBAR_SQUARE_SMALL
 *      - SIDEBAR_IMPACT
 *      - SIDEBAR_BLUR
 *      - SIDEBAR_FRAME
 *
 * 2. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 */
public class CreateWidgetsMgid4Tests extends TestBase {

    public CreateWidgetsMgid4Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_CARDS")
    @Test
    public void createSidebarCardsWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR_2")
    @Test
    public void createSidebarRectangular2Widget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR")
    @Test
    public void createSidebarRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_SQUARE_SMALL")
    @Test
    public void createSidebarSmallWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_IMPACT")
    @Test
    public void createSidebarImpactWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_BLUR")
    @Test
    public void createSidebarBlurWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_FRAME")
    @Test
    public void createSidebarFrameWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - default settings");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after create widget");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL - after edit widget");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN")
    @Test
    public void createInSiteNotificationWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
