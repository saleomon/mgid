package dashboard.mgid.publishers.createEditWidgets;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;
import java.util.Map;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.ClientsEntities.CLIENTS_VIDEO_LOGIN;
import static testData.project.EndPoints.widgetsCustomIdUrl;

public class NativeBackfillTests extends TestBase {

    private final int siteId = 130;

    public void goToCreateWidget() {
        authDashAndGo(CLIENTS_VIDEO_LOGIN, "publisher/add-widget/site/" + siteId);
    }

    @Epic("Video-widget")
    @Feature("Native Backfill")
    @Story("Native backfill childs in the dashboard")
    @Description("Create video widget without native backfill <a href='https://jira.mgid.com/browse/TA-52271'>TA-52271</a>")
    @Owner(value="RKO")
    @Test(description = "Create video widget without native backfill")
    public void createVideoWidgetWithoutNativeBackfill() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        Assert.assertNotNull(pagesInit.getWidgetClass()
                        .setNativeBackfill(false)
                        .createVideoWidget(VideoFormat.INSTREAM, true, false),
                "FAIL -> Widget doesn't create");
        int parentId = pagesInit.getWidgetClass().getWidgetId();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        authDashAndGo("publisher/widgets/site/" + siteId);
        softAssert.assertFalse(pagesInit.getWidgetClass().isCloneWidgetIconDisplayed(parentId), "FAIL -> show child icon is displayed");
        softAssert.assertEquals(operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId), 0, "FAIL -> get childId from dataBases");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Video-widget")
    @Feature("Native Backfill")
    @Story("Native backfill childs in the dashboard")
    @Description("Create video widget with native backfill <a href='https://jira.mgid.com/browse/TA-52271'>TA-52271</a>")
    @Owner(value="RKO")
    @Test(description = "Create video widget with native backfill")
    public void createVideoWidgetWithNativeBackfill() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        Assert.assertNotNull(pagesInit.getWidgetClass()
                        .setNativeBackfill(true)
                        .createVideoWidget(VideoFormat.OUTSTREAM, true, false),
                "FAIL -> Widget doesn't create");
        int parentId = pagesInit.getWidgetClass().getWidgetId();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        int childId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);

        authDashAndGo("publisher/widgets/site/" + siteId);
        softAssert.assertTrue(pagesInit.getWidgetClass().isCloneWidgetIconDisplayed(parentId), "FAIL -> show child icon is displayed");
        softAssert.assertNotEquals(childId, 0, "FAIL -> get childId from dataBases");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkShowAllChildStatusWidget(parentId,
                    Map.of(
                            String.valueOf(parentId),"status-active",
                            String.valueOf(childId), "status-active"
                    )
                ),
                "FAIL -> widgets status"
        );
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Video-widget")
    @Feature("Native Backfill")
    @Story("Native backfill childs in the dashboard")
    @Description("Block unblock video widget with native backfill <a href='https://jira.mgid.com/browse/TA-52271'>TA-52271</a>")
    @Owner(value="RKO")
    @Test(description = "Block unblock video widget with native backfill")
    public void blockUnblockNativeBackfillChild() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        Assert.assertNotNull(pagesInit.getWidgetClass()
                        .setNativeBackfill(true)
                        .createVideoWidget(VideoFormat.OUTSTREAM, true, false),
                "FAIL -> Widget doesn't create");
        int parentId = pagesInit.getWidgetClass().getWidgetId();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        authDashAndGo("publisher/widgets/site/" + siteId);
        pagesInit.getWidgetClass().showWidgetChild(parentId);

        log.info("delete(block) Native Backfill widget");
        int childId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);
        pagesInit.getWidgetClass().setCloneId(childId);
        pagesInit.getWidgetClass().deleteClonedWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkDeletedClone(pagesInit.getWidgetClass().getCloneId()), "FAIL - delete cloned widget");

        log.info("check block Native Backfill in cab");
        authCabAndGo(widgetsCustomIdUrl + childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "red", "FAIL -> 'block/unblock' icon: red");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline;", "FAIL -> block icon");

        log.info("click unblock Native Backfill in cab");
        pagesInit.getCabWidgetsList().clickBlockUnblockWidget(childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "green", "FAIL -> 'block/unblock' icon: green");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline;", "FAIL -> unblock icon");

        log.info("check unblock Native Backfill in dash");
        authDashAndGo("publisher/widgets/site/" + siteId);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkShowAllChildStatusWidget(parentId,
                        Map.of(
                                String.valueOf(parentId),"status-active",
                                String.valueOf(childId), "status-active"
                        )
                ),
                "FAIL -> widgets status"
        );

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Video-widget")
    @Feature("Native Backfill")
    @Story("Native backfill childs in the dashboard")
    @Description("turn off native backfill in edit interface <a href='https://jira.mgid.com/browse/TA-52271'>TA-52271</a>")
    @Owner(value="RKO")
    @Test(description = "turn off native backfill in edit interface")
    public void turnOffNativeBackfillInEditInterface() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        Assert.assertNotNull(pagesInit.getWidgetClass()
                        .setNativeBackfill(true)
                        .createVideoWidget(VideoFormat.INSTREAM, true, false),
                "FAIL -> Widget doesn't create");
        int parentId = pagesInit.getWidgetClass().getWidgetId();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("go to edit interface and turn off Native Backfill");
        authDashAndGo("publisher/edit-widget/id/" + parentId);
        pagesInit.getWidgetClass().switchNativeBackfillTumbler(false);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("check widgets statuses");
        int childId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);
        authDashAndGo("publisher/widgets/site/" + siteId);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkShowAllChildStatusWidget(parentId,
                        Map.of(
                                String.valueOf(parentId),"status-active",
                                String.valueOf(childId), "status-blocked"
                        )
                ),
                "FAIL -> widgets status"
        );
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
