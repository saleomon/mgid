package dashboard.mgid.publishers.createEditWidgets;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;

/*
 * 1. IN_ARTICLE:
 *      - IN_ARTICLE_MAIN
 *      - IN_ARTICLE_IMPACT
 *      - IN_ARTICLE_CAROUSEL
 *      - IN_ARTICLE_DOUBLE_PICTURE
 *
 * 2. HEADER:
 *      - HEADER_RECTANGULAR
 *      - HEADER_SQUARE
 *
 * 3. MOBILE
 */
public class CreateWidgetsMgid2Tests extends TestBase {

    public CreateWidgetsMgid2Tests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    public void goToCreateWidget(){
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_MAIN")
    @Test
    public void createInArticleMainWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_IMPACT")
    @Test
    public void createInArticleImpactWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void createInArticleCarouselWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_DOUBLE_PICTURE")
    @Test
    public void createInArticleDoublePictureWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "createInArticleImpactWidget -> fail check widget after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.HEADER, SubTypes.HEADER_RECTANGULAR")
    @Test
    public void createHeaderRectangularWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.HEADER, SubTypes.HEADER_SQUARE")
    @Test
    public void createHeaderSquareWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Create/Edit/Check widget in Dashboard")
    @Story("Create/Edit/Check widget MGID in Dashboard")
    @Description("Types.MOBILE, SubTypes.NONE")
    @Test
    public void createMobileWidget() {
        log.info("Test is started");
        goToCreateWidget();
        pagesInit.getWidgetClass().getDataAndSetTypeWidgets(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "fail check widget without change");
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        pagesInit.getWidgetClass().editWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidget(), "FAIL -> check widget after create");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
