package dashboard.mgid.publishers;

import io.qameta.allure.*;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextMgid;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.mgidLink;

public class RegistrationPublishersMgidTests extends TestBase {

    /**
     * Create client with role Publisher
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerPublisherMgid() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_PUBLISHER_LOGIN_MGID);
        log.info("Let's Register new publisher!");
        open(mgidLink + "/user/signup");
        pagesInit.getSignUp().registerPublisher(NEW_PUBLISHER_DOMAIN_MGID, NEW_PUBLISHER_LOGIN_MGID);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_PUBLISHER_LOGIN_MGID)).get(0).substring(mgidLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_PUBLISHER_LOGIN_MGID, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getWebsiteClass().setupProfileInPopup();
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkLoginOnPage(NEW_PUBLISHER_LOGIN_MGID),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertEquals(operationMySql.getClients().getAdDarknessCheckboxInNewClient(NEW_PUBLISHER_LOGIN_MGID), "0" ,
                "FAIL -> flag can_change_ad_darkness = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Registration")
    @Feature("Register legal entity client")
    @Story("Dashboard")
    @Description("Check register legal entity client - publisher" +
            "Except registration from RU and BY countries")
    @Owner("AIA")
    @Test(description = "Check register legal entity client - publisher")
    public void registerPublisherMgidLegalEntity() {
        log.info("Test is started");
        List<String> forbiddenCountries = new ArrayList<>(Arrays.asList("RU", "BY"));
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_PUBLISHER_LEGAL_ENTITY_MGID);
        log.info("Let's register new Advertiser!");
        open(mgidLink + "/user/signup");
        pagesInit.getSignUp().registerAdvertiser(NEW_PUBLISHER_LEGAL_ENTITY_MGID);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_PUBLISHER_LEGAL_ENTITY_MGID)).get(0).substring(mgidLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
        pagesInit.getSignUp().signInDash(NEW_PUBLISHER_LEGAL_ENTITY_MGID, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopupLegalEntity(forbiddenCountries);
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(NEW_PUBLISHER_LEGAL_ENTITY_MGID),
                "FAIL -> Company login on the page isn`t correct!");
        open(mgidLink + "/profile/users");
        softAssert.assertTrue(pagesInit.getCampaigns().checkProfileOfLegalEntity(NEW_PUBLISHER_LEGAL_ENTITY_MGID),
                "FAIL -> Check user profile!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
