package dashboard.mgid.publishers.widgetsOnStand.checkScreenshot;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_MGID_NAME;

/*
 * Author: RKO
 * Check widgets on stand with screenshot
 * 1. UNDER_ARTICLE
 *      - UNDER_ARTICLE_CARDS
 *      - UNDER_ARTICLE_PAIR_CARDS
 *      - UNDER_ARTICLE_MAIN
 *      - UNDER_ARTICLE_LIGHT_CARDS
 *      - UNDER_ARTICLE_RECTANGULAR
 *      - UNDER_ARTICLE_SQUARE
 *      - UNDER_ARTICLE_SQUARE_BLACK
 */
public class WidgetsOnStandMgidScreenshot1Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandMgidScreenshot1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = "testEmail49@ex.ua";
        subnetName = SUBNET_MGID_NAME;
    }


    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleCardsCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS,
                "under-article", "mgid-amp-under-article-cards",
                "standUnderArticleCardsCheckScreenshot", "standUnderArticleCardsCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_PAIR_CARDS <a href=\"https://jira.mgid.com/browse/TA-51941\">TA-51941</a>")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticlePairCardsCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS,
                "under-article", "mgid-amp-under-article-pair-cards",
                "standUnderArticlePairCardsCheckScreenshot", "standUnderArticlePairCardsCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @DataProvider
    public Object[][] dataShadowDom(){
        return new Object[][]{
                {"shadow_dom_enabled:0", 0},
                {"shadow_dom_enabled:1", 1}
        };
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_MAIN")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleMainCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN,
                "under-article", "mgid-amp-under-article-main",
                "standUnderArticleMainCheckScreenshot", "standUnderArticleMainCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT_CARDS")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleLightCardsCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS,
                "under-article", "mgid-amp-under-article-light-cards",
                "standUnderArticleLightCardsCheckScreenshot", "standUnderArticleLightCardsCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_RECTANGULAR")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleRectangularCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR,
                "under-article", "mgid-amp-under-article-rectangular",
                "standUnderArticleRectangularCheckScreenshot", "standUnderArticleRectangularCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleSquareCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE,
                "under-article", "mgid-amp-under-article-square",
                "standUnderArticleSquareCheckScreenshot", "standUnderArticleSquareCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE_BLACK")
    @Test(dataProvider = "dataShadowDom")
    public void standUnderArticleSquareBlackCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow, 204, WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK,
                "under-article", "mgid-amp-under-article-square-black",
                "standUnderArticleSquareBlackCheckScreenshot", "standUnderArticleSquareBlackCheckScreenshot");
        log.info("Test is finished " + val);
    }
}
