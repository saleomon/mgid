package dashboard.mgid.publishers.widgetsOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_MGID_NAME;

/*
 * Check widgets on stand with screenshot
 * 1. HEADER
 *      - HEADER_RECTANGULAR
 *      - HEADER_SQUARE
 *
 * 2. IN_ARTICLE
 *      - IN_ARTICLE_MAIN
 *      - IN_ARTICLE_IMPACT
 *      - IN_ARTICLE_CAROUSEL
 *      - IN_ARTICLE_DOUBLE_PICTURE
 *
 * 3. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 */
public class WidgetsOnStandMgidScreenshot4Tests extends ScreenshotWidgetStandCheckingTemplate {
    
    public WidgetsOnStandMgidScreenshot4Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = "testEmail49@ex.ua";
        subnetName = SUBNET_MGID_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_RECTANGULAR")
    @Test(dataProvider = "dataShadowDom")
    public void standHeaderRectangularCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,205, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR,
                "header", "mgid-amp-header-rectangular",
                "standHeaderRectangularCheckScreenshot", "standHeaderRectangularCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.HEADER, SubTypes.HEADER_SQUARE")
    @Test(dataProvider = "dataShadowDom")
    public void standHeaderSquareCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,205, WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE,
                "header", "mgid-amp-header-square",
                "standHeaderSquareCheckScreenshot", "standHeaderSquareCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_MAIN")
    @Test(dataProvider = "dataShadowDom")
    public void standInArticleMainCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,365, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN,
                "in-article-main", "mgid-amp-in-article-main",
                "standInArticleMainCheckScreenshot", "standInArticleMainCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_IMPACT")
    @Test(dataProvider = "dataShadowDom")
    public void standInArticleImpactCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,206, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT,
                "in-article", "mgid-amp-in-article-impact",
                "standInArticleImpactCheckScreenshot", "standInArticleImpactCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test(dataProvider = "dataShadowDom")
    public void standInArticleCarouselCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow,462, WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL,
                "in-article-carousel", "mgid-amp-in-article-carousel",
                "standInArticleCarouselCheckScreenshot", "standInArticleCarouselCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_DOUBLE_PICTURE")
    @Test
    public void standInArticleDoublePictureCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 206;
        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("in-article"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInArticleDoublePictureCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid-amp-in-article-double-picture"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().setDiffSizeTrigger(20).compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + "standInArticleDoublePictureCheckScreenshot.png")), "FAIL -> screen amp");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN")
    @Test
    public void standInSiteNotificationMainCheckScreenshot() {
        log.info("Test is started");
        int widgetId = 419;
        log.info("ShadowDomEnabled = 0");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        serviceInit.getServicerMock().countDownLatchAwait();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInSiteNotificationMainCheckScreenshot.png")), "FAIL -> screen simple");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);

        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        serviceInit.getServicerMock().countDownLatchAwait();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "standInSiteNotificationMainCheckScreenshot.png")), "FAIL -> screen simple(ShadowDomEnabled)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataShadowDom(){
        return new Object[][]{
                {"shadow_dom_enabled:0", 0},
                {"shadow_dom_enabled:1", 1}
        };
    }
}
