package dashboard.mgid.publishers.widgetsOnStand.checkScreenshot;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_MGID_NAME;


/*
 * Check widgets on stand with screenshot
 * 1. SIDEBAR
 *      - SIDEBAR_CARDS
 *      - SIDEBAR_RECTANGULAR_2
 *      - SIDEBAR_RECTANGULAR
 *      - SIDEBAR_SQUARE_SMALL
 *      - SIDEBAR_IMPACT
 *      - SIDEBAR_BLUR
 *      - SIDEBAR_FRAME
 */
public class WidgetsOnStandMgidScreenshot2Tests extends ScreenshotWidgetStandCheckingTemplate {

    public WidgetsOnStandMgidScreenshot2Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = "testEmail49@ex.ua";
        subnetName = SUBNET_MGID_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_CARDS")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarCardsCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS,
                "sidebar", "mgid-amp-sidebar-cards",
                "standSidebarCardsCheckScreenshot", "standSidebarCardsCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR_2")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarRectangular2CheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2,
                "sidebar", "mgid-amp-sidebar-rectangular-2",
                "standSidebarRectangular2CheckScreenshot", "standSidebarRectangular2CheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarRectangularCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR,
                "sidebar", "mgid-amp-sidebar-rectangular",
                "standSidebarRectangularCheckScreenshot", "standSidebarRectangularCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_SQUARE_SMALL")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarSquareSmallCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL,
                "sidebar", "mgid-amp-sidebar-square-small",
                "standSidebarSquareSmallCheckScreenshot", "standSidebarSquareSmallCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_IMPACT")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarImpactCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT,
                "sidebar", "mgid-amp-sidebar-impact",
                "standSidebarImpactCheckScreenshot", "standSidebarImpactCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_BLUR")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarBlurCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR,
                "sidebar", "mgid-amp-sidebar-blur",
                "standSidebarBlurCheckScreenshot", "standSidebarBlurCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_FRAME")
    @Test(dataProvider = "dataShadowDom")
    public void standSidebarFrameCheckScreenshot(String val, int shadow) {
        log.info("Test is started " + val);
        standHelperCheckScreenshot(shadow,207, WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME,
                "sidebar", "mgid-amp-sidebar-frame",
                "standSidebarFrameCheckScreenshot", "standSidebarFrameCheckScreenshot");
        log.info("Test is finished " + val);
    }

    @DataProvider
    public Object[][] dataShadowDom(){
        return new Object[][]{
                {"shadow_dom_enabled:0", 0},
                {"shadow_dom_enabled:1", 1}
        };
    }
}
