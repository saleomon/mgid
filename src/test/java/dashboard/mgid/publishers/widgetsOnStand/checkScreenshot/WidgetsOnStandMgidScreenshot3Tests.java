package dashboard.mgid.publishers.widgetsOnStand.checkScreenshot;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static testData.project.Subnets.SUBNET_MGID_NAME;

/*
 * Check widgets on stand with screenshot
 * 1. SMART
 *      - SMART_MAIN
 *      - SMART_BLUR
 *      - SMART_PLUS
 *
 * 2. BANNER
 *      - BANNER_300x250_4
 *      - BANNER_470x325
 *
 * 3. HEADLINE
 */
public class WidgetsOnStandMgidScreenshot3Tests extends ScreenshotWidgetStandCheckingTemplate {
    
    public WidgetsOnStandMgidScreenshot3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = "testEmail49@ex.ua";
        subnetName = SUBNET_MGID_NAME;
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SMART, SubTypes.SMART_MAIN")
    @Test(dataProvider = "dataShadowDom")
    public void standSmartMainCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 208, WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN,
                "smart", "mgid-amp-smart-main",
                "standSmartMainCheckScreenshot", "standSmartMainCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SMART, SubTypes.SMART_BLUR")
    @Test(dataProvider = "dataShadowDom")
    public void standSmartBlurCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 536, WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR,
                "smart-blur", "mgid-amp-smart-blur",
                "standSmartBlurCheckScreenshot", "standSmartBlurCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.SMART, SubTypes.SMART_PLUS")
    //todo RKO @Test(dataProvider = "dataShadowDom")
    public void standSmartPlusCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 208, WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS,
                "smart", "mgid-amp-smart-plus",
                "standSmartPlusCheckScreenshot", "standSmartPlusCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.BANNER, SubTypes.BANNER_300x250_4")
    @Test(dataProvider = "dataShadowDom")
    public void standBanner300x250_4CheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 461, WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4,
                "banner-300-250-4", "mgid-amp-banner-300-250-4",
                "standBanner300x250_4CheckScreenshot", "standBanner300x250_4CheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.BANNER, SubTypes.BANNER_470x325")
    @Test(dataProvider = "dataShadowDom")
    public void standBanner470x325CheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 209, WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325,
                "banner", "mgid-amp-banner-470-325",
                "standBanner470x325CheckScreenshot", "standBanner470x325CheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Story("Widget Stand(simple/amp code) MGID -> check screenshot")
    @Description("Types.HEADLINE, SubTypes.NONE")
    @Test(dataProvider = "dataShadowDom")
    public void standHeadlineCheckScreenshot(String val, int shadow) {
        log.info("Test is started: " + val);
        standHelperCheckScreenshot(shadow, 210, WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE,
                "headline", "mgid-amp-headline",
                "standHeadlineCheckScreenshot", "standHeadlineCheckScreenshot");
        log.info("Test is finished: " + val);
    }

    @DataProvider
    public Object[][] dataShadowDom(){
        return new Object[][]{
                {"shadow_dom_enabled:0", 0},
                {"shadow_dom_enabled:1", 1}
        };
    }
}
