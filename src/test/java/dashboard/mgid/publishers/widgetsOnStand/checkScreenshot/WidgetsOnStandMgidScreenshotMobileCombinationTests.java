package dashboard.mgid.publishers.widgetsOnStand.checkScreenshot;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.dash.publisher.logic.Widget;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.Subnets.SUBNET_MGID_NAME;

public class WidgetsOnStandMgidScreenshotMobileCombinationTests extends ScreenshotWidgetStandCheckingTemplate {

    private final int mobileToasterId = 521;
    private final int inSiteNotificationId = 523;
    private final int smartId = 524;

    public WidgetsOnStandMgidScreenshotMobileCombinationTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
        clientLogin = "testEmail49@ex.ua";
        subnetName = SUBNET_MGID_NAME;
    }

    @BeforeClass
    public void createDataWidgets(){
        int passageId = 522;
        goToCreateWidget(passageId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().saveWidgetSettings();

        goToCreateWidget(mobileToasterId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .chooseMobileWidgetType(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        goToCreateWidget(inSiteNotificationId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        goToCreateWidget(smartId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        int mobileInterstitialId = 526;
        goToCreateWidget(mobileInterstitialId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setShowAfterInteraction("2")
                .chooseMobileWidgetType(Widget.MobileWidgetType.INTERSTITIAL);
        pagesInit.getWidgetClass().saveWidgetSettings();
    }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setShadowDom(true);
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Passage and Toaster")
    @Owner("RKO")
    @Test(description = "Check work combination Passage and Toaster")
    public void checkWorkCombinationPassageAndMobileToaster() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("passage-toaster"))
                .setWidgetIds(mobileToasterId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(9000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationPassageAndMobileToaster.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination InSiteNotification and Toaster")
    @Owner("RKO")
    @Test(description = "Check work combination InSiteNotification and Toaster")
    public void checkWorkCombinationInSiteNotificationAndMobileToaster() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification-toaster"))
                .setWidgetIds(inSiteNotificationId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        sleep(9000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationInSiteNotificationAndMobileToaster.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination InSiteNotification and Passage")
    @Owner("RKO")
    @Test(description = "Check work combination InSiteNotification and Passage")
    public void checkWorkCombinationInSiteNotificationAndPassage() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification-passage"))
                .setWidgetIds(inSiteNotificationId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationInSiteNotificationAndPassage.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination InSiteNotification and Toaster")
    @Owner("RKO")
    @Test(description = "Check work combination InSiteNotification and Toaster")
    public void checkWorkCombinationSmartAndPassage() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("smart-passage"))
                .setWidgetIds(smartId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationSmartAndPassage.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination InSiteNotification and Interstitial")
    @Owner("RKO")
    @Test(description = "Check work combination InSiteNotification and Interstitial")
    public void checkWorkCombinationInSiteNotificationAndMobileInterstitial() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("in-site-notification-interstitial"))
                .setWidgetIds(inSiteNotificationId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationInSiteNotificationAndMobileInterstitial.png")));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check screenshot")
    @Stories(value = {@Story(value = "Widget Stand(simple/amp code) MGID -> check screenshot"), @Story(value = "Mobile widget")})
    @Description("Check work combination Smart and Interstitial")
    @Owner("RKO")
    @Test(description = "Check work combination Smart and Interstitial")
    public void checkWorkCombinationSmartAndMobileInterstitial() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("smart-interstitial"))
                .setWidgetIds(inSiteNotificationId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + "checkWorkCombinationSmartAndMobileInterstitial.png")));
        log.info("Test is finished");
    }
}
