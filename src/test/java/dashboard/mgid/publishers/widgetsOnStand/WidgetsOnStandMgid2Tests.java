package dashboard.mgid.publishers.widgetsOnStand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.mgidLogo;

/*
 * 1. in-article
 *      - in-article-main
 *      - in-article-impact
 *      - in-article-carousel
 *      - in-article-double-picture
 *
 * 2. sidebar-widget
 *      - sidebar-widget-cards
 *      - sidebar-widget-rectangular-2
 *      - sidebar-widget-rectangular
 *      - sidebar-widget-square-small
 *      - sidebar-widget-square-impact
 *      - sidebar-widget-square-blur
 *
 */
public class WidgetsOnStandMgid2Tests extends TestBase {


    public WidgetsOnStandMgid2Tests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    /*
     * Тесты проверяют корректную работу виджетов на стенде
     * 1. пересохраняем виджет
     * 2. Получаем сохранённые стили и параметры
     * 3. Открываем стенд с нужным виджетом и проверяем что полученные стили и параметры верно отображаются
     * RKO
     */

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_MAIN")
    @Test
    public void editAndSaveWidgetForStandInArticleMain() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_IN_ARTICLE_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("mgid_in_article_with_detailed"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "FAIL -> check base settings");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_in_article_with_detailed"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(3);
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.mgid.com")), "FAIL -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .setAutoplacement("amp")
                .checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "FAIL AMP -> check base settings");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_IMPACT")
    @Test
    public void editAndSaveWidgetForStandInArticleImpact() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_IN_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        serviceInit.getServicerMock().setStandName(setStand("mgid_in_article"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkAutoplacementOnStand(), "FAIL -> checkAutoplacementOnStand");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "600", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-size"), "15px", "FAIL: DOMAIN -> font-size");

        log.info("check TITLE '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "600", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "20px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("overflow"), "visible", "FAIL: TITLE -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("text-overflow"), "clip", "FAIL: TITLE -> text-overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("display"), "contents", "FAIL: TITLE -> display");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("max-height"), "52px", "FAIL: TITLE -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-line-clamp"), "none", "FAIL: TITLE -> webkit-line-clamp");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-box-orient"), "horizontal", "FAIL: TITLE -> webkit-box-orient");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE -> font-color");

        log.info("check DESCRIPTION '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("overflow"), "hidden", "FAIL: DESCRIPTION -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("font-size"), "17px", "FAIL: DESCRIPTION -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("max-height"), "38px", "FAIL: DESCRIPTION -> max-height");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL: MglBtnStyle -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "LEARN MORE", "FAIL -> MglBtnStyle: text");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_in_article"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();
        softAssert.assertTrue(pagesInit.getWidgetClass().setAutoplacement("amp").checkAutoplacementOnStand(), "FAIL AMP -> checkAutoplacementOnStand");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "600", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-size"), "15px", "FAIL AMP: DOMAIN -> font-size");

        log.info("check TITLE '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "600", "FAIL AMP: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "20px", "FAIL AMP: TITLE -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("overflow"), "visible", "FAIL AMP: TITLE -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("text-overflow"), "clip", "FAIL AMP: TITLE -> text-overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("display"), "contents", "FAIL AMP: TITLE -> display");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("max-height"), "52px", "FAIL AMP: TITLE -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-line-clamp"), "none", "FAIL AMP: TITLE -> webkit-line-clamp");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("webkit-box-orient"), "horizontal", "FAIL AMP: TITLE -> webkit-box-orient");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL AMP: TITLE -> font-color");

        log.info("check DESCRIPTION '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("overflow"), "hidden", "FAIL AMP: DESCRIPTION -> overflow");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("font-size"), "17px", "FAIL AMP: DESCRIPTION -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionStyle("max-height"), "38px", "FAIL AMP: DESCRIPTION -> max-height");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL AMP: MglBtnStyle -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "LEARN MORE", "FAIL AMP -> MglBtnStyle: text");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_CAROUSEL")
    @Test
    public void editAndSaveWidgetForStandInArticleCarouselSuper() {
        int widgetId = 246;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL);
        serviceInit.getServicerMock().setStandName(setStand("mgid_in_article_carousel_super"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_in_article_carousel_super"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .setAutoplacement("amp")
                .checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.IN_ARTICLE, SubTypes.IN_ARTICLE_DOUBLE_PICTURE")
    @Test
    public void editAndSaveWidgetForStandInArticleDoublePicture() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_IN_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_DOUBLE_PICTURE);
        serviceInit.getServicerMock().setStandName(setStand("mgid_in_article"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkAutoplacementOnStand(), "FAIL -> checkAutoplacementOnStand");

        log.info(".mg-button>svg");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton_Svg("fill")), "#fff", "FAIL: .mg-button>svg -> font-color");

        log.info("mg-button");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton("background-color")), "#0055a6", "FAIL: .mg-button -> background-color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("font-size"), "25px", "FAIL: .mg-button -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("position"), "absolute", "FAIL: .mg-button -> position");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info(".mcimg-inner");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("width"), "75px", "FAIL: .mcimg-inner -> width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("height"), "75px", "FAIL: .mcimg-inner -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("float"), "left", "FAIL: .mcimg-inner -> float");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "250px", "FAIL: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL: .mcimg -> object-fit");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_in_article"))
                .setWidgetIds(MGID_WIDGET_IN_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();
        softAssert.assertTrue(pagesInit.getWidgetClass().setAutoplacement("amp").checkAutoplacementOnStand(), "FAIL AMP -> checkAutoplacementOnStand");

        log.info(".mg-button>svg");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton_Svg("fill")), "#fff", "FAIL AMP: .mg-button>svg -> font-color");

        log.info("mg-button");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDoublePicture_mgButton("background-color")), "#0055a6", "FAIL AMP: .mg-button -> background-color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("font-size"), "25px", "FAIL AMP: .mg-button -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mgButton("position"), "absolute", "FAIL AMP: .mg-button -> position");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL AMP: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info(".mcimg-inner");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("width"), "75px", "FAIL AMP: .mcimg-inner -> width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("height"), "75px", "FAIL AMP: .mcimg-inner -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDoublePicture_mcimgInner("float"), "left", "FAIL AMP: .mcimg-inner -> float");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "250px", "FAIL AMP: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL AMP: .mcimg -> object-fit");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_CARDS")
    @Test
    public void editAndSaveWidgetForStandSidebarCards() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_CARDS), "fail AMP -> widget don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR_2")
    @Test
    public void editAndSaveWidgetForStandSidebarRectangular_2() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR_2), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_RECTANGULAR")
    @Test
    public void editAndSaveWidgetForStandSidebarRectangular() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_RECTANGULAR), "fail AMP -> widget don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_SQUARE_SMALL")
    @Test
    public void editAndSaveWidgetForStandSidebarSmall() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL), "fail -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar_with_detailed"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_SQUARE_SMALL), "fail AMP -> widget don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_IMPACT")
    @Test
    public void editAndSaveWidgetForStandSidebarImpact() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "270px", "FAIL: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL: .mcimg -> object-fit");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#000", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-size"), "14px", "FAIL: DOMAIN -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainTopStyle("color")), "#2a71b6", "FAIL: DOMAIN -> color");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("background")), "#2a71b6", "FAIL: MglBtnStyle -> background");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "LEARN MORE", "FAIL -> MglBtnStyle: text");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_IMPACT), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "270px", "FAIL AMP: .mcimg -> height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("object-fit"), "cover", "FAIL AMP: .mcimg -> object-fit");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "18px", "FAIL AMP: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#000", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN 'mcdomain-top mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-family"), "Arial, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-weight"), "700", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainTopStyle("font-size"), "14px", "FAIL AMP: DOMAIN -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainTopStyle("color")), "#2a71b6", "FAIL AMP: DOMAIN -> color");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "Arial, sans-serif", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "500", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "15px", "FAIL AMP: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("background")), "#2a71b6", "FAIL: MglBtnStyle -> background");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "LEARN MORE", "FAIL AMP -> MglBtnStyle: text");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_BLUR")
    @Test
    public void editAndSaveWidgetForStandSidebarBlur() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "270px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkBlurEffectOnMouseHover(), "FAIL: blur effect on mouse hover");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_BLUR), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "270px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL AMP: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL AMP: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL AMP: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkBlurEffectOnMouseHover(), "FAIL AMP: blur effect on mouse hover");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_FRAME")
    @Test
    public void editAndSaveWidgetForStandSidebarFrame() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SIDEBAR_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME);
        serviceInit.getServicerMock().setStandName(setStand("mgid_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME), "FAIL -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "248.625px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSidebarFrameHover(), "FAIL: blur effect on mouse hover");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_sidebar"))
                .setWidgetIds(MGID_WIDGET_SIDEBAR_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SIDEBAR, WidgetTypes.SubTypes.SIDEBAR_FRAME), "FAIL AMP -> base settings");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "492px", "FAIL AMP: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "277px", "FAIL AMP: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "248.625px", "FAIL: .mcimg -> height");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL AMP: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL AMP: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-family"), "Verdana, sans-serif", "FAIL AMP: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainStyle("font-weight"), "400", "FAIL AMP: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainStyle("color")), "#fff", "FAIL AMP: DOMAIN -> color");

        log.info("check blur effect on hover mouse");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSidebarFrameHover(), "FAIL AMP: blur effect on mouse hover");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
