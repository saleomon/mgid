package dashboard.mgid.publishers.widgetsOnStand;

import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.Arrays;
import java.util.List;

import static testData.project.EndPoints.widgetTemplateUrl;

public class CustomBannerInWidgetTests extends TestBase {
    public CustomBannerInWidgetTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public String setStand(String stand) {
        return String.format(widgetTemplateUrl, stand);
    }

    private final List<String> requestParametersToServicerForCustomBanner = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион\",\"Надоело жить в нищете? Хотите получить премию? Узнай как заработать больше и как себе ни в чём не отказывать\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser Altz rsd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser Altz ltg\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser Altz mqa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser Altz dtz\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    /**
     * Check custom bunner in widget
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51722">Ticket TA-51722</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkCustomBannerInSimpleWidget() {
        log.info("Test is started");
        int widgetId = 2007;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_with_custom_banner"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(requestParametersToServicerForCustomBanner)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass()
                        .switchToBannerFrame(widgetId, 1, 3)
                        .isDisplayedCustomBanner(),
                "FAIL -> custom banner");

        log.info("Test is finished");
    }
}
