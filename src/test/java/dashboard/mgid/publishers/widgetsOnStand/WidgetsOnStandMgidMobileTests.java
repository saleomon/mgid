package dashboard.mgid.publishers.widgetsOnStand;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.dash.publisher.logic.Widget;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.*;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.mgidLogo;

public class WidgetsOnStandMgidMobileTests extends TestBase {

    public WidgetsOnStandMgidMobileTests() {
        //mobileEmulationSettings.setMobileDevice(true);
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "mobile-widgets/" + Subnets.SUBNET_MGID_NAME + "/" + stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass()
                .setShadowDom(true)
                .setSubnetId(subnetId)
                .getPathToWidgetsJsonData(widgetDataMgid);

        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Toaster after scroll <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget after scroll")
    public void toasterWidgetWithScroll() {
        int widgetId = 275;
        log.info("Test is started");
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("toaster"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().waitShowToasterWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Toaster after 8s inactivity <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget after 8s inactivity")
    public void toasterWidgetShowAfter8sInactivity() {
        log.info("Test is started");
        int widgetId = 500;
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("toaster-with-inactivity"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        sleep(1000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Toaster -> show widget + refresh page + show -> without Close icon <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster Widget refresh without Close icon")
    public void toasterWidgetRefreshWithoutIconClose() {
        int widgetId = 501;
        log.info("Test is started");
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("step#1: wait 8s and widget should be visible");
        serviceInit.getServicerMock().setStandName(setStand("toaster-refresh-without-close"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("step#2: refresh page and wait 8s and widget should be visible");
        refresh();
        sleep(9000);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        log.info("step#3: refresh page and wait 8s and widget should NOT be visible (FrequencyCappingImpressions=2)");
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Toaster -> show widget + Close icon + refresh page -> widget show after capping 1 min <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work on stand Toaster with capping after click icon Close")
    public void toasterWidgetRefreshWithIconClose() {
        int widgetId = 502;
        log.info("Test is started");
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setFrequencyCappingImpressions("2")
                .setFrequencyCappingMinutes("1")
                .reSaveMobileWidget(Widget.MobileWidgetType.TOASTER);
        pagesInit.getWidgetClass().saveWidgetSettings();

        log.info("step#1: wait 8s and widget should be visible");
        serviceInit.getServicerMock().setStandName(setStand("toaster-refresh-with-close"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(8);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("step#2: click 'close' icon " +
                "refresh page " +
                "wait 8s and widget should NOT be visible " +
                "wait 60s and widget should NOT visible " +
                "scroll widget down-up and widget visible");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        sleep(30000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        sleep(30000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");

        log.info("step#3: click 'close' icon " +
                "refresh page " +
                "wait 8s and widget should NOT be visible " +
                "wait 60s and widget should NOT be visible " +
                "scroll widget down-up and widget NOT be visible");
        pagesInit.getWidgetClass().clickMobileCloseIcon();
        refresh();
        sleep(9000);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");
        sleep(60000);
        pagesInit.getWidgetClass().scrollActionForToasterWidget();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #5");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) ADSKEEPER -> check work js and widget css")
    @Story("Mobile widget")
    @Description("DragDown widget -> check work " +
            "widget should not be show in the first 5s" +
            "widget should show after 5s and scroll down and after scroll up")
    @Owner("RKO")
    @Test(description = "check work dragDown widget")
    public void dragDownWidget() {
        log.info("Test is started");
        int widgetId = 276;
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .reSaveMobileWidget(Widget.MobileWidgetType.DRAGDOWN);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("dragDown"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        log.info("check -> widget doesn't show after 1s load");
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        log.info("check -> widget show after 5s loaded");
        sleep(2000);
        pagesInit.getWidgetClass().waitShowDragdownWidget();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        log.info("check other settings");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#bbb", "FAIL: DOMAIN -> color");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Interstitial after 2 clicks links on the site and check work button 'Continue' <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work Interstitial after 2 clicks links on the site and check work button 'Continue'")
    public void interstitialWidget() {
        log.info("Test is started");
        int interstitialId = 277;
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + interstitialId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setShowAfterInteraction("2")
                .reSaveMobileWidget(Widget.MobileWidgetType.INTERSTITIAL);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("interstitial"))
                .setWidgetIds(interstitialId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        softAssert.assertTrue(pagesInit.getWidgetClass().getAdMobileContainerAttribute("class").contains("interstitial"), "FAIL -> AdWidgetContainer");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Verdana, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#bbb", "FAIL: DOMAIN -> color");

        log.info("adwidget-continue-cation");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContinueButton());
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonText(), "Continue");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMobileContinueButtonStyle("background")), "#27387a");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getMobileContinueButtonStyle("color")), "#fff");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("height"), "60px");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("font-size"), "28px");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMobileContinueButtonStyle("font-family"), "Verdana, sans-serif");

        log.info("check work button 'Continue' click it ");
        pagesInit.getWidgetClass().clickMobileContinueButton();
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("check work Interstitial after custom (4) clicks links on the site <a href='https://confluence.mgid.com/pages/viewpage.action?pageId=771342'>Doc</a>")
    @Owner("RKO")
    @Test(description = "check work Interstitial custom (4) clicks links on the site")
    public void interstitialWidgetWithCustomShowAfterInteraction() {
        log.info("Test is started");
        int interstitialWithCustomShowId = 499;
        authDashAndGo("testEmail59@ex.ua","publisher/edit-widget/id/" + interstitialWithCustomShowId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.MOBILE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setShowAfterInteraction("4")
                .reSaveMobileWidget(Widget.MobileWidgetType.INTERSTITIAL);
        pagesInit.getWidgetClass().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("interstitial-for-custom-show"))
                .setWidgetIds(interstitialWithCustomShowId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #1");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #2");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(3);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #3");

        pagesInit.getWidgetClass().clickCustomLinkInterstitial(4);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowMobileContainer(), "FAIL -> AdWidgetContainer #4");

        softAssert.assertTrue(pagesInit.getWidgetClass().getAdMobileContainerAttribute("class").contains("interstitial"), "FAIL -> AdWidgetContainer");
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowMobileCloseIcon(), "FAIL -> adwidget-close-action");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Mobile Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("Types.PASSAGE, SubTypes.NONE -> check styles <a href='https://jira.mgid.com/browse/TA-52004'>TA-52004</a>")
    @Owner(value="RKO")
    @Test(description = "Types.PASSAGE, SubTypes.NONE -> check styles")
    public void passageWidget() {
        log.info("Test is started");
        int widgetId = 472;
        authDashAndGo("testEmail49@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE)
                .setImpressionEveryPassage(1)
                .changeDataAndSaveWidget(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);

        serviceInit.getServicerMock().setStandName(setStand("passage"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetTitleInConstructor(), "FAIL -> checkWidgetTitleInConstructor");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "12px", "FAIL: TITLE(.mctitle>a) -> font-size");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#3385ff", "FAIL: DOMAIN -> color");

        log.info("check button '.mglbtn'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "400", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "16px", "FAIL: MglBtnStyle -> font-size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Mobile Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("Types.PASSAGE, SubTypes.NONE -> check impressions ever <a href='https://jira.mgid.com/browse/TA-52004'>TA-52004</a>")
    @Owner(value="RKO")
    @Test(description = "Types.PASSAGE, SubTypes.NONE -> check impressions ever")
    public void passageWidgetCheckImpressions() {
        log.info("Test is started");
        int widgetId = 476;
        authDashAndGo("testEmail49@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE)
                .setImpressionEveryPassage(1)
                .changeDataAndSaveWidget(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);

        serviceInit.getServicerMock().setStandName(setStand("passage-2"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> s#0");

        pagesInit.getWidgetClass().clickCloseIconPassage();

        pagesInit.getWidgetClass().activatePageOnStand();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> s#1");

        pagesInit.getWidgetClass().clickNav2OnStand();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> s#2");

        sleep(65000);
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(7000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(15);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> s#3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Mobile Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Mobile widget")
    @Description("Types.PASSAGE, SubTypes.NONE -> widget don't show after click in link with target='_blank' <a href='https://jira.mgid.com/browse/TA-52004'>TA-52004</a>")
    @Owner(value="RKO")
    @Test(description = "Types.PASSAGE, SubTypes.NONE -> widget don't show after click in link with target='_blank'")
    public void passageWidgetCheckShowWidgetWithTargetBlank() {
        log.info("Test is started");
        int widgetId = 477;
        authDashAndGo("testEmail49@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE)
                .setImpressionEveryPassage(1)
                .changeDataAndSaveWidget(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);

        serviceInit.getServicerMock().setStandName(setStand("passage-in"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(5);
        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0);

        log.info("Test is finished");
    }
}
