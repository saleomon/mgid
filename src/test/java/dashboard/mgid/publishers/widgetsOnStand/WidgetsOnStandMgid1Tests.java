package dashboard.mgid.publishers.widgetsOnStand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.ClientsEntities.MGID_WIDGET_HEADER_ID;
import static testData.project.ClientsEntities.MGID_WIDGET_UNDER_ARTICLE_ID;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;


/*
 * Author: RKO
 * 1. UNDER_ARTICLE:
 *      - UNDER_ARTICLE_CARDS
 *      - UNDER_ARTICLE_PAIR_CARDS
 *      - UNDER_ARTICLE_LIGHT_CARDS
 *      - UNDER_ARTICLE_RECTANGULAR
 *      - UNDER_ARTICLE_SQUARE
 *      - UNDER_ARTICLE_SQUARE_BLACK
 *      - UNDER_ARTICLE_MAIN
 *
 * 2. HEADER
 *      - HEADER_RECTANGULAR
 *      - HEADER_SQUARE
 *
 */
public class WidgetsOnStandMgid1Tests extends TestBase {

    public WidgetsOnStandMgid1Tests() {
        subnetId = SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    /*
     * Тесты проверяют корректную работу виджетов на стенде
     * 1. пересохраняем виджет
     * 2. Получаем сохранённые стили и параметры
     * 3. Открываем стенд с нужным виджетом и проверяем что полученные стили и параметры верно отображаются
     * RKO
     */

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS")
    @Test
    public void editAndSaveWidgetForStandUnderArticleCards() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(3);
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.mgid.com")), "FAIL -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "FAIL -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_PAIR_CARDS <a href=\"https://jira.mgid.com/browse/TA-51941\">TA-51941</a>")
    @Test
    public void editAndSaveWidgetForStandUnderArticlePairCards() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS);

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(3);
        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("https://servicer.mgid.com")), "FAIL -> don't load servicer request");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS), "FAIL -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT_CARDS")
    @Test
    public void editAndSaveWidgetForStandUnderArticleLightCards() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS);

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_RECTANGULAR")
    @Test
    public void editAndSaveWidgetForStandUnderArticleRectangular() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "FAIL -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE")
    @Test
    public void editAndSaveWidgetForStandUnderArticleSquare() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE_BLACK")
    @Test
    public void editAndSaveWidgetForStandUnderArticleBlack() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK);
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_MAIN")
    @Test
    public void editAndSaveWidgetForStandUnderArticleMain() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_UNDER_ARTICLE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN);
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_MAIN), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.HEADER, SubTypes.HEADER_RECTANGULAR")
    @Test
    public void editAndSaveWidgetForStandHeaderRectangular() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_HEADER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_header"))
                .setWidgetIds(MGID_WIDGET_HEADER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_header"))
                .setWidgetIds(MGID_WIDGET_HEADER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.HEADER, SubTypes.HEADER_SQUARE")
    @Test
    public void editAndSaveWidgetForStandHeaderSquare() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_HEADER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_header"))
                .setWidgetIds(MGID_WIDGET_HEADER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "FAIL -> widget don't check");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_header"))
                .setWidgetIds(MGID_WIDGET_HEADER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
