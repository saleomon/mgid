package dashboard.mgid.publishers.widgetsOnStand;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.mgidLogo;

/*
 * 1. smart
 *      - smart-main
 *      - smart-blur
 *      - smart-plus
 *
 * 2. banner
 *      - all
 *      - banner-300x250_4
 *      - banner-470x325
 *
 * 3. headline-in-picture
 *
 * 4. IN_SITE_NOTIFICATION
 *      - IN_SITE_NOTIFICATION_MAIN
 *
 * 5. PASSAGE
 */
public class WidgetsOnStandMgid3Tests extends TestBase {

    public WidgetsOnStandMgid3Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_BLUR/SMART_MAIN/SMART_PLUS\n" +
                "infinite-scroll = true <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(dataProvider = "smartType", description = "Types.SMART, SubTypes.SMART_BLUR/SMART_MAIN/SMART_PLUS,infinite-scroll=true")
    public void smartOnStandInfiniteScrollTrue(WidgetTypes.SubTypes type, String placement, String width, String height, String ident_p) {
        log.info("Test is started: " + type);
        goToCreateWidget(MGID_WIDGET_SMART_ID);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, type)
                .setInfiniteScroll(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, type)
                        .saveWidgetSettings();

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwaitWithoutTearDown(35);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 45), "FAIL -> infinite scroll");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_" + placement), width, "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_" + placement), height, "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), ident_p, "FAIL -> ident_p");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart"))
                .setServicerCountDown(3)
                .navigate()
                .scrollToSlow()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 30), "FAIL AMP -> infinite scroll");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_" + placement), width, "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_" + placement), height, "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), ident_p, "FAIL -> ident_p");

        softAssert.assertAll();
        log.info("Test is finished: " + type);
    }

    @DataProvider
    public Object[][] smartType(){
        return new Object[][]{
                {WidgetTypes.SubTypes.SMART_MAIN, "7", "470", "333", "true"},
                {WidgetTypes.SubTypes.SMART_BLUR, "6", "337", "280", "true"},
                {WidgetTypes.SubTypes.SMART_PLUS, "7", "337", "280", "true"}
        };
    }


    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_MAIN\n" +
            "infinite-scroll = false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_MAIN,infinite-scroll=false")
    public void smartMainOnStandInfiniteScrollFalse() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SMART_ID);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                        .saveWidgetSettings();

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "171"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "264.516"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "172.766"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "264.516"), "FAIL: .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: DOMAIN -> color");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_7"), "470", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_7"), "333", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> ident_p");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .scrollToTopWidgetSide()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "171"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "264.516"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "172.766"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "264.516"), "FAIL: .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: AMP DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: AMP DOMAIN -> color");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_7"), "470", "FAIL -> AMP maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_7"), "333", "FAIL -> AMP maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_BLUR\n" +
            "infinite-scroll = false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_BLUR,infinite-scroll=false")
    public void smartBlurOnStandInfiniteScrollFalse() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SMART_ID);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                        .saveWidgetSettings();

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "248.625"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "200"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "248.625"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "209.906", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL -> blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL -> blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL -> blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> blur row4");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_6"), "337", "FAIL -> maxw_6");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_6"), "280", "FAIL -> maxh_6");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> ident_p");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .scrollToTopWidgetSide()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "248.625"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "200"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "248.625"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "209.906", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: AMP TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN AMP -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: AMP DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL ->AMP blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL ->AMP blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL ->AMP blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> AMP blur row4");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_6"), "337", "FAIL -> AMP maxw_6");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_6"), "280", "FAIL -> AMP maxh_6");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> AMP ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_PLUS\n" +
            "infinite-scroll = false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_PLUS,infinite-scroll=false")
    public void smartPlusOnStandInfiniteScrollFalse() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_SMART_ID);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS)
                        .saveWidgetSettings();

        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "680", "382.5"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "189.328"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "680", "382.5"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "336.594", "189.328", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "336.594", "189.328", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#00f4b81", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: DOMAIN -> color");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_7"), "337", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_7"), "280", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> ident_p");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart"))
                .setWidgetIds(MGID_WIDGET_SMART_ID)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .scrollToTopWidgetSide()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "680", "382.5"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "189.328"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "680", "382.5"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "336.594", "189.328", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "336.594", "189.328", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#00f4b81", "FAIL: AMP TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial", "FAIL: DOMAIN AMP -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: AMP DOMAIN -> color");

        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxw_7"), "337", "FAIL -> AMP maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "maxh_7"), "280", "FAIL -> AMP maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_SMART_ID, "ident_p"), "true", "FAIL -> AMP ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.BANNER, SubTypes.NONE(random)")
    @Test
    public void editAndSaveWidgetForStandBanner() {
        log.info("Test is started");
        int widgetId = 459;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("mgid_banner_2"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE), "FAIL -> check base settings");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_banner_2"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.NONE), "FAIL AMP -> check base settings");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.BANNER, SubTypes.BANNER_300x250_4\n" +
            "<ul>\n" +
            "<li>Возможность менять цвет кнопки для виджета формата 300x250_4 <a href=\"https://jira.mgid.com/browse/TA-23686\">TA-23686</a></li>\n" +
            "<li>Добавить опции в формат 300x250_blur_button <a href=\"https://jira.mgid.com/browse/TA-23856\">TA-23856</a></li>\n" +
            "<li>Опция \"BUTTON EFFECT\" для баннерного формата 300x250_blur_button <a href=\"https://jira.mgid.com/browse/TA-23998\">TA-23998</a></li>\n" +
            "<li>Banner <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a></li>\n" +
            "</ul>")
    @Owner(value = "Kostyantyn Ratushnyak")
    @Flaky
    @Test
    public void editAndSaveWidgetForStandBannerWithButton() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_BANNER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4);
        pagesInit.getWidgetClass()
                .changeDataAndSaveWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4)
                        .saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("mgid_banner"))
                .setWidgetIds(MGID_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4), "FAIL -> check base settings");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "maxw_1"), "300", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "maxh_1"), "250", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "ident_p"), "true", "FAIL -> ident_p");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_banner"))
                .setWidgetIds(MGID_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_300x250_4), "FAIL AMP -> check base settings");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "maxw_1"), "300", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "maxh_1"), "250", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_BANNER_ID, "ident_p"), "true", "FAIL -> ident_p");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.BANNER, SubTypes.BANNER_470x325")
    @Test
    public void editAndSaveWidgetForStandBanner_470x325() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_BANNER_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.BANNER, WidgetTypes.SubTypes.BANNER_470x325);
        serviceInit.getServicerMock().setStandName(setStand("mgid_banner"))
                .setWidgetIds(MGID_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        //header (logo, show_name)
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerLogoImg(), "https://cdn.mgid.com/images/mgid/circle_logo.png", "FAIL: logo");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBannerCampaignName(), "FAIL: show_name");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerWidgetTitle(), "Sponsored", "FAIL: widget title");

        //check button '.mglbtn'
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "\"Times New Roman\"", "FAIL: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "700", "FAIL: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "11px", "FAIL: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("color")), "#1f2224", "FAIL: MglBtnStyle -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "Learn more", "FAIL: MglBtnStyle -> text");

        //TITLE (.mctitle>a)
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "15px", "FAIL: TITLE(.mctitle>a) -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE(.mctitle>a) -> font-color");

        //.mcimg
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL: .mcimg -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "182.641px", "FAIL: .mcimg -> height");


        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_banner"))
                .setWidgetIds(MGID_WIDGET_BANNER_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().switchToAmpFrame();

        //header (logo, show_name)
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerLogoImg(), "https://cdn.mgid.com/images/mgid/circle_logo.png", "FAIL AMP: logo");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBannerCampaignName(), "FAIL AMP: show_name");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBannerWidgetTitle(), "Sponsored", "FAIL AMP: widget title");

        //check button '.mglbtn'
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-family"), "\"Times New Roman\"", "FAIL AMP: MglBtnStyle -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-weight"), "700", "FAIL AMP: MglBtnStyle -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnStyle("font-size"), "11px", "FAIL AMP: MglBtnStyle -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getBrandMglBtnStyle("color")), "#1f2224", "FAIL AMP: MglBtnStyle -> color");
        softAssert.assertEquals(pagesInit.getWidgetClass().getBrandMglBtnText(), "Learn more", "FAIL AMP: MglBtnStyle -> text");

        //TITLE (.mctitle>a)
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "15px", "FAIL: TITLE(.mctitle>a) AMP -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333", "FAIL: TITLE(.mctitle>a) AMP -> font-color");

        //.mcimg
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-width"), "328px", "FAIL: .mcimg AMP -> max-width");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("max-height"), "328px", "FAIL: .mcimg AMP -> max-height");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMcimgStyle("height"), "182.641px", "FAIL: .mcimg AMP -> height");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.HEADLINE, SubTypes.NONE <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test
    public void editAndSaveWidgetForStandHeadlineInPicture() {
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_HEADLINE_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE).saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand("mgid_headline"))
                .setWidgetIds(MGID_WIDGET_HEADLINE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE), "FAIL -> widget don't check");
        log.info("request parameters to servicer for banner");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "maxw_3"), "409", "FAIL -> maxw_3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "maxh_3"), "273", "FAIL -> maxh_3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "ident_p"), "true", "FAIL -> ident_p");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_headline"))
                .setWidgetIds(MGID_WIDGET_HEADLINE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE), "FAIL -> widget AMP don't check");

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "maxw_3"), "409", "FAIL -> AMP maxw_3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "maxh_3"), "273", "FAIL -> AMP maxh_3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_HEADLINE_ID, "ident_p"), "true", "FAIL -> AMP ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.IN_SITE_NOTIFICATION, SubTypes.IN_SITE_NOTIFICATION_MAIN \nПроверяем:\n -время отображения\n -стили блока 'title'\n -виджет исчезает после клика по иконке 'close'\n -check widget position(top/bottom \n" +
            "Add \"In-site notification\" format to MGID dashboard <a href=\"https://jira.mgid.com/browse/VT-28177\">VT-28177</a>")
    @Test
    public void editAndCheckWorkInSiteNotificationMainWidgetOnStand() {
        String cssPosition;
        log.info("Test is started");
        goToCreateWidget(MGID_WIDGET_IN_SITE_NOTIFICATION_ID);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN);
        pagesInit.getWidgetClass()
                .setRerunAds(true)
                .setShadowDom(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN).saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("mgid_in_site_notification"))
                .setWidgetIds(MGID_WIDGET_IN_SITE_NOTIFICATION_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("check TITLE styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE -> font-weight");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-size"), "14px", "FAIL: TITLE -> font-size");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#333233", "FAIL: TITLE -> color");

        log.info("check widget position (top = 10px & bottom = 540px) | (top = 540px & bottom = 10px)");
        cssPosition = pagesInit.getWidgetClass().getPosition().equals("top") ? "bottom" : "top";
        log.info("cssPosition: " + cssPosition);
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgBoxStyle(pagesInit.getWidgetClass().getPosition()), (cssPosition.equals("top") ? "30px" : "40px"), "FAIL -> widget position size");
        softAssert.assertNotEquals(pagesInit.getWidgetClass().getMgBoxStyle(cssPosition), "40px", "FAIL -> widget position auto");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.IN_SITE_NOTIFICATION_MAIN), "FAIL -> base settings");

        log.info("check work reRun Ads through 5s");
        log.info("close 1 ads");
        pagesInit.getWidgetClass().clickCloseIconInSiteNotification();
        serviceInit.getServicerMock().setWidgetIds(MGID_WIDGET_IN_SITE_NOTIFICATION_ID)
                .countDownLatchAwait(70);

        // get reRun flag
        // if true -> count requests to servicer = 2
        // if false -> count requests to servicer = 1
        int countRequestToServicer = 2;
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), countRequestToServicer, "FAIL -> size != " + countRequestToServicer);

        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css")
    @Description("Types.PASSAGE, SubTypes.NONE widget don't work on desktop <a href='https://jira.mgid.com/browse/TA-52004'>TA-52004</a>")
    @Test(description = "Types.PASSAGE, SubTypes.NONE -> widget don't work on desktop")
    public void passageWidgetDontWorkInDesktop() {
        log.info("Test is started");
        int widgetId = 473;
        authDashAndGo("testEmail49@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE)
                .setImpressionEveryPassage(1)
                .changeDataAndSaveWidget(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE);

        serviceInit.getServicerMock().setStandName(setStand("mgid_passage_desktop"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().activatePageOnStand();
        serviceInit.getServicerMock().countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0);
        log.info("Test is finished");
    }
}
