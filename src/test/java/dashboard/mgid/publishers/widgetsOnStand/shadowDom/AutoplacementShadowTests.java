package dashboard.mgid.publishers.widgetsOnStand.shadowDom;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.EndPoints.widgetTemplateUrl;

public class AutoplacementShadowTests extends TestBase {

    private final int widgetId = 148;

    public AutoplacementShadowTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId) {
        authDashAndGo("testEmail42@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    private final ArrayList<String> autoplacementErrors_2 = new ArrayList<>(List.of("Smart Informer: Article is to small to render informer block"));

    private final ArrayList<String> autoplacementErrors_1 = new ArrayList<>(List.of("Smart Informer: Article not found"));


    /**
     * Загружать виджет, если он с опцией autoplacement не нашел статьи
     * <p>не подошла высота статьи</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27316">TA-27316</a>
     */
    @Test(dataProvider = "autoplacementData")
    public void autoplacement_VALID_dontRightHeight(String autoplacementValue, ArrayList<String> errors) {
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass()
                .setAutoplacement(autoplacementValue)
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("mgid_autoplacement_shadow_1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(3);
        sleep(1000);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertFalse(serviceInit.getServicerMock().isSendRequestOnServicer(), "FAIL -> send request on servicer");
        softAssert.assertEquals(serviceInit.getServicerMock().getConsoleMessages().stream().filter(i -> i.contains(errors.get(0))).count(), 1, "FAIL -> messages");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void autoplacementOff_VALID_dontRightHeight() {
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass()
                .setAutoplacement("off")
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("mgid_autoplacement_shadow_1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(3);
        sleep(1000);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().getConsoleMessages().stream().filter(i -> i.contains(autoplacementErrors_2.get(0))).count(), 0, "FAIL -> messages");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "FAIL -> base settings");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    /**
     * Загружать виджет, если он с опцией autoplacement не нашел статьи
     * <p>нету нужного тега</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27316">TA-27316</a>
     */
    @Test(dataProvider = "autoplacementData_2")
    public void autoplacement_VALID_withoutArticleTag(String autoplacementValue, ArrayList<String> errors) {
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass()
                .setAutoplacement(autoplacementValue)
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("mgid_autoplacement_shadow_2"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(3);
        sleep(1000);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().getConsoleMessages().stream().filter(i -> i.contains(errors.get(0))).count(), 1, "FAIL -> messages");

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .setAutoplacement("off")
                .checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "FAIL -> base settings");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Загружать виджет, если он с опцией autoplacement не нашел статьи
     * <p>нету нужного тега</p>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27316">TA-27316</a>
     */
    @Test
    public void autoplacement_VALID_withoutArticleTagAutoplacementOff() {
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass()
                .setAutoplacement("off")
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        serviceInit.getServicerMock().setStandName(setStand("mgid_autoplacement_shadow_2"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(3);
        sleep(1000);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().getConsoleMessages().stream().filter(i -> i.contains(autoplacementErrors_1.get(0))).count(), 0, "FAIL -> messages 1");
        softAssert.assertEquals(serviceInit.getServicerMock().getConsoleMessages().stream().filter(i -> i.contains(autoplacementErrors_2.get(0))).count(), 0, "FAIL -> messages 2");

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .setAutoplacement("off")
                .checkWidgetInStand(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN), "FAIL -> base settings");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] autoplacementData(){
        return new Object[][]{
                {"top",             autoplacementErrors_2},
                {"attention-based", autoplacementErrors_2},
                {"bottom",          autoplacementErrors_2},
                {"middle",          autoplacementErrors_2}
        };
    }

    @DataProvider
    public Object[][] autoplacementData_2(){
        return new Object[][]{
                {"top",             autoplacementErrors_1},
                {"attention-based", autoplacementErrors_1},
                {"bottom",          autoplacementErrors_1},
                {"middle",          autoplacementErrors_1}
        };
    }
}
