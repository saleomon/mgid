package dashboard.mgid.publishers.widgetsOnStand.shadowDom;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static core.helpers.BaseHelper.convertRgbaToHex;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;
import static testData.project.OthersData.mgidLogo;

/*
 * check Simple code and AMP code + ShadowDOM
 */
public class WidgetsOnStandMgidShadow1Tests extends TestBase {

    public WidgetsOnStandMgidShadow1Tests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("testEmail42@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().setShadowDom(true);
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }


    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_CARDS")
    @Test
    public void editAndSaveWidgetForStandUnderArticleCards() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "FAIL -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_PAIR_CARDS")
    @Test
    public void editAndSaveWidgetForStandUnderArticlePairCards() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_PAIR_CARDS), "FAIL -> widget AMP don't check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_LIGHT_CARDS")
    @Test
    public void editAndSaveWidgetForStandUnderArticleLightCards() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT_CARDS), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_RECTANGULAR")
    @Test
    public void editAndSaveWidgetForStandUnderArticleRectangular() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE")
    @Test
    public void editAndSaveWidgetForStandUnderArticleSquare() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.UNDER_ARTICLE, SubTypes.UNDER_ARTICLE_SQUARE_BLACK")
    @Test
    public void editAndSaveWidgetForStandUnderArticleBlack() {
        int widgetId = 139;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK);
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_under_article_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_SQUARE_BLACK), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.HEADLINE, SubTypes.NONE")
    @Test
    public void editAndSaveWidgetForStandHeadlineInPicture() {
        int widgetId = 140;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("mgid_headline_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_headline_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADLINE, WidgetTypes.SubTypes.NONE), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.HEADER, SubTypes.HEADER_RECTANGULAR")
    @Test
    public void editAndSaveWidgetForStandHeaderRectangular() {
        int widgetId = 141;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR);
        serviceInit.getServicerMock().setStandName(setStand("mgid_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.HEADER, SubTypes.HEADER_SQUARE")
    @Test
    public void editAndSaveWidgetForStandHeaderSquare() {
        int widgetId = 141;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE);
        serviceInit.getServicerMock().setStandName(setStand("mgid_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "FAIL -> widget don't check");

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_header_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_SQUARE), "FAIL -> widget AMP don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_BLUR/SIDEBAR_MAIN\n" +
            "infinite-scroll = true")
    //todo RKO https://jira.mgid.com/browse/VT-28459
    //@Test(dataProvider = "smartType")
    public void editAndSaveWidgetForStandSmartMain_infiniteScroll_true(WidgetTypes.SubTypes type) {
        int widgetId = 142;
        log.info("Test is started: " + type);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, type)
                .setInfiniteScroll(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, type);

        serviceInit.getServicerMock().setStandName(setStand("mgid_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwaitWithoutTearDown(35);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 45), "FAIL -> infinite scroll");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart_shadow"))
                .setServicerCountDown(3)
                .navigate()
                .scrollToSlow()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, type), "FAIL  AMP-> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 30), "FAIL -> amp infinite scroll");
        softAssert.assertAll();
        log.info("Test is finished: " + type);
    }

    @DataProvider
    public Object[][] smartType(){
        return new Object[][]{
                {WidgetTypes.SubTypes.SMART_MAIN},
                {WidgetTypes.SubTypes.SMART_PLUS}
        };
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.SIDEBAR, SubTypes.SIDEBAR_MAIN\n" +
            "infinite-scroll = false")
    //todo RKO https://jira.mgid.com/browse/VT-28459
    //@Test
    public void editAndSaveWidgetForStandSmartMain_infiniteScroll_false() {
        int widgetId = 142;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);

        serviceInit.getServicerMock().setStandName(setStand("mgid_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "202"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "313"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "204"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "313"), "FAIL: .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: DOMAIN -> color");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN), "FAIL AMP -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "304", "202"), "FAIL: AMP .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "470", "313"), "FAIL: AMP .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "307", "204"), "FAIL: AMP .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "470", "313"), "FAIL: AMP .mcimg -> row4");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Montserrat, Arial, \"Helvetica Neue\", Helvetica, sans-serif", "FAIL: AMP DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: AMP DOMAIN -> color");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.SIDEBAR, SubTypes.SMART_BLUR\n" +
            "infinite-scroll = false")
    //todo RKO https://jira.mgid.com/browse/VT-28459
    //@Test
    public void editAndSaveWidgetForStandSmartBlur_infiniteScroll_false() {
        int widgetId = 142;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR);

        serviceInit.getServicerMock().setStandName(setStand("mgid_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "248", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL -> blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL -> blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL -> blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> blur row4");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL: AMP .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: AMP .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL: AMP .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: AMP .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "248", 2), "FAIL: AMP .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial, sans-serif", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#fff", "FAIL: AMP TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Verdana, sans-serif", "FAIL: DOMAIN AMP -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "700", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#fff", "FAIL: AMP DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL ->AMP blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL ->AMP blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL ->AMP blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> AMP blur row4");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Widget Stand(simple/amp code) -> check work js and widget css(Shadow DOM)")
    @Story("Widget Stand(simple/amp code) MGID -> check work js and widget css(Shadow DOM)")
    @Description("Types.SMART, SubTypes.SMART_PLUS\n" +
            "infinite-scroll = false")
    //todo RKO https://jira.mgid.com/browse/VT-28459
    //@Test
    public void editAndSaveWidgetForStandSmartPlus_infiniteScroll_false() {
        int widgetId = 142;
        log.info("Test is started");
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS);

        serviceInit.getServicerMock().setStandName(setStand("mgid_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL: .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL: .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "248", 2), "FAIL: .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial", "FAIL: TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#00f4b81", "FAIL: TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial", "FAIL: DOMAIN -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL -> blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL -> blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL -> blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> blur row4");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand("mgid_amp_smart_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait(3);
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .switchToAmpFrame()
                .checkWidgetInStand(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS), "FAIL -> base settings");
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowBlockTeaserIcon(), "FAIL AMP -> isShowBlockTeaserIcon");

        log.info("infinite scroll");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkCountTeasers( 15), "FAIL -> amp infinite scroll");

        log.info("logo");
        softAssert.assertEquals(pagesInit.getWidgetClass().getLogoMgidLinkInStand(), mgidLogo, "FAIL AMP -> logo");

        log.info(".mcimg");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(1, "442", "294"), "FAIL: AMP .mcimg -> row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(2, "336", "224"), "FAIL: AMP .mcimg -> row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(3, "442", "294"), "FAIL: AMP .mcimg -> row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "300", "200", 1), "FAIL: AMP .mcimg -> row4_1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkSmartImageStyle(4, "373", "248", 2), "FAIL: AMP .mcimg -> row4_2");

        log.info("TITLE (.mctitle>a)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-family"), "Arial", "FAIL: AMP TITLE(.mctitle>a) -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleStyle("font-weight"), "700", "FAIL: AMP TITLE(.mctitle>a) -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getTitleStyle("color")), "#00f4b81", "FAIL: AMP TITLE(.mctitle>a) -> font-color");

        log.info("check DOMAIN '.mcdomain'");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-family"), "Arial", "FAIL: DOMAIN AMP -> font-family");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDomainSmartStyle("font-weight"), "400", "FAIL: AMP DOMAIN -> font-weight");
        softAssert.assertEquals(convertRgbaToHex(pagesInit.getWidgetClass().getDomainSmartStyle("color")), "#a9a9a9", "FAIL: AMP DOMAIN -> color");

        log.info("blur");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(1, "right", "0"), "FAIL ->AMP blur row1");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(2, "bottom", "0"), "FAIL ->AMP blur row2");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(3, "left", "0"), "FAIL ->AMP blur row3");
        softAssert.assertTrue(pagesInit.getWidgetClass().checkTextElementStyle(4, "bottom", "0"), "FAIL -> AMP blur row4");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
