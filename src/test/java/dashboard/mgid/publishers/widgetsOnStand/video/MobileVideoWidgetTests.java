package dashboard.mgid.publishers.widgetsOnStand.video;

import io.qameta.allure.*;
import libs.devtools.ServicerMock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.Map;

import static testData.project.EndPoints.widgetTemplateUrl;

public class MobileVideoWidgetTests extends TestBase {

    public MobileVideoWidgetTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "video-widgets/mobile/" + stand); }


    @BeforeClass
    public void prepareWidgets(){
        authDashAndGo("testEmail79@ex.ua", "publisher/edit-widget/id/" + 555);
        pagesInit.getWidgetClass()
                .chooseColumns(1)
                .chooseRows(1)
                .saveWidgetSettings();
    }

    @BeforeMethod
    public void setSomeFields(){
        pagesInit.getWidgetClass().setShadowDom(true);
        serviceInit.getServicerMock().setMobileEmulation(true);

        serviceInit.getServicerMock()
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.VIDEO)
                .setServicerCountDown(1)
                .setVpaidCountDown(1)
                .setEnableVideoFormats(
                        Map.of(
                                ServicerMock.VideoFormats.DESKTOP, true,
                                ServicerMock.VideoFormats.MOBILE, false
                        ));
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Epic("Widgets")
    @Feature("Widget Video Stand")
    @Story("Widget Video Stand MGID -> check work js")
    @Story("Mobile video widget")
    @Description("vpaid, instream => set in response 1 ad")
    @Owner("RKO")
    @Test(description = "vpaid, instream => set in response 1 ad")
    public void vpaidResponseAds1() {
        log.info("Test is started");
        int widgetId = 555;

        serviceInit.getServicerMock().setStandName(setStand("vpaid-video"))
                .setWidgetIds(widgetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        pagesInit.getWidgetClass().scrollToMgbox();
        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE, 1);

        log.info("content video#1::check preroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 1, "FAIL -> preroll::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 1, "FAIL -> preroll::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> preroll::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 1, "FAIL -> preroll::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 1, "FAIL -> preroll::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 1, "FAIL -> preroll::viewable_impression");

        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE,2);

        log.info("content video#1::check midroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 2, "FAIL -> midroll#1::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 2, "FAIL -> midroll#1::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> midroll#1::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 2, "FAIL -> midroll#1::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 2, "FAIL -> midroll#1::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 2, "FAIL -> midroll#1::viewable_impression");

        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.PROGRESS_100,1);
        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE,3);

        log.info("content video#2::check midroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 3, "FAIL -> midroll#2::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 3, "FAIL -> midroll#2::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> midroll#2::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 3, "FAIL -> midroll#2::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 3, "FAIL -> midroll#2::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 3, "FAIL -> midroll#2::viewable_impression");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Video Stand")
    @Story("Widget Video Stand MGID -> check work js")
    @Story("Mobile video widget")
    @Description("vpaid, instream => set in response 2 ads")
    @Owner("RKO")
    @Test(description = "vpaid, instream => set in response 2 ads")
    public void vpaidResponseAds2() {
        log.info("Test is started");
        int widgetId = 555;

        serviceInit.getServicerMock().setStandName(setStand("vpaid-video"))
                .setWidgetIds(widgetId)
                .setResponseCountVideoAds(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        pagesInit.getWidgetClass().scrollToMgbox();
        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE, 2);

        log.info("content video#1::check preroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 2, "FAIL -> preroll::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 2, "FAIL -> preroll::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> preroll::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 2, "FAIL -> preroll::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 2, "FAIL -> preroll::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 2, "FAIL -> preroll::viewable_impression");

        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE,4);

        log.info("content video#1::check midroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 4, "FAIL -> midroll#1::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 4, "FAIL -> midroll#1::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> midroll#1::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 4, "FAIL -> midroll#1::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 4, "FAIL -> midroll#1::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 4, "FAIL -> midroll#1::viewable_impression");

        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.PROGRESS_100,1);
        serviceInit.getServicerMock().waitCustomEvent(ServicerMock.VsEvents.COMPLETE,6);

        log.info("content video#2::check midroll");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.REQUEST), 6, "FAIL -> midroll#2::requestad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.LOAD), 6, "FAIL -> midroll#2::adloaded");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VIEW), 1, "FAIL -> midroll#2::inview");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.START), 6, "FAIL -> midroll#2::startad");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.IMPRESSION), 6, "FAIL -> midroll#2::impression");
        softAssert.assertEquals(serviceInit.getServicerMock().getVs(ServicerMock.VsEvents.VI_IMPRESSION), 6, "FAIL -> midroll#2::viewable_impression");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
