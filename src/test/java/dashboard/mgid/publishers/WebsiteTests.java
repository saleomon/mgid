package dashboard.mgid.publishers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.dash.publisher.logic.Website.WebSiteStatus;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.AIA;
import static pages.dash.publisher.variables.PublisherWebSiteVariables.adsTxtNetworksContent;
import static pages.dash.publisher.variables.PublisherWebSiteVariables.adsTxtSitesContent;
import static testData.project.ClientsEntities.CLIENTS_ADS_TXT_LOGIN;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID_REJECTED;


/*
 * - checkRemoderationOfWebsite
 * - addWebSite
 * - addWebSiteWithUtmTagging
 * - editWebSite
 * - editWebSiteWithUtmTagging
 * - checkDefaultUtmTagging
 *
 * VALIDATION
 * - addWebSite_allFieldsEmpty
 * - addWebSite_categoryFieldIsEmpty
 * - addWebSite_languageFieldIsEmpty
 * - addWebSite_domain_validation
 */
public class WebsiteTests extends TestBase {

    public WebsiteTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    /**
     * Ошибка при повторной подаче сайта на модерацию
     * <p>check send site to remoderation</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23810">TA-23810</a>
     * <p>NIO</p>
     */
    @Test
    public void checkRemoderationOfWebsite() {
        log.info("Test is started");
        authDashAndGo("publisher");
        pagesInit.getWebsiteClass().sendSiteToModeration(WEBSITE_MGID_WAGES_ID_REJECTED);
        log.info("check popup text");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkPopupText("The site was successfully sent for moderation"), "Popup text");
        pagesInit.getWebsiteClass().closePopup();
        log.info("check visibility send to moderation icon");
        softAssert.assertFalse(pagesInit.getWebsiteClass().checkVisibilitySendModerationIcon(WEBSITE_MGID_WAGES_ID_REJECTED), "Visibility of icon");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Create website -> check list/edit interface
     * <p>RKO</p>
     */
    @Test
    public void addWebSite() {
        log.info("Test is started");

        log.info("create new site");
        authDashAndGo("publisher/add-site");
        pagesInit.getWebsiteClass().addWebSite();

        log.info("check site settings in LIST site interface");
        softAssert.assertEquals(pagesInit.getWebsiteClass().getStatusWebSite(pagesInit.getWebsiteClass().getSiteId()),
                WebSiteStatus.ONMODERATION.getTypeValue(),
                "FAIL -> status site after create");
        softAssert.assertEquals(operationMySql.getClientsSites().getCoppaValue(pagesInit.getWebsiteClass().getSiteId()), 0,
                "FAIL -> parameter coppa is 1");

        log.info("check site settings in EDIT site interface");
        authDashAndGo("publisher/edit-site/site_id/" + pagesInit.getWebsiteClass().getSiteId());
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSiteInEditInterface());

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Create website with UTM tagging
     * <p>RKO</p>
     */
    @Test
    public void addWebSiteWithUtmTagging() {
        log.info("Test is started");

        log.info("create new site");
        authDashAndGo("publisher/add-site");
        pagesInit.getWebsiteClass().setUtmTagging(true).addWebSite();

        log.info("check site settings in LIST site interface");
        softAssert.assertEquals(pagesInit.getWebsiteClass().getStatusWebSite(pagesInit.getWebsiteClass().getSiteId()),
                WebSiteStatus.ONMODERATION.getTypeValue(),
                "FAIL -> status site after create");

        log.info("check site settings in EDIT site interface");
        authDashAndGo("publisher/edit-site/site_id/" + pagesInit.getWebsiteClass().getSiteId());
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSiteInEditInterface());

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Edit website -> check list/edit interface
     * <p>RKO</p>
     */
    @Test
    public void editWebSite() {
        log.info("Test is started");

        log.info("edit site");
        authDashAndGo("publisher/edit-site/site_id/" + 35);
        pagesInit.getWebsiteClass()
                .setLanguage("1")
                .editWebSite();

        log.info("check site settings in LIST site interface");
        softAssert.assertEquals(pagesInit.getWebsiteClass().getStatusWebSite("35"),
                WebSiteStatus.ACTIVE.getTypeValue(),
                "FAIL -> status site after edit");

        log.info("check site settings in EDIT site interface");
        authDashAndGo("publisher/edit-site/site_id/" + 35);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSiteInEditInterface());

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Edit website website with UTM tagging
     * <p>RKO</p>
     */
    @Test
    public void editWebSiteWithUtmTagging() {
        log.info("Test is started");

        log.info("edit site");
        authDashAndGo("publisher/edit-site/site_id/" + 36);
        pagesInit.getWebsiteClass()
                .setLanguage("1")
                .setUtmTagging(true)
                .editWebSite();

        log.info("check site settings in LIST site interface");
        softAssert.assertEquals(pagesInit.getWebsiteClass().getStatusWebSite("36"),
                WebSiteStatus.ACTIVE.getTypeValue(),
                "FAIL -> status site after edit");

        log.info("check site settings in EDIT site interface");
        authDashAndGo("publisher/edit-site/site_id/" + 36);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSiteInEditInterface());

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check default UTM tagging on create interface
     * <p>RKO</p>
     */
    @Test
    public void checkDefaultUtmTagging() {
        log.info("Test is started");

        log.info("create new site");
        authDashAndGo("publisher/add-site");
        pagesInit.getWebsiteClass().fillBaseWebSiteSettings();
        pagesInit.getWebsiteClass()
                .setUtmTagging(true)
                .setUtmSource(pagesInit.getWebsiteClass().getDomain() + "_internal")
                .setUtmMedium("internal")
                .setUtmCampaign(pagesInit.getWebsiteClass().getDomain() + "_internal")
                .switchOnUtmTagging();

        log.info("check default UTM tagging");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkUtmTagging(), "FAIL -> default UTM settings on create");

        pagesInit.getWebsiteClass().saveWebSite();

        log.info("check site settings in EDIT site interface");
        authDashAndGo("publisher/edit-site/site_id/" + pagesInit.getWebsiteClass().getSiteId());
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSiteInEditInterface(), "FAIL -> check site in edit interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    ///////////VALIDATION //////////////

    /**
     * VALIDATION: Check create website with all fields empty
     * <p>RKO</p>
     */
    @Test
    public void addWebSite_allFieldsEmpty() {
        log.info("Test is started");
        authDashAndGo("testEmail77@ex.ua","publisher/add-site");
        sleep(2000);
        pagesInit.getWebsiteClass().setSomeFields(true).addWebSite();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Fill all fields with appropriate values"));
        log.info("Test is finished");
    }

    /**
     * VALIDATION: Check create website with empty category
     * <p>RKO</p>
     */
    @Test
    public void addWebSite_categoryFieldIsEmpty() {
        log.info("Test is started");
        authDashAndGo("testEmail77@ex.ua","publisher/add-site");
        pagesInit.getWebsiteClass()
                .setSomeFields(true)
                .setDomain("testvvv.com")
                .setLanguage("1")
                .addWebSite();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Fill all fields with appropriate values"));
        log.info("Test is finished");
    }

    /**
     * VALIDATION: Check create website with empty language
     * <p>RKO</p>
     */
    @Test
    public void addWebSite_languageFieldIsEmpty() {
        log.info("Test is started");
        authDashAndGo("testEmail77@ex.ua","publisher/add-site");
        pagesInit.getWebsiteClass()
                .setSomeFields(true)
                .setDomain("testvvv.com")
                .setCategory("108")
                .addWebSite();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash("Fill all fields with appropriate values"));
        log.info("Test is finished");
    }

    /**
     * VALIDATION: Check create website with don't valid domain field
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataDomain")
    public void addWebSite_domain_validation(String domain, String message) {
        log.info("Test is started");
        authDashAndGo("testEmail77@ex.ua","publisher/add-site");
        pagesInit.getWebsiteClass()
                .setSomeFields(true)
                .setDomain(domain)
                .setCategory("108")
                .setLanguage("1")
                .addWebSite();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessageDash(message));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDomain() {
        return new Object[][]{
                {"", "Fill all fields with appropriate values"},
                {" ", "Fill all fields with appropriate values"},
                {"testtest", "Please, check the URL"},
                {"tt", "Invalid domain length"},
                {"test@ukr.net", "Please, check the URL"}
        };
    }

    /**
     * Check color off Ads.txt rows which absent in clients_sites_ads_txt.missing_lines
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24563">Ticket TA-24563</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkAdsTxtPopupLinesColor() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_ADS_TXT_LOGIN, "publisher");
        pagesInit.getWebsiteClass().openAdsTxtPopup();
        softAssert.assertTrue(pagesInit.getWebsiteClass().
                        checkAdsTxtLinesColorGreen("test-site-with-ads-txt-combined.com", adsTxtSitesContent),
                "FAIL -> Green line!");
        softAssert.assertFalse(pagesInit.getWebsiteClass().
                        checkAdsTxtLinesColorGreen("test-site-with-ads-txt-combined.com", adsTxtNetworksContent),
                "FAIL -> Not green line!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("Automatically generate the ads.txt file right after creation of a new domain")
    @Story("Websites list")
    @Description("Check adding ads.txt after creating of the website" +
            "* @see <a href=\"https://jira.mgid.com/browse/TA-52423\">Ticket TA-52423</a>")
    @Owner(AIA)
    @Test(description = "Automatically generate the ads.txt file right after creation of a new domain")
    public void checkAdsTxtAfterAddingWebSiteAndActivate() {
        log.info("Test is started");

        log.info("create new site");
        authDashAndGo("publisher/add-site");
        pagesInit.getWebsiteClass().addWebSite();

        authCabAndGo("wages/sites?id=" + pagesInit.getWebsiteClass().getSiteId());
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getWebsiteClass().getSiteId() + "/filters/%252Fid%252F" + pagesInit.getWebsiteClass().getSiteId());
        pagesInit.getCabWebsites()
                .setTier("5")
                .chooseTier()
                .editAdvertisersDomain()
                .saveEditWebsite();
        pagesInit.getCabWebsites().approveWebSite(Integer.parseInt(pagesInit.getWebsiteClass().getSiteId()));
        log.info("check ads.txt in list interface in cab");
        authCabAndGo("wages/sites?id=" + pagesInit.getWebsiteClass().getSiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkEditAdsTxtIcon(pagesInit.getWebsiteClass().getSiteId()),
                "FAIL -> edit ads.txt icon!");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
