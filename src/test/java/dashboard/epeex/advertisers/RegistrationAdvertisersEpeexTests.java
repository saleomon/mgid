package dashboard.epeex.advertisers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextMgid;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.epeexLink;

public class RegistrationAdvertisersEpeexTests extends TestBase {

    public RegistrationAdvertisersEpeexTests() {
        subnetId = Subnets.SubnetType.SCENARIO_EPEEX;
    }

    /**
     * Create client with role Advertiser
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerAdvertiserEpeex() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_ADVERTISER_LOGIN_EPEEX);
        log.info("Let's register new Advertiser!");
        open(epeexLink + "/user/signup");
        pagesInit.getSignUp().registerAdvertiser(NEW_ADVERTISER_LOGIN_EPEEX);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(epeexLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_ADVERTISER_LOGIN_EPEEX)).get(0).substring(epeexLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
        pagesInit.getSignUp().signInDash(NEW_ADVERTISER_LOGIN_EPEEX, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getCampaigns().setupProfileInPopup();
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(NEW_ADVERTISER_LOGIN_EPEEX),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Restore password")
    @Feature("Password restoring in Epeex")
    @Story("Main dashboard page")
    @Description("Check password restoring in Epeex dashboard and login with new password")
    @Owner("AIA")
    @Test(description = "Check password restoring in Epeex dashboard and login with new password")
    public void checkRestorePasswordEpeex() {
        log.info("Test is started");
        String userLogin = "restore-password.client@epeex.io";
        operationMySql.getMailPull().getCountLettersByClientEmail(userLogin);
        open(epeexLink + "/user/restore-password");
        pagesInit.getSignUp().restorePassword(userLogin);
        open(epeexLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(userLogin)).get(0).substring("https://panel.epeex.io".length()));
        pagesInit.getSignUp().createPassword(NEW_RESTORED_PASSWORD);
        pagesInit.getSignUp().signInDash(userLogin, NEW_RESTORED_PASSWORD);
        Assert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(userLogin),
                "FAIL -> Login after password restoring is failed!");
        log.info("Test is finished");
    }
}
