package dashboard.epeex.publishers;

import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.open;
import static pages.dash.signup.variables.SignUpPageVariables.confirmationPopupTextMgid;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.epeexLink;

public class RegistrationPublishersEpeexTests extends TestBase {

    /**
     * Create client with role Publisher
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27361">Ticket TA-27361</a>
     * <p>Author AIA</p>
     */
     @Test
    public void registerPublisherEpeex() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_PUBLISHER_LOGIN_EPEEX);
        log.info("Let's Register new publisher!");
        open(epeexLink + "/user/signup");
        pagesInit.getSignUp().registerPublisher(NEW_PUBLISHER_DOMAIN_EPEEX, NEW_PUBLISHER_LOGIN_EPEEX);
        softAssert.assertTrue(pagesInit.getSignUp().checkConfirmationPopup(confirmationPopupTextMgid),
                "FAIL -> confirmation popup isn't correct!");
        log.info("Get activation link from email and follow it");
        open(epeexLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_PUBLISHER_LOGIN_EPEEX)).get(0).substring(epeexLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
         pagesInit.getSignUp().signInDash(NEW_PUBLISHER_LOGIN_EPEEX, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkSelfRegisterPopup(),
                "FAIL -> Popup 'Setup your profile' isn`t displayed!");
        pagesInit.getWebsiteClass().setupProfileInPopup();
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkLoginOnPage(NEW_PUBLISHER_LOGIN_EPEEX),
                "FAIL -> login on the page isn`t correct!");
        softAssert.assertEquals(operationMySql.getClients().getAdDarknessCheckboxInNewClient(NEW_PUBLISHER_LOGIN_EPEEX), "0" ,
                "FAIL -> flag can_change_ad_darkness = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
