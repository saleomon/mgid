package testBase.baseTemplate;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.*;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

public class ScreenshotWidgetStandCheckingTemplate extends TestBase {

    public ScreenshotWidgetStandCheckingTemplate() { }

    public String clientLogin;
    public String subnetName;
    public String linkToAmpScreen = "defaultWidgetStands/%s/amp/";
    public String linkToSimpleScreen = "defaultWidgetStands/%s/simple/";

    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");
    public final SelenideElement locatorForAmp = $("#amp_widget");
    public final String  locatorForMediaElements = "[class='mgbottom_media']";
    public final SelenideElement locatorForWholePage = $("#section");

    public void goToCreateWidget(int widgetId){
        authDashAndGo(clientLogin, "publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "screenshots/" + subnetName + "/" + stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, "screenshots/" + stand); }


    public void standHelperCheckScreenshot(int widgetId, WidgetTypes.Types type, WidgetTypes.SubTypes subType,
                                           String simpleStandName, String ampStandName,
                                           String simpleScreenshot, String ampScreenshot) {
        log.info("ShadowDomEnabled = 0");
        operationMySql.getTickersComposite().updateShadowDomEnabled(0, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(type, subType);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand(simpleStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + simpleScreenshot + ".png")), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand(ampStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + ampScreenshot + ".png")), "FAIL -> screen amp");

        log.info("ShadowDomEnabled = 1");
        operationMySql.getTickersComposite().updateShadowDomEnabled(1, widgetId);
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setStand(simpleStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + simpleScreenshot + ".png")), "FAIL -> screen simple(ShadowDomEnabled)");

        log.info("AMP(ShadowDomEnabled)");
        serviceInit.getServicerMock().setStandName(setAmpStand(ampStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + ampScreenshot + ".png")), "FAIL -> screen amp(ShadowDomEnabled)");

        softAssert.assertAll();
    }

    public void standHelperCheckScreenshot(int shadowDom, int widgetId, WidgetTypes.Types type, WidgetTypes.SubTypes subType,
                                           String simpleStandName, String ampStandName,
                                           String simpleScreenshot, String ampScreenshot) {
        log.info("ShadowDomEnabled = 0");
        operationMySql.getTickersComposite().updateShadowDomEnabled(shadowDom, widgetId);
        goToCreateWidget(widgetId);
        if(shadowDom == 0) pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(type, subType);
        pagesInit.getWidgetClass().saveWidgetSettings();
        log.info("Simple code");
        serviceInit.getServicerMock().setStandName(setStand(simpleStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToSimpleScreen, subnetName) + simpleScreenshot + ".png")), "FAIL -> screen simple");

        log.info("AMP");
        serviceInit.getServicerMock().setStandName(setAmpStand(ampStandName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToTopStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot(String.format(linkToAmpScreen, subnetName) + ampScreenshot + ".png")), "FAIL -> screen amp");

        softAssert.assertAll();
    }

}
