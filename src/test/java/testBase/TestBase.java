package testBase;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import libs.dockerClient.base.*;
import org.apache.logging.log4j.LogManager;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import core.base.GeneralInit;
import libs.devtools.MobileEmulationSettings;
import testData.project.AuthUserCabData;
import testData.project.Subnets.SubnetType;

import java.io.*;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;

import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.driver;
import static core.helpers.AuthorizationHelper.*;
import static libs.dockerClient.base.DockerKafka.Topics.*;
import static core.base.BrowserCapabilities.videoRecorderMode;
import static core.service.PropertiesManager.ConfigValue;
import static core.service.PropertiesManager.getResourceByName;
import static testData.project.AuthUserCabData.AuthorizationUsers.*;
import static testData.project.CliCommandsList.Consumer.RECOMPILE_WIDGETS_CONSUMER;
import static testData.project.CliCommandsList.Cron.RECOMPILE_WIDGETS_CRON;
import static testData.project.ClientsEntities.CLIENTS_EMAIL;
import static testData.project.ClientsEntities.CLIENTS_PASSWORD;

@Listeners(MyTestListener.class)
public class TestBase extends GeneralInit{
    public    SoftAssert                softAssert;
    protected SubnetType                subnetId;
    protected String                    clientLogin;
    protected String                    clientPassword;
    private final String                    host;
    protected String                    updatedTime;
    protected static final ArrayList<String> failTests = new ArrayList<>();
    private final DockerCli                 dockerCli;
    private final DockerKafka               dockerKafka;
    private final DockerMySql               dockerMySql;
    private   LocalDateTime             startTimeClassExecuting;
    private   String                    className;
    protected MobileEmulationSettings   mobileEmulationSettings;


    public TestBase() {
        log                 = LogManager.getLogger(getClass());
        subnetId            = SubnetType.SCENARIO_MGID;
        clientLogin         = CLIENTS_EMAIL;
        clientPassword      = CLIENTS_PASSWORD;
        host                = getResourceByName(ConfigValue.HOST);
        dockerCli           = new DockerCli();
        dockerKafka         = new DockerKafka();
        dockerMySql         = new DockerMySql();
        mobileEmulationSettings = new MobileEmulationSettings();
    }

    @BeforeSuite
    public void insertBaseData() throws Exception {
        if(new File("target/surefire-reports/testng-failed.xml").exists() && System.getenv("GITLAB_CI_TOKEN").equals("gitlab-ci-token")){
            System.out.println("BeforeSuite: file testng-failed.xml exists");
            dockerMySql.reStartContainer();
            sleep(5000);
        }

        //disableSystemLog();
        // fill damp db
        operationMySql.insertBaseData();
        operationCh.insertBaseData();

        // fill aws
        awsClient.createBucket("Stonehenge.jpg", "monochromeImage.jpg", "dracaena-cinnabari.jpg", "geeraf.png",
                "mount.jpg", "happiness.jpeg", "car_driving.gif", "space_journey.gif", "1_1.gif", "3_2.gif", "16_9.gif",
                "1_1.png", "3_2.jpeg", "16_9.jpeg", "3_2_ex.gif", "managers_croped.mp4", "managers_croped.mov",
                "aurora_500x400.jpg", "happy_woman.mp4", "certif_png.png", "boat.jpg", "mashroom.jpg", "winter.jpg");

        //pull cli container
        dockerCli.pullContainer();

        // recompile widgets (cron send tasks to kafka topic, after this consumer recompile tasks from kafka)
     //  createTopicsAndRecompileWidgets();

        //get cookie for all users
        helpersInit.getAuthorizationHelper().getCookieForUsersAuthorization(GENERAL_USER, PRIVILEGE_USER, AA_USER, TEASER_MAKER_USER_1, TEASER_MAKER_USER_2, TEASER_MODERATION_USER, NEWS_MAKER_STAFFER_USER, NEWS_MODERATION_USER, ADMIN_USER);
    }

    @BeforeClass
    public void classInit() {
        try {
            // init allure for current thread
            SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                    .screenshots(true)
                    .savePageSource(false));

            // init mySql base
            operationMySql = operationMySql
                    .initDbS();

            operationCh = operationCh
                    .initDbS();

            runInit();

            startTimeClassExecuting = LocalDateTime.now();

        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }

    @BeforeMethod
    public void methodInit() {
        try {
            runInit();
        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }

    @AfterMethod
    public void driverDieBeforeMethod(ITestResult result){
        className = result.getInstanceName();
        try{
            if(result.getStatus() != ITestResult.SUCCESS) {

                // add fail tests
                failTests.add(className + "#" + result.getName());
                serviceInit.getScreenshotService().takeScreenshotOfFailedTest(result.getName());
            }
            driver().close();

            URL videoUrl = new URL("http://" + host + ":4444/video/" + sessionId + ".mp4");
            if (result.getStatus() == ITestResult.SUCCESS && videoRecorderMode.equals("enabled")) {
                log.info("sessionId is - " + sessionId);
                helpersInit.getBaseHelper().deleteVideoIfTestSuccess(videoUrl);
            } else if (result.getStatus() != ITestResult.SUCCESS && videoRecorderMode.equals("enabled")) {
                log.info("Video is: " + videoUrl);
                helpersInit.getBaseHelper().attachVideoToAllure(videoUrl);
            }
        } catch (Exception e) {
            driver().close();
        }
    }

    @AfterClass
    public void driverRemove() {
        try{
            driver().close();
            writeClassTimeEvaluating();
        }
        catch (Exception e){
            log.error("Catch " + e);
        }
    }

    @AfterSuite
    public void getAllFailTests(){
        System.out.println("############# Input variable key: MVN_COMMAND");
        System.out.println("############# Input variable value: -DVIDEO_RECORDING=enabled -Dtest=" + String.join(",", failTests));
    }


    private void runInit() {
        init(this.getClass().getSimpleName(),
                mobileEmulationSettings);
        softAssert      = new SoftAssert();
    }

    /**
     * go to custom cab link
     * auth user - if his doesn't authorization
     */
    public void authCabAndGo(String link) {
        pagesInit.getAuthorization().authCab(GENERAL_USER, link);
    }

    /**
     * go to custom cab link
     * auth user - if his doesn't authorization
     */
    public void authCabAndGo(AuthUserCabData.AuthorizationUsers user, String link) {
        pagesInit.getAuthorization().authCab(user, link);
    }

    /**
     * go to custom cab link
     * auth user for checking privileges - if his doesn't authorization
     */
    public void authCabForCheckPrivileges(String link) {
        pagesInit.getAuthorization().authCab(PRIVILEGE_USER, link);
    }

    /**
     * go to custom link
     * auth user - if his doesn't authorization
     */
    public void authDashAndGo(String link) {
        pagesInit.getSignUp().authDash(clientLogin, link, subnetId);
    }

    /**
     * auth custom user and go to link in dashboard
     */
    public void authDashAndGo(String clientsLogin, String link) {
        pagesInit.getSignUp().authDash(clientsLogin, link, subnetId);
    }

    /**
     * logout cab
     */
    public void logoutCab(AuthUserCabData.AuthorizationUsers user) {
        helpersInit.getAuthorizationHelper().deleteAuthorizationCookie(user);
        helpersInit.getAuthorizationHelper().goLinkCab(logOutCab);
    }

    /**
     * logout dash
     */
    public void logoutDash() {
        helpersInit.getAuthorizationHelper().goLinkDashboard(signOutDash, subnetId);
    }

    /**
     * recompile widgets (cron send tasks to kafka topic, after this consumer recompile tasks from kafka)
     * recompile widgets in 5 steps
     * - 1 step 1 widget from before insert in db
     * - 2-5 steps insert widgets(tasks) every step and run cron
     */
    private void createTopicsAndRecompileWidgets() {
        log.info("createTopicsAndRecompileWidgets - start");
        int KAFKA_NUM_PARTITIONS = 5;

        List<DockerKafka.Topics> listOfNeededTopics = Arrays.asList(NEWS_GENERATE_IMAGE_HASH, POPULATE_TEASERS_TAGS, TEASER_AD_TYPE_MASS, LEMATIZATION_WORD, TEASER_DUPLICATE_UPDATE_SPELL_CHECK, OVERALL_LOG);
        List<String> listOfExistedTopics = Arrays.asList(dockerKafka.getExistedTopics());

        listOfNeededTopics.forEach(element -> {
            if(!listOfExistedTopics.contains(element.getTopicsValue())) {
                dockerKafka.createTopic(element);
            }
        });

        if ((System.getenv("SUITE") == null || System.getenv("SUITE").matches("allScope|publishers|ampWidgets"))
                && operationMySql.getRecompileTasks().getRecompileTasksStatus().equals("wait")) {

            log.info("start consumers");
            for (int i = 0; i < KAFKA_NUM_PARTITIONS; i++) {
                dockerCli.runConsumer(RECOMPILE_WIDGETS_CONSUMER, "-vvv");
            }

            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");
            dockerKafka.waitUntilAllPartitionsFinished(WIDGETS_RECOMPILE_MAIN);
            sleep(2000);

            operationMySql.getRecompileTasks().insertTask();
            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");

            operationMySql.getRecompileTasks().insertTask1();
            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");

            operationMySql.getRecompileTasks().insertTask2();
            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");

            operationMySql.getRecompileTasks().insertTask3();
            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");

            operationMySql.getRecompileTasks().insertTask4();
            dockerCli.runAndStopCron(RECOMPILE_WIDGETS_CRON, "-vvv");

            dockerKafka.waitUntilAllPartitionsFinished(WIDGETS_RECOMPILE_MAIN);

            dockerCli.stopConsumer();
            log.info("finish consumers");
        }
        log.info("createTopicsAndRecompileWidgets - finish");
    }

    private void disableSystemLog() {
        System.setOut(
                new PrintStream(new OutputStream() {
                    @Override
                    public void close() {
                    }

                    @Override
                    public void flush() {
                    }

                    @Override
                    public void write(byte[] b) {
                    }

                    @Override
                    public void write(byte[] b, int off, int len) {
                    }

                    @Override
                    public void write(int b) {
                    }

                }));
        System.setErr(
                new PrintStream(new OutputStream() {
                    @Override
                    public void close() {
                    }

                    @Override
                    public void flush() {
                    }

                    @Override
                    public void write(byte[] b) {
                    }

                    @Override
                    public void write(byte[] b, int off, int len) {
                    }

                    @Override
                    public void write(int b) {
                    }

                }));
    }

    private void writeClassTimeEvaluating() throws IOException {
        if(!System.getenv("SELENOID_HOST").equalsIgnoreCase("localhost")) {
            //time execution of each classes
            LocalDateTime endTimeClassExecuting = LocalDateTime.now();
            StringBuilder timeExecution = new StringBuilder();
            timeExecution.append(className)
                    .append(" - ")
                    .append(startTimeClassExecuting.toLocalTime())
                    .append(" - ")
                    .append(endTimeClassExecuting.toLocalTime())
                    .append(". Total time evaluation in hours: ");

            endTimeClassExecuting = endTimeClassExecuting.minusSeconds(startTimeClassExecuting.getSecond()).minusMinutes(startTimeClassExecuting.getMinute()).minusHours(startTimeClassExecuting.getHour());
            timeExecution.append(endTimeClassExecuting.getHour())
                    .append(":")
                    .append(endTimeClassExecuting.getMinute())
                    .append(":")
                    .append(endTimeClassExecuting.getSecond());
            new File("/artifacts/timeExecutionOfClasses/").mkdir();
            FileWriter writerForClassExecution = new FileWriter("/artifacts/timeExecutionOfClasses/timeExecution.txt", true);
            writerForClassExecution.write(timeExecution + " \n");
            writerForClassExecution.write("+-----------------------------------------------+\n");
            writerForClassExecution.flush();
            writerForClassExecution.close();
        }
    }

}
