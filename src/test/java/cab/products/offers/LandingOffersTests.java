package cab.products.offers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.BaseHelper;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class LandingOffersTests extends TestBase {
    String imageNameDracena = "dracaena-cinnabari.jpg";

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23363">https://youtrack.mgid.com/issue/TA-23363</a>
     * Добавление продукта через интерфейс - Add New Product | CAB
     * <p>
     * Create/edit, delete product
     * NIO
     */
    @Test
    public void createProduct() {
        log.info("Test is started");
        String productNameForCreating = "TestProductName" + BaseHelper.getRandomWord(3);
        String productNameForEditing = "TestProductName" + BaseHelper.getRandomWord(3);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(productNameForCreating)
                .setPageLink("http://uchetka.dt00.net")
                .setCertificate( LINK_TO_RESOURCES_IMAGES + imageNameDracena)
                .createProduct();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product added successfully"), "Check message after create");
        pagesInit.getLandingOffers().findProduct(productNameForCreating);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkProductListInterface(), "something went wrong with created product list interface");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check created product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkCreatedProduct(), "something went wrong with created product");

        log.info("editing product");
        pagesInit.getLandingOffers().clearProductDataValues()
                .setProductName(productNameForEditing)
                .setPageLink("http://blahblah.dt00.net")
                .clearGeoSettings()
                .editProduct();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product changed successfully"), "Check message after edit");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check edited product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkCreatedProduct(), "something went wrong with edited product");
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().findProduct(productNameForEditing);
        log.info("delete product");
        pagesInit.getLandingOffers().deleteProduct();

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23363">https://youtrack.mgid.com/issue/TA-23363</a>
     * Добавление продукта через интерфейс - Add New Product | CAB
     * <p>
     * Check right for "Adding a new product" button at landing offers and creative request interfaces
     * NIO
     */
    @Privilege(name = {"goodhits/add-offer", "goodhits/remove-offer", "goodhits/update-offer"})
    @Test
    public void checkRightAddProductButton() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/add-offer", "goodhits/remove-offer", "goodhits/update-offer");
            authCabForCheckPrivileges("goodhits/ghits-teaser-landing-offers");
            softAssert.assertFalse(pagesInit.getLandingOffers().checkVisibilityAddProductButton(), "add-offer");
            pagesInit.getLandingOffers().findProduct("Something");
            Assert.assertTrue(pagesInit.getLandingOffers().checkVisibilitySearchResults(), "search results");
            softAssert.assertFalse(pagesInit.getLandingOffers().checkVisibilityDeleteProductButton(), "remove-offer");
            softAssert.assertFalse(pagesInit.getLandingOffers().checkVisibilityEditProductButton(), "update-offer");
            authCabAndGo("goodhits/creative-requests?creative_id=1");
            log.info("check icon at creative-teaser request");
            softAssert.assertFalse(pagesInit.getCabCreativeRequests().checkVisibilityAddProductIcon(), "creative interface");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            log.info("block finally execute");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/add-offer", "goodhits/remove-offer", "goodhits/update-offer");
        }
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23363">https://youtrack.mgid.com/issue/TA-23363</a>
     * Добавление продукта через интерфейс - Add New Product | CAB
     * <p>
     * Check validation of required create product form fields
     * - product name
     * - page link
     * - geo regions
     * NIO
     */
    @Test
    public void checkValidationRequiredCreateProductFormFields() {
        log.info("Test is started");
        String productName = "Test of Test Name" + BaseHelper.getRandomWord(3);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName("").setPageLink("http://uchetka.dt00.net").createProduct();
        log.info("check error message fo product name field");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Field is required"), "check error message fo product name field");

        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().clearProductDataValues().setProductName(productName).setPageLink("").createProduct();
        log.info("check error message fo page link field");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Field is required"), "check error message fo page link field");

        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().clearProductDataValues().setProductName(productName).setPageLink("http://uchetka.dt00.net");
        pagesInit.getLandingOffers().createProductWithoutAdskeepersGeoSettings("2");
        log.info("check error message for adskeeper geo settings");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Geo settings set not for all subnets"), "check error message for adskeeper geo settings");

        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().clearProductDataValues().setProductName(productName).setPageLink("http://uchetka.dt00.net").setWhiteListAdskeeper("Tier 1");
        pagesInit.getLandingOffers().createProductWithoutMgidGeoSettings("0");
        log.info("check error message for mgid geo settings");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Geo settings set not for all subnets"), "check error message for mgid geo settings");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23480">https://youtrack.mgid.com/issue/TA-23480</a>
     * Доработка продуктов
     * <p>
     * Check workability "All" checkbox for geo
     * NIO
     */
    @Test
    public void checkAllGeoCheckbox() {
        log.info("Test is started");
        String nameProduct = BaseHelper.getRandomWord(7);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(nameProduct).setPageLink("http://uchetka.dt00.net").createProductWithAllSubnetGeoSettings();
        pagesInit.getLandingOffers().findProduct(nameProduct);
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check choosen white geo");
        Assert.assertTrue(pagesInit.getLandingOffers().checkWhiteListGeo(), "check error message for product name field");
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23703">https://youtrack.mgid.com/issue/TA-23703</a>
     * <a href="https://youtrack.mgid.com/issue/CP-597">https://youtrack.mgid.com/issue/CP-597</a>
     * Добавить выбор United States штатов при добавлении/редактировании продуктов
     * - проверка создания продукта с United States регионом
     * - проверка отсутствия United States (CBD)
     * - проверка создания продукта с именем другого созданого ранее продукта
     * <p>
     * NIO
     */
    @Test
    public void checkUnitedStatesRegion() {
        log.info("Test is started");
        String productName = BaseHelper.getRandomWord(8);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(productName)
                .setPageLink("http://uchetka.dt00.net")
                .setWhiteListMgid("Tier 1")
                .setWhiteListAdskeeper("Tier 1")
                .setWhiteListCountriesMgidSource(new String[]{"United States"})
                .createProduct();

        log.info("check error message about duplicate of product name");
        pagesInit.getLandingOffers().openCreateProductForm();
        pagesInit.getLandingOffers().fillProductName(productName);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageProductForm("Offer with same name already exist"), "check error message about duplicate of product name");

        log.info("check product with CDB");
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().findProduct(productName);
        pagesInit.getLandingOffers().openEditProductForm();
        softAssert.assertFalse(pagesInit.getLandingOffers().isMarkedUnitedStatesCdb("Tier 1"), "United Sates CDB is not market");
        softAssert.assertTrue(pagesInit.getLandingOffers().getMarkedRegionsMgid().contains("United States"), "United Sates is marked");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/MT-4248">https://youtrack.mgid.com/issue/MT-4248</a>
     * Дублирование регионов проставленных в MGID в АК | добавление продукта
     * - проверка создания продукта с дублировинием регионов в Adskeeper при выборе региона для Mgid
     * <p>
     * NIO
     */
    @Test
    public void checkDuplicationMgidRegionsIntoAdskeeperRegions() {
        log.info("Test is started");
        String productName = BaseHelper.getRandomWord(8);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers()
                .setGenerateNewValues(false)
                .setWhiteListMgid("Tier 1")
                .setBlackListMgid("Tier 3")
                .setProductName(productName)
                .setPageLink("http://uchetka.dt00.net")
                .createProduct();
        log.info("Finding product");
        pagesInit.getLandingOffers().findProduct(productName);
        pagesInit.getLandingOffers().openEditProductForm();
        pagesInit.getLandingOffers().setWhiteListAdskeeper("Tier 1").setBlackListAdskeeper("Tier 3");
        log.info("Check duplication of regions Mgid to Adskeeper");
        Assert.assertTrue(pagesInit.getLandingOffers().checkMgidAndAdskeeperRegions());
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23893">https://youtrack.mgid.com/issue/TA-23893</a>
     * Поиска тизеров по продукту
     * - проверка наличия иконки
     * - проверка работы фильтра по переходе по иконке
     * NIO
     */
    @Test
    public void checkIconGoToTeasersList() {
        log.info("Test is started");
        String productName = "Something";
        log.info("Finding product");
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().findProduct(productName);

        log.info("Check link for keyword present");
        softAssert.assertTrue(helpersInit.getBaseHelper().checkDatasetContains(pagesInit.getLandingOffers().getLinkOfLinkRedirection(), "Something"), "Check keyword presence");
        authCabAndGo(pagesInit.getLandingOffers().getLinkGoToTeaserIcon());
        log.info("Check filter results be icon action");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkFilterResult("20", "1003"), "Check filter workability");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Offers")
    @Story("Добавить обозначение по офферам созданным из advertiser name")
    @Description("Check displaying icon 'advertiser name' due created offer from advertiser name\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52023\">Ticket TA-52023</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check displaying icon 'advertiser name' due created offer from advertiser name")
    public void checkDisplayingCreatedFromAdvertNameIcon() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=Something");
        softAssert.assertTrue(pagesInit.getLandingOffers().isDisplayedShowNameIcon(), "Fail - isDisplayedShowNameIcon");
        softAssert.assertEquals(pagesInit.getLandingOffers().getTipShowNameIcon(), "Offer added from advertiser name ", "FAIL - getTipShowNameIcon");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
