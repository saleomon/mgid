package cab.products.offers;

import core.helpers.BaseHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

public class BlackListOffers extends TestBase {


    @Test
    public void createOfferWithCasinoAndGamblingMgid() {
        log.info("Test is started");
        String nameProduct = BaseHelper.getRandomWord(7);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(nameProduct).setPageLink("http://testalexlun.net").createProductWithCasinoAndGamblingForMgid();
        log.info("check error message fo product name field");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Geo settings set not for all subnets"), "check error message fo product name field");

    }
    @Test
    public void createOfferWithCasinoAndGamblingAdskeeper() {
        log.info("Test is started");
        String nameProduct = BaseHelper.getRandomWord(7);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(nameProduct).setPageLink("http://testalexlun.net").createProductWithCasinoAndGamblingForAdskeeper();
        log.info("check error message fo product name field");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Geo settings set not for all subnets"), "check error message fo product name field");
    }
    @Test
    public void createOfferWithCasinoAndGamblingAllSubnets() {
        log.info("Test is started");
        String nameProduct = BaseHelper.getRandomWord(7);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(nameProduct).setPageLink("http://testalexlun.net").createProductWithCasinoAndGamblingAllSubnets();
        pagesInit.getLandingOffers().findProduct(nameProduct);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product list interface");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check choosen Casino and gambling all subnets");
        pagesInit.getLandingOffers().checkCasinoAndGamblingAllSubnetsCheckbox();
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");
        log.info("Test is finished");
    }

    @Test
    public void editOfferFromAllToCasinoAndGambling() {
        log.info("Test is started");
        String productNameForCreating = "TestProductName" + BaseHelper.getRandomWord(3);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();


        log.info("create product");
        pagesInit.getLandingOffers().setProductName(productNameForCreating)
                .setPageLink("http://testalexlun.net")
                .createProductWithAllBlackSubnetGeoSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product added successfully"), "Check message after create");
        pagesInit.getLandingOffers().findProduct(productNameForCreating);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product list interface");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check created product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");


        log.info("editing product");
        pagesInit.getLandingOffers().chooseCasinoAndGamblingAllSubnets();
        pagesInit.getLandingOffersCreateEditHelper().saveOffer();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product changed successfully"), "Check message after edit");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check edited product");
        pagesInit.getLandingOffers().checkCasinoAndGamblingAllSubnetsCheckbox();
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");

    }




    @Test
    public void editOfferToCasinoAndGamblingToAll() {
        log.info("Test is started");
        String productNameForCreating = "TestProductName" + BaseHelper.getRandomWord(3);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();


        log.info("create product");
        pagesInit.getLandingOffers().setProductName(productNameForCreating)
                .setPageLink("http://testalexlun.net")
                .createProductWithCasinoAndGamblingAllSubnets();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product added successfully"), "Check message after create");
        pagesInit.getLandingOffers().findProduct(productNameForCreating);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product list interface");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check created product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");
        pagesInit.getLandingOffers().checkCasinoAndGamblingAllSubnetsCheckbox();


        log.info("editing product");
        pagesInit.getLandingOffers().chooseAllBlackGeoForAllSubnets();
        pagesInit.getLandingOffersCreateEditHelper().saveOffer();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product changed successfully"), "Check message after edit");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check edited product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");
    }


    @Test
    public void editOfferToAllDisableCandGACheckbox() {
        log.info("Test is started");
        String productNameForCreating = "TestProductName" + BaseHelper.getRandomWord(3);
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().openCreateProductForm();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(productNameForCreating)
                .setPageLink("http://testalexlun.net")
                .createProductWithCasinoAndGamblingAllSubnets();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product added successfully"), "Check message after create");
        pagesInit.getLandingOffers().findProduct(productNameForCreating);
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product list interface");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check created product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkAllBlackGeoProductListInterface(), "something went wrong with created product");


        log.info("editing product");
        pagesInit.getLandingOffers().rejectCasinoAndGamblingAllSubnets();
        pagesInit.getLandingOffersCreateEditHelper().saveOffer();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product changed successfully"), "Check message after edit");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkErrorMessageField("Geo settings set not for all subnets"), "check error message fo product name field");

    }


}



