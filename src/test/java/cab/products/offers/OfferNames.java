package cab.products.offers;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.CliCommandsList.Consumer.OFFER_CONSUMER;

public class OfferNames extends TestBase {

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Check assigning offer name from teaser 'Advertiser name'</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check assigning offer name from teaser 'Advertiser name'")
    public void checkAssignOfferNameFromTeaserAdvertiserName() {
        try {
            log.info("Test is started");
            int teaserId = 2023;
            String advertiserName = "Advertiser name";
            serviceInit.getDockerCli().runConsumer(OFFER_CONSUMER, "-vvv");
            authCabAndGo("goodhits/on-moderation?id=" + teaserId);
            pagesInit.getCabTeasersModeration().approveTeaserWithoutLinkingOffer();
            Assert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(teaserId).contains(advertiserName),
                    "FAIL -> Offer name!");
            Assert.assertEquals(operationMySql.getTeasersLandingsOffers().selectFromShowName(advertiserName), "1",
                    "FAIL -> From show name value!");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Check creating offer from campaign 'Advertiser name'</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check creating offer from campaign 'Advertiser name'")
    public void checkCreateOfferFromCampaignAdvertiserName() {
        try {
            log.info("Test is started");
            int teaserId = 2024;
            int campaignId = 2023;
            String newAdvertiserName = "Advertiser name three";
            serviceInit.getDockerCli().runConsumer(OFFER_CONSUMER, "-vvv");
            authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
            pagesInit.getCabCampaigns().changeAdvertiserName(newAdvertiserName);
            Assert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(teaserId).contains(newAdvertiserName),
                    "FAIL -> Creating new offer from advertiser name!");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }
}
