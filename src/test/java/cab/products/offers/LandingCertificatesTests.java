package cab.products.offers;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static core.helpers.BaseHelper.getRandomWord;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class LandingCertificatesTests  extends TestBase {
    String imageNameDracena = "dracaena-cinnabari.jpg";
    /**
     * <p>Создание/редактирования оффера с сертификатом</p>
     * Редактирование сертификата
     * @see <a href="https://jira.mgid.com/browse/TA-51732">Ticket TA-51732</a>
     * <p>NIO</p>
     */
    @Test
    public void checkEditCertificate() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditCertificateOffer");
        pagesInit.getLandingOffers().openEditProductForm();

        log.info("editing product");
        pagesInit.getLandingOffers().clearProductDataValues()
                .setGenerateNewValues(false)
                .setCertificate( LINK_TO_RESOURCES_IMAGES + imageNameDracena)
                .setPageLink("http://blahblah." + getRandomWord(3) + ".net")
                .editProduct();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product changed successfully"), "Check message after edit");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("check edited product");
        softAssert.assertEquals(1, pagesInit.getLandingOffers().getAmountOfCertificates(), "something went wrong with edited product");
        softAssert.assertTrue(pagesInit.getLandingOffers().checkNameOfCertificate(imageNameDracena), "something went wrong with edited product");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирования оффера с сертификатом</p>
     * Проверка лимита загрузки числа сертификатов
     * @see <a href="https://jira.mgid.com/browse/TA-51732">Ticket TA-51732</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateOverAllowedAmount() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferCertLimit");
        pagesInit.getLandingOffers().openEditProductForm();

        log.info("upload certificates");
        pagesInit.getLandingOffers().uploadCertificates(LINK_TO_RESOURCES_IMAGES + imageNameDracena);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Limit of upload files exceeded. Limit 20"), "Check message after edit");
        log.info("check amount of certificates");
        softAssert.assertEquals(20, pagesInit.getLandingOffers().getAmountOfCertificates(), "something went wrong with edited product");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithAllowedExtension() {
        log.info("Test is started");

        String[] filesForUploading = {LINK_TO_RESOURCES_FILES + "certif_doc.doc",
                LINK_TO_RESOURCES_FILES + "certif_pdf.pdf",
                LINK_TO_RESOURCES_FILES + "certif_png.png"};

        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferAllowedExtenCerf");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("upload new certificate");

        pagesInit.getLandingOffers().uploadCertificates(filesForUploading);

        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferAllowedExtenCerf");
        pagesInit.getLandingOffers().openEditProductForm();

        Assert.assertEquals(3, pagesInit.getLandingOffers().getAmountOfCertificates(), "Fail - incorrect amount of certificates");

        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithNotValidExtension() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferNotAllowedExtenCerf");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("upload with incorrect extension");

        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_exten.txt");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("File 'certif_exten.txt' has a false extension"), "Fail - extencion");
        softAssert.assertEquals(0,  pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates first check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithNotValidSize() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferNotAllowedExtenCerf");
        pagesInit.getLandingOffers().openEditProductForm();
        log.info("upload with incorrect size");
        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_size.jpeg");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Maximum allowed size for file 'certif_size.jpeg' is '5MB' but '5.63MB' detected"), "Fail - size");
        softAssert.assertEquals(0, pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates second check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Скачивание сертификатов и вывод в интерфейсы | CAB</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51719">Ticket TA-51719</a>
     * <p>NIO</p>
     */
    @Privilege(name = "ghits-download-certificate")
    @Test
    public void downloadCertificateRight() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-download-certificate");
        authCabForCheckPrivileges("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=ExtraCertificate2");
        Assert.assertFalse(pagesInit.getLandingOffers().isDisplayedDownloadCertificateButton());
        log.info("Test is finished");
    }

    /**
     * <p>Фильтры тизеры/продукты с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51718">Ticket TA-51718</a>
     * <p>NIO</p>
     */
    @Test
    public void checkFilterByCertificate() {
        log.info("Test is started");

        log.info("filter by certificate");
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        pagesInit.getLandingOffers().filterByCertificate();
        Assert.assertEquals(pagesInit.getLandingOffers().getAmountOfProducts(),
                operationMySql.getOfferCertificates().getAmountOfProductsWithCertificates());

        log.info("Test is finished");
    }
}
