package cab.products;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static core.service.constantTemplates.ConstantsInit.*;
import static pages.cab.crm.helper.CrmAddHelper.PaymentTerms.MIXED_PAYMENT;
import static testData.project.ClientsEntities.*;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabProductsClientsTests extends TestBase {

    /**
     * check work filter 'Currency'
     * <a href="https://youtrack.mgid.com/issue/TA-23752">https://youtrack.mgid.com/issue/TA-23752</a>
     * Отсутствует фильтр "валюта клиента" в интерфейсе cab/goodhits/clients
     * RKO
     */
    @Test
    public void workCurrencyFilter(){
        log.info("Test is started");
        authCabAndGo("goodhits/clients");
        Assert.assertTrue(pagesInit.getCabProductClients().checkWorkCurrencyFilter());
        log.info("Test is finished");
    }

    /**
     * check work filter 'Low priority'
     * @see <a href="https://youtrack.mgid.com/issue/TA-24525">Ticket TA-24525</a>
     *<p>NIO</p>
     */
    @Test
    public void workLowPriorityFilter(){
        log.info("Test is started");
        authCabAndGo("goodhits/clients");
        log.info("Choose filter");
        String choosenPriority = pagesInit.getCabProductClients().filterClientsByLowPriority();
        log.info("Check displayed clients with db value. - Current filter value - " + choosenPriority);
        Assert.assertEquals(operationMySql.getClients().getAmountClientsWithPriority(choosenPriority), pagesInit.getCabProductClients().getAmountDisplayedClients());
        log.info("Test is finished");
    }

    @Story("Filters")
    @Description("Check work 'Filter By Login' filter in product clients interface\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52124\">Ticket TA-52124</a></li>\n" +
            "</ul>")
    @Owner(value = "OHV")
    @Test(description = "Check work 'Filter By Login' filter in product clients interface")
    public void checkFilterByLogin() {
        log.info("Test is started");
        String login = "testemail1000@ex.ua";
        authCabAndGo("goodhits/clients");
        pagesInit.getCabProductClients().filterClientsByLogin(login);

        softAssert.assertEquals(pagesInit.getCabProductClients().getClientLogin(),
                login,"FAIL -> get client login in product clients interface");
        softAssert.assertEquals(pagesInit.getCabProductClients().getAmountClients(),
                1,"FAIL -> get client amount in product clients interface, first method");

        pagesInit.getCabProductClients().filterClientsByLogin(login + "\uD83D\uDE02");

        softAssert.assertEquals(pagesInit.getCabProductClients().getAmountClients(),
                0,"FAIL -> get client amount in product clients interface, second method");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check right filter 'Low priority'
     * @see <a href="https://youtrack.mgid.com/issue/TA-24525">Ticket TA-24525</a>
     *<p>NIO</p>
     */
    @Privilege(name = "can_see_low_priority_filter")
    @Test
    public void checkRightLowPriorityFilter() {
        log.info("Test is started");
        log.info("Check filter - true");
        authCabForCheckPrivileges("");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_low_priority_filter");
        authCabForCheckPrivileges("goodhits/clients");
        softAssert.assertTrue(pagesInit.getCabProductClients().isDisplayedLowPriorityFilter(), "filter should be visible");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_low_priority_filter");
        log.info("Check filter - false");
        authCabForCheckPrivileges("goodhits/clients");
        softAssert.assertFalse(pagesInit.getCabProductClients().isDisplayedLowPriorityFilter(), "filter should be hidden");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Админка. Сброс пароля пользователя.
     * <ul>
     * <li>Проверка сброса пароля пользователя для делегирующей учетной записи пользователя</li>
     * <li>Проверка что сбрасывание пароля для делегируещего пользователя не влияет на пароль основного клента</li>
     * </ul>
     * @see <a href="https://jira.mgid.com/browse/TA-27412">Ticket TA-27412</a>
     *<p>NIO</p>
     */
    @Test
    public void checkResetPassword() {
        log.info("Test is started");
        int clientId = 1129;
        int subClientId = 1130;
        String subClientEmail = "user.ua@gmail.com";
        String clientEmail = "masstestEmail1129@ex.ua";

        authCabAndGo("goodhits/clients-edit/id/" + clientId);

        log.info("Reset password for sub client - user.ua@gmail.com");
        operationMySql.getMailPull().getCountLettersByClientEmail(subClientEmail);
        pagesInit.getCabProductClients().resetClientsPassword(subClientId);
        pagesInit.getCabProductClients().saveClientsSettings();
        open(helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(subClientEmail))
                .get(0).replace("https://", "http://local-"));

        log.info("Create new password for user.ua@gmail.com");
        pagesInit.getSignUp().activatePassword(NEW_CLIENT_PASSWORD);
        log.info("Check sign in dashboard for user.ua@gmail.com");
        pagesInit.getSignUp().signInDash(subClientEmail, NEW_CLIENT_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(clientEmail),
                "FAIL -> login on the page isn`t correct after changing password!");
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(clientEmail),
                "FAIL -> login on the page isn`t correct after sign in by user.ua@gmail.com!");

        log.info("Check sign in dashboard for masstestEmail1129@ex.ua");
        logoutDash();
        pagesInit.getSignUp().signInDash(clientEmail, CLIENTS_PASSWORD);
        softAssert.assertTrue(pagesInit.getCampaigns().checkLoginOnPage(clientEmail),
                "FAIL -> login on the page isn`t correct after sign in by masstestEmail1129@ex.ua!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Флаг управления созданием пуш-рк для НКС")
    @Description("Проверка работы права для включения возможности создавать пуш рк для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Privilege(name = "goodhits/can_create_push_campaign")
    @Test(description = "Проверка работы права для включения возможности создавать пуш рк для новых самозарегов")
    public void checkFlagForAbilityToCreatePushCampaignForSelfRegisterClientsPrivilege() {
        log.info("Test is started");
        authCabForCheckPrivileges("goodhits/clients-edit/id/" + 2037);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkCanCreatePushCampaignSettingsCheckboxIsDisplayed(),
                "FAIL -> Can create push campaign checkbox with enabled rights!");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_create_push_campaign");
        authCabForCheckPrivileges("goodhits/clients-edit/id/" + 2037);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkCanCreatePushCampaignSettingsCheckboxIsDisplayed(),
                "FAIL -> Can create push campaign checkbox with disabled rights");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Флаг управления созданием пуш-рк для НКС")
    @Description("Проверка состояния флага возможности создавать пуш рк для новых самозарегов в зависимости от трат\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка состояния флага возможности создавать пуш рк для новых самозарегов в зависимости от трат")
    public void checkStateOfFlagForAbilityToCreatePushCampaignForSelfRegisterClientsWithSpends() {
        log.info("Test is started");
        authCabAndGo("goodhits/clients-edit/id/" + 2038);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfCheckboxForCreatePushCampaignSettings(),
                "FAIL -> Can create push campaign checkbox for client with $1 spend!");
        authCabAndGo("goodhits/clients-edit/id/" + 2039);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateOfCheckboxForCreatePushCampaignSettings(),
                "FAIL -> Can create push campaign checkbox for client without $1 spend!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Флаг управления созданием пуш-рк для НКС")
    @Description("Проверка редактирования флага возможности создавать пуш рк для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51880\">Ticket TA-51880</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка редактирования флага возможности создавать пуш рк для новых самозарегов")
    public void checkEditFlagForAbilityToCreatePushCampaign() {
        log.info("Test is started");
        int clientId = 2037;
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateOfCheckboxForCreatePushCampaignSettings(),
                "FAIL -> Can create push campaign checkbox before editing!");
        pagesInit.getCabProductClients().setCanCreatePushCampaign(true).saveClientsSettings();
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfCheckboxForCreatePushCampaignSettings(),
                "FAIL -> Can create push campaign checkbox after enabling option!");
        pagesInit.getCabProductClients().setCanCreatePushCampaign(false).saveClientsSettings();
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateOfCheckboxForCreatePushCampaignSettings(),
                "FAIL -> Can create push campaign checkbox after disabling option!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Ограничение дневных спендов для новых саморегов")
        @Story("Флаг управления дневными ограничениями для НКС")
        @Description("Проверка работы права для флага ограничения трат по кампаниям для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51891\">Ticket TA-51891</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Privilege(name = "goodhits/can_set_verified_client")
    @Test(description = "Проверка работы права для флага ограничения трат по кампаниям для новых самозарегов")
    public void checkFlagVerifyClientForSelfRegisterClientsPrivilege() {
        log.info("Test is started");
        int clientId = 2043;
        authCabForCheckPrivileges("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkVerifiedClientSettingsCheckboxIsDisplayed(),
                "FAIL -> 'Verified client' checkbox with enabled rights!");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_set_verified_client");
        authCabForCheckPrivileges("goodhits/clients-edit/id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkVerifiedClientSettingsCheckboxIsDisplayed(),
                "FAIL -> 'Verified client' checkbox with disabled rights");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Ограничение дневных спендов для новых саморегов")
    @Description("Проверка редактирования флага 'Верифицированный клиент' для новых самозарегов\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51891\">Ticket TA-51891</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка редактирования флага 'Верифицированный клиент' для новых самозарегов")
    public void checkEditFlagVerifyClientForSelfRegisterClients() {
        log.info("Test is started");
        int clientId = 2043;
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfVerifiedClientSettingsCheckbox(),
                "FAIL -> 'Verified client' checkbox before editing!");
        pagesInit.getCabProductClients().setVerifiedClient(false).saveClientsSettings();
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateOfVerifiedClientSettingsCheckbox(),
                "FAIL -> 'Verified client' checkbox after disabling option!");
        pagesInit.getCabProductClients().setVerifiedClient(true).saveClientsSettings();
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfVerifiedClientSettingsCheckbox(),
                "FAIL -> 'Verified client' checkbox after enabling option!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Заборона ru доменів на рівні системи")
    @Story("Add a flag that allows the client to rotate teasers with the russian domains")
    @Description("Check edit flag 'Allow rotate ru domains'\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52120\">Ticket TA-52120</a></li>\n" +
            "   <li><p>NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check edit flag 'Allow rotate ru domains'")
    public void checkEditAllowRotateRuDomainsFlag() {
        log.info("Test is started");
        int clientId = 1124;

        authCabAndGo("goodhits/clients?client_id=" + clientId);

        log.info("Check enable flag");
        pagesInit.getCabProductClients().changeStateAllowRotateRuDomainFlag();
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateAllowRotateRuDomainFlag("On"), "Fail - is on");
        softAssert.assertTrue(operationMySql.getClients().getOptions(String.valueOf(clientId)).contains("\"allowRussianDomains\": true"), "Fail - should be true");

        log.info("Check disable flag");
        pagesInit.getCabProductClients().changeStateAllowRotateRuDomainFlag();
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateAllowRotateRuDomainFlag("Off"), "Fail - is off");
        softAssert.assertFalse(operationMySql.getClients().getOptions(String.valueOf(clientId)).contains("allowRussianDomains"), "Fail - should be false");

        softAssert.assertAll();
        log.info("Test is finished");
    }
    @Feature("Заборона ru доменів на рівні системи")
    @Story("Add a flag that allows the client to rotate teasers with the russian domains")
    @Description("Check privilege 'Allow rotate ru domains'\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52120\">Ticket TA-52120</a></li>\n" +
            "   <li><p>NIO</p></li>\n" +
            "</ul>")
    @Privilege(name="toggle-allow-russian-domains-flag")
    @Test(description = "Check privilege 'Allow rotate ru domains'")
    public void checkPrivilegeAllowEditAllowRotateRuDomainsFlag() {
        log.info("Test is started");
        int clientId = 1124;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/toggle-allow-russian-domains-flag");

        authCabForCheckPrivileges("goodhits/clients?client_id=" + clientId);

        log.info("Check presence flag");
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateAllowRotateRuDomainFlag("On"), "Fail - is on");
        softAssert.assertFalse(pagesInit.getCabProductClients().checkStateAllowRotateRuDomainFlag("Off"), "Fail - is off");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_GOODHITS_LIST_INTERFACE)
    @Description("Check edit options prepayment terms by icon \n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52121\">Ticket TA-52121</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check edit options prepayment terms by icon")
    public void checkEditPaymentTermsOptionByIcon() {
        log.info("Test is started!");
        Map<String, String> data = Map.of("file", "table.pdf", "net", "5");

        authCabAndGo("goodhits/clients?client_id=1128");
        pagesInit.getCabProductClients().clickPaymentTermsIcon();

        pagesInit.getCabProductClients().deletePaymentTermsFile();
        pagesInit.getCabProductClients().choosePaymentTerms(MIXED_PAYMENT);
        pagesInit.getCabProductClients().setPaymentTermsNet(data.get("net"));
        pagesInit.getCabProductClients().setPaymentTermsFile(data.get("file"));
        pagesInit.getCabProductClients().saveSettingsOfPaymentTermsPopup();

        authCabAndGo("goodhits/clients?client_id=1128");
        pagesInit.getCabProductClients().clickPaymentTermsIcon();
        Assert.assertTrue(pagesInit.getCabProductClients().checkPaymentTerms(MIXED_PAYMENT, data));
        log.info("Test is finished!");
    }

    @Epic("Creative Safety Ratings")
    @Feature("FEATURE | Creative Safety Ratings. MVP")
    @Story("Story to BT-4039 Отображение csr для отдельных клиентов, " +
            "CSR. Interface changes and more")
    @Description("Check save 'Creative Safety Ratings' options for client\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52126\">Ticket TA-52126</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52164\">Ticket TA-52164</a></li>\n" +
            "   <li><p>AIA</p></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that 'Creative Safety Ratings' is enabled for client by default")
    public void checkCsrEnablingByDefault() {
        log.info("Test is started");
        int clientId = 2049;
        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabProductClients().checkUseCsrIsDisplayed(),
                "FAIL -> CSR is visible, but should be hidden!");
        authCabAndGo("goodhits/ghits/?id=2026");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkCsrMediumIconIsDisplayed(),
                "FAIL -> Creative safety rating Medium icon is not visible!");
        authCabAndGo("goodhits/ghits/?id=2027");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkCsrHighIconIsDisplayed(),
                "FAIL -> Creative safety rating High icon is not visible!");
        log.info("Check CSR icon for teaser on moderation");
        authCabAndGo("goodhits/ghits/?id=2030");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkCsrHighIconIsDisplayed(),
                "FAIL -> Creative safety rating High icon is visible for teaser on moderation!");
        log.info("Check CSR icon for teaser without ad_type");
        authCabAndGo("goodhits/ghits/?id=2031");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkCsrHighIconIsDisplayed(),
                "FAIL -> Creative safety rating High icon is visible for teaser without ad_type!");
        log.info("Check CSR icon for teaser without landing_type");
        authCabAndGo("goodhits/ghits/?id=2032");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkCsrHighIconIsDisplayed(),
                "FAIL -> Creative safety rating High icon is visible for teaser without landing_type!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_GOODHITS_LIST_INTERFACE)
    @Description("Check prepayment terms privileges\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52273\">Ticket TA-52273</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Privilege(name={"goodhits/can_edit_payment_terms"})
    @Test(description = "Check prepayment terms privileges")
    public void checkPrivilegePrePaymentOption() {
        log.info("Test is started!");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_payment_terms");
            authCabForCheckPrivileges("goodhits/clients?client_id=1128");
            Assert.assertFalse(pagesInit.getCabProductClients().isDisplayedPaymentTermsIcon(), "Fail - can_edit_payment_terms");
            log.info("Test is finished!");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_payment_terms");
        }
    }
}
