package cab.products.creative;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.*;
import static core.service.constantTemplates.ConstantsInit.NIO;
import static testData.project.AuthUserCabData.AuthorizationUsers.*;
import static testData.project.AuthUserCabData.*;

public class CabCreativeTests extends TestBase {
    private static final String limitError = "The %s campaign has reached its limit on the number of teasers. The available limit within the campaign is %s teasers. Clean up teasers that don't get impressions.";

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23874">https://youtrack.mgid.com/issue/TA-23874</a>
     * Внедрение нового фильтра по куратору в интерфейс `creative`
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     * RKO
     */

    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator =  helpersInit.getBaseHelper().getRandomFromArray(new String[] {indiaProductUser, apacPublisherUser, cisProductUser, europeProductUser});
        authCabAndGo("goodhits/creative");
        log.info("set chosen curator and search creative tasks");
        Assert.assertTrue(pagesInit.getCabCreative().checkWorkCuratorFilter(curator), "FAIL -> tasks don't search by curator " + curator);
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24042">https://youtrack.mgid.com/issue/TA-24042</a>
     * Выставление типов тизеров по заявке
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7591&group_by=cases:section_id&group_order=asc&group_id=1737">Тест-кейсы</a>
     * #4, #5
     * RKO
     */
    @Test(dataProvider = "data_validationAdType")
    public void validationAdType(String testCase, String amountTasks, String message){
        log.info("test is started -> " + testCase);
        authCabAndGo("goodhits/creative/?id=" + 9);

        log.info("get random AdType");
        String customType =  helpersInit.getBaseHelper().getRandomFromArray(new String[] {"pg", "pg13", "r", "nc17", "nsfw"});

        log.info("set " + customType + " to task and get her");
        pagesInit.getCabCreative().assumeTask(amountTasks, customType);
        pagesInit.getCabCreative().goToCreateTeaser();

        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> don't correct message");
        log.info("Test is finished -> " + testCase);
    }

    @DataProvider
    public Object[][] data_validationAdType(){
        return new Object[][]{
                {"notValidData", "fdgd 345345", "Total number of teasers (345345)chosen for editors, exceed number of available in request(1)"},
                {"#5", "0", "Number of teasers not chosen"},
                {"#4", "2", "Total number of teasers (2)chosen for editors, exceed number of available in request(1)"}
        };
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка сохранения тизера в драфт</li>
     *     <li>Проверка статуса драфт в списке тизера</li>
     *     <li>Проверка инкремента количества драфтов в интерфейсе создания/редактирования тизера</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkDraftStatusAfterCreateTeaserFromRequest() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add/campaign_id/1002/task_id/1000");

        log.info("create teaser with task");
        String amountOfDrafts = pagesInit.getCabTeasers().getAmountOfDrafts();
        softAssert.assertNotEquals(pagesInit.getCabTeasers().useTitle(true).usePriceOfClick(false).setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE).setNeedToEditCategory(false).saveToDraft(), 0, "FAIL -> teaser doesn't created");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  CabTeasers.Statuses.INTERNAL_DRAFTS.getStatusesValue(), "FAIL - status list interface");

        authCabAndGo("goodhits/ghits-edit/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertNotEquals(pagesInit.getCabTeasers().getAmountOfDrafts(),  amountOfDrafts, "FAIL - problem with amount of drafts");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка количества драфтов в колонку креатива</li>
     *     <li>Проверка линки на драфт тезура</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkAmountOfDraftsAndLinkOnIt() {
        log.info("Test is started");
        log.info("Check displaying amount draft teasers");
        authCabAndGo("goodhits/creative?id=1001");
        softAssert.assertEquals(pagesInit.getCabCreative().getAmountOfDrafts(),  "1", "FAIL - wrong displayed amount");

        log.info("Check link to draft teaser and its status");
        authCabAndGo(pagesInit.getCabCreative().getLinkToDrafts());
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  CabTeasers.Statuses.INTERNAL_DRAFTS.getStatusesValue(), "FAIL - wrong status");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка удаления заявки на тизер со статусом драфт</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkDeleteTeaserAtDraftStatus() {
        log.info("Test is started");
        authCabAndGo("goodhits/creative?id=1002");
        pagesInit.getCabCreative().deleteTask();
        Assert.assertTrue(pagesInit.getCabCreative().isDisplayedRejectIcon(), "FAIL - should  be visible isDisplayedSendToModerationButton");
        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Назначение изображений
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51807">TA-51807</a>
     * NIO
     */
    @Test
    public void assignImagesForTeaserCreation() {
        log.info("Test is started");
        int creativeId = 1007;
        authCabAndGo("goodhits/creative?id=" + creativeId);

        log.info("Set images for teaser makers");
        pagesInit.getCabCreative().openGivenTaskPopup();
        pagesInit.getCabCreative().addTeaserMakerByIcon("beaver_with_beer");
        pagesInit.getCabCreative().chooseTeaserAmountAndAdType("beaver_with_beer", "1");
        pagesInit.getCabCreative().chooseImagesForTask("beaver_with_beer", "geeraf.png");
        pagesInit.getCabCreative().addTeaserMakerByIcon("herring_in_fur_coat");
        pagesInit.getCabCreative().chooseTeaserAmountAndAdType("herring_in_fur_coat", "2");
        pagesInit.getCabCreative().chooseImagesForTask("herring_in_fur_coat", "Stonehenge.jpg", "mount.jpg");
        pagesInit.getCabCreative().saveGiveTaskSettings();

        logoutCab(GENERAL_USER);
        sleep(4000);

        authCabAndGo(TEASER_MAKER_USER_2, "goodhits/creative?dateInterval=&id=" + creativeId);

        authCabAndGo(pagesInit.getCabCreative().goToCreateTeaser());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAvailableImagesFromRequest("Stonehenge.jpg", "mount.jpg"), "FAIL - herring_in_fur_coat not a beaver");

        logoutCab(TEASER_MAKER_USER_2);
        authCabAndGo(TEASER_MAKER_USER_1, "goodhits/creative?dateInterval=&id=" + creativeId);

        authCabAndGo(pagesInit.getCabCreative().goToCreateTeaser());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAvailableImagesFromRequest("geeraf.png"), "FAIL - beaver_with_beer not a beaver");


        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Назначение изображений
     * Дефолтное состояние
     * @see <a href="https://jira.mgid.com/browse/TA-51807">TA-51807</a>
     * NIO
     */
    @Test
    public void checkDefaultStateOfDisplayedImages() {
        log.info("Test is started");
        int creativeId = 1008;
        authCabAndGo("goodhits/creative?id=" + creativeId);

        pagesInit.getCabCreative().openGivenTaskPopup();
        pagesInit.getCabCreative().addTeaserMakerByIcon("beaver_with_beer");
        pagesInit.getCabCreative().chooseTeaserAmountAndAdType("beaver_with_beer", "1");
        pagesInit.getCabCreative().openEditRequest("beaver_with_beer");

        sleep(1000);

        Assert.assertEquals(pagesInit.getCabCreative().getAmountOfDisplayedMarkedImages(), 3);
        Assert.assertEquals(pagesInit.getCabCreative().getAmountOfRequestDataComments(), 3);
        Assert.assertEquals(pagesInit.getCabCreative().getAmountOfRequestDataLinks(), 3);

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 teaser one of them blocked second dropped</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //todo NIO https://jira.mgid.com/browse/CP-3033 @Test (description = "Check create teaser from request with over limit for campaign")
    public void checkCreateTeaserFromRequestOverLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1436;
        int limit = 2;

        authCabAndGo("goodhits/ghits-add/campaign_id/" + campaignId + "/task_id/1011");

        log.info("create teaser from task");
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .useTitle(true)
                .usePriceOfClick(false)
                .useCallToAction(true)
                .setNeedToEditCategory(false)
                .setDomain("https://testhim.com")
                .createTeaser(), 0, "FAIL -> teaser doesn't created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(String.format(limitError, campaignId, limit)), "FAIL -> checkErrorMessageCabForCustomField");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
