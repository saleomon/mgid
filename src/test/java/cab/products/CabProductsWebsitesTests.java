package cab.products;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.helpers.BaseHelper;
import testBase.TestBase;

import static testData.project.AuthUserCabData.*;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;

public class CabProductsWebsitesTests extends TestBase {

    /**
     * create/edit/check site
     * <p>NIO</p>
     */
    @Test
    public void createSite() {
        log.info("Test is started");
        String domain = "tushkanchika" + BaseHelper.getRandomWord(3) + ".net";

        log.info("create site");
        authCabAndGo("goodhits/sites-new/client_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabProductWebsites().setDomain("https://" + domain)
                .setComment("Wild animal")
                .setAllowAddDomainOtherPartner(true)
                .setAllowAddTeasersSubdomain(true)
                .createWebsite();

        log.info("check site at list interface");
        softAssert.assertTrue(pagesInit.getCabProductWebsites().checkDomainAtWebsiteListInterface(), "check domain at list interface");

        log.info("check site after creating");
        authCabAndGo("goodhits/sites-edit/id/" + pagesInit.getCabProductWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabProductWebsites().checkWebsiteEditInterface(), "check settings after creating");

        log.info("edit site");
        pagesInit.getCabProductWebsites()
                .setComment("Pet animal")
                .setAllowAddDomainOtherPartner(false)
                .setAllowAddTeasersSubdomain(false)
                .editWebsite();

        log.info("check site after editing");
        authCabAndGo("goodhits/sites-edit/id/" + pagesInit.getCabProductWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabProductWebsites().checkWebsiteEditInterface(), "check settings after editing");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23937">https://youtrack.mgid.com/issue/TA-23937</a>
     * Внедрение нового фильтра по куратору для `sites`
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     * RKO
     */
    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator = helpersInit.getBaseHelper().getRandomFromArray(new String[]{indiaProductUser, cisProductUser, europeProductUser});
        authCabAndGo("goodhits/sites");
        log.info("set chosen curator and search sites");
        Assert.assertTrue(pagesInit.getCabProductWebsites().checkWorkCuratorFilter(curator), "FAIL -> sites don't search by curator " + curator);
        log.info("Test is finished");
    }

}
