package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.*;
import static pages.cab.products.logic.CabTeasers.OwnershipOfMedia.OWN_IMAGE;

public class FocalPointTests extends TestBase {
    private final String videoLinkForTeaser = "/imgh/video/upload/if_iw_lte_680_or_ih_lte_680/ar_1:1,c_fill,w_680/if_else/ar_1:1,c_crop,w_680,x_-5,y_220/if_end/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4";

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check cancel edits of manual focal point</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check cancel edits of manual focal point")
    public void checkCancelEditingFocalPointVideo() {
        log.info("Test is started");
        int teaserId = 1471;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("managers_croped.mp4", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getCabTeasers().cancelManualFocalPoint();

        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .isNeedToEditUrl(false)
                .useDescription(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertTrue(options.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check reset data of manual focal point to default</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check reset data of manual focal point to default")
    public void checkResetFocalPointDataToDefault() {
        log.info("Test is started");
        int teaserId = 1458;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().setFocalPointToDefault();
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .isNeedToEditUrl(false)
                .useDescription(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        softAssert.assertTrue(options.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check reset active crop after set focal point to default")
    public void checkResetActiveCropToDefaultAfterSetFocalPoint() {
        log.info("Test is started");
        int teaserId = 1459;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().enableCropFormat("3-2");
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        Assert.assertTrue(pagesInit.getCabTeasers().isActiveCropVideo("16-9"), "FAIL - 16-9");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check message of editing focal point not completed")
    public void checkMessageOfEditingFocalPointNotCompletedVideo() {
        log.info("Test is started");
        int teaserId = 1472;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("managers_croped.mp4", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getCabTeasers()
                .setNeedToEditImage(false)
                .isNeedToEditUrl(false)
                .useDescription(true)
                .editTeaser();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Please disable focal point first"), "Fail - message inline");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = \"clVideoEffects\": [] </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check displayed button for focal point settings, approved teaser")
    public void checkDisplayingFocalPointButtonForApprovedTeaser() {
        log.info("Test is started");
        int teaserId = 1455;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        log.info("Test finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = \"clVideoEffects\": [\"x_-5\", \"y_-5\"] </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check displayed button for focal point settings, approved teaser")
    public void checkDisplayingManualAndDefaultButtonsForApprovedTeaser() {
        log.info("Test is started");
        int teaserId = 1456;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointDefaultButton(), "Fail - isDisplayedFocalPointDefaultButton");
        softAssert.assertAll();
        log.info("Test finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check getty images parameter with set focal point")
    public void checkGettyImagesFlagDueSetFocalPointForVideo() {
        log.info("Test is started");
        int teaserId = 1457;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_KID_FROM_GETTY, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers()
                .setNeedToEditImage(false)
                .isNeedToEditUrl(false)
                .useDescription(true)
                .editTeaser();

        Assert.assertEquals(operationMySql.getgHits1().getGettyImagesFlag(teaserId), "1212123", "Fail - getGettyImagesFlag");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check edit focal point on moderation inline")
    public void editFocalPointInlineOnModeration() {
        log.info("Test is started");
        int teaserId = 1460;

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);

        log.info("Edit teaser with response from cloudinary");
        pagesInit.getCabTeasersModeration().openPopupEditImageInline();
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasersModeration().setFocalPoint(133, 144).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasersModeration().savePopupImageSettings();

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);

        log.info("optionValues - " + optionValues);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);

        softAssert.assertTrue(optionValues.contains("clVideoEffects"), "Fail - clVideoEffects");
        softAssert.assertTrue(optionValues.contains("x_-5"), "Fail - x_");
        softAssert.assertTrue(optionValues.contains("y_220"), "Fail - y_");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "FAIL - 3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "FAIL - 16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains(videoLinkForTeaser), "FAIL - link");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check edit focal point inline teaser list interface")
    public void editFocalPointInlineListInterface() {
        log.info("Test is started");
        int teaserId = 1461;

        authCabAndGo("goodhits/ghits/?id=" + teaserId);

        log.info("Edit teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().openPopupEditVideoInline();
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers().savePopupImageSettings();

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);

        log.info("optionValues - " + optionValues);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);

        softAssert.assertTrue(optionValues.contains("clVideoEffects"), "Fail - clVideoEffects");
        softAssert.assertTrue(optionValues.contains("x_-5"), "Fail - x_");
        softAssert.assertTrue(optionValues.contains("y_220"), "Fail - y_");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "FAIL - 3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "FAIL - 16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains(videoLinkForTeaser), "FAIL - link");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1, </li>\n" +
            "   <li>g_hits_1.options = \"clVideoEffects\": [\"x_-5\", \"y_-5\"]</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check edit focal point zion inline")
    public void editSetToDefaultFocalPointInlineZion() {
        log.info("Test is started");
        int teaserId = 1462;

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);

        log.info("Create teaser with response from cloudinary");
        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        pagesInit.getCabTeasersModeration().editInLineImageInZion();

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasersModeration().setFocalPointToDefault();
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasersModeration().savePopupImageSettings();

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);

        softAssert.assertTrue(optionValues.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");
        softAssert.assertFalse(optionValues.contains("x_"), "Fail - x_");
        softAssert.assertFalse(optionValues.contains("y_"), "Fail - y_");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>g_hits_1.options \"clEffects\": [\"e_sharpen:100\", \"c_fill\", \"f_jpg\", \"q_auto:good\", \"g_xy_center\", \"x_236\", \"y_299\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state focal point with static to focal point with video")
    public void checkChangeStaticFocalPointToVideoFocalPoint() {
        log.info("Test is started");
        int teaserId = 1474;

        authCabAndGo("goodhits/ghits/?id=" + teaserId);

        log.info("Edit teaser with response from cloudinary");
        pagesInit.getCabTeasers().openPopupEditImageInline();
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinaryInline("managers_croped.mp4", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().setFocalPoint(130, 114).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertFalse(options.contains("e_sharpen"), "Fail - e_sharpen");
        softAssert.assertFalse(options.contains("f_jpg"), "Fail - f_jpg");
        softAssert.assertFalse(options.contains("g_xy_center"), "Fail - g_xy_center");
        softAssert.assertFalse(options.contains("x_236"), "Fail - x_236");
        softAssert.assertFalse(options.contains("y_299"), "Fail - y_299");
        softAssert.assertTrue(options.contains("clVideoEffects"), "Fail - clVideoEffects");
        softAssert.assertTrue(options.contains("x_-12"), "Fail - x_-12");
        softAssert.assertTrue(options.contains("y_288"), "Fail - y_288");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_DASH_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.karantin = 2</li>\n" +
            "      <li>g_hits_1.blocked = 1</li>\n" +
            "      <li>g_hits_1.options \"clVideoEffects\": [\"x_-5\", \"y_-5\"]</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52276\">Ticket TA-52276</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state focal point with video to focal point with static")
    public void checkChangeVideoFocalPointToStaticFocalPoint() {
        log.info("Test is started");
        int teaserId = 1470;

        authCabAndGo("goodhits/ghits/?id=" + teaserId);

        log.info("Edit teaser with response from cloudinary");
        pagesInit.getCabTeasers().openPopupEditVideoInline();
        pagesInit.getCabTeasers().loadImageCloudinaryInline("mount.jpg", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(130, 114).setFocalPoint(true);
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options - " + options);
        softAssert.assertTrue(options.contains("\"clVideoEffects\": []"), "Fail - clVideoEffects");
        softAssert.assertTrue(options.contains("clEffects"), "Fail - clEffects");
        softAssert.assertTrue(options.contains("e_sharpen"), "Fail - e_sharpen");
        softAssert.assertTrue(options.contains("f_jpg"), "Fail - f_jpg");
        softAssert.assertTrue(options.contains("g_xy_center"), "Fail - g_xy_center");
        softAssert.assertTrue(options.contains("x_328"), "Fail - x_328");
        softAssert.assertTrue(options.contains("y_495"), "Fail - y_495");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check cancel focal point option for teaser edit inline")
    public void checkCancelFocalPointSettingsEditTeaserInline() {
        log.info("Test is started");
        int teaserId = 1261;
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().loadImageCloudinaryInline("Stonehenge.jpg", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        pagesInit.getCabTeasers()
                .useManualFocalPoint(true)
                .setFocalPoint(124, 143)
                .setFocalPoint(false);
        pagesInit.getCabTeasers().cancelManualFocalPoint();
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check edit focal point on moderation status inline")
    public void checkEditFocalPointOnModerationInline() {
        log.info("Test is started");
        int teaserId = 5015;
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().openPopupEditImageInline();

        String optionsDebug = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("optionsDebug value - " + optionsDebug);

        pagesInit.getCabTeasersModeration().setFocalPoint(100, 144).setFocalPoint(true);

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().getImageLink().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");
        pagesInit.getCabTeasersModeration().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_171\""), "x_171");
        softAssert.assertTrue(options.contains("\"y_283\""), "y_283");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check edit focal point of rejected teaser inline")
    public void checkEditFocalPointOfRejectedTeaserInline() {
        log.info("Test is started");
        int teaserId = 5014;
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();

        String optionsDebug = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("optionsDebug value - " + optionsDebug);

        pagesInit.getCabTeasers().setFocalPoint(100, 144).setFocalPoint(true);

        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLink().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_171\""), "x_171");
        softAssert.assertTrue(options.contains("\"y_283\""), "y_283");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check set default settings for focal point, teasers on moderation inline")
    public void checkSetToDefaultSettingsOfFocalPointModerationInline() {
        log.info("Test is started");
        int teaserId = 5003;
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().clickImageCheckButton();

        pagesInit.getCabTeasersModeration().openPopupEditImageInlineZion();
        pagesInit.getCabTeasersModeration().setFocalPointToDefault();

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().getImageLink().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");
        pagesInit.getCabTeasersModeration().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check set default settings for focal point, teasers on moderation inline")
    public void checkSetToDefaultSettingsOfFocalPointRejectedInline() {
        log.info("Test is started");
        int teaserId = 5013;
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        pagesInit.getCabTeasers().setFocalPointToDefault();

        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLink().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check displayed button for focal point settings inline")
    public void checkDisplayingButtonForFocalPointApprovedTeaserWithDefaultFocalPointInline() {
        log.info("Test is started");
        int teaserId = 5005;
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52183\">Ticket TA-52183</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check displayed button for focal point settings inline")
    public void checkDisplayingButtonForFocalPointApprovedTeaserWithCustomFocalPointInline() {
        log.info("Test is started");
        int teaserId = 5006;
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointDefaultButton(), "Fail - isDisplayedFocalPointDefaultButton");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
