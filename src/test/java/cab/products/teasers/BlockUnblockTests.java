package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.*;
import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static testData.project.CliCommandsList.Consumer.*;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class BlockUnblockTests extends TestBase {

    private final String messageErrorOfBlocking = "\n" +
            "Couldn't block the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s .";


    @Epic(TEASERS)
    @Features({@Feature(BLOCK_UNBLOCK), @Feature(ADVERT_NAME), @Feature(MASS_ACTION)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check unblock video teaser without Ad name\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52237\">Ticket TA-52237</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check unblock video teaser without Ad name")
    public void checkUnblockVideoTeaserWithoutAdName() {
        log.info("Test is started");
        int teaserId = 1340;

        log.info("Check unblock teaser");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().unBlockTeaser();

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertTrue(pagesInit.getCabTeasers().isTeaserUnblocked(), "FAIL - create teaser");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(BLOCK_UNBLOCK)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52350\">Ticket TA-52350</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check regular block unblock mass action")
    public void checkBlockUnblockTeasersMassAction() {
        log.info("Test is started");
        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            log.info("Check unblock teaser");
            authCabAndGo("goodhits/ghits/?campaign_id=1410");
            pagesInit.getCabTeasers().unblockTeaserByMassAction();
            softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Teasers have been sent to queued for unblocking process"), "Fail - one message");
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UNBLOCK, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_UNBLOCK_BATCHES);
            authCabAndGo("goodhits/ghits/?campaign_id=1410");
            softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserUnblocked(), "FAIL - isTeaserUnblocked");

            serviceInit.getDockerCli().runConsumer(BULK_ACTION_BLOCK, "-vvv");
            log.info("Check unblock teaser");
            authCabAndGo("goodhits/ghits/?campaign_id=1411");
            pagesInit.getCabTeasers().blockTeaserByMassAction(false, "6");
            softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Teasers have been sent to queued for blocking process"), "Fail - two message");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_BLOCK_BATCHES);
            authCabAndGo("goodhits/ghits/?campaign_id=1411");
            softAssert.assertFalse(pagesInit.getCabTeasers().isTeaserUnblocked(), "FAIL - isTeaserUnblocked");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(BLOCK_UNBLOCK)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>1. Создаем тизер в Кабе как аккаунт-менеджер и проверяем отображение иконки и тайтла</li>\n" +
            "      <li>2. Создаем тизер в Дешборде как клиент, переходим в каб и проверяем отображение иконки и тайтла</li>\n" +
            "      <li>3. Делаем переблокировку тизера Модератором и проверяем, что он остается заблокированным системой</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-23908\">Ticket TA-23908</a></li>\n" +
            "     </ul>")
    @Owner(AIA)
    @Test(description = "Check re block by moderator blocked teaser by system")
    @Flaky
    public void checkBlockedTeaserBySystem() {
        log.info("Test is started");
        String campaignId = "2001";
        authCabForCheckPrivileges("goodhits/ghits-add/campaign_id/" + campaignId);
        log.info("* Create teaser from Cab *");
        pagesInit.getCabTeasers()
                .useTitle(true)
                .setPriceOfClick("5")
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .setNeedToEditCategory(false)
                .createTeaser();
        authCabForCheckPrivileges("goodhits/ghits/id/" + pagesInit.getCabTeasers().getTeaserId());
        log.info("Check teaser blocking after creating in Cab");
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedBySystem(),
                "FAIL -> for teaser created from cab");
        log.info("* Create teaser from Dash *");
        authDashAndGo("advertisers/add-teaser-goods/campaign_id/" + campaignId);
        pagesInit.getTeaserClass().setPriceOfClick("5")
                        .createTeaser();
        authCabForCheckPrivileges("goodhits/ghits/id/" + pagesInit.getTeaserClass().getTeaserId());
        log.info("Check teaser blocking after creating in Dashboard");
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedBySystem(),
                "FAIL -> for teaser created from dashboard");
        log.info("Set privileges for moderator");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/teaser_blocked_by_account_manager");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/teaser_blocked_by_moderator", "goodhits/unlock_teaser_blocked_by_moderator");
        authCabForCheckPrivileges("goodhits/ghits/?campaign_id=" + campaignId + "&client_id=&id=" + pagesInit.getCabTeasers().getTeaserId());
        log.info("Block/unblock teaser as moderator");

        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_BLOCK, "-vvv");

            pagesInit.getCabTeasers().blockTeaserByMassAction(false, "Blocked by moderator");
            helpersInit.getMessageHelper().isSuccessMessagesCab();
            authCabForCheckPrivileges("goodhits/ghits/?campaign_id=" + campaignId + "&client_id=&id=" + pagesInit.getCabTeasers().getTeaserId());
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_BLOCK_BATCHES);
            pagesInit.getCabTeasers().markTeaserGeoAsCompliant(RAC, ZPL, IAP, AUTOCONTROL, POLAND_REG_COMPLIANT);
            pagesInit.getCabTeasers().unBlockTeaser();
            log.info("Check teasers blocking after block/unblock as Moderator");
            softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedBySystem(),
                    "FAIL -> Block/unblock by moderator");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/teaser_blocked_by_account_manager");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/teaser_blocked_by_moderator", "goodhits/unlock_teaser_blocked_by_moderator");
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(BLOCK_UNBLOCK)
    @Stories({@Story(TEASER_LIST_CAB_INTERFACE), @Story(TEASER_LIST_DASH_INTERFACE)})
    @Description("\n" +
            "     <ul>\n" +
            "      <li>1. Блокируем тизер в Дешборде как клиент и проверяем отображение иконки и тайтла</li>\n" +
            "      <li>2. Переходим из каба в Деш как аккакунт менеджер - блокируем тизер и проверяем, что в кабе он заблокирован:\n" +
            "     * есть правильная иконка и тайтл</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-22131\">Ticket TA-22131</a></li>\n" +
            "     </ul>")
    @Owner(RKO)
    @Test(description = "Check block teaser by client and manager at dashboard as well")
    public void blockUnblockTeasersForClientAndAccManager() {
        log.info("Test is started");
        String campaignId = "1";
        String teaserId = "2001";
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().blockTeaserInDashboard();
        log.info("Go to Cab and check that teaser is Blocked and has icon 'C - client' and title 'Teaser blocked by client'");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByClient(),
                "FAIL -> teaser blocked by client!");
        log.info("Prepare teaser: it mast have status Unblocked");
        pagesInit.getCabTeasers().unBlockTeaser();
        log.info("Go to Dashboard by gogol as Account manager");
        helpersInit.getAuthorizationHelper().goByGogol();
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().blockTeaserInDashboard();
        log.info("Go to Cab and check that teaser is Blcoked and has icon 'A - Account manager' and title 'Teaser blocked by account manager'");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByAccountManager(),
                "FAIL -> teaser blocked by account manager!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(BLOCK_UNBLOCK)
    @Stories({@Story(TEASER_LIST_CAB_INTERFACE), @Story(TEASER_LIST_DASH_INTERFACE)})
    @Description("\n" +
            "     <ul>\n" +
            "      <li>1. Блокируем тизер через массовые действия в Дешборде как клиент и проверяем отображение иконки и тайтла</li>\n" +
            "      <li>2. Переходим из каба в Деш как аккакунт менеджер - блокируем тизер через массовые действия и проверяем, что в\n" +
            "     * кабе тизер заблокирован, нужная иконка и тайтл</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-22131\">Ticket TA-22131</a></li>\n" +
            "     </ul>")
    @Owner(RKO)
    @Test(description = "Check block teaser by client and manager at dashboard as well. Mass")
    public void blockUnblockTeasersForClientAndAccManagerWithMassAction() {
        log.info("Test is started");
        String campaignId = "1";
        String teaserId = "2001";
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().blockTeaserInDashByMassActionWithUnblocking();
        log.info("Go to Cab and check that teaser is Blocked and has icon 'C - client' and title 'Teaser blocked by client'");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByClient(),
                "FAIL -> teaser blocked by client!");
        log.info("Prepare teaser: it mast have status Unblocked (by Mass actions)");
        pagesInit.getCabTeasers().unBlockTeaser();
        log.info("Go to Dashboard by gogol as Account manager");
        helpersInit.getAuthorizationHelper().goByGogol();
        authDashAndGo("advertisers/teasers-goods/campaign_id/" + campaignId + "/search/" + teaserId);
        pagesInit.getTeaserClass().blockTeaserInDashByMassActionWithUnblocking();
        log.info("Go to Cab and check that teaser is Blcoked and has icon 'A - Account manager' and title 'Teaser blocked by account manager'");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByAccountManager(),
                "FAIL -> teaser blocked by account manager!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(BLOCK_UNBLOCK), @Feature(ADVERT_NAME)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check unblocking teaser without 'Advertiser name' in list</>" +
            "   <li>Check error message for unblocking teaser without advertiser name</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Test(description = "Check error message for blocking teaser without advertiser name")
    public void checkUnblockingTeaserWithoutAdvertiserName() {
        log.info("Test is started");
        String teaserId = "2020";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName(teaserId), "",
                "FAIL -> Empty advertiser name!");
        pagesInit.getCabTeasers().unBlockTeaser();
        Assert.assertEquals(helpersInit.getMessageHelper().getCustomMessageValueCab("Field \"Advertiser Name\" is required to unblock ad"),
                "Field \"Advertiser Name\" is required to unblock ad",
                "FAIL -> Approve teaser without Advertiser name error!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(BLOCK_UNBLOCK)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24031\">Ticket TA-24031</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Privilege(name = "goodhits/unlock_teaser_blocked_by_moderator")
    @Test
    public void checkReBlockingForbiddanceForTeaserBlockedByScheduleCab() {
        log.info("Test is started");
        int teaserId = 2003;
        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            operationMySql.getMailPull().getCountLettersByBody(String.format(messageErrorOfBlocking, teaserId));

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/unlock_teaser_blocked_by_moderator");
            authCabForCheckPrivileges("goodhits/ghits/?campaign_id=2001&id=" + teaserId);
            pagesInit.getCabTeasers().blockTeaserByMassAction(false, "Block by account manager.");

            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_MAIN_TOPIC);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_BLOCK, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_BLOCK_BATCHES);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");

            Assert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(messageErrorOfBlocking, teaserId)), "FAIL -> message");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/unlock_teaser_blocked_by_moderator");
            serviceInit.getDockerCli().stopConsumer();
        }
    }
}
