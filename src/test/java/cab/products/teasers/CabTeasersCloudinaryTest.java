package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.*;
import static pages.cab.products.logic.CabTeasers.OwnershipOfMedia.OWN_IMAGE;
import static testData.project.ClientsEntities.*;

public class CabTeasersCloudinaryTest extends TestBase {

    private final String videoLinkForTeaser = "/imgh/video/upload/if_iw_lte_680_or_ih_lte_680/ar_1:1,c_fill,w_680/if_else/ar_1:1,c_crop,w_680,x_-5,y_-5/if_end/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4";
    /**
     * Проверка создания тизера с опцией cloudinary
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24659">TA-24659</a>
     */
    @Test
    public void createTeaserWithCloudinaryCab() {
        log.info("Test is started");
        int campaignId = 1041;
        authCabAndGo("goodhits/ghits-add/campaign_id/" + campaignId);
        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(STONEHENGE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("Stonehenge.jpg", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .useCallToAction(true)
                .setRotateValidFormats(true)
                .setNeedToEditCategory(false)
                .setNeedToEditImage(false)
                .createTeaser();

        log.info("SEL_COORDS - " + operationMySql.getgHits1().getSellCoords(pagesInit.getCabTeasers().getTeaserId()));

        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        log.info("Check created teaser");
        pagesInit.getCabTeasers().openPopupEditImageInline();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkImageVisibility(), "Fail - image visibility");

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(true), "Fail - valid formats");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(pagesInit.getCabTeasers().getTeaserId());
        log.info("Options - " + optionValues);
        softAssert.assertTrue(optionValues.contains("e_sharpen:100"), "FAIL - e_sharpen cloudinary");
        softAssert.assertTrue(optionValues.contains("c_fill"), "FAIL - c_fill cloudinary");
        softAssert.assertTrue(optionValues.contains("g_faces:auto"), "FAIL - g_faces cloudinary");
        softAssert.assertTrue(optionValues.contains("f_jpg"), "FAIL - f_jpg cloudinary");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 | Database, pre-processing, creation form")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check create teaser with video</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check create teaser with video")
    public void createTeaserWithVideo() {
        log.info("Test is started");
        int campaignId = 1041;
        authCabAndGo("goodhits/ghits-add/campaign_id/" + campaignId);
        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("managers_croped.mp4", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_FALSE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().setFocalPoint(133, 244).setFocalPoint(true);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .useCallToAction(true)
                .setRotateValidFormats(true)
                .setNeedToEditCategory(false)
                .setNeedToEditImage(false)
                .createTeaser();

        int teaserId = pagesInit.getCabTeasers().getTeaserId();

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(true), "Fail - valid formats");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options links - " + optionVideoLinksValues);
        log.info("Options - " + options);
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "FAIL - 3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "FAIL - 16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains(videoLinkForTeaser), "FAIL - link");
        softAssert.assertTrue(options.contains("clVideoEffects"), "FAIL - g_xy_center");
        softAssert.assertTrue(options.contains("x_-5"), "FAIL - x_-5");
        softAssert.assertTrue(options.contains("y_-5"), "FAIL - y_-5");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_FORMAT)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check active format default values</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24577\">Ticket TA-24577</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check active format default values")
    public void checkActiveImageFormatsDefaultValuesCreate() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-add/campaign_id/1041/");

        log.info("Check default crop");
        pagesInit.getCabTeasers().loadImageCloudinary("Stonehenge.jpg", OWN_IMAGE);
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("16-9"), "FAIL - 16-9");

        log.info("Check crop 3-2");
        pagesInit.getCabTeasers().enableCropFormat("3-2");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("3-2"), "FAIL - 3-2");

        log.info("Check crop 1-1");
        pagesInit.getCabTeasers().enableCropFormat("1-1");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("1-1"), "FAIL - 1-1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_FORMAT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check active format default values</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24577\">Ticket TA-24577</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check active format default values")
    public void checkActiveImageFormatsDefaultValuesInline() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?id=1123");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        log.info("Check default crop");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("16-9"), "FAIL - 16-9");
        pagesInit.getCabTeasers().enableCropFormat("16-9");

        log.info("Check crop 3-2");
        pagesInit.getCabTeasers().enableCropFormat("3-2");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("3-2"), "FAIL - 3-2");

        log.info("Check crop 1-1");
        pagesInit.getCabTeasers().enableCropFormat("1-1");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveCrop("1-1"), "FAIL - 1-1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Интеграция Cloudinary. Cab. Редактирование тизера
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-24579">TA-24579</a>
     */
    @Test
    public void editTeaserWithCloudinary() {
        log.info("Test is started");
        int teaserId = 1121;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MOUNT, "*ghits-cloudinary-load*");

        pagesInit.getCabTeasers().loadImageCloudinary("mount.jpg", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait();
        pagesInit.getCabTeasers().useDescription(true)
                .useCallToAction(true)
                .setNeedToEditImage(false)
                .editTeaser();

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        log.info("Check created teaser");

        pagesInit.getCabTeasers().openPopupEditImageInline();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkImageVisibility());

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(false), "Fail - valid formats");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("--------- " + optionValues);
        softAssert.assertTrue(optionValues.contains("\"c_fill\""), "FAIL - effects c_fill");
        softAssert.assertTrue(optionValues.contains("\"g_faces:auto\""), "FAIL - effects g_faces");
        softAssert.assertTrue(optionValues.contains("\"f_jpg\""), "FAIL - effects f_jpg");
        softAssert.assertTrue(optionValues.contains("\"e_sharpen:100\""), "FAIL - effects e_sharpen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 | Database, pre-processing, creation form")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit teaser with video</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check edit teaser with video")
    public void editTeaserWithCloudinaryVideo() {
        log.info("Test is started");
        int teaserId = 1299;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("managers_croped.mp4", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers().useDescription(true)
                .useCallToAction(true)
                .setNeedToEditImage(false)
                .editTeaser();

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(false), "Fail - valid formats");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        log.info("Options - " + optionValues);
        softAssert.assertTrue(optionValues.contains("\"clEffects\": []"), "FAIL - effects c_fill");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4"), "link");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Интеграция Cloudinary. Cab. Редактирование тизера
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-24579">TA-24579</a>
     */
    @Test
    public void editTeaserWithCloudinaryInline() {
        log.info("Test is started");
        int teaserId = 1124;

        authCabAndGo("goodhits/ghits?id=" + teaserId);

        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MOUNT, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().loadImageCloudinaryInline("mount.jpg", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers()
                .useCallToAction(true)
                .setRotateValidFormats(true)
                .setImage("mount.jpg")
                .editImageBlock();

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        log.info("Check created teaser");

        pagesInit.getCabTeasers().openPopupEditImageInline();

        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(true), "Fail - valid formats");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        softAssert.assertTrue(optionValues.contains("\"clEffects\": [\"e_sharpen:100\", \"c_fill\", \"g_faces:auto\", \"f_jpg\", \"q_auto:good\"]"), "FAIL - effects cloudinary");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 | Database, pre-processing, creation form")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit teaser with video</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check edit teaser with video")
    public void editTeaserWithCloudinaryVideoInline() {
        log.info("Test is started");
        int teaserId = 1300;

        authCabAndGo("goodhits/ghits?id=" + teaserId);

        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MANAGERS_RESET_TRUE, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().loadImageCloudinaryInline("managers_croped.mov", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers()
                .useCallToAction(true)
                .setRotateValidFormats(true)
                .setNeedToEditImage(false)
                .editImageBlock();

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        log.info("Check created teaser");

        pagesInit.getCabTeasers().openPopupEditVideoInline();

        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(true), "Fail - valid formats");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);

        log.info("Options - " + optionValues);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);
        softAssert.assertTrue(optionValues.contains("\"clEffects\": []"), "FAIL - effects c_fill");
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4"), "link");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Интеграция Cloudinary. Cab. Редактирование тизера
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-24579">TA-24579</a>
     */
    @Test
    public void editTeaserWithCloudinaryInlineModeration() {
        log.info("Test is started");
        int teaserId = 1125;

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);

        log.info("Create teaser with response from cloudinary");
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(MOUNT, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasersModeration().openPopupEditImageInline();
        pagesInit.getCabTeasersModeration().loadImageCloudinaryInline("mount.jpg");
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);
        pagesInit.getCabTeasers()
                .useCallToAction(true)
                .setRotateValidFormats(true)
                .setImage("mount.jpg")
                .editImageBlock();

        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + teaserId);
        log.info("Check created teaser");

        pagesInit.getCabTeasersModeration().openPopupEditImageInline();

        softAssert.assertTrue(pagesInit.getCabTeasers().checkRotateValidFormats(true), "Fail - valid formats");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisableCrop("1-1"), "FAIL - 1-1");

        String optionValues = operationMySql.getgHits1().getOptionsValue(teaserId);
        softAssert.assertTrue(optionValues.contains("\"clEffects\": [\"e_sharpen:100\", \"c_fill\", \"g_faces:auto\", \"f_jpg\", \"q_auto:good\"]"), "FAIL - effects cloudinary");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Интеграция Cloudinary. Cab. Редактирование тизера
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-24579">TA-24579</a>
     */
    @Test
    public void checkPopupImageZionInlineVisibility() {
        log.info("Test is started");
        int teaserId = 1126;

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);

        log.info("Create teaser with response from cloudinary");
        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkImageVisibility(), "Fail - image visibility");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_FORMAT)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check active format using enable checkbox of rotate valid formats</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24577\">Ticket TA-24577</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check active format using enable checkbox of rotate valid formats")
    public void checkActiveImageFormatsUsingRotateValidFormatOption() {
        log.info("Test is started");
        int teaserId = 1127;

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        pagesInit.getCabTeasersModeration().enableRotateValidFormatsOnly(false);

        log.info("Check default crop");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isActiveCrop("16-9"), "FAIL - 16-9");

        log.info("Check crop 1-1");
        pagesInit.getCabTeasers().enableCropFormat("1-1");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isActiveCrop("1-1"), "FAIL - 1-1");

        log.info("Check crop 3-2");
        pagesInit.getCabTeasersModeration().enableCropFormat("3-2");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isActiveCrop("3-2"), "FAIL - 3-2");

        pagesInit.getCabTeasersModeration().enableRotateValidFormatsOnly(true);
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().clickImageCheckButton();

        log.info("Check crop 1-1");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisableCrop("1-1"), "FAIL - disable 1-1");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isSelectedRotateValidFormatsOnly(), "FAIL - rotate valid formats only");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Set manual focal point and check saved data to g_hits_1.options</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Set manual focal point and check saved data to g_hits_1.options create")
    public void checkFocalPointValuesCreateTeaser() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers()
                .useManualFocalPoint(true)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .setFocalPoint(144, 133)
                .useTitle(true)
                .setNeedToEditCategory(false)
                .useDescription(true)
                .createTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getCabTeasers().getTeaserId());

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_236\""), "x_236");
        softAssert.assertTrue(options.contains("\"y_299\""), "y_299");
        softAssert.assertAll();
        log.info("Test finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Set focal point edit teaser")
    public void checkEditionOfFocalPointEditTeaser() {
        log.info("Test is started");
        int teaserId = 1260;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        pagesInit.getCabTeasers()
                .useManualFocalPoint(true)
                .setFocalPoint(124, 143)
                .isGenerateNewValues(false)
                .useDescription(true)
                .setNeedToEditImage(false)
                .setImage("mount.jpg")
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_206\""), "x_206");
        softAssert.assertTrue(options.contains("\"y_284\""), "y_284");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check cancel focal point option for teaser edit")
    public void checkCancelFocalPointSettingsEditTeaser() {
        log.info("Test is started");
        int teaserId = 1260;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        pagesInit.getCabTeasers()
                .setImage("mount.jpg")
                .useManualFocalPoint(true)
                .setFocalPoint(124, 143)
                .setFocalPoint(false);

        pagesInit.getCabTeasers().cancelManualFocalPoint();

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .useManualFocalPoint(false)
                .useDescription(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check edit focal point on moderation status")
    public void checkEditFocalPointOnModeration() {
        log.info("Test is started");
        int teaserId = 5001;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        pagesInit.getCabTeasers()
                .useManualFocalPoint(true)
                .setFocalPoint(124, 143)
                .setDomain("https://testsite5000.com")
                .useDescription(true)
                .isGenerateNewValues(false);

        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_206\""), "x_206");
        softAssert.assertTrue(options.contains("\"y_284\""), "y_284");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check edit focal point of rejected teaser")
    public void checkEditingFocalPointOfRejectedTeaser() {
        log.info("Test is started");
        int teaserId = 5002;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        pagesInit.getCabTeasers()
                .useManualFocalPoint(true)
                .setFocalPoint(124, 143)
                .setDomain("https://testsite5000.com")
                .useDescription(true)
                .isGenerateNewValues(false);

        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"g_xy_center\""), "g_xy_center");
        softAssert.assertTrue(options.contains("\"x_206\""), "x_206");
        softAssert.assertTrue(options.contains("\"y_284\""), "y_284");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 1</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check set default settings for focal point, teasers on moderation")
    public void checkSetToDefaultSettingsOfFocalPointModerationStatus() {
        log.info("Test is started");
        int teaserId = 5012;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        String optionsDebug = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("optionsDebug value - " + optionsDebug);

        pagesInit.getCabTeasers().setFocalPointToDefault();

        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .useManualFocalPoint(false)
                .setDomain("https://testsite5000.com")
                .useDescription(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 2</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check set default settings for focal point, teasers on moderation")
    public void checkSetToDefaultSettingsOfFocalPointRejectedStatus() {
        log.info("Test is started");
        int teaserId = 5004;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);

        String optionsDebug = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("optionsDebug value - " + optionsDebug);

        pagesInit.getCabTeasers().setFocalPointToDefault();
        softAssert.assertTrue(pagesInit.getCabTeasers().getImageLinkValue().contains("Stonehenge.jpg"), "There is incorrect image link is displayed!");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .useManualFocalPoint(false)
                .setDomain("https://testsite5000.com")
                .useDescription(true)
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = g_faces:auto </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check displayed button for focal point settings")
    public void checkDisplayingButtonForFocalPointApprovedTeaserWithDefaultFocalPoint() {
        log.info("Test is started");
        int teaserId = 5005;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1.karantin = 0</li>\n" +
            "   <li>g_hits_1.options = \"g_xy_center\", \"x_###\", \"y_###\" </li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52182\">Ticket TA-52182</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check displayed button for focal point settings")
    public void checkDisplayingButtonForFocalPointApprovedTeaserWithCustomFocalPoint() {
        log.info("Test is started");
        int teaserId = 5006;
        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointDefaultButton(), "Fail - isDisplayedFocalPointDefaultButton");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check cancel edits of manual focal point</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check cancel edits of manual focal point")
    public void checkFocalPointCancelEditOption() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers()
                .loadImageCloudinary("happiness.jpeg", OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getCabTeasers().cancelManualFocalPoint();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .useTitle(true)
                .useDescription(true)
                .setNeedToEditCategory(false)
                .createTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getCabTeasers().getTeaserId());

        softAssert.assertFalse(options.contains("\"g_xy_center\""), "Fail - g_xy_center");
        softAssert.assertFalse(options.contains("\"x_2\""), "Fail - x_236");
        softAssert.assertFalse(options.contains("\"y_2\""), "Fail - y_299");
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");
        softAssert.assertTrue(options.contains("g_faces:auto"), "FAIL - g_faces cloudinary");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check reset data of manual focal point to default</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check reset data of manual focal point to default")
    public void checkResetFocalPointDataToDefault() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers()
                .loadImageCloudinary("happiness.jpeg", OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(true);
        pagesInit.getCabTeasers().setFocalPointToDefault();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        pagesInit.getCabTeasers().setNeedToEditImage(false)
                .useTitle(true)
                .useDescription(true)
                .setNeedToEditCategory(false)
                .createTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(pagesInit.getCabTeasers().getTeaserId());

        softAssert.assertFalse(options.contains("\"g_xy_center\""), "Fail - g_xy_center");
        softAssert.assertTrue(options.contains("\"e_sharpen:100\""), "e_sharpen:100");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit data of manual focal point </>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check edit data of manual focal point")
    public void checkEditFocalPointData() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers()
                .loadImageCloudinary("happiness.jpeg", OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(true);
        pagesInit.getCabTeasers().editDataFocalPoint();
        Assert.assertTrue(pagesInit.getCabTeasers().isDisplayedFocalPointIcon(), "Fail - isDisplayedFocalPointIcon");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit data of manual focal point inline. Teaser on moderation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check edit data of manual focal point inline. Teaser on moderation")
    public void checkEditFocalPointInline() {
        log.info("Test is started");
        int teaserId = 1261;
        authCabAndGo("goodhits/ghits/id/" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().loadImageCloudinaryInline("Stonehenge.jpg", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(100, 144).setFocalPoint(true);
        pagesInit.getCabTeasers().savePopupImageSettings();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertTrue(options.contains("g_xy_center"), "Fail - g_xy_center");
        softAssert.assertTrue(options.contains("x_171"), "Fail - x_171");
        softAssert.assertTrue(options.contains("y_283"), "Fail - y_283");
        softAssert.assertTrue(options.contains("e_sharpen:100"), "e_sharpen:100");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check forbid editing data of manual focal point inline. Approved teaser</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check forbid editing data of manual focal point inline approve teaser. Approved teaser")
    public void checkForbidEditFocalPointApprovedTeaserInline() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?id=" + 1262);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton first");

        pagesInit.getCabTeasers().loadImageCloudinaryInline("happiness.jpeg", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedFocalPointManualButton(), "Fail - isDisplayedFocalPointManualButton second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(FOCAL_POINT)
    @Stories({@Story(TEASER_EDIT_CAB_INTERFACE), @Story(TEASER_CREATE_CAB_INTERFACE)})
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check message of editing focal point not completed</>" +
            "   <li>Inline</>" +
            "   <li>Create interface</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52094\">Ticket TA-52094</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check message of editing focal point not completed")
    public void checkMessageOfEditingFocalPointNotCompleted() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?id=" + 1263);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        pagesInit.getCabTeasers().setFocalPoint(133, 144).setFocalPoint(false);
        pagesInit.getCabTeasers().savePopupImageSettings();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Please disable focal point first"), "Fail - message inline");

        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        pagesInit.getCabTeasers().loadImageCloudinary("happiness.jpeg", OWN_IMAGE);
        pagesInit.getCabTeasers().setFocalPoint(123, 124).setFocalPoint(false);
        pagesInit.getCabTeasers()
                .setNeedToEditImage(false)
                .useDescription(true)
                .setNeedToEditCategory(false)
                .createTeaser();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Please disable focal point first"), "Fail - message create interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_EXTENSIONS)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check presence of cloudinary property due changing image to gif</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52113\">Ticket TA-52113</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check presence of cloudinary property due changing image to gif")
    public void checkCloudinaryEffectDueChangingFromStaticImageToGif() {
        log.info("Test is started");
        int teaserId = 1265;

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("car_driving.gif", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers()
                .isGenerateNewValues(false)
                .useDescription(true)
                .setNeedToEditImage(false)
                .setImage("car_driving.gif")
                .editTeaser();

        String options = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("options value - " + options);
        softAssert.assertFalse(options.contains("e_sharpen:100"), "e_sharpen:100");
        softAssert.assertTrue(options.contains("\"clEffects\": []"), "clEffects");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_EXTENSIONS)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check message with false extension</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check message with false extension")
    public void checkValidationVideoExtension() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/1000");

        pagesInit.getCabTeasers().loadImageCloudinary("managers_croped.avi", OWN_IMAGE);
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("File 'managers_croped.avi' has a false extension"));

        log.info("Test is finished");
    }
}
