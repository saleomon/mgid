package cab.products.teasers;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static libs.dockerClient.base.DockerKafka.Topics.TEASER_AD_TYPE_MASS;
import static testData.project.CliCommandsList.Consumer.AD_TYPE_CHANGING;

public class CabTeasersRelatedImagesNewTests extends TestBase {

    private final int teaserId = 23;

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23912">https://youtrack.mgid.com/issue/TA-23912</a>
     * Добавить в массовые действия по "Related images new" смену категории
     * 1. Проверяем что при фильтре по "Related images new" в массовых действиях отображается "add type" и не отображается 'Edit category'
     * 2. Проверяем что при фильтре по "Related images new" и при выборе в "Campaign type" - product and push
     * в массовых действиях отображается "add type" и не отображается 'Edit category'
     * RKO
     */
    @Test(dataProvider = "editCategoryValueDontExistInMassAction_cases")
    public void editCategoryValueDontExistInMassAction(String url) {
        log.info("Test is started -> " + url);
        authCabAndGo(url);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkExistCustomValueInMassActions("typechange"), "FAIL -> mass action 'add type' isn't displayed");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkExistCustomValueInMassActions("catchange"), "FAIL -> mass action 'Edit category' is displayed");
        softAssert.assertAll();
        log.info("Test is finished -> " + url);
    }

    @DataProvider
    public Object[][] editCategoryValueDontExistInMassAction_cases(){
        return new Object[][]{
                {"goodhits/ghits/related_image_hashes/" + teaserId},
                {"goodhits/ghits/?campaign_types%5B%5D=product&campaign_types%5B%5D=push&related_image_hashes=" + teaserId}
        };
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23912">https://youtrack.mgid.com/issue/TA-23912</a>
     * Добавить в массовые действия по "Related images new" смену категории
     * Проверяем смену категории
     * RKO
     */
    @Test
    public void editCategoryInMassByRelatedImagesNew() {
        log.info("Test is started");
        try {
            serviceInit.getDockerCli().runConsumer(AD_TYPE_CHANGING);
            operationMySql.getgHits1().updateTeaserSendApprove(24, 25);
            authCabAndGo("goodhits/ghits/?campaign_types%5B%5D=product&related_image_hashes=" + teaserId);
            pagesInit.getCabTeasers().editCategoryInMass();
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("For 24, 25, 43 all changes have been applied."), "FAIL - checkCustomMessagesCab");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkCategoryIdInListInterface(pagesInit.getCabTeasers().getCategoryId()), "FAIL -> category don't correct");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/MT-4007">https://youtrack.mgid.com/issue/MT-4007</a>
     * Массовая смена ad_type в рамках системы для тизеров-дубликатов
     * Проверяем смену adType
     * RKO
     */
    @Test
    public void editAdTypeInMassByRelatedImagesNew() {
        log.info("Test is started");
        try {
            serviceInit.getDockerCli().runConsumer(AD_TYPE_CHANGING, "-vvv");
            operationMySql.getgHits1().updateTeaserSendApprove(24, 25);
            authCabAndGo("goodhits/ghits/campaign_id/1/related_image_hashes/" + teaserId);
            String customType = pagesInit.getCabTeasers().chooseRandomAdTypeButSome("b", "x");
            log.info("customType: " + customType);
            pagesInit.getCabTeasers().editAdTypeInMass(customType);
            helpersInit.getMessageHelper().isSuccessMessagesCab();
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(TEASER_AD_TYPE_MASS);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAdTypeInListInterface(pagesInit.getCabTeasers().getType()), "FAIL -> adType don't correct");
        softAssert.assertAll();
        log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }
}
