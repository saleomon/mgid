package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import libs.dockerClient.base.DockerKafka;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.randomNumbersInt;
import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.HAPPY_WOMAN;
import static libs.devtools.CloudinaryMock.ResponseWithImages.HAPPY_WOMAN1;
import static pages.cab.products.logic.CabTeasers.ApproveOption.APPLY_WITH_APPROVE;
import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static pages.cab.products.logic.CabTeasers.OwnershipOfMedia.OWN_IMAGE;
import static testData.project.CliCommandsList.Consumer.*;
import static testData.project.CliCommandsList.Cron.CAMPAIGN_SHOW_NAME_TEMP;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.cabLink;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabTeasers2Tests extends TestBase {

    private static final String limitError = "The %s campaign has reached its limit on the number of teasers. The available limit within the campaign is %s teasers. Clean up teasers that don't get impressions.";
    private static final String errorMessageSameDescription = "\n" +
            "Couldn't replace the title for the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s,%s";
    private static final String successMessageSameDescription = "Successfully replaced titles for the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s,%s";
    private static final String errorMessageForTwoTeasersWithDifferentCondition = "Successfully replaced titles for the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s\n" +
            "Couldn't replace the title for the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s";

    @Epic(TEASERS)
    @Features({@Feature(IMAGE_EXTENSIONS), @Feature(TITLE_AND_DESCRIPTION)})
    @Stories({@Story(TEASER_EDIT_CAB_INTERFACE), @Story(TEASER_CREATE_CAB_INTERFACE)})
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check create/edit/delete teaser with image gif</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52225\">Ticket TA-52225</a></li>\n" +
            "   <li>Allow the use of special characters  ™, ®, ° in titles and descriptions</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52409\">Ticket TA-52409</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check create/edit/delete teaser with image gif")
    public void createEditDeleteTeaserGif() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("space_journey.gif", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        log.info("create teaser");
        softAssert.assertNotEquals(pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .setNeedToEditImage(false)
                .setNeedToEditCategory(false)
                .useDiscount(true)
                .setTitle("™ New")
                .setDescription("Sign ®")
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        int teaserId = pagesInit.getCabTeasers().getTeaserId();

        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue create");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/happy_woman.mp4"), "FAIL - link create");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoLinksValue create");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "FAIL - getAnimationValue create");


        log.info("check created teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(RAC, ZPL, IAP, AUTOCONTROL, POLAND_REG_COMPLIANT), "FAIL - selected compliant required");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(5), "FAIL - amount of displayed countries");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInListInterface(), "FAIL -> CREATED teaser doesn't check in list interface");

        log.info("check created teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInCreateInterface(), "FAIL -> CREATED teaser doesn't check in edit interface");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN1, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinaryInline("car_driving.gif", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        log.info("edit created teaser");
        softAssert.assertTrue(pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .setImage("car_driving.gif")
                .setNeedToEditImage(false)
                .setNeedToEditCategory(true)
                .isGenerateNewValues(true)
                .setTitle("Sign ®")
                .setDescription("° Celsius")
                .editTeaser(), "FAIL -> teaser doesn't edit");

        log.info("optionVideoLinksValues - " + optionVideoLinksValues);
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue edit");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1 edit");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2 edit");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9 edit");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/happy_woman.mp4"), "Fail - link edit");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoLinksValu eedit");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "FAIL - getAnimationValue edit");

        log.info("check EDITED teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInListInterface(), "FAIL -> EDITED teaser doesn't check in list interface");

        log.info("check EDITED teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInCreateInterface(), "FAIL -> EDITED teaser doesn't check in edit interface");

        log.info("reject/delete teaser");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().rejectAndDeleteTeaser(pagesInit.getCabTeasers().getTeaserId()), "FAIL -> teaser doesnt reject or delete");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * change teasers with equals title
     * <p>
     * CASE(dataProvider):
     * 1. Product teasers - valid case (65 symbols)
     * <p>
     * 2. PUSH teasers - valid case (30 symbols)
     * RKO
     */
    @Test(dataProvider = "validDataTeasersForChangeMassTitle")
    public void checkChangeMassTitle(String teaserType, int teaser_1, int teaser_2, String teaserTitle, String teaserNewPartTitle, String message) {
        try {
            log.info("Test is started - " + teaserType);
        String changePartTitle = "Women";

        log.info("set equals title for teasers");
            operationMySql.getMailPull().getCountLettersByBody(message);
            operationMySql.getgHits1().updateTeaserTitleAndApprove(teaser_1, teaserTitle);
            operationMySql.getgHits1().updateTeaserTitleAndApprove(teaser_2, teaserTitle);

            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UPDATE_TITLES, "-vvv");

            authCabAndGo("goodhits/ghits/?keywordTitle=" + teaserTitle);
            pagesInit.getCabTeasers().changeMassTitle(changePartTitle, teaserNewPartTitle);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Teasers have been queued for partial title change"), "fail -> Success message doesnt right");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(DockerKafka.Topics.BULK_ACTION_UPDATE_TITLE);
            sleep(4000);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");

            authCabAndGo("goodhits/ghits/?keywordTitle=" + teaserTitle.replace(changePartTitle, teaserNewPartTitle));
            softAssert.assertEquals(pagesInit.getCabTeasers().getCountTeasersOnPage(), 2, "fail -> count teasers don't equals");

        softAssert.assertAll();
        log.info("Test is finished - " + teaserType);
    } finally {
        serviceInit.getDockerCli().stopConsumer();
    }
    }

    @DataProvider
    public Object[][] validDataTeasersForChangeMassTitle() {
        String messageOne = String.format(errorMessageSameDescription, 3, 4);
        String messageTwo = String.format(errorMessageSameDescription, 11, 12);
        return new Object[][]{
                {"product teaser", TEASER_MGID_1_APPROVE, TEASER_MGID_2_APPROVE, "Sexy Women Looking For {City} Men O", "Girl and her friend with great hope", messageOne},
                {"push teaser", TEASER_PUSH_1_APPROVE, TEASER_PUSH_2_APPROVE, "Sexy Women O", "Girl and her friend wit", messageTwo}
        };
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * change teasers with equals title
     * <p>
     * CASE(dataProvider):
     * 1. Product teasers - not valid case (66 symbols)
     * <p>
     * 2. Push teasers - not valid case (31 symbols)
     * RKO
     */
    @Flaky
    @Test(dataProvider = "notValidDataTeasersForChangeMassTitle")
    public void checkChangeMassTitleMoreThan_X_symbols(String teaserType, int teaser_1, int teaser_2, String teaserTitle, String teaserNewPartTitle, String message) {
        try {
            log.info("Test is started - " + teaserType);

        String changePartTitle = "Bird";

        log.info("set equals title for teasers");
        operationMySql.getMailPull().getCountLettersByBody(message);

        operationMySql.getgHits1().updateTeaserTitleAndApprove(teaser_1, teaserTitle);
        operationMySql.getgHits1().updateTeaserTitleAndApprove(teaser_2, teaserTitle);

        serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
        serviceInit.getDockerCli().runConsumer(BULK_ACTION_UPDATE_TITLES, "-vvv");

        authCabAndGo("goodhits/ghits/?keywordTitle=" + teaserTitle);
        pagesInit.getCabTeasers().changeMassTitle(changePartTitle, teaserNewPartTitle);
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(DockerKafka.Topics.BULK_ACTION_UPDATE_TITLE);

            sleep(4000);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");
            sleep(2000);

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Teasers have been queued for partial title change"), "fail -> Success message doesnt right");

        authCabAndGo("goodhits/ghits/?keywordTitle=" + teaserTitle.replace(changePartTitle, teaserNewPartTitle));
        softAssert.assertEquals(pagesInit.getCabTeasers().getCountTeasersOnPage(), 0, "fail -> count teasers don't equals");
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(message), "FAIL -> message");

        softAssert.assertAll();
        log.info("Test is finished - " + teaserType);
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @DataProvider
    public Object[][] notValidDataTeasersForChangeMassTitle() {
        int teaser1 = 1515;
        int teaser2 = 1516;
        int teaser3 = 1517;
        int teaser4 = 1518;
        String messageOne = String.format(errorMessageSameDescription, teaser1, teaser2);
        String messageTwo = String.format(errorMessageSameDescription, teaser3, teaser4);
        return new Object[][]{
                {"product teaser", teaser1, teaser2, "Sexy Bird Looking For {City} Men Ol", "Girl and her friend with great hope", messageOne},
                {"push teaser", teaser3, teaser4, "Sexy Bird Ol", "Girl and her friend wit", messageTwo}
        };
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * change teasers with notEquals title
     * <p>
     * RKO
     */
    @Test
    public void checkChangeMassTitle_notValidTitle() {
        try {
            log.info("Test is started");
        String changePartTitle = "Women";
        String newPartTitle = "Girls";
        int teaserId1 = 1511;
        int teaserId2 = 1512;

        log.info("set equals title for teasers");
        operationMySql.getMailPull().getCountLettersByBody(String.format(errorMessageForTwoTeasersWithDifferentCondition, teaserId1, teaserId2));

        operationMySql.getgHits1().updateTeaserTitleAndApprove(teaserId1, "Sexy Women Looking For {City} coffee Older Than 30");
        operationMySql.getgHits1().updateTeaserTitleAndApprove(teaserId2, "Sexy Babes Looking For {City} coffee Older Than 30");
        serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
        serviceInit.getDockerCli().runConsumer(BULK_ACTION_UPDATE_TITLES, "-vvv");

        authCabAndGo("goodhits/ghits/?keywordTitle=" + "Looking For {City} coffee Older Than 30");
        pagesInit.getCabTeasers().changeMassTitle(changePartTitle, newPartTitle);
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(DockerKafka.Topics.BULK_ACTION_UPDATE_TITLE);
            sleep(4000);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");
            sleep(2000);

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(errorMessageForTwoTeasersWithDifferentCondition, teaserId1, teaserId2)), "FAIL -> message");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Teasers have been queued for partial title change"), "fail -> Success message doesnt right");

        authCabAndGo("goodhits/ghits/?keywordTitle=" + "Sexy Girls Looking For {City} coffee Older Than 30");
        softAssert.assertEquals(pagesInit.getCabTeasers().getCountTeasersOnPage(), 1, "fail -> count teasers don't equals");

        softAssert.assertAll();
        log.info("Test is finished");
    } finally {
        serviceInit.getDockerCli().stopConsumer();
    }
}

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * change teasers with Equals title - one teaser onModeration
     * RKO
     */
    @Test
    @Privilege(name = "goodhits/ghits_can_edit_teasers_on_karantin")
    public void checkChangeMassTitle_oneTeaserOnModeration() {
        log.info("Test is started");
        try {
            String changePartTitle = "Women";
            String newPartTitle = "Girls";
            int teaserId1 = 1513;
            int teaserId2 = 1514;

            operationMySql.getMailPull().getCountLettersByBody(String.format(errorMessageForTwoTeasersWithDifferentCondition, teaserId1, teaserId2));
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits_can_edit_teasers_on_karantin");

            log.info("set equals title for teasers");
            operationMySql.getgHits1().updateTeaserTitleAndApprove(teaserId1, "Sexy Women Looking For {City} table Older Than 30");
            operationMySql.getgHits1().updateTeaserTitle(teaserId2, "Sexy Women Looking For {City} table Older Than 30");
            operationMySql.getgHits1().updateTeaserModerationStatus(teaserId2, 1);

            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UPDATE_TITLES, "-vvv");

            authCabForCheckPrivileges("goodhits/ghits/?keywordTitle=" + "Looking For {City} table Older Than 30");
            pagesInit.getCabTeasers().changeMassTitle(changePartTitle, newPartTitle);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Teasers have been queued for partial title change"), "fail -> Success message doesnt right");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(DockerKafka.Topics.BULK_ACTION_UPDATE_TITLE);
            sleep(4000);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");
            sleep(2000);
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(errorMessageForTwoTeasersWithDifferentCondition, teaserId1, teaserId2)), "FAIL -> message");

            authCabAndGo("goodhits/ghits/?keywordTitle=" + "Sexy Girls Looking For {City} table Older Than 30");
            softAssert.assertEquals(pagesInit.getCabTeasers().getCountTeasersOnPage(), 1, "fail -> count teasers don't equals");

            softAssert.assertAll();
        } finally {
            serviceInit.getDockerCli().stopConsumer();
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits_can_edit_teasers_on_karantin");
        }
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * check change mass title with empty base fields
     * <p>
     * RKO
     */
    @Test
    public void checkChangeMassTitle_emptyBaseFields() {
        log.info("Test is started");
        log.info("set equals title for teasers");

        operationMySql.getgHits1().updateTeaserTitleAndApprove(TEASER_MGID_1_APPROVE, "Sexy Women Looking For {City} Men Older Than 30");
        operationMySql.getgHits1().updateTeaserTitleAndApprove(TEASER_MGID_2_APPROVE, "Sexy Women Looking For {City} Men Older Than 30");

        authCabAndGo("goodhits/ghits/?keywordTitle=" + "Looking For {City} Men Older Than 30");
        pagesInit.getCabTeasers().changeMassTitle("", "");

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("To apply an action, specify the word or phrase you want to replace"), "fail -> alert doesnt right");
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23807">https://youtrack.mgid.com/issue/TA-23807</a>
     * Изменение тайтлов тизеров массовыми действиями
     * check change mass title the value equals description
     * <p>
     * RKO
     */
    @Test
    public void checkChangeMassTitle_NewPartTitleEqualsDescription() {
        log.info("Test is started");
        try {
            int oneTeaser = 1519;
            int twoTeaser = 1520;
            log.info("set equals title for teasers");
            operationMySql.getMailPull().getCountLettersByBody(String.format(errorMessageSameDescription, oneTeaser, twoTeaser));
            operationMySql.getgHits1().updateTeaserTitleAndApprove(oneTeaser, "Sexy Women Watching For {City} Men Older Than 30");
            operationMySql.getgHits1().updateTeaserTitleAndApprove(twoTeaser, "Sexy Women Watching For {City} Men Older Than 30");
            operationMySql.getgHits1().updateTeaserDescription(oneTeaser, "Sexy Girls Watching For {City} Men Older Than 30");
            operationMySql.getgHits1().updateTeaserDescription(twoTeaser, "Sexy Girls Watching For {City} Men Older Than 30");
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");

            authCabAndGo("goodhits/ghits/?keywordTitle=" + "Watching For {City} Men Older Than 30");
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UPDATE_TITLES, "-vvv");
            pagesInit.getCabTeasers().changeMassTitle("Women", "Girls");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(DockerKafka.Topics.BULK_ACTION_UPDATE_TITLE);
            sleep(4000);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");

            Assert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(errorMessageSameDescription, oneTeaser, twoTeaser)), "FAIL -> message");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23821">https://youtrack.mgid.com/issue/TA-23821</a>
     * Разделить маркировку Brand и Whitehat
     * <p>
     * NIO
     */
    @Test
    public void checkDisplayingTierOption() {
        log.info("Test is started");
        String[] tierOption = new String[]{"brand", "whitehat"};
        int index = randomNumbersInt(tierOption.length);
        int campaignId = 1043;

        log.info("Create campaign with tier option - " + tierOption[index]);
        operationMySql.getgPartners1().updateTierOption(campaignId, tierOption[index]);

        log.info("Check tier option");
        authCabAndGo("goodhits/on-moderation?campaign_id=" + campaignId);
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkDisplayingTierOption(tierOption[index]));

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23900">https://youtrack.mgid.com/issue/TA-23900</a>
     * <p>
     * Check correct displaying teaser link with two domains (second domain such a parameter value) in Cab teasers list
     * AIA
     */
    @Test
    public void checkTeaserUrlWithTwoDomains() {
        log.info("Test is started");
        int teaserId = 21;
        String teaserUrl = "https://testsitegoodsmg.com/?lp=https://h9tkd.rdtk.io/5ec5229ca3445e00016c02a3?var5=PUSHMGID&var6={click_id}&var7={widget_id}&var8={teaser_id}&click_id={click_id}";
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().checkTeasersUrl(teaserUrl);
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/BT-1442">https://youtrack.mgid.com/issue/BT-1442</a>
     * Добавить возможность загрузки иконки в форме создания/редактирования пуш рк в админке
     * NIO
     */
    @Test(dataProvider = "dataImageForTeasers")
    public void checkFieldValidationForPushIcon(String typeCampaign, String filePath, String textError) {
        log.info("Test is started - " + filePath);
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().fillFieldValidationForPushIcon(typeCampaign, filePath);
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(textError));
        log.info("Test is finished - " + filePath);
    }

    @DataProvider
    public Object[][] dataImageForTeasers() {
        return new Object[][]{
//                // toDo NIO https://jira.mgid.com/browse/BT-5482 {"push", "http://tmp.dt07.net/video/test/kr/im/bmp.bmp", "File '5a6a72ad0e4ba0b9d98cad85c615451b.bmp' has a false extension"},
                {"push", LINK_TO_RESOURCES_IMAGES + "38.jpg", "Minimum expected width for image '38.jpg' should be '328' but '64' detected,Minimum expected height for image '38.jpg' should be '328' but '64' detected"},
        };
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check advert validation with brand campaign option create interface
     * NIO
     */
    @Test
    public void checkValidationAdvertFieldAtCreateInterface() {
        log.info("Test is started");
        int cammpaignId = 13;
        authCabAndGo("goodhits/ghits-add/campaign_id/" + cammpaignId);

        log.info("create teaser");
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(false)
                .useCallToAction(true)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                        .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        log.info("Check error message advert validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Value is required and can't be empty"), "FAIL - Advert Text Validation");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check call to action validation with brand campaign option create interface
     * NIO
     */
    @Test
    public void checkValidationCallToActionFieldAtCreateInterface() {
        log.info("Test is started");
        int cammpaignId = 13;
        authCabAndGo("goodhits/ghits-add/campaign_id/" + cammpaignId);
        log.info("create teaser");
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(true)
                .useCallToAction(false)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                        .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        log.info("Check error message call to action validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Value is required and can't be empty"), "FAIL - Call TO Action Validation");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check advert validation with brand campaign option edit interface
     * NIO
     */
    @Test
    public void checkValidationAdvertFieldAtEditInterface() {
        log.info("Test is started");
        int teaserId = 26;
        authCabAndGo("goodhits/ghits-edit/id/" + teaserId);

        log.info("edit teaser");
        softAssert.assertFalse(pagesInit.getCabTeasers()
                .isGenerateNewValues(false)
                .useDescription(false)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .useCallToAction(true)
                .editTeaser(), "FAIL -> teaser doesn't create");

        log.info("Check error message advert validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Value is required and can't be empty"), "FAIL - Advert Text Validation");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check advert and call to action validation inline actions
     * NIO
     */
    @Test
    public void checkValidationAdvertAndCallToActionInlineListInterface() {
        log.info("Test is started");
        int teaserId = 27;
        authCabAndGo("goodhits/ghits/?id=" + teaserId);

        log.info("edit advert");
        pagesInit.getCabTeasers().setDescription(" ");
        pagesInit.getCabTeasers().editDescriptionInline();
        log.info("Check error message advert validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Advert text is less than 1 characters long"), "FAIL - Advert Text Validation");

        log.info("edit call to action");
        pagesInit.getCabTeasers().setCallToAction(" ");
        pagesInit.getCabTeasers().editCallToActionInline();
        log.info("Check error message call to action validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Call to Action can not be empty"), "FAIL - Call to action Text Validation");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check right of call to action editing inline
     * NIO
     */
    @Test
    @Privilege(name = "goodhits/can_edit_teaser_call_to_action")
    public void checkPossibilityOfEditingCallOfAction() {
        try {
            log.info("Test is started");
            int teaserId = 27;
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_teaser_call_to_action");
            authCabForCheckPrivileges("goodhits/ghits/?id=" + teaserId);
            log.info("check posibility of editing");
            softAssert.assertFalse(pagesInit.getCabTeasers().checkPossibilityOfEditingCallToAction(), "FAIL - teaser list interface");
            authCabAndGo("goodhits/on-moderation/id/" + teaserId);
            log.info("check posibility of editing quarantine");
            softAssert.assertFalse(pagesInit.getCabTeasers().checkPossibilityOfEditingCallToAction(), "FAIL - teaser list interface quarantine");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_teaser_call_to_action");
        }
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23893">https://youtrack.mgid.com/issue/TA-23893</a>
     * Поиска тизеров по продукту
     * - check finding product by product name
     * - check finding product by product name and 'show certified' flag
     * NIO
     */
    @Test
    public void checkProductFilter() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/");
        pagesInit.getCabTeasers().filterTeasersByProduct(false, "Bustiere");
        log.info("Check filter product");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkFilterResult("1"), "FAIL - Check without checkbox");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(20, "Viet Nam");
        pagesInit.getCabTeasers().filterTeasersByProduct(true, "Something");
        log.info("Check filter product with certify");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkFilterResult("20", "1003"), "FAIL - Check with checkbox");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Story| Провайдер фида для кампаний Native-to-Search | Кампании</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51741">Ticket TA-51741</a>
     * <p>NIO</p>
     */
    @Test
    public void checkChangingFeedsProviderForAllTeasers() {
        log.info("Test is started");
        String provider = "drink-milk.net";

        authCabAndGo("goodhits/ghits/?campaign_id=1117");

        softAssert.assertEquals(
                pagesInit.getCabTeasers().getAmountOfTeasers(),
                pagesInit.getCabTeasers().getAmountOfTeasersWithFeedsProvider("Bing"), "Fail - start condition");

        pagesInit.getCabTeasers().editFeedsProviderInline(provider);

        softAssert.assertEquals(
                pagesInit.getCabTeasers().getAmountOfTeasers(),
                pagesInit.getCabTeasers().getAmountOfTeasersWithFeedsProvider(provider), "Fail - final condition");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Story| Провайдер фида для кампаний Native-to-Search | Кампании</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51741">Ticket TA-51741</a>
     * <p>NIO</p>
     */
    @Test
    public void checkFilterByFeedsProvider() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits");
        pagesInit.getCabTeasersModeration().filterByFeedsProvider("Yahoo");

        Assert.assertEquals(
                pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers(),
                pagesInit.getCabTeasersModeration().getAmountOfTeasersWithFeedsProvider("Yahoo"));

        log.info("Test is finished");
    }

    @Story("Валидация макросов")
    @Description("Валидация макросов {gdpr} и {gdpr_consent}\n" +
            "<ul>\n" +
            "   <li>при создании тизера</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51808\">Ticket TA-51808</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание тизера для кампании с макросами {gdpr} и {gdpr_consent}")
    public void createTeaserWithGdprMacros() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add/campaign_id/" + 2008);

        log.info("create teaser");
        softAssert.assertNotEquals(pagesInit.getCabTeasers()
                .useTitle(true)
                .setDomain("gdpr-macroses-test.com")
                        .setNeedToEditCategory(false)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        log.info("check created teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());

        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserLinkParamsInListInterface(),
                "gdpr={gdpr}&gdpr_consent={gdpr_consent}",
                "FAIL -> teasers link without GDPR macroses in list interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_VALIDATION)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check image validation - width, height, size</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51863\">Ticket TA-51863</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check image validation - width, height, size")
    public void checkImageValidationWidthHeightSize() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers().loadImageCloudinary("492_327_validation1.jpeg", OWN_IMAGE);
        log.info("Check error message about HEIGHT");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum expected height for image '492_327_validation1.jpeg' should be '328' but '327' detected"), "geeraf_740x380");

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers().loadImageCloudinary("491_328_validation2.jpeg", OWN_IMAGE);
        log.info("Check error message about WIDTH");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum expected width for image '491_328_validation2.jpeg' should be '492' but '491' detected"), "aurora_500x400");

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        pagesInit.getCabTeasers().loadImageCloudinary("leo.gif", OWN_IMAGE);
        log.info("Check error message about size");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Maximum allowed size for file 'leo.gif' is '5MB' but '7.16MB' detected"), "leo.gif");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Создание, Редактирование вывод анимированных тизеров в кабе")
    @Description("Check preview teaser with:\n" +
            "   <li>gif</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51863\">Ticket TA-51863</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check preview teaser with gif")
    public void checkPreviewTeaserWithGif() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=" + 1182);
        open(pagesInit.getCabTeasers().getLinkForPreviewTeaserInWidget());

        Assert.assertTrue(pagesInit.getTeaserClass().isDisplayedImageFrame(),
                "FAIL -> Displaying teaser!");
        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Создание, Редактирование вывод анимированных тизеров в кабе")
    @Description("Check preview teaser with:\n" +
            "   <li>video</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52144\">Ticket TA-52144</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check preview teaser with video")
    public void checkPreviewTeaserWithVideo() {
        log.info("Test is started");
        String expectedVideoLink = "http://local-dashboard.mgid.com/imgh/video/upload/ar_16:9,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/managers_croped.mp4";
        authCabAndGo("goodhits/ghits/?id=" + 1297);
        open(pagesInit.getCabTeasers().getLinkForPreviewTeaserInWidget());

        Assert.assertEquals(pagesInit.getTeaserClass().getVideoLink(), expectedVideoLink,
                "FAIL -> Displaying teaser!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature("Image Extensions")
    @Story("Create Interface Cab")
    @Description("Inline editing with animation\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51863\">Ticket TA-51863</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Inline editing with animation")
    public void checkInlineEditImageWithAnimation() {
        log.info("Test is started");
        int teaserId = 1183;
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinaryInline("space_journey.gif", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers().savePopupImageSettings();

        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);
        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue create");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/happy_woman.mp4"), "link create");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoLinksValue create");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "FAIL - getAnimationValue create");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Создание и редактирование тизера в кабе с middot в середине тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактирование тизера в кабе с middot в середине тайтла и дескрипшена")
    public void checkCreateEditTeaserWithMiddotInTheMiddleOfTitleAndDescriptionInCab() {
        log.info("Test is started");
        int campaignId = 2014;
        String teasersTitle = "Title with one middot·in the middle!";
        String teasersDescription = "Description with one middot·in the middle!";
        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .setTitle(teasersTitle)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .setNeedToEditCategory(false)
                .setDescription(teasersDescription)
                .createTeaser();

        int teaserId = pagesInit.getCabTeasers().getTeaserId();

        log.info("Check created teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersListInterface(),
                "FAIL -> Title with middot has failed check in teasers list after creating!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersListInterface(),
                "FAIL -> Description with middot has failed check in teasers list after creating!");

        log.info("Check created teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersEditInterface(),
                "FAIL -> Title with middot has failed check in edit interface after creating!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersEditInterface(),
                "FAIL -> Description with middot has failed check in edit interface after creating!");

        teasersTitle = "Title·with·middots·in·the·middle!";
        teasersDescription = "Description·with·middots·in·the·middle!";

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinary("car_driving.gif", OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        log.info("Edit created teaser");
        softAssert.assertTrue(pagesInit.getCabTeasers().setImage("car_driving.gif")
                .setNeedToEditImage(false)
                .useTitle(true)
                .useDescription(true)
                .setTitle(teasersTitle)
                .setNeedToEditCategory(true)
                .setDescription(teasersDescription)
                .editTeaser(), "FAIL -> teaser editing");

        log.info("Check EDITED teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersListInterface(),
                "FAIL -> Title with middot has failed check in teasers list after editing!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersListInterface(),
                "FAIL -> Description with middot has failed check in teasers list after editing!");

        log.info("Check EDITED teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersEditInterface(),
                "FAIL -> Title with middot has failed check in edit after editing!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersEditInterface(),
                "FAIL -> Description with middot has failed check in edit interface after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Проверка ошибки при создании тизера в кабе с middot в конце тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка ошибки при создании тизера в кабе с middot в конце тайтла и дескрипшена")
    public void checkCreateTeaserWithMiddotAtTheEndOfTitleAndDescriptionInCab() {
        log.info("Test is started");
        int campaignId = 2014;
        String teasersTitle = "Title with middot at the end·";
        String teasersDescription = "Description with middot at the end·";
        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        pagesInit.getCabTeasers()
                .useTitle(true)
                .setTitle(teasersTitle)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .setNeedToEditCategory(false)
                .createTeaser();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Title with middot has failed check in teasers list after creating!");

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        pagesInit.getCabTeasers()
                .resetAllVariables()
                .useDescription(true)
                .setDescription(teasersDescription)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .createTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Description with middot has failed check in teasers list after creating!");
        softAssert.assertAll();
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Проверка ошибки при редактировании тизера в кабе с middot в конце тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка ошибки при редактировании тизера в кабе с middot в конце тайтла и дескрипшена")
    public void checkEditTeaserWithMiddotAtTheEndOfTitleAndDescriptionInCab() {
        log.info("Test is started");
        int teaserId = 2008;
        String teasersTitle = "Title with middot at the end·";
        String teasersDescription = "Description with middot at the end·";
        authCabAndGo("goodhits/ghits-edit/id/" + teaserId);
        pagesInit.getCabTeasers()
                .useDescription(true)
                .setTitle(teasersTitle)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .setDescription(teasersDescription)
                .editTeaser();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Title with middot has failed check in teasers list after creating!");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Description with middot has failed check in teasers list after creating!");
        softAssert.assertAll();
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Инлайн редактирование тизера в кабе с middot в середине тайтла и дескрипшена\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Инлайн редактирование тизера в кабе с middot в середине тайтла и дескрипшена")
    public void checkEditTeaserWithMiddotInTheMiddleOfTitleAndDescriptionInlineInCab() {
        log.info("Test is started");
        int teaserId = 2009;
        String teasersTitle = "Title's with one middot·in the middle!";
        String teasersDescription = "Description with one middot·in the middle!";
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().editTitleInline(teasersTitle);

        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersListInterface(),
                "FAIL -> Title with middot has failed check in teasers list after editing inline!");
        pagesInit.getCabTeasers().setDescription(teasersDescription);
        pagesInit.getCabTeasers().editDescriptionInline();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersListInterface(),
                "FAIL -> Description with middot has failed check in teasers list after editing inline!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Add middot symbol as valid in the title and description of the teaser")
    @Description("Инлайн редактирование тайтла и дескрипшена тизера в кабе с middot в конце текста\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51951\">Ticket TA-51951</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Инлайн редактирование тайтла и дескрипшена тизера в кабе с middot в конце текста")
    public void checkEditTeaserWithMiddotAtTheEndOfTitleAndDescriptionInlineInCab() {
        log.info("Test is started");
        int teaserId = 2009;
        String teasersTitle = "Title with one middot at the end·";
        String teasersDescription = "Description with one middot at the end·";
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().editTitleInline(teasersTitle);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Title with middot at the end has failed checks after editing inline!");
        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getCabTeasers().setDescription(teasersDescription);
        pagesInit.getCabTeasers().editDescriptionInline();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(
                "It is prohibited to use punctuation and special characters at the end of a headline, except for (!?% \" '}])+$(currency characters))"),
                "FAIL -> Description with middot at the end has failed checks after editing inline!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP")
    @Story("Copyright certificate LP | V 1.1 | Viewing results")
    @Description("Check copyleaks matching result\n" +
            "     <ul>\n" +
            "      <li>Check main screenshot info</li>\n" +
            "      <li>Check percentage</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52020\">Ticket TA-52020</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check copyleaks matching result")
    public void checkCopyLeaksMatchingResult() {
        log.info("Test is started");
        List<String> mainScreenshot = Arrays.asList(cabLink +"goodhits/ghits-screenshots-history/id/1210/activeScreenshot/77",
                "https://imghosts.com/snapshot/snapshot/501584c344e23b406640be0f232f8a2d.jpeg");
        String percentage = "in progress";

        authCabAndGo("goodhits/ghits?campaign_id=1247&id=1210");

        pagesInit.getCabTeasers().openCopyLeaksScreenshotPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkMainScreenshot(mainScreenshot), "FAIL - checkMainScreenshot");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAllPercentage(percentage), "FAIL - checkAllPercentage");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP")
    @Story("Copyright certificate LP | V 1.1 | Viewing results")
    @Description("Check privilege copyleaks matching icon\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52020\">Ticket TA-52020</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Privilege(name="ghits-get-plagiarism-check-result")
    @Test (description = "Check privilege copyleaks matching icon")
    public void checkPrivilegeCopyLeaksPlagiarism() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-get-plagiarism-check-result");

        authCabForCheckPrivileges("goodhits/ghits?campaign_id=1245&id=1208");

        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedPlagiarismIcon());
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check 'Advertiser name' for teaser in list</>" +
            "   <li>Check approve teaser with 'Advertiser name' in list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check 'Advertiser name' and approve teaser with it in list")
    public void checkAdvertiserNameInTeasersList() {
        log.info("Test is started");
        String teaserId = "2018";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName(teaserId), "Advertiser name");
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();
        Assert.assertFalse(pagesInit.getCabTeasers().checkVisibilityApproveIcon());
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check temporary 'Advertiser name' for teaser in list</>" +
            "   <li>Change temporary 'Advertiser name' in list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check 'Advertiser name' and approve teaser with it in list")
    public void checkTemporaryAdvertiserNameInList() {
        log.info("Test is started");
        String teaserId = "2025";
        String newAdvertiserName = "New advertiser name";
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserNameTemp(teaserId), "Temp advertiser name",
                "FAIL -> Advertiser name!");
        pagesInit.getCabTeasers().changeAdvertiserName(teaserId, newAdvertiserName);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserNameTemp(teaserId), newAdvertiserName,
                "FAIL -> Changed Advertiser name!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check teaser without 'Advertiser name' in list</>" +
            "   <li>Set 'Advertiser name' in list</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check teaser without 'Advertiser name' in list and change it")
    public void checkTeaserWithoutAdvertiserNameInList() {
        log.info("Test is started");

        String teaserId = "2016";
        String secondTeaserId = "2017";
        String newAdvertiserName = "New advertiser name";
        authCabAndGo("goodhits/on-moderation?campaign_id=2019");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserNameTemp(teaserId), "",
                "FAIL -> Advertiser name!");
        pagesInit.getCabTeasers().changeAdvertiserName(teaserId, newAdvertiserName);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserNameTemp(secondTeaserId), newAdvertiserName,
                "FAIL -> Changed Advertiser name!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check approve teaser without 'Advertiser name' in list</>" +
            "   <li>Check error message for approve teaser without advertiser name</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check error message for approve teaser without advertiser name")
    public void checkApproveTeaserWithoutAdvertiserName() {
        log.info("Test is started");
        String teaserId = "2019";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName(teaserId), "",
                "FAIL -> Empty advertiser name!");
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();
        Assert.assertEquals(helpersInit.getMessageHelper().getCustomMessageValueCab("Field \"Advertiser Name\" is required for approve ad"),
                "Field \"Advertiser Name\" is required for approve ad",
                "FAIL -> Approve teaser without Advertiser name error!");
        log.info("Test is finished");
    }
    
    @Epic("Campaigns")
    @Feature("Advertiser Name")
    @Story("Ad Name | Сampaigns with Content and Push types")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check set 'Advertiser name' from advertiser_name_temp after approving'</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52161\">Ticket TA-52161</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Check set 'Advertiser name' from advertiser_name_temp after approving")
    public void checkSetAdvertiserNameFromTempAfterApproveTeaser() {
        log.info("Test is started");
        String teaserId = "1316";
        String campaignId = "1307";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().checkVisibilityApproveIcon(), "Fail - checkVisibilityApproveIcon");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignAdvertiserName(campaignId), "Neptune", "Fail - getTeasersAdvertiserName");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Campaigns")
    @Feature("Advertiser Name")
    @Story("Ad Name | Сampaigns with Content and Push types")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check edit 'Advertiser name' approved teaser</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52161\">Ticket TA-52161</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Test(description = "Check edit 'Advertiser name' approved teaser")
    public void checkEditingAdvertiserNameInline() {
        log.info("Test is started");
        String teaserId = "1317";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasers().isEditableAdvertiserNameField(teaserId));
        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Закрити можливість додавання gif у Push тизерах")
    @Description("Check possibility to create and edit push teaser with gif\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52114\">Ticket TA-52114</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check possibility to create and edit push teaser with gif")
    public void createPushTeaserWithAnimation() {
        log.info("Test is started");

        int campaignId = 1266;
        int teaserId = 1267;

        log.info("Check load gif create interface");
        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        pagesInit.getCabTeasers().loadImageCloudinary("car_driving.gif", OWN_IMAGE);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Animated images for push campaigns are not allowed"), "FAIL - create teaser");

        log.info("Check load gif edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + teaserId);
        pagesInit.getCabTeasers().loadImageCloudinary("car_driving.gif", OWN_IMAGE);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Animated images for push campaigns are not allowed"), "FAIL - edit teaser");

        log.info("Check load gif edit inline interface");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().openPopupEditImageInline();
        pagesInit.getCabTeasers().loadImageCloudinaryInline("car_driving.gif", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Animated images for push campaigns are not allowed"), "FAIL - inline teaser");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Check privileges automoderation\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Privilege(name={"toggle-moderation-verification-flag", "can_see_filter_auto_moderated_not_need_verification", "can_see_filter_auto_moderated"})
    @Test (description = "Check privileges automoderation")
    public void checkRightsAutoModeration() {
        log.info("Test is started");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/toggle-moderation-verification-flag");
        authCabForCheckPrivileges("goodhits/ghits");
        pagesInit.getCabTeasers().filterByAutoModeration("allAutoApproved");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - On");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedVerifyIcon("Off"), "Fail - Off");


        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_filter_auto_moderated_not_need_verification");
        authCabForCheckPrivileges("goodhits/ghits");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedNotNeedVerificationOption(), "Fail - isDisplayedNotNeedVerificationOption");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_filter_auto_moderated");
        authCabForCheckPrivileges("goodhits/ghits");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedAutoModerationFilter(), "Fail - isDisplayedAutoModerationFilter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Filters")
    @Story("Filter by moderator action")
    @Description("Check filter be moderation action\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52163\">Ticket TA-52163</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check filter be moderation action")
    public void checkFilterByModerationAction() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?campaign_id=1280");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("moderatorActionsBox", "Approved");
        pagesInit.getCabTeasers().pressFilterButton();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedTeasersWithIds("1293", "1294"), "Fail - Approved");

        authCabAndGo("goodhits/ghits?campaign_id=1280");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("moderatorActionsBox", "Reject");
        pagesInit.getCabTeasers().pressFilterButton();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedTeasersWithIds("1287", "1288"), "Fail - Reject");

        authCabAndGo("goodhits/ghits?campaign_id=1280");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("moderatorActionsBox", "Verification");
        pagesInit.getCabTeasers().pressFilterButton();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedTeasersWithIds("1289", "1292"), "Fail - Verification");

        authCabAndGo("goodhits/ghits?campaign_id=1280");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("moderatorActionsBox", "Mass approve");
        pagesInit.getCabTeasers().pressFilterButton();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedTeasersWithIds("1285", "1286"), "Fail - Mass approve");

        authCabAndGo("goodhits/ghits?campaign_id=1280");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("moderatorActionsBox", "Mass reject");
        pagesInit.getCabTeasers().pressFilterButton();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedTeasersWithIds("1290", "1291"), "Fail - Mass reject");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Ad Name")
    @Feature("Ad Name in campaigns by offers")
    @Story("Teaser list interface")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Related teasers with same offers</>\n" +
            "   <li>Related teasers with different offers</>\n" +
            "   <li>Related teasers without offers</>\n" +
            "   <li>Without related teasers</>\n" +
            "   <li>With campaign type differ product</>\n" +
            "   <li>With subnet differ mgid/adskeeper</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52160\">Ticket TA-52160</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    //toDo NIO https://jira.mgid.com/browse/CP-2242 @Test(description = "Check fill Ad Name field through similar lp hashes")
    public void checkFillingAdvertiserNameThroughLpHashes() {
        log.info("Test is started");

        serviceInit.getDockerCli().runAndStopCron(CAMPAIGN_SHOW_NAME_TEMP, "-vvv");

        authCabAndGo("goodhits/ghits?related_lp_hashes=" + 1324 + "&activePartner=active");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 3, "Fail - amount 1324");
        log.info("Two related teaser with the same offer. Check field value - should be offer name related teasers");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignShowNameTemp("1318"), "AdName+Offer1","Fail - 1318");

        authCabAndGo("goodhits/ghits?related_lp_hashes=" + 1327 + "&activePartner=active");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 2, "Fail - amount 1327");
        log.info("Two related teaser with different offer. Check field value - should be name of category of teaser");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignShowNameTemp("1321"), "Events Hub","Fail - 1321");

        authCabAndGo("goodhits/ghits?related_lp_hashes=" + 1330 + "&activePartner=active");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 1, "Fail - amount 1330");
        log.info("One related teaser without offer. Check field value - should be name of category of teaser");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignShowNameTemp("1324"), "Events Hub","Fail - 1324");

        authCabAndGo("goodhits/ghits?related_lp_hashes=" + 1332 + "&activePartner=active");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 0, "Fail - amount 1332");
        log.info("No related teasers. Check field value - should be name of category of teaser");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignShowNameTemp("1326"), "Events Hub","Fail - 1326");

        log.info("Campaign type not included to cron logic. Check field value - should be null");
        softAssert.assertNull(operationMySql.getgPartners1().selectCampaignShowNameTemp("1327"),"Fail - 1327");

        log.info("Subnet not included to cron logic. Check field value - should be name of category of teaser");
        softAssert.assertNull(operationMySql.getgPartners1().selectCampaignShowNameTemp("1328"), "Fail - 1328");

        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.suspected_of_cloaking = 0</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52353\">Ticket TA-52353</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state suspected of cloaking mass")
    public void checkChangeStateSuspectedOfCloakingMass() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?campaign_id=1414");
        pagesInit.getCabTeasers().changeStateSuspectCloakingMass("suspectedOfCloakingOn");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Red"), "Fail - icon Red");

        authCabAndGo("goodhits/ghits?campaign_id=1414");
        pagesInit.getCabTeasers().changeStateSuspectCloakingMass("suspectedOfCloakingOff");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Black"), "Fail - icon Black");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>g_hits_1.suspected_of_cloaking = 0</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52353\">Ticket TA-52353</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check change state suspected of cloaking")
    public void checkChangeStateSuspectedOfCloaking() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?campaign_id=1415");
        pagesInit.getCabTeasers().changeStateSuspectCloaking();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Red"), "Fail - icon Red");

        authCabAndGo("goodhits/ghits?campaign_id=1415");
        pagesInit.getCabTeasers().changeStateSuspectCloaking();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Black"), "Fail - icon Black");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52353\">Ticket TA-52353</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name="can_see_teaser_suspected_of_cloaking")
    @Test(description = "Check privilege for suspected of cloaking")
    public void checkPrivilegeForNotification() {
        log.info("Test is started");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_teaser_suspected_of_cloaking");

        authCabForCheckPrivileges("goodhits/ghits?campaign_id=1415");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Red"), "Fail - isDisplayedSuspectCloakingIcon 1");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedSuspectCloakingIcon("Black"), "Fail - isDisplayedSuspectCloakingIcon 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(APPROVE)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>teaser has offer</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52351\">Ticket TA-52351</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check approve teaser by mass")
    public void checkApproveTeaserByMass() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?campaign_id=1416");
        pagesInit.getCabTeasers().approveTeaserMassAction();
        pagesInit.getCabTeasers().clickApproveOption(APPLY_WITH_APPROVE);

        authCabAndGo("goodhits/ghits?campaign_id=1416");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(), "Approved", "Fail - getTeaserStatus");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Fail - isDisplayedApproveIcon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(APPROVE)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>teaser has offer</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52351\">Ticket TA-52351</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check reject teaser by mass")
    public void checkRejectTeaserByMass() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits?campaign_id=1416");
        pagesInit.getCabTeasers().rejectTeaserMassAction();

        authCabAndGo("goodhits/ghits?campaign_id=1416");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(), "Rejected", "Fail - getTeaserStatus");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedRejectIcon(), "Fail - isDisplayedApproveIcon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 teaser which one dropped and second is blocked</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo NIO https://jira.mgid.com/browse/CP-3033 @Test (description = "Check create teaser over limit")
    public void checkCreateTeaserOverLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1425;
        String campaignName = "Limit for amount of teasers 1";
        int limit = 2;

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(true)
                .setNeedToEditImage(false)
                .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");


        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(String.format(limitError, campaignName, limit)), "Fail - checkMessagesCab");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 2 teaser which one dropped</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check create teaser with max range of limit")
    public void checkCreateTeaserWithMaxLimitForCampaign() {
        log.info("Test is started");
        int campaignId = 1426;
        int clientId = 1000;

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        softAssert.assertNotEquals(pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .setDomain("https://testhim.com")
                .setNeedToEditImage(true)
                        .setNeedToEditCategory(false)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserPresent(clientId, pagesInit.getCabTeasers().getTeaserId()), "Fail - isTeaserPresent");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 regular teaser</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check create teaser with exceeded limit")
    public void checkCreateTeaserWithExceededLimit() {
        log.info("Test is started");

        int campaignId = 1427;
        String campaignName = "Limit for amount of teasers 3";
        int limit = 2;

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + campaignId);
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(true)
                .setNeedToEditImage(false)
                        .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");


        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(String.format(limitError, campaignName, limit)), "Fail - checkMessagesCab");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>campaign has 3 teasers one of them with dropped=1, limit = 2</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo https://jira.mgid.com/browse/CP-3033 @Test (description = "Check redo teaser with exceeded limit")
    public void checkRedoTeaserWithExceededLimit() {
        log.info("Test is started");

        int teaserId = 1487;
        String campaignName = "Limit for amount of teasers 4";
        int campaignId = 1428;
        int limit = 2;

        authCabAndGo("goodhits/ghits?id=" + teaserId);

        pagesInit.getCabTeasers().clickRedoIcon();
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab(String.format(limitError, campaignName, limit)), "Fail - checkMessagesCab");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TITLE_AND_DESCRIPTION)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52409\">Ticket TA-52409</a></li>\n" +
            "</ul>")
    @Test(description = "Edit title and description by inline with special characters  ™, ®, ° in list interface")
    public void checkEditTeaserTitleAndDescriptionInlineTeaserListInterface() {
        log.info("Test is started");
        int teaserId = 5002;
        String teaserTitle = "™ New and Sign ®";
        String teaserDescription = "Sign ® and ° Celsius";

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().editTitleInline(teaserTitle);
        pagesInit.getCabTeasers().setDescription(teaserDescription);
        pagesInit.getCabTeasers().editDescriptionInline();

        softAssert.assertTrue(pagesInit.getCabTeasers().checkTitleInTeasersListInterface(), "There is incorrect teaser title in teasers list!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDescriptionInTeasersListInterface(), "There is incorrect teaser description in teasers list!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}