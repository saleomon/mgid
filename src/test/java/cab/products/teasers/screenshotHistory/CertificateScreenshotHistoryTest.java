package cab.products.teasers.screenshotHistory;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static pages.cab.products.variables.CertificateScreenshotHistoryTest.messageForModerator;
import static pages.cab.products.variables.CertificateScreenshotHistoryTest.messageForModeratorKyiv;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CertificateScreenshotHistoryTest  extends TestBase {


    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Upload/ Download/ Delete file")
    @Description(
            "<ul>\n" +
            "   <li>Проверка загрузки сертификатов</li>\n" +
            "   <li>Проверка удаления сертификата</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51870\">Ticket TA-51870</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Copyright certificate LP | Upload/ Download/ Delete file")
    public void checkUploadDeleteCertificate() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1176");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();

        log.info("Upload certificate");
        pagesInit.getScreenshotHistory().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_png.png");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1176");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();
        softAssert.assertTrue(pagesInit.getScreenshotHistory().checkVisibilityOfCertificate("certif_png.png"), "FAIL - certificate  isn't visible but should");

        pagesInit.getScreenshotHistory().deleteCertificate();

        authCabAndGo("goodhits/ghits-screenshots-history/id/1176");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();
        softAssert.assertFalse(pagesInit.getScreenshotHistory().checkVisibilityOfCertificate("certif_png.png"), "FAIL - product visible");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Upload/ Download/ Delete file")
    @Description(
            "<ul>\n" +
            "   <li>Проверка загрузки сертификата больше 5 мб</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51870\">Ticket TA-51870</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Загрузка сертификата больше 5 мб")
    public void checkUploadOversizeCertificate() {
        log.info("Test is started");

        log.info("Upload certificate");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1168");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();
        pagesInit.getScreenshotHistory().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_size.jpeg");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Maximum allowed size for file 'certif_size.jpeg' is '5MB' but '5.63MB' detected"), "Check campaign after editing");

        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Upload/ Download/ Delete file")
    @Description(
            "<ul>\n" +
            "   <li>Проверка лимита количества загрузки сертификатов</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51870\">Ticket TA-51870</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка лимита количества загрузки сертификатов")
    public void checkAllowedAmountOfUploadedCertificate() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1169");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();
        log.info("Upload certificate");
        pagesInit.getScreenshotHistory().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_png.png");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Limit of upload files exceeded. Limit 20"), "Check campaign after editing");

        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Upload/ Download/ Delete file")
    @Description(
            "<ul>\n" +
            "   <li>Проверка лимита количества загрузки сертификатов</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51870\">Ticket TA-51870</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка скачивания сертификата")
    public void checkDownloadCertificates() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1169");
        log.info("Download certificate");
        pagesInit.getScreenshotHistory().downloadCertificates();
        serviceInit.getDownloadedFiles().getDownloadedFile();

        Assert.assertEquals(serviceInit.getDownloadedFiles().getFileName(), "copyright_lp_certificate_1169.zip","Check downloaded file");

        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Upload/ Download/ Delete file")
    @Description(
            "<ul>\n" +
            "   <li>Проверка  загрузки невалидного сертификатов</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51870\">Ticket TA-51870</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка загрузки невалидного сертификата")
    public void checkUploadInvalidExtencionCertificates() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1170");
        log.info("Download certificate");
        pagesInit.getScreenshotHistory().openPopupUploadCertificate();
        log.info("Upload certificate");
        pagesInit.getScreenshotHistory().uploadCertificates(LINK_TO_RESOURCES_FILES + "import_teaser.csv");

        log.info("Check message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("File 'import_teaser.csv' has a false extension"), "Check upload file");

        log.info("Test is finished");
    }

    @Privilege(name="set-screenshot-certificate-verification")
    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Verification and marking")
    @Description(
            "<ul>\n" +
            "   <li>Проверка права иконки verified</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51871\">Ticket TA-51871</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка права иконки verified")
    public void checkRightVerifiedIcon() {
        log.info("Test is started");

        authCabForCheckPrivileges("goodhits/ghits-screenshots-history/id/1169");
        softAssert.assertTrue(pagesInit.getScreenshotHistory().isDisplayVerifiedIcon(), "Should be displayed");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/set-screenshot-certificate-verification");

        authCabForCheckPrivileges("goodhits/ghits-screenshots-history/id/1169");
        softAssert.assertFalse(pagesInit.getScreenshotHistory().isDisplayVerifiedIcon(), "Should be hidden");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Verification and marking")
    @Description(
            "<ul>\n" +
            "   <li>Проверка работы екшна Verified</li>\n" +
            "   <li>Проверка отправки сообщения после изменения статуса Verified</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51871\">Ticket TA-51871</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51872\">Ticket TA-51872</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка работы екшна Verified")
    public void checkVerifyAction() {
        log.info("Test is started");
        try {
            operationMySql.getMailPull().getCountLettersByClientEmail("moderators.creative@mgid.com", "kyiv.mods.creative@mgid.com");
            authCabAndGo("goodhits/ghits-screenshots-history/id/1169");
            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(false);
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Not verified", "Should be not verified");

            softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMailByClientEmail("moderators.creative@mgid.com"), "There is no needed letters!", "Check moderator creative message Add");
            softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMailByClientEmail("kyiv.mods.creative@mgid.com"), "There is no needed letters!","Check moderator kyiv message Add");

            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(true);
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Verified", "Should verified");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(true);
        }
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Verification and marking")
    @Description(
            "<ul>\n" +
            "   <li>Проверка смены статуса Verified посредством добавления нового сертификата</li>\n" +
            "   <li>Проверка смены статуса Verified посредством удаления сертификата</li>\n" +
            "   <li>Проверка отправки сообщения после изменения статуса Verified</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51871\">Ticket TA-51871</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51872\">Ticket TA-51872</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка работы екшна Verified через удаление/добавление сертификатов", priority = 1)
    public void checkVerifyActionByAddingDeletingCertificates() {
        log.info("Test is started");

        try {
            operationMySql.getMailPull().getCountLettersBySubject("Copyright verification for campaign 1213 has been removed", "Верификация авторских прав для кампании 1213 удалена");
            authCabAndGo("goodhits/ghits-screenshots-history/id/1171");
            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(true);
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Verified", "Should verified");

            log.info("Add certificate");
            pagesInit.getScreenshotHistory().openPopupUploadCertificate();
            pagesInit.getScreenshotHistory().uploadCertificates(LINK_TO_RESOURCES_FILES + "certif_png.png");

            log.info("Check verified icon");
            authCabAndGo("goodhits/ghits-screenshots-history/id/1171");
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Not verified", "Check after adding");

            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMail("Copyright verification for campaign 1213 has been removed", String.format(messageForModerator, "1213", "1171", "1171")), "Check moderator creative message Add");
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMail("Верификация авторских прав для кампании 1213 удалена", String.format(messageForModeratorKyiv, "1213", "1171", "1171")), "Check moderator kyiv message  Delete");

            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(true);
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Verified", "Check icon status");

            log.info("Delete certificate");
            pagesInit.getScreenshotHistory().openPopupUploadCertificate();
            pagesInit.getScreenshotHistory().deleteCertificate();

             log.info("Check verified icon");
            authCabAndGo("goodhits/ghits-screenshots-history/id/1171");
            softAssert.assertEquals(pagesInit.getScreenshotHistory().getTitleOfVerifiedIcon(), "Not verified", "Check after deleting");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            authCabAndGo("goodhits/ghits-screenshots-history/id/1171");
            pagesInit.getScreenshotHistory().changeStateVerifiedIcon(true);
        }
    }


    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Verification and marking")
    @Description(
            "<ul>\n" +
                    "   <li>Проверка отображения иконки список клиентов</li>\n" +
                    "   <li>Проверка отображения иконки список кампаний</li>\n" +
                    "   <li>Проверка отображения иконки список тизеров</li>\n" +
                    "   <li>Проверка отображения иконки список модерации</li>\n" +
                    "</ul>\n" +
                    "<ul>\n" +
                    "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51871\">Ticket TA-51871</a></li>\n" +
                    "   <li><p>Author NIO</p></li>\n" +
                    "</ul>")
    @Test(description = "Проверка работы екшна Verified через удаление/добавление сертификатов")
    public void checkVerifiedIconAtDifferentInterfaces() {
        log.info("Test is started");

        log.info("Check clients interface");
        authCabAndGo("goodhits/clients?client_id=1");
        softAssert.assertTrue(pagesInit.getCabProductClients().isDisplayVerifiedIcon(), "Clients interface");

        log.info("Check campaign interface");
        authCabAndGo("goodhits/campaigns/id/1211");
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayVerifiedIcon(), "Campaign interface");

        log.info("Check teaser interface");
        authCabAndGo("goodhits/ghits/?id=1169");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayVerifiedIcon(),"Teaser interface");

        log.info("Check moderation interface");
        authCabAndGo("goodhits/on-moderation?id=1172");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayVerifiedIcon(),"Teaser moderation");

        log.info("Check clients interface without certificate");
        authCabAndGo("goodhits/clients?client_id=2");
        softAssert.assertFalse(pagesInit.getCabProductClients().isDisplayVerifiedIcon(), "Clients interface 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Verification and marking")
    @Description(
            "<ul>\n" +
                    "   <li>Проверка наследования иконки другому клиенту с одинаковым хешем скринов для обоих клиентов</li>\n" +
                    "</ul>\n" +
                    "<ul>\n" +
                    "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51871\">Ticket TA-51871</a></li>\n" +
                    "   <li><p>Author NIO</p></li>\n" +
                    "</ul>")
    @Test(description = "Проверка наследования иконки Verified другому клиенту с тем же хешем")
    public void checkInheritanceVerifiedIconWithDifferentClientWithTheSameScreenshotHash() {
        log.info("Test is started");

        log.info("Check clients interface");
        authCabAndGo("goodhits/clients?client_id=3");
        Assert.assertFalse(pagesInit.getCabProductClients().isDisplayVerifiedIcon(), "Clients interface");

        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Campaign list in client interface")
    @Description(
            "<ul>\n" +
                    "   <li>Проверка отображения копирайта</li>\n" +
                    "   <li>Переход по копирайту на список тизеров с лендингами</li>\n" +
                    "</ul>\n" +
                    "<ul>\n" +
                    "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51873\">Ticket TA-51873</a></li>\n" +
                    "   <li><p>Author NIO</p></li>\n" +
                    "</ul>")
    @Test(description = "Проверка отображения копирайта и линки к списку тизеров")
    public void checkCopyrightIconAndForwardingToTeasers() {
        log.info("Test is started");

        log.info("Check clients interface");
        authCabAndGo("goodhits/clients?client_id=1004");
        Assert.assertTrue(pagesInit.getCabProductClients().isDisplayCopyrightIcon(), "Clients interface");
        authCabAndGo(pagesInit.getCabProductClients().getLinkCopyrightIcon());
        Assert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), Integer.valueOf(2), "Different amount of teasers");
        log.info("Test is finished");
    }

    @Privilege(name="can_see_copyright_certificate_icon")
    @Feature("Copyright certificate LP | V 1.0")
    @Story("Copyright certificate LP | Campaign list in client interface")
    @Description(
            "<ul>\n" +
                    "   <li>Проверка права иконки копирайта</li>\n" +
                    "</ul>\n" +
                    "<ul>\n" +
                    "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51873\">Ticket TA-51873</a></li>\n" +
                    "   <li><p>Author NIO</p></li>\n" +
                    "</ul>")
    @Test(description = "Проверка права иконки копирайта")
    public void checkPrivilegeCopyrightIcon() {
        log.info("Test is started");

        log.info("Check clients interface");
        authCabForCheckPrivileges("goodhits/clients?client_id=1004");
        softAssert.assertTrue(pagesInit.getCabProductClients().isDisplayCopyrightIcon(), "Clients interface");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_copyright_certificate_icon");

        authCabForCheckPrivileges("goodhits/clients?client_id=1004");
        softAssert.assertFalse(pagesInit.getCabProductClients().isDisplayCopyrightIcon(), "Clients interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
