package cab.products.teasers.screenshotHistory;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

public class CabScreenshotHistoryTest extends TestBase {
    /**
     * Не привязывается продукт на скрины. Осуществляем привязку продукта(Pattern) к тизеру по его паттерну(саб тайтлу Template)
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27537">TA-27537</a>
     */
    @Test
    public void checkLinkingProductByPattern() {
        log.info("Test is started");
        log.info("Link product with teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1046");
        pagesInit.getScreenshotHistory().linkOfferWithTeaser("Template");

        log.info("Check presence linked product");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1046");
        pagesInit.getScreenshotHistory().checkVisibilityLinkedProduct("Pattern");
        log.info("Test is finished");
    }

    /**
     * Доработки ввода слов интерфейса продуктов
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27516">TA-27516</a>
     */
    @Test(dataProvider = "productData")
    public void checkSearchProductByName(String productName) {
        log.info("Test is started");
        log.info("Link product with teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1");
        pagesInit.getScreenshotHistory().searchProduct(productName);
        Assert.assertTrue(pagesInit.getScreenshotHistory().checkAutocompleteProduct(productName));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] productData() {
        return new Object[][]{
                {"Do"},
                {"At home"}
        };
    }

    /**
     * 	Удаление продукта привязанного к скрину
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27622">TA-27622</a>
     */
    @Test
    public void checkDeleteProductFromScreenshot() {
        log.info("Test is started");
        log.info("Delete product from screenshot");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1047");
        pagesInit.getScreenshotHistory().deleteLinkedProduct("Screenshot");
        log.info("Check message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Successfully removed offer Screenshot from screenshot"), "FAIL - no successful message");
        log.info("Check product presence");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1047");
        softAssert.assertFalse(pagesInit.getScreenshotHistory().checkProductPresence("Screenshot") , "FAIL - product is present");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Offers")
    @Story("Добавить обозначение по офферам созданным из advertiser name")
    @Description("Check displaying icon 'advertiser name' due created offer from advertiser name\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52023\">Ticket TA-52023</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check displaying icon 'advertiser name' due created offer from advertiser name")
    public void checkDisplayingCreatedFromAdvertNameIcon() {
        log.info("Test is started");
        log.info("Link product with teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1046");

        softAssert.assertTrue(pagesInit.getScreenshotHistory().isDisplayedShowNameIcon(), "FAIL - isDisplayedShowNameIcon");
        softAssert.assertEquals(pagesInit.getScreenshotHistory().getTipShowNameIcon(), "Offer added from advertiser name ", "FAIL - getTipShowNameIcon");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
