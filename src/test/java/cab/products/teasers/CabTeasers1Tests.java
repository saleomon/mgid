package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.UPDATE_DUPLICATES_SPELL_ERROR;
import static testData.project.AuthUserCabData.AuthorizationUsers.TEASER_MODERATION_USER;
import static testData.project.CliCommandsList.Consumer.TEASERS_UPDATE_DUPLICATE;
import static testData.project.CliCommandsList.Cron.LANDING_DOMAINS;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;
import static testData.project.ClientsEntities.WEBSITE_MGID_GOODHITS_DOMAIN;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabTeasers1Tests extends TestBase {
    /**
     * Админка. Добавить новый тип кампании Search Feed в фильтры, добавить новую иконку
     * <ul>
     *  <li>Проверка работы фильтра по типу кампании</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24329">Ticket TA-24329</a>
     * <p>NIO</p>
     */
    @Test
    public void checkFilterByCampaignType() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits");
        log.info("Filter by campaign type");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("campaignTypesBox", "Search feed");
        pagesInit.getCabTeasers().pressFilterButton();
        log.info("Get amount from db");
        String amountCampaignDB = operationMySql.getgHits1().selectAmountOfTeasersWithCampaignType("search_feed");
        log.info("Check amount from db and from interface");
        Assert.assertEquals(amountCampaignDB, pagesInit.getCabTeasers().getTotalAmountOfTeasers());
        log.info("Test is finished");
    }

    /**
     * Админка. Добавить новый тип кампании Search Feed в фильтры, добавить новую иконку
     * <ul>
     *  <li>Проверка иконки кампании</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24329">Ticket TA-24329</a>
     * <p>NIO</p>
     */
    @Test
    public void checkCampaignIconForSearchFeedType() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?campaign_types=search_feed");
        log.info("Check visibility of icon");
        Assert.assertTrue(pagesInit.getCabTeasers().checkVisibilityOfSearchFeedIcon());
        log.info("Test is finished");
    }


    /**
     * Check approve without link offer
     * <ul>
     *     <li>Click approve teaser</li>
     *     <li>Click button 'Continue without linking' at popup</li>
     *     <li>Approve teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24520">TA-24520</a>
     */
    @Test
    public void approveTeaserWithoutLinking() {
        log.info("Test is started");
        operationMySql.getgHits1().updateTeaserModerationStatus(1005, 1);
        authCabAndGo("goodhits/ghits/?id=1005");
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();
        Assert.assertFalse(pagesInit.getCabTeasers().checkVisibilityApproveIcon());
        log.info("Test is finished");
    }

    /**
     * Check approve with link offer
     * <ul>
     *     <li>Click approve teaser</li>
     *     <li>Check displayed popup for linking options</li>
     *     <li>Link teaser with offer at screenshot history interface</li>
     *     <li>Approve teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24520">TA-24520</a>
     */
    @Test
    public void approveTeaserWithLinkingOffer() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=1007");
        log.info("Approve teaser and check visibility popup for link option");
        pagesInit.getCabTeasers().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedPopupLinkOffer(), "popup should be visible");

        log.info("Link offer with teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1007");
        pagesInit.getScreenshotHistory().linkOfferWithTeaser("Bustiere");

        log.info("Approve teaser and check visibility popup");
        authCabAndGo("goodhits/ghits/?id=1007");
        pagesInit.getCabTeasers().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedPopupLinkOffer(), "popup should not be visible");
        pagesInit.getCabTeasers().clickContinueWithAproveRejectButton();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "icon should not be visible");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка статуса тизеров(с определенными категориями) при изменении гео кампании:
     * <ul>
     *     <li>Изменение гео с гео Spain, Canada на 'Ukraine', 'Canada'</li>
     *     <li>Проверка статуса тизеров</li>
     *     <li>Изменение гео с гео 'Ukraine', 'Canada' на Spain, Canada</li>
     *     <li>Проверка статуса тизеров</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24494">TA-24494</a>
     */
    @Test
    public void checkModerationStatusTeaserWithGeoSpainCampaign() {
        log.info("Test is started");

        log.info("Check teaser status. Geo Spain, Canada -> 'Ukraine', 'Canada'");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1039, "Canada", "Spain");
        authCabAndGo("goodhits/ghits?campaign_id=1039");

        authCabAndGo("goodhits/campaigns-edit/id/1039/");
        pagesInit.getCabCampaigns().selectLocationTargetAndSaveSettings("include", "Ukraine", "Canada");

        authCabAndGo("goodhits/ghits?campaign_id=1039");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Geo Spain, Canada -> 'Ukraine', 'Canada'");

        log.info("Check teaser status. Geo 'Ukraine', 'Canada' -> 'Spain', 'Canada'");
        authCabAndGo("goodhits/campaigns-edit/id/1039/");
        pagesInit.getCabCampaigns().selectLocationTargetAndSaveSettings("include", "Spain", "Canada");
        authCabAndGo("goodhits/ghits?id=1022");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Geo 'Ukraine', 'Canada' -> 'Spain', 'Canada' 1022");
        authCabAndGo("goodhits/ghits?id=1023");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Geo 'Ukraine', 'Canada' -> 'Spain', 'Canada' 1023");
        authCabAndGo("goodhits/ghits?id=1024");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Geo 'Ukraine', 'Canada' -> 'Spain', 'Canada' 1024");
        authCabAndGo("goodhits/ghits?id=1025");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Geo 'Ukraine', 'Canada' -> 'Spain', 'Canada' 1025");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка права на апрув тизера без заполненного поля test_square_crop с опцией cloudinary
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24598">TA-24598</a>
     */
    @Privilege(name = "can_aprove_ad_without_crop_verfication")
    @Test
    public void checkPrivilegeCanApproveTeaserWithoutSquareCrop() {
        log.info("Test is started");

        authCabForCheckPrivileges("goodhits/on-moderation?id=1032");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_aprove_ad_without_crop_verfication");
        pagesInit.getCabTeasersModeration().approveTeaserWithoutLinkingOffer();

        log.info("Check presence of error message and visibility approve icon");
        softAssert.assertFalse(helpersInit.getMessageHelper().checkCustomMessagesCab("Impossible to approve/reject ad. Cloudinary: please check square crop correctness"), "FAIL - message");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityApproveIcon(), "FAIL - icon visibility - moderation");

        authCabForCheckPrivileges("goodhits/ghits/?id=1033");
        pagesInit.getCabTeasersModeration().rejectTeaser();

        log.info("Check presence of error message and visibility reject icon");
        softAssert.assertFalse(helpersInit.getMessageHelper().checkCustomMessagesCab("Impossible to approve/reject ad. Cloudinary: please check square crop correctness"), "FAIL - message");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkVisibilityRejectIcon(), "FAIL - icon visibility - list");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Убрать автозамену тайтлов по дубликатам при редактировании
     * <ul>
     *     <li>Есть 2 тизера с одинаковым тайтлом ы записью в spell_check_data о наличии ошибки в орфографии</li>
     *     <li>Изменяем тайтл одного тизера</li>
     *     <li>Запускаем крон по изменению идентичных тайтлов в других тизерах</li>
     *     <li>Проверяем что тайтл в других тизерах изменился на тот который отредактировали ранее</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27494">TA-27494</a>
     */
    @Test
    public void checkAutoEditTeasersTitleWithSimilarOne() {
        log.info("Test is started");
        try {
            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+Similor+title");

            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_DUPLICATE);
            pagesInit.getCabTeasers().editTitleInline("Teaser Similar title");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(UPDATE_DUPLICATES_SPELL_ERROR);


            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+Similor+title");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(0), "FAIL - Teaser+Similor+title failed");

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+Similar+title");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(2), "FAIL - Teaser+Similar+title failed");

        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Убрать автозамену тайтлов по дубликатам при редактировании
     *
     * <ul>
     *     <li>Есть 2 тизера с одинаковым тайтлом ы записью в spell_check_data об отсутствии ошибки в орфографии</li>
     *     <li>Изменяем тайтл одного тизера</li>
     *     <li>Запускаем крон по изменению идентичных тайтлов в других тизерах</li>
     *     <li>Проверяем что тайтл в других тизерах НЕ изменился на тот который отредактировали ранее</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27494">TA-27494</a>
     */
    @Test
    public void checkNotEditTeaserTitleWithSimilarOne() {
        log.info("Test is started");
        try {
            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+amazing+title");
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_DUPLICATE);
            pagesInit.getCabTeasers().editTitleInline("Teaser Correct title");

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+Correct+title");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "FAIL - Teaser+Correct+title failed");

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordTitle=Teaser+amazing+title");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "FAIL - Teaser+amazing+title failed");

        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Убрать автозамену тайтлов по дубликатам при редактировании
     * <ul>
     *     <li>Есть 2 тизера с одинаковым дескрипшеном ы записью в spell_check_data о наличии ошибки в орфографии</li>
     *     <li>Изменяем дескрипшен одного тизера</li>
     *     <li>Запускаем крон по изменению идентичных дескрипшенов в других тизерах</li>
     *     <li>Проверяем что дескрипшен в других тизерах изменился на тот который отредактировали ранее</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27494">TA-27494</a>
     */
    @Test
    public void checkAutoEditTeasersDescriptionWithSimilarOne() {
        log.info("Test is started");
        try {
            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Somec+text+present");
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_DUPLICATE);
            pagesInit.getCabTeasers().setDescription("Some ambitious text");
            pagesInit.getCabTeasers().editDescriptionInline();
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(UPDATE_DUPLICATES_SPELL_ERROR);

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Somec+text+present");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(0), "FAIL - Somec+text failed");

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Some+ambitious+text");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(2), "FAIL - Some+ambitious+text failed");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Убрать автозамену тайтлов по дубликатам при редактировании
     * <ul>
     *     <li>Есть 2 тизера с одинаковым дескрипшеном ы записью в spell_check_data об отсутствии ошибки в орфографии</li>
     *     <li>Изменяем дескрипшен одного тизера</li>
     *     <li>Запускаем крон по изменению идентичных дескрипшенов в других тизерах</li>
     *     <li>Проверяем что дескрипшен в других тизерах НЕ изменился на тот который отредактировали ранее</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27494">TA-27494</a>
     */
    @Test
    public void checkNotEditTeasersDescriptionWithSimilarOne() throws InterruptedException {
        log.info("Test is started");
        try {
            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Some+intelligent+text");
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_DUPLICATE);
            pagesInit.getCabTeasers().setDescription("Some awesome text");
            pagesInit.getCabTeasers().editDescriptionInline();
            Thread.sleep(2000);

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Some+intelligent+text");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "FAIL - Some+intelligent+text failed");

            authCabAndGo("goodhits/ghits?campaign_id=1003&keywordDescription=Some+awesome+text");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "FAIL - Some+awesome+text failed");

        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * 	Удаление продукта привязанного к тизеру
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27622">TA-27622</a>
     */
    @Test
    public void checkDeleteProductFromScreenshot() {
        log.info("Test is started");
        log.info("Delete product from screenshot");
        authCabAndGo("goodhits/on-moderation?id=1048");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().deleteLinkedProduct("Moderation");
        log.info("Check message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Successfully removed offer Moderation from screenshot"), "FAIL - no successful message");
        log.info("Check product presence");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkProductPresence("Moderation") , "FAIL - product is present");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * DFP | Blacklisted domains flag. Ручное проставление Флага для Тизера
     * @see <a href="https://youtrack.mgid.com/issue/TA-22286">TA-22286</a>
     * <p>NIO</p>
     */
    @Test
    public void checkSetRemoveGoogleAddManager() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits?id=1050");
        pagesInit.getCabTeasers().setRemoveGoogleAddManager(true);
        Assert.assertTrue(pagesInit.getCabTeasers().checkGoogleAddManagerIcon(true), "Fail - should be true");
        pagesInit.getCabTeasers().setRemoveGoogleAddManager(false);
        Assert.assertTrue(pagesInit.getCabTeasers().checkGoogleAddManagerIcon(false), "Fail - should be false");
        log.info("Test is finished");
    }

    /**
     * Offers. | Присвоение офферов тизерам | Ошибки
     * <ul>
     *     <li>Linking offer to teaser</li>
     *     <li>Check at popup linking that offer linked for this teaser</li>
     *     <li>Check at popup linking that offer is displayed for another teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27282">TA-27282</a>
     */
    @Test
    public void checkDisplayingOfferAtPopupAfterLinkingtoAnotherTeaser() {
        log.info("Test is started");
        log.info("Add product to teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/1084");
        pagesInit.getScreenshotHistory().linkOfferWithTeaser("OfferGroupPlus");

        log.info("Check displaying linked offer");
        authCabAndGo("goodhits/ghits/?id=1084");
        pagesInit.getCabTeasers().approveTeaser();
        softAssert.assertEquals(pagesInit.getCabTeasers().getLinkedOffers(), "OfferGroupPlus", "FAIL - offer isn't linked");

        log.info("Check displaying linked offer for possibility linking");
        authCabAndGo("goodhits/ghits/?id=1086");
        pagesInit.getCabTeasers().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedOfferAtPopup("OfferGroupPlus"), "FAIL - offer isn't displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Offers. | Присвоение офферов тизерам | Ошибки
     * <ul>
     *     <li>Approve teaser with offers</li>
     *     <li>Check at popup linking that offer linked for this teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27282">TA-27282</a>
     */
    @Test
    public void checkDisplayingPopupOfferDuringApprovingTeaser() {
        log.info("Test is started");
        log.info("Add product to teaser");

        log.info("Check displaying linked offer");
        authCabAndGo("goodhits/ghits/?id=1087");
        pagesInit.getCabTeasers().approveTeaser();
        pagesInit.getCabTeasers().applyWithAprove();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - teaser isn't approved");

        log.info("Check displaying linked offer for possibility linking");
        authCabAndGo("goodhits/ghits/?id=1088");
        pagesInit.getCabTeasers().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedPopupLinkOffer(), "FAIL - popup isn't displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Draft - товары - воспроизведение кейса
     * <ul>
     *     <li>Проверка отправки тизера в определенный статус
     *     при редактировании/не редактировании и с правом/без права</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-44294">TA-44294</a>
     */
    //toDo NIO Edit teaser fields forbidden: draft and triger DB Nobody knows what happen @Test(dataProvider = "dataForTeaserEditing")
    public void checkSendTeaserToModerationAfterEditing(int teaserId, String status, boolean stateRight) {
        log.info("Test is started");
        log.info("Add product to teaser");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(stateRight, AUTOTEST_ROLE, "goodhits/ghits_edit_no_moderation");

        log.info("Edit inline and check status");
        authCabForCheckPrivileges("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().editTitleInline();
        authCabForCheckPrivileges("goodhits/ghits/?id=" + teaserId);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  status, "FAIL - inline editing");

        log.info("Back to draft");
        operationMySql.getgHits1().updateDraftStatus(teaserId, 1);
        operationMySql.getgHits1().updateTeaserModerationStatus(teaserId, 2);

        log.info("Edit at edit interface without changes and check status");
        authCabForCheckPrivileges("goodhits/ghits-edit/id/" + teaserId);
        pagesInit.getCabTeasers().sendToModeration();
        authCabForCheckPrivileges("goodhits/ghits/?id=" + teaserId);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  status, "FAIL - general editing without");

        log.info("Back to draft");
        operationMySql.getgHits1().updateDraftStatus(teaserId, 1);
        operationMySql.getgHits1().updateTeaserModerationStatus(teaserId, 2);

        log.info("Edit at edit interface with changes and check status");
        authCabForCheckPrivileges("goodhits/ghits-edit/id/" + teaserId);
        pagesInit.getCabTeasers().setNeedToEditImage(false).editTeaser();
        authCabForCheckPrivileges("goodhits/ghits/?id=" + teaserId);
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  status, "FAIL - general editing with");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForTeaserEditing() {
        return new Object[][]{
                {1091, CabTeasers.Statuses.ON_MODERATION.getStatusesValue(), false},
//                {1092, CabTeasers.Statuses.APPROVED.getStatusesValue(), true},
        };
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка фильтра по драфт и интернал драфт</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkInternalDraftFilter() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits");

        log.info("check internal draft filter");

        pagesInit.getCabTeasers().filterByStatus("internal_drafts");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(),  pagesInit.getCabTeasers().getAmountOfTeasersWithStatus("Internal drafts"), "FAIL - internal_drafts");

        pagesInit.getCabTeasers().filterByStatus("client_drafts");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(),  pagesInit.getCabTeasers().getAmountOfTeasersWithStatus("Client drafts"), "FAIL - client_drafts");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка отмены модерации тизера и возвращение его в статус клаентс драфтс</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkCancelModeration() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=1107");

        log.info("check internal draft filter");

        pagesInit.getCabTeasers().cancelModeration();
        authCabAndGo("goodhits/ghits/?id=1107");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedSendToModerationButton(), "FAIL - send to moderation button");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(),  CabTeasers.Statuses.CLIENT_DRAFTS.getStatusesValue(), "FAIL - wrong status");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка отмены модерации тизера при условии что тизер взят в работу</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    //toDo privileges problem https://jira.mgid.com/browse/CP-2807@Test@Test
    public void checkCancelModerationDuringProcessingByModerator() {
        log.info("Test is started");
        String teaserId = "1108";
        authCabAndGo(TEASER_MODERATION_USER, "goodhits/on-moderation/id/" + teaserId);

        log.info("check displaying button during start moderation");
        pagesInit.getCabTeasersModeration().startModeration();
        authCabAndGo(TEASER_MODERATION_USER, "goodhits/ghits/?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedSendToModerationButton(), "FAIL - should not be hidden isDisplayedSendToModerationButton 1");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedCancelModerationButton(), "FAIL - should not be hidden isDisplayedCancelModerationButton 1");

        log.info("check displaying button during stop moderation");
        authCabAndGo(TEASER_MODERATION_USER, "goodhits/on-moderation/id/" + teaserId);
        pagesInit.getCabTeasersModeration().stopModeration();
        authCabAndGo(TEASER_MODERATION_USER, "goodhits/ghits/?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedSendToModerationButton(), "FAIL - should not be visible isDisplayedSendToModerationButton 2");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedCancelModerationButton(), "FAIL - should not be visible isDisplayedCancelModerationButton 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(REJECT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1_id.rejection_reasons_proposed = '{\"copyright_infringement\": [\"\"]}'</>" +
            "   <li>check displaying before taking in work(amount)</>" +
            "   <li>check displaying auto rejection block</>" +
            "   <li>check reject teaser with suggested reason through auto rejection block</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52338\">Ticket TA-52338</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    //toDo privileges problem https://jira.mgid.com/browse/CP-2807@Test@Test(description = "Check auto reject full flow")
    public void checkAutoRejectFullFlow() {
        log.info("Test is started");
        int teaserId = 1422;

        authCabAndGo(TEASER_MODERATION_USER, "goodhits/on-moderation/id/" + teaserId);

        log.info("check displaying button before start moderation");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAmountOfAutoRejectReasonsBeforeStartModeration(), 1, "Fail - getAmountOfAutoRejectReasonsBeforeStartModeration");

        log.info("open block auto rejection and check displayed information from it");
        pagesInit.getCabTeasersModeration().startModeration();
        pagesInit.getCabTeasersModeration().clickShowSuggestedReasons();

        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAutorejectComponent(), "Thumbnail", "Fail - getAutorejectComponent");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getRejectionReason(), "Copyright infringement", "Fail - getRejectionReason");

        log.info("apply rejection with suggestion reasons and check result");
        pagesInit.getCabTeasersModeration().applySuggestedReasonsButton();

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkEnabledRejectReasons("Copyright infringement"), "Fail - checkEnabledRejectReasons");
        log.info("check that comment field should be empty");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().getListOfCommentsEnabledRejectReasons().size() > 0, "Fail - getListOfCommentsEnabledRejectReasons");
        pagesInit.getCabTeasersModeration().saveRejectReasonForm();

        softAssert.assertEquals(operationMySql.getgHits1().getQuarantineValue(teaserId), 2, "Fail - getQuarantineValue");
        softAssert.assertTrue(operationMySql.getGHitsRejections().getRejectionReasons(teaserId).contains("copyright_infringement"), "Fail - containsAll");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkVisibilityRejectIcon(), "Fail - containsAll");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(REJECT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>check reject with empty reason</>" +
            "   <li>check reject with auto reject and custom reasons</>" +
            "   <li>g_hits_1_id.rejection_reasons_proposed = '{\"copyright_infringement\": [\"\"]}'</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52338\">Ticket TA-52338</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check auto reject with empty reason and continue rejection with custom")
    public void checkRejectWithEmptyReasonAndContinueWithCustom() {
        log.info("Test is started");
        int teaserId = 1424;

        authCabAndGo("goodhits/on-moderation/id/" + teaserId);

        log.info("check displaying button during start moderation");
        pagesInit.getCabTeasersModeration().clickShowSuggestedReasons();
        pagesInit.getCabTeasersModeration().applySuggestedReasonsButton();

        pagesInit.getCabTeasersModeration().clearAllRejectedReason();
        pagesInit.getCabTeasersModeration().saveRejectReasonForm();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Rejection reason in not specified"), "Fail - checkMessagesCab");

        pagesInit.getCabTeasersModeration().chooseRejectReason("0", "6", "7");
        pagesInit.getCabTeasersModeration().saveRejectReasonForm();

        softAssert.assertEquals(operationMySql.getgHits1().getQuarantineValue(teaserId), 2, "Fail - getQuarantineValue");
//        softAssert.assertTrue(operationMySql.getGHitsRejections().getRejectionReasons(teaserId).containsAll(Arrays.asList("copyright_infringement", "misleading")), "Fail - containsAll");
        softAssert.assertTrue(operationMySql.getGHitsRejections().getRejectionReasons(teaserId).containsAll(Arrays.asList("copyright_infringement", "private_photos_are_forbidden_us")), "Fail - containsAll");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(REJECT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1_id.rejection_reasons_proposed = '{\"copyright_infringement\": [\"\"]}'</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52338\">Ticket TA-52338</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check non selected reasons at rejection form by thumb")
    public void checkNonSelectedReasonsAtRejectionByThumb() {
        log.info("Test is started");
        int teaserId = 1424;

        authCabAndGo("goodhits/on-moderation/id/" + teaserId);

        pagesInit.getCabTeasersModeration().clickRejectIconByThumb();
        Assert.assertFalse(pagesInit.getCabTeasersModeration().checkEnabledRejectReasons("Copyright infringement"),"Fail - checkEnabledRejectReasons");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(REJECT)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>g_hits_1_id.rejection_reasons_proposed = '{\"copyright_infringement\": [\"\"]}'</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52338\">Ticket TA-52338</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Privilege(name = "can_see_auto_reject")
    @Test(description = "Check privilege auto rejection")
    public void checkPrivilegeAutoRejection() {
        log.info("Test is started");
        int teaserId = 1424;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_auto_reject");

        authCabForCheckPrivileges("goodhits/on-moderation/id/" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedAutoRejectionForm());

        log.info("Test is finished");
    }

    /**
     * Internal Draft teasers
     * <ul>
     *     <li>Проверка удаления по заявке тизера со статусом драфт</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51681">TA-51681</a>
     * NIO
     */
    @Test
    public void checkDeleteTeaserAtDraftStatus() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=1109");
        pagesInit.getCabTeasers().deleteTeaser();
        Assert.assertTrue(pagesInit.getCabTeasers().isDisplayedRedoIcon(), "FAIL - should  be visible isDisplayedSendToModerationButton 1");
        log.info("Test is finished");
    }

    /**
     * <p>Фильтры тизеры/продукты с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51718">Ticket TA-51718</a>
     * <p>NIO</p>
     */
    @Test
    public void checkFilterByCertificate() {
        log.info("Test is started");

        log.info("filter by certificate");
        authCabAndGo("goodhits/ghits");
        pagesInit.getCabTeasers().filterByCertificate();
        Assert.assertEquals(pagesInit.getCabTeasers().getAmountOfIconsDownloadCertificates(),
                pagesInit.getCabTeasers().getAmountOfTeasers());

        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for fields:\n" +
            "     <ul>\n" +
            "      <li>Link</li>\n" +
            "      <li>Title</li>\n" +
            "      <li>Advertising text</li>\n" +
            "      <li>UPLOAD image link </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for fields create interface")
    public void checkResetButtonsCreateInterface() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        log.info("create teaser");
        pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .fillDomain(WEBSITE_MGID_GOODHITS_DOMAIN)
                .fillTitle("Some title")
                .fillAdvertText("Hi there");

        pagesInit.getCabTeasers().useDiscount(true).fillProductPrice("13");

        softAssert.assertEquals(pagesInit.getCabTeasers().getDomainValue(), WEBSITE_MGID_GOODHITS_DOMAIN, "FAIL - domain first");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTitleValue(), "Some title", "FAIL - title first");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAdvertValue(), "Hi there", "FAIL - Advert first");

        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountResetButtons(), 3,  "FAIL - first check first");

        pagesInit.getCabTeasers().resetAllFieldsByResetButtons();
        helpersInit.getMessageHelper().isSuccessMessagesCab(true);
        pagesInit.getCabTeasers().resetAllFieldsByResetButtons();
        pagesInit.getCabTeasers().fillProductPrice("16");
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkResetButtonsCreateInterface_111");

        softAssert.assertEquals(pagesInit.getCabTeasers().getDomainValue(), "", "FAIL - domain second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTitleValue(), "", "FAIL - title second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAdvertValue(), "", "FAIL - Advert second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountResetButtons(), 0,  "FAIL - first check first  second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for fields:\n" +
            "     <ul>\n" +
            "      <li>Link</li>\n" +
            "      <li>Title</li>\n" +
            "      <li>Advertising text</li>\n" +
            "      <li>UPLOAD image link </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for fields edit interface")
    public void checkResetButtonsEditInterface() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-edit/id/" + 1199);

        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountResetButtons(), 4,  "FAIL - first check first");

        pagesInit.getCabTeasers().resetAllFieldsByResetButtons();

        softAssert.assertEquals(pagesInit.getCabTeasers().getDomainValue(), "", "FAIL - domain second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTitleValue(), "", "FAIL - title second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAdvertValue(), "", "FAIL - Advert second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getImageLinkValue(), "", "FAIL - image link  second");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountResetButtons(), 0,  "FAIL - first check first  second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for image url inline:\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for image url inline")
    public void checkResetButtonsImageLinkInline() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits?id=1200");

        pagesInit.getCabTeasers().openPopupEditImageInline();

        helpersInit.getMessageHelper().checkCustomMessagesCab("");
        pagesInit.getCabTeasers().resetAllFieldsByResetButtons();

        Assert.assertEquals(pagesInit.getCabTeasers().getImageLinkValue(), "");
        log.info("Test is finished");
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check teaser is not present in list after filtering by campaign id\n" +
            "<ul>\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
                "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check filter job, restore and clone icons for deleted campaign")
    public void checkConditionallyDeletedCampaignsTeasers() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?campaign_id=" + 2015);
        Assert.assertFalse(pagesInit.getCabTeasers().isTeaserPresent(2015, 2010),
                "FAIL -> Teaser of 'dead' campaign is present!");
        log.info("Test is finished");
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check that statistics is present for deleted campaign\n" +
            "<ul>\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
                "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check that statistics is present for deleted campaign")
    public void checkConditionallyDeletedCampaignsStatistic() {
        log.info("Test is started");
        authCabAndGo(String.format("goodhits/campaigns-all-stat/client_id/%s?campaign_id=%s",2044, 2015));
        Assert.assertEquals(pagesInit.getCabAllStats().getStatisticsTotalClicks(), "10",
                "FAIL -> Statistics of 'dead' campaign is not present!");
        log.info("Test is finished");
    }

    @Feature("Approve teaser")
    @Story("Prohibition of approval of type B teasers for mgid")
    @Description("Check approve teaser with type b at mgid subnet\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52074\">Ticket TA-52074</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check approve teaser with type b at mgid subnet")
    public void checkApproveTeaserWithTypeBMgid() {
        log.info("Test is started");

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits/?campaign_id=" + 1244);

        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Forbidden to approve teasers with AD_type = B in the MGID subnet"), "FAIL - first check");
        log.info("Test is finished");
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Check filter auto approve\n" +
            "     <ul>\n" +
            "      <li>verified</li>\n" +
            "      <li>notVerified</li>\n" +
            "      <li>rejected</li>\n" +
            "      <li>notNeedVerification</li>\n" +
            "      <li>allAutoApproved</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check filter auto approve")
    public void checkAutoModerationFilter() {
        log.info("Test is started");
        List<String> idsEtalonVerified = Arrays.asList("1212", "1214", "1216");
        List<String> idsEtalonNotVerified = Arrays.asList("1218", "1220", "1222");
        List<String> idsEtalonRejected = Arrays.asList("1224", "1225", "1226");
        List<String> idsEtalonDoNotNeedModeration = Arrays.asList("1227", "1228", "1238", "1229", "1239");
        List<String> idsEtalonAutoModerated = Arrays.asList("1214", "1220", "1228", "1238");

        log.info("approve teaser");
        authCabAndGo("goodhits/ghits?campaign_id=1249,1250,1251");
        pagesInit.getCabTeasers().filterByAutoModeration("verified");
        softAssert.assertTrue(pagesInit.getCabTeasers().getListOfDislpayedTeaserIds().equals(idsEtalonVerified), "Fail - verified");

        authCabAndGo("goodhits/ghits?campaign_id=1249,1250,1251");
        pagesInit.getCabTeasers().filterByAutoModeration("notVerified");
        softAssert.assertTrue(pagesInit.getCabTeasers().getListOfDislpayedTeaserIds().equals(idsEtalonNotVerified), "Fail - notVerified");

        authCabAndGo("goodhits/ghits?campaign_id=1249,1250,1251");
        pagesInit.getCabTeasers().filterByAutoModeration("rejected");
        softAssert.assertTrue(pagesInit.getCabTeasers().getListOfDislpayedTeaserIds().equals(idsEtalonRejected), "Fail - rejected");

        authCabAndGo("goodhits/ghits?campaign_id=1249,1250,1251");
        pagesInit.getCabTeasers().filterByAutoModeration("notNeedVerification");
        softAssert.assertTrue(pagesInit.getCabTeasers().getListOfDislpayedTeaserIds().equals(idsEtalonDoNotNeedModeration), "Fail - idsEtalonDoNotNeedModeration");

        authCabAndGo("goodhits/ghits?campaign_id=1250");
        pagesInit.getCabTeasers().filterByAutoModeration("allAutoApproved");
        softAssert.assertTrue(pagesInit.getCabTeasers().getListOfDislpayedTeaserIds().equals(idsEtalonAutoModerated), "Fail - allAutoApproved");

        softAssert.assertAll();
        log.info("Test is finished");
    }
    @Epic(TEASERS)
    @Feature("Post moderation")
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check filter auto moderation campaign type/category\n" +
            "     <ul>\n" +
            "      <li>Search feed, trusted category(207)  -  notVerified</li>\n" +
            "      <li>Search feed, trusted category(113)  -  notNeedVerification</li>\n" +
            "      <li>Content, trusted category(228)  -  notVerified</li>\n" +
            "      <li>Content, trusted category(113)  -  notNeedVerification</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52259\">Ticket TA-52259</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check filter auto approve", dataProvider = "autoModerationFilterData")
    public void checkAutoModerationFilterCampaignTypeAndCategory(String teaserId, String category, String filterOption) {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits-edit-teaser/id/" + teaserId);
        pagesInit.getCabTeasers().setCategoryId(category).chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().filterByAutoModeration(filterOption);
        Assert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), 1,"Fail - amount");

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] autoModerationFilterData() {
        return new Object[][]{
                {"1376", "207", "notVerified"},
                {"1377", "112", "notNeedVerification"},
                {"1378", "228", "notNeedVerification"},
                {"1379", "210", "notNeedVerification"}
        };
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Change categories and check influence on 'verified_moderation' field\n" +
            "     <ul>\n" +
            "      <li>Start conditions: Product, 'verified_moderation'=4, trusted category.  Actions: not trusted. Exp.result:'verified_moderation'=1</li>\n" +
            "      <li>Start conditions: Push,    'verified_moderation'=4, trusted category. Actions:change category to not trusted Exp.result:'verified_moderation'=4</li>\n" +
            "      <li>Start conditions: Product, 'verified_moderation'=1, not trusted category. Actions:change category to  trusted Exp.result:'verified_moderation'=4</li>\n" +
            "      <li>Start conditions: Product, 'verified_moderation'=2,3, not trusted/trusted category. Actions:change category to  trusted/not trusted Exp.result:'verified_moderation'=2,3</li>\n" +
            "      <li>Start conditions: Product, 'verified_moderation'=0, not trusted category. Actions:change category to  trusted Exp.result:'verified_moderation'=0</li>\n" +
            "      <li>Start conditions: Content, 'verified_moderation'=1, not trusted category. Actions:change category to not trusted Exp.result:'verified_moderation'=4</li>\n" +
            "      <li>Start conditions: Content, 'verified_moderation'=1, not trusted category. Actions:change category to trusted Exp.result:'verified_moderation'=1</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Change categories and check influence on 'verified_moderation' field")
    public void checkAutoModerationStateDueChangingCategory() {
        log.info("Test is started");

        log.info("change to not trusted category for product teaser - expected changed 'verified moderation' field from 4 to 1");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1241");
        pagesInit.getCabTeasers().setCategoryId("145").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1241), "4", "FAIL - 1241");

        log.info("change to not trusted category for push teaser - expected not changed 'verified moderation' field stayed 4");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1242");
        pagesInit.getCabTeasers().setCategoryId("254").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1242), "4", "FAIL - 1242");

        log.info("change to trusted category for product teaser - expected changed 'verified moderation' field from 1 to 4");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1243");
        pagesInit.getCabTeasers().setCategoryId("103").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1243), "4", "FAIL - 1243");

        log.info("change to trusted category for product teaser - expected not changed 'verified moderation' field stayed 2");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1244");
        pagesInit.getCabTeasers().setCategoryId("103").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1244), "2", "FAIL - 1244");

        log.info("change to trusted category for product teaser - expected not changed 'verified moderation' field stayed 3");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1245");
        pagesInit.getCabTeasers().setCategoryId("111").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1245), "3", "FAIL - 1245");

        log.info("change to trusted category for product teaser - expected not changed 'verified moderation' field stayed 0");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1246");
        pagesInit.getCabTeasers().setCategoryId("111").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1246), "0", "FAIL - 1246");

        log.info("change to trusted category for content teaser - expected after change category to 'Fine Art' verification moderation = 4");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1247");
        pagesInit.getCabTeasers().setCategoryId("217").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1247), "4", "FAIL - 1247");

        log.info("change to trusted category for content teaser - expected not changed 'verified moderation' field stayed 1");
        authCabAndGo("goodhits/ghits-edit-teaser/id/1375");
        pagesInit.getCabTeasers().setCategoryId("228").chooseCategory();
        pagesInit.getCabTeasers().sendToModeration();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1375), "4", "FAIL - 1375");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Change categories and check influence on 'verified_moderation' field mass\n" +
            "     <ul>\n" +
            "      <li>Start conditions: 'verified_moderation'=4, trusted category. Actions:change category to not trusted. Exp.result:'verified_moderation'=1</li>\n" +
            "      <li>Start conditions: 'verified_moderation'=1, not trusted category. Actions:change category to trusted, Exp.result:'verified_moderation'=4</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Change categories and check influence on 'verified_moderation' field mass", dataProvider = "autoModerationStateDueChangingCategoryMass")
    public void checkAutoModerationStateDueChangingCategoryMass(int teaserId, String categoryId, String verifiedModeration) {
        log.info("Test is started");

        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE, "goodhits/ghits_edit_no_moderation");

            log.info("change to not trusted category for product teaser - expected changed 'verified moderation' field");
            authCabAndGo("goodhits/ghits?campaign_id=1255&id=" + teaserId);
            pagesInit.getCabTeasers().editCategoryInMass(categoryId);
            helpersInit.getMessageHelper().isSuccessMessagesCab();
            Assert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(teaserId), verifiedModeration, "FAIL - " + teaserId);
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE, "goodhits/ghits_edit_no_moderation");
        }
    }

    @DataProvider
    public Object[][] autoModerationStateDueChangingCategoryMass() {
        return new Object[][]{
                {1248, "145", "4"},
                {1249, "103", "4"}
        };
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Change categories and check influence on 'verified_moderation' field inline\n" +
            "     <ul>\n" +
            "      <li>Start conditions: 'verified_moderation'=4, trusted category. Actions:change category to not trusted. Exp.result:'verified_moderation'=1</li>\n" +
            "      <li>Start conditions: 'verified_moderation'=1, not trusted category. Actions:change category to trusted, Exp.result:'verified_moderation'=4</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Change categories and check influence on 'verified_moderation' field inline", dataProvider = "autoModerationStateDueChangingCategoryInline")
    public void checkAutoModerationStateDueChangingCategoryInline(int teaserId, String categoryId, String verifiedModeration) {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE, "goodhits/ghits_edit_no_moderation");

        log.info("change to not trusted category for product teaser - expected changed 'verified moderation' field");
        authCabAndGo("goodhits/ghits?campaign_id=1257&id=" + teaserId);
        pagesInit.getCabTeasers().editCategoryInline(categoryId);
        Assert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(teaserId), verifiedModeration, "FAIL - " + teaserId);
        log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE, "goodhits/ghits_edit_no_moderation");
        }
    }

    @DataProvider
    public Object[][] autoModerationStateDueChangingCategoryInline() {
        return new Object[][]{
                {1252, "145", "4"},
                {1253, "103", "4"}
        };
    }

    @Feature("Post moderation interface V 1.2")
    @Story("Post-moderation | trusted categories")
    @Description("Check verified icon\n" +
            "     <ul>\n" +
            "      <li>check off icon, click it, check on icon</li>\n" +
            "      <li>check on icon, click it, check off icon</li>\n" +
            "      <li>verified_moderation=1 --- reject --- check verified_moderation=3, icon hidden</li>\n" +
            "      <li>verified_moderation=4  --- check icon hidden</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52014\">Ticket TA-52014</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check verified icon")
    public void checkVerifiedIcon() {
        log.info("Test is started");

        log.info("check reverse icon off to on");
        authCabAndGo("goodhits/ghits?id=1254");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - 1254 grey");
        pagesInit.getCabTeasers().clickVerifyIcon();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1254), "2", "FAIL - 1254");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedVerifyIcon("Off"), "Fail - 1254 green");

        log.info("check reverse icon on to off");
        authCabAndGo("goodhits/ghits?id=1255");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedVerifyIcon("Off"), "Fail - 1255 grey");
        pagesInit.getCabTeasers().clickVerifyIcon();
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1255), "1", "FAIL - 1255");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - 1255 green");

        log.info("check hidden icon and changed verification field");
        authCabAndGo("goodhits/ghits?id=1256");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - 1256 grey");
        pagesInit.getCabTeasers().rejectTeaser("7");
        authCabAndGo("goodhits/ghits?id=1256");
        softAssert.assertEquals(operationMySql.getgHits1().getVerifiedModeration(1256), "3", "FAIL - 1256");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - 1256 green");

        log.info("check hidden icon");
        authCabAndGo("goodhits/ghits?id=1257");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedVerifyIcon("On"), "Fail - 1257 grey");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedVerifyIcon("Off"), "Fail - 1257 green");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Campaigns")
    @Feature("Ad name")
    @Story("Ad Name | Сampaigns with Content and Push types")
    @Description("Check cropping Advertiser name from landing\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52161\">Ticket TA-52161</a></li>\n" +
            "     </ul>")
    @Owner(value = "NIO")
    @Test (description = "Check cropping Advertiser name from landing")
    public void checkCroppedAdvertiseNameFromTeaserLanding() {
        log.info("Test is started");

        serviceInit.getDockerCli().runAndStopCron(LANDING_DOMAINS,  "-vvv");
        log.info("check reverse icon off to on");
        authCabAndGo("goodhits/ghits?id=1314");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName("1314"), "Connectiko","Fail - getTeasersAdvertiserName");

        authCabAndGo("goodhits/ghits?id=1315");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName("1315"), "Com","Fail - getTeasersAdvertiserName");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 and MOV formats for motion ads")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check by animation/video</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "   <li><p>NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check by animation/video")
    public void checkFilterByVideo() {
        log.info("Test is started");

        log.info("filter by certificate");
        authCabAndGo("goodhits/ghits?client_id=1100");
        pagesInit.getCabTeasers().filterByVideo("video");

        List<String> fromInterface = pagesInit.getCabTeasers().getListOfDislpayedTeaserIds();
        List<String> fromDb = operationMySql.getgHits1().getClientVideoTeasersIds(false, 1100);

        log.info("fromInterface - " + fromInterface);
        log.info("fromDb - " + fromDb);
        softAssert.assertTrue(fromInterface.containsAll(fromDb), "Fail - contains");
        softAssert.assertTrue(fromInterface.size() == fromDb.size(), "Fail - size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Mass action (cab)")
    @Feature("Mass change CPC block in teasers list (cab)")
    @Story("Teaser list (cab)")
    @Description("Check setting new teasers CPC by Mass action")
    @Owner("AIA")
    @Test(description = "Check setting new teasers CPC by Mass action")
    public void checkMassChangeCpcForTeasers() {
        log.info("Test is started");
        int newTeaserPrice = 10;
        authCabAndGo("goodhits/ghits?campaign_id=2049");
        pagesInit.getCabTeasers().setNewTeasersCpcMass(newTeaserPrice);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("For 2033, 2034 new CPC has been set for the chosen regions"),
                "FAIL -> Message after set teasers new price by Mass action");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeasersCpc(String.valueOf(newTeaserPrice)),
                "FAIL -> New Teasers CPC Mass!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Mass action (cab)")
    @Feature("Mass change CPC block in teasers list (cab)")
    @Story("Teaser list (cab)")
    @Description("Check increase teasers CPC by Mass action" +
            "Preconditions: g_hits_1.price_of_click = 50")
    @Owner("AIA")
    @Test(description = "Check increase teasers CPC by Mass action")
    public void checkMassIncreaseCpcForTeasers() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits?campaign_id=2050");
        pagesInit.getCabTeasers().increaseTeasersCpcMass(3);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("For 2035, 2036 new CPC has been set for the chosen regions"),
                "FAIL -> Message after increase teasers price by Mass action");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeasersCpc("53"),
                "FAIL -> Increase Teasers CPC Mass!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Mass action (cab)")
    @Feature("Mass change CPC block in teasers list (cab)")
    @Story("Teaser list (cab)")
    @Description("Check decrease teasers CPC by Mass action" +
            "Preconditions: g_hits_1.price_of_click = 50")
    @Owner("AIA")
    @Test(description = "Check decrease teasers CPC by Mass action")
    public void checkMassDecreaseCpcForTeasers() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits?campaign_id=2051");
        pagesInit.getCabTeasers().decreaseTeasersCpcMass(3);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("For 2037, 2038 new CPC has been set for the chosen regions"),
                "FAIL -> Message after decrease teasers price by Mass action");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeasersCpc("47"),
                "FAIL -> Decrease Teasers CPC Mass!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
