package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.CliCommandsList.Cron.MIRROR_IMAGE_CHANGE;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class MirroringImagesTest extends TestBase {

    @Epic(CLIENTS)
    @Feature(IMAGE_MIRRORING)
    @Story(CLIENT_GOODHITS_EDIT_INTERFACE)
    @Description("Check edit mirroring images option\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check edit mirroring images option")
    public void checkEditMirroringImagesOption() {
        log.info("Test is started");
        int clientId = 1001;

        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        pagesInit.getCabProductClients().setStateMirroringImagesCheckbox(true);
        pagesInit.getCabProductClients().saveClientsSettings();

        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfMirroringImagesCheckbox(true), "Fail - checkbox is disable");
        pagesInit.getCabProductClients().setStateMirroringImagesCheckbox(false);
        pagesInit.getCabProductClients().saveClientsSettings();

        authCabAndGo("goodhits/clients-edit/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabProductClients().checkStateOfMirroringImagesCheckbox(false), "Fail - checkbox is enable");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(IMAGE_MIRRORING)
    @Story(CLIENT_GOODHITS_EDIT_INTERFACE)
    @Description("Check privileges mirroring images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name = "goodhits/can_edit_client_allow_teaser_image_mirroring")
    @Test (description = "Check privileges mirroring images")
    public void checkRightMirroringImages() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_client_allow_teaser_image_mirroring");

        log.info("check right can_see_auto_approve");
        authCabForCheckPrivileges("goodhits/clients-edit/id/1001");
        Assert.assertFalse(pagesInit.getCabProductClients().isDisplayedMirroringImageCheckbox(), "FAIL - label should not be visible");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_MIRRORING)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check privileges mirroring images teasers\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name = "goodhits/toggle-mirror-image-flag")
    @Test (description = "Check privileges mirroring images teasers")
    public void checkRightMirroringImagesTeaserListIcon() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/toggle-mirror-image-flag");

        log.info("check right can_see_auto_approve");
        authCabForCheckPrivileges("goodhits/ghits/?id=1307");
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedMirroringImagesIcon(), "FAIL - label should not be visible");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_MIRRORING)
    @Stories({@Story(TEASER_LIST_CAB_INTERFACE), @Story(TEASER_QUARANTINE_LIST_INTERFACE)})
    @Description("Check changing state of mirroring images state\n" +
            "     <ul>\n" +
            "      <li>list interface</li>\n" +
            "      <li>moderation</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check changing state of mirroring images state")
    public void checkChangeStateMirroringImage() {
        log.info("Test is started");

        log.info("check right can_see_auto_approve");
        authCabAndGo("goodhits/ghits/?id=1307");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("On"), "Fail - On");
        softAssert.assertEquals(pagesInit.getCabTeasers().getMirroringImagesToolTip(), "Image can be mirrored", "Fail - On tooltip");


        pagesInit.getCabTeasers().clickMirroringImages();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("Off"), "Fail - Off");
        softAssert.assertEquals(pagesInit.getCabTeasers().getMirroringImagesToolTip(), "Image can not be mirrored", "Fail - Off tooltip");

        authCabAndGo("goodhits/on-moderation?id=1308");

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedMirroringImageIcon("On"), "Fail - Off moderation");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getMirroringImagesToolTip(), "Image can be mirrored", "Fail - On tooltip moderation");

        pagesInit.getCabTeasers().clickMirroringImages();
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("Off"), "Fail - Off moderation");
        softAssert.assertEquals(pagesInit.getCabTeasers().getMirroringImagesToolTip(), "Image can not be mirrored", "Fail - Off tooltip moderation");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_MIRRORING)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check changing state of mirroring images with forbidden tags\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check changing state of mirroring images with forbidden tags")
    public void checkChangeStateMirroringImageWithForbiddenTags() {
        log.info("Test is started");

        log.info("check right can_see_auto_approve");
        authCabAndGo("goodhits/ghits/?id=1309");

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("Off"), "Fail - Off");

        pagesInit.getCabTeasers().clickMirroringImages();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("It is forbidden to mirror teaser image for tags ho_chi_minh. Remove prohibited tags and re-install icon"), "Fail - message");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("Off"), "Fail - Off second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_MIRRORING)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check enable of mirroring images with allowed tags\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check enable of mirroring images with allowed tags")
    public void checkEnableMirroringImageWithAllowedTags() {
        log.info("Test is started");

        log.info("check right can_see_auto_approve");
        authCabAndGo("goodhits/ghits/?id=1310");

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("Off"), "Fail - Off");

        pagesInit.getCabTeasers().clickMirroringImages();

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("On"), "Fail - On second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_MIRRORING)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check disable of mirroring images with allowed tags\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52158\">Ticket TA-52158</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check disable of mirroring images with allowed tags")
    public void checkDisableMirroringImageWithAllowedTags() {
        log.info("Test is started");

        log.info("check right can_see_auto_approve");
        authCabAndGo("goodhits/ghits/?id=1311");

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("On"), "Fail - On");

        pagesInit.getCabTeasers().clickMirroringImages();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("It is forbidden to remove flag “Mirroring Image” in teasers that have tag sets involved in the mirroring algorithm."), "Fail - message");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon("On"), "Fail - On second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(IMAGE_MIRRORING), @Feature(TAGS)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check change Images mirroring state by forbidden/allowed tags\n" +
            "     <ul>\n" +
            "      <li>Set forbidden tags</li>\n" +
            "      <li>Set allowed tags</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52157\">Ticket TA-52157</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check change Images mirroring state by forbidden/allowed tags", dataProvider = "dataChangeImageMirroringStateByTags")
    public void checkChangeImageMirroringStateByTags(int teaserId, String tag, String state) {
        log.info("Test is started");

        log.info("set tags");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().setTagsType("image");
        pagesInit.getCabTeasers().setTags(tag);
        pagesInit.getCabTeasers().chooseTags(false, true);

        serviceInit.getDockerCli().runAndStopCron(MIRROR_IMAGE_CHANGE, "-vvv");

        log.info("check activity icon of mirroring");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        Assert.assertTrue(pagesInit.getCabTeasers().isDisplayedMirroringImageIcon(state), "Fail - first On");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataChangeImageMirroringStateByTags() {
        return new Object[][]{
                {1337, "politician", "On"},
                {1338, "ho_chi_minh", "Off"},
                {1339, "politician", "On"},
        };
    }
}
