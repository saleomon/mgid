package cab.products.teasers;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import testBase.TestBase;

public class CabTeasersMassActionsTest extends TestBase {

    @BeforeMethod
    public void initializeVariables() {
        softAssert = new SoftAssert();
    }

    /**
     * Валидация апрува/реджеката тизера с/без офера. Apply with approve
     * <ul>
     *  <li>Апрув кнопкой Apply with approve</li>
     *  <li>Проверка отображения иконок апрува</li>
     *  <li>Проверка отображения прикрепленных продуктиов к тизерам</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24673">Ticket TA-24673</a>
     * <p>NIO</p>
     */
    @Test
    public void checkApplyWithApproveTeaser() {
        log.info("Test is started");

        log.info("Approve teasers by mass actions");
        authCabAndGo("goodhits/ghits/?campaign_id=1053");
        pagesInit.getCabTeasers().approveTeaserMassAction();

        pagesInit.getCabTeasers().clickContinueWithAproveRejectButton();

        log.info("Check visibility of reject icon");
        authCabAndGo("goodhits/ghits/?campaign_id=1053");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - reject icon is displayed");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1052");
        softAssert.assertTrue(pagesInit.getScreenshotHistory().checkProductPresence("MassOffer"), "FAIL - product is displayed 1052");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1053");
        softAssert.assertFalse(pagesInit.getScreenshotHistory().checkProductPresence("MassOffer"), "FAIL - product is displayed 1053");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Валидация апрува/реджеката тизера с/без офера. Массовые действия. Continue without linking
     * <ul>
     *  <li>Апрув кнопкой Continue without linking</li>
     *  <li>Проверка отображения иконок апрува</li>
     *  <li>Проверка отображения прикрепленных продуктиов к тизерам</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24673">Ticket TA-24673</a>
     * <p>NIO</p>
     */
    @Test
    public void checkContinueWithoutLinkingOffer() {
        log.info("Test is started");

        log.info("Approve teasers by mass actions");
        authCabAndGo("goodhits/ghits/?campaign_id=1055");
        pagesInit.getCabTeasers().approveTeaserMassAction();

        pagesInit.getCabTeasers().clickContinueWithoutLinking();

        log.info("Check visibility of reject icon");
        authCabAndGo("goodhits/ghits/?campaign_id=1055");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - approve icon is displayed");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1056");
        softAssert.assertFalse(pagesInit.getScreenshotHistory().checkProductPresence("MassOfferContinue"), "FAIL - product is displayed 1056");

        authCabAndGo("goodhits/ghits-screenshots-history/id/1057");
        softAssert.assertFalse(pagesInit.getScreenshotHistory().checkProductPresence("MassOfferContinue"), "FAIL - product is displayed 1057");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
