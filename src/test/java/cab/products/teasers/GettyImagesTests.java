package cab.products.teasers;

import io.qameta.allure.*;
import libs.devtools.CloudinaryMock;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.*;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;

public class GettyImagesTests extends TestBase {
    String notificationNoImages = "No results found for the keywords. Try to use different keywords or check for spelling errors.";

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Stories({@Story(TEASER_CREATE_CAB_INTERFACE), @Story(TEASER_EDIT_CAB_INTERFACE)})
    @Description("Getty images| Cab | Images Gallery| forms\n" +
            "     <ul>\n" +
            "      <li>Create/Edit teaser with static or video view block using Getty Images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51867\">Ticket TA-51867</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Create/Edit teaser with image using Getty Images", dataProvider = "createTeaserGettyTypeOfBlockView")
    public void createEditDeleteTeaser(String typeOfFile, String requestGetty, String responseGettyFile, CloudinaryMock.ResponseWithImages responseCloudinaryEdit) {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        log.info("create teaser");

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestGetty, responseGettyFile);
        pagesInit.getCabTeasers().openStockGettyImageForm();
        pagesInit.getCabTeasers().selectTypeOfFormatTeaserViewBlock(typeOfFile);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);


        if(typeOfFile.equalsIgnoreCase("video")) serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-video-download-url*", "responseStockVideoUrl1");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait();
        serviceInit.getCloudinaryMock().countDownLatchAwait();

        softAssert.assertNotEquals(pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .isNeedToEditCategory(false)
                .setNeedToEditImage(false)
                .isSetImageFromRequest(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        log.info("check created teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInListInterface(), "FAIL -> CREATED teaser doesn't check in list interface");

        log.info("check created teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInCreateInterface(), "FAIL -> CREATED teaser doesn't check in edit interface");

        log.info("edit created teaser");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestGetty, responseGettyFile);
        pagesInit.getCabTeasers().openStockGettyImageForm();
        pagesInit.getCabTeasers().selectTypeOfFormatTeaserViewBlock(typeOfFile);
        serviceInit.getGettyImagesMock().countDownLatchAwait();

        if(typeOfFile.equalsIgnoreCase("video")) serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-video-download-url*", "responseStockVideoUrl2");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(responseCloudinaryEdit, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait();
        serviceInit.getCloudinaryMock().countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getCabTeasers().isGenerateNewValues(true)
                 .setNeedToEditImage(false)
                        .setNeedToEditCategory(true)
                 .isSetImageFromRequest(false)
                 .editTeaser(), "FAIL -> teaser doesn't edit");

        log.info("check EDITED teaser in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInListInterface(), "FAIL -> EDITED teaser doesn't check in list interface");

        log.info("check EDITED teaser in edit interface");
        authCabAndGo("goodhits/ghits-edit/id/" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserSettingsInCreateInterface(), "FAIL -> EDITED teaser doesn't check in edit interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] createTeaserGettyTypeOfBlockView() {
        return new Object[][]{
                {"static", "*ghits-get-stock-images*", "responseGettyImages",  GEERAF},
                {"video", "*ghits-get-stock-videos*", "responseGettyVideos",  HAPPY_WOMAN}
        };
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| forms\n" +
            "     <ul>\n" +
            "      <li>Check possibility of search images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51867\">Ticket TA-51867</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check possibility of search images")
    public void checkEditGettyImageSearchPhrase() {
        log.info("Test is started");
        String searchPhrase = "Yippee Ki-Yay";

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        log.info("Choose category");
        pagesInit.getCabTeasers().openStockGettyImageForm();
        pagesInit.getCabTeasers().deleteGettyImageSearchPhrases();
        pagesInit.getCabTeasers().inputGettyImageSearchPhrase(searchPhrase);

        Assert.assertEquals(pagesInit.getCabTeasers().getCategoryNameAtGettyImagesSearchForm(), searchPhrase);
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_EDIT_CAB_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| forms\n" +
            "     <ul>\n" +
            "      <li>Check message of missing images</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51867\">Ticket TA-51867</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check message of missing images")
    public void checkDisplayedMessageNoResults() {
        log.info("Test is started");
        String searchPhrase = "Салко";

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        log.info("Choose category");
        pagesInit.getCabTeasers().openStockGettyImageForm();
        pagesInit.getCabTeasers().deleteGettyImageSearchPhrases();

        log.info("edit created teaser");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-images*", "responseGettyImages_noImages");
        pagesInit.getCabTeasers().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        Assert.assertEquals(pagesInit.getCabTeasers().getStockGalleryNotification(), notificationNoImages);
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| inline editing\n" +
            "     <ul>\n" +
            "      <li>Check inline editing</li>\n" +
            "      <li>Check forwarding category</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51866\">Ticket TA-51866</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check inline editing forwarding with static media")
    public void checkInlineEditingWithGettyStatic() {
        log.info("Test is started");
        String searchPhrase = "Цибулька";

        authCabAndGo("goodhits/ghits/?id=1177");

        log.info("Choose category");
        pagesInit.getCabTeasers().openPopupEditImageInline();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        pagesInit.getCabTeasers().openStockGettyImageForm();
        softAssert.assertEquals(pagesInit.getCabTeasers().getCategoryNameAtGettyImagesSearchForm(), "Events", "Fail - wrong category");
        pagesInit.getCabTeasers().deleteGettyImageSearchPhrases();

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-images*", "responseGettyImages");
        pagesInit.getCabTeasers().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPINESS, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().clickFirstImageGettyStock();
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasers().savePopupImageSettings();

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedTeaserImageBlock(), "Fail - isDisplayedTeaserImage");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| inline editing\n" +
            "     <ul>\n" +
            "      <li>Check inline editing</li>\n" +
            "      <li>Check forwarding category</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51866\">Ticket TA-51866</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check inline editing forwarding with static media")
    public void checkInlineEditingWithGettyVideo() {
        log.info("Test is started");
        String searchPhrase = "Цибулька";

        authCabAndGo("goodhits/ghits/?id=1345");

        log.info("Choose category");
        pagesInit.getCabTeasers().openPopupEditImageInline();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        pagesInit.getCabTeasers().openStockGettyImageForm();
        softAssert.assertEquals(pagesInit.getCabTeasers().getCategoryNameAtGettyImagesSearchForm(), "Diabetes", "Fail - wrong category");
        pagesInit.getCabTeasers().deleteGettyImageSearchPhrases();

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-videos*", "responseGettyVideos");
        pagesInit.getCabTeasers().selectTypeOfFormatTeaserViewBlock("video");
        pagesInit.getCabTeasers().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-video-download-url*", "responseStockVideoUrl1");

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().clickFirstImageGettyStock();
        serviceInit.getGettyImagesMock().countDownLatchAwait();
        serviceInit.getCloudinaryMock().countDownLatchAwait();

        pagesInit.getCabTeasers().savePopupImageSettings();

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedTeaserVideoBlock(), "Fail - isDisplayedTeaserImage");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Check filtering by and pagination\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52214\">Ticket TA-52214</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Flaky
    // toDo NIO  https://jira.mgid.com/browse/CP-2299 Zhynzher
    //@Test (description = "Check filtering by and pagination")
    public void checkFilteringOfGettyAndPaginationVideo() {
        log.info("Test is started");

        String fileFormat = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"video", "static"});
        String requestName = fileFormat.equalsIgnoreCase("video") ? "*ghits-get-stock-videos*":"*ghits-get-stock-images*";
        String responseFile = fileFormat.equalsIgnoreCase("video") ? "responseGettyVideos":"responseGettyImages";
        String sortBy = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"most_popular", "newest"});
        log.info("fileFormat - " + fileFormat);
        log.info("sortBy - " + sortBy);

        authCabAndGo("goodhits/ghits-add-teaser/campaign_id/" + CAMPAIGN_MGID_PRODUCT);

        log.info("Open form");
        pagesInit.getCabTeasers().openStockGettyImageForm();

        log.info("Choose type of format media content");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestName, responseFile);
        pagesInit.getCabTeasers().selectTypeOfFormatTeaserViewBlock(fileFormat);
        pagesInit.getCabTeasers().selectSortFilesForViewBlockBy(sortBy);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        log.info("Check parameters in requests");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl(sortBy), sortBy, "Fail - error sortBy");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl("page=1"), "page=1", "Fail - error page=1");
        softAssert.assertEquals(pagesInit.getCabTeasers().getActiveGettyImageMediaPage(), "1", "Fail - active pag 1");

        log.info("Action with pagination");
        serviceInit.getGettyImagesMock().prepareGettyImagesResponse(requestName, responseFile);
        pagesInit.getCabTeasers().clickNextPageOfGalleryFiles();
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        log.info("Check parameter in requests and active page");
        softAssert.assertEquals(serviceInit.getGettyImagesMock().getRequestedParameterUrl("page=2"), "page=2", "Fail - error page=2");
        softAssert.assertEquals(pagesInit.getCabTeasers().getActiveGettyImageMediaPage(), "2", "Fail - active pag 2");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(GETTY_IMAGES)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("Getty images| Cab | Images Gallery| inline editing\n" +
            "     <ul>\n" +
            "      <li>Check inline editing with forwarding moderation</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51866\">Ticket TA-51866</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check inline editing with forwarding moderation")
    public void checkInlineEditingWithGettyModeration() {
        log.info("Test is started");
        String searchPhrase = "Цибулька";

        authCabAndGo("goodhits/on-moderation/id/1177");

        log.info("Choose category");
        pagesInit.getCabTeasersModeration().openPopupEditImageInline();
        helpersInit.getMessageHelper().checkCustomMessagesCab("");
        pagesInit.getCabTeasersModeration().openStockGettyImageForm();
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getCategoryNameAtGettyImagesSearchForm(), "Events", "Fail - wrong category 1");
        pagesInit.getCabTeasersModeration().deleteGettyImageSearchPhrases();

        serviceInit.getGettyImagesMock().prepareGettyImagesResponse("*ghits-get-stock-images*", "responseGettyImages");
        pagesInit.getCabTeasersModeration().inputGettyImageSearchPhrase(searchPhrase);
        serviceInit.getGettyImagesMock().countDownLatchAwait(1);

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPINESS, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasersModeration().clickFirstImageGettyStock();
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getCategoryNameAtGettyImagesSearchForm(), searchPhrase, "Fail - wrong category " + searchPhrase);

        pagesInit.getCabTeasersModeration().savePopupImageSettings();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedTeaserImage(), "Fail - isDisplayedTeaserImage");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
