package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.helpers.BaseHelper;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.POPULATE_TEASER_TAGS;
import static libs.dockerClient.base.DockerKafka.Topics.TEASER_LANDINGS;
import static testData.project.CliCommandsList.Consumer.TEASERS_LANDING_TAGS;
import static testData.project.CliCommandsList.Consumer.TEASERS_UPDATE_TAGS;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;
import static testData.project.ClientsEntities.TEASER_MGID_APPROVE;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabTeasersTagsTests extends TestBase {

    @BeforeMethod
    public void initializeVariables() {
        operationMySql.getgHitsTags().deleteAllTags(TEASER_MGID_APPROVE, 23, 24);
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeTitleInListInterface() {
        log.info("Test is started");
        serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
        authCabAndGo("goodhits/ghits/?id=1510");


        log.info("записываем тип тегов в массив");
        pagesInit.getCabTeasers().setTagsType("title");

        log.info("проставляем рандомные теги для TITLE_CHECK");
        pagesInit.getCabTeasers().chooseTags(false, false);
        serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

        log.info("После inline изменения title - нажимаем SAVE проверяем что теги не перетираются и отображаются");
        pagesInit.getCabTeasers().editTitleInline();
        authCabAndGo("goodhits/ghits/?id=1510");
        pagesInit.getCabTeasers().openTagsCloudForm();
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedTitleTagsBlockSingle(), "FAIL -> isDisplayedTitleTagsBlock");

        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeTitleInEditInterface() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=" + TEASER_MGID_APPROVE);

        serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");

        log.info("записываем тип тегов в массив");
        pagesInit.getCabTeasers().setTagsType("title");

        log.info("проставляем рандомные теги для TITLE_CHECK");
        pagesInit.getCabTeasers().chooseTags(false, false);

        serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

        log.info("переходим в интерфейс редактирования тизера");
        authCabAndGo("goodhits/ghits-edit/id/" + TEASER_MGID_APPROVE);
        pagesInit.getCabTeasers()
                .useTitle(true)
                .isSetSomeFields(true)
                .setTitle("new title" + BaseHelper.getRandomWord(2))
                .editTeaser();

        log.info("После редактироания title - нажимаем SAVE проверяем что теги не перетираются и отображаются");
        authCabAndGo("goodhits/ghits/?id=" + TEASER_MGID_APPROVE);
        pagesInit.getCabTeasers().openTagsCloudForm();
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedTitleTagsBlockSingle(), "FAIL -> isDisplayedTitleTagsBlock");

        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeLandingTypeInline() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=" + TEASER_MGID_APPROVE);

        log.info("записываем тип тегов в массив");
        pagesInit.getCabTeasers().setTagsType("lp");

        log.info("проставляем рандомные теги для LP_CHECK");
        pagesInit.getCabTeasers().chooseTags(false, false);

        log.info("После inline изменения lp - нажимаем SAVE проверяем что теги не перетираются и отображаются");
        pagesInit.getCabTeasers().editLandingInline(false);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> изменение lp + нажимаем SAVE: теги не отображаются");

        log.info("После inline изменения lp - нажимаем DELETE проверяем что выбранные теги не отображаются");
        pagesInit.getCabTeasers().editLandingInline(true);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(true), "FAIL -> изменение lp + нажимаем DELETE: теги отображаются");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeLandingTypeOnMassAction() {
        log.info("Test is started");
        try {
            authCabAndGo("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_PRODUCT + "&id=" + TEASER_MGID_APPROVE);
            serviceInit.getDockerCli().runConsumer(TEASERS_LANDING_TAGS, "-vvv");

            log.info("записываем тип тегов в массив");
            pagesInit.getCabTeasers().setTagsType("lp");

            log.info("проставляем рандомные теги для LP_CHECK");
            pagesInit.getCabTeasers().chooseTags(false, false);

            log.info("После массового изменения LandingType - нажимаем SAVE проверяем что теги не перетираются и отображаются");
            pagesInit.getCabTeasers().changeMassLandingType();
            pagesInit.getCabTeasers().clickButtonSaveTags();
            helpersInit.getMessageHelper().isSuccessMessagesCab();
            helpersInit.getMessageHelper().cleanJsMessage();
            helpersInit.getBaseHelper().refreshCurrentPage();
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(TEASER_LANDINGS);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> изменение LandingType + нажимаем SAVE: LP теги не отображаются");

            log.info("После массового изменения LandingType - нажимаем DELETE проверяем что выбранные теги не отображаются");
            pagesInit.getCabTeasers().changeMassLandingType(pagesInit.getCabTeasers().getRandomNotSelectedLandingType());
            pagesInit.getCabTeasers().clickButtonDeleteTags();
            helpersInit.getMessageHelper().isSuccessMessagesCab();
            helpersInit.getBaseHelper().refreshCurrentPage();
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(TEASER_LANDINGS);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(true), "FAIL -> изменение LandingType + нажимаем DELETE: LP теги отображаются");

            softAssert.assertAll();
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void searchTagsInFormTeaser() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits/?id=" + TEASER_MGID_APPROVE);
        Assert.assertTrue(pagesInit.getCabTeasers().checkWorkSearchingTagsFilter());
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudForTeaser() {
        log.info("Test is started");

        authCabAndGo("goodhits/ghits/?id=" + TEASER_MGID_APPROVE);

        log.info("записываем тип тегов в массив");
        pagesInit.getCabTeasers().setTagsType("title", "description", "lp");

        log.info("проставляем рандомные теги для TITLE_CHECK, IMAGE_CHECK, LP_CHECK");
        pagesInit.getCabTeasers().chooseTags(false, false);

        log.info("проверяем проставленные теги в форме для TITLE_CHECK, IMAGE_CHECK, LINK_CHECK");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> изменение title, image, lp: теги не отображаются");

        log.info("открываем форму, снимаем все активные теги и сохраняем форму");
        pagesInit.getCabTeasers().clearAllTags();

        log.info("проверяем что все теги снялись и форма очистилась");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(true), "FAIL -> очистка тегов title, image, lp: теги отображаются");

        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeImageTagsAndCheckThisInTeaserWithSameImage() {
        log.info("Test is started");

        try {
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            authCabAndGo("goodhits/ghits/?id=" + 23);

            log.info("записываем тип тегов в массив");
            pagesInit.getCabTeasers().setTagsType("image");

            log.info("проставляем рандомные теги для IMAGE_CHECK");
            pagesInit.getCabTeasers().chooseTags(false, false);
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            authCabAndGo("goodhits/ghits/?id=" + 24);

            log.info("проверяем проставленные теги в форме для IMAGE_CHECK для другого тизера(24) но с таким же хешом картинки");
            Assert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> image: теги не отображаются");
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Description(
            "     <ul>\n" +
            "      <li>Change image tags for teaser 1 g_hits_1.g_hits_image_hashes_id = 6, g_hits_tags = 8,9,10</li>\n" +
            "      <li>Similar image for teaser 2 to teaser 1 g_hits_1.g_hits_image_hashes_id = 6 </li>\n" +
            "      <li>Similar image for teaser 3 to teaser 1 g_hits_1.g_hits_image_hashes_id = 6, g_hits_tags = 9 </li>\n" +
            "      <li>Similar image for teaser 4 to teaser 1 g_hits_1.g_hits_image_hashes_id = 7, g_hits_tags = 8,9,10,11 </li>\n" +
            "      <li>Similar image for teaser 5 to teaser 1 g_hits_1.g_hits_image_hashes_id = 7, g_hits_tags = 5,6,7 </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52357\">Ticket TA-52357</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check inheritance image tags for related and similar images")
    public void checkInheritanceTagsForTeasersWithSimilarAndRelatedImages() {
        log.info("Test is started");

        try {
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            authCabAndGo("goodhits/ghits/?id=" + 1429);

            log.info("set tags type and custom tags");
            pagesInit.getCabTeasers().setTagsType("image");
            pagesInit.getCabTeasers().setTags("couple_in_bed", "marijuana", "breast");

            log.info("choose custom tags");
            pagesInit.getCabTeasers().chooseTags(false, true);
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            log.info("check inheritance tag for teasers");
            authCabAndGo("goodhits/ghits/?id=" + 1430);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL ->  1430");

            authCabAndGo("goodhits/ghits/?id=" + 1431);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL ->  1431");

            authCabAndGo("goodhits/ghits/?id=" + 1432);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL ->  1432");

            authCabAndGo("goodhits/ghits/?id=" + 1433);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL ->  1433");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Description(
            "     <ul>\n" +
            "      <li>Change image tags for teaser 1 g_hits_1.g_hits_image_hashes_id = 10, g_hits_tags(image) = 8,9,10; g_hits_tags(lp) = 37; g_hits_tags(title) = 54; g_hits_tags(description) = 144</li>\n" +
            "      <li>Similar image for teaser 2 to teaser 1 g_hits_1.g_hits_image_hashes_id = 10, g_hits_tags(lp) = 48; g_hits_tags(description) = 144; g_hits_tags(title) = 144 </li>\n" +
            "      <li>Similar image for teaser 3 to teaser 1 g_hits_1.g_hits_image_hashes_id = 11, g_hits_tags(lp) = 48; g_hits_tags(description) = 144; g_hits_tags(title) = 144</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52357\">Ticket TA-52357</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check influence changing image tags on different type of tags")
    public void checkInfluenceDueChangingImageTagsOnAnotherTypesOfTags() {
        log.info("Test is started");

        try {
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            authCabAndGo("goodhits/ghits/?id=" + 1439);

            log.info("set tags type and custom tags");
            pagesInit.getCabTeasers().setTagsType("image");
            pagesInit.getCabTeasers().setTags("couple_in_bed", "marijuana", "breast");

            log.info("choose custom tags");
            pagesInit.getCabTeasers().chooseTags(false, true);
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            log.info("set tags that inserted at dump for teasers");
            pagesInit.getCabTeasers().setChosenTags("lp", "fb_comments");
            pagesInit.getCabTeasers().setChosenTags("description", "dollar");
            pagesInit.getCabTeasers().setChosenTags("title", "quarantine");

            log.info("check influence changing image tags on different type of tags");
            authCabAndGo("goodhits/ghits/?id=" + 1440);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> image: теги не отображаются - 1440");

            authCabAndGo("goodhits/ghits/?id=" + 1441);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> image: теги не отображаются - 1441");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test
    public void tagsCloudChangeLpTagsOnMassAction() {
        try {
            log.info("Test is started");
            authCabAndGo("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_PRODUCT + "&id=" + TEASER_MGID_APPROVE);

            log.info("записываем тип тегов в массив");
            pagesInit.getCabTeasers().setTagsType("lp");

            log.info("проставляем рандомные теги для LP_CHECK");
            pagesInit.getCabTeasers().chooseTags(true, false);
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            helpersInit.getBaseHelper().refreshCurrentPage();
            log.info("проверяем что проставленные теги отображаются");
            Assert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> LP теги не отображаются");
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Owner  (NIO)
    @Test(dependsOnMethods = "tagsCloudChangeLpTagsOnMassAction")
    @Privilege(name = "goodhits/can_edit_tags")
    public void tagsCloudCheckRightCanEditTags() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_tags");
            authCabForCheckPrivileges("goodhits/ghits/?campaign_id=" + CAMPAIGN_MGID_PRODUCT + "&id=" + TEASER_MGID_APPROVE);
            Assert.assertFalse(pagesInit.getCabTeasers().checkExistCustomValueInMassActions("changeTags"), "element visible");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_edit_tags");
        }
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Description("Check presence title tags block with filtering by mass\n" +
            "     <ul>\n" +
            "      <li>by campaign id</li>\n" +
            "      <li>by teaser title</li>\n" +
            "      <li>by client id</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51860\">Ticket TA-51860</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check presence title tags block with filtering by mass")
    public void checkPresenceTitleTagsCloudBlockMassAction() {
        log.info("Test is started");

        log.info("Check presence due clients filter");
        authCabAndGo("goodhits/ghits/?client_id=1000");
        pagesInit.getCabTeasers().openTagsCloudFormByMass();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedTitleTagsBlockMass(), "FAIL - client_id");

        log.info("Check presence due title teaser filter");
        authCabAndGo("goodhits/ghits/?keywordTitle=Special+title");
        pagesInit.getCabTeasers().openTagsCloudFormByMass();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedTitleTagsBlockMass(), "FAIL - keywordTitle");

        log.info("Check presence due campaign filter");
        authCabAndGo("goodhits/ghits/?campaign_id=1278");
        pagesInit.getCabTeasers().openTagsCloudFormByMass();
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedTitleTagsBlockMass(), "FAIL - campaign_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_LIST_CAB_INTERFACE)
    @Description("Check tags inheritance icon and mass action\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52224\">Ticket TA-52224</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check tags inheritance icon and mass action", dataProvider = "dataForTagsInheritance")
    public void checkInheritanceTagsByTitle(int campaignId, int teaserDonorId, int teaserRecipient1Id, int teaserRecipient2Id, int teaserRecipient3Id, boolean isMassAction) {
        try {
            log.info("Test is started");
                authCabAndGo("goodhits/ghits/?campaign_id=" + campaignId + "&id=" + teaserDonorId);

            log.info("Set of changed tags types");
            pagesInit.getCabTeasers().setTagsType("title");

            log.info("Set random tags");
            pagesInit.getCabTeasers().chooseTags(isMassAction, false);
            pagesInit.getCabTeasers().setChosenTags("title", "strikethrough");
            pagesInit.getCabTeasers().setChosenTags("title", "euro");
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);
            sleep(2000);

            helpersInit.getBaseHelper().refreshCurrentPage();

            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> donor");

            authCabAndGo("goodhits/ghits/?id=" + teaserRecipient1Id);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> recipient 1");

            authCabAndGo("goodhits/ghits/?id=" + teaserRecipient2Id);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> recipient 2");

            authCabAndGo("goodhits/ghits/?id=" + teaserRecipient3Id);
            softAssert.assertFalse(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> recipient 3");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @DataProvider
    public Object[][] dataForTagsInheritance() {
        return new Object[][]{
                {1349, 1350, 1351, 1352, 1357, false}
        };
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check tags inheritance Zion\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52224\">Ticket TA-52224</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test (description = "Check tags inheritance Zion")
    public void checkInheritanceTagsByTitleZion() {
        try {
            log.info("Test is started");
            authCabAndGo("goodhits/on-moderation?id=1353&campaign_id=1352");

            log.info("Set of changed tags types");
            pagesInit.getCabTeasersModeration().clickTitleCheckButton();

            log.info("Set random tags");
            pagesInit.getCabTeasersModeration().chooseRandomTags("title");
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            helpersInit.getBaseHelper().refreshCurrentPage();

            pagesInit.getCabTeasersModeration().clickTitleCheckButton();
            softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", false), "FAIL -> donor");

            authCabAndGo("goodhits/on-moderation?id=1354");
            pagesInit.getCabTeasersModeration().clickTitleCheckButton();
            softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", false), "FAIL -> recipient 1");

            authCabAndGo("goodhits/on-moderation?id=1355");
            pagesInit.getCabTeasersModeration().clickTitleCheckButton();
            softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", false), "FAIL -> recipient 2");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TITLE_TAGS_VERIFICATION_INTERFACE)
    @Description("Check set differ tags for teasers with the same title\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52236\">Ticket TA-52236</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check set differ tags for teasers with the same title")
    public void checkSetDifferTagsForTeasersWithTheSameTitle() {
        try {
            log.info("Test is started");

            authCabAndGo("goodhits/titles-tags-verification?title=Some+non+uniq+title+for+ok");

            pagesInit.getCabTitleTagsVerification().clickOkAction();

            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            authCabAndGo("goodhits/titles-tags-verification?title=Some+non+uniq+title+for+ok");
            softAssert.assertTrue(pagesInit.getCabTitleTagsVerification().isEmptyTable(), "Fail - amount");

            pagesInit.getCabTeasers().setTagsType("title");
            pagesInit.getCabTeasers().setTags("msn_public", "adderall", "old_granny");

            authCabAndGo("goodhits/ghits/?id=1360");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 1");

            authCabAndGo("goodhits/ghits/?id=1361");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 2");

            authCabAndGo("goodhits/ghits/?id=1362");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 3");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TITLE_TAGS_VERIFICATION_INTERFACE)
    @Description("Check set custom tags for teasers with the same title\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52236\">Ticket TA-52236</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check set custom tags for teasers with the same title")
    public void checkSetCustomTagsForTeasersWithTheSameTitle() {
        try {
            log.info("Test is started");
            String[] newTags = new String[]{"quarantine", "old_granny"};

            authCabAndGo("goodhits/titles-tags-verification?title=not+ok");

            pagesInit.getCabTitleTagsVerification().clickNotOkAction();
            pagesInit.getCabTitleTagsVerification().chooseCustomTags(newTags);

            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            authCabAndGo("goodhits/titles-tags-verification?title=not+ok");
            softAssert.assertTrue(pagesInit.getCabTitleTagsVerification().isEmptyTable(), "Fail - amount");

            pagesInit.getCabTeasers().setTagsType("title");
            pagesInit.getCabTeasers().setTags(newTags);

            authCabAndGo("goodhits/ghits/?id=1363");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 1");

            authCabAndGo("goodhits/ghits/?id=1364");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 2");

            authCabAndGo("goodhits/ghits/?id=1365");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 3");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TITLE_TAGS_VERIFICATION_INTERFACE)
    @Description("Check ignore tags for teasers with the same title\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52236\">Ticket TA-52236</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check ignore tags for teasers with the same title")
    public void checkIgnoreTagsForTeasersWithTheSameTitle() {
        try {
            log.info("Test is started");

            authCabAndGo("goodhits/titles-tags-verification?title=for+ignore");

            pagesInit.getCabTitleTagsVerification().clickIgnoreAction();

            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            authCabAndGo("goodhits/titles-tags-verification?title=for+ignore");
            softAssert.assertTrue(pagesInit.getCabTitleTagsVerification().isEmptyTable(), "Fail - amount");

            pagesInit.getCabTeasers().setTagsType("title");
            pagesInit.getCabTeasers().setTags("adderall", "old_granny");
            authCabAndGo("goodhits/ghits/?id=1367");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 1");

            pagesInit.getCabTeasers().setTagsType("title");
            pagesInit.getCabTeasers().setTags("adderall", "old_granny", "msn_public");
            authCabAndGo("goodhits/ghits/?id=1368");
            softAssert.assertTrue(pagesInit.getCabTeasers().checkChoosenTags(false), "FAIL -> checkChoosenTags 2");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TITLE_TAGS_VERIFICATION_INTERFACE)
    @Description("Check filters\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52236\">Ticket TA-52236</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check filters")
    public void checkFiltersTitleTagsVerification() {
        log.info("Test is started");
        String firstTitle = "по мові";

        authCabAndGo("goodhits/titles-tags-verification");
        pagesInit.getCabTitleTagsVerification().fillTitle(firstTitle).selectLanguage("uk").filter();

        softAssert.assertTrue(pagesInit.getCabTitleTagsVerification().isDisplayedRowWithTitle(firstTitle), "Fail - title 1");
        softAssert.assertEquals(pagesInit.getCabTitleTagsVerification().getAmountOfDisplayedRows(), "1", "Fail - amount 1");

        authCabAndGo("goodhits/titles-tags-verification");
        pagesInit.getCabTitleTagsVerification().fillTitle(firstTitle).selectTranslationLanguage("en").filter();

        softAssert.assertTrue(pagesInit.getCabTitleTagsVerification().isDisplayedRowWithTitle(firstTitle), "Fail - title 2");
        softAssert.assertEquals(pagesInit.getCabTitleTagsVerification().getAmountOfDisplayedRows(), "1", "Fail - amount 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TITLE_TAGS_VERIFICATION_INTERFACE)
    @Description("Check link with the same title for teasers\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52236\">Ticket TA-52236</a></li>\n" +
            "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check link with the same title for teasers")
    public void checkLinkRedirectionToTeasersWithTheSameTitle() {
        log.info("Test is started");
        String href = "http://local-admin.mgid.com/cab/goodhits/ghits/?title_duplicates=Check+filter+by+translation";

        authCabAndGo("goodhits/titles-tags-verification?title=by+translation");
        Assert.assertEquals(pagesInit.getCabTitleTagsVerification().getLinkWithRedirection(), href);

        log.info("Test is finished");
    }
}
