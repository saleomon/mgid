package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;
import testData.project.CliCommandsList;

import static core.helpers.BaseHelper.randomNumbersInt;
import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.BULK_ACTION_UNBLOCK_BATCHES;
import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static pages.cab.products.variables.CabTeaserVariables.messageMissingAutocompliant;
import static testData.project.CliCommandsList.Consumer.*;

public class CompliantTeaser1Tests extends TestBase {
    private final String subjectLetter = "Results of copying ads";
    private final String messageErrorOfUnblocking = "\n" +
            "Couldn't unblock the following ads https://admin.mgid.com/cab/goodhits/ghits/?id=%s .";

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check filter compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check filter compliant")
    public void checkFilterCompliant() {
        log.info("Test is started");
        CabTeasers.CompliantType [] arrayCountries = new CabTeasers.CompliantType[]{RAC, ZPL, IAP, AUTOCONTROL, POLAND_REG_COMPLIANT};
        int randomIndex = randomNumbersInt(arrayCountries.length);

        authCabAndGo("goodhits/ghits");
        pagesInit.getCabTeasers().filterByComplient(arrayCountries[randomIndex].getCompliantType() + "_1");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkCampaignsCompliantTitlesAllMatch(arrayCountries[randomIndex]), "FAIL - include filters");

        log.info("Filter by country - " + arrayCountries[randomIndex]);
        authCabAndGo("goodhits/ghits");
        pagesInit.getCabCampaigns().filterByComplient(arrayCountries[randomIndex].getCompliantType() + "_0");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkCampaignsCompliantTitlesAnyMatch(arrayCountries[randomIndex]), "FAIL - exclude filters");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkWorkCompliantPopupData() {
        return new Object[][]{
                {1218, 1178},
                {1219, 1179}
        };
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check selected compliant and edit it\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check selected compliant and edit it", dataProvider = "checkWorkCompliantPopupData")
    public void checkWorkCompliantPopup(int campaignId, int teaserId) {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, arrayCountries);

        log.info("Check all geo icon and selected checkboxes");
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAllChosenCompliantIconVisibility(), "FAIL - multiple icon");
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(RAC, ZPL, IAP, AUTOCONTROL, POLAND_REG_COMPLIANT), "FAIL - selected compliant all");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(5), "FAIL - amount of displayed countries");

        log.info("Check part geo icon and selected checkboxes after editing");
        pagesInit.getCabTeasers().submitCompliantWithCheckboxes(ZPL, IAP, AUTOCONTROL);
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL - message");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkPartChosenCompliantIconVisibility(), "FAIL - part icon");
        pagesInit.getCabTeasers().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(ZPL, IAP, AUTOCONTROL), "FAIL - selected compliant part");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkSelectedCompliantGeo(POLAND_REG_COMPLIANT), "FAIL - selected POLAND_REG_COMPLIANT");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkSelectedCompliantGeo(RAC), "FAIL - selected RAC");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check icon for precise compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check icon for precise compliant", dataProvider = "checkWorkCompliantPopupData")
    public void checkWorkCompliantIcon(int campaignId, int teaserId) {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};
        CabTeasers.CompliantType[] arrayCompliant = new CabTeasers.CompliantType[]{AUTOCONTROL, IAP, RAC, ZPL, POLAND_REG_COMPLIANT};
        int randomIndex = randomNumbersInt(arrayCountries.length);

        log.info("Check country icon for one geo - " + arrayCountries[randomIndex]);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, arrayCountries[randomIndex]);
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, arrayCompliant[randomIndex].getCompliantType());
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        Assert.assertTrue(pagesInit.getCabTeasers().checkCompliantCountryIcon(arrayCountries[randomIndex]), "FAIL - country icon - " + arrayCountries[randomIndex]);

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check icon for precise compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check icon for precise compliant", dataProvider = "checkDisableCompliantCampaignFlagData")
    public void checkDisableCompliantCampaignFlag(int campaignId, int teaserId) {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, arrayCountries);

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        log.info("Unmark AUTOCOMPLETE compliant");
        pagesInit.getCabTeasers().openCompliantPopup();
        pagesInit.getCabTeasers().submitCompliantWithCheckboxes(ZPL, IAP, RAC);

        log.info("Check not selected AUTOCOMPLETE at campaign interface");
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(ZPL, IAP, RAC), "FAIL - not selected compliant");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(3), "FAIL - amount of selected option wrong");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkDisableCompliantCampaignFlagData() {
        return new Object[][]{
                {1074, 1072},
                {1068, 1071}
        };
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(BLOCK_UNBLOCK)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check unblock teaser with type b without compliant flag\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51673\">Ticket TA-51673</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Flaky
    @Test(description = "Check unblock teaser with type b without compliant flag")
    public void checkUnblockTeaserTypeBWithCompliantAtCampaign() {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1075, arrayCountries);

        authCabAndGo("goodhits/ghits/?id=" + 1073);
        softAssert.assertTrue(pagesInit.getCabTeasers().unBlockTeaser(), "FAIL - teaser blocked");

        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            authCabAndGo("goodhits/ghits/?id=" + 1074 + "&campaign_id=1075");
            pagesInit.getCabTeasers().unblockTeaserByMassAction();
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UNBLOCK, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_UNBLOCK_BATCHES);
            helpersInit.getBaseHelper().refreshCurrentPage();
            softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserUnblocked(), "FAIL - blocked by mass");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(BLOCK_UNBLOCK)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check unblock teaser without compliant flag\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51673\">Ticket TA-51673</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Flaky
    @Test(description = "Check unblock teaser without compliant flag")
    public void checkUnblockTeaserWithCompliantAtCampaign() {
        log.info("Test is started");
        int teaserId = 1075;
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1075, arrayCountries);
        operationMySql.getMailPull().getCountLettersByBody(String.format(messageErrorOfUnblocking, teaserId));

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabTeasers().unBlockTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Teaser you’re trying to unblock is not RAC/Autocontrol/Poland Reg/IAP compliant"), "FAIL - message");
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByAccountManager(), "FAIL - blocked by");

        authCabAndGo("goodhits/ghits/?id=" + teaserId + "&campaign_id=" + teaserId);
        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");

            pagesInit.getCabTeasers().unblockTeaserByMassAction();
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_UNBLOCK, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_UNBLOCK_BATCHES);
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_HANDLER, "-vvv");

            softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserBlockedByAccountManager(), "FAIL - blocked by mass");
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(messageErrorOfUnblocking, teaserId)), "Fail - checkCustomMailBody");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(COMPLIANT)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check unblock teaser without compliant flag\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51673\">Ticket TA-51673</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check presence compliant icon with type b and other type")
    public void checkCompliantIconVisibility() {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1076, arrayCountries);

        log.info("check visibility compliant icon teaser landing B");
        authCabAndGo("goodhits/ghits/?id=" + 1077);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedCompliantIcon(), "FAIL - icon displayed teaser list 1");

        log.info("check visibility compliant icon campaign");
        authCabAndGo("goodhits/campaigns/id/" + 1076);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedCompliantIcon(), "FAIL - icon displayed campaign list 1");

        log.info("change type B to G");
        authCabAndGo("goodhits/ghits/?id=" + 1077);
        pagesInit.getCabTeasers().editLandingInline(false, "r");

        log.info("check visibility compliant icon teaser without landing B");
        authCabAndGo("goodhits/ghits/?id=" + 1077);
        log.info("check visibility compliant icon campaign - " + pagesInit.getCabTeasers().isDisplayedCompliantIcon());
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedCompliantIcon(), "FAIL - icon displayed teaser list 2");

        log.info("check visibility compliant icon campaign");
        authCabAndGo("goodhits/campaigns/id/" + 1076);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedCompliantIcon(), "FAIL - icon displayed campaign list 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check unblock teaser without compliant flag\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers compliant Italy with forbid category")
    public void checkCopyTeasersItalyIapCompliantForbidCategory() {
        log.info("Test is started");
        int campaignItalyDonor = 1058;
        int campaignItalyRecepWithIap = 1059;
        int teaserDonorWithoutIap = 1059;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyDonor, "Italy", "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyRecepWithIap, "Italy");

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignItalyRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(CliCommandsList.Cron.COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserDonorWithoutIap));

        log.info("Check recording presence");
        softAssert.assertFalse(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignItalyRecepWithIap, teaserDonorWithoutIap), "FAIL - present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers with type B to campaign with compliant options\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers with type B to campaign with compliant options")
    public void checkCopyWithTypeBWithoutCompliantToCompliantCampaign() {
        log.info("Test is started");
        int campaignDonor = 1077;
        int teaserDonor = 1079;
        int campaignRecepWithIap = 1078;
        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepWithIap, "Italy", "Romania", "Spain", "Czech Republic", "Poland");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy");

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(CliCommandsList.Cron.COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserDonor));

        softAssert.assertTrue(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignRecepWithIap, teaserDonor), "FAIL - record is present");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignRecepWithIap);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "FAIL - check amount of teaser");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers with compliant to campaign without compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers with compliant to campaign without compliant")
    public void checkCopyTeasersItalyIapCompliantToCampaignWithNotCompliantGeoFrance() {
        log.info("Test is started");
        int campaignItalyDonor = 1056;
        int campaignItalyRecepWithIap = 1060;
        int teaserDonor = 1058;
        int teaserRecipient;


        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyDonor, "Italy", "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyRecepWithIap, "France");

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignItalyRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(CliCommandsList.Cron.COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserDonor));

        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignItalyRecepWithIap, teaserDonor);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyRecepWithIap + "&id=" + teaserRecipient);

//        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser campaignItalyRecepWithIap , teaserDonorWithIap1");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedEnabledIapCompliant(), "check teaser campaignItalyRecepWithIap , teaserDonorWithIap2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers compliant to campaign with more wide compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers compliant to campaign with more wide compliant geo")
    public void checkCopyTeasersCompliantToCampaignWithMoreWideCompliantGeo() {
        log.info("Test is started");
        int campaignDonor = 1285;
        int campaignRecepWithIap = 1286;
        Integer teaserDonor = 1304;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy", "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepWithIap, "Italy", "Spain");
        operationMySql.getgPartners1().updateOptionCompliant(campaignDonor, IAP.getCompliantType());
        operationMySql.getgPartners1().updateOptionCompliant(campaignRecepWithIap, IAP.getCompliantType(), AUTOCONTROL.getCompliantType());

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor + "&id=" + teaserDonor);
        operationMySql.getMailPull().getCountLettersByBody(String.format(messageMissingAutocompliant, teaserDonor));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(CliCommandsList.Cron.COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertFalse(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignRecepWithIap, teaserDonor), "FAIL - record is present");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(messageMissingAutocompliant, teaserDonor)), "FAIL - message");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
