package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.cab.products.logic.CabTeasers.KycTypes.*;

public class CabTeasersKycTests extends TestBase {

    @Epic("KYC")
    @Feature("CAB filters. Additional filters for teasers with suspected KYC")
    @Story("Teaser list (cab)")
    @Description("Check filter teasers with suspected KYC" +
            "1) Filter by:" +
            "- Not Verified" +
            "- Verified" +
            "- Blocked" +
            "- Reject" +
            "2) Check icons for KYC types:" +
            "- Not Verified" +
            "- Verified")
    @Owner("AIA")
    @Test(description = "Check filter teasers with suspected KYC")
    public void checkTeasersWithKYC_Filter() {
        log.info("Test is started");
        int campaignId = 2052;
        int clientId = 2001;
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().filterTeasersByKyc(NOT_VERIFIED);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserPresent(clientId,2040),
                "FAIL -> KYC not verified filter");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), Integer.valueOf(1),
                "FAIL -> KYC not verified filter - teasers amount!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC not verified icon");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().filterTeasersByKyc(VERIFIED);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserPresent(clientId,2041),
                "FAIL -> KYC verified filter");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), Integer.valueOf(1),
                "FAIL -> KYC verified filter - teasers amount!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(VERIFIED),
                "FAIL -> KYC verified icon");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().filterTeasersByKyc(BLOCKED);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserPresent(clientId, 2042),
                "FAIL -> KYC blocked");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), Integer.valueOf(1),
                "FAIL -> KYC not verified filter - teasers amount!");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().filterTeasersByKyc(REJECT);
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserPresent(clientId,2043),
                "FAIL -> KYC reject");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), Integer.valueOf(1),
                "FAIL -> KYC not verified filter - teasers amount!");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC Not verified icon is displayed!");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkKycIconIsDisplayed(VERIFIED),
                "FAIL -> KYC Verified icon is displayed!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("KYC")
    @Feature("CAB mass action for teasers with suspected KYC")
    @Story("Teaser list (cab)")
    @Description("Check set teasers suspected KYC by mass action" +
            "- Not Verified" +
            "- Verified")
    @Owner("AIA")
    @Test(description = "Check set teasers suspected KYC by mass action")
    public void checkTeasersWithKYC_MassAction() {
        log.info("Test is started");
        String campaignId = "2053";
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().setTeasersKycMass("kycVerified");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(VERIFIED),
                "FAIL -> KYC verified icon is not displayed!");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC not verified icon is displayed!");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), pagesInit.getCabTeasers().getAmountOfKycVerifiedIcons(),
                "FAIL -> KYC verified icons vs teasers!");
        pagesInit.getCabTeasers().setTeasersKycMass("kycNotVerified");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC not verified icon is not displayed!");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkKycIconIsDisplayed(VERIFIED),
                "FAIL -> KYC verified icon is not displayed!");
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), pagesInit.getCabTeasers().getAmountOfKycNotVerifiedIcons(),
                "FAIL -> KYC not verified icons vs teasers!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("KYC")
    @Feature("CAB change state for teasers with suspected KYC")
    @Story("Teaser list (cab)")
    @Description("Check set teasers suspected KYC by icon" +
            "- Not Verified" +
            "- Verified")
    @Owner("AIA")
    @Test(description = "Check set teasers suspected KYC by icon")
    public void checkTeasersWithKYC_Icon() {
        log.info("Test is started");
        String teaserId = "2046";
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC not verified icon is not displayed!");
        pagesInit.getCabTeasers().clickKycNotVerifiedIcon();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Flag successfully changed!"),
                "FAIL -> KYC verified message is not displayed!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(VERIFIED),
                "FAIL -> KYC verified icon is not displayed!");
        pagesInit.getCabTeasers().clickKycVerifiedIcon();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Flag successfully changed!"),
                "FAIL -> KYC verified message is not displayed!");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkKycIconIsDisplayed(NOT_VERIFIED),
                "FAIL -> KYC not verified icon is not displayed!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
