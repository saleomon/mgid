package cab.products.teasers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

public class CabTeasersMaxCpcCreationTest extends TestBase {

    @BeforeMethod
    public void initializeVariables() {
        softAssert = new SoftAssert();
    }

    private int productCampaignId = 3000;
    private int contentCampaignId = 3001;
    private int searchFeedCampaignId = 3003;
    private int clientId = 3001;
    private String productTeaserId = "3000";
    private String contentTeaserId = "3001";

    @Feature("Максимизация CPC в системе")
    @Story("Max CPC. Cab")
    @Description("Валидация установки максимального CPC в Кабе.\n" +
            "     <ul>\n" +
            "      <li>Проверка валидации НЕ возможности создания тизера для РК типов `product, content, search_feed`</li>\n" +
            "      <li>Проверка валидации работает при установке единого СРС на все ГЕО. В данном случае выбрано ГЕО USA, Ukraine</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/BT-4065\">Ticket BT-4065</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51902\">Ticket TA-51902</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test (dataProvider = "campaignsMaxCPC", description = "Валидация установки CPC большего чем разрешено в рамках системи в кабе")
    public void checkValidationMaxCpcInTeaserCreationInterfaceForAllGeo(int campaignId, String categoryId, String cpc, boolean isEditCategory) {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(productCampaignId, "United States");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(contentCampaignId, "United States");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(searchFeedCampaignId, "United States", "Ukraine");
        authCabAndGo("goodhits/ghits-add/campaign_id/" + campaignId);
        log.info("set teaser fields and price of click more than acceptable");
        Assert.assertEquals(pagesInit.getCabTeasers()
                .setCategoryId(categoryId)
                .isNeedToEditUrl(true)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                        .setNeedToEditCategory(isEditCategory)
                .setDomain("test-t.com/testmaxcpc")
                .setPriceOfClick(cpc)
                .useCallToAction(false)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");
        log.info("Check error message max cpc validation");
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("The price for one or more regions is higher than acceptable"), "FAIL - max cpc is valid!!");
        log.info("Test is finished");
    }


    @DataProvider
    public Object[][] campaignsMaxCPC(){
        return new Object[][]{
                {productCampaignId, "132", "400", false},
                {contentCampaignId, "226", "400", true},
                {searchFeedCampaignId, "132", "100", false}
        };
    }

}
