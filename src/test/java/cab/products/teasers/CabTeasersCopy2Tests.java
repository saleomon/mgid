package cab.products.teasers;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.CliCommandsList.Cron.CLEAR_CREATIVE_ORDERS;
import static testData.project.CliCommandsList.Cron.COPY_TEASERS;

public class CabTeasersCopy2Tests extends TestBase {
    private final String successLetterWithoutSubject = "Teasers were copiedhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/id/";
    private final String forbiddenDueGifAtPushTeaser = "Animated images for push campaigns are not allowed (%s)";
    private final String forbiddenOffersBlackList = " It is forbidden to copy the teasers which offers are in the blacklist for geo-targeting of the recipient's campaign. (%s)";
    private final String letterWithWrongCategory = "Results of copying ads \n Copying error: wrong category. (%s)";
    private final String letterWithNotUkrainianTitleLanguagePart = "It is forbidden to copy teasers with a title/description language other than Ukrainian in campaigns that include targeting Ukraine. (%s)";
    private final String letterWithNotUkrainianTitleRequest = "Results of copying ads \n Hello! Autocopying ads for campaign %s from request %s had failed.It is forbidden to copy teasers with a title/description language other than Ukrainian in campaigns that include targeting Ukraine. (%s)";
    private final String successLetterLandingWarning = "Results of copying ads \n Landing page type of certain donor ads does not match the landing page type set for the campaign-recipient . Please adjust the landing page types accordinglyhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/";
    private final String successLetter = "Results of copying ads \n Teasers were copiedhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/id/";

    @Feature("Copy Teasers")
    @Story("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    @Description("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category\n" +
            "     <ul>\n" +
            "      <li>Category not subscriptions: All geo -> CIS</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51803\">Ticket TA-51803</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Prohibition of copy from WW to CIS  subscriptions category All geo -> CIS")
    public void checkCopyTeaserWithoutSubscribersCategoryAndAllGeoToCis() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1144;
        int campaignRecepient = 1145;
        String recipientGeo = "Republic of Moldova";

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, recipientGeo);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        log.info("getCountLettersByBody - " + operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient)));
        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient)).forEach((key, value) -> {
            System.out.println("el key  - " + key);
            System.out.println("el value- " + value);
        });
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1143");

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(successLetter, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    @Description("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category\n" +
            "     <ul>\n" +
            "      <li>Category subscriptions: All geo -> without CIS</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51803\">Ticket TA-51803</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Prohibition of copy from WW to CIS  subscriptions category All geo -> without CIS")
    public void checkCopyTeaserWithSubscribersCategoryAndAllGeoToWithoutCisCampaign() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1146;
        int campaignRecepient = 1147;
        String [] recipientGeo = new String[] {"Ukraine", "Azerbaijan", "Armenia", "Belarus", "Georgia", "Kazakhstan", "Kyrgyzstan", "Latvia", "Lithuania", "Republic of Moldova",
                "Russian Federation", "Tajikistan", "Turkmenistan", "Uzbekistan", "Estonia"};
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(campaignRecepient, recipientGeo);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1144");

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(successLetter, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Copying in RK types. Categories validation.")
    @Description("Copying teasers | Copying in RK types. Categories validation.\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51820\">Ticket TA-51820</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Copying teasers | Copying in RK types. Categories validation")
    public void checkCopyTeasersWithWrongCategory() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;
        int campaignDonor = 1163;
        int campaignRecepient1 = 1164;
        Integer teaserDonor = 1147;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Canada");


        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(letterWithWrongCategory, teaserDonor));
            pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(letterWithWrongCategory, teaserDonor)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine")
    @Description("Copying teasers | Copying in RK types. Categories validation.\n" +
            "     <ul>\n" +
            "      <li>Проверка копирования с отсутствующим языком тайтла</li>\n" +
            "      <li>Проверка копирования с отсутствующим языком дескрипшна</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51804\">Ticket TA-51804</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (dataProvider = "copyTeasersToCampaignGeoUkraine", description = "Copying teasers | Copying in RK types. Categories validation")
    public void checkCopyTeasersWithoutTitleLanguageToCampaignGeoUkraine(int campaignDonor, int campaignRecepient1, Integer teaserDonor) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Ukraine");


        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(letterWithNotUkrainianTitleLanguagePart, teaserDonor));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(letterWithNotUkrainianTitleLanguagePart, teaserDonor)).contains(String.format(letterWithNotUkrainianTitleLanguagePart, teaserDonor)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersToCampaignGeoUkraine(){
        return new Object[][]{
                {1167, 1168, 1149},
                {1170, 1171, 1150},
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine")
    @Description("Copying teasers | Copying in RK types. Categories validation.\n" +
            "     <ul>\n" +
            "      <li>Проверка что тизер копируется с языком украинским в кампанию з гео и языком Украина</li>\n" +
            "      <li>Проверка что тизер копируется с языком украинским в кампанию з гео Латвия</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51804\">Ticket TA-51804</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (dataProvider = "copyTeasersWithTitleLanguageToCampaignGeoUkraine", description = "Forbid copy by language Ukraine for targeting including Ukraine")
    public void checkCopyTeasersWithTitleLanguageToCampaignGeoUkraine(int campaignDonor, int campaignRecepient1, Integer teaserId, String geo) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, geo);


        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient1));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(successLetter, campaignRecepient1)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersWithTitleLanguageToCampaignGeoUkraine(){
        return new Object[][]{
                {1172, 1173, 1151, "Ukraine"},
                {1174, 1175, 1152, "Latvia"},
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine")
    @Description("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine\n" +
            "     <ul>\n" +
            "      <li>Проверка что тизер копируется с языком украинским в кампанию з гео и языком Украина</li>\n" +
            "      <li>Проверка что тизер копируется с языком украинским в кампанию з гео Латвия</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51804\">Ticket TA-51804</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (dataProvider = "copyTeasersWithTitleLanguageToCampaignAdskeeperGeoUkraine", description = "Forbid copy by language Ukraine for targeting including Ukraine, Adskeeper")
    public void checkCopyTeasersWithTitleLanguageToCampaignAdskeeperGeoUkraine(int campaignDonor, int campaignRecepient1, Integer teaserId, String ad, String landing, String message, String...geo) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, geo[0]);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, geo[1]);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(message, campaignRecepient1));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(message, campaignRecepient1)).contains(String.format(message, campaignRecepient1)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAdTypeInListInterface(ad), "FAIL - teaser ad type");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkLandingTypeInListInterface(landing), "FAIL - teaser landing type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersWithTitleLanguageToCampaignAdskeeperGeoUkraine(){
        return new Object[][]{
                {1176, 1177, 1153, "pg", "g", successLetter, new String[] {"Ukraine", "Ukraine"}},
                {1178, 1179, 1154, "b", "b", successLetterLandingWarning, new String[] {"Russian Federation", "Ukraine"}},
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine")
    @Description("Copying teasers | Forbid copy by language Ukraine for targeting including Ukraine\n" +
            "     <ul>\n" +
            "      <li>Проверка работы копирки через заявку</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51804\">Ticket TA-51804</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Forbid copy by language Ukraine for targeting including Ukraine request")
    public void checkCopyTeasersFromRequest() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;
        int campaignDonor = 1180;
        int campaignRecepient1 = 1181;
        int orderId = 1009;
        Integer teaserDonor = 1155;
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Ukraine");
        serviceInit.getDockerCli().runAndStopCron(CLEAR_CREATIVE_ORDERS, "-vvv");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(letterWithNotUkrainianTitleRequest, campaignRecepient1, orderId, teaserDonor));

        log.info("Check cab message");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

//        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(letterWithNotUkrainianTitleRequest, campaignRecepient1, orderId, teaserDonor)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Сopying teasers | MGID -> Adskeeper | offer all geo
     * <ul>
     *     <li>mgid - adskeeper, Campaign donor "United Kingdom", offer "United Kingdom": mgid black, adskeeper white, campaign recipient "United Kingdom"</li>
     *     <li>ideal - adskeeper, Campaign donor "Russian", offer "Ukraine": mgid black, adskeeper white, campaign recipient "Ukraine"</li>
     *     <li>mgid - adskeeper, Campaign donor "Ukraine", offer "Russian": mgid black, adskeeper black, campaign recipient "Russian"</li>
     *     <li>mgid - mgid, Campaign donor "United Kingdom", offer "United Kingdom": mgid black, adskeeper white, campaign recipient "United Kingdom"</li>
     *     <li>mgid - adskeeper, Campaign donor "United Kingdom", offer "United Kingdom": mgid black, adskeeper white, campaign recipient "Canada"</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51801">TA-51801</a>
     */

    @Feature("Copy Teasers")
    @Story("Сopying teasers | MGID -> Adskeeper | offer all geo")
    @Description("Сopying teasers | MGID -> Adskeeper | offer all geo\n" +
            "     <ul>\n" +
            "      <li>mgid - adskeeper, Campaign donor 'United Kingdom', offer 'United Kingdom': mgid black, adskeeper white, campaign recipient 'United Kingdom'</li>\n" +
            "      <li>ideal - adskeeper, Campaign donor 'Russian', offer 'Ukraine': mgid black, adskeeper white, campaign recipient 'Ukraine'</li>\n" +
            "      <li>mgid - adskeeper, Campaign donor 'Ukraine', offer 'Russian': mgid black, adskeeper black, campaign recipient 'Russian'</li>\n" +
            "      <li>mgid - mgid, Campaign donor 'United Kingdom', offer 'United Kingdom': mgid black, adskeeper white, campaign recipient 'United Kingdom'</li>\n" +
            "      <li>mgid - adskeeper, Campaign donor 'United Kingdom', offer 'United Kingdom': mgid black, adskeeper white, campaign recipient 'Canada'</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51801\">Ticket TA-51801</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Copy with black listed offer", dataProvider = "copyTeasersWithOfferBlackListProduct")
    public void checkCopyTeasersWithOfferBlackListProduct(int campaignDonor, int campaignRecepient, Integer teaserId, String landing, String geoDonor, String geoRecipient) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, geoDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, geoRecipient);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        log.info("getCountLettersByBody - " + operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient)));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId.toString());

        var temp = operationMySql.getMailPull().getMailByBody(String.format(successLetter, campaignRecepient));
        softAssert.assertTrue(temp.contains(String.format(successLetter, campaignRecepient)),temp + "__" + String.format(successLetter, campaignRecepient));

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should be present");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkLandingTypeInListInterface(landing), "FAIL - teaser landing type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersWithOfferBlackListProduct(){
        return new Object[][]{
                {1182, 1183, 1156, "b", "Canada", "Canada"},
                {1186, 1187, 1158, "g", "Russian Federation",    "Ukraine"},
                {1188, 1189, 1159, "g", "Ukraine",    "Russian Federation"},
                {1190, 1191, 1160, "g", "United Kingdom", "United Kingdom"},
                {1192, 1193, 1161, "g", "United Kingdom", "Canada"},
        };
    }

    @Feature("Copy Teasers")
    @Story("Сopying teasers | MGID -> Adskeeper | offer all geo")
    @Description("Сopying teasers | MGID -> Adskeeper | offer all geo\n" +
            "     <ul>\n" +
            "      <li>mgid - adskeeper, Campaign donor \"Russian Federation\", offer \"Ukraine\": mgid black, adskeeper white, campaign recipient \"Krym\"</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51801\">Ticket TA-51801</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Copy with black listed offer Krym")
    public void checkCopyTeasersWithOfferBlackListPush() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1184;
        int campaignRecepient = 1185;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Russian Federation");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingIncludeRegions(campaignRecepient, "Krym");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetterWithoutSubject, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1157");

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetterWithoutSubject, campaignRecepient)).contains(String.format(successLetterWithoutSubject, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkLandingTypeInListInterface("b"), "FAIL - teaser landing type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Restrictions offers | all geo | MGID/ Lentainform")
    @Description("Копіювання тизерів\n" +
            "<ul>\n" +
            "   <li>Компанія донор mgid, product, geo - Italy</li>\n" +
            "   <li>Тизер донор approved</li>\n" +
            "   <li>Оффер: blacklist - Canada</li>\n" +
            "   <li>Кампанія реципієнт mgid, product, geo - Canada, United Kingdom</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51802\">Ticket TA-51802</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру MGID, offer:blacklist - canada -> MGID, geo - canada, united kingdom")
    public void copyTeasersOfferBlackListToCampaignWithBlacklistedGeo() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;
        int campaignDonor = 1194;
        int campaignRecepient = 1195;
        Integer teaserDonor = 1162;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Canada");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(forbiddenOffersBlackList, teaserDonor));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(forbiddenOffersBlackList, teaserDonor)).contains(String.format(forbiddenOffersBlackList, teaserDonor)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Restrictions offers | all geo | MGID/ Lentainform")
    @Description("Копіювання тизерів\n" +
            "<ul>\n" +
            "   <li>Компанія донор mgid, product, geo - Italy</li>\n" +
            "   <li>Тизер донор approved</li>\n" +
            "   <li>Оффер: mgid blacklist - Canada</li>\n" +
            "   <li>Кампанія реципієнт adskeeper, product, geo - Canada, United Kingdom</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51802\">Ticket TA-51802</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру MGID, offer:mgid blacklist - canada -> Adskeeper, geo - canada, united kingdom")
    public void copyTeasersOfferBlackListToCampaignWithBlacklistedGeoAnotherSubnet() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1196;
        int campaignRecepient = 1197;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Canada");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetterWithoutSubject, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1163");

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetterWithoutSubject, campaignRecepient)).contains(String.format(successLetterWithoutSubject, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Restrictions offers | all geo | MGID/ Lentainform")
    @Description("Копіювання тизерів\n" +
            "<ul>\n" +
            "   <li>Компанія донор mgid, product, geo - Italy</li>\n" +
            "   <li>Тизер донор approved</li>\n" +
            "   <li>Оффер: not defined - Canada</li>\n" +
            "   <li>Кампанія реципієнт mgid, product, geo - Canada, United Kingdom</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51802\">Ticket TA-51802</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру MGID, offer:not defined - canada -> MGID, geo - canada, germany, united kingdom")
    public void copyTeasersOfferNotDefinedToCampaignWithNotDefinedGeo() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1198;
        int campaignRecepient = 1199;
        int teaserDonor = 1164;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Canada", "Germany");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetterWithoutSubject, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserDonor));

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetterWithoutSubject, campaignRecepient)).contains(String.format(successLetterWithoutSubject, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Icon is not displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Restrictions offers | all geo | MGID/ Lentainform")
    @Description("Копіювання тизерів\n" +
            "<ul>\n" +
            "   <li>Компанія донор mgid, product, geo - Italy</li>\n" +
            "   <li>Тизер донор approved</li>\n" +
            "   <li>Оффер: not defined - Canada</li>\n" +
            "   <li>Кампанія реципієнт Adskeeper, product, geo - Canada, United Kingdom</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51802\">Ticket TA-51802</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру MGID, offer:not defined - canada -> Adskeeper, geo - canada")
    public void copyTeasersOfferNotDefinedToCampaignWithNotDefinedGeoAnotherSubnet() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1200;
        int campaignRecepient = 1201;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Italy");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Canada");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetterWithoutSubject, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1165");

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetterWithoutSubject, campaignRecepient)).contains(String.format(successLetterWithoutSubject, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Icon is not displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Restrictions offers | all geo | MGID/ Lentainform")
    @Description("Копіювання тизерів\n" +
            "<ul>\n" +
            "   <li>Компанія донор mgid, product, geo - Russia</li>\n" +
            "   <li>Тизер донор approved</li>\n" +
            "   <li>Оффер: blacklist - Russia</li>\n" +
            "   <li>Кампанія реципієнт mgid, product, geo - Crimea</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51802\">Ticket TA-51802</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру MGID, offer:blacklist - Russia -> mgid, geo - crimea")
    public void copyTeasersOfferBlacklistRussiaToCampaignWithCrimeaGeo() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1202;
        int campaignRecepient = 1203;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingIncludeRegions(campaignRecepient, "Krym");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetterWithoutSubject, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1166");

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetterWithoutSubject, campaignRecepient)).contains(String.format(successLetterWithoutSubject, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Icon is not displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Cloudinary GIF - MVP")
    @Story("Копирование анимированных тизеров")
    @Description("Копирование анимированных тизеров\n" +
            "<ul>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Копіювання тизеру з анімацією")
    public void copyTeasersWithAnimation() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1220;
        int teaserDonor = 1180;
        int campaignRecepient = 1221;

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserDonor));

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetter, campaignRecepient)).contains(String.format(successLetter, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Check copy teaser without 'Advertiser name' to one campaign</>\n" +
            "   <li>Check error message for blocking teaser without advertiser name</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check copy teaser without 'Advertiser name' to one campaign")
    public void checkCopyTeaserWithoutAdvertiserNameToOneCampaign() {
        log.info("Test is started");
        String campaignId = "2021";
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().copyTeaserByMassActionsToOneCampaign(2020);
        Assert.assertEquals(helpersInit.getMessageHelper().getCustomMessageValueCab("The source and recipient campaigns do not have advertiser name set. Сopying is not possible"),
                "The source and recipient campaigns do not have advertiser name set. Сopying is not possible",
                "FAIL -> Error message for copy teaser between campaigns without 'Advertiser name'!");
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Check copy teaser without 'Advertiser name' to many campaigns</>\n" +
            "   <li>Check error message for blocking teaser without advertiser name</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check copy teaser without 'Advertiser name' to many campaigns")
    public void checkCopyTeaserWithoutAdvertiserNameToManyCampaigns() {
        log.info("Test is started");
        String campaignId = "2021";
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignId);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(2019, 2020);
        Assert.assertEquals(helpersInit.getMessageHelper().getCustomMessageValueCab("The source and recipient campaigns do not have advertiser name set. Сopying is not possible"),
                "The source and recipient campaigns do not have advertiser name set. Сopying is not possible",
                "FAIL -> Error message for copy teaser between campaigns without 'Advertiser name'!");
        log.info("Test is finished");
    }


    @Feature("Cloudinary GIF - MVP")
    @Story("Закрити можливість додавання gif у Push тизерах")
    @Description("Check possibility to copy push teaser with gif\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52114\">Ticket TA-52114</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check possibility to copy push teaser with gif")
    public void checkCopyPushTeaserWithGif() {
        log.info("Test is started");

        int campaignIdDonor = 1266;
        int campaignIdRecipient = 1267;
        String teaserId = "1268";

        authCabAndGo("goodhits/ghits?id=" + teaserId + "&campaign_id=" + campaignIdDonor);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignIdRecipient);

        operationMySql.getMailPull().getCountLettersByBody(String.format(forbiddenDueGifAtPushTeaser, teaserId));
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId);

        Assert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(forbiddenDueGifAtPushTeaser, teaserId)).contains(String.format(forbiddenDueGifAtPushTeaser, teaserId)), "FAIL - Check message of copy result");

        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 | Ad's Copying")
    @Description("Check possibility to copy teaser with video\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52143\">Ticket TA-52143</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check possibility to copy teaser with video")
    public void checkCopyTeaserWithVideo() {
        log.info("Test is started");

        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignIdDonor = 1279;
        int campaignIdRecipient = 1309;
        String teaserId = "1295";

        authCabAndGo("goodhits/ghits?id=" + teaserId + "&campaign_id=" + campaignIdDonor);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignIdRecipient);

        operationMySql.getMailPull().getCountLettersByBody(String.format(successLetter, campaignIdRecipient));
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId);

        softAssert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(successLetter, campaignIdRecipient)).contains(String.format(successLetter, campaignIdRecipient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignIdRecipient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 | Ad's Copying")
    @Description("Check possibility to copy push teaser with video\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52143\">Ticket TA-52143</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check possibility to copy push teaser with video")
    public void checkCopyPushTeaserWithVideo() {
        log.info("Test is started");

        int campaignIdDonor = 1310;
        int campaignIdRecipient = 1311;
        String teaserId = "1296";

        authCabAndGo("goodhits/ghits?id=" + teaserId + "&campaign_id=" + campaignIdDonor);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignIdRecipient);

        operationMySql.getMailPull().getCountLettersByBody(String.format(forbiddenDueGifAtPushTeaser, teaserId));
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserId);

        Assert.assertTrue(operationMySql.getMailPull().getMailByBody(String.format(forbiddenDueGifAtPushTeaser, teaserId)).contains(String.format(forbiddenDueGifAtPushTeaser, teaserId)), "FAIL - Check message of copy result");

        log.info("Test is finished");
    }

}
