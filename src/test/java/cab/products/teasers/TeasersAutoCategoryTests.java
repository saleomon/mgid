package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class TeasersAutoCategoryTests extends TestBase {

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check reject verification of category\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check reject verification of category")
    public void checkRejectVerificationCategory() {
        log.info("Test is started");
        int teaserId = 1405;

        log.info("Reject category");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().rejectAutoCategory();

        String option = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("Current options - " + option);

        softAssert.assertTrue(option.contains("\"autoCategoryVerified\":false"), "FAIL - checkDisplayedCategory");

        Assert.assertTrue(pagesInit.getCabTeasers().checkDisplayedCategory("Events"), "FAIL - checkDisplayedCategory");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check privilege auto category\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name = "goodhits/auto-category-set-verification-status")
    @Test(description = "Check privilege auto category")
    public void checkPrivilegeAutoCategory() {
        log.info("Test is started");
        int teaserId = 1404;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/auto-category-set-verification-status");

        log.info("Check displaying auto category");
        authCabForCheckPrivileges("goodhits/ghits?id=" + teaserId);

        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedAutoCategory(), "FAIL - isDisplayedAutoCategory");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check approve teaser with not verifying category thumb\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check approve teaser with not verifying category thumb")
    public void checkApproveTeaserWithNotVerifyingCategoryThumb() {
        log.info("Test is started");
        int teaserId = 1411;

        log.info("check displayed message");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("You have to verify the auto category before applying this action"), "FAIL - checkMessagesCab");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check approve teaser with  verifying category thumb\n" +
            "     <ul>\n" +
            "      <li>Check approving with verified category </li>\n" +
            "      <li>Check approving without category </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check approve teaser with or without verifying category thumb", dataProvider = "approveTeaserWithOrWithoutCategoryThumb")
    public void checkApproveTeaserWithOrWithoutCategoryThumb(int teaserId) {
        log.info("Test is started");

        log.info("Check approving teaser with verified category");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();

        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - isDisplayedApproveIcon");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] approveTeaserWithOrWithoutCategoryThumb() {
        return new Object[][]{
                {1413},
                {1415},
        };
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check approve teaser with not verifying category mass\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check approve teaser with not verifying category mass")
    public void checkApproveTeaserWithNotVerifyingCategoryMass() {
        log.info("Test is started");
        int teaserId = 1412;

        log.info("Check displayed message");
        authCabAndGo("goodhits/ghits?campaign_id=1399&id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserMassAction();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("You have to verify the auto category before applying this action"), "FAIL - checkMessagesCab");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check approve teaser with verifying category mass\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check approve teaser with verifying category mass")
    public void checkApproveTeaserWithVerifyingCategoryMass() {
        log.info("Test is started");
        int teaserId = 1414;

        log.info("Check displayed approve icon");
        authCabAndGo("goodhits/ghits?campaign_id=1399&id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserMassAction();

        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - isDisplayedApproveIcon");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check approve teaser without privileges but with auto category\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name="goodhits/auto-category-set-verification-status")
    @Test(description = "Check approve teaser without privileges but with auto category")
    public void checkApproveTeaserWithoutPrivilegesButWithAutoCategory() {
        log.info("Test is started");
        int teaserId = 1416;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/auto-category-set-verification-status");

        log.info("Check displayed approve icon");
        authCabForCheckPrivileges("goodhits/ghits?campaign_id=1399&id=" + teaserId);
        pagesInit.getCabTeasers().approveTeaserMassAction();

        authCabForCheckPrivileges("goodhits/ghits?campaign_id=1399&id=" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - isDisplayedApproveIcon");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check auto category by send to moderation action\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check auto category by send to moderation action")
    public void checkClearAutoCategoryByReModeration() {
        log.info("Test is started");
        int teaserId = 1417;

        log.info("Check displayed approve icon");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().sendToReModeration();

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedAutoCategory(), "FAIL - isDisplayedApproveIcon");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check clear auto category by block client\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    //toDo NIO cron@Test(description = "Check clear auto category by block client")
    public void checkClearAutoCategoryByBlockClient() {
        log.info("Test is started");
        int teaserId = 1418;

        log.info("Check displayed approve icon");
        authCabAndGo("crm/edit-client/id/1132");
        pagesInit.getCrmClients().setBlockedClient(true).saveSettings();

        String option = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("Current options - " + option);
        softAssert.assertFalse(option.contains("autoCategoryVerified") , "FAIL - option");

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedAutoCategory(), "FAIL - isDisplayedAutoCategory");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "FAIL - isDisplayedApproveIcon");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(AUTO_CATEGORY)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check clear auto category by Spain geo\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52213\">Ticket TA-52213</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check clear auto category by Spain geo")
    public void checkAcceptVerificationCategorySpainGeo() {
        log.info("Test is started");

        int campaignId = 1401;
        int teaserId =1419;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Spain");
        log.info("Accept category");
        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().acceptAutoCategory();

        String option = operationMySql.getgHits1().getOptionsValue(teaserId);
        log.info("Current options - " + option);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        softAssert.assertTrue(option.contains("autoCategoryVerified") , "FAIL - option");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayedCategory("Options"), "FAIL - checkDisplayedCategory");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(),  "FAIL - isDisplayedApproveIcon");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
