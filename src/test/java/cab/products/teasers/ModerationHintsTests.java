package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.BULK_ACTION_BLOCK_BATCHES;
import static testData.project.CliCommandsList.Consumer.BULK_ACTION_BLOCK;
import static testData.project.CliCommandsList.Consumer.BULK_ACTION_MAIN;
import static testData.project.CliCommandsList.Cron.MODERATOR_HINTS;
import static testData.project.EndPoints.cabLink;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.Subnets.SUBNET_ADSKEEPER_NAME;
import static testData.project.Subnets.SUBNET_MGID_NAME;

public class ModerationHintsTests extends TestBase {
    private final String textForAdTypeMgid = "For the teaser 1383, found duplicate by titles and image, in which the ad type is different from the one that the moderator changed R in campaigns\n1373";
    private final String textForAdTypeAdskeeper = "For the teaser 1385, found duplicate by titles and image, in which the ad type is different from the one that the moderator changed R in campaigns\n1375";
    private final String textForLpTypeMgid = "For the teaser 1381, found duplicate LP & type is higher than the one has been changed R in campaigns\n1326\n, \n1379";
    private final String textForLpTypeAdskeeper = "For the teaser 1387, found duplicate LP & type is higher than the one has been changed R in campaigns\n1377";
    private final String textForRejectWithTitle = "For teaser 1392 that had been rejected for Language mismatch found similar in campaigns\n1383\n, \n1384";
    private final String textForRejectWithImageLink = "For teaser 1394 that had been rejected for Inappropriate motion effects found similar in campaigns\n1384";
    private final String textForRejectWithLandingType = "For teaser 1396 that had been rejected for Spoilers found similar in campaigns\n1387";
//    private final String textForRejectWithTitle = "For teaser 1392 that had been rejected for Inappropriate wording found similar in campaigns\n1383\n, \n1384";
//    private final String textForRejectWithImageLink = "For teaser 1394 that had been rejected for Bad cropping found similar in campaigns\n1384";
//    private final String textForRejectWithLandingType = "For teaser 1396 that had been rejected for Violence found similar in campaigns\n1387";
    private final String textForBlockWithCloaking = "Teasers with a cloaking LP found in campaigns\n1381\n, \n1382\n, \n1385";
    private final String textForBlockWithCloakingSchedule = "Teasers with a cloaking LP found in campaigns\n1390\n, \n1392";

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check close message notification\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check close message notification")
    public void checkCloseModeratorHint() {
        log.info("Test is started");
        int teaserId1 = 1000;

        operationMySql.getModeratorsHints().truncateTable().pushDumpToTable();

        authCabAndGo("");
        int amountOfHints = componentsInit.getModerationHints().getAmountOfHints();
        componentsInit.getModerationHints().openHintsBox();
        componentsInit.getModerationHints().closeTeaserHintWithId(teaserId1);

        authCabAndGo("");
        softAssert.assertEquals(componentsInit.getModerationHints().getAmountOfHints(), amountOfHints - 1, "Fail - amount");
        componentsInit.getModerationHints().openHintsBox();
        softAssert.assertFalse(componentsInit.getModerationHints().checkVisibilityHintWithTeaser(teaserId1), "Fail - amount");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check clear all messages notification\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check clear all messages notification")
    public void checkClearAllModeratorHints() {
        log.info("Test is started");

        operationMySql.getModeratorsHints().truncateTable().pushDumpToTable();

        authCabAndGo("");
        componentsInit.getModerationHints().openHintsBox();
        componentsInit.getModerationHints().clearAllNotifications();

        authCabAndGo("");
        Assert.assertFalse(componentsInit.getModerationHints().isDisplayHintsCounter(), "Fail - isDisplayHintsCounter");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check message for precise action\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check message for precise action", dataProvider = "messageForPreciseAction")
    public void checkMessageForPreciseAction(int teaserId, String message) {
        log.info("Test is started");

        operationMySql.getModeratorsHints().truncateTable().pushDumpToTable();
        log.info("Test is started");

        authCabAndGo("");
        componentsInit.getModerationHints().openHintsBox();

        Assert.assertTrue( componentsInit.getModerationHints().checkNotificationMessage(teaserId, message), "Fail - isDisplayHintsCounter");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] messageForPreciseAction() {
        return new Object[][]{
                {1003, "For the teaser 1003, found duplicate by titles and image, in which the category is different from the one that the moderator changed Amazing in campaigns\n3"},
                {1004, "Teasers with a cloaking LP found in campaigns\n3"},
                {1005, "For teaser 1005 that had been rejected for Landing page issue found similar in campaigns\n3"}
        };
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check privilege for notification\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name="moderation-hint/index")
    @Test(description = "Check privilege for notification")
    public void checkPrivilegeForNotification() {
        log.info("Test is started");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "moderation-hint/index");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "moderation-hint/hints");

        authCabForCheckPrivileges("");

        Assert.assertFalse( componentsInit.getModerationHints().isDisplayedNotificationIcon(), "Fail - isDisplayHintsCounter");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check LP change flow\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Flaky
    @Test(description = "Check LP change flow", dataProvider = "dataForLpTypeFlow")
    public void checkLpTypeChangesFlow(int teaserId, String message, String subnet) {
        log.info("Test is started " + subnet);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().editLandingInline(false, "r");
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        sleep(1000);
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);
        log.info("Hint id --- "  + hintId);

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");
        sleep(1500);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        Assert.assertTrue(componentsInit.getModerationHints().checkNotificationMessage(hintId, message), "Fail - checkNotificationMessage");

        log.info("Test is finished "  + subnet);
    }

    @DataProvider
    public Object[][] dataForLpTypeFlow() {
        return new Object[][]{
                {1381, textForLpTypeMgid, SUBNET_MGID_NAME},
                {1387, textForLpTypeAdskeeper, SUBNET_ADSKEEPER_NAME}
        };
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check Ad change flow\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check Ad change flow", dataProvider = "dataForAdTypeFlow")
    public void checkAdTypeChangesFlow(int teaserId, String message, String subnet) {
        log.info("Test is started for " + subnet);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().editAdInline("r");
        sleep(500);
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);
        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        Assert.assertTrue( componentsInit.getModerationHints().checkNotificationMessage(hintId, message), "Fail - checkNotificationMessage");

        log.info("Test is finished for "  + subnet);
    }

    @DataProvider
    public Object[][] dataForAdTypeFlow() {
        return new Object[][]{
                {1383, textForAdTypeMgid,  SUBNET_MGID_NAME},
                {1385, textForAdTypeAdskeeper, SUBNET_ADSKEEPER_NAME}
        };
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check displaying hint with status 5\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check displaying hint with status 5")
    public void checkDisplayingHintWithNoDuplicatedEntity() {
        log.info("Test is started");
        int teaserId = 1391;

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().editLandingInline(false, "r");
        sleep(700);
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkDisplayingHintWithNoDuplicatedEntity");
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);
        log.info("Hint id - " + hintId);

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        softAssert.assertFalse(componentsInit.getModerationHints().checkVisibilityHintWithTeaser(hintId), "Fail - checkNotificationMessage");
        softAssert.assertEquals(operationMySql.getModeratorsHints().getHintsStatus(teaserId), 5,  "Fail - getHintsStatus");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check deleting hint with status 4\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52219\">Ticket TA-52219</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check deleting hint with status 4")
    public void checkDeletingHintWithStatus4() {
        log.info("Test is started");
        int teaserId1 = 1012;

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");
        sleep(1000);
        Assert.assertFalse(operationMySql.getModeratorsHints().isPresentHintForTeaser(teaserId1),  "Fail - getHintsStatus");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check rejection with 'title|image|lp' type flow\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52220\">Ticket TA-52220</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check rejection with 'title|image|lp' type flow", dataProvider = "rejectFlowWithTitleReason")
    public void checkRejectFlowWithTypeOfReason(String type, int teaserId, String message, String reason) {
        log.info("Test is started " + type);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().rejectTeaser(reason);
        sleep(500);
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        Assert.assertTrue( componentsInit.getModerationHints().checkNotificationMessage(hintId, message), "Fail - checkNotificationMessage");

        log.info("Test is finished "  + type);
    }

    @DataProvider
    public Object[][] rejectFlowWithTitleReason() {
        return new Object[][]{
                {"title", 1392, textForRejectWithTitle,       "32"},
                {"image", 1394, textForRejectWithImageLink,   "11"},
                {"lp",    1396, textForRejectWithLandingType, "57"}
        };
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check block with cloaking flow by icon\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52220\">Ticket TA-52220</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check block with cloaking flow by icon")
    public void checkBlockFlowWithCloaking() {
        log.info("Test is started");
        int teaserId = 1398;

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().blockTeaserByIcon(true, "Important reason");
        sleep(500);
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        Assert.assertTrue( componentsInit.getModerationHints().checkNotificationMessage(hintId, textForBlockWithCloaking), "Fail - checkNotificationMessage");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check block with cloaking flow by mass\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52220\">Ticket TA-52220</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check block with cloaking flow by mass", priority = 1)
    public void checkBlockFlowWithCloakingByMassAction() {
        try {
            serviceInit.getDockerCli().runConsumer(BULK_ACTION_MAIN, "-vvv");
            log.info("Test is started");
            int teaserId = 1400;

            authCabAndGo("goodhits/ghits?campaign_id=1389&id=" + teaserId);
            pagesInit.getCabTeasers().blockTeaserByMassAction(true, "Very important reason");

            serviceInit.getDockerCli().runConsumer(BULK_ACTION_BLOCK, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(BULK_ACTION_BLOCK_BATCHES);

            Assert.assertFalse(operationMySql.getModeratorsHints().isPresentHintForTeaser(teaserId), "Fail - isPresentHintForTeaser");

            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("Check block with cloaking flow by schedule\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52220\">Ticket TA-52220</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check block with cloaking flow by schedule", priority = 2)
    public void checkBlockFlowWithCloakingBySchedule() {
        log.info("Test is started");
        int teaserId = 1402;

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        pagesInit.getCabTeasers().blockBySchedule(true, "Very important reason");
        sleep(1000);
        int hintId = operationMySql.getModeratorsHints().getHintEntityId(teaserId);

        serviceInit.getDockerCli().runAndStopCron(MODERATOR_HINTS, "-vvv");
        sleep(500);

        authCabAndGo("goodhits/ghits?id=" + teaserId);
        componentsInit.getModerationHints().openHintsBox();
        Assert.assertTrue( componentsInit.getModerationHints().checkNotificationMessage(hintId, textForBlockWithCloakingSchedule), "Fail - checkNotificationMessage");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MODERATORS_HINTS)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52216\">Ticket TA-52216</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check link for teaser for redirection")
    public void checkLinkFromHintForRedirection() {
        log.info("Test is started");
        int hintElement = 1000;
        int teaserId = 1010;

        operationMySql.getModeratorsHints().truncateTable().pushDumpToTable();

        authCabAndGo("");
        componentsInit.getModerationHints().openHintsBox();

        Assert.assertEquals(componentsInit.getModerationHints().getLinkFromHint(hintElement), cabLink + "goodhits/ghits/?id=" + teaserId);
        log.info("Test is finished");
    }
}
