package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.CliCommandsList.Cron.COPY_TEASERS;
import static testData.project.CliCommandsList.Cron.COUNT_TEASERS_FOR_RK;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabTeasersCopy1Tests extends TestBase {

    //test data for mail_pull
    private final String subjectLetter = "Results of copying ads";
    private final String patternForMailCountingByBodyTextSuccess = "Results of copying ads \n Teasers were copiedhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/id/";
    private final String successLetterWithBlockerTeasser = "Results of copying ads \n Copied teasers will be blocked because the donor or recipient campaign has the country Spain and contains categories with which the teaser cannot be launched automaticallyhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/id/%sTeasers were copiedhttp://admin.mgid.com/cab/goodhits/ghits/campaign_id/%s/id/";
    private final String letterWithDifferentCampaignsType = "Results of copying ads \n Source and target campaigns types does not match (%s)";
    private final String letterWithSubscriptionCategory = "Results of copying ads \n It is forbidden to copy teasers category “Subscriptions” to campaign with CIS targeting (%s)";

    @BeforeMethod
    public void initializeVariables() {
        softAssert = new SoftAssert();
    }

    @Feature("Copy Teasers")
    @Story("Ограничение копирования тизеров стран APAC")
    @Description("Ограничение копирования тизеров стран APAC\n" +
            "     <ul>\n" +
            "      <li> - копирование тизеров APAC - APAC, withour APAC - в несколько кампаний</li>\n" +
            "      <li> - копирование тизеров APAC - APAC+WW - в одну кампанию</li>\n" +
            "      <li> - копирование тизеров c таргетингом без APAC - APAC - в одн кампанию</li>\n" +
            "      <li> - копирование тизеров c таргетингом без APAC - APAC, withour APAC - в несколько кампаний</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-23411\">Ticket TA-23411</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Ограничение копирования тизеров стран APAC")
    public void checkCopyTeasersWithApacGeoProductCampaign() {
        log.info("Test is started");
        int apacCampaignId = 9;
        int mixedCampaignId = 10;
        int cisCampaignId = 11;

        String failRegionMatchingLetterSingle = "Results of copying ads \n" +
                " Ad's that do not include APAC, MENA, GCC countries are not allowed to be copied to APAC, MENA, GCC geo-targeted campaigns (%s)Ad's with geo targeting on CIS regional group can only be copied to campaigns with the same regional group as in the donor campaign. (%s)";
        String formatedMessage = String.format(failRegionMatchingLetterSingle, "1003", "1003");

//        operationMySql.getgHits1().deleteTeasersByCampaignsButOne(19, apacCampaignId, mixedCampaignId);
//        operationMySql.getgHits1().deleteTeasersByCampaignsButOne(1003, cisCampaignId);

        log.info("set GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(apacCampaignId, "India", "Malaysia");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(mixedCampaignId, "Thailand", "Egypt");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(cisCampaignId, "Russian Federation");

        log.info("first case apac -> apac+ww, ww");
        authCabAndGo("goodhits/ghits/campaign_id/" + apacCampaignId);
        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, cisCampaignId), String.format(patternForMailCountingByBodyTextSuccess, mixedCampaignId));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(mixedCampaignId, cisCampaignId);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "first case apac -> apac+ww, ww");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", "19");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, cisCampaignId)), "first case apac -> apac+ww, ww (mail_pull)");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, mixedCampaignId)), "first case apac -> apac+ww, ww (mail_pull)");

        log.info("third not apac -> apac");
        authCabAndGo("goodhits/ghits/?id=1197&campaign_id=" + cisCampaignId);
        operationMySql.getMailPull().getCountLettersByBody(String.format(failRegionMatchingLetterSingle, "1197", "1197"));
        pagesInit.getCabTeasers().copyTeaserByMassActionsToOneCampaign(apacCampaignId);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "third not apac -> apac");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1197");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(failRegionMatchingLetterSingle, "1197", "1197")), "third not apac -> apac (mail_pull)");

        log.info("forth case not apac -> apac, ww");
        authCabAndGo("goodhits/ghits/?id=1003&campaign_id=" + cisCampaignId);

        softAssert.assertEquals(operationMySql.getMailPull().getCountLettersByBody(formatedMessage).get(formatedMessage).intValue(), 0);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(mixedCampaignId, apacCampaignId);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "forth case not apac -> apac, ww");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", "1003");
        softAssert.assertEquals(operationMySql.getMailPull().getCountLettersByBody(formatedMessage).get(formatedMessage).intValue(), 2);

        log.info("Check amount of teasers after copy the teaser");
        serviceInit.getDockerCli().runAndStopCron(COUNT_TEASERS_FOR_RK);

        log.info("check amount of teasers after copy the teaser");
        authCabAndGo("goodhits/ghits/campaign_id/" + mixedCampaignId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "forth count at ww campaign");
        authCabAndGo("goodhits/ghits/campaign_id/" + apacCampaignId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(1), "forth count at apac campaign too");
        authCabAndGo("goodhits/ghits/campaign_id/" + cisCampaignId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(3), "second count at mixed campaign");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Ограничение копирования тизеров категории Casinos and Gambling")
    @Description("Ограничение копирования тизеров категории Casinos and Gambling\n" +
            "     <ul>\n" +
            "      <li> 1.Case Canada - Canada - single copy +</li>\n" +
            "      <li> 2.Case Canada - Canada, Canada+Australia - multi copy -</li>\n" +
            "      <li> 3.Case Canada - USA - single copy -</li>\n" +
            "      <li> 4.Case Canada - Canada, Canada - multi copy +</li>\n" +
            "      <li> 5.Case Australia,Canada - Canada - single copy +</li>\n" +
            "      <li> 6.Case Australia,Canada - Canada, Canada - multi copy +</li>\n" +
            "      <li> 7.Case USA exclude - United Kingdom include - single +</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-23112\">Ticket TA-23112</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Ограничение копирования тизеров категории Casinos and Gambling")
    public void checkCanCopyTeasersWithCategoryCasinosAndGambling() {
        try {
            log.info("Test is started");
            int campaignFromCopy = 14;
            int campaignToCopy = 15;
            int campaignWithMixedTargeting = 16;
            int copiedTeaser1 = 28;
            Integer copiedTeaser2 = 1189;
            int copiedTeaser3 = 1190;
            int copiedTeaser4 = 1191;
            int copiedTeaser5 = 1192;
            int copiedTeaser6 = 1193;

            String failMessage = "Results of copying ads \n" +
                    " Casinos And Gambling Ad's can only be copied to campaigns with the same targeting as in the donor campaign (%s)";

            operationMySql.getgHits1().deleteTeasersByCampaignsButOne(28, 15, 16);

            log.info("settings GEO to campaigns");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignFromCopy, "Canada");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignToCopy, "Canada");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithMixedTargeting, "Canada", "Australia");

            log.info("2.Case Canada -> Canada, Canada+Australia -> multi copy +/-");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser1);
            pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithMixedTargeting, campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "2.Case Canada -> Canada, Canada+Australia (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy), String.format(failMessage, copiedTeaser1));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(copiedTeaser1));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy)), "2.Case Canada -> Canada, Canada+Australia (mail_pull) " + String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(failMessage, copiedTeaser1)), "2.Case Canada -> Canada, Canada+Australia (mail_pull) " + failMessage);

            log.info("3.Case Canada -> USA -> single copy -");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignToCopy, "United States");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser2);
            pagesInit.getCabTeasers().copyTeaserByMassActionsToOneCampaign(campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "3.Case Canada -> USA -> single copy (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(failMessage, copiedTeaser2));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", copiedTeaser2.toString());
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(failMessage, copiedTeaser2)), "3.Case Canada -> USA -> single copy (mail_pull)");


            log.info("4.Case Canada -> Canada, Canada -> multi copy +");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignToCopy, "Canada");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithMixedTargeting, "Canada");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser3);
            pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithMixedTargeting, campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "4.Case Canada -> Canada, Canada -> multi copy+ (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy), String.format(patternForMailCountingByBodyTextSuccess, campaignWithMixedTargeting));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(copiedTeaser3));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy)), "4.1 Case Canada -> Canada, Canada -> multi copy+ (mail_pull)");
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithMixedTargeting)), "4.2 Case Canada -> Canada, Canada -> multi copy+ (mail_pull)");


            log.info("5.Case Australia,Canada -> Canada -> single copy +");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignFromCopy, "Australia", "Canada");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser4);
            pagesInit.getCabTeasers().copyTeaserByMassActionsToOneCampaign(campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "5.Case Australia,Canada -> Canada -> single copy (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(copiedTeaser4));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy)), "5.Case Australia,Canada -> Canada -> single copy (mail_pull)");


            log.info("6.Case Australia,Canada -> Canada, Canada -> multi copy +");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser5);
            pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithMixedTargeting, campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "6.Case Australia,Canada -> Canada, Canada -> multi copy (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy), String.format(patternForMailCountingByBodyTextSuccess, campaignWithMixedTargeting));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(copiedTeaser5));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy)), "6.1 Case Australia,Canada -> Canada, Canada -> multi copy (mail_pull)");
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithMixedTargeting)), "6.2 Case Australia,Canada -> Canada, Canada -> multi copy (mail_pull)");


            log.info("7.Case USA exclude  -> United Kingdom include -> single +");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(campaignFromCopy, "United States");
            operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignToCopy, "United Kingdom");
            authCabAndGo("goodhits/ghits/?campaign_id=" + campaignFromCopy + "&id=" + copiedTeaser6);
            pagesInit.getCabTeasers().copyTeaserByMassActionsToOneCampaign(campaignToCopy);
            softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "7.Case USA exclude  -> United Kingdom include -> single + (ui-message)");
            operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy));
            serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(copiedTeaser6));
            softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignToCopy)), "7.Case USA exclude  -> United Kingdom include -> single + (mail_pull)");


            log.info("8.check amount of teasers after copy the teaser");
            serviceInit.getDockerCli().runAndStopCron(COUNT_TEASERS_FOR_RK);

            authCabAndGo("goodhits/ghits/campaign_id/" + campaignWithMixedTargeting);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(2), "8.1.Get count teasers after copy in campaignWithMixedTargeting");
            authCabAndGo("goodhits/ghits/campaign_id/" + campaignToCopy);
            softAssert.assertTrue(pagesInit.getCabTeasers().checkAmountOfTeaser(5), "8.2.Get count teasers after copy in campaignToCopy");
            softAssert.assertAll();

            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Feature("Copy Teasers")
    @Story("Копирование тизера с разрешенной категорией( не Currencies, Options, Stocks and Bonds) и с гео Испания")
    @Description("Копирование тизера с разрешенной категорией( не Currencies, Options, Stocks and Bonds) и с гео Испания\n" +
            "     <ul>\n" +
            "      <li>Копирование тизера с разрешенной категорией( не Currencies, Options, Stocks and Bonds)</li>\n" +
            "      <ul>" +
            "      <li>Кампанию с тергетингом Испания</li>\n" +
            "      <li>Кампанию с тергетингом кроме Испании</li>\n" +
            "      <li>Проверка что тизер одобрен в кампании с тергетингом Испания</li>\n" +
            "      <li>Проверка что тизер одобрен в кампании кампании с тергетингом кроме Испании</li>\n" +
            "      </ul>" +
            "     <ul>\n" +
            "      <li>Копирование тизера с запрещенной категорией( Currencies, Options, Stocks and Bonds)</li>\n" +
            "      <ul>" +
            "      <li>Кампанию с тергетингом Испания</li>\n" +
            "      <li>Кампанию с тергетингом кроме Испании</li>\n" +
            "      <li>Проверка что тизер на модерации в кампании с тергетингом Испания</li>\n" +
            "      <li>Проверка что тизер одобрен в кампании кампании с тергетингом кроме Испании</li>\n" +
            "      </ul>" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-24470\">Ticket TA-24470</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Копирование тизера с разрешенной категорией( не Currencies, Options, Stocks and Bonds)")
    public void checkCopyTeasersWithGeoSpain() {
        log.info("Test is started");
        int campaignSpain = 1017;
        int campaignMixedWithSpain = 1018;
        int campaignWithoutSpain = 1019;
        int teaserDonorAllowedCategory = 1011;
        int teaserDonorForbidCategory = 1012;
        int teaserRecipient;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignSpain, "Spain");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignMixedWithSpain, "Spain", "Canada", "Republic of Moldova");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithoutSpain, "Russian Federation", "Canada", "Republic of Moldova");

        log.info("Start copy teaser with allowed category");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignSpain + "&id=" + teaserDonorAllowedCategory);
        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain), String.format(patternForMailCountingByBodyTextSuccess, campaignMixedWithSpain));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithoutSpain, campaignMixedWithSpain);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(teaserDonorAllowedCategory));
        log.info("Check email message");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain)), "(mail_pull) 1");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignMixedWithSpain)), "(mail_pull) 2");

        log.info("Get id of new teaser and check approve icon - " + campaignMixedWithSpain + " " + teaserDonorAllowedCategory);

        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignMixedWithSpain, teaserDonorAllowedCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignMixedWithSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignMixedWithSpain first copy");

        log.info("Get id of new teaser and check approve icon - " + campaignWithoutSpain + " " + teaserDonorAllowedCategory);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignWithoutSpain, teaserDonorAllowedCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignWithoutSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignWithoutSpain first copy");

        log.info("Start copy teaser with forbid category");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignSpain + "&id=" + teaserDonorForbidCategory);
        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain), String.format(successLetterWithBlockerTeasser, campaignMixedWithSpain, teaserDonorForbidCategory, campaignMixedWithSpain));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithoutSpain, campaignMixedWithSpain);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message second copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(teaserDonorForbidCategory));

        log.info("Check email message");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain)), "(mail_pull) - 1");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(successLetterWithBlockerTeasser, campaignMixedWithSpain, teaserDonorForbidCategory, campaignMixedWithSpain)), "(mail_pull) - 2");

        log.info("Get id of new teaser and check approve icon - " + campaignMixedWithSpain + " " + teaserDonorForbidCategory);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignMixedWithSpain, teaserDonorForbidCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignMixedWithSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignMixedWithSpain second copy");

        log.info("Get id of new teaser and check approve icon - " + campaignWithoutSpain + " " + teaserDonorForbidCategory);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignWithoutSpain, teaserDonorForbidCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignWithoutSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignWithoutSpain second copy");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Копирование тизера с запрещенной категорией( Currencies, Options, Stocks and Bonds) и с гео не Испания в")
    @Description("Копирование тизера с запрещенной категорией( Currencies, Options, Stocks and Bonds)\n" +
            "     <ul>\n" +
            "      <li> Кампанию с тергетингом Испания</li>\n" +
            "      <li> Кампанию с тергетингом не включающий Испании</li>\n" +
            "      <li> Проверка что тизер на модерации в кампании с тергетингом Испания</li>\n" +
            "      <li> Проверка что тизер одобрен в кампании кампании с тергетингом кроме Испании</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-24470\">Ticket TA-24470</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Копирование тизера с гео не Испания")
    public void checkCopyTeasersWithoutGeoSpain() {
        log.info("Test is started");
        int campaignSpain = 1020;
        int campaignWithoutSpain = 1021;
        int campaignWithoutSpainDonor = 1022;
        int teaserDonorForbidCategory = 1013;
        int teaserRecipient;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithoutSpainDonor, "Norway", "Canada", "Latvia");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignSpain, "Spain", "Canada", "Latvia");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithoutSpain, "Canada", "Latvia");

        log.info("Start copy teaser with forbidden category");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignWithoutSpainDonor + "&id=" + teaserDonorForbidCategory);
        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain), String.format(successLetterWithBlockerTeasser, campaignSpain, teaserDonorForbidCategory, campaignSpain));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithoutSpain, campaignSpain);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(teaserDonorForbidCategory));
        log.info("Check email message");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignWithoutSpain)), "(mail_pull)");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(successLetterWithBlockerTeasser, campaignSpain, teaserDonorForbidCategory, campaignSpain)), "(mail_pull)");

        log.info("Get id of new teaser and check approve icon - " + campaignWithoutSpain + " " + teaserDonorForbidCategory);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignWithoutSpain, teaserDonorForbidCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignWithoutSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignMixedWithSpain first copy");

        log.info("Get id of new teaser and check approve icon - " + campaignSpain + " " + teaserDonorForbidCategory);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignSpain, teaserDonorForbidCategory);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignSpain + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser from campaignWithoutSpain second copy");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Копирование тизеров с флагом IAP compliant и без, с гео Италия")
    @Description("Копирование тизеров с флагом IAP compliant и без, с гео Италия\n" +
            "     <ul>\n" +
            "      <li> Кампанию с флагом IAP и гео Италия</li>\n" +
            "      <li> Кампанию без флага IAP и гео Италия</li>\n" +
            "      <li> Проверка скопированих тизеров и присвоение им флага IAP для кампании с флагом IAP</li>\n" +
            "      <li> Проверка скопированих тизеров и присвоение им флага IAP для кампании без флагом IAP</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-24321\">Ticket TA-24321</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Копирование тизеров с флагом IAP compliant и без, с гео Италия")
    public void checkCopyTeasersItalyIapCompliant() {
        log.info("Test is started");
        int campaignItalyDonor = 1030;
        int campaignItalyRecepWithIap = 1031;
        int campaignItalyRecepWithoutIap = 1032;
        int teaserDonorWithoutIap = 1020;
        int teaserDonorWithIap = 1021;
        int teaserRecipient;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyDonor, "Italy", "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyRecepWithIap, "Italy");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignItalyRecepWithoutIap, "Italy", "Latvia");

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyDonor);
        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignItalyRecepWithIap), String.format(patternForMailCountingByBodyTextSuccess, campaignItalyRecepWithoutIap));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignItalyRecepWithIap, campaignItalyRecepWithoutIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(teaserDonorWithoutIap), Integer.toString(teaserDonorWithIap));

        log.info("Check email message");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignItalyRecepWithIap)), "(mail_pull) - 2");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignItalyRecepWithoutIap)), "(mail_pull) - 3");

        log.info("Get id of new teaser and check flag IAP - campaignItalyRecepWithIap, teaserDonorWithoutIap");
        softAssert.assertFalse(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignItalyRecepWithIap, teaserDonorWithoutIap), "check teaser campaignItalyRecepWithIap , teaserDonorWithoutIap");

        log.info("Get id of new teaser and check flag IAP - campaignItalyRecepWithIap, teaserDonorWithIap");
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignItalyRecepWithIap, teaserDonorWithIap);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyRecepWithIap + "&id=" + teaserRecipient);

        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser campaignItalyRecepWithIap , teaserDonorWithIap approve icon");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedEnabledIapCompliant(), "check teaser campaignItalyRecepWithIap , teaserDonorWithIap is displayed compliant");

        log.info("Get id of new teaser and check flag IAP - campaignItalyRecepWithoutIap, teaserDonorWithoutIap");
        softAssert.assertFalse(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignItalyRecepWithoutIap, teaserDonorWithoutIap), "FAIL - present");

        log.info("Get id of new teaser and check flag IAP - campaignItalyRecepWithoutIap, teaserDonorWithIap");
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignItalyRecepWithoutIap, teaserDonorWithIap);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignItalyRecepWithoutIap + "&id=" + teaserRecipient);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedDisabledIapCompliant(), "check teaser campaignItalyRecepWithoutIap , teaserDonorWithIap");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Копирование тизера c записью в teasers_offers с проверкой наследования записи скопированым тизерам")
    @Description("Копирование тизера c записью в teasers_offers с проверкой наследования записи скопированым тизерам\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-24468\">Ticket TA-24468</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Копирование тизера c записью в teasers_offers с проверкой наследования записи")
    public void checkTeasersOffersAfterCopyTeasers() {
        log.info("Test is started");
        int campaignDonor = 1044;
        int campaignRecep1 = 1045;
        int campaignRecep2 = 1046;
        int teaserDonor = 1035;
        int teaserRecipient;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecep1, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecep2, "Canada");

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecep1, campaignRecep2);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=2", "-vvv", Integer.toString(teaserDonor));

        log.info("Get id of new teaser and check teasers_offer option - " + campaignRecep1 + " " + teaserDonor);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignRecep1, teaserDonor);
        softAssert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(teaserRecipient).contains("Something"), "FAIL - " + campaignRecep1);
        log.info("Get id of new teaser and check teasers_offer option - " + campaignRecep2 + " " + teaserDonor);
        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignRecep2, teaserDonor);
        softAssert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(teaserRecipient).contains("Something"), "FAIL - " + campaignRecep2);

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Интеграция Cloudinary. Cab. Копирование тизеров")
    @Description("Интеграция Cloudinary. Cab. Копирование тизеров\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-27435\">Ticket TA-27435</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Интеграция Cloudinary. Cab. Копирование тизеров")
    public void copyTeaserCampaignCloudinaryToCampaignCloudinary() {
        log.info("Test is started");
        int campaignWithCloudinary1 = 1050;
        int campaignWithCloudinary2 = 1049;
        int teaserWithCloudinary = 1037;
        int teaserResult;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithCloudinary1, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignWithCloudinary2, "Canada");

        log.info("Start copy teasers with cloudinary to cloudinary");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignWithCloudinary2);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignWithCloudinary1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", Integer.toString(teaserWithCloudinary));

        log.info("Check result of copying - " + teaserWithCloudinary);
        softAssert.assertTrue(operationMySql.getCopyRequestTeasers().isPresentRecordTeaserCopyRequest(teaserWithCloudinary), "FAIL - record is present");

        teaserResult = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignWithCloudinary1, teaserWithCloudinary);
        softAssert.assertTrue(operationMySql.getgHits1().getOptionsValue(teaserResult).contains("\"validFormatsOnly\": true"), "FAIL - option not present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Правила копирования гео LATAM (и унификация по Испании)")
    @Description("Правила копирования гео LATAM (и унификация по Испании)\n" +
            "     <ul>\n" +
            "      <li>Проверка что тизер скопировался заапрувленым - право включено\n" +
            "      <li>Проверка что тизер скопировался и отправлен на модерацию - право включено\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-27425\">Ticket TA-27425</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Интеграция Cloudinary. Cab. Копирование тизеров")
    @Privilege(name = "ghits_copy_no_moderation")
    public void checkModerationStatusTeaserWithGeoSpainCampaignAfterCopy() {
        log.info("Test is started");
        int campaignDonor = 1083;
        int campaignRecepient1 = 1084;
        int campaignRecepient2 = 1085;

        log.info("Check teaser status. Geo Spain, Canada -> 'Ukraine', 'Canada'");
        authCabForCheckPrivileges("goodhits/ghits?campaign_id=" + campaignDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada", "Spain");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Spain");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient2, "Spain");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits_copy_no_moderation");

        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1083");
        log.info("Check approve teaser enable right");
        authCabForCheckPrivileges("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Icon is displayed");

        authCabForCheckPrivileges("goodhits/ghits?campaign_id=" + campaignDonor);
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits_copy_no_moderation");
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient2);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message second copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1083");
        log.info("Check approve teaser disable right");
        authCabForCheckPrivileges("goodhits/ghits?campaign_id=" + campaignRecepient2);
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Icon is not displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copy Teasers")
    @Story("Копіювання тизерів в рамках одного типу РК")
    @Description("Копіювання тизерів в рамках одного типу РК\n" +
            "     <ul>\n" +
            "      <li>Проверка что тизер не скопировался product -> push\n" +
            "      <li>Проверка что тизер не скопировался push -> product\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51827\">Ticket TA-51827</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(dataProvider = "copyTeasersToDifferentCampaignType", description = "Копіювання тизерів в рамках одного типу РК")
    public void checkCopyTeasersToDifferentCampaignType(int campaignDonor, int campaignRecepient1, Integer teaserDonor) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Canada");


        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(letterWithDifferentCampaignsType, teaserDonor));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(letterWithDifferentCampaignsType, teaserDonor)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersToDifferentCampaignType(){
        return new Object[][]{
                {1128, 1129, 1135},
                {1130, 1131, 1136}
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    @Description("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category\n" +
            "     <ul>\n" +
            "      <li>Category subscriptions: WW -> CIS\n" +
            "      <li>Category subscriptions: APAC -> CIS\n" +
            "      <li>Category subscriptions: LATAM -> CIS\n" +
            "      <li>Category subscriptions: VIET NAM -> CIS\n" +
            "      <li>Category subscriptions: LATAM -> CIS+WW\n" +
                "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51803\">Ticket TA-51803</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    //category isn't available, Labunets discuss with business
    @Test(enabled = false, dataProvider = "copyTeaserWithSubscribersCategoryAndAllowedGeoToCis", description = "Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    public void checkCopyTeaserWithSubscribersCategoryAndForbidGeoToCis(int campaignDonor, int campaignRecepient, Integer teaserDonor, String donorGeo, String...recipientGeo) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, donorGeo);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, recipientGeo);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMail("Results of copying ads"), String.format(letterWithSubscriptionCategory, teaserDonor), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeaserWithSubscribersCategoryAndForbidGeoToCis(){
        return new Object[][]{
                {1132, 1133 , 1137, "Portugal", new String[] {"Russian Federation"}},
                {1134, 1135 , 1138, "Brazil", new String[] {"Kazakhstan"}},
                {1136, 1137 , 1139, "Malaysia", new String[] {"Georgia"}},
                {1136, 1137 , 1139, "Viet Nam", new String[] {"Republic of Moldova"}},
                {1140, 1141, 1141, "Brazil", new String[] {"Kyrgyzstan", "Canada"}},
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    @Description("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category\n" +
            "     <ul>\n" +
            "      <li>Category subscriptions: CIS -> CIS\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51803\">Ticket TA-51803</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    //category isn't available, Labunets discuss with business
    @Test(enabled = false, dataProvider = "copyTeaserWithSubscribersCategoryAndAllowedGeoToCis", description = "Prohibition of copy from WW to CIS  subscriptions category")
    public void checkCopyTeaserWithSubscribersCategoryAndAllowedGeoToCis(int campaignDonor, int campaignRecepient, String donorGeo, String...recipientGeo) {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, donorGeo);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, recipientGeo);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", "1140");

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMail(subjectLetter, String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeaserWithSubscribersCategoryAndAllowedGeoToCis(){
        return new Object[][]{
                {1138, 1139, "Kazakhstan", new String[] {"Russian Federation"}},
        };
    }

    @Feature("Copy Teasers")
    @Story("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category")
    @Description("Copying teasers | Prohibition of copy from WW to CIS  subscriptions category\n" +
            "     <ul>\n" +
            "      <li>Category subscriptions: All geo -> CIS\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51803\">Ticket TA-51803</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    //category isn't available, Labunets discuss with business
    @Test(enabled = false, description = "Prohibition of copy from WW to CIS  subscriptions category")
    public void checkCopyTeaserWithSubscribersCategoryAndAllGeoToCis() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 0;
        int campaignDonor = 1142;
        int campaignRecepient = 1143;
        Integer donorTeaserId = 1142;
        String recipientGeo = "Latvia";

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(campaignDonor, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, recipientGeo);

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMail(subjectLetter, String.format(letterWithSubscriptionCategory, donorTeaserId)), "FAIL - Check message of copy result");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Copy Teasers")
    @Feature("Sexual health rule")
    @Story("List interface. Mass action")
    @Description("Check rule sexual health success copy\n" +
            "     <ul>\n" +
            "      <li>Campaign with geo United States, Spain, India. Category of teaser - Sexual health</li>\n" +
            "      <li>Copy to campaign without geo targeting</li>\n" +
            "      <li>Copy to campaign with United States, Spain, India targeting</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52211\">Ticket TA-52211</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check rule sexual health success copy")
    public void checkCopyTeaserWithSexualHealthSuccessCopy() {
        log.info("Test is started");
        Integer expectedAmountOfCopiedTeasers = 1;
        int campaignDonor = 1335;
        int campaignRecepient1 = 1336;
        int campaignRecepient2 = 1337;
        Integer donorTeaserId = 1342;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "United States", "Spain", "India");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient2, "United States", "Spain", "India");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1), String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient2));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1, campaignRecepient2);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1)), "FAIL - Check message of copy result campaignRecepient1");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient2)), "FAIL - Check message of copy result campaignRecepient2");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present campaignRecepient1");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient2);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers(), expectedAmountOfCopiedTeasers, "FAIL - teaser should not be present campaignRecepient2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Copy Teasers")
    @Feature("Sexual health rule")
    @Story("List interface. Mass action")
    @Description("Check rule sexual health success and forbid copy\n" +
            "     <ul>\n" +
            "      <li>Campaign with geo United States, Spain, India. Category of teaser - Sexual health</li>\n" +
            "      <li>Copy to campaign with required geo targeting - United States, Spain, India</li>\n" +
            "      <li>Copy to campaign with disallowed geo targeting - Italy</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52211\">Ticket TA-52211</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check rule sexual health success and forbid copy")
    public void checkCopyTeaserWithSexualHealthSuccessFailedCopy() {
        log.info("Test is started");
        String forbiddenText = "Results of copying ads \n" +
                " Sexual Health Ad's can only be copied to campaigns with the same targeting as in the donor campaign (1343)";

        int campaignDonor = 1338;
        int campaignRecepient1 = 1339;
        int campaignRecepient2 = 1340;
        Integer donorTeaserId = 1343;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "United States", "Spain", "India");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "United States", "Spain", "India");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient2, "Italy");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1), String.format(forbiddenText, donorTeaserId));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient1, campaignRecepient2);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1)), "FAIL - Check message of copy result campaignRecepient1");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody( String.format(forbiddenText, donorTeaserId)), "FAIL - Check message of copy result campaignRecepient2");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient1);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 1, "FAIL - teaser should not be present campaignRecepient1");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient2);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 0, "FAIL - teaser should not be present campaignRecepient2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Copy Teasers")
    @Feature("Sexual health rule")
    @Story("List interface. Mass action")
    @Description("Check copy with different category from Sexual health with geo forbidden for copying with Sexual health \n" +
            "     <ul>\n" +
            "      <li>Campaign with geo United States, Spain, India. Category of teaser is different from Sexual health</li>\n" +
            "      <li>Copy to campaign with geo targeting - Italy</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52211\">Ticket TA-52211</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check copy with different category from Sexual health with geo forbidden for copying with Sexual health")
    public void checkCopyTeaserWithoutSexualHealth() {
        log.info("Test is started");

        int campaignDonor = 1341;
        int campaignRecepient = 1342;
        Integer donorTeaserId = 1344;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, "United States", "Spain", "India");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Italy");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient)), "FAIL - Check message of copy result campaignRecepient1");

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 1, "FAIL - teaser should not be present campaignRecepient1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Copy Teasers")
    @Feature("Ad name inheritance")
    @Story("List interface. Mass action")
    @Description("Check copy with inherit ad name from recipient campaign \n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52212\">Ticket TA-52212</a></li>\n" +
            "     </ul>")
    @Owner(value="NIO")
    @Test (description = "Check copy with inherit ad name from recipient campaign")
    public void checkCopyTeaserInheritAdNameRecipient() {
        log.info("Test is started");

        int campaignDonor = 1355;
        int campaignRecepient = 1344;
        Integer donorTeaserId = 1358;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Germany", "India");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?id=1358&campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient));
        pagesInit.getCabTeasers().needToBlockTeaser(false).copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient)), "FAIL - Check message of copy result campaignRecepient1");

        int teaserResult = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignRecepient, donorTeaserId);

        authCabAndGo("goodhits/ghits?campaign_id=" + campaignRecepient);
        softAssert.assertEquals(pagesInit.getCabTeasers().getAmountOfTeasers().intValue(), 1, "FAIL - teaser should  be present campaignRecepient1");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeasersAdvertiserName(String.valueOf(teaserResult)), "Awesome name", "FAIL - advert name problem");
        softAssert.assertTrue(pagesInit.getCabTeasers().isTeaserUnblocked(),  "FAIL - blocked thing");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(COPY_TEASER)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("" +
            "     <ul>\n" +
            "      <li>campaignRecepient1, limit = 2, 3 teaser one blocked and other dropped</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52212\">Ticket TA-52212</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check copy with over limit for campaign")
    public void checkCopyTeaserWithOverLimitForCampaign() {
        log.info("Test is started");

        int campaignDonor = 1425;
        int campaignRecepient1 = 1430;
        int campaignRecepient2 = 1431;
        Integer donorTeaserId = 1480;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient1, "Germany", "India");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?id=" + donorTeaserId + "&campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1));
        pagesInit.getCabTeasers().needToBlockTeaser(false).copyTeaserByMassActionsIntoCampaigns(campaignRecepient1, campaignRecepient2);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertFalse(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient1)), "FAIL - Check message of copy result campaignRecepient1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(COPY_TEASER)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("" +
            "     <ul>\n" +
            "      <li>campaignRecepient, limit = 2, 3 regular teaser</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52212\">Ticket TA-52212</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check create teaser with exceeded limit")
    public void checkCopyTeaserWithExceededLimit() {
        log.info("Test is started");

        int campaignDonor = 1427;
        int campaignRecepient = 1430;
        Integer donorTeaserId = 1484;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepient, "Germany", "India");

        log.info("Copy teaser to campaign with differ type");
        authCabAndGo("goodhits/ghits?id=" + 1484 + "&campaign_id=" + campaignDonor);

        operationMySql.getMailPull().getCountLettersByBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient));
        pagesInit.getCabTeasers().needToBlockTeaser(false).copyTeaserByMassActionsIntoCampaigns(campaignRecepient);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", donorTeaserId.toString());

        softAssert.assertFalse(operationMySql.getMailPull().checkCustomMailBody(String.format(patternForMailCountingByBodyTextSuccess, campaignRecepient)), "FAIL - Check message of copy result campaignRecepient1");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
