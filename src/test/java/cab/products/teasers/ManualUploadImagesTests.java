package cab.products.teasers;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.open;
import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;

public class ManualUploadImagesTests extends TestBase {

    private void goToCreateTeaser(int campaignId){
        authCabAndGo("goodhits/ghits-add/campaign_id/" + campaignId);
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Check create teaser with manual loading with:\n" +
            "     <ul>\n" +
            "      <li>gif</li>\n" +
            "      <li>static</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52032\">Ticket TA-52032</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check create teaser with manual loading with gif and static", dataProvider = "createWithManualUploading")
    public void createWithManualUploading(String image16_9, String image3_2, String image1_1, int animatedValue) {
        log.info("Test is started");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("create teaser");
        pagesInit.getCabTeasers().openManualLoading();

        pagesInit.getCabTeasers().setImages(image16_9, image3_2, image1_1).loadImageManualCrateInterface();
        softAssert.assertNotEquals(pagesInit.getCabTeasers()
                .useTitle(true)
                .useDescription(true)
                .setNeedToEditImage(false)
                .useDiscount(true)
                        .setNeedToEditCategory(false)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        int teaserId = pagesInit.getCabTeasers().getTeaserId();
        String options = operationMySql.getgHits1().getOptionsValue(teaserId);

        log.info("Options value - " + options);
        softAssert.assertFalse(options.contains("clEffects"), "clEffects");
        softAssert.assertTrue(options.contains("\"manualImages\": true"), "manualImages");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), animatedValue, "manualImages");

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkImageVisibility());
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] createWithManualUploading(){
        return new Object[][]{
                {"16_9.jpeg", "3_2.jpeg", "1_1.png", 0},
        };
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Check load gif through manual image form\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52226\">Ticket TA-52226</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check load gif through manual image form")
    public void checkLoadGifIntoManualImagesForm() {
        log.info("Test is started");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("Open form");
        pagesInit.getCabTeasers().openManualLoading();

        log.info("Load for 16_9");
        pagesInit.getCabTeasers().setImage("duck_639_360.gif").loadImageManualLoading("16_9");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - 16_9");

        log.info("Load for 3_2");
        pagesInit.getCabTeasers().setImage("planet_600_399.gif").loadImageManualLoading("3_2");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - 3_2");

        log.info("Load for 1_1");
        pagesInit.getCabTeasers().setImage("owl_499_500.gif").loadImageManualLoading("1_1");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - 1_1");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Check load gif through manual image form with static images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52226\">Ticket TA-52226</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check load gif through manual image form with static images")
    public void createTeasersWithDifferentTypeImages() {
        log.info("Test is started");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("Open form");
        pagesInit.getCabTeasers().openManualLoading();

        pagesInit.getCabTeasers().setImages("16_9.gif", "3_2.jpeg", "1_1.png").loadImageManualCrateInterface();
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(true)
                .setNeedToEditImage(false)
                        .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Images uploaded with manual upload is incorrect"), "FAIL - message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_CREATE_CAB_INTERFACE)
    @Description("Check load gif with one empty format\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52032\">Ticket TA-52032</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check load gif with one empty format")
    public void createTeasersWithOneEmptyFormat() {
        log.info("Test is started");
        goToCreateTeaser(CAMPAIGN_MGID_PRODUCT);

        log.info("Open form");
        pagesInit.getCabTeasers().openManualLoading();

        pagesInit.getCabTeasers().setImages("16_9.jpeg", "3_2.jpeg").loadImageManualCrateInterface();
        softAssert.assertEquals(pagesInit.getCabTeasers()
                .useDescription(true)
                .setNeedToEditImage(false)
                        .setNeedToEditCategory(false)
                .useDiscount(true)
                .createTeaser(), 0, "FAIL -> teaser doesn't create");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Images uploaded with manual upload is incorrect"), "FAIL - space_500_499.gif");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("Check load gif with usual image inline\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52032\">Ticket TA-52032</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check load gif with usual image inline")
    public void editTeasersInlineList() {
        log.info("Test is started");
        authCabAndGo("goodhits/ghits?id=1318");

        log.info("Open form");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().setImages("1_1.gif").loadImageManualListInterface();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - message wrong");

        pagesInit.getCabTeasers().setImages("3_2.gif").loadImageManualListInterface();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - message wrong");

        pagesInit.getCabTeasers().setImages("16_9.gif").loadImageManualListInterface();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - message wrong");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("Check edit gif to static images inline\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52188\">Ticket TA-52188</a></li>\n" +
            "     </ul>")
    @Owner(HOV)
    @Test(description = "Check edit gif to static images inline")
    public void editGifsToImagesFormatInline() {
        log.info("Test is started");
        int teaserId = 1198;
        authCabAndGo("goodhits/ghits?id=" + teaserId);

        log.info("Open form");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().setImages("16_9.jpeg", "3_2.jpeg", "1_1.png").loadImageManualListInterface();
        pagesInit.getCabTeasers().savePopupManualImageSettings();

        Assert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "Incorrect animation value is displayed!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_INLINE_CAB_INTERFACE)
    @Description("Check editing teasers image followed by sending it to moderation\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52188\">Ticket TA-52188</a></li>\n" +
            "     </ul>")
    @Owner(HOV)
    @Test(description = "Check editing teasers image followed by sending it to moderation")
    public void editApprovedAndBlockedTeaserInline() {
        log.info("Test is started");
        int teaserId = 5000;
        authCabAndGo("goodhits/ghits?id=" + teaserId);

        log.info("Open form");
        pagesInit.getCabTeasers().openPopupEditImageInline();

        pagesInit.getCabTeasers().setImages("3_2.jpeg").loadImageManualListInterface();
        pagesInit.getCabTeasers().savePopupManualImageSettings();

        Assert.assertEquals(operationMySql.getgHits1().getQuarantineValue(teaserId), 1, "Incorrect quarantine value is displayed!");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check displaying images at zion tab\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52032\">Ticket TA-52032</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check displaying images at zion tab")
    public void checkDisplayingImagesAtZionTab() {
        log.info("Test is started");
        int amountOfDisplayedGifs = 3;
        authCabAndGo("goodhits/on-moderation?id=1198");

        log.info("Open form");
        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        Assert.assertEquals(pagesInit.getCabTeasersModeration().getAmountOfDisplayedManualImages(), amountOfDisplayedGifs);

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(MANUAL_UPLOAD_IMAGES)
    @Story(TEASER_LIST_DASH_INTERFACE)
    @Description("Check redirect links for teaser preview manual loaded images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51927\">Ticket TA-51927</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check redirect links for teaser preview manual loaded images")
    public void checkRedirectLinksTeaserManualLoading() {
        log.info("Test is started");
        int teaserId = 1205;
        String partOflink = "mgid.com/ghits/" + teaserId;

        authDashAndGo("testemail1000@ex.ua", "advertisers/teasers-goods/campaign_id/1222/search/" + teaserId);
        open(pagesInit.getTeaserClass().getLinkForPreviewTeaserInWidget());

        Assert.assertTrue(pagesInit.getTeaserClass().checkLinksOfRedirection(partOflink));
        log.info("Test is finished");
    }
}
