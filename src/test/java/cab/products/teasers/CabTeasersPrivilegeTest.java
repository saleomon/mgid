package cab.products.teasers;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.cab.products.variables.CabTeaserVariables.error_offer_missing;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabTeasersPrivilegeTest extends TestBase {

    /**
     * Check approve without landing and right
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24520">TA-24520</a>
     */
    @Privilege(name = "can_approve_teaser_without_offer")
    @Test
    public void createApproveTeaserWithoutOfferRight() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_approve_teaser_without_offer");
        authCabForCheckPrivileges("goodhits/ghits/?id=1009");
        pagesInit.getCabTeasers().approveTeaserWithoutLinkingOffer();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(error_offer_missing), "approve list");
        pagesInit.getCabTeasers().rejectTeaser("7");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(error_offer_missing), "reject list");

        authCabForCheckPrivileges("goodhits/on-moderation/?id=1030");
        pagesInit.getCabTeasersModeration().approveTeaserWithoutLinkingOffer();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(error_offer_missing), "approve moderation");
        pagesInit.getCabTeasersModeration().rejectTeaser();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(error_offer_missing), "reject moderation");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Валидация апрува/реджекта тизера с/без офера. Массовые действия
     * <ul>
     *  <li>Кампанию с тергетингом Испания</li>
     *  <li>Кампанию с тергетингом не включающий Испании</li>
     *  <li>Проверка что тизер на модерации в кампании с тергетингом Испания</li>
     *  <li>Проверка что тизер одобрен в кампании кампании с тергетингом кроме Испании</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24673">Ticket TA-24673</a>
     * <p>NIO</p>
     */
    @Privilege(name = "can_approve_teaser_without_offer")
    @Test
    public void checkCopyTeasersWithoutOfferRightMass() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_approve_teaser_without_offer");

        log.info("Approve teasers by mass actions");
        authCabForCheckPrivileges("goodhits/ghits/?campaign_id=1054");
        pagesInit.getCabTeasers().approveTeaserMassAction();
        log.info("Check error message");
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedPopupLinkOffer(), "FAIL - isDisplayedPopupLinkOffer");

        log.info("Check amount of accept icons");
        authCabForCheckPrivileges("goodhits/ghits/?campaign_id=1054");
        softAssert.assertEquals(pagesInit.getCabTeasers().checkAmountOfDisplayedAcceptIcon(), 2, "FAIL - amount of accepts");

        log.info("Check amount of reject icons");
        pagesInit.getCabTeasers().rejectTeaserMassAction();
        authCabForCheckPrivileges("goodhits/ghits/?campaign_id=1054");
        softAssert.assertEquals(pagesInit.getCabTeasers().checkAmountOfDisplayedRejectIcon(), 0, "FAIL - amount of rejects");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Скачивание сертификатов и вывод в интерфейсы | CAB</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51719">Ticket TA-51719</a>
     * <p>NIO</p>
     */
    @Privilege(name = "ghits-download-certificate")
    @Test
    public void downloadCertificateRight() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-download-certificate");
        authCabForCheckPrivileges("goodhits/ghits?id=1096");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedDownloadCertificateButton(), "teaser list");

        authCabForCheckPrivileges("goodhits/on-moderation/?id=1096");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedDownloadCertificateButton(), "teaser moderation");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Getty Images")
    @Story("Image downloaded from Getty Images and edited")
    @Description("Check privilege for 'Downloaded getty images'\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52115\">Ticket TA-52115</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Privilege(name = "can_use_edited_getty_image_flag")
    @Test (description = "Check privilege for 'Downloaded getty images'")
    public void checkPrivilegeDownloadedGettyImagesFlag() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_use_edited_getty_image_flag");
        authCabForCheckPrivileges("goodhits/ghits-add/campaign_id/" + CAMPAIGN_MGID_PRODUCT);
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedDownloadGettyImages());
        log.info("Test is finished");
    }

    @Feature("Filters")
    @Story("Filter by moderator action")
    @Description("Check privilege filter moderation action\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52163\">Ticket TA-52163</a></li>\n" +
            "     </ul>")
    @Privilege(name = "ghits_can_see_mod_action_filter")
    @Owner(value="NIO")
    @Test (description = "Check privilege filter moderation action")
    public void checkPrivilegeModerationActionFilter() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits_can_see_mod_action_filter");
        authCabForCheckPrivileges("goodhits/ghits");
        Assert.assertFalse(pagesInit.getCabTeasers().isDisplayedModerationActionFilter());
        log.info("Test is finished");
    }
}
