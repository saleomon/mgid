package cab.products.teasers;

import core.service.constantTemplates.ConstantsInit;
import io.qameta.allure.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static core.service.constantTemplates.ConstantsInit.TEASER_LIST_CAB_INTERFACE;
import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static pages.cab.products.variables.CabTeaserVariables.*;
import static testData.project.CliCommandsList.Cron.COPY_TEASERS;

public class CompliantTeaser2Tests extends TestBase {
    private final String subjectLetter = "Results of copying ads";

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(ConstantsInit.COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers with compliant and allowed category\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers with compliant and allowed category", dataProvider = "checkCopyTeasersCompliantAllowedCategoryData")
    public void checkCopyTeasersCompliantAllowedCategory(String geo, CabTeasers.CompliantType type) {
        log.info("Test is started");
        int campaignDonor = 1056;
        int campaignRecepWithIap = 1057;
        Integer teaserDonor = 1058;
        int teaserRecipient;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, geo, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepWithIap, geo);
        operationMySql.getgPartners1().updateOptionCompliant(campaignRecepWithIap, type.getCompliantType());
        operationMySql.getgHits1().updateOption(teaserDonor, type);

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignRecepWithIap, teaserDonor);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignRecepWithIap + "&id=" + teaserRecipient);

//        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser campaignRecepWithIap , teaserDonorWithIap icon approve");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkCompliantCountryIcon(geo), "check teaser campaignRecepWithIap , teaserDonorWithIap icon country");
        softAssert.assertTrue(pagesInit.getCabTeasers().isActiveIconCompliant(geo), "check teaser campaignRecepWithIap , teaserDonorWithIap icon is active");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkCopyTeasersCompliantAllowedCategoryData() {
        return new Object[][]{
                {"Italy", IAP},
                {"Romania", RAC},
                {"Spain", AUTOCONTROL},
                {"Czech Republic", ZPL},
                {"Poland", POLAND_REG_COMPLIANT}
        };
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(ConstantsInit.COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers without compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers without compliant", dataProvider = "checkCopyWithMissingOneRequiredCompliantData")
    public void checkCopyWithMissingOneRequiredCompliant(Integer teaserDonor, String message, CabTeasers.CompliantType...type) {
        log.info("Test is started");
        int campaignDonor = 1073;
        int campaignRecepWithIap = 1072;
        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepWithIap, "Italy", "Romania", "Spain", "Czech Republic", "Poland");
        operationMySql.getgHits1().updateOption(teaserDonor, type);

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor + "&id=" + teaserDonor);
        operationMySql.getMailPull().getCountLettersByBody(String.format(message, teaserDonor));
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        softAssert.assertFalse(operationMySql.getCopyRequestTeasers().checkPresenceRecordingForTeaser(campaignRecepWithIap, teaserDonor), "FAIL - record is present");
        softAssert.assertTrue(operationMySql.getMailPull().checkCustomMailBody(String.format(message, teaserDonor)), "FAIL - message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkCopyWithMissingOneRequiredCompliantData() {
        return new Object[][]{
                {1194, messageMissingRac,  new CabTeasers.CompliantType[]{IAP, AUTOCONTROL, POLAND_REG_COMPLIANT}},
                {1195, messageMissingAutocompliant, new CabTeasers.CompliantType[]{IAP, RAC, POLAND_REG_COMPLIANT}},
                {1196, messageMissingIap,  new CabTeasers.CompliantType[]{AUTOCONTROL, RAC, POLAND_REG_COMPLIANT}},
                {1302, messageMissingPoland,  new CabTeasers.CompliantType[]{IAP, AUTOCONTROL, RAC}}
        };
    }

    @Epic(TEASERS)
    @Features({@Feature(COMPLIANT), @Feature(ConstantsInit.COPY_TEASER)})
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Description("Check copy teasers with compliant to campaign without compliant geo\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-27522\">Ticket TA-27522</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Check copy teasers with compliant to campaign without compliant geo", dataProvider = "copyTeasersIapCompliantToCampaignWithNotCompliantGeo")
    public void checkCopyTeasersIapCompliantToCampaignWithNotCompliantGeo(int campaignDonor, int campaignRecepWithIap, Integer teaserDonor, String geo, CabTeasers.CompliantType type) {
        log.info("Test is started");
        int teaserRecipient;

        log.info("Set settings GEO to campaigns");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignDonor, geo, "Canada");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignRecepWithIap, geo);
        operationMySql.getgHits1().updateTeaserTitleAndApprove(campaignRecepWithIap, geo);
        operationMySql.getgHits1().updateOption(teaserDonor, type);

        log.info("Start copy teasers");
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignDonor);
        operationMySql.getMailPull().getCountLettersBySubject(subjectLetter);
        pagesInit.getCabTeasers().copyTeaserByMassActionsIntoCampaigns(campaignRecepWithIap);

        log.info("Check cab message");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ads are queued for copy and will be proccessed soon"), "Check message first copy");
        serviceInit.getDockerCli().runAndStopCron(COPY_TEASERS, "--limit=1", "-vvv", teaserDonor.toString());

        teaserRecipient = operationMySql.getCopyRequestTeasers().selectTeaserIdAfterCopy(campaignRecepWithIap, teaserDonor);
        authCabAndGo("goodhits/ghits/?campaign_id=" + campaignRecepWithIap + "&id=" + teaserRecipient);

//        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "check teaser campaignRecepWithIap , teaserDonorWithIap icon approve");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkCompliantCountryIcon(geo), "check teaser campaignRecepWithIap , teaserDonorWithIap icon country");
        softAssert.assertFalse(pagesInit.getCabTeasers().isActiveIconCompliant(geo), "check teaser campaignRecepWithIap , teaserDonorWithIap icon is active");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] copyTeasersIapCompliantToCampaignWithNotCompliantGeo() {
        return new Object[][]{
                {1056, 1061, 1058, "Italy", IAP},
                {1056, 1061, 1058, "Romania", RAC},
                {1056, 1061, 1058, "Spain", AUTOCONTROL},
                {1056, 1061, 1058, "Czech Republic", ZPL},
                {1056, 1061, 1058, "Poland", POLAND_REG_COMPLIANT},
                {1070, 1071, 1066, "Italy", IAP},
                {1070, 1071, 1066, "Romania", RAC},
                {1070, 1071, 1066, "Spain", AUTOCONTROL},
                {1070, 1071, 1066, "Czech Republic", ZPL},
                {1070, 1071, 1066, "Poland", POLAND_REG_COMPLIANT}
        };
    }
}
