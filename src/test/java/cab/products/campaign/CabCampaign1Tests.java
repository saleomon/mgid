package cab.products.campaign;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.helpers.BaseHelper;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.List;

import static pages.cab.products.variables.CampaignsVariables.*;
import static core.helpers.BaseHelper.randomNumbersInt;
import static testData.project.AuthUserCabData.*;
import static testData.project.ClientsEntities.CLIENT_MGID_RUB_ID;
import static testData.project.ClientsEntities.CLIENT_MGID_UAH_ID;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.TargetingType.Targeting.*;

public class CabCampaign1Tests extends TestBase {
    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23731">https://youtrack.mgid.com/issue/TA-23731</a>
     * check budget day limit for Rub client
     * NIO
     */
    @Test
    public void checkDayLimitRub() {
        log.info("Test is started");

        log.info("create campaign Rub");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "2499", "2499")
                .createCampaignWithLimitsChanges();

        log.info("check validation Rub client");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum daily limit: 2500. To remove limit enter \"0\""), "check validation message");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "2500", "2500")
                .createCampaignWithLimitsChanges();

        log.info("check campaign was created");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkPresenceCampaignAtListInterface(pagesInit.getCabCampaigns().getCampaignName()), "check campaign at teaser list interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23731">https://youtrack.mgid.com/issue/TA-23731</a>
     * check budget day limit for Uah client
     * NIO
     */
    @Test
    public void checkDayLimitUah() {
        log.info("Test is started");

        log.info("create campaign Uah");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_UAH_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "1249", "1249")
                .createCampaignWithLimitsChanges();

        log.info("check validation Uah client");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum daily limit: 1250. To remove limit enter \"0\""), "check validation message");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_UAH_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "1250", "1250")
                .createCampaignWithLimitsChanges();

        log.info("check campaign was created");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkPresenceCampaignAtListInterface(pagesInit.getCabCampaigns().getCampaignName()), "check campaign at teaser list interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23962">https://youtrack.mgid.com/issue/TA-23962</a>
     * Внедрение нового фильтра по куратору в интерфейс goodhits/campaigns
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     * RKO
     */
    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator = helpersInit.getBaseHelper().getRandomFromArray(new String[]{indiaProductUser, goodhitsCurator, cisProductUser, europeProductUser});
        authCabAndGo("goodhits/campaigns");
        log.info("set " + curator + " curator and search campaigns");
        Assert.assertTrue(pagesInit.getCabCampaigns().checkWorkCuratorFilter(curator), "FAIL -> campaigns don't search by curator " + curator);
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23028">https://youtrack.mgid.com/issue/TA-23028</a>
     * Маркировать APAC регионы с особыми требованиями по креативу
     * - проверка отображения иконки APAC с включенными регионами APAC в таргетинге в интерфейсах: список кампаний, список тизеров, модерация, zion
     * - проверка отображения иконки APAC с выключенными регионами APAC в таргетинге в интерфейсах: список кампаний, список тизеров, модерация, zion
     * NIO
     */
    @Test
    public void checkApacRegionsIcon() {
        String[] regions = {"India", "Malaysia"};
        String campaignId = "12";
        String teaserId = "20";
        log.info("Test is started");
        log.info("Update targeting with apac regions");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), regions);
        log.info("Check icon at campaign list interface");
        authCabAndGo("goodhits/campaigns/id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkDisplayingApacIcon(regions), "campaign list with apac regions");
        authCabAndGo("goodhits/ghits/campaign_id/" + campaignId);
        log.info("Check icon at teaser list interface");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkDisplayingApacIcon(), "teaser list with apac regions");
        log.info("Check icon at teaser moderation interface");
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkDisplayingApacIcon(), "quarantine list with apac regions");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkDisplayingApacIconAtZionTab(), "zion tab with apac regions");
        log.info("Exclude apac regions");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(Integer.parseInt(campaignId), "India", "Malaysia", "Viet Nam", "Thailand", "Indonesia", "Comoros");
        log.info("Check icon at campaign list interface");
        authCabAndGo("goodhits/campaigns/id/" + campaignId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkDisplayingApacIcon(), "campaign list without apac regions");
        authCabAndGo("goodhits/ghits/campaign_id/" + campaignId);
        log.info("Check icon at teaser list interface");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkDisplayingApacIcon(), "teaser list without apac regions");
        log.info("Check icon at teaser moderation interface");
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkDisplayingApacIcon(), "quarantine list without apac regions");
        log.info("Check icon at teaser moderation interface zion");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkDisplayingApacIconAtZionTab(), "zion tab without apac regions");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка клонирования(копирования) кампании с включенными всеми настройками
     * КАТЕГОРИЯ и ЯЗЫК - изменяются/не изменяются при копировании
     * Все методы универсальные могут принимать как все параметры - 'all', так и группу - "showName", "isWhite", "copySchedule", "copyAdLivetime"
     * RKO
     */
    @Test(dataProvider = "dataForCreateCloneCampaignWithAllOptions")
    public void createCloneCampaignWithAllOptions(String logs, String campaignId, boolean isCopyCategory,
                                                  boolean isCopyLanguage, String filterId) {
        log.info("Test is started - " + logs);
        log.info("Insert geo-targeting");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), "India");
        pagesInit.getCabCampaigns().setCustomTaggingSwitchOn(true);
        pagesInit.getCabCampaigns().setFilterType(filterId);

        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        log.info("Create clone");
        pagesInit.getCabCampaigns().createCloneCompanyWithCopyAllOptions(isCopyCategory, isCopyLanguage, clonedCampaignSettingsAll, true);

        log.info("Check clone options in LIST interface");
        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCloneInListInterface(clonedCampaignSettingsAll),
                "Check clone options in LIST interface - FALSE");

        log.info("Check clone options in EDIT interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkOptionsForCloneInEditInterface(clonedCampaignSettingsAll),
                "Check clone options in EDIT interface - FALSE");

        log.info("check list widgets in 'Filter blocked publishers'");
        authCabAndGo("goodhits/campaigns-filters/filter/" + filterId + "/pid/" + pagesInit.getCabCampaigns().getCampaignId());

        softAssert.assertEquals(pagesInit.getFilterBlockedPublishers().getDataCheckedWidgets("all"), widgetsCampaignFilter,
                "check list widgets in 'Filter blocked publishers' - FALSE");

        log.info("Check Data Widgets list and their Factor In 'Publishers efficiency' interface");
        authCabAndGo("goodhits/analysis-quality-platforms/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertEquals(pagesInit.getPublishersEfficiencyClass().getWidgetsIdAndFactor(), widgetsAndFactorForPublisherEfficiency,
                "Check Data Widgets list and their Factor In 'Publishers efficiency' interface - FALSE");

        softAssert.assertAll();
        log.info("Test is finished - " + logs);
    }

    @DataProvider
    public Object[][] dataForCreateCloneCampaignWithAllOptions() {
        return new Object[][]{
                {"static category, language and filter except for create clone", "18", true, true, "2"},
                {"choose category, language and filter only for create clone", "19", false, false, "1"}
        };
    }

    /**
     * Админка. Добавить новый тип кампании Search Feed в фильтры, добавить новую иконку
     * <ul>
     *  <li>Проверка работы фильтра по типу кампании</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24329">Ticket TA-24329</a>
     * <p>NIO</p>
     */
    @Test
    public void checkFilterByCampaignType() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns");
        log.info("Filter by campaign type");
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("campaignTypesBox", "Search feed");
        pagesInit.getCabCampaigns().pressFilterButton();
        log.info("Get amount from db");
        String campaignAmount = pagesInit.getCabCampaigns().getTotalAmountOfCampaigns();
        String amountCampaignDB = operationMySql.getgPartners1().selectAmountOfCampaignsWithType("search_feed");
        log.info("Check amount from db and from interface");
        Assert.assertEquals(amountCampaignDB, campaignAmount);
        log.info("Test is finished");
    }

    /**
     * Админка. Добавить новый тип кампании Search Feed в фильтры, добавить новую иконку
     * <ul>
     *  <li>Проверка иконки типа кампании</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24329">Ticket TA-24329</a>
     * <p>NIO</p>
     */
    @Test
    public void checkCampaignIconForSearchFeedType() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns/?campaign_types=search_feed");
        log.info("Check visibility of icon");
        Assert.assertTrue(pagesInit.getCabCampaigns().checkVisibilityOfSearchFeedIcon());
        log.info("Test is finished");
    }

    /**
     * Cab | Слетающая настройка таргетинга при редактировании при создании рк
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24557">Ticket TA-24557</a>
     * <p>NIO</p>
     */
    @Test
    public void checkTargetSettigsAfterChangingCampaignType() {
        log.info("Test is started");
        authCabAndGo("goodhits/clients-new-campaign/client_id/1000");
        pagesInit.getCabCampaigns().selectTargeting();
        pagesInit.getCabCampaigns().chooseCampaignType("product");
        log.info("check campaign targeting");
        Assert.assertTrue(pagesInit.getCabCampaigns().checkDisplayedTargeting());
        log.info("Test is finished");
    }

    /**
     * <p>Ограничение при создание РК на гео включая Bьетнам с категориями
     * "Currencies", "Options", "Stocks and Bonds", "Casino and gambling"</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27204">Ticket TA-27204</a>
     * <p>NIO</p>
     */
    @Test
    public void checkCreateCampaignWithGeoVietnamAndForbiddenCategories() {
        log.info("Test is started");
        //"Currencies", "Options", "Stocks and Bonds", "Casino and gambling"
        String[] categories = {"254", "149", "148", "105"};
        String category = categories[randomNumbersInt(categories.length)];
        authCabAndGo("goodhits/clients-new-campaign/client_id/1000");
        log.info("create campaign");
        pagesInit.getCabCampaigns().setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage("1")
                .setCampaignCategory(category)
                .setUseTargeting(true)
                .selectLocationTarget("include", "Vietnam")
                .createCampaign();
        log.info("Check error message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Category is not available for the selected region"));
        log.info("Test is finished");
    }

    /**
     * <p>Ограничение при редактировании РК на гео включая Bьетнам с категориями
     * "Currencies", "Options", "Stocks and Bonds", "Casino and gambling"</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-32808">Ticket TA-32808</a>
     * <p>NIO</p>
     */
    @Test
    public void checkEditCampaignWithGeoVietnamAndForbiddenCategories() {
        log.info("Test is started");
        //"Currencies", "Options", "Stocks and Bonds", "Casino and gambling"
        String[] categories = {"254", "149", "148", "105"};
        String category = categories[randomNumbersInt(categories.length)];
        authCabAndGo("goodhits/campaigns-edit/id/1047");
        log.info("edit campaign");
        pagesInit.getCabCampaigns()
                .setCampaignCategory(category)
                .setUseTargeting(true)
                .selectLocationTarget("exclude", "Italy")
                .editCampaign();

        log.info("Check error message");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Category is not available for the selected region"));
        log.info("Test is finished");
    }

    /**
     * <p>Изменение принципа работы со сменой типа кампании/категориями</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51781">Ticket TA-51781</a>
     * <p>NIO</p>
     */
    @Test(dataProvider = "changeCampaignType")
    public void checkChangeCampaignTypeByIcon(int campaignId, String campaignType, String[] arrayCategories, boolean alertIsPresent, int teaserId, int isBlockedValue) {
        log.info("test is started. campaign id - " + campaignId);
        String category = helpersInit.getBaseHelper().getRandomFromArray(arrayCategories);
        authCabAndGo("goodhits/campaigns?id=" + campaignId);
        pagesInit.getCabCampaigns().openChangeCampaignTypePopup();
        pagesInit.getCabCampaigns().changeCampaignTypeAndCategory(campaignType, category);

        softAssert.assertEquals(pagesInit.getCabCampaigns().checkAlertPresenceWithText(alertIsPresent), alertIsPresent, "Fail - alert message");
        helpersInit.getBaseHelper().alertIsShow();
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignType(campaignId), campaignType, "Fail - selectCampaignType");
        softAssert.assertEquals(operationMySql.getgPartners1().selectCampaignCategory(campaignId), category, "Fail - selectCampaignCategory");
        softAssert.assertEquals(operationMySql.getgHits1().getBlockedValue(teaserId), isBlockedValue, "Fail - getBlockedValue");
        softAssert.assertEquals(operationMySql.getgHits1().getCategoryIdValue(teaserId), category, "Fail - getCategoryIdValue");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] changeCampaignType() {
        return new Object[][]{
                {1124,  "content", new String[]{"230", "241", "215"}, false, 1131, 0},
                //toDo NIO Plaksa + Ira try to undestand is it correct behavior 12.05.2023
//                {1125,  "product", new String[]{"108", "251", "168"},  true, 1132, 1}
        };
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка контекстного таргетинга при:\n" +
            "<ul>\n" +
            "   <li>создании продуктовой кампании</li>\n" +
            "   <li>редактировании продуктовой кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51893\">Ticket TA-51893</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактировании продуктовой кампании с блоком контекстного таргетинга")
    public void createCampaignWithContextTargeting() {
        log.info("Test is started");

        int clientId = 2001;
        log.info("Create campaign");
        String languageForContext = helpersInit.getBaseHelper().getRandomFromList(
                operationMySql.getGlobalSettings().getContextTargetingLanguageIds());

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage(languageForContext)
                .setLimitsOption("budget", "500", "500")
                .setTargetSections("context", "sentiments", "location")
                .createCampaign();

        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(),
                "FAIL -> Name in Campaigns list interface!");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkContextSettingsInListInterface("Including"),
                "FAIL -> Context in Campaigns list interface!");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSentimentsSettingsInListInterface("Including"),
                "FAIL -> Sentiments in Campaigns list interface!");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after create");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setShowsOnlyForThisLanguage(true)
                .setCampaignLanguage("1")
                .setTargetSections("context", "sentiments", "location", "os")
                .setLimitsOption("clicks", "500", "500")
                .editCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after edit");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка отсутствия блока контекстного таргетинга для языков не из списка:\n" +
            "<ul>\n" +
            "   <li>Английский\n" +
            "   <li>Русский\n" +
            "   <li>Итальянский\n" +
            "   <li>Украинский\n" +
            "   <li>Индонезийский\n" +
            "   <li>Тайский\n" +
            "   <li>Вьетнамский\n" +
            "   <li>Испанский</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51893\">Ticket TA-51893</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание продуктовой кампании с языком, для которого недоступен контекстный таргетинг",
            dataProvider = "campaignTypes")
    public void checkCreatingCampaignForLanguagesWithoutContextTargeting(String campaignType) {
        log.info("Test is started");
        int clientId = 2035;
        log.info("Create campaign");
        List<String> languagesForContext = operationMySql.getGlobalSettings().getContextTargetingLanguageIds();
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignType(campaignType)
                .enableTargeting(true)
                .chooseCampaignLanguage(pagesInit.getCabCampaigns().chooseLanguageWithoutContext(languagesForContext));
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedContextTargetingTab(),
                "FAIL -> Context targeting is present for language - "
                        + pagesInit.getCabCampaigns().getCampaignLanguage()
                        + " in campaign type - "
                        + campaignType + "!");
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedSentimentsTargetingTab(),
                "FAIL -> Sentiments targeting is present for language - "
                        + pagesInit.getCabCampaigns().getCampaignLanguage()
                        + " in campaign type - "
                        + campaignType + "!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Контекстный таргетинг")
    @Description("Проверка блока контекстного таргетинга в кампаниях разных типов")
    @Test(description = "Проверка контекстного таргетинга в кампаниях разных типов", dataProvider = "campaignTypes")
    public void checkCreatingCampaignsForLanguagesWithContextTargeting(String campaignType) {
        log.info("Test is started");
        int clientId = 2035;
        log.info("Create campaign");
        String languageForContext = helpersInit.getBaseHelper().getRandomFromList(
                operationMySql.getGlobalSettings().getContextTargetingLanguageIds());
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignType(campaignType)
                .enableTargeting(true)
                .chooseCampaignLanguage(languageForContext);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedContextTargetingTab(),
                "FAIL -> Context targeting is not present for language - "
                        + languageForContext + " in campaign type - " + campaignType + "!");
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedSentimentsTargetingTab(),
                "FAIL -> Sentiments targeting is not present for language - "
                        + languageForContext
                        + " in campaign type - "
                        + campaignType + "!");
        log.info("Test is finished");
        softAssert.assertAll();
    }

    @DataProvider
    public Object[][] campaignTypes() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"dsp"},
                {"search_feed"},
                {"video"}
        };
    }

    @DataProvider
    public Object[][] campaignTypesExcludeTargetingDisabled() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"dsp"},
                {"search_feed"},
        };
    }

    @Story("Валидация макросов")
    @Description("Валидация макросов {gdpr} и {gdpr_consent}\n" +
            "<ul>\n" +
            "   <li>при создании кампании</li>\n" +
            "   <li>при редактировании кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51808\">Ticket TA-51808</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактировании кампании с макросами {gdpr} и {gdpr_consent}")
    public void checkCreateCampaignWithGdprMacros() {
        log.info("Test is started");

        int clientId = 2001;
        log.info("Create campaign with GDPR macros");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkUtmDescriptionInEditCampaignInterface("{gdpr}, {gdpr_consent}"),
                "FAIL -> Custom tagging description!");
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("product")
                .setCampaignName("Campaign with GDPR" + BaseHelper.getRandomWord(6))
                .setTargetSections("location")
                .setLimitsOption("budget", "500", "500")
                .setUtmType("gdpr={gdpr}&gdpr_consent={gdpr_consent}")
                .createCampaign();

        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkUTMSettingsInEditCampaignInterface(),
                "FAIL -> Check UTM macroses {gdpr} и {gdpr_consent} after creating!");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setTargetSections("location")
                .setLimitsOption("clicks", "500", "500")
                .setUtmType("gdpr_consent={gdpr_consent}&gdpr={gdpr}")
                .editCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkUTMSettingsInEditCampaignInterface(),
                "FAIL -> Check UTM macroses {gdpr} и {gdpr_consent} after editing!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Campaign flag")
    @Description("Проверка права флана открутки кампании " +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51946\">Ticket TA-51946</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Privilege(name = "ghits-set-campaign-can-rotate-ua")
    @Test(description = "Проверка права флага открутки кампании")
    public void checkPrivilegeNotUkrainianLanguageWithGeoUkraineIcon() {
        log.info("Test is started");
        int campaignIdUkraineWithoutUkrLanguage = 1238;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdUkraineWithoutUkrLanguage, "Ukraine");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-set-campaign-can-rotate-ua");

        authCabForCheckPrivileges("goodhits/campaigns/id/" + campaignIdUkraineWithoutUkrLanguage);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedRotateUkraineIcon(), "FAIL - should be visible");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-set-campaign-can-rotate-ua");

        authCabForCheckPrivileges("goodhits/campaigns/id/" + campaignIdUkraineWithoutUkrLanguage);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedRotateUkraineIcon(), "FAIL - should be hidden");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Добавление таргета на стоимость телефонов")
    @Story("Добавление таргета на ценовой диапазон устройств")
    @Description("Проверка таргета на ценовой диапазон устройств при:\n" +
            "<ul>\n" +
            "   <li>создании продуктовой кампании</li>\n" +
            "   <li>редактировании продуктовой кампании</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51876\">Ticket TA-51876</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Создание и редактировании продуктовой кампании с блоком таргетинга на ценовой диапазон телефонов")
    public void createCampaignWithPhonesPricesTargeting() {
        log.info("Test is started");

        int clientId = 2001;
        log.info("Create campaign");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setLimitsOption("budget", "500", "500")
                .setTargetSections("phonePriceRange", "location")
                .createCampaign();

        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(),
                "FAIL -> Name in Campaigns list interface!");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(),
                "FAIL -> Check targeting after creating!");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setShowsOnlyForThisLanguage(true)
                .setTargetSections("phonePriceRange", "location")
                .setLimitsOption("clicks", "500", "500")
                .editCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(),
                "FAIL -> Check targeting after editing!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Campaign flag")
    @Description("Проверка флага открутки кампании:" +
            "<ul>\n" +
            "   <li>без украинского языка з украинским гео</li>\n" +
            "   <li>с украинским языком з украинским гео</li>\n" +
            "   <li>изменение флага</li>\n" +
            "   <li>без украинского языка з крымским гео</li>\n" +
            "</ul>\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51946\">Ticket TA-51946</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Проверка флага открутки кампании")
    public void checkFlagNotUkrainianLanguageWithGeoUkraine() {
        log.info("Test is started");
        int campaignIdUkraineWithoutUkrLanguage = 1225;
        int campaignIdUkraineWithUkrLanguage = 1227;
        int campaignIdKrym = 1226;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdUkraineWithoutUkrLanguage, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdUkraineWithUkrLanguage, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingIncludeRegions(campaignIdKrym, "Krym");

        authCabAndGo("goodhits/campaigns/id/" + campaignIdUkraineWithoutUkrLanguage);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkStateOfRotateInUkraine(), "FAIL - campaignIdUkraineWithoutUkrLanguage");

        pagesInit.getCabCampaigns().changeStateOfRotateInUkraine(true);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkStateOfRotateInUkraine(), "FAIL - campaignIdUkraineWithoutUkrLanguage after changing");

        authCabAndGo("goodhits/campaigns/id/" + campaignIdUkraineWithUkrLanguage);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedRotateUkraineIcon(), "FAIL - campaignIdUkraineWithUkrLanguage");

        authCabAndGo("goodhits/campaigns/id/" + campaignIdKrym);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedRotateUkraineIcon(), "FAIL - campaignIdKrym");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Вывод таргетов в дашборд (соц. дем., интересы, цены девайсов) p.2")
    @Story("Изменение вывода в дереве таргетов для демографии и интересов")
    @Description("Проверка того, что интересы, соцдем и тип подключения не работают на exclude" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51960\">Ticket TA-51960</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
//   todo AIA https://jira.mgid.com/browse/BT-5603 https://jira.mgid.com/browse/TA-52204 @Test(dataProvider = "campaignTypesExcludeTargetingDisabled")
    public void checkExcludeTargetingDisabled(String campaignType) {
        log.info("Test is started");
        int clientId = 2035;
        log.info("Create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignType(campaignType)
                .enableTargeting(true);
        pagesInit.getCabCampaigns().setTargetsExclude(MOBILE_CONNECTION);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkTargetsPresence(MOBILE_CONNECTION),
                "FAIL -> 'Mobile connection' target can be excluded!");
        pagesInit.getCabCampaigns().setTargetsExclude(DEMOGRAPHICS);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkTargetsPresence(DEMOGRAPHICS),
                "FAIL -> 'Demographics' target can be excluded!");
        pagesInit.getCabCampaigns().setTargetsExclude(INTERESTS);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkTargetsPresence(INTERESTS),
                "FAIL -> 'Interests' target can be excluded!");
        softAssert.assertAll();
        log.info("Test is finished");

    }
    @Feature("Вывод таргетов в дашборд (соц. дем., интересы, цены девайсов) p.2")
    @Story("Изменение вывода в дереве таргетов для демографии и интересов")
    @Description("Проверка того, что интересы, соцдем и тип подключения не работают на exclude" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51960\">Ticket TA-51960</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
//   todo AIA https://jira.mgid.com/browse/BT-5603 https://jira.mgid.com/browse/TA-52204   @Test
    public void checkExcludeTargetingDisabledVideo() {
        log.info("Test is started");
        int clientId = 2035;
        log.info("Create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignType("video")
                .enableTargeting(true);

        pagesInit.getCabCampaigns().setTargetsExclude(DEMOGRAPHICS);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkTargetsPresence(DEMOGRAPHICS),
                "FAIL -> 'Demographics' target can be excluded!");
        pagesInit.getCabCampaigns().setTargetsExclude(INTERESTS);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkTargetsPresence(INTERESTS),
                "FAIL -> 'Interests' target can be excluded!");
        softAssert.assertAll();
        log.info("Test is finished");

    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Cab")
    @Description("Check message at create interface:" +
            "<ul>\n" +
            "   <li>Language != Ukraine, choose targeting Ukraine</li>\n" +
            "   <li>Targeting == Ukraine, choose language  not Ukrainian</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51934\">Ticket TA-51934</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "check message at create interface")
    public void checkMessageAtCreateInterface() {
        log.info("Test is started");
        int clientId = 1;
        log.info("Create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignLanguage("2");

        log.info("Check message language != Ukraine, targeting all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - first check");

        log.info("Check message language != Ukraine, targeting include Ukraine");
        pagesInit.getCabCampaigns()
                .chooseCampaignLanguage("2")
                .selectLocationTarget("include", "Ukraine");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - second check");

        log.info("Check targeting include Ukraine, message language != Ukraine");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .selectLocationTarget("include", "Ukraine");
        pagesInit.getCabCampaigns().chooseCampaignLanguage("2");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - third check");

        log.info("Check message language != Ukraine, targeting exclude Ukraine");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().chooseCampaignLanguage("2").selectLocationTarget("exclude", "Ukraine");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - fourth check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Create campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Create campaign form")
    @Description("Check that 'Rotate in MGID network' checkbox is enabled by default")
    @Owner("AIA")
    @Test(dataProvider = "campaignTypesForCheckRotateInMgidForAdskeeperCampaigns",
            description = "Check that 'Rotate in MGID network' checkbox is enabled by default")
    public void checkRotateInMgidForAdskeeperCampaigns(String campaignTypeValue) {
        log.info("Test is started");
        int clientId = 2002;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().chooseCampaignType(campaignTypeValue);
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypesForCheckRotateInMgidForAdskeeperCampaigns() {
        return new Object[][]{
                {"content"},
                {"push"},
                {"search_feed"}
        };
    }

    @Epic("Create campaign")
    @Feature("Rotate in subnet Mgid - improvement")
    @Story("Create campaign form")
    @Description("Check that 'Rotate in MGID network' saved correctly")
    @Owner("AIA")
    @Test(description = "Check that 'Rotate in MGID network' saved correctly")
    public void createAdskeeperCampaignWithRotateInMgid() {
        log.info("Test is started");
        int clientId = 2002;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setUseTargeting(false)
                .createCampaignWithRotateInMgid();
        authCabAndGo("goodhits/campaigns/client_id/2002");
        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkStateRotateMgidSubnet(true),
                "FAIL -> Check Rotate in MGID network after creating campaign!");
        log.info("Test is finished");
    }
}
