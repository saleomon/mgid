package cab.products.campaign;

import com.google.gson.JsonObject;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabCampaigns;
import core.helpers.BaseHelper;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static pages.cab.products.variables.CampaignsVariables.*;
import static testData.project.CliCommandsList.Cron.AUTO_APROVE;
import static testData.project.ClientsEntities.*;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabCampaign2Tests extends TestBase {
    String imageNameDracena = "dracaena-cinnabari.jpg";
    String imageNameStonehenge = "Stonehenge.jpg";

    /**
     * create/edit/check/delete product campaign
     * NIO
     */
    @Test
    public void createProductCampaign() {
        log.info("Test is started");

        int clientId = 1000;
        log.info("create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage("1")
                .setCampaignCategory("112")
                .setUseTargeting(true)
                .setTargetSections("all")
                .setCertificate(LINK_TO_RESOURCES_IMAGES + imageNameDracena)
                .setLimitsOption("budget", "100", "100")
                .setUtmType("gdpr={gdpr}&gdpr_consent={gdpr_consent}")
                .setConversion(CabCampaigns.Conversion.OUR_SENSORS)
                .createCampaign();

        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(), "List interface");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after create");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkUTMSettingsInEditCampaignInterface(), "UTM - create");


                log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setShowsOnlyForThisLanguage(true)
                .setCertificate(LINK_TO_RESOURCES_IMAGES + imageNameStonehenge)
                .setUtmType("gdpr_consent={gdpr_consent}&gdpr={gdpr}")
                .setTargetSections("all")
                .setLimitsOption("clicks", "500", "500")
                .editCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after edit");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkUTMSettingsInEditCampaignInterface(), "UTM - edit");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().pauseCampaign();
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * create/edit/check/delete content campaign
     * <p>NIO</p>
     */
    @Test
    public void createContentCampaign() {
        log.info("Test is started");

        int clientId = 1000;
        log.info("create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage("1")
                .setCampaignCategory("219")
                .setUseTargeting(true)
                .setTargetSections("location", "browser", "os", "audience")
                .setLimitsOption("budget", "200", "200")
                .setConversion(CabCampaigns.Conversion.OUR_SENSORS)
                .createCampaign();

        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(), "List interface");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after create");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setTargetSections("provider", "bundle", "location")
                .setLimitsOption("clicks", "510", "510")
                .editCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after edit");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().pauseCampaign();
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * create/edit/check/delete DSP campaign
     * NIO
     */
    @Test
    public void createDspCampaign() {
        log.info("Test is started");

        int clientId = 1;
        log.info("create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setCampaignType("dsp")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setUseTargeting(true)
                .setTargetSections("location", "browser", "os")
                .setLimitsOption("budget", "1000", "1000")
                .setUseSendSiteInfo(true)
                .setDontSendCampaignToGeoEdge(false)
                .setAuctionType("video")
                .setUtmTaggingGa(false)
                .setIsEnabledCustomTag(false)
                .setOnlyMathedVisitors(true)
                .setUseNativeVersion(true)
                .createDspCampaign();

        softAssert.assertTrue(helpersInit.getBaseHelper().checkDatasetContains(
                operationMySql.getgPartners1().getShowProbabilityCh(
                        pagesInit.getCabCampaigns().getCampaignIdFromListInterface()),
                pagesInit.getCabCampaigns().getShowProbabilityDsp()),
                "Fail - show probability");
        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(), "List interface");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit-dsp/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after create");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings()
                .setUseTargeting(true)
                .setTargetSections("provider", "bundle", "location")
                .setUseNativeVersion(false)
                .setUseSendSiteInfo(false)
                .setDontSendCampaignToGeoEdge(false)
                .setOnlyMathedVisitors(false)
                .setLimitsOption("clicks", "1100", "1100")
                .editDspCampaign();

        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit-dsp/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after edit");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().pauseCampaign();
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }
    /**
     * create/edit/check/delete search feed campaign
     * <p>NIO</p>
     */
    @Test
    public void createSearchFeedCampaign() {
        log.info("Test is started");
        int clientId = 1000;
        JsonObject jsonTargetingElement = new JsonObject();
        JsonObject jsonTargetingTypeAndData = new JsonObject();
        jsonTargetingTypeAndData.addProperty("typeTarget", "include");
        jsonTargetingTypeAndData.addProperty("data-id", "23");

        log.info("create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setCampaignType("search_feed")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage("1")
                .setCampaignCategory("112")
                .setFeedsProvider("Google")
                .setCampaignKeyword("wild elephant")
                .setUtmType("mg=sub_id={click_id}")
                .setLimitsOption("budget", "100", "100")
                .setConversion(CabCampaigns.Conversion.OUR_SENSORS)
                .createCampaign();
        pagesInit.getCabCampaigns().setShowsOnlyForThisLanguage(true);
        jsonTargetingElement.add("bundle", jsonTargetingTypeAndData);
        pagesInit.getCabCampaigns().setTargetingElements(jsonTargetingElement).setCampaignTier("whitehat").setTargetCounter(1);


        log.info("check list interface");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(), "List interface");

        log.info("check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after create");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkVisibilityOfAutoRetargetting(), "FAIL - autoretargetting visibility");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkDisabledTierOption("whitehat"), "FAIL - disable tier option");

        log.info("edit settings");
        pagesInit.getCabCampaigns().clearSettings()
                .setUseTargeting(true)
                .setLimitsOption("clicks", "500", "500")
                .setFeedsProvider("Horse")
                .setCampaignKeyword("racoon")
                .setUtmType("mg={click_id}")
                .setCampaignCategory("114")
                .editCampaign();

        jsonTargetingElement.add("bundle", jsonTargetingTypeAndData);
        pagesInit.getCabCampaigns().setTargetingElements(jsonTargetingElement);
        log.info("check edit interface after editing");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(), "Check after edit");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().pauseCampaign();
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23731">https://youtrack.mgid.com/issue/TA-23731</a>
     * check budget day limit for INR client
     * NIO
     */
    @Test
    public void checkDayLimitInr() {
        log.info("Test is started");

        log.info("create campaign INR");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_INR_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "3749", "3749")
                .createCampaignWithLimitsChanges();

        log.info("check validation INR client");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum daily limit: 3750. To remove limit enter \"0\""), "check validation message");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_INR_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "3750", "3750")
                .createCampaignWithLimitsChanges();

        log.info("check campaign was created");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkPresenceCampaignAtListInterface(pagesInit.getCabCampaigns().getCampaignName()), "check campaign at teaser list interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23731">https://youtrack.mgid.com/issue/TA-23731</a>
     * check budget day limit for Euro client
     * NIO
     */
    @Test
    public void checkDayLimitEuro() {
        log.info("Test is started");

        log.info("create campaign Euro");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_EUR_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "49", "49")
                .createCampaignWithLimitsChanges();

        log.info("check validation Euro client");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum daily limit: 50. To remove limit enter \"0\""), "check validation message");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_EUR_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "50", "50")
                .createCampaignWithLimitsChanges();

        log.info("check campaign was created");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkPresenceCampaignAtListInterface(pagesInit.getCabCampaigns().getCampaignName()), "check campaign at teaser list interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23731">https://youtrack.mgid.com/issue/TA-23731</a>
     * check budget day limit for Usd client
     * NIO
     */
    @Test
    public void checkDayLimitUsd() {
        log.info("Test is started");

        log.info("create campaign Usd");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "49", "49")
                .createCampaignWithLimitsChanges();

        log.info("check validation Usd client");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Minimum daily limit: 50. To remove limit enter \"0\""), "check validation message");

        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType("content")
                .setLimitsOption("budget", "50", "50")
                .createCampaignWithLimitsChanges();

        log.info("check campaign was created");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkPresenceCampaignAtListInterface(pagesInit.getCabCampaigns().getCampaignName()), "check campaign at teaser list interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Cab")
    @Description("Check message at edit interface:" +
            "<ul>\n" +
            "   <li>Language != Ukraine, choose targeting Ukraine</li>\n" +
            "   <li>Targeting == Ukraine, choose language  not Ukrainian</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51934\">Ticket TA-51934</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "check message at edit interface")
    public void checkMessageAtEditInterface() {
        log.info("Test is started");
        int campaignIdWithUkraineLanguage = 1231;
        int campaignIdWithoutUkraineLanguage = 1230;
        log.info("Create campaign");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingExclude(campaignIdWithUkraineLanguage, "Ukraine");

        log.info("Check message language != Ukraine, targeting include Ukraine");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithoutUkraineLanguage);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - first edit check");

        log.info("Check message language != Ukraine, targeting exclude Ukraine");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithoutUkraineLanguage);
        pagesInit.getCabCampaigns()
                .selectLocationTarget("exclude", "Ukraine");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - second edit check");

        log.info("Check message language == Ukraine, targeting exclude Ukraine");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithUkraineLanguage);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - third edit check");

        log.info("Check message language == Ukraine, targeting include Ukraine");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithUkraineLanguage);
        pagesInit.getCabCampaigns().selectLocationTarget("include", "Ukraine");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - fourth edit check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Automatic changes")
    @Description("Check mail to curator" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51936\">Ticket TA-51936</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    //toDo privileges problem https://jira.mgid.com/browse/CP-2807@Test(description = "Check mail to curator after auto approve")
    public void checkMailToCuratorAfterAutoApprove() {
        log.info("Test is started");
        log.info("Create campaign");
        int campaignIdWithoutUkraineLanguage = 1235;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");
        serviceInit.getDockerCli().runAndStopCron(AUTO_APROVE,"-vvv");

        softAssert.assertEquals(operationMySql.getMailPull().getListOfBodyFromMail(subjectUkraineNotRotated).size(), 1, "Amount of messages");
        softAssert.assertTrue(operationMySql.getMailPull().getListOfBodyFromMail(subjectUkraineNotRotated).contains(String.format(messageUkraineNotRotated, campaignIdWithoutUkraineLanguage)), "Fail - first check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Automatic changes")
    @Description("Check changing campaign language after running auto approve " +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51936\">Ticket TA-51936</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    //toDo privileges problem https://jira.mgid.com/browse/CP-2807 @Test(description = "Check changing campaign language after auto approve")
    public void checkChangingCampaignLanguageAfterAutoApprove() {
        log.info("Test is started");
        log.info("Create campaign");
        int campaignIdWithoutUkraineLanguage = 1236;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");
        serviceInit.getDockerCli().runAndStopCron(AUTO_APROVE,"-vvv");

        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithoutUkraineLanguage);

        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignLanguage("50"), "Fail - first check");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Automatic changes")
    @Description("Check mail after changing campaign language manually" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51936\">Ticket TA-51936</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check mail to curator after manual changing language")
    public void checkMailToCuratorAfterChangingCampaignLanguageManually() {
        log.info("Test is started");
        log.info("Create campaign");
        int campaignIdWithoutUkraineLanguage = 1237;

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");
        operationMySql.getTeasersModerationScore().updateDetails(campaignIdWithoutUkraineLanguage, "{\"image\": 100, \"offer\": 100, \"title\": 100, \"landing\": 100}");

        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithoutUkraineLanguage);
        pagesInit.getCabCampaigns().chooseCampaignLanguage("2").saveCampaign();

        Assert.assertFalse(operationMySql.getMailPull().getListOfBodyFromMail(subjectUkraineNotRotated).stream().anyMatch(el->el.contains(Integer.toString(campaignIdWithoutUkraineLanguage))), "Amount of messages");
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Cab")
    @Description("Check message with canRotateUa flag" +
            "<ul>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51934\">Ticket TA-51934</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check message with canRotateUa flag")
    public void checkMessageAtEditInterfaceWithCanRotateUaFlag() {
        log.info("Test is started");
        int campaignIdWithoutUkraineLanguage = 1232;
        log.info("Create campaign");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignIdWithoutUkraineLanguage, "Ukraine");

        log.info("Check message language with rotate flag");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignIdWithoutUkraineLanguage);
        Assert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - rotate flag");
        log.info("Test is finished");
    }

    @Feature("Exclusion not Ukraine language for Ukraine geo")
    @Story("Exclusion not Ukraine language for Ukraine geo | Cab")
    @Description("Check privilege" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51934\">Ticket TA-51934</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Privilege(name="can_see_ukraine_law_alert_msg")
    @Test(description = "check privilege can_see_ukraine_law_alert_msg")
    public void checkPrivilegeCanRotateUa() {
        log.info("Test is started");
        int clientId = 1;
        log.info("Create campaign");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_ukraine_law_alert_msg");

        authCabForCheckPrivileges("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns()
                .chooseCampaignLanguage("2");

        Assert.assertFalse(pagesInit.getCabCampaigns().checkWarningAboutRotationInUkraine(messageRotateUa), "Fail - first check");
        log.info("Test is finished");
    }

    @Feature("Удаление лимитов по конверсиям по всем сабнетам/ Disable limits for conversion for all subnets")
    @Story("Удаление кампаний с лимитом по конверсиям в 'новые мертвые'")
    @Description("Check filter:\n" +
            "<ul>\n" +
            "<li>1) Filter by 'Trash' value - deleted campaign is present in list</li>\n" +
            "<li>2) 'Clone campaign' icon is unavailable for deleted campaign</li>\n" +
            "<li>3) 'Restore campaign' icon is unavailable for deleted campaign</li>\n" +
            "<li>@see <a href \"https://jira.mgid.com/browse/TA-51958\">Ticket TA-51958</a></li>\n" +
            "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check filter job, restore and clone icons for deleted campaign")
    public void checkConditionallyDeletedCampaignsByFilterDeleted() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns/?trash=1&id=" + 2015);
        Assert.assertEquals("2015", pagesInit.getCabCampaigns().getCampaignIdFromListInterface(),
                "FAIL -> Dead campaign is not in list!");
        Assert.assertFalse(pagesInit.getCabCampaigns().checkCloneCampaignIsDisplayed(),
                "FAIL -> Clone campaign icon is displayed!");
        Assert.assertFalse(pagesInit.getCabCampaigns().checkRestoreCampaignIsDisplayed(),
                "FAIL -> Restore campaign icon is displayed!");
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Cases:\n" +
            "<ul>\n" +
            "   <li>Check clone campaign with 'Advertiser name'</>\n" +
            "   <li>Check state of 'Advertiser name' checkbox in clone popup</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check clone campaign with 'Advertiser name'")
    public void checkCloneCampaignWithAdvertiserName() {
        log.info("Test is started");
        String campaignId = "2016";
        authCabAndGo("goodhits/campaigns?id=" + campaignId);
        pagesInit.getCabCampaigns().pressCloneCampaignIcon();
        Assert.assertTrue(pagesInit.getCabCampaigns().checkAdvertiserNameCheckbox(),
                "FAIL -> Check advertiser name checkbox in clone popup!");
        pagesInit.getCabCampaigns().finishCloneCampaign();
        Assert.assertEquals(operationMySql.getgPartners1().selectCampaignAdvertiserName(pagesInit.getCabCampaigns().getCampaignIdFromListInterface()),
                "Advertiser name", "FAIL -> 'Advertiser name' in cloned campaign!");
        log.info("Test is finished");
    }

    @Feature("Inventory planner ( v1)")
    @Story("Perfomance DSP. Create new campaign type")
    @Description("Cases:\n" +
            "<ul>\n" +
                "<li>Create and check in edit interface external product cpm campaign\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-52087\">Ticket TA-52087</a></li>\n" +
                "<li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Create and check in edit interface external product cpm campaign")
    public void createExternalCpmCampaign() {
        log.info("Test is started");

        int clientId = 2001;
        log.info("Create campaign");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setCampaignType("product")
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignMarketingObjective("brand_awareness")
                .setCampaignMediaSource("3")
                .setCampaignCategory("112")
                .setCampaignLanguage("1")
                .setTargetSections("location")
                .setLimitsOption("clicks", "500", "500")
                .setUtmType("gdpr={gdpr}&gdpr_consent={gdpr_consent}")
                .createCampaign();

        log.info("check list interface");
        String campaignId = pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        authCabAndGo("goodhits/campaigns/id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsListInterface(), "List interface");

        log.info("Check edit interface after creating");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignSettingsEditInterface(),
                "FAIL -> Check after creating!");

        log.info("Check media source in edit interface");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkMediaSourceSelectStateInEditCampaignInterface(),
                "FAIL - Media Source is enabled!");
        authDashAndGo("sok.autotest.payouts@mgid.com","advertisers/edit/campaign_id/" + campaignId);
        softAssert.assertFalse(pagesInit.getCampaigns().checkMediaSourceSelectIsDisplayed(),
                "FAIL -> Media source is displayed and enabled in dashboard!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Inventory planner ( v1)")
    @Story("Copy campaign. Improvement")
    @Description("Cases:\n" +
            "<ul>\n" +
                "<li>Clone CPM campaign with Media source settings and Viewability targeting and check result\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-52066\">Ticket TA-52066</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Clone CPM campaign with Media source settings and Viewability targeting")
    public void checkCloneCampaignWithMediaSourceAndViewability() {
        log.info("Test is started!");

        authCabAndGo("goodhits/campaigns/?id=" + 2035);
        log.info("Create clone");
        pagesInit.getCabCampaigns().createCloneCompanyWithCopyAllOptions(true, true, clonedCampaignSettingsViewability, true);

        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();

        log.info("Check clone options in EDIT interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkOptionsForCloneInEditInterface(clonedCampaignSettingsViewability),
                "Check clone options in EDIT interface - FALSE");
        log.info("Test is finished!");
    }

    @Feature("Inventory planner ( v1)")
    @Story("Copy campaign. Improvement")
    @Description("Cases:\n" +
            "<ul>\n" +
                "<li>Clone CPM campaign without Media source settings and Viewability targeting and check result\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-52066\">Ticket TA-52066</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Clone CPM campaign without Media source settings and Viewability targeting")
    public void checkCloneCampaignWithoutMediaSourceAndViewability() {
        log.info("Test is started!");

        authCabAndGo("goodhits/campaigns/?id=" + 2035);
        log.info("Create clone");
        pagesInit.getCabCampaigns().createCloneCompanyWithCopyAllOptions(true, true, clonedCampaignSettingsViewability, false);

        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();

        log.info("Check clone options in EDIT interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        Assert.assertFalse(pagesInit.getCabCampaigns().checkOptionsForCloneInEditInterface(clonedCampaignSettingsViewability),
                "FAIL -> Check clone options in EDIT interface!");
        log.info("Test is finished!");
    }

    @Epic("Privileges")
    @Feature("Ad Name. Show campaigns name")
    @Stories(value = {@Story(value = "Show Ad name in create/edit campaign"), @Story(value = "Show Ad name in campaigns list")})
    @Description("Cases:\n" +
            "<ul>\n" +
                "<li>Check privilege for campaigns show name\n" +
                "<li>@see <a href \"https://jira.mgid.com/browse/TA-52159\">Ticket TA-52159</a></li>\n" +
            "</ul>")
    @Owner(value = "NIO")
    @Privilege(name={"campaigns-edit-show-name", "can_create_campaigns_with_show_name"})
    @Test(description = "Check privilege for campaigns show name")
    public void checkPrivilegeForCampaignsShowName() {
        log.info("Test is started!");
        String campaignId = "1329";
        String clientId = "1000";

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/campaigns-edit-show-name");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_create_campaigns_with_show_name");

        authCabForCheckPrivileges("goodhits/clients-new-campaign/client_id/" + clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaignNameForTeasers(), "Fail - create interface");

        authCabForCheckPrivileges("goodhits/campaigns-edit/id/" + campaignId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isEnabledCampaignNameForTeasers(), "Fail - edit interface");

        authCabForCheckPrivileges("goodhits/ghits/?campaign_id=" + campaignId);
        softAssert.assertFalse(pagesInit.getCabTeasers().isEditableAdvertiserNameField("1335"), "Fail - inline interface");

        authCabForCheckPrivileges("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().chooseCampaignType("search_feed");
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaignNameForTeasers(), "Fail - create search feed campaign");

        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @Epic("Campaigns")
    @Feature("Conversion tracking for RM-dsp campaign")
    @Story("Create DSP campaign")
    @Description("Check that Conversion tracking block is not displayed in create campaign form for ReachMedia DSP campaign")
    @Owner("AIA")
    @Test(dataProvider = "clientsForReachmedia", description = "Check that Conversion tracking block is not displayed in create campaign form for ReachMedia DSP campaign")
    public void checkReachMediaDspCampaignConversionTrackingOnCreateDSPCampaignForm(int clientId) {
        log.info("Test is started!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedConversionBlock(),
                "FAIL -> Conversion block is not displayed by default!");
        pagesInit.getCabCampaigns().chooseCampaignType("dsp");
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedConversionBlock(),
                "FAIL -> Conversion block is displayed for DSP campaign without Reachmedia!");
        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @DataProvider
    public Object[][] clientsForReachmedia() {
        return new Object[][]{
                {2058},
                {2059}
        };
    }

    @Epic("Campaigns")
    @Feature("Conversion tracking for RM-dsp campaign")
    @Story("Edit DSP Reachmedia campaign")
    @Description("Check that Conversion tracking block is displayed for ReachMedia DSP campaign in edit campaign form")
    @Owner("AIA")
    @Test(dataProvider = "campaignsReachmedia", description = "Check that Conversion tracking block is displayed for ReachMedia DSP campaign in edit campaign form")
    public void checkReachMediaDspCampaignConversionTrackingOnEditDSPCampaignForm(int campaignId) {
        log.info("Test is started!");
        authCabAndGo("goodhits/campaigns-edit-dsp/id/" + campaignId);
        Assert.assertTrue(pagesInit.getCabCampaigns().isDisplayedConversionBlock(),
                "FAIL -> Conversion block is not displayed for DSP Reachmedia campaign!");
        log.info("Test is finished!");
    }

    @DataProvider
    public Object[][] campaignsReachmedia() {
        return new Object[][]{
                {2038},
                {2039}
        };
    }

    @Epic("Limits")
    @Feature("Limits. Daily can be equal to overall")
    @Story("Creating campaign form")
    @Description("Check error message when general limit is less then daily for campaign types:" +
            "- product" +
            "- content" +
            "- push")
    @Owner("AIA")
    @Test(dataProvider = "campaignTypes")
    public void checkLimitsValidation(String campaignType) {
        log.info("Test is started");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + 2001);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType(campaignType)
                .setLimitsOption("budget", "51", "50")
                .createCampaignWithLimitsChanges();
        log.info("Check validation");
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("The overall budget should be greater than the daily budget"),
                "FAIL -> Check validation message!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + 2001);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignTypes() {
        return new Object[][]{
                {"product"},
                {"content"},
                {"push"}
        };
    }

    @Epic("Targeting")
    @Feature("Saving of campaign settings")
    @Story("Editing campaign form")
    @Description("Check that error popup don't appeared after 'Publisher category' target adding " +
            "and editing form is closed after saving." +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52417\">Ticket TA-52417</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that error popup don't appeared after 'Publisher category' target adding and editing form is closed after saving.")
    public void checkPublisherCategoryTarget() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns-edit/id/2055");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setTargetSections("platform")
                .selectPublisherCategoryTargeting()
                .saveCampaign();
        Assert.assertFalse(pagesInit.getCabCampaigns().restrictionPopupIsDisplayed(),
                "FAIL -> Restriction popup is displayed!");
        Assert.assertTrue(pagesInit.getCabCampaigns().checkFilterFormIsDisplayed(),
                "FAIL -> Edit campaign form don't closed; campaign list don't opened.");
        log.info("Test is finished");
    }

    @Epic("Adding Domain filter")
    @Feature("Updating field updated_time for campaigns")
    @Story("Filter 'Only' For adding Domain filter")
    @Description("Check that g_partners.updated_time changed after adding site to Domain filter\n" +
            "<ul>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52411\">Ticket TA-52411</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that g_partners.updated_time changed after adding site to Domain filter")
    public void checkUpdatedTimeAfterAddingDomainFilter() {
        log.info("Test is started");
        int campaignId = 2056;
        String updatedTimeStart = operationMySql.getgPartners1().selectCampaignUpdatedTime(campaignId);
        authCabAndGo("goodhits/campaigns-sites-filters/filter/1/pid/" + campaignId);
        pagesInit.getCabCampaignsSitesFilters().addWebsiteAndSaveSettings("test.com");
        String updatedTimeCurrent = operationMySql.getgPartners1().selectCampaignUpdatedTime(campaignId);
        Assert.assertNotEquals(updatedTimeStart, updatedTimeCurrent,
                "FAIL -> updated_time the same!");
        log.info("Test is finished");
    }
}
