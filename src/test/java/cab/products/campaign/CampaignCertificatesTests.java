package cab.products.campaign;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CampaignCertificatesTests extends TestBase {
    String imageNameDracena = "dracaena-cinnabari.jpg";

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateOverAllowedAmount() {
        log.info("Test is started");

        authCabAndGo("goodhits/campaigns-edit/id/1097");
        log.info("upload new certificate");
        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_IMAGES + imageNameDracena);

        log.info("check error about limit of uploaded certificates");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Limit of upload files exceeded. Limit 20"), "Fail - missing error message");
        softAssert.assertEquals(20, pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates");

        softAssert.assertAll();
        log.info("Test is finished");

    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithAllowedExtension() {
        log.info("Test is started");
        String[] filesForUploading = {LINK_TO_RESOURCES_FILES + "certif_doc.doc",
                LINK_TO_RESOURCES_FILES + "certif_pdf.pdf",
                LINK_TO_RESOURCES_FILES + "certif_png.png"};

        authCabAndGo("goodhits/campaigns-edit/id/1098");
        log.info("upload new certificate");

        pagesInit.getCabCampaigns().uploadCertificates(filesForUploading);

        authCabAndGo("goodhits/campaigns-edit/id/1098");
        Assert.assertEquals(3, pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates");

        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithNotValidExtension() {
        log.info("Test is started");

        authCabAndGo("goodhits/campaigns-edit/id/1099");
        log.info("upload with incorrect extension");

        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_exten.txt");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("File 'certif_exten.txt' has a false extension"), "Fail - extencion");
        softAssert.assertEquals(0, pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates first check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Создание/редактирование РК с сертификатом</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51731">Ticket TA-51731</a>
     * <p>NIO</p>
     */
    @Test
    public void checkUploadCertificateWithNotValidSize() {
        log.info("Test is started");

        authCabAndGo("goodhits/campaigns-edit/id/1099");
        log.info("upload with incorrect size");
        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_size.jpeg");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Maximum allowed size for file 'certif_size.jpeg' is '5MB' but '5.63MB' detected"), "Fail - size");
        softAssert.assertEquals(0, pagesInit.getCabCampaigns().getAmountOfCertificates(), "Fail - incorrect amount of certificates second check");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
