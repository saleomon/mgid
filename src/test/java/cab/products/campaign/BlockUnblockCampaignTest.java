package cab.products.campaign;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

public class BlockUnblockCampaignTest extends TestBase {
    @Feature("Compliant")
    @Story("Compliant Adskeeper | Type B")
    @Description("Check unblocking campaigns without compliant option" +
            "<ul>\n" +
            "   <li>Check unblock campaign without option compliant at teaser</li>\n" +
            "   <li>Check unblock campaign without option compliant at teaser and teaser with landing b value</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51932\">Ticket TA-51932</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24526\">Ticket TA-24526</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblocking campaigns without compliant option", dataProvider = "dataUnblockCampaignAndMessageForCompliantOption")
    public void checkUnblockCampaignAndMessageForCompliantOption(String campaignId, String subnet, String message) {
        log.info("Test is started " + subnet);
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Canada"};

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), arrayCountries);
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        log.info("Unblock campaign");
        pagesInit.getCabCampaigns().unBlockCampaign();

        log.info("Check popup message");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkPopupMessage(message), "Unblock by icon");
        pagesInit.getCabCampaigns().clickReadButton();

        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        log.info("Check icon visibility");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVisibilityUnblockIcon(), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataUnblockCampaignAndMessageForCompliantOption() {
        return new Object[][]{
                {"1034", "Mgid", "Campaign you’re trying to unblock is not RAC/Autocontrol/IAP compliant"},
                {"1277", "Adskeeper", "The teasers of the campaign you are trying to unlock are not compliant"},
        };
    }

    /**
     * Запрет на снятие блока с кампаний ГЕО Италия со статусом IAP NOT Compliant
     * <ul>
     *  <li>Создаем кампанию с настройками гео Италия и другие страны без опции IAP NOT Compliant</li>
     *  <li>Проверяем разблокировку кампании</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24526">TA-24526</a>
     *<p>NIO</p>
     */
    @Test
    public void checkUnblockCampaignWithGeoItalyAndOthers() {
        log.info("Test is started");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1034, "Italy", "Canada", "Ukraine");
        authCabAndGo("goodhits/campaigns/?id=1034");
        log.info("Unblock campaign");
        pagesInit.getCabCampaigns().unBlockCampaign();

        log.info("Check popup message");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkPopupMessage("Campaign you’re trying to unblock is not IAP compliant"), "Unblock by icon");
        pagesInit.getCabCampaigns().clickReadButton();

        authCabAndGo("goodhits/campaigns/?id=1034");
        log.info("Check icon visibility");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVisibilityUnblockIcon(), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Compliant")
    @Story("Compliant Adskeeper | Type B")
    @Description("Check unblocking campaigns with compliant option" +
            "<ul>\n" +
            "   <li>Check unblock without compliant geo</li>\n" +
            "   <li>Check unblock with compliant option and teaser with landing b type</li>\n" +
            "   <li>Check unblock with teaser with ad type b</li>\n" +
            "   <li></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51932\">Ticket TA-51932</a></li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-24526\">Ticket TA-24526</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblocking campaigns with compliant option", dataProvider = "dataUnblockCampaignWithDifferentCompliantOption")
    public void checkUnblockCampaignWithDifferentCompliantOption(String campaignId, String subnet, String[]geo) {
        log.info("Test is started " + subnet);
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), geo);
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        log.info("Unblock campaign");
        pagesInit.getCabCampaigns().unBlockCampaign();

        log.info("Check icon visibility");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkVisibilityUnblockIcon(), "Check icon visibility ");

        softAssert.assertAll();
        log.info("Test is finished");
    }
    @DataProvider
    public Object[][] dataUnblockCampaignWithDifferentCompliantOption() {
        return new Object[][]{
                {"1042",      "Mgid", new String[]{"Canada", "Ukraine"}},
                {"1275", "Adskeeper", new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Canada"}},
                {"1276", "Adskeeper", new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Canada"}},
        };
    }
}
