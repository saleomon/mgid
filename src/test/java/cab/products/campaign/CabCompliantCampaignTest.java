package cab.products.campaign;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabCampaigns;
import core.helpers.BaseHelper;
import testBase.TestBase;

import static pages.cab.products.logic.CabCampaigns.CompliantType.*;
import static core.helpers.BaseHelper.randomNumbersInt;

public class CabCompliantCampaignTest extends TestBase {
    @Feature("Compliant")
    @Story("Добавление маркировки Compliant для ряда стран")
    @Description("Check icon for precise compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check icon for precise compliant", dataProvider = "checkWorkCompliantPopupData")
    public void checkWorkCompliantPopup(int campaignId) {
        log.info("Test is started");
        String [] arrayCountries = new String[]{"Spain", "Italy", "Romania", "Czech Republic", "Poland"};
        int randomIndex = randomNumbersInt(arrayCountries.length);

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, "Spain", "Italy", "Romania", "Czech Republic", "Poland");
        operationMySql.getgPartners1().updateOptionCompliant(campaignId, RAC.getCompliantType(), ZPL.getCompliantType(), IAP.getCompliantType(), AUTOCOMPLIANT.getCompliantType(), POLAND_REG_COMPLIANT.getCompliantType());

        log.info("Check all geo icon and selected checkboxes");
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkAllChosenCompliantIconVisibility(), "FAIL - multiple icon");
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(RAC, ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT), "FAIL - selected compliant all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(5), "FAIL - amount of displayed countries");

        log.info("Check part geo icon and selected checkboxes after editing");
        pagesInit.getCabCampaigns().submitCompliantWithCheckboxes(ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkPartChosenCompliantIconVisibility(), "FAIL - part icon");
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT), "FAIL - selected compliant part");

        log.info("Check country icon for one geo");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(campaignId, arrayCountries[randomIndex]);
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCompliantCountryIcon(arrayCountries[randomIndex]), "FAIL - country icon - " + arrayCountries[randomIndex]);

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkDisableCompliantCampaignFlagData() {
        return new Object[][]{
                {1062, 1305},
                {1079, 1306}
        };
    }

    @DataProvider
    public Object[][] checkWorkCompliantPopupData() {
        return new Object[][]{
                {1081},
                {1082}
        };
    }

    @Feature("Compliant")
    @Story("Добавление маркировки Compliant для ряда стран")
    @Description("Check displayed compliant option and inherit them to teaser\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check displayed compliant option and inherit them to teaser", dataProvider = "checkDisableCompliantCampaignFlagData")
    public void checkDefaultCompliantSettingsEditing(int campaignId, int teaserId) {
        log.info("Test is started");

        authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
        pagesInit.getCabCampaigns().selectLocationTargetAndSaveSettings("include", "Spain", "Italy", "Romania", "Czech Republic", "Ukraine", "France", "Thailand", "Brazil", "Poland");

        log.info("Check all geo icon and selected checkboxes");
        authCabAndGo("goodhits/campaigns/?id="/*1063*/ + campaignId);
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(IAP), "FAIL - selected compliant all");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(ARPP, NBTC, CONAR), "FAIL - not selected compliant all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(1), "FAIL - amount of displayed countries");

        pagesInit.getCabCampaigns().submitCompliantWithCheckboxes(RAC, ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT);

        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(RAC, ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT), "FAIL - selected compliant all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(5), "FAIL - amount of displayed countries");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Compliant")
    @Story("Добавление маркировки Compliant для ряда стран")
    @Description("Check default settings of compliant after create campaign\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check default settings of compliant after create campaign", dataProvider = "checkDefaultCompliantSettingsCreatingData")
    public void checkDefaultCompliantSettingsCreating(String campaignType) {
        log.info("Test is started");

        authCabAndGo("goodhits/clients-new-campaign/client_id/1000");
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true).setCampaignType(campaignType)
                .setCampaignName("TestCampaign " + BaseHelper.getRandomWord(6))
                .setCampaignLanguage("1")
                .setUseTargeting(true)
                .setTargetSections("os")
                .createCampaign();

        log.info("Check all geo icon and selected checkboxes");
        authCabAndGo("goodhits/campaigns/?id=" + pagesInit.getCabCampaigns().getCampaignIdFromListInterface());
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(RAC, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT), "FAIL - selected compliant all");
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(ARPP, NBTC, CONAR), "FAIL - not selected compliant all");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkSelectedCompliantGeo(4), "FAIL - amount of displayed countries");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkDefaultCompliantSettingsCreatingData() {
        return new Object[][]{
                {"product"},
                {"push"}
        };
    }

    @Feature("Compliant")
    @Story("Добавление маркировки Compliant для ряда стран")
    @Description("Check filter compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51669\">Ticket TA-51669</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check filter compliant")
    public void checkFilterCompliant() {
        log.info("Test is started");
        CabCampaigns.CompliantType [] arrayCountries = new CabCampaigns.CompliantType[]{RAC, ZPL, IAP, AUTOCOMPLIANT, POLAND_REG_COMPLIANT};
        int randomIndex = randomNumbersInt(arrayCountries.length);

        authCabAndGo("goodhits/campaigns/?client_id=1");
        pagesInit.getCabCampaigns().filterByComplient(arrayCountries[randomIndex].getCompliantType() + "_1");
        log.info("Filter type - " + arrayCountries[randomIndex]);
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCampaignsCompliantTitlesAllMatch(arrayCountries[randomIndex]), "FAIL - include filters");

        authCabAndGo("goodhits/campaigns/?client_id=1");
        pagesInit.getCabCampaigns().filterByComplient(arrayCountries[randomIndex].getCompliantType() + "_0");
        log.info("Filter type - " + arrayCountries[randomIndex]);
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkCampaignsCompliantTitlesAnyMatch(arrayCountries[randomIndex]), "FAIL - exclude filters");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Compliant")
    @Story("Adding Poland to the list of geos need compliant")
    @Description("Check unblock campaign with no flag compliant\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52156\">Ticket TA-52156</a></li>\n" +
            "</ul>")
    @Owner(value="NIO")
    @Test(description = "Check unblock campaign with no flag compliant")
    public void checkUnblockingWithoutCompliantFlag() {
        log.info("Test is started");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1282, "Spain");
        authCabAndGo("goodhits/campaigns?id=1282");
        pagesInit.getCabCampaigns().unBlockCampaign();
        softAssert.assertEquals(pagesInit.getCabCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not Autocontrol compliant", "FAIL - Spain");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1282, "Italy");
        authCabAndGo("goodhits/campaigns?id=1282");
        pagesInit.getCabCampaigns().unBlockCampaign();
        softAssert.assertEquals(pagesInit.getCabCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not IAP compliant", "FAIL - Italy");

        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1282, "Romania");
        authCabAndGo("goodhits/campaigns?id=1282");
        pagesInit.getCabCampaigns().unBlockCampaign();
        softAssert.assertEquals(pagesInit.getCabCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not RAC compliant", "FAIL - Romania");


        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(1282, "Poland");
        authCabAndGo("goodhits/campaigns?id=1282");
        pagesInit.getCabCampaigns().unBlockCampaign();
        softAssert.assertEquals(pagesInit.getCabCampaigns().getComplaintPopupMessage(),
                "Campaign you’re trying to unblock is not Poland Reg compliant", "FAIL - Poland");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
