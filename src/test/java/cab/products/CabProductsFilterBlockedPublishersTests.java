package cab.products;

import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import testBase.TestBase;

import static testData.project.ClientsEntities.*;
import static testData.project.OthersData.LINK_TO_RESOURCES_FILES;

public class CabProductsFilterBlockedPublishersTests extends TestBase {

    /**
     * check import widgets to Filter blocked Publisher
     * RKO
     */
    @Test
    public void importFilterBlockedPublishers() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns-filters/filter/1/pid/" + CAMPAIGN_MGID_PRODUCT);
        Assert.assertTrue(pagesInit.getFilterBlockedPublishers().importPublishersForProductCampaign(LINK_TO_RESOURCES_FILES + "campaign_filter_blocked_only.txt"));
        log.info("Test is finished");
    }

    /**
     * check export sources filters for filter only
     * <p>NIO</p>
     */
    @Test
    public void checkExportFileOnly() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns-filters/filter/1/pid/" + 1013);
        pagesInit.getFilterBlockedPublishers().clickButtonCsvCab();
        Assert.assertTrue(serviceInit.getExportFileTableStructure().loadExportedFileCab(), "load");
        Assert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Source ID", "2", "16", "17"), "check");
        log.info("Test is finished");
    }

    /**
     * check export sources filters for filter except
     * <p>NIO</p>
     */
    @Test
    public void checkExportFileExcept() {
        log.info("Test is started");
        authCabAndGo("goodhits/campaigns-filters/filter/2/pid/" + 1007);
        pagesInit.getFilterBlockedPublishers().clickButtonCsvCab();
        Assert.assertTrue(serviceInit.getExportFileTableStructure().loadExportedFileCab(), "load");
        Assert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForAllRowsInFile("Source ID", "2", "31", "11"), "check");
        log.info("Test is finished");
    }

    /**
     * Проверка фильтра "Только/Кроме"
     * 1. проверяем что фильтр по площадкам выключен(если нет, то выключаем)
     * 2. Проставляем площадку и сохраняем состояние фильтра
     * 3. проверяем что фильтр включился
     * 4. выключаем его
     * RKO
     */
    @Test(dataProvider = "dataCheckWorkFilterWidget")
    public void checkWorkFilterWidget(String comment, String filterId) {
        log.info("Test is started - " + comment);

        log.info("проверяем что фильтр по площадкам выключен(если нет, то выключаем)");
        authCabAndGo("goodhits/campaigns/?id=" + CAMPAIGN_MGID_CONTENT);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isFilterWidgetSwitchOn(), "fail -> publisher filter doesn't off");

        log.info("Проставляем площадку и сохраняем состояние фильтра");
        authCabAndGo("goodhits/campaigns-filters/filter/" + filterId + "/pid/" + CAMPAIGN_MGID_CONTENT);
        softAssert.assertTrue(pagesInit.getFilterBlockedPublishers().switchOnWidgetInFilter(MGID_WIDGET_BANNER_ID), "fail -> widget doesn't set in publisher filter");

        log.info("проверяем что фильтр включился и выключаем его");
        authCabAndGo("goodhits/campaigns/?id=" + CAMPAIGN_MGID_CONTENT);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isFilterWidgetShow(filterId), "fail -> publisher filter doesn't check");

        softAssert.assertAll();
        log.info("Test is finished - " + comment);
    }

    @DataProvider
    public Object[][] dataCheckWorkFilterWidget() {
        return new Object[][]{
                {"check work filterWidget 'ONLY'", "1"},
                {"check work filterWidget 'EXCEPT'", "2"}
        };
    }
}
