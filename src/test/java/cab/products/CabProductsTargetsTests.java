package cab.products;

import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.CLIENT_MGID_ID;

public class CabProductsTargetsTests extends TestBase {

    /**
     * Check clients target creation, editing and deleting
     * AIA
     */
    @Test
    public void checkTargetsClient() {
        log.info("Test is started");
        authCabAndGo("retargeting/targets/client_id/" + CLIENT_MGID_ID);
        log.info("Create new target and get it ID");
        pagesInit.getCabTargets().addTarget();
        pagesInit.getCabTargets().readAndSaveTargetId();

        log.info("Check created target");
        authCabAndGo("retargeting/edit-target/id/" + pagesInit.getCabTargets().getTargetId() + "/client_id/" + CLIENT_MGID_ID);
        softAssert.assertTrue(pagesInit.getCabTargets().checkClientTarget(),
                "FAIL -> check client target after create");
        pagesInit.getCabTargets().resetAllVariables().editTarget();

        log.info("Check edited target");
        authCabAndGo("retargeting/edit-target/id/" + pagesInit.getCabTargets().getTargetId() + "/client_id/" + CLIENT_MGID_ID);
        softAssert.assertTrue(pagesInit.getCabTargets().checkClientTarget(),
                "FAIL -> check client target after editing");

        log.info("Delete target");
        authCabAndGo("retargeting/targets/client_id/" + CLIENT_MGID_ID);
        softAssert.assertTrue(pagesInit.getCabTargets().deleteClientTarget(),
                "FAIL -> delete client target");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Check filling of mandatory fields in target creating
     * AIA
     */
    @Test
    public void checkCreateClientTargetValidations() {
        log.info("Test is started");

        log.info("* Create target with empty 'Name' and 'URL' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets()
                .setTargetName("")
                .setTargetType("url")
                .setTargetPageLink(new String[]{null, ""})
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> empty Target name");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Mandatory field"),
                "FAIL -> empty URL");

        log.info("* Create target with space in 'Name' and 'URL' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetName(" ")
                .setTargetType("url")
                .setTargetPageLink(new String[]{null, " "})
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> space in Target name");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Mandatory field"),
                "FAIL -> space in URL");

        log.info("* Create target with empty page visits and without domain *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("visited")
                .setTargetWebsiteVisitors(new String[]{"", ""})
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Input value must be a number bigger than 0"),
                "FAIL -> empty visits");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Incorrect domain"),
                "FAIL -> empty domain");

        log.info("* Create target with space in page visits and domain *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("visited")
                .setTargetWebsiteVisitors(new String[]{" ", " "})
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Input value must be a number bigger than 0"),
                "FAIL -> space in visits");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Incorrect domain"),
                "FAIL -> space in domain");

        log.info("* Create target with empty 'Target identifier' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("event")
                .setTargetIdentifier("")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("" +
                        "FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> empty Target identifier");

        log.info("* Create target with space in 'Target identifier' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("event")
                .setTargetIdentifier(" ")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> space in Target identifier");

        log.info("* Create target with empty 'Regular expression' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("regexp")
                .setTargetRegExp("")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> empty Regular expression");

        log.info("* Create target with space in 'Regular expression' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("regexp")
                .setTargetRegExp(" ")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> space in Regular expression");

        log.info("* Create target with empty Postback 'Event name' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("postback")
                .setTargetPostbackEventsName("")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Name is mandatory"),
                "FAIL -> empty postback Event name");

        log.info("* Create target with space in Postback 'Event name' *");
        authCabAndGo("retargeting/add-target/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables()
                .setTargetType("postback")
                .setTargetPostbackEventsName(" ")
                .setTargetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Field must contain only alphanumeric symbols."),
                "FAIL -> space in postback Event name");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check filling all mandatory fields in target editing
     * AIA
     */
    @Test
    public void checkEditClientTargetValidations() {
        log.info("Test is started");
        authCabAndGo("retargeting/targets/client_id/" + CLIENT_MGID_ID);
        log.info("Create new target and get it ID");
        int targetId = 1108;

        log.info("* Edit target with empty 'Name' and 'URL' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetName("")
                .setTargetType("url")
                .setTargetPageLink(new String[]{null, ""})
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> empty Target name");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Mandatory field"),
                "FAIL -> empty URL");

        log.info("* Edit target with space in 'Name' and 'URL' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetName(" ")
                .setTargetType("url")
                .setTargetPageLink(new String[]{null, " "})
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> space in Target name");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Mandatory field"),
                "FAIL -> space in URL");

        log.info("* Edit target with empty page visits and without domain *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("visited")
                .setTargetWebsiteVisitors(new String[]{"", ""})
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Input value must be a number bigger than 0"),
                "FAIL -> empty visits");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Incorrect domain"),
                "FAIL -> empty domain");

        log.info("* Edit target with space in page visits and domain *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("visited")
                .setTargetWebsiteVisitors(new String[]{" ", " "})
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Input value must be a number bigger than 0"),
                "FAIL -> space in visits");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Incorrect domain"),
                "FAIL -> space in domain");

        log.info("* Edit target with empty 'Target identifier' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("event")
                .setTargetIdentifier("")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> empty Target identifier");

        log.info("* Edit target with space in 'Target identifier' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("event")
                .setTargetIdentifier(" ")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> space in Target identifier");

        log.info("* Edit target with empty 'Regular expression' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("regexp")
                .setTargetRegExp("")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> empty regular expression");

        log.info("* Edit target with space in 'Regular expression' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("regexp")
                .setTargetRegExp(" ")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Value is required and can't be empty"),
                "FAIL -> space in Regular expression");

        log.info("* Edit target with empty Postback 'Event name' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("postback")
                .setTargetPostbackEventsName("")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Name is mandatory"),
                "FAIL -> empty postback Event name");

        log.info("* Edit target with space in Postback 'Event name' *");
        authCabAndGo("retargeting/edit-target/id/" + targetId + "/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabTargets().resetAllVariables().setSomeFields(true)
                .setTargetType("postback")
                .setTargetPostbackEventsName(" ")
                .editTarget();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(
                "Field must contain only alphanumeric symbols."),
                "FAIL -> space in postback Event name");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}