package cab.products.creativeRequest;

import core.service.customAnnotation.Privilege;
import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabCreativeRequestAdTypesTests extends TestBase {

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24042">https://youtrack.mgid.com/issue/TA-24042</a>
     * Выставление типов тизеров по заявке
     * Проверка работы права goodhits/can_set_order_ad_types
     * RKO
     */
    @Test
    @Privilege(name = "goodhits/can_set_order_ad_types")
    public void checkPrivilege() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_set_order_ad_types");
            authCabForCheckPrivileges("goodhits/creative-requests");
            Assert.assertFalse(pagesInit.getCabCreativeRequests().isDisplayedAdTypeInputs(), "FAIL -> ad types are displayed");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_set_order_ad_types");
        }
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24042">https://youtrack.mgid.com/issue/TA-24042</a>
     * Выставление типов тизеров по заявке
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7590&group_by=cases:section_id&group_order=asc&group_id=1737">Test cases #2, #3, #5</a>
     * RKO
     */
    @Test(dataProvider = "data_validationAdType")
    public void validationAdType(String testCase, String value, String message) {
        log.info("test is started -> " + testCase);
        authCabAndGo("goodhits/creative-requests?creative_id=" + 7);
        pagesInit.getCabCreativeRequests().validationAdType(value);
        pagesInit.getCabCreativeRequests().clickApproveIcon();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> don't correct message");
        log.info("Test is finished -> " + testCase);
    }

    @DataProvider
    public Object[][] data_validationAdType() {
        return new Object[][]{
                {"#2", "fdgd 345345", "Invalid data entry format"},
                {"#3", "0", "Invalid data entry format"},
                {"#5", "1", "Exceeded the required number of teasers"}
        };
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24042">https://youtrack.mgid.com/issue/TA-24042</a>
     * Выставление типов тизеров по заявке
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7591&group_by=cases:section_id&group_order=asc&group_id=1737">Test cases #1, #2, #3</a>
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7592&group_by=cases:section_id&group_order=asc&group_id=1737">Test cases #1, #2</a>
     * RKO
     */
    @Test
    public void createTeaserWithAdType() {
        log.info("Test is started");
        int creativeRequestId = 8;
        operationMySql.getCreativeOrders().setCreativeRequestOnModerationStatusAndDeleteAllTasks(creativeRequestId);
        authCabAndGo("goodhits/creative-requests?creative_id=" + creativeRequestId);

        log.info("set AdType to creative request and approve her");
        pagesInit.getCabCreativeRequests().setPgType("1");
        pagesInit.getCabCreativeRequests().setPg13Type("2");
        pagesInit.getCabCreativeRequests().setRType("3");
        pagesInit.getCabCreativeRequests().setNc17Type("4");
        pagesInit.getCabCreativeRequests().setNsfwType("5");
        pagesInit.getCabCreativeRequests().approveTeaserRequest();

        log.info("go to creative interface and check adType");
        authCabAndGo("goodhits/creative/?id=" + creativeRequestId);
        softAssert.assertEquals(pagesInit.getCabCreative().getCountTeasers("PG"), "1", "FAIL -> type pg doesn't right in creative interface");
        softAssert.assertEquals(pagesInit.getCabCreative().getCountTeasers("PG13"), "2", "FAIL -> type pg13 doesn't right in creative interface");
        softAssert.assertEquals(pagesInit.getCabCreative().getCountTeasers("R"), "3", "FAIL -> type r doesn't right in creative interface");
        softAssert.assertEquals(pagesInit.getCabCreative().getCountTeasers("NC17"), "4", "FAIL -> type nc17 doesn't right in creative interface");
        softAssert.assertEquals(pagesInit.getCabCreative().getCountTeasers("NSFW"), "5", "FAIL -> type nsfw doesn't right in creative interface");

        log.info("get random AdType");
        String customType = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"pg", "pg13", "r", "nc17", "nsfw"});

        log.info("set " + customType + " to task and get her");
        pagesInit.getCabCreative().assumeTask("1", customType);
        String linkToCreateTeaserWithTask = pagesInit.getCabCreative().goToCreateTeaser();

        log.info("check adType: " + customType + " in (get) task");
        softAssert.assertTrue(pagesInit.getCabCreative().checkAdTypeInTakingTasks(customType), "FAIL -> adType in taking Tasks doesn't correct");

        log.info("go to create teaser and check adType in form (check adType in select -> 1 and custom)");
        authCabAndGo(linkToCreateTeaserWithTask);
        softAssert.assertTrue(pagesInit.getCabTeasers().checkTeaserTypeForTaskInterface(customType), "FAIL -> adType in create teaser interface doesn't correct");

        log.info("create teaser with task");
        softAssert.assertNotEquals(pagesInit.getCabTeasers().useTitle(true).setNeedToEditCategory(false).usePriceOfClick(false).setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE).createTeaser(), 0, "FAIL -> teaser doesn't created");

        log.info("go to created teaser and check adType in list interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().checkAdTypeInListInterface(customType), "FAIL -> adType in teaser list interface doesn't correct");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
