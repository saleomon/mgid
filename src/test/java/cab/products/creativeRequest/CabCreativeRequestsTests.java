package cab.products.creativeRequest;

import core.helpers.BaseHelper;
import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.*;
import static core.service.constantTemplates.ConstantsInit.NIO;
import static pages.cab.products.logic.CabTeasers.CompliantType.*;
import static testData.project.AuthUserCabData.*;
import static testData.project.ClientsEntities.CAMPAIGN_MGID_PRODUCT;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabCreativeRequestsTests extends TestBase {
    private static final String limitError = "The %s campaign has reached its limit on the number of teasers. The available limit within the campaign is %s teasers. Clean up teasers that don't get impressions.";

    /**
     * create/edit/check - teaser request
     * <ul>
     * <li>approve - teaser request</li>
     * <li>delete - teaser request</li>
     * <li>check settings teaser request - in creative interface</li>
     * </ul>
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7590&group_by=cases:section_id&group_order=asc&group_id=1737">Test case #4</a>
     * <p>Author AIA</p>
     */
    @Test
    public void createTeasersCreativeRequestTest() {
        log.info("Test is started");
        log.info("* Create campaign *");
        int campaignId = CAMPAIGN_MGID_PRODUCT;
        log.info("* Add teaser creative request *");
        authCabAndGo("goodhits/creative-add/id/" + campaignId);
        pagesInit.getCabCreativeRequests().setTeasersAmount("2")
                .setAmountOfImages(1)
                .createTeaserCreativeRequest();
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().checkTeaserRequest(),
                "FAIL -> Teasers Creative Request failed check in list, after creating!");
        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().checkAttachedImagesInformation(),
                "FAIL -> image information creation request");
        pagesInit.getCabCreativeRequests().deleteImageFromRequest(0);
        log.info("* Edit teaser creative request campaign *");
        pagesInit.getCabCreativeRequests().setAmountOfImages(1).editTeaserRequest();
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().checkTeaserRequest(),
                "FAIL -> Teasers Creative Request failed check in list, after editing!");

        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().checkAttachedImagesInformation(), "FAIL - image information");
        pagesInit.getCabCreativeRequests().closePopupEditRequest();
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().approveTeaserRequest(), "FAIL -> Teaser creative request wasn't approved!");
        authCabAndGo("goodhits/creative?id=" + pagesInit.getCabCreativeRequests().getRequestId());
        softAssert.assertTrue(pagesInit.getCabCreative().checkRequestsForCreative(pagesInit.getCabCreativeRequests().getRequestId(),
                pagesInit.getCabCreativeRequests().getPriority(),
                pagesInit.getCabCreativeRequests().getTeasersAmount()), "FAIL -> Creative check failed!");
        authCabAndGo("goodhits/creative-requests?showClosedOrders=all&creative_id=" + pagesInit.getCabCreativeRequests().getRequestId());
        softAssert.assertTrue(pagesInit.getCabCreativeRequests().deleteTeaserRequest(), "FAIL -> Teaser creative request wasn't deleted");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Creating teaser from creative request
     * <p>Author AIA</p>
     */
    @Test
    public void createTeaserFromRequest() {
        log.info("Test is started");
        int creativeOrderId = 6;
        operationMySql.getCreativeOrders().setCreativeRequestOnModerationStatusAndDeleteAllTasks(creativeOrderId);
        log.info("* Approve teaser creative request *");
        authCabAndGo("goodhits/creative-requests?creative_id=" + creativeOrderId);
        pagesInit.getCabCreativeRequests().setPgType("1");
        pagesInit.getCabCreativeRequests().approveTeaserRequest();
        log.info("* Set teasers amount and create it *");
        authCabAndGo("goodhits/creative/?dateInterval=all&id=" + creativeOrderId);
        pagesInit.getCabCreative().assumeTask("1");
        sleep(500);
        authCabAndGo(pagesInit.getCabCreative().goToCreateTeaser());
        log.info("* Fill all fields and create teaser *");
        Assert.assertNotEquals(pagesInit.getCabTeasers().usePriceOfClick(false)
                .useTitle(true)
                .isNeedToEditUrl(false)
                .isSetImageFromRequest(true)
                .setNeedToEditCategory(false)
                .createTeaser(), 0, "Fail -> teaser doesn't created");
        softAssert.assertFalse(pagesInit.getCabTeasers().getTeaserUrl().contains("amp;"),"Teaser url - teasers creating interface");
        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedTeaserImageBlock(), "FAIL - image isn`t displayed");
        softAssert.assertFalse(pagesInit.getCabTeasers().checkTeasersUrl("amp;"), "Teaser url - teasers list");
        pagesInit.getCabCampaigns().openCompliantPopup();
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(RAC, ZPL, IAP, AUTOCONTROL, POLAND_REG_COMPLIANT), "FAIL - selected compliant required");
        softAssert.assertTrue(pagesInit.getCabTeasers().checkSelectedCompliantGeo(5), "FAIL - selected compliant required");
        Assert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(pagesInit.getCabTeasers().getTeaserId()).contains("Bustiere"));
        Assert.assertTrue(operationMySql.getTeasersOffers().selectAddedType(pagesInit.getCabTeasers().getTeaserId()).contains("manual"));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Внедрение нового фильтра по куратору в интерфейс `creative`
     * <p>expand all spans</p>
     * <ul>
     * <li>choose custom curator</li>
     * <li>click filter</li>
     * <li>check tasks by curator</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23874">Ticket TA-23874"</a>
     * <p>Author RKO</p>
     */
    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator =  helpersInit.getBaseHelper().getRandomFromArray(new String[] {indiaProductUser, apacPublisherUser, cisProductUser, europeProductUser, advertisingAgencyUser});
        authCabAndGo("goodhits/creative-requests");
        log.info("set chosen curator and search creative tasks");
        Assert.assertTrue(pagesInit.getCabCreativeRequests().checkWorkCuratorFilter(curator), "FAIL -> tasks don't search by curator " + curator);
        log.info("Test is finished");
    }

    /**
     * Вывести опцию добавления продукта в базу в интерфейс заявок
     * <ul>
     * <li>проверка работы екшена добавления продукта</li>
     * <li>добавление продукта</li>
     * <li>проверка что продукт был добавлен</li>
     * </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23935">Ticket TA-23935</a>
     * <p>Author NIO</p>
     */
    @Test
    public void checkAddingProduct() {
        log.info("Test is started");
        String nameOfProduct = "SomeProductTest" +  BaseHelper.getRandomWord(2);
        String creativeId = "1";
        authCabAndGo("goodhits/creative-requests?creative_id=1");
        log.info("set chosen curator and search creative tasks");
        pagesInit.getCabCreativeRequests().openFormAddProduct();
        log.info("create product");
        pagesInit.getLandingOffers().setProductName(nameOfProduct)
                .setPageLink("http://uchetka.dt00.net")
                .createProduct();
        authCabAndGo("goodhits/ghits-teaser-landing-offers");
        log.info("find product");
        pagesInit.getLandingOffers().findProduct(nameOfProduct);
        log.info("check displaying product");
        softAssert.assertTrue(operationMySql.getCreativeOrders().selectOfferName(creativeId).contains(nameOfProduct));
        Assert.assertTrue(pagesInit.getLandingOffers().checkVisibilitySearchResults());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Редактирование заявки
     * <ul>
     *     Удаление изображения из заявки
     * </ul>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51784">TA-51784</a>
     * <p>NIO</p>
     */
    @Test
    public void checkDeleteImageForRequest() {
        log.info("Test is started");
        log.info("* Edit request *");
        int requestId = 1003;
        log.info("* Add teaser creative request *");
        authCabAndGo("goodhits/creative-requests?creative_id=" + requestId);
        pagesInit.getCabCreativeRequests().setAmountOfImages(1).setImagesCommentText("Some special text");

        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        pagesInit.getCabCreativeRequests().editTeaserPrice();
        pagesInit.getCabCreativeRequests().deleteImageFromRequest(1);
        pagesInit.getCabCreativeRequests().editImageInformationAndSaveSettings();

        pagesInit.getCabCreativeRequests().setAmountOfImages(1);
        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        Assert.assertTrue(pagesInit.getCabCreativeRequests().checkAttachedImagesInformation(), "FAIL - image information");

        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Редактирование заявки
     * <ul>
     *     Проверка лимита на добавление изображений к заявке
     * </ul>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51784">TA-51784</a>
     * <p>NIO</p>
     */
    @Test
    public void checkImageLimitForRequest() {
        log.info("Test is started");
        log.info("* Edit request *");
        int requestId = 1004;

        authCabAndGo("goodhits/creative-requests?creative_id=" + requestId);
        pagesInit.getCabCreativeRequests().setAmountOfImages(1);

        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        pagesInit.getCabCreativeRequests().loadImagesForRequest();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("You can not add images more than ads in requests"));

        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Редактирование заявки
     * <ul>
     *     Проверка сохранения значений поля в текстовом формате
     * </ul>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51784">TA-51784</a>
     * <p>NIO</p>
     */
    @Test
    public void checkValidationImageInformationCommentField() {
        log.info("Test is started");
        log.info("* Edit request *");
        String[] texts = new String[] {"Nice site,  I think I'll take it. <script>alert('Executing JS')</script>", "'-prompt()-'", "<blink>Hello there</blink>"};
        int requestId = 1005;

        authCabAndGo("goodhits/creative-requests?creative_id=" + requestId);
        pagesInit.getCabCreativeRequests().setAmountOfImages(1).setImagesCommentText(texts[0]);

        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        pagesInit.getCabCreativeRequests().editTeaserPrice();

        pagesInit.getCabCreativeRequests().editImageInformationAndSaveSettings();
        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        log.info("check saved value - " + texts[0]);
        softAssert.assertEquals(pagesInit.getCabCreativeRequests().checkValueOfImageCommentField(), texts[0], "FAIL - first check");

        pagesInit.getCabCreativeRequests().setImagesCommentText(texts[1]);
        pagesInit.getCabCreativeRequests().editImageInformationAndSaveSettings();
        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        log.info("check saved value - " + texts[1]);
        softAssert.assertEquals(pagesInit.getCabCreativeRequests().checkValueOfImageCommentField(), texts[1], "FAIL - second check");

        pagesInit.getCabCreativeRequests().setImagesCommentText(texts[2]);
        pagesInit.getCabCreativeRequests().editImageInformationAndSaveSettings();
        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        log.info("check saved value - " + texts[2]);
        softAssert.assertEquals(pagesInit.getCabCreativeRequests().checkValueOfImageCommentField(), texts[2], "FAIL - third check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Изображения прикрепленные к заявке | Редактирование заявки
     * <ul>
     *     Проверка валидации поля линка изображения
     * </ul>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51784">TA-51784</a>
     * <p>NIO</p>
     */
    @Test(dataProvider = "validationImageLinkField")
    public void checkValidationImageLinkField(String value, String message) {
        log.info("Test is started. Value - " + value);
        int requestId = 1006;

        authCabAndGo("goodhits/creative-requests?creative_id=" + requestId);

        pagesInit.getCabCreativeRequests().openPopupEditRequest();
        pagesInit.getCabCreativeRequests().fillImageLinkField(value);

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(message));

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] validationImageLinkField(){
        return new Object[][]{
                {"Robert'); DROP TABLE Students;--", "'Robert&#039;); DROP TABLE Students;--' is not a valid uri"},
                {"Nice site,  I think I'll take it. <script>alert('Executing JS')</script>", "'Nice site,  I think I&#039;ll take it. &lt;script&gt;alert(&#039;Executing JS&#039;)&lt;/script&gt;' is not a valid uri"},
                {"'-prompt()-'", "'&#039;-prompt()-&#039;' is not a valid uri"},
                {"<i><b>Bold</i></b>", "'&lt;i&gt;&lt;b&gt;Bold&lt;/i&gt;&lt;/b&gt;' is not a valid uri"},
                {"://www.mysite.com", "'://www.mysite.com' is not a valid uri"},
                {"http:/www.mysite.com", "'http:/www.mysite.com' is not a valid uri"}
        };
    }

    @Story("Зображення тизерів заявки")
    @Description("Перевірка можливості мануального додання зображення до тизеру з присутніми зображеннями із заявки\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51806\">Ticket TA-51806</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Перевірка можливості мануального додання зображення до тизеру з присутніми зображеннями із заявки")
    public void checkPossibilityLoadImageManuallyInSpiteOfImagesFromRequest() {
        log.info("Test is started");
        int creativeOrderId = 1010;

        authCabAndGo("goodhits/creative/?dateInterval=all&id=" + creativeOrderId);
        authCabAndGo(pagesInit.getCabCreative().goToCreateTeaser());
        log.info("* Fill all fields and create teaser *");
        Assert.assertNotEquals(pagesInit.getCabTeasers().usePriceOfClick(false)
                .useTitle(true)
                .isNeedToEditUrl(true)
                .setOwnershipForImage(CabTeasers.OwnershipOfMedia.OWN_IMAGE)
                .isSetImageFromRequest(false)
                .setNeedToEditCategory(false)
                .createTeaser(), 0,"Fail -> teaser doesn't created");

        authCabAndGo("goodhits/ghits/?id=" + pagesInit.getCabTeasers().getTeaserId());
        softAssert.assertTrue(pagesInit.getCabTeasers().isDisplayedTeaserImageBlock(), "FAIL - image isn`t displayed");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Зображення тизерів заявки")
    @Description("Перевірка відкриття попапу з назначеними зображеннями\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51805\">Ticket TA-51805</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Перевірка відкриття попапу з назначеними зображеннями")
    public void checkPopupWithAssignedImages() {
        log.info("Test is started");
        int creativeOrderId = 1011;

        authCabAndGo("goodhits/creative/?dateInterval=all&id=" + creativeOrderId);
        pagesInit.getCabCreativeRequests().openPopupWithAssignedImages();
        Assert.assertEquals(pagesInit.getCabCreativeRequests().checkAmountOfDisplayedImages(), 3, "Different amount of images");

        log.info("Test is finished");
    }

    @Privilege(name="can_see_assigned_tasks")
    @Story("Зображення тизерів заявки")
    @Description("Перевірка права на відображення іконки попапу з назначеними зображення\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51806\">Ticket TA-51806</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Перевірка права на відображення іконки попапу з назначеними зображення")
    public void checkPrivilegeIconAssignedImages() {
        log.info("Test is started");
        int creativeOrderId = 1011;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_assigned_tasks");

        authCabForCheckPrivileges("goodhits/creative/?dateInterval=all&id=" + creativeOrderId);
        Assert.assertFalse(pagesInit.getCabCreativeRequests().isDisplayedIconAssignedImages(), "icon visible oops");
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Case:\n" +
            "<ul>\n" +
            "   <li>Check copy teaser from creative order to campaign with different 'Advertiser name'</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check copy teaser from creative order to campaign with different 'Advertiser name'")
    public void checkCopyTeaserFromCreativeOrderToCampaignWithDifferentAdvertiserName() {
        log.info("Test is started");
        int donorCampaignId = 2018;
        int recipientCampaignId = 2019;
        authCabAndGo("goodhits/creative-add/id/" + donorCampaignId);
        pagesInit.getCabCreativeRequests().prepareTeaserCreativeRequestWithCopyingToCampaign(recipientCampaignId);
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Advertiser name is not set for the campaign"),
                "FAIL -> Error message teaser creative request when copying to campaign with different 'Advertiser name'!");
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("Case:\n" +
            "<ul>\n" +
            "   <li>Check copy teaser from creative order to campaign without 'Advertiser name'</>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check copy teaser from creative order to campaign without 'Advertiser name'")
    public void checkCopyTeaserFromCreativeOrderToCampaignWithoutAdvertiserName() {
        log.info("Test is started");
        int donorCampaignId = 2016;
        int recipientCampaignId = 2022;
        authCabAndGo("goodhits/creative-add/id/" + donorCampaignId);
        pagesInit.getCabCreativeRequests().prepareTeaserCreativeRequestWithCopyingToCampaign(recipientCampaignId);
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Advertiser name in campaign's copy settings does not match the donor campaign"),
                "FAIL -> Error message teaser creative request when copying to campaign without 'Advertiser name'!");
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATIVE_ADD_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 teaser which one dropped and second is blocked</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check create teaser creative request over limit")
    public void createTeasersCreativeRequestOverLimitForCampaign() {
        log.info("Test is started");
        log.info("* Create request *");
        int campaignId = 1433;
        int limit = 2;
        log.info("* Add teaser creative request *");
        authCabAndGo("goodhits/creative-add/id/" + campaignId);
        pagesInit.getCabCreativeRequests().setTeasersAmount("2")
                .setAmountOfImages(1)
                .createTeaserCreativeRequest();


        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(String.format(limitError, campaignId, limit)), "FAIL -> checkErrorMessageCabForCustomField");
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATIVE_ADD_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 regular teaser</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check create teaser request with exceeded limit")
    public void checkCreateTeaserRequestWithExceededLimit() {
        log.info("Test is started");
        log.info("* Create request *");
        int campaignId = 1434;
        int limit = 2;
        log.info("* Add teaser creative request *");
        authCabAndGo("goodhits/creative-add/id/" + campaignId);
        pagesInit.getCabCreativeRequests().setTeasersAmount("2")
                .setAmountOfImages(1)
                .createTeaserCreativeRequest();

        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(String.format(limitError, campaignId, limit)), "FAIL -> checkErrorMessageCabForCustomField");
        log.info("Test is finished");
    }

    @Epic(CAMPAIGNS)
    @Feature(LIMITS_OF_TEASERS)
    @Story(TEASER_CREATIVE_ADD_CAB_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>limit=2, campaign has 3 regular teaser</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52415\">Ticket TA-52415</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check copy teaser from teaserr request form with exceeded limit")
    public void checkCopyTeaserFromRequestWithExceededLimit() {
        log.info("Test is started");
        log.info("* Create request *");
        int campaignId = 1434;
        int campaignIdRecipient = 1435;
        int limit = 2;

        log.info("* Add teaser creative request *");
        authCabAndGo("goodhits/creative-add/id/" + campaignId);
        pagesInit.getCabCreativeRequests().prepareTeaserCreativeRequestWithCopyingToCampaign(campaignIdRecipient);

        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(String.format(limitError, campaignId, limit)), "FAIL -> checkErrorMessageCabForCustomField");
        log.info("Test is finished");
    }
}