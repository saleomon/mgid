package cab.products.teasersQuarantine;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.products.logic.CabTeasers;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.Arrays;
import java.util.List;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.devtools.CloudinaryMock.ResponseWithImages.HAPPY_WOMAN;
import static pages.cab.products.logic.CabTeasers.ApproveOption.APPLY_WITH_APPROVE;
import static pages.cab.products.variables.CabTeaserVariables.languageIconTooltip;
import static testData.project.CliCommandsList.Cron.REJECT_TEASERS_BLOCKED_CLIENTS;
import static testData.project.CliCommandsList.Cron.UPDATE_TEASER_TO_MODERATION;
import static testData.project.EndPoints.cabLink;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabProductTeasersModerationTests extends TestBase {

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24034">https://youtrack.mgid.com/issue/TA-24034</a>
     * CTA в cab. Создание тизера.
     * Check advert and call to action validation inline actions
     * NIO
     */
    @Test
    public void checkValidationAdvertAndCallToActionInlineListInterface() {
        log.info("Test is started");
        int teaserId = 27;
        authCabAndGo("goodhits/on-moderation/id/" + teaserId);

        log.info("edit advert");
        pagesInit.getCabTeasersModeration().setDescription(" ");
        pagesInit.getCabTeasersModeration().editDescriptionInline();
        log.info("Check error message advert validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Advert text is less than 1 characters long"), "FAIL - Advert Text Validation");

        log.info("edit call to action");
        pagesInit.getCabTeasersModeration().setCallToAction(" ");
        pagesInit.getCabTeasersModeration().editCallToActionInline();
        log.info("Check error message call to action validation");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Call to Action can not be empty"), "FAIL - Call to action Text Validation");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check approve without link offer
     * <ul>
     *     <li>Click approve teaser</li>
     *     <li>Click button 'Continue without linking' at popup</li>
     *     <li>Approve teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24520">TA-24520</a>
     */
    @Test
    public void approveTeaserWithoutLinkingOffer() {
        log.info("Test is started");
        operationMySql.getgHits1().updateTeaserModerationStatus(1006, 1);
        authCabAndGo("goodhits/on-moderation/?id=1006");
        pagesInit.getCabTeasersModeration().approveTeaserWithoutLinkingOffer();
        Assert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityApproveIcon());
        log.info("Test is finished");
    }

    /**
     * Check approve with link offer
     * <ul>
     *     <li>Click approve teaser</li>
     *     <li>Check displayed popup for linking options</li>
     *     <li>Link teaser with offer at screenshot history interface</li>
     *     <li>Approve teaser</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24520">TA-24520</a>
     */
    @Test
    public void approveTeaserWithLinkingOffer() {
        log.info("Test is started");
        int teaserId = 1010;
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        log.info("Approve teaser and check visibility popup for link option");
        pagesInit.getCabTeasersModeration().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPopupLinkOffer(), "popup should be visible");

        log.info("Link offer with teaser");
        authCabAndGo("goodhits/ghits-screenshots-history/id/" + teaserId);
        pagesInit.getScreenshotHistory().linkOfferWithTeaser("Something");

        log.info("Approve teaser and check visibility popup");
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().approveTeaser();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPopupLinkOffer(), "popup should not be visible");
        helpersInit.getMessageHelper().checkCustomMessagesCab("Offers successfully added");
        pagesInit.getCabTeasersModeration().clickContinueWithApproveRejectButton();
        authCabAndGo("goodhits/ghits/?id=" + teaserId);
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityApproveIcon(), "icon should not be visible");
        log.info("DEBUG LOG Something - " + operationMySql.getTeasersOffers().selectOfferName(teaserId));
        log.info("DEBUG LOG manual - " + operationMySql.getTeasersOffers().selectAddedType(teaserId));
        softAssert.assertTrue(operationMySql.getTeasersOffers().selectOfferName(teaserId).contains("Something"), "Something");
        softAssert.assertTrue(operationMySql.getTeasersOffers().selectAddedType(teaserId).contains("manual"), "manual");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check show popup linking offer during approve teaser by mass action
     * <ul>
     *     <li>Approve teaser be mass</li>
     *     <li>Check show popup linking offer</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24468">TA-24468</a>
     */
    @Test
    public void checkShowLinkingPopupDuringApproveTeaserMass() {
        log.info("Test is started");
        String teaserId = "1034";
        authCabAndGo("goodhits/on-moderation?id=" + teaserId + "&campaign_id=1003");
        log.info("Approve teaser and check visibility popup for link option");
        pagesInit.getCabTeasersModeration().approveTeaserMassAction();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPopupLinkOffer(), "popup should be visible");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * DFP | Blacklisted domains flag. Ручное проставление Флага для Тизера
     * @see <a href="https://youtrack.mgid.com/issue/TA-22286">TA-22286</a>
     * <p>NIO</p>
     */
    @Test
    public void checkSetRemoveGoogleAddManager() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=1051");
        pagesInit.getCabTeasersModeration().setRemoveGoogleAddManager(true);
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkGoogleAddManagerIcon(true), "Fail - should be true");
        pagesInit.getCabTeasersModeration().setRemoveGoogleAddManager(false);
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkGoogleAddManagerIcon(false), "Fail - should be false");
        log.info("Test is finished");
    }

    /**
     * Не выводить попап привязки продукта при отлонении тизера на модерации
     * <ul>
     *     <li>Click reject teaser</li>
     *     <li>Check visibility product linking popup</li>
     *     <li>Check that teaser rejected</li>
     *     <li>The same steps for mass action</li>
     * </ul>
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-38449">TA-38449</a>
     */
    @Test
    public void checkDisplayingLinkingPopupOfferDuringRejectTeaser() {
        log.info("Test is started");
        operationMySql.getgHits1().updateTeaserModerationStatus(1080, 1);
        authCabAndGo("goodhits/on-moderation/?id=1080");

        log.info("Check rejection without popup linking offer - icon");
        pagesInit.getCabTeasersModeration().rejectTeaser();
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedPopupLinkOffer(), "FAIL - Popup is displayed icon action");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityRejectIcon(), "FAIL - Reject icon is displayed icon action");

        operationMySql.getgHits1().updateTeaserModerationStatus(1081, 1);
        authCabAndGo("goodhits/on-moderation/?id=1081&campaign_id=1003");

        log.info("Check rejection without popup linking offer - mass");
        pagesInit.getCabTeasersModeration().rejectTeaserMassAction(Subnets.SubnetType.SCENARIO_MGID.getTypeValue());
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedPopupLinkOffer(), "FAIL - Popup is displayed mass action");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityRejectIcon(), "FAIL - Reject icon is displayed mass action");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * 	Удаление продукта | Кейс 1. Отображение удаленного продукта в попапе при апруве тизера
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-44492">TA-44492</a>
     */
    @Test
    public void checkDisplayedProductAtLinkingPopupAfterDeleting() {
        log.info("Test is started");
        log.info("Delete product from screenshot");
        authCabAndGo("goodhits/on-moderation?id=1082");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().deleteLinkedProduct("DisplayingDeleted");
        log.info("Check product presence");
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().checkProductPresence("Moderation") , "FAIL - product is present");

        authCabAndGo("goodhits/on-moderation?id=1082");
        pagesInit.getCabTeasersModeration().approveTeaser();
        softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedProductAtPopupLinkingOffer("Moderation") , "FAIL - product is present at popup");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * 	Выводить алерт в случае другого языка тизера чем РК
     * <p>NIO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-28480">TA-28480</a>
     */
    @Test
    public void checkDisplayingLanguageMismatchIcon() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=1090");
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkDisplayingLanguageMismatchIcon(), "FAIL - icon");
        Assert.assertEquals(pagesInit.getCabTeasersModeration().checkToolTipMismatchIcon(), languageIconTooltip, "FAIL - tooltip");
        log.info("Test is finished");
    }

    /**
     * 	Auto-approve | вывод рейтинга в интерфейс карантина. 4 правила, score для отображения от 300.
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51743">TA-51743</a>
     */
    @Test
    public void checkVisibilityAutoApproveLabel() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/show_moderation_flow");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_auto_approve");

            authCabForCheckPrivileges("goodhits/on-moderation?id=1097");
            softAssert.assertTrue(pagesInit.getCabTeasersModeration().isVisibleAutoAprove(), "FAIL - score 300, should be visible");

            authCabForCheckPrivileges("goodhits/on-moderation?id=1098");
            softAssert.assertFalse(pagesInit.getCabTeasersModeration().isVisibleAutoAprove(), "FAIL - score 299, should not be visible");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/show_moderation_flow");
        }
    }

    /**
     * 	Auto-approve | вывод рейтинга в интерфейс карантина. Право на лейбл.
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51743">TA-51743</a>
     */
    @Privilege(name = "can_see_auto_approve")
    @Test
    public void checkRightAutoApproveLabel() {
        try {
            log.info("Test is started");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/show_moderation_flow");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_see_auto_approve");

            log.info("check right can_see_auto_approve");
            authCabForCheckPrivileges("goodhits/on-moderation?id=1097");
            Assert.assertFalse(pagesInit.getCabTeasersModeration().isVisibleAutoAprove(), "FAIL - label should not be visible");

            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/show_moderation_flow");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/show_moderation_flow");
        }
    }

    /**
     * 	Auto-approve | вывод рейтинга в интерфейс карантина. Фильтр.
     * <p>NIO</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51743">TA-51743</a>
     */
    @Test
    public void checkFilterAutoApproveLabel() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation");
        pagesInit.getCabTeasersModeration().filterByAutoApprove();

        Assert.assertEquals(pagesInit.getCabTeasersModeration().getAmountOfAutoApproveLabels(),
                pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers());

        log.info("Test is finished");
    }

    /**
     * <p>Story| Провайдер фида для кампаний Native-to-Search | Кампании</p>
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51741">Ticket TA-51741</a>
     * <p>NIO</p>
     */
    @Test
    public void checkChangingFeedsProviderForAllTeasers() {
        log.info("Test is started");
        String provider = "eat-zucchini.net";

        authCabAndGo("goodhits/on-moderation?campaign_id=1118");

        softAssert.assertEquals(
                pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers(),
                pagesInit.getCabTeasersModeration().getAmountOfTeasersWithFeedsProvider("Bing"), "Fail - start condition");


        pagesInit.getCabTeasersModeration().editFeedsProviderInline(provider);

        softAssert.assertEquals(
                pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers(),
                pagesInit.getCabTeasersModeration().getAmountOfTeasersWithFeedsProvider(provider), "Fail - final condition");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Проверка тизера с неразрывными пробелами заголовке и описании\n" +
            "     <ul>\n" +
            "      <li>в интерфейсе карантина</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test
    public void checkNbspInTeasers() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?campaign_id=2007");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkErrorIconForTeaserTitle(),
                "FAIL -> There is not icon for teaser title with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTitleOfErrorIconForTeaserTitle(
                "The title contains special characters! Go to edit to see and verify the spelling of the title."),
                "FAIL -> Error in icon's text title for teaser title with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkErrorIconForTeaserDescription(),
                "FAIL -> There is not icon for teaser description with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTitleOfErrorIconForTeaserDescription(
                "The description contains special characters! Go to edit to see and verify the spelling of the description."),
                "FAIL -> Error in icon's text title for teaser description with &nbsp!");

                softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature("Image Extensions")
    @Story("Inline Quarantine Interface")
    @Description("Inline editing image with gif extention\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51863\">Ticket TA-51863</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Inline editing image with gif extention")
    public void checkInlineEditImageWithAnimation() {
        log.info("Test is started");
        int teaserId = 1184;
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().openPopupEditImageInline();

        serviceInit.getCloudinaryMock().prepareCloudinaryResponse(HAPPY_WOMAN, "*ghits-cloudinary-load*");
        pagesInit.getCabTeasers().loadImageCloudinaryInline("space_journey.gif", CabTeasers.OwnershipOfMedia.OWN_IMAGE);
        serviceInit.getCloudinaryMock().countDownLatchAwait(1);

        pagesInit.getCabTeasersModeration().savePopupImageSettings();

        String optionVideoLinksValues = operationMySql.getgHits1().getVideoLinksValue(teaserId);
        log.info("optionVideoLinksValues - " + optionVideoLinksValues);

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoValue create");
        softAssert.assertTrue(optionVideoLinksValues.contains("1:1"), "FAIL - 1:1 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("3:2"), "3:2 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("16:9"), "16:9 create");
        softAssert.assertTrue(optionVideoLinksValues.contains("/imgh/video/upload/ar_1:1,c_fill,w_680/local.s3.eu-central-1.amazonaws.com/happy_woman.mp4"), "FAIL - link create");

        softAssert.assertEquals(operationMySql.getgHits1().getVideoValue(teaserId), 1, "FAIL - getVideoLinksValue create");
        softAssert.assertEquals(operationMySql.getgHits1().getAnimationValue(teaserId), 0, "FAIL - getAnimationValue create");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TITLES)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52418\">Ticket TA-52418</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Edit title by inline with symbol '")
    public void checkEditTeaserTitleInline() {
        log.info("Test is started");
        int teaserId = 1508;
        String teaserTitle = "Teaser's title edited";

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().editInLineTitle(teaserTitle);

        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkTeasersTitle(teaserTitle),
                "FAIL -> Title");
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TITLES)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52418\">Ticket TA-52418</a></li>\n" +
            "</ul>")
    @Owner(NIO)
    @Test(description = "Edit title by inline with symbol ' at zion")
    public void checkEditTeaserTitleInlineZion() {
        log.info("Test is started");
        int teaserId = 1509;
        String teaserTitle = "Teaser's title edited zion";

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);

        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        pagesInit.getCabTeasersModeration().editInLineTitleInZion(teaserTitle);

        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkTitleInZion(teaserTitle),
                "FAIL -> Title");
        log.info("Test is finished");
    }

    @Feature("Removing teasers from moderation when client blocking")
    @Story("Removing teasers from moderation when client blocking | Back teaser to moderation")
    @Description("Send teaser to moderation due clients unblocking\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51950\">Ticket TA-51950</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Send teaser to moderation due clients unblocking")
    public void checkSendTeaserToModerationAfterClientsUnblock() {
        log.info("Test is started");
        Integer amountOfTeaserForFirstCheck = 0;
        Integer amountOfTeaserForSecondCheck = 2;

        authCabAndGo("goodhits/on-moderation?client_id=1006");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers(), amountOfTeaserForFirstCheck, "FAIL - teasers displayed but shouldn`t");

        authCabAndGo("crm/edit-client/id/1006");
        pagesInit.getCrmClients().setBlockedClient(false);
        pagesInit.getCrmClients().saveSettings();

        serviceInit.getDockerCli().runAndStopCron(UPDATE_TEASER_TO_MODERATION);

        authCabAndGo("goodhits/on-moderation?client_id=1006&autoModerationDone=1");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers(), amountOfTeaserForSecondCheck, "FAIL - teasers isn`t displayed but shouldn");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check 'Advertiser name' for teaser on Moderation</>" +
            "   <li>Check approve teaser with 'Advertiser name' on Moderation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check 'Advertiser name' on moderation and approve teaser with it")
    public void checkTeaserAdvertiserNameOnModeration() {
        log.info("Test is started");
        String teaserId = "2011";
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        Assert.assertEquals(pagesInit.getCabTeasersModeration().getTeasersAdvertiserName(teaserId), "Advertiser name",
                "FAIL -> Advertiser name!");
        pagesInit.getCabTeasersModeration().approveTeaserWithoutLinkingOffer();
        Assert.assertFalse(pagesInit.getCabTeasersModeration().checkVisibilityApproveIcon(),
                "FAIL -> Approve teaser with advertiser name!");
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check temporary 'Advertiser name' for teaser on Moderation</>" +
            "   <li>Change temporary 'Advertiser name' on Moderation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check teaser with temporary 'Advertiser name' on moderation and changing it")
    public void checkTeaserTemporaryAdvertiserNameOnModeration() {
        log.info("Test is started");
        String teaserId = "2012";
        String newAdvertiserName = "New advertiser name";
        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getTeasersAdvertiserNameTemp(teaserId), "Temp advertiser name",
                "FAIL -> Advertiser name!");
        pagesInit.getCabTeasersModeration().changeAdvertiserName(teaserId, newAdvertiserName);
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getTeasersAdvertiserNameTemp(teaserId), newAdvertiserName,
                "FAIL -> Changed Advertiser name!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Advertiser Name in teasers for Push campaigns")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check teaser without 'Advertiser name' on Moderation</>" +
            "   <li>Set 'Advertiser name' on Moderation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52075\">Ticket TA-52075</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check teaser without 'Advertiser name' on Moderation and change it")
    public void checkTeaserWithoutAdvertiserNameOnModeration() {
        log.info("Test is started");

        String teaserId = "2013";
        String secondTeaserId = "2014";
        String newAdvertiserName = "New advertiser name";
        authCabAndGo("goodhits/on-moderation?campaign_id=2018");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getTeasersAdvertiserNameTemp(teaserId), "",
                "FAIL -> Advertiser name!");
        pagesInit.getCabTeasersModeration().changeAdvertiserName(teaserId, newAdvertiserName);
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getTeasersAdvertiserNameTemp(secondTeaserId), newAdvertiserName,
                "FAIL -> Changed Advertiser name!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Removing teasers from moderation when client blocking")
    @Story("Removing teasers from moderation when client blocking | Teaser rejection")
    @Description("Check next cases\n" +
            "   <li>moderation interface - check displaying teaser on moderation after cron</li>\n" +
            "   <li>moderation interface - check displaying teaser after start moderation after cron</li>\n" +
            "   <li>list interface - check teasers for rejection with right reason</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-51949\">Ticket TA-51949</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Removing teasers from moderation when client blocking")
    public void checkHidingTeaserOnModerationDueBlockingClient() {
        log.info("Test is started");
        int teaserIdDefault = 1206;
        int teaserIdInProgressOfModeration = 1207;
        int campaignId = 1243;
        authCabAndGo("goodhits/on-moderation?campaign_id=" + campaignId);
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers().intValue(), 2, "FAIL - start condition");

        authCabAndGo("goodhits/on-moderation?id=" + teaserIdInProgressOfModeration);
        operationMySql.getClients().updateClientsGodmodeBlock("1", "1010");
        serviceInit.getDockerCli().runAndStopCron(REJECT_TEASERS_BLOCKED_CLIENTS, "-vvv");

        authCabAndGo("goodhits/on-moderation?campaign_id=" + campaignId);
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers().intValue(), 0, "FAIL - after cron condition");

        authCabAndGo("goodhits/ghits/?id=" + teaserIdDefault);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedRejectIcon(), "FAIL - list interface icon teaserIdDefault");
        softAssert.assertEquals(pagesInit.getCabTeasers().getRejectReason(), "Account is blocked", "FAIL - list interface reason teaserIdDefault");

        authCabAndGo("goodhits/ghits/?id=" + teaserIdInProgressOfModeration);
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedRejectIcon(), "FAIL - list interface icon teaserIdInProgressOfModeration");
        softAssert.assertEquals(pagesInit.getCabTeasers().getRejectReason(), "Account is blocked", "FAIL - list interface reason teaserIdInProgressOfModeration");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Approve teaser")
    @Story("Prohibition of approval of type B teasers for mgid")
    @Description("Check approve teaser with type b at mgid subnet mass\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52074\">Ticket TA-52074</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check approve teaser with type b at mgid subnet")
    public void checkApproveTeaserWithTypeBMgid() {
        log.info("Test is started");

        log.info("approve teaser");
        authCabAndGo("goodhits/on-moderation?campaign_id=1244&id=1204");

        pagesInit.getCabTeasersModeration().approveTeaserMassAction();
        pagesInit.getCabTeasersModeration().closePopupLinkingOffer();

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("Forbidden to approve teasers with AD_type = B in the MGID subnet"), "FAIL - first check");
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP")
    @Story("Copyright certificate LP | V 1.1 | Viewing results")
    @Description("Check copyleaks matching result\n" +
            "     <ul>\n" +
            "      <li>Check main screenshot info</li>\n" +
            "      <li>Check secondary screenshot info</li>\n" +
            "      <li>Check percentage</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52020\">Ticket TA-52020</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check copyleaks matching result")
    public void checkCopyLeaksMatchingResult() {
        log.info("Test is started");
        List<String> mainScreenshot = Arrays.asList(cabLink +"goodhits/ghits-screenshots-history/id/1208/activeScreenshot/75",
                                             "https://imghosts.com/snapshot/snapshot/501584c344e23b406640be0f232f8a2d.jpeg");
        List<String> secondaryScreenshot = Arrays.asList(cabLink + "goodhits/ghits/id/1168,1169,1171,1172,1173",
                                                 "https://imghosts.com/snapshot/snapshot/501584c344e23b406640be0f232f8a2d.jpeg",
                                                 cabLink + "goodhits/ghits/id/1174,1175",
                                                 "https://imghosts.com/snapshot/snapshot/501584c344e23b406640be0f232f8a2d.jpeg");
        String percentage = "69%";

        authCabAndGo("goodhits/on-moderation?campaign_id=1245&id=1208");

        pagesInit.getCabTeasersModeration().openCopyLeaksScreenshotPopup();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkMainScreenshot(mainScreenshot), "FAIL - checkMainScreenshot");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkSecondaryScreenshot(secondaryScreenshot), "FAIL - checkSecondaryScreenshot");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkAllPercentage(percentage), "FAIL - checkAllPercentage");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP")
    @Story("Copyright certificate LP | V 1.1 | Viewing results")
    @Description("Check copyleaks matching result\n" +
            "     <ul>\n" +
            "      <li>Check main screenshot info</li>\n" +
            "      <li>Check secondary screenshot info</li>\n" +
            "      <li>Check percentage</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52020\">Ticket TA-52020</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check copyleaks matching result lp tab")
    public void checkCopyLeaksMatchingResultLp() {
        log.info("Test is started");
        List<String> mainScreenshot = Arrays.asList(cabLink + "goodhits/ghits-screenshots-history/id/1211/activeScreenshot/78",
                                             "https://imghosts.com/snapshot/snapshot/501584c344e23b406640be0f232f8a2d.jpeg");
        String percentage = "failed";

        authCabAndGo("goodhits/on-moderation?campaign_id=1248&id=1211");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();

        pagesInit.getCabTeasersModeration().openCopyLeaksScreenshotPopup();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkMainScreenshot(mainScreenshot), "FAIL - checkMainScreenshot");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkAllPercentage(percentage), "FAIL - checkAllPercentage");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Copyright certificate LP")
    @Story("Copyright certificate LP | V 1.1 | Viewing results")
    @Description("Check displayed type of icons\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52020\">Ticket TA-52020</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check displayed plagiarism icon")
    public void checkCopyLeaksTypeIcons() {
        log.info("Test is started");

        authCabAndGo("goodhits/on-moderation?campaign_id=1245&id=1208");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPlagiarismIcon("blue"), "FAIL - checkMainScreenshot blue");

        authCabAndGo("goodhits/on-moderation?campaign_id=1246&id=1209");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPlagiarismIcon("grey"), "FAIL - checkMainScreenshot grey");

        authCabAndGo("goodhits/on-moderation?campaign_id=1247&id=1210");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPlagiarismIcon("yellow"), "FAIL - checkMainScreenshot yellow");

        authCabAndGo("goodhits/on-moderation?campaign_id=1248&id=1211");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedPlagiarismIcon("red"), "FAIL - checkMainScreenshot red");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MP4 and MOV formats for motion ads")
    @Story("MP4 and MOV formats for motion ads")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check by animation/video moderation</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52146\">Ticket TA-52146</a></li>\n" +
            "   <li><p>NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check by animation/video moderation")
    public void checkFilterByVideo() {
        log.info("Test is started");

        log.info("filter by certificate");
        authCabAndGo("goodhits/on-moderation?client_id=1000");
        pagesInit.getCabTeasers().filterByVideo("video");

        List<String> fromInterface = pagesInit.getCabTeasersModeration().getListOfDislpayedTeaserIds();
        List<String> fromDb = operationMySql.getgHits1().getClientVideoTeasersIds(true, 1000);

        fromInterface.forEach(el-> log.info("fromInterface - " + el));
        fromDb.forEach(el-> log.info("fromDb - " + el));
        softAssert.assertTrue(fromInterface.containsAll(fromDb), "Fail - contains");
        softAssert.assertTrue(fromInterface.size() == fromDb.size(), "Fail - size");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(IMAGE_EXTENSIONS)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("Check load gif through manual image form\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52226\">Ticket TA-52226</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test(description = "Check load gif through manual image form")
    public void checkLoadGifIntoManualImagesForm() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=1359");

        log.info("Open form");
        pagesInit.getCabTeasersModeration().openPopupEditImageInlineManualLoad();

        pagesInit.getCabTeasersModeration().setImages("1_1.gif").loadImageManualListInterface();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is impossible to add animated images"), "FAIL - message wrong");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(APPROVE)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>teaser has offer</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52351\">Ticket TA-52351</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check reject teaser by mass")
    public void checkRejectTeaserByMass() {
        log.info("Test is started");

        authCabAndGo("goodhits/on-moderation?campaign_id=1418");
        pagesInit.getCabTeasersModeration().rejectTeaserMassAction(Subnets.SubnetType.SCENARIO_MGID.getTypeValue());

        authCabAndGo("goodhits/ghits?campaign_id=1418");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(), "Rejected", "Fail - getTeaserStatus");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedRejectIcon(), "Fail - isDisplayedApproveIcon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(APPROVE)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("\n" +
            "     <ul>\n" +
            "      <li>teaser has offer</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52351\">Ticket TA-52351</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check approve teaser by mass")
    public void checkApproveTeaserByMass() {
        log.info("Test is started");

        authCabAndGo("goodhits/on-moderation?campaign_id=1417");
        pagesInit.getCabTeasersModeration().approveTeaserMassAction();
        pagesInit.getCabTeasersModeration().clickApproveOption(APPLY_WITH_APPROVE);

        authCabAndGo("goodhits/ghits?campaign_id=1417");
        softAssert.assertEquals(pagesInit.getCabTeasers().getTeaserStatus(), "Approved", "Fail - getTeaserStatus");
        softAssert.assertFalse(pagesInit.getCabTeasers().isDisplayedApproveIcon(), "Fail - isDisplayedApproveIcon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TITLE_AND_DESCRIPTION)
    @Story(TEASER_QUARANTINE_INLINE_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52409\">Ticket TA-52409</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Edit title and description by inline with special characters  ™, ®, ° on moderation")
    public void checkEditTeaserTitleAndDescriptionInlineOnModeration() {
        log.info("Test is started");
        int teaserId = 5001;
        String teaserTitle = "™ New and Sign ®";
        String teaserDescription = "Sign ® and ° Celsius";

        authCabAndGo("goodhits/on-moderation?id=" + teaserId);
        pagesInit.getCabTeasersModeration().editInLineTitle(teaserTitle);
        pagesInit.getCabTeasersModeration().setDescription(teaserDescription);
        pagesInit.getCabTeasersModeration().editDescriptionInline();

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTeasersTitle(teaserTitle), "There is incorrect teaser title!");
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTeasersDescription(teaserDescription), "There is incorrect teaser description!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
