package cab.products.teasersQuarantine;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static libs.dockerClient.base.DockerKafka.Topics.POPULATE_TEASER_TAGS;
import static testData.project.CliCommandsList.Consumer.TEASERS_UPDATE_TAGS;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;


public class CabTeasersQuarantineTagsTests extends TestBase {

    @BeforeMethod
    public void initializeVariables(){
        operationMySql.getgHitsTags().deleteAllTags(43);
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Owner(NIO)
    @Test
    public void searchTagsInTeaserQuarantine() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43);

        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkWorkSearchTags("lp"), "FAIL -> don't search lp tags");

        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkWorkSearchTags("image"), "FAIL -> don't search image tags");

        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkWorkSearchTags("title"), "FAIL -> don't search title tags");

        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkWorkSearchTags("description"), "FAIL -> don't search description tags");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudForTeaserOnQuarantine() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43);

        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("lp");

        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("image");

        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("title");

        log.info("check");

        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", false), "FAIL -> lp tags don't correct show");

        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("image", false), "FAIL -> image tags don't correct show");

        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", false), "FAIL -> title tags don't correct show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic   (TEASERS)
    @Feature(TAGS)
    @Story  (TEASER_QUARANTINE_ZION_INTERFACE)
    @Description(
            "     <ul>\n" +
                    "      <li>Change image tags for teaser 1 g_hits_1.g_hits_image_hashes_id = 8, g_hits_tags = 8,9,10</li>\n" +
                    "      <li>Similar image for teaser 2 to teaser 1 g_hits_1.g_hits_image_hashes_id = 8 </li>\n" +
                    "      <li>Similar image for teaser 3 to teaser 1 g_hits_1.g_hits_image_hashes_id = 8, g_hits_tags = 9 </li>\n" +
                    "      <li>Similar image for teaser 4 to teaser 1 g_hits_1.g_hits_image_hashes_id = 9, g_hits_tags = 8,9,10,11 </li>\n" +
                    "      <li>Similar image for teaser 5 to teaser 1 g_hits_1.g_hits_image_hashes_id = 9, g_hits_tags = 5,6,7 </li>\n" +
                    "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52357\">Ticket TA-52357</a></li>\n" +
                    "     </ul>")
    @Owner  (NIO)
    @Test(description = "Check inheritance image tags for related and similar images")
    public void checkClearTagsByInheritanceForTeasersWithSimilarAndRelatedImages() {
        try {
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            log.info("Test is started");
            authCabAndGo("goodhits/on-moderation?id=" + 1434);

            pagesInit.getCabTeasersModeration().clickImageCheckButton();
            pagesInit.getCabTeasersModeration().clearAllTags();

            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            log.info("check tag inheritance");
            authCabAndGo("goodhits/on-moderation?id=" + 1435);
            pagesInit.getCabTeasersModeration().clickImageCheckButton();
            softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedActiveTags(), "FAIL -> 1435");

            authCabAndGo("goodhits/on-moderation?id=" + 1436);
            pagesInit.getCabTeasersModeration().clickImageCheckButton();
            softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedActiveTags(), "FAIL -> 1436");

            authCabAndGo("goodhits/on-moderation?id=" + 1437);
            pagesInit.getCabTeasersModeration().clickImageCheckButton();
            softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedActiveTags(), "FAIL -> 1437");

            authCabAndGo("goodhits/on-moderation?id=" + 1438);
            pagesInit.getCabTeasersModeration().clickImageCheckButton();
            softAssert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedActiveTags(), "FAIL -> 1438");

            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudChangeLpOnMassAction() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43 + "&campaign_id=" + 1);
        try {

            log.info("выбираем через массовые действия тип LP_CHECK type");
            pagesInit.getCabTeasersModeration().chooseLpTypeTagOnMassAction();

            log.info("проставляем рандомные теги для LP_CHECK");
            pagesInit.getCabTeasersModeration().chooseRandomTags("lp", "massAction");

            log.info("сохраняем теги");
            pagesInit.getCabTeasersModeration().saveTagsForm();
            serviceInit.getDockerCli().runConsumer(TEASERS_UPDATE_TAGS, "-vvv");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(POPULATE_TEASER_TAGS);

            log.info("проверяем что проставленные теги отображаются");
            pagesInit.getCabTeasersModeration().clickLpCheckButton();
            Assert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", false), "FAIL -> LP теги не отображаются");
            log.info("Test is finished");

        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudChangeTitleInline() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43);

        log.info("choose title tags");
        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("title");

        log.info("change inline title and wait show popup where click 'Save' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editInLineTitle();
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", true),"FAIL -> изменение title + нажимаем SAVE: теги не отображаются");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudChangeTitleInZionForm() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43);

        log.info("choose title tags");
        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("title");

        log.info("change zion title and wait show popup where click 'Save' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editInLineTitleInZion();
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("title", true),"FAIL -> изменение title in zion + нажимаем SAVE: теги не отображаются");

        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudChangeLandingTypeInline() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?autoModerationDone=1&id=" + 43);

        log.info("choose lp tags");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("lp");

        log.info("change inline LandingType and wait show popup where click 'Save' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editLandingInline();
        pagesInit.getCabTeasersModeration().clickButtonSaveTags();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", false),"FAIL -> изменение LandingType + нажимаем SAVE: теги не отображаются");

        log.info("change inline LandingType and wait show popup where click 'Delete' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editLandingInline();
        pagesInit.getCabTeasersModeration().clickButtonDeleteTags();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", true), "FAIL -> изменение LandingType + нажимаем DELETE: теги отображаются");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Owner(NIO)
    @Test
    public void tagsCloudChangeLandingTypeInMassAction() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=" + 43 + "&autoModerationDone=1&campaign_id=" + 1);

        log.info("choose lp tags");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().chooseRandomTags("lp");

        log.info("change mass LandingType and wait show popup where click 'Save' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editLandingInMassAction();
        pagesInit.getCabTeasersModeration().clickButtonSaveTags();
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", false),"FAIL -> изменение mass LandingType + нажимаем SAVE: теги не отображаются");

        log.info("change mass LandingType and wait show popup where click 'Delete' tags state; After check state tags");
        pagesInit.getCabTeasersModeration().editLandingInMassAction();
        pagesInit.getCabTeasersModeration().clickButtonDeleteTags();
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().checkTagsByType("lp", true), "FAIL -> изменение mass LandingType + нажимаем DELETE: теги отображаются");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("Check privileges title tags\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52018\">Ticket TA-52018</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Privilege(name="can_mass_edit_title_tags")
    @Test (description = "Check privileges title tag")
    public void checkPrivilegeTitleTagsOnMassAction() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_mass_edit_title_tags");
        authCabForCheckPrivileges("goodhits/on-moderation?autoModerationDone=1&id=" + 42 + "&campaign_id=" + 1);

        log.info("choose change ad type mass action");
        pagesInit.getCabTeasersModeration().chooseLpTypeTagOnMassAction();

        log.info("check displayed tags");
        Assert.assertFalse(pagesInit.getCabTeasersModeration().isDisplayedTitleSection());
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Description("Check search title tags by mass actions\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52018\">Ticket TA-52018</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check search title tags by mass actions")
    public void checkSearchTitleTagsOnMassAction() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=" + 43 + "&autoModerationDone=1&campaign_id=" + 1);

        log.info("choose change ad type mass action");
        pagesInit.getCabTeasersModeration().chooseLpTypeTagOnMassAction();

        log.info("search tags");
        pagesInit.getCabTeasersModeration().searchTagMassAction("ant");
        log.info("check displayed tags");
        Assert.assertTrue(pagesInit.getCabTeasersModeration().checkDisplayedTags("ant"));
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check unmark last tag at lp tab\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52246\">Ticket TA-52246</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check unmark last tag at lp tab")
    public void checkUnmarkLastTagLpTab() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        pagesInit.getCabTeasersModeration().deleteAllActiveTags();

        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickLpCheckButton();

        Assert.assertFalse(pagesInit.getCabTeasersModeration().isPresentActiveTags());
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check unmark last tag at image tab\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52246\">Ticket TA-52246</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check unmark last tag at image tab")
    public void checkUnmarkLastTagImageTab() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickImageCheckButton();
        pagesInit.getCabTeasersModeration().deleteAllActiveTags();

        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickImageCheckButton();

        Assert.assertFalse(pagesInit.getCabTeasersModeration().isPresentActiveTags());
        log.info("Test is finished");
    }

    @Epic(TEASERS)
    @Feature(TAGS)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check unmark last tag at title tab\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52246\">Ticket TA-52246</a></li>\n" +
            "     </ul>")
    @Owner(NIO)
    @Test (description = "Check unmark last tag at title tab")
    public void checkUnmarkLastTagTitleTab() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickTitleCheckButton();
        pagesInit.getCabTeasersModeration().deleteAllActiveTags();

        authCabAndGo("goodhits/on-moderation?id=" + 1380);
        pagesInit.getCabTeasersModeration().clickTitleCheckButton();

        Assert.assertFalse(pagesInit.getCabTeasersModeration().isPresentActiveTags());
        log.info("Test is finished");
    }
}