package cab.products.teasersQuarantine;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class TeasersCertificatesTests extends TestBase {
    String imageNameDracena = "dracaena-cinnabari.jpg";

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Stories({@Story(TEASER_QUARANTINE_LIST_INTERFACE), @Story(TEASER_LANDING_OFFERS)})
    @Privilege(name="ghits-upload-certificate-file")
    @Test
    public void checkRightUploadCertificateButton() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/add-offer", "goodhits/remove-offer", "goodhits/update-offer");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/ghits-upload-certificate-file");

        log.info("Check at moderation");
        authCabForCheckPrivileges("goodhits/on-moderation?id=1094");
        pagesInit.getCabTeasersModeration().openEditProductForm();
        softAssert.assertFalse(pagesInit.getLandingOffers().checkVisibilityUploadButton(), "Moderation interface");

        log.info("Check at landing offers");
        authCabForCheckPrivileges("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=EditOfferCertLimit");
        pagesInit.getLandingOffers().openEditProductForm();
        softAssert.assertFalse(pagesInit.getLandingOffers().checkVisibilityUploadButton(), "Landing offers interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Test
    public void checkUploadCertificateOverAllowedAmount() {
        log.info("Test is started");
        authCabAndGo("goodhits/on-moderation?id=1093");
        pagesInit.getCabTeasersModeration().openEditProductForm();

        log.info("upload certificates");
        pagesInit.getLandingOffers().uploadCertificates(LINK_TO_RESOURCES_IMAGES + imageNameDracena);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Limit of upload files exceeded. Limit 20"), "Check message after edit");
        log.info("check amount of certificates");
        softAssert.assertEquals(20, pagesInit.getLandingOffers().getAmountOfCertificates(), "something went wrong with edited product");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Test
    public void checkUploadCertificateWithAllowedExtension() {
        log.info("Test is started");
        String[] filesForUploading = {LINK_TO_RESOURCES_FILES + "certif_doc.doc",
                LINK_TO_RESOURCES_FILES + "certif_pdf.pdf",
                LINK_TO_RESOURCES_FILES + "certif_png.png"};
        authCabAndGo("goodhits/on-moderation?id=1095");
        pagesInit.getCabTeasersModeration().openEditProductForm();
        log.info("upload new certificate");

        pagesInit.getLandingOffers().uploadCertificates(filesForUploading);

        authCabAndGo("goodhits/on-moderation?id=1095");
        pagesInit.getCabTeasersModeration().openEditProductForm();

        Assert.assertEquals(3, pagesInit.getLandingOffers().getAmountOfCertificates(), "Fail - incorrect amount of certificates");

        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Test
    public void checkUploadCertificateWithNotValidExtension() {
        log.info("Test is started");

        authCabAndGo("goodhits/on-moderation?id=1094");
        pagesInit.getCabTeasersModeration().openEditProductForm();
        log.info("upload with incorrect extension");

        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_exten.txt");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("File 'certif_exten.txt' has a false extension"), "Fail - extencion");
        softAssert.assertEquals(0, pagesInit.getLandingOffers().getAmountOfCertificates(), "Fail - incorrect amount of certificates first check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Test
    public void checkUploadCertificateWithNotValidSize() {
        log.info("Test is started");

        authCabAndGo("goodhits/on-moderation?id=1094");
        pagesInit.getCabTeasersModeration().openEditProductForm();
        log.info("upload with incorrect size");
        pagesInit.getCabCampaigns().uploadCertificates( LINK_TO_RESOURCES_FILES + "certif_size.jpeg");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Maximum allowed size for file 'certif_size.jpeg' is '5MB' but '5.63MB' detected"), "Fail - size");
        softAssert.assertEquals(0, pagesInit.getLandingOffers().getAmountOfCertificates(), "Fail - incorrect amount of certificates second check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Test
    public void checkDownloadedCertificateOnModeration() {
        log.info("Test is started");

        log.info("download certificate");
        authCabAndGo("goodhits/on-moderation?id=1096");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        sleep(2000);
        pagesInit.getCabTeasersModeration().downloadCertificate();
        serviceInit.getDownloadedFiles().getDownloadedFile();

        log.info("check downloaded certificate");
        Assert.assertEquals(serviceInit.getDownloadedFiles().getFileName(), "certificate_ExtraCertificate2.zip");

        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_LIST_CAB_INTERFACE)
    @Test
    public void checkDownloadedCertificateListInterface() {
        log.info("Test is started");

        log.info("download certificate");
        authCabAndGo("goodhits/ghits?id=1096");
        sleep(2000);
        pagesInit.getCabTeasers().downloadCertificate();
        serviceInit.getDownloadedFiles().getDownloadedFile();

        log.info("check downloaded certificate");
        Assert.assertEquals(serviceInit.getDownloadedFiles().getFileName(), "1096.zip");

        log.info("Test is finished");
    }

    @Epic(OFFERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_LANDING_OFFERS)
    @Test
    public void checkDownloadedCertificateLandingInterface() {
        log.info("Test is started");

        log.info("download certificate");
        authCabAndGo("goodhits/ghits-teaser-landing-offers?offerNameTplForSearch=ExtraCertificate2");
        sleep(2000);
        pagesInit.getLandingOffers().downloadCertificate();
        serviceInit.getDownloadedFiles().getDownloadedFile();

        log.info("check downloaded certificate");
        Assert.assertEquals(serviceInit.getDownloadedFiles().getFileName(), "ExtraCertificate2.zip");

        log.info("Test is finished");
    }


    @Epic(FILTERS)
    @Feature(CERTIFICATES)
    @Story(TEASER_QUARANTINE_LIST_INTERFACE)
    @Test
    public void checkFilterByCertificate() {
        log.info("Test is started");

        log.info("filter by certificate");
        authCabAndGo("goodhits/on-moderation");
        pagesInit.getCabTeasersModeration().filterByCertificate();
        Assert.assertEquals(pagesInit.getCabTeasersModeration().getAmountOfIconsDownloadCertificates(),
        pagesInit.getCabTeasersModeration().getAmountDisplayedTeasers());

        log.info("Test is finished");
    }


    @Epic(OFFERS)
    @Feature(ADVERT_NAME)
    @Story(TEASER_QUARANTINE_ZION_INTERFACE)
    @Description("Check displaying icon 'advertiser name' due created offer from advertiser name\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52023\">Ticket TA-52023</a></li>\n" +
            "   <li><p>Author AIA</p></li>\n" +
            "</ul>")
    @Test(description = "Check displaying icon 'advertiser name' due created offer from advertiser name")
    public void checkDisplayingCreatedFromAdvertNameIcon() {
        log.info("Test is started");
        log.info("Link product with teaser");
        authCabAndGo("goodhits/on-moderation?id=1096");
        pagesInit.getCabTeasersModeration().clickLpCheckButton();
        softAssert.assertTrue(pagesInit.getCabTeasersModeration().isDisplayedShowNameIcon(), "FAIL - isDisplayedShowNameIcon");
        softAssert.assertEquals(pagesInit.getCabTeasersModeration().getTipShowNameIcon(), "Offer added from advertiser name ", "FAIL - getTipShowNameIcon");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
