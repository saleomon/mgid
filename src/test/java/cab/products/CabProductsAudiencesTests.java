package cab.products;

import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.CLIENT_MGID_RUB_ID;

public class CabProductsAudiencesTests extends TestBase {

    /**
     * Check clients audience creation, editing and deleting
     */
    @Test
    public void checkAudiencesClient() {
        log.info("Test is started");

        log.info("Create new audience and get it ID");
        authCabAndGo("retargeting/audiences/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabAudiences().addAudience();
        pagesInit.getCabAudiences().readAndSaveAudienceId();

        log.info("проверяем созданую аудиторию и редактируем её");
        authCabAndGo("retargeting/edit-audience/audience_id/" + pagesInit.getCabAudiences().getAudienceId() + "/client_id/" + CLIENT_MGID_RUB_ID);
        softAssert.assertTrue(pagesInit.getCabAudiences().checkClientAudience(), "check client audience after create -> FAIL");
        pagesInit.getCabAudiences().resetAllVariables().editClientAudience();

        log.info("проверяем отредактированную аудиторию");
        authCabAndGo("retargeting/edit-audience/audience_id/" + pagesInit.getCabAudiences().getAudienceId() + "/client_id/" + CLIENT_MGID_RUB_ID);
        softAssert.assertTrue(pagesInit.getCabAudiences().checkClientAudience(), "check client audience after edit -> FAIL");

        log.info("удаляем аудиторию");
        authCabAndGo("retargeting/audiences/client_id/" + CLIENT_MGID_RUB_ID);

        softAssert.assertTrue(pagesInit.getCabAudiences().deleteClientAudience(), "FAIL -> delete audience icon");
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> delete audience message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check filling of mandatory fields in audience creating
     * AIA
     */
    @Test(dependsOnMethods = "checkEditClientAudienceValidations")
    public void checkCreateClientAudienceValidations() {
        log.info("Test is started");
        log.info("Do preconditions: create one target");

        log.info("* Create audience with empty 'Name' *");
        authCabAndGo("retargeting/add-audience/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabAudiences()
                .setAudienceName("")
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Fill all fields with appropriate valid values"),
                "FAIL -> message for empty Audience name");
        softAssert.assertTrue(helpersInit.getBaseHelper().checkErrorSelectionField("name"),
                "FAIL -> input selection for empty Audience name");

        log.info("* Create audience with space in 'Name' *");
        authCabAndGo("retargeting/add-audience/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabAudiences().resetAllVariables()
                .setAudienceName(" ")
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Fill all fields with appropriate valid values"),
                "FAIL -> message for space in Audience name");
        softAssert.assertTrue(helpersInit.getBaseHelper().checkErrorSelectionField("name"),
                "FAIL -> input selection for space in Audience name");

        log.info("* Create audience with duplicate Target *");
        authCabAndGo("retargeting/add-audience/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabAudiences().resetAllVariables()
                .setSomeFields(true)
                .setAudienceName("Audience with duplicate targets")
                .addNewCondition()
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Duplicate targets"),
                "FAIL -> Duplicate targets");

        log.info("* Create audience without targets *");
        log.info("* Delete all targets *");
            operationMySql.getSensorsTargets().makeDeletedAllClientsTargets(CLIENT_MGID_RUB_ID);
        authCabAndGo("retargeting/add-audience/client_id/" + CLIENT_MGID_RUB_ID);
        pagesInit.getCabAudiences().resetAllVariables()
                .setSomeFields(true)
                .setAudienceName("Audience without target")
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Audience must contain at least one target"),
                "FAIL -> audience without target");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check filling of mandatory fields in audience editing
     * AIA
     */
     @Test
    public void checkEditClientAudienceValidations() {
        log.info("Test is started");
        int audienceIdWithTarget = 1101;
        int audienceIdWithoutTarget = 1102;
        int clientId = 1131;

        log.info("* Edit audience with empty 'Name' *");
        authCabAndGo("retargeting/edit-audience/audience_id/" + audienceIdWithTarget + "/client_id/" + clientId);
        pagesInit.getCabAudiences().resetAllVariables().setSomeFields(true)
                .setAudienceName("")
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Fill all fields with appropriate valid values"),
                "FAIL -> message for empty Audience name");
        softAssert.assertTrue(helpersInit.getBaseHelper().checkErrorSelectionField("name"),
                "FAIL -> input selection for empty Audience name");

        log.info("* Edit audience with space in 'Name' *");
        authCabAndGo("retargeting/edit-audience/audience_id/" + audienceIdWithTarget + "/client_id/" + clientId);
        pagesInit.getCabAudiences().resetAllVariables().setSomeFields(true)
                .setAudienceName(" ")
                .setAudienceSettings();
         serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkEditClientAudienceValidations");
         softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Fill all fields with appropriate valid values"),
                "FAIL -> message for space in Audience name");
        softAssert.assertTrue(helpersInit.getBaseHelper().checkErrorSelectionField("name"),
                "FAIL -> input selection for space in Audience name");

        log.info("* Edit audience with duplicate Target *");
        authCabAndGo("retargeting/edit-audience/audience_id/" + audienceIdWithTarget + "/client_id/" + clientId);
        pagesInit.getCabAudiences().resetAllVariables().setSomeFields(true)
                .setAudienceName("Audience duplicate targets")
                .addNewCondition()
                .setAudienceSettings();
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkEditClientAudienceValidations");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Duplicate targets"),
                "FAIL -> Duplicate targets");

        log.info("* Edit audience without targets *");
        authCabAndGo("retargeting/edit-audience/audience_id/" + audienceIdWithoutTarget + "/client_id/1130");
        pagesInit.getCabAudiences().resetAllVariables().setSomeFields(true)
                .setAudienceName("Audience without target")
                .setAudienceSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Audience must contain at least one target"),
                "FAIL -> audience without target");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
