package cab.video.videoContent;

import libs.mongoDb.video.ProvidedVideo;
import org.testng.annotations.Test;
import testBase.TestBase;

import static libs.mongoDb.variables.VideoVariables.*;

public class VideoContentTest extends TestBase {

    /**
     * Проверка работы фильтра для списка видео контента по части заголовка
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24050">Ticket TA-24050</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkVideoContentFilter() {
        log.info("Test is started");
        ProvidedVideo.insertVideos(video1, video2, video3);
        authCabAndGo("video-feed/list-content");
        softAssert.assertEquals(pagesInit.getVideoContent().countVideos(), 3,
                "FAIL -> Video content list!");
        pagesInit.getVideoContent().filterVideoContent("5");
        softAssert.assertEquals(pagesInit.getVideoContent().countVideos(), 2,
                "FAIL -> Search video with digit 5!");
        pagesInit.getVideoContent().filterVideoContent("bravest");
        softAssert.assertEquals(pagesInit.getVideoContent().countVideos(), 1,
                "FAIL -> Search video by word 'bravest'!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
