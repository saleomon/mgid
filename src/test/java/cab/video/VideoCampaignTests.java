package cab.video;

import io.qameta.allure.*;
import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import pages.cab.products.logic.CabCampaigns;
import testBase.TestBase;

import static pages.cab.products.logic.CabCampaigns.VideoCampaignSubTypes.*;
import static pages.cab.products.variables.CampaignsVariables.*;
import static testData.project.ClientsEntities.CLIENT_MGID_ID;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class VideoCampaignTests extends TestBase {

    /**
     * Создание/Редактирование, Проверка, Удаление кампании
     * type: video
     * subType: directAds
     * RKO
     */
    @Test
    public void createEditVideoDirectAdsVideoCampaign() {
        log.info("Test is started");
        int clientId = 1;
        CabCampaigns.VideoCampaignSubTypes subType = DIRECT_ADS;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setLimitsOption("budget", "100", "100")
                .setBlockTeaserAfterCreation(true)
                .setUseTargeting(true)
                .setUtmType("mgclida=sub_id&sub_id={click_id}")
                .setTargetSections("location", "browser", "os", "browserLanguage")
                .setEventUrls(eventUrls, true)
                .saveVideoCampaign(subType);
        log.info("go to custom campaign and get it ID");

        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVisibilitySybTypeIcon("Direct ads"), "Campaign subtype");
        log.info("Go to edit campaign interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        log.info("check campaign after creating");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVideoCampaignDirectAdsFields(), "FAIL -> check campaign after creating");
        log.info("Go to edit campaign");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setTargetSections("platform", "interests", "browser", "socdem")
                .setLimitsOption("impressions", "500", "500")
                .setBlockedByManager(true)
                .setEventUrls(eventUrls, false)
                .saveVideoCampaign(subType);
        log.info("Go to edit campaign interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        log.info("check campaign after editing");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVideoCampaignDirectAdsFields(), "FAIL -> check campaign after editing");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-22549">https://youtrack.mgid.com/issue/TA-22549</a>
     * cab: Настройки видео кампании с типом DirectAds
     * rights - default/goodhits/can_use_campaign_type_video
     * RKO
     */
    @Test
    public void workRightCanUseCampaignTypeVideo() {
        log.info("Test is started");
        log.info("switch off right can_use_campaign_type_video");
        String campaignId = "17";
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_use_campaign_type_video");
        authCabForCheckPrivileges("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isCampaignTypeHaveCustomValue("video"), "FAIL -> type video isn't displayed");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().canEditVideoCampaign(cantEditCampaignMessage),
                "FAIL -> switch off right can_use_campaign_type_video, campaign can't edit");
        log.info("switch on right can_use_campaign_type_video");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "goodhits/can_use_campaign_type_video");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isCampaignTypeHaveCustomValue("video"), "FAIL -> type video is displayed");
        authCabAndGo("goodhits/campaigns-edit/id/" + campaignId);
        softAssert.assertTrue(pagesInit.getCabCampaigns().isShowCampaignTypeSelect(),
                "FAIL -> switch off right can_use_campaign_type_video, campaign can edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка клонирования(копирования) видео кампании DirectAds с включенными настройками из списка
     * КАТЕГОРИЯ и ЯЗЫК - изменяются изменяются при копировании
     * type: video
     * subType: directAds
     * <p>Author RKO/Moved by AIA</p>
     */
    @Test
    public void cloneDirectAdsVideoCampaign() {
        log.info("Test is started");

        String campaignId = "2003";
        log.info("Insert geo-targeting");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), "India");

        log.info("Save campaign settings");
        pagesInit.getCabCampaigns().setCustomTaggingSwitchOn(true);

        log.info("Create clone");
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        pagesInit.getCabCampaigns().createCloneCompanyWithCopyAllOptions(false, false, clonedCampaignSettings, true);

        log.info("Check clone options in EDIT interface");
        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkOptionsForCloneInEditInterface(clonedCampaignSettings),
                "FAIL -> Check clone options in EDIT interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка клонирования(копирования) видео кампании типа VAST/VPAID с включенными настройками из списка
     * КАТЕГОРИЯ и ЯЗЫК - изменяются изменяются при копировании
     * type: video
     * subType: directAds
     * <p>Author AIA</p>
     */
    @Test
    public void cloneVastVpaidVideoCampaign() {
        log.info("Test is started");

        String campaignId = "2004";
        log.info("Insert geo-targeting");
        operationMySql.getInsertGGeoTargeting().insertUpdateGeoTargetingInclude(Integer.parseInt(campaignId), "India");

        log.info("Save campaign settings");
        pagesInit.getCabCampaigns().setCustomTaggingSwitchOn(true);

        log.info("Create clone");
        authCabAndGo("goodhits/campaigns/?id=" + campaignId);
        pagesInit.getCabCampaigns().createCloneCompanyWithCopyAllOptions(false, false, clonedCampaignSettings, true);

        log.info("Check clone options in EDIT interface");
        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        Assert.assertTrue(pagesInit.getCabCampaigns().checkOptionsForCloneInEditInterface(clonedCampaignSettings),
                "FAIL -> Check clone options in EDIT interface");

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23039">https://youtrack.mgid.com/issue/TA-23039</a>
     * Создание/Редактирование, Проверка, Удаление кампании
     * type: video
     * subType: VAST/VPAID
     * <p>Author RKO/Moved by AIA</p>
     */
    @Test
    public void createEditVastVpaidVideoCampaign() {
        log.info("Test is started");
        int clientId = 1;
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + clientId);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setLimitsOption("impressions", "100", "100")
                .setUseTargeting(true)
                .setUtmType("mgclida=sub_id&sub_id={click_id}")
                .setTargetSections("location", "browser", "os", "browserLanguage")
                .saveVideoCampaign(subType);
        log.info("go to custom campaign and get her ID");
        pagesInit.getCabCampaigns().getCampaignIdFromListInterface();
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVisibilitySybTypeIcon("VAST/VPAID"), "Campaign subtype");
        log.info("Go to edit campaign interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        log.info("check campaign settings in edit interface after create");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVideoCampaignVastFields(), "FAIL -> check campaign after create");
        log.info("Go to edit campaign");
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setUseTargeting(true)
                .setTargetSections("platform", "interests", "location", "socdem")
                .setLimitsOption("both", "500", "501")
                .setBlockedByManager(true)
                .saveVideoCampaign(subType);
        log.info("Go to edit campaign interface");
        authCabAndGo("goodhits/campaigns-edit/id/" + pagesInit.getCabCampaigns().getCampaignId());
        log.info("check campaign after edit");
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkVideoCampaignVastFields(), "FAIL -> check campaign after edit");

        log.info("Delete campaign");
        authCabAndGo("goodhits/campaigns/id/" + pagesInit.getCabCampaigns().getCampaignId());
        pagesInit.getCabCampaigns().deleteCampaign(clientId);
        softAssert.assertFalse(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> After deleting campaign ");
        authCabAndGo(String.format("goodhits/campaigns/?id=%s&trash=1", pagesInit.getCabCampaigns().getCampaignId()));
        softAssert.assertTrue(pagesInit.getCabCampaigns().isDisplayedCampaign(clientId),
                "FAIL -> check deleted campaign by filter in trash");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23039">https://youtrack.mgid.com/issue/TA-23039</a>`
     * check valid field 'Price'
     * RKO
     */
    @Test(dataProvider = "vastPrices")
    public void vastVpaidVideoCampaignValidPrice(String vastPrice) {
        log.info("Test is started -> " + vastPrice);
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setUnlimitedState(true)
                .setVastPrice(vastPrice)
                .saveVideoCampaign(subType);
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Wrong VAST CPM. Should be more then 0.1 and less then 9.99"),
                "FAIL -> Wrong message for price - " + vastPrice);
        log.info("Test is finished -> " + vastPrice);
    }

    @DataProvider
    public Object[][] vastPrices() {
        return new Object[][]{
                {"a-z"},
                {"0"},
                {"0.09"},
                {"10.00"}
        };
    }

    @Epic("Create video campaign with incorrect capping limit")
    @Feature("Check validations for capping limit input")
    @Story("Video campaign create interface")
    @Description("Check validations for input incorrect capping limit values" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52469\">Ticket TA-52469</a>")
    @Owner("AIA")
    @Test(dataProvider = "cappingLimits", description = "Check validations for input incorrect capping limit values")
    public void vastVpaidVideoCampaignValidCappingLimits(String cappingLimit, String error) {
        log.info("Test is started -> " + cappingLimit);
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setUnlimitedState(true)
                .setCappingLimit(cappingLimit)
                .saveVideoCampaign(subType);
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("'" + cappingLimit + "' " + error));
        log.info("Test is finished -> " + cappingLimit);
    }

    @DataProvider
    public Object[][] cappingLimits() {
        return new Object[][]{
                {"-1", "is not between '0' and '100', inclusively"},
                {"101", "is not between '0' and '100', inclusively"},
                {"0.1", "does not appear to be an integer"},
                {"a-zA-Z", "does not appear to be an integer"}
        };
    }

    @Epic("Create video campaign with incorrect capping limit")
    @Feature("Check validations for capping limit input")
    @Story("Video campaign create interface")
    @Description("Check saving of VAST/VPAID video campaign with empty input of capping events limit" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52469\">Ticket TA-52469</a>")
    @Owner("AIA")
    @Test(description = "Check saving of VAST/VPAID video campaign with empty input of capping events limit")
    public void vastVpaidVideoCampaignWithEmptyCappingEventsLimits() {
        log.info("Test is started");
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setUnlimitedState(true)
                .setCappingLimit("")
                .saveVideoCampaign(subType);
        log.info("Check form is not closed with empty event capping by 'Frequency Capping' select is displayed");
        Assert.assertTrue(pagesInit.getCabCampaigns().checkFrequencyCappingIsDisplayed(),
                "FAIL -> VAST/VPAID campaign saved with empty capping events limits!");
        log.info("Test is finished");
    }

    @Epic("Create video campaign with incorrect capping limit")
    @Feature("Video campaign capping")
    @Story("Video campaign create interface")
    @Description("Check dependence for 'Send no more than' input state of 'Frequency capping' select" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52469\">Ticket TA-52469</a>")
    @Owner("AIA")
    @Test(description = "Check dependence for 'Send no more than' input state of 'Frequency capping' select")
    public void vastVpaidVideoCampaignWithUnlimitedCappingAndDisabledEventsLimits() {
        log.info("Test is started");
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setFrequencyCapping("Unlimited")
                .setVideoCampaignBaseFields();
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkCappingLimitIsEnabled(),
                "FAIL -> VAST/VPAID capping event limit 'Send no more than' is not disabled for 'Unlimited' Frequency capping by default!");
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setFrequencyCapping("15 minutes")
                .selectFrequencyCapping();
        softAssert.assertTrue(pagesInit.getCabCampaigns().checkCappingLimitIsEnabled(),
                "FAIL -> VAST/VPAID capping event limit 'Send no more than' is disabled for '15 minutes' Frequency capping!");
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setFrequencyCapping("Unlimited")
                .selectFrequencyCapping();
        softAssert.assertFalse(pagesInit.getCabCampaigns().checkCappingLimitIsEnabled(),
                "FAIL -> VAST/VPAID capping event limit 'Send no more than' is not disabled for 'Unlimited' Frequency capping!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Video campaigns create/edit")
    @Feature("Check validations for limits")
    @Story("Video campaign create interface")
    @Description("Check validations for input with empty limit values for VAST-CPCV campaign" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52458\">Ticket TA-52458</a>")
    @Owner("AIA")
    @Test(description = "Check validations for input with empty limit values for VAST-CPCV campaign")
    public void vastVpaidVideoLimitsValidation() {
        log.info("Test is started");
        CabCampaigns.VideoCampaignSubTypes subType = VAST;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("budget", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Budget' limit!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("impressions", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Impressions' limit!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("completeViews", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Complete Views' limit!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Video campaigns create/edit")
    @Feature("Check validations for limits")
    @Story("Video campaign create interface")
    @Description("Check validations for input with empty limit values for Prebid-CPCV campaign" +
            "@see <a href=\"https://jira.mgid.com/browse/TA-52458\">Ticket TA-52458</a>")
    @Owner("AIA")
    @Test(description = "Check validations for input with empty limit values for Prebid-CPCV campaign")
    public void prebidVideoLimitsValidation() {
        log.info("Test is started");
        CabCampaigns.VideoCampaignSubTypes subType = PREBID;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("clicks", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Clicks' limit!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("budget", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Budget' limit!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("impressions", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Impressions' limit!");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + CLIENT_MGID_ID);
        pagesInit.getCabCampaigns().clearSettings().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setCampaignPaymentModel("cpcv")
                .setLimitsOption("completeViews", "", "")
                .saveVideoCampaign(subType);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Fill one of limit's field"),
                "FAIL -> validation for empty 'Complete Views' limit!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Limits")
    @Feature("Limits. Daily can be equal to overall")
    @Story("Creating campaign form")
    @Description("Check error message when general limit is less then daily for campaign types:" +
            "- VAST" +
            "- directAds" +
            "- Prebid")
    @Owner("AIA")
    @Test(dataProvider = "campaignSybTypes")
    public void checkLimitsValidation(CabCampaigns.VideoCampaignSubTypes subType) {
        log.info("Test is started");
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + 2001);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setLimitsOption("budget", "51", "50")
                .saveVideoCampaign(subType);
        log.info("Check validation");
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("The overall budget should be greater than the daily budget"),
                "FAIL -> Check validation message!");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] campaignSybTypes() {
        return new Object[][]{
                {VAST},
                {PREBID}
        };
    }

    @Epic("Limits")
    @Feature("Limits. Daily can be equal to overall")
    @Story("Creating campaign form")
    @Description("Check error message when general limit is less then daily for campaign types:" +
            "- directAds")
    @Owner("AIA")
    @Test
    public void checkLimitsValidationForDirectAds() {
        log.info("Test is started");
        CabCampaigns.VideoCampaignSubTypes subType = DIRECT_ADS;
        authCabAndGo("goodhits/clients-new-campaign/client_id/" + 2001);
        pagesInit.getCabCampaigns().setIsGenerateNewValue(true)
                .setCampaignType("video")
                .setCampaignSubType(subType)
                .setEventUrls(eventUrls, true)
                .setLimitsOption("budget", "51", "50")
                .saveVideoCampaign(subType);
        log.info("Check validation");
        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("The overall budget should be greater than the daily budget"),
                "FAIL -> Check validation message!");
        log.info("Test is finished");
    }
}