package cab.publishers.sites;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.CabWebsites;
import core.helpers.BaseHelper;
import core.service.Retry;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static testData.project.EndPoints.sitesWagesListUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

/*
 * - addSites_PRIVILEGE_can_manage_site_source
 * - approveSite_PRIVILEGE_can_manage_site_source
 *
 * push_providers_id
 * - addSites_WithPushProvider
 * - editSites_WithSourceId_OnPushProvider
 * - addSites_VALID_WithoutPushProvider
 * - approveSite_validation_withoutPushProvider
 * - inline_changeCategory_pushProviders
 *
 * sources_id
 * - addSites_WithSourceId
 * - editSites_WithPushProvider_OnSourceId
 * - addSites_WithCreateNewSourceId
 * - addSites_VALID_WithCreateNewSourceId
 * - addSites_WithCreateNewSourceId_shouldBeSelected
 * - addSites_VALID_WithoutSourceId
 * - approveSite_validation_withoutSourceId
 * - inline_changeCategory_sourceId
 * - inline_changeCategory_addNewSourceId
 * - inline_changeCategory_VALID_WithCreateNewSourceId
 */
public class CabWebsitePushSourceProvidersTests extends TestBase {

    /**
     * Add website with push providers
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void addSites_WithPushProvider() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 29);
        Assert.assertNotNull(pagesInit.getCabWebsites()
                .setCategoryId("150")
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");
        softAssert.assertEquals(operationMySql.getClientsSites().getPushProvidersId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getPushProviderId(),
                "FAIL -> db: clients_sites.push_providers_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Edit website with push providers
     * У сайта должно быть заполнено что-то одно push_providers_id или sources_id. Если меняем категорию на НЕ пушовую, тогда push_providers_id должно затереться и наоборот, если на пушовую меняем, тогда затирается sources_id при сохранении сайта
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void editSites_WithPushProvider_OnSourceId() {
        log.info("Test is started");
        int siteId = 42;
        operationMySql.getClientsSites().updatePushProvidersId(130, 149, siteId);

        log.info("edit webSite");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setWebsiteId(String.valueOf(siteId))
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("8820")
                .editWebsite();

        log.info("check base fields created site in list interface");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");

        softAssert.assertEquals(operationMySql.getClientsSites().getPushProvidersId(pagesInit.getCabWebsites().getWebsiteId()),
                null,
                "FAIL -> db: clients_sites.push_providers_id");

        softAssert.assertEquals(operationMySql.getClientsSites().getSourcesId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getDomainThatWillBeDisplayedToAdvertisersId(),
                "FAIL -> db: clients_sites.source_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add website with Domain, that will be displayed to advertisers
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test(retryAnalyzer = Retry.class)
    public void addSites_WithSourceId() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        Assert.assertNotNull(pagesInit.getCabWebsites()
                .setCategoryId("108")
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");
        softAssert.assertEquals(operationMySql.getClientsSites().getSourcesId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getDomainThatWillBeDisplayedToAdvertisersId(),
                "FAIL -> db: clients_sites.source_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Edit website with sourceId
     * У сайта должно быть заполнено что-то одно push_providers_id или sources_id. Если меняем категорию на НЕ пушовую, тогда push_providers_id должно затереться и наоборот, если на пушовую меняем, тогда затирается sources_id при сохранении сайта
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void editSites_WithSourceId_OnPushProvider() {
        log.info("Test is started");
        int siteId = 43;
        operationMySql.getClientsSites().updateSourcesId(8820, 108, siteId);

        log.info("edit webSite");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setWebsiteId(String.valueOf(siteId))
                .setCategoryId("150")
                .setPushProviderId("110")
                .editWebsite();

        log.info("check base fields created site in list interface");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");

        softAssert.assertEquals(operationMySql.getClientsSites().getPushProvidersId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getPushProviderId(),
                "FAIL -> db: clients_sites.push_providers_id");

        softAssert.assertEquals(operationMySql.getClientsSites().getSourcesId(pagesInit.getCabWebsites().getWebsiteId()),
                null,
                "FAIL -> db: clients_sites.source_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    /**
     * Add website with - with create new source - 'Domain, that will be displayed to advertisers'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void addSites_WithCreateNewSourceId() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        Assert.assertNotNull(pagesInit.getCabWebsites()
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("-1")
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * На поле ввода нужна валидация существует ли уже сорс с типом domain или domain-low-volume. В случае совпадения - выдавать соответствующую ошибку при попытке добавления
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataDomain")
    public void addSites_VALID_WithCreateNewSourceId(String value, String errorMessage) {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        pagesInit.getCabWebsites()
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("-1")
                .setDomainThatWillBeDisplayedToAdvertisersName(value)
                .fillCategoryPushProviderOrDomainAdvertisersBlock();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage), "FAIL -> message");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDomain() {
        return new Object[][]{
                {"", "The entered domain for advertisers is not valid"},
                {"sourceDomain_1", "Source already exists, please enter another name"},
                {"sourceDomain_2", "Source already exists, please enter another name"}
        };
    }

    /**
     * После добавления домена - автоматом его устанавливать в поле Domain, that will be displayed to advertisers
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void addSites_WithCreateNewSourceId_shouldBeSelected() {
        String domainToAdvertisers = "tempDomainToAdvertisers" + BaseHelper.getRandomWord(2);
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        pagesInit.getCabWebsites()
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("-1")
                .setDomainThatWillBeDisplayedToAdvertisersName(domainToAdvertisers)
                .fillCategoryPushProviderOrDomainAdvertisersBlock();
        Assert.assertEquals(operationMySql.getSources().getSourcesName(pagesInit.getCabWebsites().getDomainThatWillBeDisplayedToAdvertisersId()),
                domainToAdvertisers);
        log.info("Test is finished");
    }

    /**
     * 'Domain, that will be displayed to advertisers' - Поле обязательное для заполнения
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void addSites_VALID_WithoutSourceId() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        softAssert.assertNull(pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setDomain("testDomainFF.com")
                .setCategoryId("108")
                .setLanguageId("2")
                .setTier("2")
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Domain not set in the category"), "FAIL -> message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * 'Push provider' - Поле обязательное для заполнения
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void addSites_VALID_WithoutPushProvider() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        softAssert.assertNull(pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setDomain("testDomainFF.com")
                .setCategoryId("150")
                .setLanguageId("2")
                .setTier("2")
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Push provider not set in the category"), "FAIL -> message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * create/edit/check webSite with (wages/can_manage_site_source=false)
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_manage_site_source")
    @Test(retryAnalyzer = Retry.class)
    public void addSites_PRIVILEGE_can_manage_site_source() {
        log.info("Test is started");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_manage_site_source");

        log.info("create new webSite");
        authCabForCheckPrivileges("wages/sites-new/client_id/" + 1);
        Assert.assertNotNull(pagesInit.getCabWebsites()
                .setRight_canManageSiteSource(false)
                .createWebsite(), "webSite doesn't create");

        log.info("check base fields created site in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabForCheckPrivileges("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");

        log.info("edit website");
        pagesInit.getCabWebsites()
                .isGenerateNewValues(true)
                .setOpportunityToPromoteNews(false)
                .setUseSubdomainsInNewsLinks(false)
                .editWebsite();

        log.info("check base fields edited site in list interface");
        authCabForCheckPrivileges(sitesWagesListUrl + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after EDIT: list interface don't check");

        log.info("check base fields edited site in edit interface");
        authCabForCheckPrivileges("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Approve website without PushProvider -> wait popup 'Push provider not set in the category'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void approveSite_validation_withoutPushProvider() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 44);
        pagesInit.getCabWebsites().approveWebSite(44);
        softAssert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "Push provider not set in the category", "FAIL -> popUp text");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Approve website without SourceId -> wait popup 'Domain not set in the category'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void approveSite_validation_withoutSourceId() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 45);
        pagesInit.getCabWebsites().approveWebSite(45);
        softAssert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "Domain not set in the category", "FAIL -> popUp text");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Approve website - without privilege 'can_manage_site_source'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_manage_site_source")
    @Test
    public void approveSite_PRIVILEGE_can_manage_site_source() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_manage_site_source");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(47, 1);
        authCabForCheckPrivileges(sitesWagesListUrl + 47);
        softAssert.assertTrue(pagesInit.getCabWebsites().approveWebSite(47), "FAIL -> approve icon");
        helpersInit.getBaseHelper().refreshCurrentPage();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.APPROVE, 47), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(47), "3", "FAIL -> db status_id");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteMirror(47), pagesInit.getCabWebsites().getMirrorsSubnet(), "FAIL -> db mirror");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Approve website without PushProvider -> wait popup 'Push provider not set in the category'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void verifySite_validation_withoutPushProvider() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 53);
        pagesInit.getCabWebsites().verifyWebSite(53);
        softAssert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "Push provider not set in the category", "FAIL -> popUp text");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Approve website without SourceId -> wait popup 'Domain not set in the category'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void verifySite_validation_withoutSourceId() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 54);
        pagesInit.getCabWebsites().verifyWebSite(54);
        softAssert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "Domain not set in the category", "FAIL -> popUp text");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check Verify website - without privilege 'can_manage_site_source'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_manage_site_source")
    @Test
    public void verifySite_PRIVILEGE_can_manage_site_source() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_manage_site_source");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(55, 1);
        authCabForCheckPrivileges(sitesWagesListUrl + 55);
        pagesInit.getCabWebsites().verifyWebSite(55);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.VERIFY, 55), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(55), "6", "FAIL -> db status_id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * inline change category: change push_providers_id on sources_id
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void inline_changeCategory_sourceId() {
        int siteId = 48;
        log.info("Test is started");
        operationMySql.getClientsSites().updatePushProvidersId(130, 149, siteId);

        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setWebsiteId(String.valueOf(siteId))
                .setCategoryId("108")
                .editCategoryInline();

        log.info("check base fields created site in list interface");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertEquals(operationMySql.getClientsSites().getPushProvidersId(pagesInit.getCabWebsites().getWebsiteId()),
                null,
                "FAIL -> db: clients_sites.push_providers_id");

        softAssert.assertEquals(operationMySql.getClientsSites().getSourcesId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getDomainThatWillBeDisplayedToAdvertisersId(),
                "FAIL -> db: clients_sites.source_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Edit Category inline - with create new source - 'Domain, that will be displayed to advertisers'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void inline_changeCategory_addNewSourceId() {
        int siteId = 48;
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setWebsiteId(String.valueOf(siteId))
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("-1")
                .editCategoryInline();

        log.info("check base fields created site in list interface");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * На поле ввода нужна валидация существует ли уже сорс с типом domain или domain-low-volume. В случае совпадения - выдавать соответствующую ошибку при попытке добавления
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataDomain")
    public void inline_changeCategory_VALID_WithCreateNewSourceId(String value, String errorMessage) {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo(sitesWagesListUrl + 48);
        pagesInit.getCabWebsites()
                .setCategoryId("108")
                .setDomainThatWillBeDisplayedToAdvertisersId("-1")
                .setDomainThatWillBeDisplayedToAdvertisersName(value)
                .editCategoryInline();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage), "FAIL -> message");
        log.info("Test is finished");
    }

    /**
     * inline change category: change sources_id on push_providers_id
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27217">TA-27217</a>
     * <p>RKO</p>
     */
    @Test
    public void inline_changeCategory_pushProviders() {
        int siteId = 49;
        log.info("Test is started");

        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setWebsiteId(String.valueOf(siteId))
                .setCategoryId("150")
                .editCategoryInline();

        log.info("check base fields created site in list interface");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertEquals(operationMySql.getClientsSites().getPushProvidersId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getPushProviderId(),
                "FAIL -> db: clients_sites.push_providers_id");

        softAssert.assertEquals(operationMySql.getClientsSites().getSourcesId(pagesInit.getCabWebsites().getWebsiteId()),
                null,
                "FAIL -> db: clients_sites.source_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check that 'Push provider' select is available for site with 'push' platform_type category
     * @see <a href="https://jira.mgid.com/browse/TA-51793">Ticket TA-51793</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkPushProviderDependencyFromCategory() {
        log.info("Test is started");
        int siteId = 2017;
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkPushProvider(),
                "FAIL -> Push provider is available for site with push-category!");
        softAssert.assertFalse(pagesInit.getCabWebsites().checkDomainSource(),
                "FAIL -> Domain (source) isn't available for site with push-category!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check that 'Domain, that will be displayed to advertisers' select is available for site with 'website' platform_type category
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51793">Ticket TA-51793</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkDomainSourceDependencyFromCategory() {
        log.info("Test is started");
        int siteId = 2018;
        authCabAndGo("wages/sites-edit/id/" + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDomainSource(),
                "FAIL -> Domain (source) is available for site with website-category!");
        softAssert.assertFalse(pagesInit.getCabWebsites().checkPushProvider(),
                "FAIL -> Push provider isn't available for site with website-category!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
