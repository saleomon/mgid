package cab.publishers.sites;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.List;

import static core.service.constantTemplates.ConstantsInit.NIO;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.EndPoints.sitesWagesListUrl;

public class CabWebsiteTierTests extends TestBase {

    @Epic("Publisher Website")
    @Feature("Create Website")
    @Story("Create settings")
    @Description("<ul>check: change category platform don't influence on site tier:</ul>" +
            "<li> -set category and tier options</li>" +
            "<li> -check successful saving of site</li>" +
            "<li> -check tier option for selected element</li>" +
            "<li><a href=\"https://youtrack.mgid.com/issue/TA-23804\">TA-23804</a></li>")
    @Owner(NIO)
    @Test(description = "check changing tier option by edit category")
    public void checkChangingTierOptionByEditCategory() {
        log.info("Test is started");
        int tier = 5;
        operationMySql.getClientsSites().updateTier(tier, WEBSITE_MGID_WAGES_ID);
        operationMySql.getClientsSites().updateCategory(142, WEBSITE_MGID_WAGES_ID);
        authCabAndGo("wages/sites-edit/id/" + WEBSITE_MGID_WAGES_ID);
        log.info("change category");
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setCategoryId("109")
                .editWebsite();
        pagesInit.getCabWebsites().setTier(Integer.toString(tier));
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Editing site successfully"), "Message wasn't shown");
        authCabAndGo("wages/sites-edit/id/" + WEBSITE_MGID_WAGES_ID);
        log.info("check tier option");
        Assert.assertTrue(pagesInit.getCabWebsites().checkTierInEditInterface(), "There are no selected options");
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("Inline edit tier " +
            "<a href=\"https://youtrack.mgid.com/issue/VT-25959\">VT-25959</a>")
    @Owner(RKO)
    @Test(description = "Inline edit tier")
    public void checkInlineWebsiteTier() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 19);
        pagesInit.getCabWebsites().editTierInline();
        Assert.assertTrue(pagesInit.getCabWebsites().checkTierInListInterface(), "FAIL -> inline tier");
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website filters")
    @Description("Filter by tier (Choose random tier and check work filter) " +
            "<a href=\"https://youtrack.mgid.com/issue/TA-23929\">TA-23929</a>")
    @Owner(RKO)
    @Test(description = "Filter by tier")
    public void tierFilter() {
        log.info("Test is started");
        // get list tier (id::name)
        List listTier = operationMySql.getClientsSitesTier().getListTier();
        authCabAndGo("wages/sites");
        pagesInit.getCabWebsites().chooseTierFilterValue(listTier);
        Assert.assertTrue(pagesInit.getCabWebsites().checkTierInListInterface(), "FAIL -> tier filter doesn't work");
        log.info("Test is finished");
    }
}
