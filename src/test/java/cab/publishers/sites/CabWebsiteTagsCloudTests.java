package cab.publishers.sites;

import org.testng.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.EndPoints.sitesWagesListUrl;

public class CabWebsiteTagsCloudTests extends TestBase {

    @BeforeMethod
    public void initializeVariables(){
        operationMySql.getClientsSitesExcludedTags().deleteAllTags(WEBSITE_MGID_WAGES_ID, 5);
    }

    /**
     * https://youtrack.mgid.com/issue/TA-22017
     * Tags Cloud. Список сайтов wages
     * Проверяем что теги верно сохраняются, редактируются и снимаются
     * RKO
     */
    @Test
    public void tagsCloudForOneSite() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_WAGES_ID);

        log.info("записываем тип тегов в массив");
        pagesInit.getCabWebsites().setTagsType("title", "image", "lp", "description");

        log.info("проставляем рандомные теги и сохраняем форму");
        pagesInit.getCabWebsites().openTagsCloudForm();
        pagesInit.getCabWebsites().chooseRandomTags();
        pagesInit.getCabWebsites().saveTagsForm();

        log.info("открываем форму и проверям что все теги сохранились");
        pagesInit.getCabWebsites().openTagsCloudForm();
        Assert.assertTrue(pagesInit.getCabWebsites().checkChooseTags(), "FAIL -> choose tags don't show");
        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/TA-22017
     * Tags Cloud. Список сайтов wages
     * Проверяем что теги правильно отфильтровуются
     * RKO
     */
    @Test
    public void searchTagsInForm() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_WAGES_ID);
        pagesInit.getCabWebsites().openTagsCloudForm();
        Assert.assertTrue(pagesInit.getCabWebsites().checkSearchTags());
        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/TA-22017
     * Tags Cloud. Список сайтов wages
     * Проверяем что теги правильно проставляются для всех сайтов клиента
     * RKO
     */
    @Test
    public void tagsCloudForAllClientSites() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_WAGES_ID);

        log.info("записываем тип тегов в массив");
        pagesInit.getCabWebsites().setTagsType("title", "image", "lp", "description");

        log.info("проставляем рандомные теги и сохраняем форму");
        pagesInit.getCabWebsites().openTagsCloudForm();
        pagesInit.getCabWebsites().chooseRandomTags();
        pagesInit.getCabWebsites().clickSwitchOnTagsForAllSitesClient();
        pagesInit.getCabWebsites().saveTagsForm();

        log.info("Переходим на другой сайт єтого же клиента открываем форму и проверям что все теги скопировались");
        authCabAndGo(sitesWagesListUrl + 5);
        pagesInit.getCabWebsites().openTagsCloudForm();
        Assert.assertTrue(pagesInit.getCabWebsites().checkChooseTags(), "FAIL -> choose tags don't copy to other site");
        log.info("Test is finished");
    }
}
