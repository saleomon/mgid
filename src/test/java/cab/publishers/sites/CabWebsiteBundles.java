package cab.publishers.sites;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Selenide.refresh;
import static testData.project.EndPoints.sitesWagesListUrl;


/*
 * 1. check auto set bundles in create webSite interface (change tier/category)
 * 2. check auto set bundles in edit webSite interface (change tier/category)
 * 3. check auto set bundles in list webSite interface (change inline tier)
 * 4. check extend site bundles to widget
 * 5. get widget with bundles and change him category_platform to PUSH -> bundles clear
 * 6. get widget without bundles and change him category_platform THEN PUSH -> bundles don't set
 * 7. get widget and set him bundles; after create push -> and check new bundles
 */
public class CabWebsiteBundles extends TestBase {

    /**
     * check auto set bundles in create webSite interface (change tier/category)
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void autoSetBundlesInCreateInterface() {
        List bundles;
        log.info("Test is started");
        authCabAndGo("wages/sites-new/client_id/" + 1);

        log.info("case #1 - tier: 1, category: 108");
        bundles = operationMySql.getBundles().selectBundleName("1", "108");

        pagesInit.getCabWebsites()
                .setTier("1")
                .setCategoryId("108")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 1, category: 108");

        log.info("case #2 - tier: 3, category: 101");
        bundles = operationMySql.getBundles().selectBundleName("3", "101");

        pagesInit.getCabWebsites()
                .setTier("3")
                .setCategoryId("101")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 3, category: 101");

        log.info("case #4 - tier: 4, category: 102");
        bundles = operationMySql.getBundles().selectBundleName("4", "102");

        pagesInit.getCabWebsites()
                .setTier("4")
                .setCategoryId("102")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 4, category: 102");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check auto set bundles in edit webSite interface (change tier/category)
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void autoSetBundlesInEditInterface() {
        List bundles;
        log.info("Test is started");
        authCabAndGo("wages/sites-edit/id/" + 24);

        log.info("case #1 - tier: 1, category: 108");
        bundles = operationMySql.getBundles().selectBundleName("1", "108");

        pagesInit.getCabWebsites()
                .setTier("1")
                .setCategoryId("108")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 1, category: 108");

        log.info("case #2 - tier: 3, category: 101");
        bundles = operationMySql.getBundles().selectBundleName("3", "101");

        pagesInit.getCabWebsites()
                .setTier("3")
                .setCategoryId("101")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 3, category: 101");

        log.info("case #4 - tier: 4, category: 102");
        bundles = operationMySql.getBundles().selectBundleName("4", "102");

        pagesInit.getCabWebsites()
                .setTier("4")
                .setCategoryId("102")
                .setBundles((ArrayList<String>) bundles);

        softAssert.assertTrue(pagesInit.getCabWebsites()
                .chooseTier()
                .chooseCategory()
                .checkBundles(), "FAIL -> tier: 4, category: 102");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check auto set bundles in list webSite interface (change inline tier)
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void autoSetBundlesInListInterface() {
        List bundles;
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 24);

        log.info("case #1 - tier: 1, category: 108");
        bundles = operationMySql.getBundles().selectBundleName("1", "108");

        pagesInit.getCabWebsites()
                .setBundles((ArrayList<String>) bundles)
                .setTier("1");

        pagesInit.getCabWebsites().editTierInline();
        refresh();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkBundlesInListInterface(), "FAIL(case #1) -> tier: 1, category: 108");

        log.info("case #2 - tier: 3, category: 108");
        bundles = operationMySql.getBundles().selectBundleName("3", "108");

        pagesInit.getCabWebsites()
                .setBundles((ArrayList<String>) bundles)
                .setTier("3");

        pagesInit.getCabWebsites().editTierInline();
        refresh();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkBundlesInListInterface(), "FAIL(case #2) -> tier: 3, category: 108");

        log.info("case #3 - tier: 5, category: 108");
        bundles = operationMySql.getBundles().selectBundleName("5", "108");

        pagesInit.getCabWebsites()
                .setBundles((ArrayList<String>) bundles)
                .setTier("5");

        pagesInit.getCabWebsites().editTierInline();
        refresh();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkBundlesInListInterface(), "FAIL(case #3) -> tier: 5, category: 108");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check extend bundles from site to widget
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void extendSiteBundlesToWidget() {
        int tickerCompositeId;
        List sitesBundles, gBlocksBundles;
        log.info("Test is started");
        authDashAndGo("testEmail31@ex.ua", "publisher/add-widget/site/" + 33);
        softAssert.assertNotEquals((tickerCompositeId = pagesInit.getWidgetClass().createSimpleWidget()), 0, "FAIL -> widget isn't create");

        log.info("case #1 - tier: 1, category: 108");
        sitesBundles = operationMySql.getClientsSitesBundles().getBundlesId(33);
        gBlocksBundles = operationMySql.getBundlesGBlocks().getBundlesId(tickerCompositeId);

        softAssert.assertEquals(sitesBundles, gBlocksBundles, "FAIL -> copy bundles to gBlocks");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <p>Если в настройках виджета меняется категория на пушовую (SELECT id FROM category_platform WHERE platform_type = 'push') + сохраняем виджет, тогда все bundles для этого виджета с bundles_g_blocks должны удалиться</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void changeWidgetPublisherCategory_Push_and_CheckClearWidgetBundles() {
        log.info("Test is started");
        operationMySql.getBundlesGBlocks().insertBundles(51, 8, 9, 10, 11);
        authCabAndGo("wages/informers-edit/type/composite/id/" + 57);
        pagesInit.getCabCompositeSettings().setCategoryPlatform("150").chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertFalse(operationMySql.getBundlesGBlocks().selectIsExistBundlesId(51));
        log.info("Test is finished");
    }

    /**
     * <p>Если меняем назад категорию на не пушовую, bundles назад не наполняем</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void changeWidgetPublisherCategory_Other_and_CheckWidgetBundles() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 58);
        pagesInit.getCabCompositeSettings().setCategoryPlatform("108").chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertFalse(operationMySql.getBundlesGBlocks().selectIsExistBundlesId(52));
        log.info("Test is finished");
    }

    /**
     * <ul>Если мы создаем внутренний пуш виджет, тогда удаляем существующие bundles для виджета и добавляем новые:</ul>
     * <li>1.Для виджетов lifetime 0-3 и 3-10 - bundles.bundles_id=2</li>
     * <li>2. Для виджета lifetime 10+ - bundles.bundles_id=3</li>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24146">TA-24146</a>
     * <p>RKO</p>
     */
    @Test
    public void checkBundlesOnCreatePushWidget() {
        int tickerCompositeId_10, tickerCompositeId_0_3, tickerCompositeId_3_10, gBlocksId_10;
        log.info("Test is started");
        authDashAndGo("testEmail32@ex.ua","publisher/add-widget/site/" + 34);
        softAssert.assertNotEquals((tickerCompositeId_10 = pagesInit.getWidgetClass().createSimpleWidget()), 0, "FAIL -> widget isn't create");

        gBlocksId_10 = operationMySql.getGblocks().getGBlocksId(tickerCompositeId_10);
        operationMySql.getBundlesGBlocks().insertBundles(gBlocksId_10, 8, 9, 10, 11);

        log.info("create push widget");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId_10);
        pagesInit.getCabWidgetsList().openPushPopUp(tickerCompositeId_10);
        Assert.assertTrue(pagesInit.getCabWidgetsList().createEditPushWidget(tickerCompositeId_10), "FAIL -> push widget doesn't created");

        log.info("get CompositeLifetime id and g_blocks ids");
        tickerCompositeId_0_3 = operationMySql.getTickersCompositePushRules().getCompositeLifetime0(tickerCompositeId_10);
        tickerCompositeId_3_10 = operationMySql.getTickersCompositePushRules().getCompositeLifetime1(tickerCompositeId_10);

        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(tickerCompositeId_10), Collections.singletonList("3"), "FAIL -> gBlocksId_10 bundles");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(tickerCompositeId_0_3), Collections.singletonList("2"), "FAIL -> gBlocksId_0_3 bundles");
        softAssert.assertEquals(operationMySql.getBundlesGBlocks().getBundlesId(tickerCompositeId_3_10), Collections.singletonList("2"), "FAIL -> gBlocksId_3_10 bundles");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
