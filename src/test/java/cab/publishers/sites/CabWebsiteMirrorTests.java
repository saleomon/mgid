package cab.publishers.sites;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.CabWebsites;
import testBase.TestBase;

import static testData.project.EndPoints.sitesWagesListUrl;

public class CabWebsiteMirrorTests extends TestBase {

    /**
     * Возможность добавлять домены формата .domain.com в зеркала с админки
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24507">TA-24507</a>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataMirrors")
    public void setMirror(String mirror) {
        pagesInit.getCabWebsites().setWebsiteId("56");
        log.info("edit website");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setMirror(mirror)
                .editWebsite();

        log.info("check base fields edited site in list interface");
        authCabAndGo(sitesWagesListUrl + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after EDIT: list interface don't check");

        log.info("check base fields edited site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertEquals(operationMySql.getClientsSites().getMirrorDomains(pagesInit.getCabWebsites().getWebsiteId()), mirror, "FAIL -> db: mirror_domains");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataMirrors() {
        return new Object[][]{
                {"1-test.domain.com"},
                {"test.seo.blog.test.domain.com"}
        };
    }

    /**
     * Возможность добавлять домены формата .domain.com в зеркала с админки
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24507">TA-24507</a>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataWrongMirrors")
    public void setMirror_VALID(String mirror) {
        log.info("Test is started");

        log.info("edit webSite");
        authCabAndGo("wages/sites-edit/id/" + 56);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setMirror(mirror)
                .editWebsite();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("NO RIGHT DOMAIN NAME"), "FAIL -> message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataWrongMirrors() {
        return new Object[][]{
                {"test@@@"},
                {"wwwwresrtestresttesttestseoblogtestdomaincomestseoblogtestdomaincomtestseoblogtestdomain.test.seo.com"},
                {"test&&&&&&&ex.ua"}
        };
    }
}
