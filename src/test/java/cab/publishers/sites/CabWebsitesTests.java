package cab.publishers.sites;

import io.qameta.allure.*;
import org.testng.*;
import org.testng.annotations.*;
import pages.cab.publishers.logic.CabWebsites;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.AIA;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static testData.project.AuthUserCabData.*;
import static testData.project.ClientsEntities.CLIENT_IDEALMEDIA_USD_ID;
import static testData.project.EndPoints.sitesWagesListUrl;

/*
 * - createEditWebSites
 * - rejectSite
 * - approveSite
 * - deleteSite
 * - verifySite
 * - newCuratorFilter
 * - checkSiteAndExchangeCampaignLanguageAfterCreating
 *
 * VALIDATION
 * - verifySite_validation_withoutTier
 * - approveSite_validation_withoutTier
 */
public class CabWebsitesTests extends TestBase {

    @Epic("Publisher Website")
    @Feature("Create Website")
    @Story("Create Website")
    @Description("create/edit/check webSite " +
            "check adding ads.txt after creating of the website" +
            "* @see <a href=\"https://jira.mgid.com/browse/TA-52423\">Ticket TA-52423</a>")
    @Owner(RKO)
    @Test(description = "create/edit/check webSite")
    public void createEditWebSites() {
        log.info("Test is started");

        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        Assert.assertNotNull(pagesInit.getCabWebsites().createWebsite(), "webSite doesn't create");
        log.info("check COPPA flag in DB");
        softAssert.assertEquals(operationMySql.getClientsSites().getCoppaValue(pagesInit.getCabWebsites().getWebsiteId()), 1,
                "FAIL -> parameter coppa is 0");
        log.info("check base fields created site in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after CREATE: list interface don't check");

        log.info("check base fields created site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after CREATE: edit interface don't check");

        log.info("edit website");
        pagesInit.getCabWebsites()
                .isGenerateNewValues(true)
                .setOpportunityToPromoteNews(false)
                .setUseSubdomainsInNewsLinks(false)
                .setCoppaCheckbox(false)
                .editWebsite();

        log.info("check COPPA flag in DB");
        softAssert.assertEquals(operationMySql.getClientsSites().getCoppaValue(pagesInit.getCabWebsites().getWebsiteId()), 0,
                "FAIL -> parameter coppa is 1");
        log.info("check base fields edited site in list interface");
        authCabAndGo(sitesWagesListUrl + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().setStatus(CabWebsites.SiteStatus.APPROVE).checkWebsiteInListInterface(), "after EDIT: list interface don't check");

        log.info("check base fields edited site in edit interface");
        authCabAndGo("wages/sites-edit/id/" + pagesInit.getCabWebsites().getWebsiteId());
        softAssert.assertTrue(pagesInit.getCabWebsites().checkWebSiteInEditInterface(), "after EDIT: edit interface don't check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("Create Website")
    @Story("Create Website")
    @Description("validation => add site with the same domain")
    @Owner(RKO)
    @Test(description = "validation => add site with the same domain")
    public void addSiteWithSameDomain() {
        log.info("Test is started");

        log.info("create webSite");
        authCabAndGo("wages/sites-new/client_id/" + 1);
        pagesInit.getCabWebsites()
                .isSetSomeFields(true)
                .setDomain(".testsitemg.com")
                .setCategoryId("108")
                .setLanguageId("2")
                .setTier("2")
                .createWebsite();

        Assert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("This domain belongs to other client"));
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website filters")
    @Description("Filter by manager (expand all spans / choose custom curator / click filter / check tasks by curator)" +
            "<a href=\"https://youtrack.mgid.com/issue/TA-23937\">TA-23937</a>")
    @Owner(RKO)
    @Test(description = "Filter by manager")
    public void filterByManager() {
        log.info("Test is started");
        log.info("get random curator");
        String curator = helpersInit.getBaseHelper().getRandomFromArray(new String[]{indiaPublisherUser, apacPublisherUser, cisPublisherUser, europePublisherUser});
        authCabAndGo("wages/sites");
        log.info("set chosen curator and search sites");
        Assert.assertTrue(pagesInit.getCabWebsites().checkWorkCuratorFilter(curator), "FAIL -> sites don't search by curator " + curator);
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check site and exchange campaign language after creating " +
            "<a href=\"https://youtrack.mgid.com/issue/TA-24160\">TA-24160, </a>" +
            "<a href=\"https://youtrack.mgid.com/issue/TA-24137\">TA-24137</a>")
    @Owner(AIA)
    @Test(description = "check site and exchange campaign language after creating")
    public void checkSiteAndExchangeCampaignLanguageAfterCreating() {
        log.info("Test is started");
        log.info("create new webSite");
        authCabAndGo("wages/sites-new/client_id/" + CLIENT_IDEALMEDIA_USD_ID);
        pagesInit.getCabWebsites().createWebsite();
        log.info("* Check website language ID *");
        softAssert.assertEquals(operationMySql.getClientsSites().getSiteLanguageId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getLanguageId(),
                "FAIL -> Site language is wrong!");
        log.info("* Check exchange campaign language ID *");
        softAssert.assertEquals(operationMySql.getPartners1().getExchangeCampaignLanguageId(pagesInit.getCabWebsites().getWebsiteId()),
                pagesInit.getCabWebsites().getLanguageId(),
                "FAIL -> Exchange campaign language is wrong!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Reject website")
    @Owner(RKO)
    @Test(description = "reject website")
    public void rejectSite() {
        log.info("Test is started");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(38, 1);
        authCabAndGo(sitesWagesListUrl + 38);
        softAssert.assertTrue(pagesInit.getCabWebsites().rejectWebSite(38), "FAIL -> reject icon");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.REJECT, 38), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(38), "2", "FAIL -> db status_id");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getRefuseReason(38), pagesInit.getCabWebsites().getRejectReason(), "FAIL -> db refuse_reason");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Approve website")
    @Owner(RKO)
    @Test(description = "approve website")
    public void approveSite() {
        log.info("Test is started");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(37, 1);
        authCabAndGo(sitesWagesListUrl + 37);
        softAssert.assertTrue(pagesInit.getCabWebsites().approveWebSite(37), "FAIL -> approve icon");
        helpersInit.getBaseHelper().refreshCurrentPage();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.APPROVE, 37), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(37), "3", "FAIL -> db status_id");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteMirror(37), pagesInit.getCabWebsites().getMirrorsSubnet(), "FAIL -> db mirror");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Delete website")
    @Owner(RKO)
    @Test(description = "delete website")
    public void deleteSite() {
        log.info("Test is started");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(40, 1);
        authCabAndGo(sitesWagesListUrl + 40);
        softAssert.assertTrue(pagesInit.getCabWebsites().deleteWebSite(40), "FAIL -> delete icon");
        authCabAndGo("wages/sites?droped=1&id=" + 40);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.DELETE, 40), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(40), "1", "FAIL -> db status_id");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getDroped(40), "1", "FAIL -> db droped");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Verify website")
    @Owner(RKO)
    @Test(description = "verify site")
    public void verifySite() {
        log.info("Test is started");
        operationMySql.getClientsSitesStatuses().updateSiteStatus(39, 1);
        authCabAndGo(sitesWagesListUrl + 39);
        pagesInit.getCabWebsites().verifyWebSite(39);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.VERIFY, 39), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(39), "6", "FAIL -> db status_id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Verify website without Tier -> wait popup 'You can not approve this site. No tier selected.'")
    @Owner(RKO)
    @Test(description = "validation => verify site without Tier")
    public void verifySiteWithoutTier() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 41);
        pagesInit.getCabWebsites().verifyWebSite(41);
        Assert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "No tier selected.");
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Approve website without Tier -> wait popup 'You can not approve this site. No tier selected.'")
    @Owner(RKO)
    @Test(description = "validation => approve site without Tier")
    public void approveSiteWithoutTier() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 41);
        softAssert.assertFalse(pagesInit.getCabWebsites().approveWebSite(41), "FAIL -> site is approve");
        softAssert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "No tier selected.", "FAIL -> popUp text");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Approve website without Category Platform -> wait popup 'You can not approve this site. No category selected.'")
    @Owner(RKO)
    @Test(description = "validation => approve site without category platform")
    public void approveSiteWithoutCategoryPlatform() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 46);
        pagesInit.getCabWebsites().approveWebSite(46);
        Assert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "No category selected.");
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("clients_sites.tier_id=2" +
            " approve site without 'Client legal name' and 'Campaign main website' " +
            "<a href='https://jira.mgid.com/browse/TA-52413'>TA-52413</a>")
    @Owner(RKO)
    @Test(dataProvider = "dataApproveSiteValidationTier2WithoutClientLegalNameAndCampaignMainWebsite", description = "validation => approve site without 'Client legal name' and 'Campaign main website'")
    public void approveSiteValidationTier2WithoutClientLegalNameAndCampaignMainWebsite(int siteId, String comment) {
        log.info("Test is started: " + comment);
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().approveWebSite(siteId);
        Assert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(),
                "Clients Legal Name and Companies Main Website should be filled in the client settings.");
        log.info("Test is finished: " + comment);
    }

    @DataProvider
    public Object[][] dataApproveSiteValidationTier2WithoutClientLegalNameAndCampaignMainWebsite(){
        return new Object[][]{
                {144, "site with filled both 'legal_name' and 'main_site'"},
                {141, "site with only 'legal_name'"}
        };
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("clients_sites.tier_id=1" +
            " verify site without 'Client legal name' and 'Campaign main website' " +
            "<a href='https://jira.mgid.com/browse/TA-52413'>TA-52413</a>")
    @Owner(RKO)
    @Test(dataProvider = "dataVerifySiteValidationTier1WithoutClientLegalNameAndCampaignMainWebsite", description = "validation => verify site without 'Client legal name' and 'Campaign main website'")
    public void verifySiteValidationTier1WithoutClientLegalNameAndCampaignMainWebsite(int siteId, String comment) {
        log.info("Test is started: " + comment);
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().verifyWebSite(siteId);
        Assert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(),
                "Clients Legal Name and Companies Main Website should be filled in the client settings.");
        log.info("Test is finished: " + comment);
    }

    @DataProvider
    public Object[][] dataVerifySiteValidationTier1WithoutClientLegalNameAndCampaignMainWebsite(){
        return new Object[][]{
                {143, "site with filled both 'legal_name' and 'main_site'"},
                {142, "site with only 'main_site'"}
        };
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Website settings")
    @Description("check Verify website without Category Platform -> wait popup 'Domain not set in the category'")
    @Owner(RKO)
    @Test(description = "validation => verify website without Category Platform")
    public void verifySiteWithoutCategoryPlatform() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 46);
        pagesInit.getCabWebsites().verifyWebSite(46);
        Assert.assertEquals(pagesInit.getCabWebsites().getValidationPopUpOnApprove(), "Domain not set in the category");
        log.info("Test is finished");
    }

    @Epic("Publisher Website")
    @Feature("List Website")
    @Story("Disable widgets when the manager has rejected the website")
    @Description("Reject site with disable all widgets <a href='https://jira.mgid.com/browse/TA-52302'>TA-52302</a>")
    @Owner(RKO)
    @Test(description = "Reject site with disable all widgets")
    public void rejectSiteWithDisableWidgets() {
        log.info("Test is started");
        int siteId = 131, clientId = 78;
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().rejectWebSite(siteId), "FAIL -> reject icon");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkStatus(CabWebsites.SiteStatus.REJECT, siteId), "FAIL -> ui status field");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getSiteStatus(siteId), "2", "FAIL -> db status_id");
        softAssert.assertEquals(operationMySql.getClientsSitesStatuses().getRefuseReason(siteId), pagesInit.getCabWebsites().getRejectReason(), "FAIL -> db refuse_reason");
        softAssert.assertEquals(pagesInit.getCabWebsites().getTextAlertRejectSiteAndDisableWidgets(), "All active widgets on the website will be disabled", "FAIL -> alert text");

        authCabAndGo("wages/widgets/?client_id=" + clientId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(527), "red", "FAIL -> 'block/unblock' icon: 527");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(528), "red", "FAIL -> 'block/unblock' icon: 528");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(529), "red", "FAIL -> 'block/unblock' icon: 529");
        softAssert.assertAll();
        log.info("Test is finished");
    }

}
