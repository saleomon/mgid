package cab.publishers.sites;

import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.refresh;
import static testData.project.EndPoints.sitesWagesListUrl;


/*
 * 1. work filter 'Post moderation'
 * 2. choose 3 random postModeration checkboxes + click save + check postModeration icon
 *    choose all postModeration checkboxes + click finish,
 *    after click cancel and check that form open (with filled all postModeration checkboxes)
 *    after click approve and check
 * 3. get 2 sites
 *    site 1: choose 3 random postModeration checkboxes + click save
 *    site 2: open post-moderation form - she should be clear
 */
public class CabWebsitePostModerationTests extends TestBase {

    private final int siteId = 32;

    /**
     * work filter 'Post moderation'
     * <p>RKO</p>
     */
    @Test
    public void postModeration_filter() {
        log.info("Test is started");
        operationMySql.getClientsSites().updateToDefaultPostModerationSettings(siteId);
        operationMySql.getClientsSitesPostModeration().deletePostModerationBySite(siteId);
        authCabAndGo("wages/sites");
        pagesInit.getCabWebsites().clickPostModerationFilter(true);
        pagesInit.getCabWebsites().submitFilter();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkPostModerationFilter(), "FAIL -> filter selected");

        pagesInit.getCabWebsites().clickPostModerationFilter(false);
        pagesInit.getCabWebsites().chooseSubnetFilter("{\"subnet\":0,\"mirror\":null}");
        pagesInit.getCabWebsites().submitFilter();
        softAssert.assertFalse(pagesInit.getCabWebsites().checkPostModerationFilter(), "FAIL -> filter doesn't selected");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * work 'Post moderation' functional
     * <p>RKO</p>
     */
    @Test
    public void postModeration_checkSaveForm() {
        log.info("Test is started");
        operationMySql.getClientsSites().updateToDefaultPostModerationSettings(siteId);
        operationMySql.getClientsSitesPostModeration().deletePostModerationBySite(siteId);

        log.info("choose random options and click 'Save'");
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickPostModerationIcon(siteId);
        pagesInit.getCabWebsites().chooseRandomPostModerationOptions();
        pagesInit.getCabWebsites().fillPostModerationComment();
        softAssert.assertFalse(pagesInit.getCabWebsites().isEnabledFinishModerationButton(), "FAIL -> isEnabledFinishModerationButton");
        pagesInit.getCabWebsites().clickSavePostModerationForm();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Moderation process saved"), "FAIL -> message 'Moderation process saved'");

        log.info("check random options");
        pagesInit.getCabWebsites().clickPostModerationIcon(siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkPostModerationOptions(), "FAIL -> checkPostModerationOptions");

        log.info("choose all options");
        pagesInit.getCabWebsites().chooseAllPostModerationOptions();
        pagesInit.getCabWebsites().clickFinishPostModerationForm();

        log.info("confirm cancel");
        pagesInit.getCabWebsites().clickConfirmCancelPostModeration();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkPostModerationOptions(), "FAIL -> checkPostModerationOptions");

        log.info("confirm approve");
        pagesInit.getCabWebsites().clickFinishPostModerationForm();
        pagesInit.getCabWebsites().clickConfirmApprovePostModeration();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Post-moderation completed successfully"), "FAIL -> message 'Post-moderation completed successfully'");

        log.info("check that site show without postModeration");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertFalse(pagesInit.getCabWebsites().isExistPostModerationIcon(siteId), "FAIL -> isExistPostModerationIcon");
        softAssert.assertEquals(operationMySql.getClientsSites().getPostModerationEnabled(siteId), "0", "FAIL -> check post_moderation_enabled in db");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * work 'Post moderation' functional for 2 sites
     * <ul>
     *     <li>site 1: choose 3 random postModeration checkboxes + click save</li>
     *     <li>site 2: open post-moderation form - she should be clear</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test
    public void postModeration_checkTwoSitesOnInterface() {
        log.info("Test is started");
        operationMySql.getClientsSites().updateToDefaultPostModerationSettings(31, siteId);
        operationMySql.getClientsSitesPostModeration().deletePostModerationBySite(31, siteId);
        authCabAndGo("wages/sites?client_id=" + 31);
        pagesInit.getCabWebsites().clickPostModerationIcon(siteId);
        pagesInit.getCabWebsites().chooseRandomPostModerationOptions();
        pagesInit.getCabWebsites().fillPostModerationComment();
        pagesInit.getCabWebsites().clickSavePostModerationForm();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Moderation process saved"), "FAIL -> message 'Moderation process saved'");

        pagesInit.getCabWebsites().clickPostModerationIcon(31);
        pagesInit.getCabWebsites().getPostModerationCustomCheckboxes().clear();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkPostModerationOptions(), "FAIL -> checkPostModerationOptions");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Снимать флаг post_moderation_enabled при блокировке сайта
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24536">TA-24536</a>
     * <p>RKO</p>
     */
    @Test
    public void postModeration_post_moderation_enabled_0_ifBlockSite() {
        log.info("Test is started");
        operationMySql.getClientsSites().updateAntifraudBlocked(0, siteId);
        operationMySql.getClientsSites().updateToDefaultPostModerationSettings(siteId);
        operationMySql.getClientsSitesPostModeration().deletePostModerationBySite(siteId);

        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().blockByAntifrod();
        pagesInit.getCabWebsites().waitUnblockByAntifrodIcon();
        refresh();

        softAssert.assertFalse(pagesInit.getCabWebsites().isExistPostModerationIcon(siteId), "FAIL -> isExistPostModerationIcon");
        softAssert.assertEquals(operationMySql.getClientsSites().getPostModerationEnabled(siteId), "0", "FAIL -> check post_moderation_enabled in db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check postpone site moderation
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27534">Ticket TA-27534</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkPostponeSitePostmoderation() {
        log.info("Test is started");
        int siteId = 2013;
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickPostModerationIcon(siteId);
        pagesInit.getCabWebsites().clickPostponeButton();
        softAssert.assertEquals(pagesInit.getCabWebsites().getCurrentSitesCLicks(), "555",
                "FAIL -> Current site's clicks!");
        pagesInit.getCabWebsites().postponeSiteModeration();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),
                "FAIL -> There is not Success message!");
        softAssert.assertFalse(pagesInit.getCabWebsites().isExistPostModerationIcon(siteId),
                "FAIL -> Post moderation icon is still present!");
        softAssert.assertEquals(operationMySql.getClientsSites().getPostModerationClicks(siteId), "1000",
                "FAIL -> Clicks!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
