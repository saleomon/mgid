package cab.publishers.sites;

import org.testng.annotations.*;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.publisher.variables.PublisherWebSiteVariables.*;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.sitesWagesListUrl;

public class CabWebsiteAdsTxtTests extends TestBase {

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23457">https://youtrack.mgid.com/issue/TA-23457</a>
     * Проверки:
     * 1) добавление  Ads.txt сайту с доступным доменом
     * 2) изменение иконки после добавления Ads.txt
     * 3) появление и содержание попапа с Ads.txt в дашборде на странице ../publishers при первой авторизации после добавления
     * 4) отсутствие попапа Ads.txt в дашборде на странице ../publishers при повторной авторизации
     * 5) информация из Ads.txt сайта в попапе Ads.txt в дашборде на странице ../publishers по кнопке
     * 6) удаление Ads.txt
     * 7) отсутствие попапа Ads.txt в дашборде на странице ../publishers
     * 8) отсуствие информация из Ads.txt сайта в попапе Ads.txt в дашборде на странице ../publishers по кнопке
     * AIA
     */
    @Test
    public void addAdsTxtToReachableWebSite() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_FOR_ADS_TXT_ID);
        log.info("Do preconditions: delete old ads.txt file if it exists.");
        pagesInit.getCabWebsites().deleteAdsTxt(String.valueOf(WEBSITE_MGID_FOR_ADS_TXT_ID));
        log.info("Add new ads.txt file.");
        pagesInit.getCabWebsites().addAdsTxt(String.valueOf(WEBSITE_MGID_FOR_ADS_TXT_ID));
        softAssert.assertTrue(pagesInit.getCabWebsites().checkEditAdsTxtIcon(String.valueOf(WEBSITE_MGID_FOR_ADS_TXT_ID)),
                "FAIL -> edit ads.txt icon!");
        log.info("* Go to Publisher page first time after adding Ads.txt *");
        authDashAndGo(CLIENTS_ADS_TXT_LOGIN, "publisher");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkCustomAdsTxtPopupAndText("new",
                pagesInit.getCabWebsites().getDomain()),
                "FAIL -> First time Ads.txt popup in dashboard!");
        log.info("* Logout from dashboard *");
        logoutDash();
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_FOR_ADS_TXT_ID);
        log.info("* Go to Publisher page second time after adding Ads.txt *");
        authDashAndGo(CLIENTS_ADS_TXT_LOGIN, "publisher");
        softAssert.assertFalse(pagesInit.getWebsiteClass().checkDefaultAdsTxtPopupAndText("new",
                pagesInit.getCabWebsites().getDomain()),
                "FAIL -> Second time Ads.txt popup in dashboard!");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkCustomAdsTxtPopupAndText("by button",
                pagesInit.getCabWebsites().getDomain()),
                "FAIL -> By button Ads.txt popup in dashboard!");
        helpersInit.getAuthorizationHelper().goLinkDashboard("user/signout", Subnets.SubnetType.SCENARIO_MGID);
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_FOR_ADS_TXT_ID);
        log.info("* Delete Ads.txt in Cab *");
        softAssert.assertTrue(pagesInit.getCabWebsites().deleteAdsTxt(String.valueOf(WEBSITE_MGID_FOR_ADS_TXT_ID)),
                "FAIL -> Delete Ads.txt in Cab!");
        log.info("* Go to Publisher page after deleting Ads.txt *");
        authDashAndGo(CLIENTS_ADS_TXT_LOGIN, "publisher");
        softAssert.assertFalse(pagesInit.getWebsiteClass().checkDefaultAdsTxtPopupAndText("new",
                pagesInit.getCabWebsites().getDomain()),
                "FAIL -> First time Ads.txt popup in dashboard after deleting!");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkDefaultAdsTxtPopupAndText("by button",
                pagesInit.getCabWebsites().getDomain()),
                "FAIL -> By button Ads.txt popup in dashboard after deleting!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Checks default values in Ads.txt adding popup and validations
     * <p>Cases:</p>
     * <ul>
     * <li>1) Compare domains in list and in popup</li>
     * <li>2) Check default value of checkboxes in 'Create Ads.txt file?' popup</li>
     * <li>3) Check impossibility of adding Ads.txt without domain name</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23457">Ticket TA-23457</a>
     * <p> Author AIA</p>
     */
    @Test
    public void checkAdsTxtPopupDefaultValuesAndValidations() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + WEBSITE_MGID_WITHOUT_ADS_TXT_ID);
        pagesInit.getCabWebsites().getSiteDomainFromWebsitesList();
        softAssert.assertTrue(pagesInit.getCabWebsites().checkAddAdsTxtPopupDefaultValues(String.valueOf(WEBSITE_MGID_WITHOUT_ADS_TXT_ID)),
                "FAIL - default checkboxes values");
        pagesInit.getCabWebsites().submitAdsTxtPopupWithoutDomain();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> Message after adding ads.txt with empty domain");
        pagesInit.getCabWebsites().closeAddAdsTxtPopup();
        softAssert.assertFalse(pagesInit.getCabWebsites().checkEditAdsTxtIcon(String.valueOf(WEBSITE_MGID_WITHOUT_ADS_TXT_ID)),
                "FAIL -> Add ads.txt with empty domain");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23860">https://youtrack.mgid.com/issue/TA-23860</a>
     * Check presence of add Ads.txt icon depends on websites cooperation types:
     * - 'wages'
     * - 'wages+video
     * - 'exchange+video'
     * - 'exchange'
     * AIA
     */
    @Test
    public void checkCooperationTypeForAdsTxt() {
        log.info("Test is started");
        int websiteId = 18;
        log.info("Check add Ads.txt icon for 'wages' cooperation type");
        operationMySql.getClientsSites().updateCooperationType(websiteId, "wages");
        authCabAndGo(sitesWagesListUrl + websiteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkAddAdsTxtIcon(websiteId),
                "FAIL -> 'Wages', icon is absent!");
        log.info("Check add Ads.txt icon for 'wages+video' cooperation type");
        operationMySql.getClientsSites().updateCooperationType(websiteId, "wages\", \"video");
        authCabAndGo(sitesWagesListUrl + websiteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkAddAdsTxtIcon(websiteId),
                "FAIL -> 'Wages + Video', icon is absent!");
        log.info("Check add Ads.txt icon for 'exchange+video' cooperation type");
        authCabAndGo(sitesWagesListUrl + websiteId);
        operationMySql.getClientsSites().updateCooperationType(websiteId, "exchange\", \"video");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkAddAdsTxtIcon(websiteId),
                "FAIL -> 'Exchange + Video', icon is absent!");
        log.info("Check add Ads.txt icon for 'exchange' cooperation type");
        operationMySql.getClientsSites().updateCooperationType(websiteId, "exchange");
        authCabAndGo(sitesWagesListUrl + websiteId);
        softAssert.assertFalse(pagesInit.getCabWebsites().checkAddAdsTxtIcon(websiteId),
                "FAIL -> 'Exchange', icon is absent!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check the current Ads.txt state
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24562">Ticket TA-24562</a>
     *
     * <p>Author AIA</p>
     */
    @Test
    public void checkCurrentStateOfAdsTxt() {
        log.info("Test is started");
        String websiteId = "2007";

        authCabAndGo(sitesWagesListUrl + websiteId);
        pagesInit.getCabWebsites().openEditAdsTxtPopup(websiteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkShowDashboardPopupHidden(),
                "FAIL -> 'Show dashboard popup' is not hidden!");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkSendMailOnScheduleHidden(),
                "FAIL -> 'Send Mail on schedule' is not hidden!");
        softAssert.assertEquals(pagesInit.getCabWebsites().checkMissingLines(), "",
                "FAIL -> 'Missing lines' are not empty!");
        pagesInit.getCabWebsites().saveAdsTxt();
        pagesInit.getCabWebsites().openEditAdsTxtPopup(websiteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkSendMailOnScheduleSelected(),
                "FAIL -> 'Show dashboard popup' is not selected or disabled!");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkShowDashboardPopupSelected(),
                "FAIL -> 'Send Mail on schedule' is not selected or disabled!");
        softAssert.assertEquals(pagesInit.getCabWebsites().checkMissingLines(),
                adsTxtMissingLines,
                "FAIL -> 'Missing lines' are incorrect!");
        authDashAndGo("for.ads.txt.publisher.2021@mgid.com", "publisher");
        softAssert.assertTrue(pagesInit.getWebsiteClass().checkCustomAdsTxtPopupAndText("new", "test-with-ads-txt.com"),
                "FAIL -> show popup in dashboard!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check the Ads.txt content
     *
     * @see <a href="https://youtrack.mgid.com/issue/VID-2054">Ticket VID-2054</a>
     *
     * <p>Author AIA</p>
     */
    @Test
    public void checkAdsTxtContent() {
        log.info("Test is started");
        authDashAndGo(CLIENTS_ADS_TXT_LOGIN, "publisher");
        pagesInit.getWebsiteClass().openAdsTxtPopup();
        softAssert.assertEquals(pagesInit.getWebsiteClass().getAdsTxtContent("test-site-with-ads-txt.com"),
                adsTxtSitesContent,
                "FAIL -> Simple Ads.txt sites content!");
        softAssert.assertTrue(pagesInit.getWebsiteClass().
                        getAdsTxtContent("test-site-with-ads-txt-combined.com").contains(adsTxtSitesContent),
                "FAIL -> Combined Ads.txt sites content!");
        softAssert.assertTrue(pagesInit.getWebsiteClass().
                        getAdsTxtContent("test-site-with-ads-txt-combined.com").contains(adsTxtNetworksContent),
                "FAIL -> Combined Ads.txt networks content!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
