package cab.publishers.sites;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.ArrayList;
import java.util.List;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.goodWidgetSettingUrl;
import static testData.project.EndPoints.sitesWagesEditUrl;

public class CabWebsiteCategoryFilterTests extends TestBase {

    private ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>();
    int siteId = 100;

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("End-To-End -> set up category for site and check copy them to new widget")
    @Test
    public void categoryFilterEnd2End() {
        log.info("Test is started");
        authCabAndGo(sitesWagesEditUrl + siteId);
        pagesInit.getCabWebsites().clearAllSelectedCategoryInMultiFilter();
        pagesInit.getCabWebsites().chooseCategoriesFilterType("1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabWebsites().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabWebsites().saveEditWebsite();

        log.info("create new widget for this site");
        authDashAndGo("testEmail63@ex.ua", "publisher/add-widget/site/" + siteId);
        pagesInit.getWidgetClass().createSimpleWidget();

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId()));
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface));
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Site category don't copy to Impact widget")
    @Test
    public void categoryFilterDontCopyToImpact() {
        log.info("Test is started");
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface.add("Careers");
        chooseCategoryInEditInterface.add("Events and Attractions");
        chooseCategoryInEditInterface.add("Society");

        log.info("create new widget for this site");
        authDashAndGo("testEmail63@ex.ua", "publisher/add-widget/site/" + 101);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId()));
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().isSelectedCustomCheckboxInCategoryFilter("Content promotions"), "Fail -> 'Content promotions' - is selected");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().isSelectedCustomCheckboxInCategoryFilter("Product promotions"), "Fail -> 'Product promotions' - is selected");
        softAssert.assertFalse(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> not equals categories");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Site category don't copy to child widget. Check copy category from parent widget")
    @Test
    public void categoryFilterCreateChild() {
        log.info("Test is started");
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface.add("Careers");
        chooseCategoryInEditInterface.add("Events and Attractions");
        chooseCategoryInEditInterface.add("Society");

        log.info("create new widget for this site");
        authDashAndGo("testEmail63@ex.ua", "publisher/widgets/site/" + 101);
        pagesInit.getWidgetClass().createSubwidget("363");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getCloneId()));
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        Assert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> not equals categories");
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Site category don't copy to child AB widget. Check copy category from parent AB widget")
    @Test
    public void categoryFilterCreateAbChild() {
        log.info("Test is started");
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface.add("Careers");
        chooseCategoryInEditInterface.add("Events and Attractions");
        chooseCategoryInEditInterface.add("Society");

        log.info("create new widget for this site");
        authDashAndGo("testEmail63@ex.ua", "publisher/widgets/site/" + 101);
        pagesInit.getWidgetClass().addAbTestWidget(364);
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getCloneId()));
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        Assert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> not equals categories");
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Check work search in site category")
    @Test
    public void categoryFilterCheckSearchCategoryInFilter() {
        log.info("Test is started");
        authCabAndGo(sitesWagesEditUrl + siteId);
        pagesInit.getCabWebsites().chooseCategoriesFilterType("1");
        Assert.assertTrue(pagesInit.getCabWebsites().checkSearchCategoryFilter());
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Check edit category filter from only to except")
    @Test
    public void categoryFilterEditOnlyOnExcept() {
        log.info("Test is started");
        insertCategoryFilterForWidget(1);
        checkCategoryFilter("2");
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Check edit category filter from except to only")
    @Test
    public void categoryFilterEditExceptOnOnly() {
        log.info("Test is started");
        insertCategoryFilterForWidget(2);
        checkCategoryFilter("1");
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Check edit category filter from except to no")
    @Test
    public void categoryFilterEditExceptOnNo() {
        log.info("Test is started");
        insertCategoryFilterForWidget(2);
        authCabAndGo(sitesWagesEditUrl + siteId);
        pagesInit.getCabWebsites().chooseCategoriesFilterType("0");
        pagesInit.getCabWebsites().saveEditWebsite();
        authCabAndGo(sitesWagesEditUrl + siteId);
        softAssert.assertFalse(pagesInit.getCabWebsites().categoryFilterBlockIsDisplayed(), "FAIL -> category filter is visible");
        softAssert.assertEquals(operationMySql.getClientsSites().getDefaultWidgetCategoriesFilter(siteId), "0", "FAIL -> use_cat=1");
        softAssert.assertFalse(operationMySql.getClientsSitesDefaultWidgetCategories().isExistCategoryInWidget(siteId), "FAIL -> categories is exist");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Set up new category filter with Ad types and Landing types")
    @Test(dataProvider = "categoryFilterValue")
    public void categoryFilterExceptOnlyAdTypesAndLanding(String val, String categoryFilterVal) {
        log.info("Test is started: " + val);

        log.info("choose adTypes and landingTypes in Category Filter");
        authCabAndGo(sitesWagesEditUrl + siteId);
        pagesInit.getCabWebsites().clearAllSelectedCategoryInMultiFilter();
        pagesInit.getCabWebsites().chooseCategoriesFilterType(categoryFilterVal);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabWebsites().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabWebsites().saveEditWebsite();

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(sitesWagesEditUrl + siteId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        Assert.assertTrue(pagesInit.getCabWebsites().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface));
        log.info("Test is finished: " + val);
    }

    @DataProvider
    public Object[][] categoryFilterValue() {
        return new Object[][]{
                {"1", "1"},
                {"2", "2"}
        };
    }

    @Feature("MVP. Set up Category filter & Publishers filter for a website")
    @Story("Set up Category filter for website")
    @Description("Category filter don't show for sites(3: exchange; 4: exchange,video; 89: exchange,int_exchange) without wages type")
    @Test(dataProvider = "dataCategoryFilterShowOnlyWages")
    public void categoryFilterShowOnlyWages(int siteId) {
        log.info("Test is started");
        authCabAndGo(sitesWagesEditUrl + siteId);
        Assert.assertFalse(pagesInit.getCabWebsites().isDisplayedCategoriesFilter());
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataCategoryFilterShowOnlyWages(){
        return new Object[][]{
                {3},
                {4},
                {89}
        };
    }

    private void checkCategoryFilter(String filterValue) {
        authCabAndGo(sitesWagesEditUrl + siteId);
        pagesInit.getCabWebsites().clearAllSelectedCategoryInMultiFilter();
        pagesInit.getCabWebsites().chooseCategoriesFilterType(filterValue);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        chooseCategoryInEditInterface = helpersInit.getMultiFilterHelper().chooseCustomValueInMultiFilterAndGetTheirData(2);
        pagesInit.getCabWebsites().saveEditWebsite();
        authCabAndGo(sitesWagesEditUrl + siteId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkCategoriesFilterTypeValue(filterValue), "FAIL -> categoryFilter_Only UI");
        List<String> clientCategories = operationMySql.getClientsSitesDefaultWidgetCategories().getSelectedCategoriesNames(siteId);
        clientCategories.forEach(i-> log.info("clientCategories: " + i));
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(clientCategories),"FAIL -> not equals categories");
        softAssert.assertEquals(operationMySql.getClientsSites().getDefaultWidgetCategoriesFilter(siteId), filterValue, "FAIL -> default_widget_categories_filter=1");
        softAssert.assertAll();
    }

    private void insertCategoryFilterForWidget(int filterValue) {
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface.add("Automotive");
        operationMySql.getClientsSitesDefaultWidgetCategories().deleteAllCategories(siteId);
        operationMySql.getClientsSitesDefaultWidgetCategories().insertWidgetCategories(siteId);
        operationMySql.getClientsSites().updateDefaultWidgetCategoriesFilter(siteId, filterValue);
    }
}
