package cab.publishers.sites;

import org.testng.annotations.Test;
import pages.cab.publishers.variables.DfpPublisher.DfpValues;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static testData.project.EndPoints.sitesWagesListUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabWebsiteDfpTests extends TestBase {

    private final int siteId = 61;
    private final int otherSiteTheSameClient = 62;
    private final int clientId = 35;
    private final String domain = "testsite62.com";
    private final int widgetId_1 = 106;
    private final int widgetId_2 = 107;

    /**
     * DFP | Blacklisted domains flag. Флаг DFP | Сайты
     * Проверка проставление флага только одному сайту
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22314">TA-22314</a>
     */
    @Test
    public void checkSetRemoveGoogleAddManager() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + siteId);
        log.info("Устанавливаем флаг сайта");
        pagesInit.getCabWebsites().switchDfpIcon(DfpValues.ON, siteId);

        log.info("Проверяем состояние иконки");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDfpIcon(DfpValues.ON, siteId), "FAIL -> turnOn dfp");

        log.info("Проверяем что иконка проставилась только одному сайту");
        authCabAndGo("wages/sites/client_id/" + clientId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDfpIcon(DfpValues.ON, siteId), "FAIL -> check dfp flag by client -> turnOn icon");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDfpIcon(DfpValues.OFF, otherSiteTheSameClient), "FAIL -> check dfp flag by client -> turnOff icon");

        log.info("Убираем флаг сайта");
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().switchDfpIcon(DfpValues.OFF, siteId);

        log.info("Проверяем состояние иконки");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDfpIcon(DfpValues.OFF, siteId), "FAIL -> turnOff dfp");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * DFP | Blacklisted domains flag. Флаг DFP | Сайты
     * Проверка работы права
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22314">TA-22314</a>
     */
    @Privilege(name = "set-site-dfp-flag")
    @Test
    public void checkRightLowPriorityFilter(){
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/set-site-dfp-flag");
        authCabForCheckPrivileges(sitesWagesListUrl + siteId);
        softAssert.assertFalse(pagesInit.getCabWebsites().isExistDfpIcon(siteId), "FAIL -> website dfp icon");

        authCabAndGo("wages/widgets/?domain=" + domain);
        softAssert.assertFalse(pagesInit.getCabWidgetsList().isExistDfpIcon(siteId), "FAIL -> widget dfp icon");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * DFP | Blacklisted domains flag. Флаг DFP | Виджеты
     * <ul>
     *     <li>site -> set flag in site</li>
     *     <li>widgets -> check inherit flag in widgets</li>
     *     <li>widgets -> remove dfp flag in one widget</li>
     *     <li>site -> check new state in site 'Google Ad manager flag set to some widgets on the website'</li>
     *     <li>site -> click icon 'Google Ad manager flagg set to some widgets on the website'</li>
     *     <li>widgets -> check all widgets dfp flag ON</li>
     *     <li>site -> switch off dfp flag</li>
     *     <li>widgets -> check all widgets dfp flag OFF</li>
     * </ul>
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22859">TA-22859</a>
     */
    @Test
    public void checkSetRemoveGoogleAddManager_widgets() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + siteId);
        log.info("Устанавливаем флаг сайта");
        pagesInit.getCabWebsites().switchDfpIcon(DfpValues.ON, siteId);

        log.info("проверяем что иконка унаследовалась всем виджетам сайта");
        authCabAndGo("wages/widgets/?domain=" + domain);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.ON, widgetId_1), "FALSE: inherit widgetId_1 turnOn");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.ON, widgetId_2), "FALSE: inherit widgetId_2 turnOn");

        log.info("отключаем dfp флаг для одного из виджетов");
        pagesInit.getCabWidgetsList().switchDfpIcon(DfpValues.OFF, widgetId_1);

        log.info("проверяем что у сайта сменилась иконка на 'Google Ad manager flag set to some widgets on the website'");
        authCabAndGo(sitesWagesListUrl + siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().checkDfpIcon(DfpValues.PART, siteId), "FALSE: Google Ad manager flag don't have need state");

        log.info("Убираем флаг сайта 'Google Ad manager flag set to some widgets on the website' на 'Remove as Google Ad manager website'");
        pagesInit.getCabWebsites().switchDfpIcon(DfpValues.ON, siteId);

        log.info("проверяем что иконка вернулась всем виджетам сайта WEB_SITE_MGID_WAGES_ID");
        authCabAndGo("wages/widgets/?domain=" + domain);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.ON, widgetId_1), "FALSE: widgetId_1 return turnOn");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.ON, widgetId_2), "FALSE: widgetId_2 return turnOn");

        log.info("Убираем флаг сайта 'Remove as Google Ad manager website' на 'Set as Google Ad manager website'");
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().switchDfpIcon(DfpValues.OFF, siteId);

        log.info("проверяем что иконка 'Google Ad manager' снялась у всех виджетов");
        authCabAndGo("wages/widgets/?domain=" + domain);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.OFF, widgetId_1), "FALSE: inherit widgetId_1 turnOff");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpValues.OFF, widgetId_2), "FALSE: inherit widgetId_2 turnOff");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
