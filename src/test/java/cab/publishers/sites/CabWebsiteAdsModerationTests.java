package cab.publishers.sites;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.Map;

import static testData.project.EndPoints.sitesWagesListUrl;

public class CabWebsiteAdsModerationTests extends TestBase {

    @Feature("Ads Premoderation")
    @Story("Cab | Enabling publisher ad moderation")
    @Description("Add config for site without widgets <a href='https://jira.mgid.com/browse/KOT-2931'>KOT-2931</a>")
    @Owner("RKO")
    @Test(description = "Add config for site without widgets")
    public void premoderationForSiteWithoutWidgets() {
        log.info("Test is started");
        authCabAndGo(sitesWagesListUrl + 123);
        pagesInit.getCabWebsites().clickAdsModerationIcon(123);
        softAssert.assertEquals(pagesInit.getCabWebsites().getAdsModerationFieldsetConfig(), "There is no specified configuration", "FAIL -> Configuration text");
        softAssert.assertEquals(pagesInit.getCabWebsites().getAdsModerationFieldsetNewConfig(), "Site should contains at least one widget", "FAIL -> Adding new config text");
        softAssert.assertFalse(pagesInit.getCabWebsites().isDisplayedAdsModerationDeleteAds(), "FAIL -> isDisplayedAdsModerationDeleteAds");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Cab | Enabling publisher ad moderation")
    @Description("Add config for site with widgets <a href='https://jira.mgid.com/browse/KOT-2931'>KOT-2931</a>")
    @Owner("RKO")
    @Test(description = "Add new config")
    public void premoderationAddNewConfig() {
        log.info("Test is started");
        int siteId = 58;
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        pagesInit.getCabWebsites().adsModerationAddNewConfiguration(true, "14",4);
        softAssert.assertFalse(pagesInit.getCabWebsites().isDisplayedAdsModerationDeleteAds(), "FAIL -> isDisplayedAdsModerationDeleteAds");
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().adsModerationCheckNewConfiguration(true, "14"));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Cab | Enabling publisher ad moderation <a href='https://jira.mgid.com/browse/KOT-2931'>KOT-2931</a>")
    @Description("Edit config")
    @Owner("RKO")
    @Test(description = "Edit config")
    public void premoderationEditConfig() {
        log.info("Test is started");
        int siteId = 76;
        pagesInit.getCabWebsites().setAdsModeration(Map.of("Country: Georgia", "Widget: [281] title_281 (under-article-widget)",
                "Country: Cyprus", "Widget: [283] title_283 (under-article-widget)"));

        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        pagesInit.getCabWebsites().adsModerationEditConfiguration(true, "11",29);
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        Assert.assertTrue(pagesInit.getCabWebsites().adsModerationCheckNewConfiguration(true, "11"));
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Cab | Enabling publisher ad moderation <a href='https://jira.mgid.com/browse/KOT-2931'>KOT-2931</a>")
    @Description("Delete all configs")
    @Owner("RKO")
    @Test(description = "Delete all configs")
    public void premoderationDeleteAllConfigs() {
        log.info("Test is started");
        int siteId = 93;
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        pagesInit.getCabWebsites().adsModerationEditConfiguration(false, "11",15, 29, 51);
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        Assert.assertEquals(pagesInit.getCabWebsites().getAdsModerationFieldsetConfig(), "There is no specified configuration");
        log.info("Test is finished");
    }

    @Feature("Ads Premoderation")
    @Story("Cab | Enabling publisher ad moderation <a href='https://jira.mgid.com/browse/KOT-2931'>KOT-2931</a>")
    @Description("Validation auto-approve hours")
    @Owner("RKO")
    @Test(description = "Validation auto-approve hours", dataProvider = "dataPremoderationValidationAutoApproveHours")
    public void premoderationValidationAutoApproveHours(String title, String hours) {
        log.info("Test is started: " + title);
        int siteId = 1;
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        pagesInit.getCabWebsites().adsModerationAddNewConfiguration(true, hours,1);
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The value of \"hours\" must be an integer between 1 and 999"));
        log.info("Test is finished: " + title);
    }

    @DataProvider
    public Object[][] dataPremoderationValidationAutoApproveHours(){
        return new Object[][]{
                {"case 1", "1.7"},
                {"case 2", "abc"},
                {"case 3", "%#7"}
        };
    }

    @Feature("Ads Premoderation")
    @Story("Remove publishers ads moderation option")
    @Description("Delete all ads <a href='https://jira.mgid.com/browse/TA-52147'>TA-52147</a>")
    @Owner("RKO")
    @Test(description = "Delete all ads")
    public void premoderationDeleteAllAds() {
        log.info("Test is started");
        int siteId = 87;
        authCabAndGo(sitesWagesListUrl + siteId);
        pagesInit.getCabWebsites().clickAdsModerationIcon(siteId);
        softAssert.assertTrue(pagesInit.getCabWebsites().isDisplayedAdsModerationDeleteAds(), "FAIL -> isDisplayedAdsModerationDeleteAds");
        pagesInit.getCabWebsites().clickAdsModerationDeleteAds();
        softAssert.assertEquals(pagesInit.getCabWebsites().getAdsModerationDeletePopupText(), "Are you going to clear up all ads from moderation?", "FAIL -> Adding new config text");
        pagesInit.getCabWebsites().submitAdsModerationDeleteAds();
        softAssert.assertFalse(operationMySql.getPublishersAdsModeration().selectIsExistPublishersAds(siteId), "FAIL -> count PublishersAds in db != 0");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
