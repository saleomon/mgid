package cab.publishers;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.AuthUserCabData.*;

public class CabExchangeCampaignsTests extends TestBase {

    /**
     * create exchange campaign
     * check adding ads.txt after creating of the website
     * @see <a href="https://jira.mgid.com/browse/TA-52423">Ticket TA-52423</a>"
     * RKO
     */
    @Test
    public void createWebSites() {
        log.info("Test is started");
        log.info("create new exchange webSite");
        authCabAndGo("wages/sites-new/client_id/" + 3);
        Assert.assertNotNull(pagesInit.getCabWebsites().setCooperationType("exchange").createWebsite(), "webSite doesn't create");
        log.info("check ads.txt in list interface");
        softAssert.assertTrue(pagesInit.getCabWebsites().checkEditAdsTxtIcon(pagesInit.getCabWebsites().getWebsiteId()),
                "FAIL -> edit ads.txt icon!");
        log.info("check after create webSite -> created exchange campaign");
        authCabAndGo("exchange/partners?site_id=" + pagesInit.getCabWebsites().getWebsiteId());
        Assert.assertNotNull(pagesInit.getCabExchangeCampaigns().readCampaignId(), "exchange campaign doesn't create");

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23941">...</a>
     * Внедрение нового фильтра по куратору для `exchange/partners`
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     * RKO
     */
    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator = helpersInit.getBaseHelper().getRandomFromArray(new String[]{indiaPublisherUser, apacPublisherUser, cisPublisherUser, europePublisherUser});
        authCabAndGo("exchange/partners");
        log.info("set chosen curator and search campaigns");
        Assert.assertTrue(pagesInit.getCabExchangeCampaigns().checkWorkCuratorFilter(curator), "FAIL -> campaigns don't search by curator " + curator);
        log.info("Test is finished");
    }
}
