package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

public class CabPublisherClientsMassActionTest extends TestBase {
    /**
     * Проверка отображения опции 'Выбрать все' при выборе массовых действий
     * @see <a href="https://youtrack.mgid.com/issue/TA-27492">Ticket TA-27492</a>
     * <p>NIO</p>
     */
    @Test(dataProvider = "maxActionValues")
    public void checkSelectAllOptionVisibility(String massActionName) {
        log.info("Test is started");
        authCabAndGo("wages/clients?login=test11&subnet=%7B\"subnet\"%3A0%2C\"mirror\"%3Anull%7D");
        pagesInit.getCabPublisherClients().markCheckbox(true);
        pagesInit.getCabPublisherClients().selectMassAction(massActionName);
        log.info("Check selected all option for - " + massActionName);
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkVisibilitySelectAllOption(), massActionName);
        log.info("Test is finished");
    }

    @DataProvider
    public static Object[][] maxActionValues() {
        return new Object[][]{
                {"wages_change_curator"},
                {"change_mediabuyer"},
                {"disable_bonus_media_buyer"},
                {"enable_bonus_media_buyer"}
        };
    }

    /**
     * Check visibility of mass action without filter by subnet
     * @see <a href="https://youtrack.mgid.com/issue/TA-27492">Ticket TA-27492</a>
     * <p>NIO</p>
     */
    @Test
    public void checkMassOptionVisibilityFilteredBySubnet() {
        log.info("Test is started");
        authCabAndGo("wages/clients?login=test11");
        pagesInit.getCabPublisherClients().markCheckbox(true);
        log.info("Check visibility of actions");
        Assert.assertFalse(pagesInit.getCabPublisherClients().isPresentMassAction("enable_bonus_media_buyer"));
        log.info("Test is finished");
    }

    /**
     * Проверка работы опции 'Выбрать все' при выборе массовых действий
     * @see <a href="https://youtrack.mgid.com/issue/TA-27492">Ticket TA-27492</a>
     * <p>NIO</p>
     */
    @Test
    public void checkWorkingSelectAllOption() {
        log.info("Test is started");
        authCabAndGo("wages/clients?login=test11");
        pagesInit.getCabPublisherClients().markCheckbox(true);
        pagesInit.getCabPublisherClients().selectMassAction("wages_change_curator");
        pagesInit.getCabPublisherClients().clickSelectAll();
        pagesInit.getCabPublisherClients().selectCuratorAndApply("yoka.ono");
        log.info("Check selected all option for wages_change_curator");
        helpersInit.getBaseHelper().amountOfDisplayedEntitiesPerPage("50");
        Assert.assertEquals(pagesInit.getCabPublisherClients().getAmountManagerLabel("yoka.ono"), 26);
        log.info("Test is finished");
    }
}
