package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.CLIENT_ADSKEEPER_PAYOUTS_ID;
import static testData.project.ClientsEntities.CLIENT_MGID_PAYOUTS_ID;
import static testData.project.EndPoints.clientWagesEditUrl;

// 12
public class CabPublisherClientsPayoutsTests_2 extends TestBase {

    /**
     * Client's payout region
     * Если у клиента есть хоть один не удаленный кошелек (кроме InterTipalti MGID Asia, USD (id=222)), то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен cо значением "Asia";</li>
     * <li>в тултипе при наведении выводится подсказка: "You have already added wallets"</li>.
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void clientPayoutRegionForClientWithMultipleWalletsInterTipalti() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2014");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Selected value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(), "You have already added wallets",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть хоть один не удаленный кошелек (
     * кроме InterTipalti MGID Asia (id=222), Webmoney MIRS Ltd, USD (id=83) и PayPal MGID Asia, USD (id=223)), то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен cо значением "Asia";</li>
     * <li>в тултипе при наведении выводится подсказка: "You have already added wallets"</li>.
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24497>Ticket TA-24497</a>
     * <p>Author AIA</p>
     */
    @Test
    public void clientPayoutRegionForClientWithMultipleWalletsPayPal() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2020");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Selected value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(), "You have already added wallets",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть кошелек InterTipalti MGID Asia, USD (id = 222) и одобренная заявка, то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен cо значением "Asia";</li>
     * <li>в тултипе при наведении выводится подсказка:
     * "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets
     * of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\";</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionForPurseInterTipalti222WithApprovedPayments() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2011");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Selected value in 'Client payout region'!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(),
                "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets " +
                        "of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\"",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть кошелек PayPal MGID Inc, USD (id = 89) и одобренная заявка, то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен c дефолтным значением "Not selected";</li>
     * <li>в тултипе при наведении выводится подсказка:
     * "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets
     * of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\"";</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24497>Ticket TA-24497</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionForPursePayPal89WithApprovedPayments() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2018");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Not selected",
                "FAIL -> Selected value in 'Client payout region'!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(),
                "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets" +
                        " of InterTipalti MGID Inc, USD / InterTipalti MGID Asia / PayPal MGID Inc, USD/ " +
                        "PayPal MGID Asia, USD in the status of \"approved\"",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть кошелек PayPal MGID Asia, USD (id = 223) и одобренная заявка, то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен cо значением "Asia";</li>
     * <li>в тултипе при наведении выводится подсказка:
     * "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets
     * of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\";</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24497>Ticket TA-24497</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionForPursePayPal223WithApprovedPayments() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2019");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Selected value in 'Client payout region'!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(),
                "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets" +
                        " of InterTipalti MGID Inc, USD / InterTipalti MGID Asia / PayPal MGID Inc, USD/ " +
                        "PayPal MGID Asia, USD in the status of \"approved\"",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check payout limit for Payoneer purse
     *
     * @see <a href='https://jira.mgid.com/browse/TA-51751'>Ticket TA-51751</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkLimitForPayoutsOnPayoneer() {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + 2034);
        pagesInit.getCabPublisherClients().setRequestAmountForClient("Payoneer", "49");
        pagesInit.getCabPublisherClients().submitPayoutScheme();
        Assert.assertEquals(helpersInit.getBaseHelper().getTextFromAlert(), "Required payout sum is less than minimal allowed ");
        log.info("Test is finished");
    }

    /**
     * Add wallet Payoneer IDAA, USD (id=195) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51753">Ticket TA-51753</a>
     * <p>AIA</p>
     */
    @Test
    public void addPayoneerPurseForAdskeeperClient() {
        log.info("Test is started");
        String purseId = "195";
        String purseName = "Payoneer IDAA, USD";
        String clientsPurse = "sok.autotest-payoneer_cab+123!#$%&*/=@test-123.com.ua";
        authCabAndGo(clientWagesEditUrl + CLIENT_ADSKEEPER_PAYOUTS_ID);
        log.info("Check if exist Payoneer purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("Add new Payoneer purse");
        pagesInit.getCabPublisherClients().addPayoneerPurse(purseId, clientsPurse);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Payoneer' doesn't created");
        log.info("Check Payoneer details");
        Assert.assertTrue(pagesInit.getCabPublisherClients().
                        checkPayoneerPurse(purseName, clientsPurse),
                "FAIL -> Payoneer details!");
        log.info("Delete Payoneer purse");
        Assert.assertTrue(pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName),
                "FAIL -> deleting Payoneer purse!");
        log.info("Test is finished");
    }

    /**
     * Add incorrect wallet value for Payoneer IDAA, USD (id=195) and check validations
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51753">Ticket TA-51753</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "wrongPayoneerPurseValues")
    public void checkValidationsForPayoneerPurse (String clientPurseValue) {
        log.info("Test is started");
        String purseId = "195";
        String purseName = "Payoneer IDAA, USD";
        authCabAndGo(clientWagesEditUrl + CLIENT_ADSKEEPER_PAYOUTS_ID);
        log.info("Check if exist Payoneer purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("Add new Payoneer purse");
        pagesInit.getCabPublisherClients().addPayoneerPurse(purseId, clientPurseValue);

        Assert.assertNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Payoneer' doesn't created");
        Assert.assertEquals(pagesInit.getCabPublisherClients().getValidationError(),
                "'" + clientPurseValue + "'" + " is not valid email address in the basic format local-part@hostname",
                "FAIL -> error in validation ");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] wrongPayoneerPurseValues() {
        return new Object[][]{
                {"emai‘l@test.com"},
                {"emai,l@test.com"},
                {"emai—l@test.com"},
                {"email?@test"},
                {"emai^l@test"},
                {"emai`l@test"},
                {"emai{l@test"},
                {"emai|l@test"},
                {"emai}l@test"},
                {"emai~l@test"},
                {"emailtest.com"},
                {"e mail@test.com"},
                {"email@te_st.com"},
                {"email@te st.com"},
                {"@test.com"},
                {"email@"},
                {"email@test.c"},
                {"email@тест.ком"},
                {"email..email@test.com"},
                {"email.email@test..com"},
                {"email.email@test.com."},
                {"email.email@.test.com"},
                {".email@test.com"},
                {"email.@test.com"},
                {"emailemailemailemailemailemailemailemailemailemailemailemailemailemailemailemail@test.com"},
                {"email@emailemailemailemailemailemailemailemailemailemailemailemailtestemail.com"},
                {"email@test.emailemailemailemailemailemailemailemailemailemailemailemailemailcom"}
        };
    }

    /**
     * Check payout limit for Capitalist purse
     *
     * @see <a href='https://jira.mgid.com/browse/TA-51756'>Ticket TA-51756</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkLimitForPayoutsOnCapitalist() {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + 2034);
        pagesInit.getCabPublisherClients().setRequestAmountForClient("Capitalist", "49");
        pagesInit.getCabPublisherClients().submitPayoutScheme();
        Assert.assertEquals(helpersInit.getBaseHelper().getTextFromAlert(), "Required payout sum is less than minimal allowed ");
        log.info("Test is finished");
    }

    /**
     * Add wallet Capitalist IDAA, USD (id=201) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51755">Ticket TA-51755</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addCapitalistIDAAUsdPurse() {
        log.info("Test is started");
        String purseId = "201";
        String purseName = "Capitalist IDAA, USD";
        String purseNumber = "U12345678";
        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);
        log.info("Check if exist Capitalist IDAA, USD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("add new Capitalist IDAA, USD purse");
        pagesInit.getCabPublisherClients().addCapitalistIdaaUsdPurse(purseId, purseNumber);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Capitalist IDAA, USD' doesn't created");
        log.info("check Capitalist IDAA, USD detail");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkCapitalistIdaaUsdPurseDetail(purseNumber, purseName),
                "FAIL -> Capitalist IDAA, USD details!");
        log.info("Delete Capitalist IDAA, USD purse");
        Assert.assertTrue(pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName),
                "FAIL - deleting Capitalist IDAA USD Purse!");
        log.info("Test is finished");
    }

    /**
     * Check validations for wallet Capitalist IDAA, USD (id=201)
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51755">Ticket TA-51755</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "wrongCapitalistIdaaUsdPurseNumbers")
    public void checkCapitalistIDAAUsdPurseValidations(String purseNumber) {
        log.info("Test is started");
        String purseId = "201";
        String purseName = "Capitalist IDAA, USD";
        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);
        log.info("Check if exist Capitalist IDAA, USD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("add new Capitalist IDAA, USD purse");
        pagesInit.getCabPublisherClients().addCapitalistIdaaUsdPurse(purseId, purseNumber);
        Assert.assertNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Capitalist IDAA, USD' doesn't created");
        log.info("check Capitalist IDAA, USD validation error text");
        Assert.assertEquals(pagesInit.getCabPublisherClients().getCapitalistIdaaUsdErrorMessage(),
                "Invalid Capitalist account number, it should be U12345678 format",
                "FAIL -> message isn't correct");
        log.info("Delete Capitalist IDAA, USD purse");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] wrongCapitalistIdaaUsdPurseNumbers() {
        return new Object[][]{
                {"123456789"},
                {"UUUUUUUUU"}
        };
    }
}
