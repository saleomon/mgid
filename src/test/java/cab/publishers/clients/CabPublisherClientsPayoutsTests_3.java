package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.HashMap;

import static core.helpers.BaseHelper.randomNumbersInt;
import static testData.project.ClientsEntities.CLIENTS_PAYOUTS_LOGIN;
import static testData.project.ClientsEntities.CLIENT_MGID_PAYOUTS_ID;
import static testData.project.EndPoints.clientWagesEditUrl;

// 15
public class CabPublisherClientsPayoutsTests_3 extends TestBase {

    public CabPublisherClientsPayoutsTests_3() {
        clientLogin = CLIENTS_PAYOUTS_LOGIN;
    }


    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO:</p>
     * <ul>
     *    <li>209 - Capitalist MIRS WORLD Card, USD</li>
     *    <li>214 - Capitalist IDAA WORLD Card, USD</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void addCapitalistWorldPurse() {
        log.info("Test is started");
        log.info("fill key-value map needed FO");
        HashMap<String, String> purses = new HashMap<>();
        purses.put("209", "Capitalist MIRS WORLD Card, USD");
        purses.put("214", "Capitalist IDAA WORLD Card, USD");

        log.info("get random purse(FO)");
        int temp = randomNumbersInt(purses.size());
        String pursesFo = purses.keySet().toArray()[temp].toString();
        String pursesName = purses.values().toArray()[temp].toString();

        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);

        log.info("check if exist Capitslist WORLD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        log.info("add new Capitalist WORLD purse");
        pagesInit.getCabPublisherClients().addCapitalistWorldPurse(pursesFo);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(pursesName),
                "FAIL -> purse 'Capitalist WORLD' doesn't created");

        log.info("check Capitalist WORLD detail");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkCapitalistWorldPurseDetail(pursesName),
                "FAIL -> Capitalist World details!");

        log.info("Delete Capitslist WORLD purse");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        log.info("Test is finished");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO:</p>
     * <ul>
     *    <li>207 - Capitalist MIRS UA Card, USD</li>
     *    <li>208 - Capitalist MIRS RU Card, RUB</li>
     *    <li>212 - Capitalist IDAA UA Card, USD</li>
     *    <li>213 - Capitalist IDAA RU Card, RUB</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void addCapitalistPurse() {
        log.info("Test is started");
        log.info("fill key-value map needed FO");
        HashMap<String, String> purses = new HashMap<>();
        purses.put("207", "Capitalist MIRS UA Card, USD");
        purses.put("208", "Capitalist MIRS RU Card, RUB");
        purses.put("212", "Capitalist IDAA UA Card, USD");
        purses.put("213", "Capitalist IDAA RU Card, RUB");

        log.info("get random purse(FO)");
        int temp = randomNumbersInt(purses.size());
        String pursesFo = purses.keySet().toArray()[temp].toString();
        String pursesName = purses.values().toArray()[temp].toString();

        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);

        log.info("check if exist Capitslist purse -> delete his");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        log.info("add new Capitalist purse");
        pagesInit.getCabPublisherClients().addCapitalistPurse(pursesFo);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(pursesName),
                "FAIL -> 'Capitalist' wallet not created!");

        log.info("check Capitalist detail");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkCapitalistPurseDetail(pursesName));

        log.info("Delete his Capitalist purse");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        log.info("Test is finished");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO:</p>
     * <p>209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation empty</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurse_AllFieldsEmpty() {
        log.info("Test is started");
        log.info("fill key-value map needed FO");
        HashMap<String, String> purses = new HashMap<>();
        purses.put("209", "Capitalist MIRS WORLD Card, USD");
        purses.put("214", "Capitalist IDAA WORLD Card, USD");

        log.info("get random purse(FO)");
        int temp = randomNumbersInt(purses.size());
        String pursesFo = purses.keySet().toArray()[temp].toString();
        String pursesName = purses.values().toArray()[temp].toString();

        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);

        log.info("check if exist Capitslist WORLD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        log.info("all fields empty");
        pagesInit.getCabPublisherClients().setCapitalistCardInfo("")
                .setCapitalistExpDateMonth("")
                .setCapitalistExpDateYear("")
                .setCapitalistName("")
                .setCapitalistSecondName("")
                .setCapitalistDateOfBirth("")
                .setCapitalistAddress("")
                .setCapitalistCity("")
                .setCapitalistCountry("");

        pagesInit.getCabPublisherClients().addCapitalistWorldPurse(pursesFo);
        softAssert.assertEquals(helpersInit.getBaseHelper().getTextFromAlert(),
                "Not all mandatory fields are filled",
                "FAIL -> alert has doesn't correct text");
        softAssert.assertNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(pursesName),
                "FAIL -> purse 'Capitalist WORLD' doesn't created");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getCountErrorValidFields(), 9, "FAIL -> don't correct count error fields");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'Card info'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_CardInfo() {
        pagesInit.getCabPublisherClients().setCapitalistCardInfo("");
        capitalistWorldValidationEmptyFields("Card info");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'Expiration Date Month'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_ExpirationDateMonth() {
        pagesInit.getCabPublisherClients().setCapitalistExpDateMonth("");
        capitalistWorldValidationEmptyFields("Expiration Date Month");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'Expiration Date Year'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_ExpirationDateYear() {
        pagesInit.getCabPublisherClients().setCapitalistExpDateYear("");
        capitalistWorldValidationEmptyFields("Expiration Date Year");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'Name'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_Name() {
        pagesInit.getCabPublisherClients().setCapitalistName("");
        capitalistWorldValidationEmptyFields("Name");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * FO: 209, 214 - validation empty 'Second name'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_SecondName() {
        pagesInit.getCabPublisherClients().setCapitalistSecondName("");
        capitalistWorldValidationEmptyFields("Second name");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'date of birth'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_DateOfBirth() {
        pagesInit.getCabPublisherClients().setCapitalistDateOfBirth("");
        capitalistWorldValidationEmptyFields("date of birth");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'Address'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_Address() {
        pagesInit.getCabPublisherClients().setCapitalistAddress("");
        capitalistWorldValidationEmptyFields("Address");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209, 214 - validation empty 'City'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_City() {
        pagesInit.getCabPublisherClients().setCapitalistCity("");
        capitalistWorldValidationEmptyFields("City");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * FO: 209, 214 - validation empty 'Country'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test
    public void validationCapitalistWorldPurseEmpty_Country() {
        pagesInit.getCabPublisherClients().setCapitalistCountry("");
        capitalistWorldValidationEmptyFields("Country");
    }

    /**
     * Capitalist World helper method for check empty custom fields
     * <p>AIA</p>
     */
    public void capitalistWorldValidationEmptyFields(String fieldName) {
        log.info("Test is started for field - '" + fieldName + "'");
        log.info("fill key-value map needed FO");
        HashMap<String, String> purses = new HashMap<>();
        purses.put("209", "Capitalist MIRS WORLD Card, USD");
        purses.put("214", "Capitalist IDAA WORLD Card, USD");

        log.info("get random purse(FO)");
        int temp = randomNumbersInt(purses.size());
        String pursesFo = purses.keySet().toArray()[temp].toString();
        String pursesName = purses.values().toArray()[temp].toString();

        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);

        log.info("check if exist Capitslist WORLD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        pagesInit.getCabPublisherClients().addCapitalistWorldPurse(pursesFo);
        softAssert.assertEquals(helpersInit.getBaseHelper().getTextFromAlert(),
                "Not all mandatory fields are filled",
                "FAIL -> alert has doesn't correct text");
        Assert.assertNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(pursesName),
                "FAIL -> purse 'Capitalist WORLD' doesn't created");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getCountErrorValidFields(), 1,
                "FAIL -> don't correct count error fields");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().isShowErrorLightForCustomField(fieldName),
                "FAIL -> error show field - '" + fieldName + "'!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка всех схем выплат(рандом)
     * <p>Author AIA</p>
     */
    @Test
    public void checkAutoPaid() {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);

        log.info("* Set clients payout scheme. *");
        pagesInit.getCabPublisherClients().editAutoPaid();
        helpersInit.getBaseHelper().refreshCurrentPage();

        log.info("* Check clients payout scheme. *");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkAutoPaid(pagesInit.getCabPublisherClients().getPurseId()));
        log.info("Test is finished");
    }

    /**
     * Проверка перевода на МГ-кошелёк без утверждения заявки при создании
     * <ul>
     * <li>1. Ввод валидных данных в поле попапа по переводу на MG-кошелек - величина корректировки</li>
     * <li>2. Проверка схождения баланса после завершения транзакции</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22508">Ticket TA-22508</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "dataForCheckingTransfersToMgWallet")
    public void checkTransferToMgWallet(String sumTransferValue, String comment) {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("Get client MG-Wallet balance.");
        pagesInit.getCabPublisherClients().getClientMgWalletBalance();
        log.info("Get client MG-Wallet payouts amount.");
        pagesInit.getCabPublisherClients().getClientMgWalletAmount();
        log.info("Get first value of unconfirmed wage.");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("Add MG-transfer transaction.");
        pagesInit.getCabPublisherClients().fillSumWithCommentAndSubmitMgTransferPopup(sumTransferValue, comment, false);
        log.info("Open clients payment by client ID and status Pending.");
        authCabAndGo("wages/clients-payments?status=1&client_id=" + CLIENT_MGID_PAYOUTS_ID + "&direction=internal");
        log.info("Get and save payout transaction request ID.");
        pagesInit.getCabPublisherPayouts().getPayoutRequestId();
        log.info("Check payment transaction attributes.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("1", sumTransferValue, comment),
                "FAIL -> Check payment transaction attributes!");
        log.info("Approve payout request.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().approvePayoutRequest(),
                "FAIL -> Approve payout request!");
        log.info("Open clients payment by request ID.");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId);
        log.info("Check payment transaction attributes.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("5", sumTransferValue, comment),
                "FAIL -> Check payment transaction attributes after approve!");
        log.info("Go back to clients and check results.");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCorrectTransferToMgProcess(sumTransferValue),
                "FAIL - Check results!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка перевода на МГ-кошелёк c утверждением заявки при создании
     * <ul>
     * <li>1. Ввод валидных данных в поле попапа по переводу на MG-кошелек - величина корректировки</li>
     * <li>2. Проверка схождения баланса после завершения транзакции</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22508">Ticket TA-22508</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "dataForCheckingTransfersToMgWallet")
    public void checkTransferToMgWalletApproved(String sumTransferValue, String comment) {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("Get client MG-Wallet balance.");
        pagesInit.getCabPublisherClients().getClientMgWalletBalance();
        log.info("Get client MG-Wallet payouts amount.");
        pagesInit.getCabPublisherClients().getClientMgWalletAmount();
        log.info("Get first value of unconfirmed wage.");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("Add MG-transfer transaction.");
        pagesInit.getCabPublisherClients().fillSumWithCommentAndSubmitMgTransferPopup(sumTransferValue, comment, true);
        log.info("Refresh clients page and check results.");
        helpersInit.getBaseHelper().refreshCurrentPage();
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkCorrectTransferToMgProcess(sumTransferValue));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForCheckingTransfersToMgWallet() {
        return new Object[][]{
                {"0.01", "test_auto"}
        };
    }
}
