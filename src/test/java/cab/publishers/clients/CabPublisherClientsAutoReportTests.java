package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static pages.cab.publishers.variables.CabPublisherClientVariables.autoReportEmailBody;
import static testData.project.CliCommandsList.Cron.AUTO_REPORT;

public class CabPublisherClientsAutoReportTests extends TestBase {
    /**
     * Check Create/Edit/Delete settings for Auto Report for Stats by Sites
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51796">Ticket TA-51796</a>
     * @see <a href="https://jira.mgid.com/browse/TA-51797">Ticket TA-51797</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkAutoReportBySitesStatistics() {
        log.info("Test is started");
        int clientId = 2001;
        authCabAndGo("wages/clients?client_currency=&agency=&client_id=" + clientId);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Disabled Auto report");
        pagesInit.getCabPublisherClients().createAutoReport("Stats by Sites");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report after creation!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by Sites", false),
                "FAIL -> Check auto report after creation!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCancelAutoReport(),
                "FAIL -> Cancel Auto report!");
        pagesInit.getCabPublisherClients().editAutoReport("Stats by Sites");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report after editing!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by Sites", true),
                "FAIL -> Check auto report after editing!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCloseAutoReport(),
                "FAIL -> Close Auto report!");
        pagesInit.getCabPublisherClients().turnOffAutoReport();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Auto report after turn off");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Create/Edit/Delete settings for Auto Report for Stats by Widgets
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51817">Ticket TA-51817</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkAutoReportByWidgetsStatistics() {
        log.info("Test is started");
        int clientId = 2001;
        authCabAndGo("wages/clients?client_currency=&agency=&client_id=" + clientId);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Disabled Auto report");
        pagesInit.getCabPublisherClients().createAutoReport("Stats by Widgets");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by Widgets", false),
                "FAIL -> Check auto report after creation!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCancelAutoReport(),
                "FAIL -> Cancel Auto report!");
        pagesInit.getCabPublisherClients().editAutoReport("Stats by Widgets");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by Widgets", true),
                "FAIL -> Check auto report after editing!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCloseAutoReport(),
                "FAIL -> Close Auto report!");
        pagesInit.getCabPublisherClients().turnOffAutoReport();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Auto report after turn off");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Create/Edit/Delete settings for Auto Report for Stats by total sites
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51794">Ticket TA-51794</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkAutoReportBySitesStatisticsTotal() {
        log.info("Test is started");
        int clientId = 2001;
        authCabAndGo("wages/clients?client_currency=&agency=&client_id=" + clientId);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Disabled Auto report");
        pagesInit.getCabPublisherClients().createAutoReport("Stats by total sites");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by total sites", false),
                "FAIL -> Check auto report after creation!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCancelAutoReport(),
                "FAIL -> Cancel Auto report!");
        pagesInit.getCabPublisherClients().editAutoReport("Stats by total sites");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("enabled"),
                "FAIL -> Enabled Auto report");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReport("Stats by total sites", true),
                "FAIL -> Check auto report after editing!");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCloseAutoReport(),
                "FAIL -> Close Auto report!");
        pagesInit.getCabPublisherClients().turnOffAutoReport();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Setting Auto reports has been saved"));
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkAutoReportIcon("disabled"),
                "FAIL -> Auto report after turn off");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check Auto Report e-mail
     *
     * @see <a href="https://jira.mgid.com/browse/TA-51797">Ticket TA-51797</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkAutoReportMail() {
        log.info("Test is started");
        String clientId = "2001";
        authCabAndGo("wages/clients?client_currency=&agency=&client_id=" + clientId);
        pagesInit.getCabPublisherClients().createAutoReport("Stats by Sites",
                helpersInit.getBaseHelper().getCurrentHour("Pacific/Honolulu"), "daily");
        operationMySql.getMailPull().getCountLettersByClientEmail(pagesInit.getCabPublisherClients().getAutoReportEmails());
        serviceInit.getDockerCli().runAndStopCron(AUTO_REPORT, "-vv");
        Assert.assertEquals(operationMySql.getMailPull().getBodyFromMailByClientEmail(
                pagesInit.getCabPublisherClients().getAutoReportEmails()),
                String.format(autoReportEmailBody, clientId, helpersInit.getBaseHelper().getYesterdayDate("Pacific/Honolulu", "dd/MM/yyyy")),
                "FAIL -> Auto report mail body!");
        pagesInit.getCabPublisherClients().turnOffAutoReport();
        log.info("Test is finished");
    }
}
