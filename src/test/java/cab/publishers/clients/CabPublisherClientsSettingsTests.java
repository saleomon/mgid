package cab.publishers.clients;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.cab.publishers.variables.CabPublisherClientVariables.defaultRevenueSharePercent;
import static pages.cab.publishers.variables.CabPublisherClientVariables.newRevenueShareProc;
import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.clientWagesEditUrl;
import static testData.project.EndPoints.sitesWagesListUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.Subnets.SubnetType.SCENARIO_ADSKEEPER;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;

/*
 * 1. Forbid creating new widgets:
 *      - flagCanCreateWidget
 *      - flagCanNotCreateWidget
 *      - flagCanEditCreateWidget
 *
 * 2. RS Income percentage for new widgets, %:
 *      - checkClientRevenueShare
 *
 * 3. Allowed clicks from client domains only:,
 *      - allowedClicksFromClientDomainsOnly
 *
 * 4. Enabled direct publisher demand ads
 *      - enabledDirectPublisherDemandAds
 *
 */
public class CabPublisherClientsSettingsTests extends TestBase {

    //private int tickerCompositeId;

    /**
     * Allowed clicks from client domains only
     * <p>RKO</p>
     */
    @Test
    public void allowedClicksFromClientDomainsOnly() {
        log.info("Test is started");
        log.info("switch on Allowed clicks from client domains only");
        authCabAndGo(clientWagesEditUrl + 29);
        pagesInit.getCabPublisherClients().switchAllowedClicksFromClientDomainsOnly(true);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("check show icon in other interfaces: clients, sites, widgets, crm");
        authCabAndGo("wages/clients?client_id=" + 29);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon doesn't show in clients");
        authCabAndGo(sitesWagesListUrl + 29);
        softAssert.assertTrue(pagesInit.getCabWebsites().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon doesn't show in sites");
        authCabAndGo("wages/widgets/?c_id=" + 46);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon doesn't show in widget");
        authCabAndGo("crm/index/client_id/" + 29);
        softAssert.assertTrue(pagesInit.getCrmClients().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon doesn't show in crm");

        log.info("switch off Allowed clicks from client domains only");
        authCabAndGo(clientWagesEditUrl + 29);
        pagesInit.getCabPublisherClients().switchAllowedClicksFromClientDomainsOnly(false);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("check doesn't show icon in other interfaces: clients, sites, widgets, crm");
        authCabAndGo("wages/clients?client_id=" + 29);
        softAssert.assertFalse(pagesInit.getCabPublisherClients().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon show in clients");
        authCabAndGo(sitesWagesListUrl + 29);
        softAssert.assertFalse(pagesInit.getCabWebsites().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon show in sites");
        authCabAndGo("wages/widgets/?c_id=" + 46);
        softAssert.assertFalse(pagesInit.getCabWidgetsList().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon show in widget");
        authCabAndGo("crm/index/client_id/" + 29);
        softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedAllowedClicksFromClientDomainsOnly(), "FAIL -> icon show in crm");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность запретить создание информеров клиенту
     * проверка что клиент может создавать виджет
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21772">VT-21772</a>
     */
    @Test
    public void flagCanCreateWidget() {
        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + 22);
        pagesInit.getCabPublisherClients().switchForbidCreateNewWidget(false);

        authDashAndGo("testEmail24@ex.ua","publisher/add-widget/site/" + 22);

        Assert.assertNotEquals(pagesInit.getWidgetClass().createSimpleWidget(), 0, "FAIL -> widget isn't create");
        log.info("Test is finished");
    }

    /**
     * Добавить возможность запретить создание информеров клиенту
     * проверка что клиент НЕ может создавать виджет
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21772">VT-21772</a>
     */
    @Test
    public void flagCanNotCreateWidget() {
        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + 22);
        pagesInit.getCabPublisherClients().switchForbidCreateNewWidget(true);

        authDashAndGo("testEmail24@ex.ua", "publisher/add-widget/site/" + 22);

        softAssert.assertTrue( helpersInit.getMessageHelper().checkMessageDash("Adding new widgets is not allowed"), "FAIL -> error message");
        softAssert.assertTrue(pagesInit.getWidgetClass().createButtonIsEnabled(), "FAIL -> create button is displayed");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность запретить создание информеров клиенту
     * проверка что клиент может редактировать виджеты созданные ранее
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21772">VT-21772</a>
     */
    @Test
    public void flagCanEditCreateWidget() {
        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + 22);
        pagesInit.getCabPublisherClients().switchForbidCreateNewWidget(true);

        authDashAndGo("testEmail24@ex.ua", "publisher/edit-widget/id/" + 306);
        Assert.assertTrue(pagesInit.getWidgetClass().reSaveWithEditWidget());

        log.info("Test is finished");
    }

    /**
     * Проверка настройки клиента "revenue share for new widgets"
     * <p>Кейсы которые покрыты:
     * <ul>
     * <li>- проверка работы права;</li>
     * <li>- проверка дефолтного значения Revenue Share у клиента;</li>
     * <li>- проверка дефолтного значение Revenue Share для виджетов;</li>
     * <li>- изменение Revenue Share у клиента;</li>
     * <li>- проверка неизменности значения Revenue Share для старых виджетов;</li>
     * <li>- проверка измененного значения Revenue Share для новых виджетов;</li>
     * <li>- проверяется для MGID и Adskeeper.</li>
     * </ul>
     * <p>Используется право - default/wages/can_set_rs_for_new_widgets</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21942">Ticket VT-21942</a>
     * <p>Author AIA</p>
     */
    @Privilege(name = "wages/can_set_rs_for_new_widgets")
    @Test(dataProvider = "revenueShareForClient")
    void checkClientRevenueShare(String clientSubnet, int clientId, int webSiteID, Subnets.SubnetType subnetType) {
        try {
            log.info("Test is started for " + clientSubnet);
            subnetId = subnetType;
            log.info("Право выключено.");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_set_rs_for_new_widgets");
            authCabForCheckPrivileges(clientWagesEditUrl + clientId);
            softAssert.assertFalse(pagesInit.getCabPublisherClients().checkRevenueShareFieldDisplaying(),
                    "FAIL -> Check revenue share field isn't displaying!");
            log.info("Право включено.");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_set_rs_for_new_widgets");
            authCabAndGo(clientWagesEditUrl + clientId);
            softAssert.assertTrue(pagesInit.getCabPublisherClients().checkRevenueShareFieldDisplaying(),
                    "FAIL -> Check revenue share field displaying!");
            authCabAndGo(clientWagesEditUrl + clientId);
            softAssert.assertTrue(pagesInit.getCabPublisherClients().checkDefaultRevenueSharePercent(),
                    "FAIL -> Check default revenue share percent!");
            log.info("Переходим в дешборд");
            authDashAndGo("sok.autotest.payouts@mgid.com","publisher/add-widget/site/" + webSiteID);
            pagesInit.getWidgetClass().createSimpleWidget();
            authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getWidgetId());
            log.info("Проверяем значение ставки Revenue Share для виджета");
            softAssert.assertTrue(pagesInit.getCabWidgetsList().checkWidgetRevenueShare(
                    pagesInit.getWidgetClass().getWidgetId(), defaultRevenueSharePercent),
                    "FAIL -> Check revenue share for widget!");
            log.info("Изменяем значение ставки Revenue Share для клиента.");
            authCabAndGo(clientWagesEditUrl + clientId);
            pagesInit.getCabPublisherClients().editRevenueShareForClient(newRevenueShareProc);
            log.info("Проверяем, что значение ставки Revenue Share для виджета не изменилось.");
            authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getWidgetId());
            softAssert.assertTrue(pagesInit.getCabWidgetsList().checkWidgetRevenueShare(
                    pagesInit.getWidgetClass().getWidgetId(), defaultRevenueSharePercent),
                    "FAIL -> Check revenue share for widget doesn't change after clients change!");
            log.info("Пересохраняем виджет.");
            authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
            pagesInit.getCabCompositeSettings().chooseSourceId();
            pagesInit.getCabCompositeSettings().saveWidgetSettings();
            log.info("Проверяем, что значение ставки Revenue Share для виджета не изменилось.");
            softAssert.assertTrue(pagesInit.getCabWidgetsList().checkWidgetRevenueShare(
                    pagesInit.getWidgetClass().getWidgetId(), defaultRevenueSharePercent),
                    "FAIL -> Check revenue share for widget doesn't change after widget re-save!");
            log.info("Создаем новый виджет и проверяем, что новая ставка Revenue Share применяется.");
            authDashAndGo("publisher/add-widget/site/" + webSiteID);
            pagesInit.getWidgetClass().createSimpleWidget();
            authCabAndGo("wages/widgets/?c_id=" + pagesInit.getWidgetClass().getWidgetId());
            log.info("Проверяем измененное значение ставки Revenue Share для нового виджета");
            softAssert.assertTrue(pagesInit.getCabWidgetsList().checkWidgetRevenueShare(
                    pagesInit.getWidgetClass().getWidgetId(), newRevenueShareProc),
                    "FAIL -> Check revenue share for new widget!");
            softAssert.assertAll();
            log.info("Test is finished for " + clientSubnet);
        } finally {
            log.info("Возвращаем клиенту дефолтное значение Revenue Share ");
            authCabAndGo(clientWagesEditUrl + clientId);
            pagesInit.getCabPublisherClients().editRevenueShareForClient(defaultRevenueSharePercent);
            subnetId = SCENARIO_MGID;
        }
    }

    @DataProvider
    public static Object[][] revenueShareForClient() {
        return new Object[][]{
                {"MGID client", CLIENT_MGID_USD_ID, WEBSITE_FOR_USD_CLIENT_MGID, SCENARIO_MGID},
                {"Adskeeper client", CLIENT_ADSKEEPER_USD_ID, WEBSITE_FOR_USD_CLIENT_ADSKEEPER, SCENARIO_ADSKEEPER}
        };
    }

    @Epic("CLIENT PUBLISHER SETTINGS")
    @Feature("direct publisher demand")
    @Story("direct publisher demand")
    @Description("Client settings -> Enabled direct publisher demand ads <a href='https://jira.mgid.com/browse/KOT-2916'>KOT-2916</a>")
    @Test(description = "Client settings -> Enabled direct publisher demand ads")
    public void enabledDirectPublisherDemandAds() {
        log.info("Test is started");
        log.info("enabled DirectPublisherDemandAds");
        authCabAndGo(clientWagesEditUrl + 70);
        pagesInit.getCabPublisherClients().switchDirectPublisherDemand(true);
        pagesInit.getCabPublisherClients().saveSettings();
        authCabAndGo(clientWagesEditUrl + 70);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkDirectPublisherDemand(true), "FAIL -> DirectPublisherDemandAds cab enabled");
        authDashAndGo("testEmail70@ex.ua", "publisher");
        softAssert.assertTrue(pagesInit.getWebsiteClass().directDemandIsDisplayed(), "FAIL -> DirectPublisherDemandAds dash enabled");

        log.info("disabled DirectPublisherDemandAds");
        authCabAndGo(clientWagesEditUrl + 70);
        pagesInit.getCabPublisherClients().switchDirectPublisherDemand(false);
        pagesInit.getCabPublisherClients().saveSettings();
        authCabAndGo(clientWagesEditUrl + 70);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkDirectPublisherDemand(false), "FAIL -> DirectPublisherDemandAds cab disabled");
        authDashAndGo("testEmail70@ex.ua", "publisher");
        softAssert.assertFalse(pagesInit.getWebsiteClass().directDemandIsDisplayed(), "FAIL -> DirectPublisherDemandAds dash disabled");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("CLIENT PUBLISHER SETTINGS")
    @Feature("client publisher settings")
    @Story("Add \"Clients Legal Name\" and \"Companies Main Website\" fields to client editing")
    @Description("Add \"Clients Legal Name\" and \"Companies Main Website\" fields to client editing <a href='https://jira.mgid.com/browse/TA-52412'>TA-52412</a>")
    @Test(description = "check Clients Legal Name and Companies Main Website")
    public void clientLegalNameAndCampaignMainWebSite() {
        log.info("Test is started");
        int clientId = 22;
        String mainWebsite = "s.cu",
                mainWebsiteEdit = "test.com.ua",
                legalName = "legal-name",
                legalNameEdit = "legal-name-edit";

        log.info("set clientLegalNameAndCampaignMainWebSite");
        authCabAndGo(clientWagesEditUrl + clientId);
        pagesInit.getCabPublisherClients().fillCampaignMainWebsite(mainWebsite);
        pagesInit.getCabPublisherClients().fillClientLegalName(legalName);
        pagesInit.getCabPublisherClients().saveSettings();
        authCabAndGo(clientWagesEditUrl + clientId);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCampaignMainWebsite(mainWebsite), "FAIL -> checkCampaignMainWebsite");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkClientLegalName(legalName), "FAIL -> checkClientLegalName");

        log.info("edit clientLegalNameAndCampaignMainWebSite");
        authCabAndGo(clientWagesEditUrl + clientId);
        pagesInit.getCabPublisherClients().fillCampaignMainWebsite(mainWebsiteEdit);
        pagesInit.getCabPublisherClients().fillClientLegalName(legalNameEdit);
        pagesInit.getCabPublisherClients().saveSettings();
        authCabAndGo(clientWagesEditUrl + clientId);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCampaignMainWebsite(mainWebsiteEdit), "FAIL -> checkCampaignMainWebsite edit");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkClientLegalName(legalNameEdit), "FAIL -> checkClientLegalName edit");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
