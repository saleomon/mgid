package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.HashMap;

import static core.helpers.BaseHelper.randomNumbersInt;
import static pages.cab.publishers.variables.CabPublisherClientVariables.valueForCorrectionWages;
import static testData.project.ClientsEntities.CLIENT_MGID_PAYOUTS_ID;
import static testData.project.EndPoints.clientWagesEditUrl;
import static testData.project.OthersData.MAX_STRING_VALUE_256;

// 14
public class CabPublisherClientsPayouts1Tests extends TestBase {

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'Card info'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_CardInfoValid(String value) {
        pagesInit.getCabPublisherClients().setCapitalistCardInfo(value);
        capitalistWorldValidationFields("Wrong card number, only 16 digits allowed");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'Name'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_NameValid(String value) {
        pagesInit.getCabPublisherClients().setCapitalistName(value);
        capitalistWorldValidationFields("Wrong cardholder name");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'Second name'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_SecondNameValid(String value) {
        pagesInit.getCabPublisherClients().setCapitalistSecondName(value);
        capitalistWorldValidationFields("Wrong cardholder second name");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'date of birth (YYYY-MON-DD)'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_DateOfBirth(String value) {
        pagesInit.getCabPublisherClients().setCapitalistDateOfBirth(value);
        capitalistWorldValidationFields("Wrong date of birthday");
    }

    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'Address'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_Address(String value) {
        pagesInit.getCabPublisherClients().setCapitalistAddress(value);
        capitalistWorldValidationFields("Wrong cardholder address");
    }
    /**
     * Доработать Payment details для ФО Capitalist
     * <p>FO: 209 - Capitalist MIRS WORLD Card, USD</p>
     * <p>validation 'City'</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23399">Ticket TA-23399</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataValidCapitalistWorldPurse")
    public void validationCapitalistWorldPurse_City(String value) {
        pagesInit.getCabPublisherClients().setCapitalistCity(value);
        capitalistWorldValidationFields("Wrong cardholder city");
    }

    @DataProvider
    public Object[][] dataValidCapitalistWorldPurse() {
        return new Object[][]{
                {"v"},
                {MAX_STRING_VALUE_256}
        };
    }

    /**
     * Capitalist World helper method for check custom fields on validation - min/max value
     */
    public void capitalistWorldValidationFields(String errorMessage) {
        log.info("Test is started");
        log.info("fill key-value map needed FO");
        HashMap<String, String> purses = new HashMap<>();
        purses.put("209", "Capitalist MIRS WORLD Card, USD");
        purses.put("214", "Capitalist IDAA WORLD Card, USD");

        log.info("get random purse(FO)");
        int temp = randomNumbersInt(purses.size());
        String pursesFo = purses.keySet().toArray()[temp].toString();
        String pursesName = purses.values().toArray()[temp].toString();

        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);

        log.info("check if exist Capitslist WORLD purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(pursesName);

        pagesInit.getCabPublisherClients().addCapitalistWorldPurse(pursesFo);
        softAssert.assertNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(pursesName),
                "FAIL -> purse 'Capitalist WORLD' doesn't created");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getCountErrorValidFields(), 0,
                "FAIL -> don't correct count error fields");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getCapitalistWorldErrorMessage(), errorMessage,
                "FAIL -> message isn't correct");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка валидации полей при переводе на MG-кошелек
     * <ul>
     * <li>1. Ввод невалидных данных в поле попапа по переводу на MG-кошелек - величина корректировки</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка ошибок валидации.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22508">Ticket TA-22508</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "dataForCheckValidationInMgTransferPopupFields")
    public void checkValidationInMgTransferPopupFields(String payOut, String errorMessage) {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        pagesInit.getCabPublisherClients().fillDataAndSubmitMgTransferPopup(payOut);
        Assert.assertEquals(pagesInit.getCabPublisherClients().getMoneyWithdrawNotValid(), errorMessage);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForCheckValidationInMgTransferPopupFields() {
        return new Object[][]{
                {"", "Amount claimed by user has not been provided"},
                {"-5", "Value is less than the minimum"},
                {"dfdf", "Wrong format. Data type of the value is not floating point number."}
        };
    }

    /**
     * Проверка вывода заработка без автоподтверждения
     * <ul>
     * <li>1. Ввод валидных данных в поле попапа по выводу заработка</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка корректности вывода.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22509">Ticket TA-22509</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "checkMoneyWithdraw")
    public void checkMoneyWithdraw(String payOutValue, String comment, String purseId) {
        log.info("Test is started");
        log.info("отклоняем заявки если есть со статусом 'На модерации'");
        authCabAndGo("wages/clients-payments?status=1&client_id=" + CLIENT_MGID_PAYOUTS_ID);
        pagesInit.getCabPublisherPayouts().checkPendingRequestsAndClearThem();

        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("Get first value of unconfirmed wage.");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("Get first value of Payouts amount.");
        pagesInit.getCabPublisherClients().getClientPayoutsAmount();
        log.info("Add payment transaction.");
        pagesInit.getCabPublisherClients().fillAmountWithCommentAndSubmitPayOutPopup(payOutValue, comment, purseId, false);
        log.info("Open clients payment by client ID and status Pending.");
        authCabAndGo("wages/clients-payments?status=1&client_id=" + CLIENT_MGID_PAYOUTS_ID + "&direction=external");
        log.info("Get and save payout transaction request ID.");
        pagesInit.getCabPublisherPayouts().getPayoutRequestId();
        log.info("Check payment transaction attributes.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("1", payOutValue, comment),
                "FAIL -> Check payment transaction attributes!");
        log.info("Approve payout request.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().approvePayoutRequest(),
                "FAIL -> Approve payout request.!");
        log.info("Open clients payment by request ID.");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId);
        log.info("Check payment transaction attributes.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("2", payOutValue, comment),
                "FAIL -> Check payment transaction attributes after approve!");
        log.info("Open paid dialog and check purse ID.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPayoutRequest(purseId, payOutValue, comment),
                "FAIL -> Open paid dialog and check purse ID!");
        log.info("Submit paid dialog.");
        pagesInit.getCabPublisherPayouts().paidPayoutRequest();
        log.info("Open clients payments by request ID and status Paid.");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId + "&status=3");
        log.info("Check payment transaction attributes.");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("3", payOutValue, comment),
                "FAIL -> Check payment transaction attributes after paid!");
        log.info("Go back to clients and check results.");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCorrectPaymentProcess(payOutValue),
                "FAIL -> Check results!");
        log.info("Open clients payments by request ID with status Paid and cancel it.");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId);
        pagesInit.getCabPublisherPayouts().cancelTransaction(comment);
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkThatTransactionCancellation("Canceled"),
                "FAIL -> Cancel transaction!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] checkMoneyWithdraw() {
        return new Object[][]{
                {"0.01", "test_auto", "655765788797978"}
        };
    }

    /**
     * Проверка вывода заработка с автоподтверждением
     * <ul>
     * <li>1. Ввод валидных данных в поле попапа по выводу заработка</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка корректности вывода.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22509">Ticket TA-22509</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "checkMoneyWithdraw")
    public void checkMoneyWithdrawApprove(String payOutValue, String comment, String purseID) {
        log.info("Test is started");
        authCabAndGo("wages/clients-payments?status=2&client_id=" + CLIENT_MGID_PAYOUTS_ID);
        pagesInit.getCabPublisherPayouts().checkApprovedRequestsAndClearThem();
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("* Get first value of unconfirmed wage. *");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("* Get first value of Payouts amount. *");
        pagesInit.getCabPublisherClients().getClientPayoutsAmount();
        log.info("* Add payment transaction. *");
        pagesInit.getCabPublisherClients().fillAmountWithCommentAndSubmitPayOutPopup(payOutValue, comment, purseID, true);
        log.info("* Open clients payment by client ID and status Approved. *");
        authCabAndGo("wages/clients-payments?status=2&direction=external&client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("* Get and save payout transaction request ID. *");
        pagesInit.getCabPublisherPayouts().getPayoutRequestId();
        log.info("* Check payment transaction attributes. *");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("2", payOutValue, comment),
                "FAIL -> Check payment transaction attributes");
        log.info("* Open paid dialog and check purse ID. *");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPayoutRequest(purseID, payOutValue, comment),
                "FAIL -> Check purse ID!");
        log.info("* Submit paid dialog. *");
        pagesInit.getCabPublisherPayouts().paidPayoutRequest();
        log.info("* Open clients payments by request ID and status Paid. *");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId + "&status=3");
        log.info("* Check payment transaction attributes. *");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayoutRequests("3", payOutValue, comment),
                "FAIL -> Check payment transaction attributes after paid!");
        log.info("* Go back to clients and check results. *");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkCorrectPaymentProcess(payOutValue),
                "FAIL -> Check results!");
        log.info("* Open clients payments by request ID with status Paid and cancel it. *");
        authCabAndGo("wages/clients-payments?id=" + pagesInit.getCabPublisherPayouts().requestId);
        pagesInit.getCabPublisherPayouts().cancelTransaction(comment);
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkThatTransactionCancellation("Canceled"),
                "FAIL -> Cancel transaction!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка валидации при выводе заработка
     * <ul>
     * <li>1. Ввод невалидных данных в поле попапа по выводу заработка - величина корректировки</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка ошибок валидации.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22509">Ticket TA-22509</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "dataForCheckMoneyWithdrawNotValid")
    public void checkValidationPayOutPopupFields(String payOut, String errorMessage) {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        pagesInit.getCabPublisherClients().fillDataAndSubmitPayOutPopup(payOut);
        Assert.assertEquals(pagesInit.getCabPublisherClients().getMoneyWithdrawNotValid(), errorMessage);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForCheckMoneyWithdrawNotValid() {
        return new Object[][]{
                {"", "Amount claimed by user has not been provided"},
                {"-5", "Value is less than the minimum"},
                {"dfdf", "Wrong format. Data type of the value is not floating point number."}
        };
    }

    /**
     * Проверка корректировки заработка
     * <ul>
     * <li>1. Ввод валидных данных в поля попапа по корректировке заработка (величина корректировки, комментарий)</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка изменений.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22507">Ticket TA-22507</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkWagesCorrection() {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        log.info("Получаем текущее значение нераспределенного заработка.");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("Корректируем значение нераспределенного заработка.");
        pagesInit.getCabPublisherClients().setWagesValue(valueForCorrectionWages);
        log.info("Проверяем, что корректировка применилась.");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkNewWagesValue(valueForCorrectionWages));
        log.info("Получаем значение нераспределенного заработка после предыдущей корректировки.");
        pagesInit.getCabPublisherClients().getCurrentUnconfirmedWagesValue();
        log.info("Возвращаем предыдущее значение нераспределенного заработка.");
        pagesInit.getCabPublisherClients().setWagesValue("-" + valueForCorrectionWages);
        log.info("Проверяем, что корректировка применилась.");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkNewWagesValue("-" + valueForCorrectionWages));
        log.info("Test is finished");
    }

    /**
     * Проверка валидации при корректировке заработка
     * <ul>
     * <li>1. Ввод невалидных данных в поля попапа по корректировке заработка (величина корректировки, комментарий)</li>
     * <li>2. Сабмит попапа.</li>
     * <li>3. Проверка ошибок валидации.</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22507">Ticket TA-22507</a>
     * <p>AIA</p>
     */
    @Test(dataProvider = "dataForCheckCorrectWages")
    public void checkValidationCorrectWagesPopupFields(String amount, String comment, String error) {
        log.info("Test is started");
        authCabAndGo("wages/clients?client_id=" + CLIENT_MGID_PAYOUTS_ID);
        pagesInit.getCabPublisherClients().fillDataAndSubmitPopup(amount, comment);
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkValidationErrors(error));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForCheckCorrectWages() {
        return new Object[][]{
                {"0", "test", "'0' is not greater than '0'"},
                {"0.01", "test", "'test' is less than 10 characters long"},
                {"dfdf", "test", "Provided data ain't numeric"},
                {"0.01", "testtestt", "'testtestt' is less than 10 characters long"}
        };
    }

    /**
     * Формирование и проверка пакетов на выплату для клиентов с валютой EUR, RUB и USD
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24340">Ticket TA-24340</a>
     * <p>Author AIA</p>
     */
    @Test(dataProvider = "clientsCurrencies")
    public void checkPaymentPacks(String clientId) {
        log.info("Test is started");
        authCabAndGo("wages/clients-payments?status=2&client_id=" + clientId);
        pagesInit.getCabPublisherPayouts().getClientsPayoutRequestsIds();
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPaymentsPackCheckboxes(true),
                "FAIL -> checkboxes state before adding to pack");
        log.info("* Add payments to pack *");
        pagesInit.getCabPublisherPayouts().addPaymentsToPack();
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPaymentsPackCheckboxes(false),
                "FAIL -> checkboxes state before adding to pack!");
        softAssert.assertEquals(operationMySql.getPaymentsPack().getCountPackWithStatus17(clientId),
                operationMySql.getPayments().getCountPayments(clientId),
                "FAIL -> getCountPackWithStatus17");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] clientsCurrencies() {
        return new Object[][]{
                {"2008"},
                {"2009"},
                {"2024"},
                {"2007"}
        };
    }

    /**
     * Client's payout region
     * Если у клиента есть неудаленный кошелек Tipalti MGID Inc, USD (id=150), то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен c дефолтным значением "Not selected";</li>
     * <li>в тултипе при наведении выводится подсказка: "Please contact to fin dept";</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24496">Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionForClientWithTipaltiWallet() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2008");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Not selected",
                "FAIL -> Selected value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(), "Please contact to fin dept",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть кошелек InterTipalti MGID Inc, USD (id = 160) и одобренная заявка, то:
     * <ul>
     * <li>селект-бокс "Client's payout region" не активен c дефолтным значением "Not selected";</li>
     * <li>в тултипе при наведении выводится подсказка:
     * "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets
     * of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\"";</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24496">Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionForPurseInterTipalti160WithApprovedPayments() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2010");
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkPayoutRegionAvailability(),
                "FAIL -> select state!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Not selected",
                "FAIL -> Selected value in 'Client payout region'!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getTooltipText(),
                "It's impossible to set/withdraw \"Asia Client\" while there are applications on the wallets " +
                        "of InterTipalti MGID Inc, USD / InterTipalti MGID Asia in the status of \"approved\"",
                "FAIL -> Tooltip text!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}