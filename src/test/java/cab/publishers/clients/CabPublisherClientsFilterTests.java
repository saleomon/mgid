package cab.publishers.clients;

import org.testng.*;
import org.testng.annotations.*;
import testBase.TestBase;

/*
 * 1. Publisher currency
 *      - currencyFilter
 */
public class CabPublisherClientsFilterTests extends TestBase {

    /**
     * check work filter 'Currency'
     * <p>RKO</p>
     */
    @Test
    public void currencyFilter() {
        log.info("Test is started");
        authCabAndGo("wages/clients");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkWorkCurrencyFilter());
        log.info("Test is finished");
    }
}
