package cab.publishers.clients;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.*;
import static testData.project.EndPoints.clientWagesEditUrl;

// 14
public class CabPublisherClientsPayouts4Tests extends TestBase {

    public CabPublisherClientsPayouts4Tests() {
        clientLogin = CLIENTS_PAYOUTS_LOGIN;
    }

    /**
     * Не сбрасывать настройки клиентских выплат при смене кошелька
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21225">Ticket TA-21225</a>
     * @see <a href="https://youtrack.mgid.com/issue/KOT-152">Ticket KOT-152</a>
     * <p>AIA</p>
     */
    @Test
    public void changeFavoriteWalletWithCheckClientsPaymentsInCab() {
        log.info("Test is started");
        int clientId = 2040;
        authCabAndGo("wages/clients?client_id=" + clientId);
        log.info("Edit auto-paid settings");
        pagesInit.getCabPublisherClients().editAutoPaid();
        log.info("Change favorite wallet");
        authDashAndGo("sok.autotest.payouts_40@mgid.com", "publisher/payouts");
        String purseId = pagesInit.getPayoutsClass().changeFavoriteWallet(
                pagesInit.getCabPublisherClients().getCurrentPayoutScheme(),
                pagesInit.getCabPublisherClients().getPurseId());
        authCabAndGo("wages/clients?client_id=" + clientId);
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkAutoPaid(purseId),
                "FAIL -> Check clients payout settings after changing favorite wallet!");
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть неудаленный кошелек InterTipalti MGID Inc, USD (id=160), то
     * <ul>
     * <li>при выборе "Client's payout region = Asia" необходимо обновлять кошелек клиента на InterTipalti MGID Asia, USD (id=222)</li>
     * </li>и обновить его так же в заявках клиента (если они есть с кошельком id=160).</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionAsiaChangeClientWalletInterTipalti160() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2012");
        pagesInit.getCabPublisherClients().setClientsPayoutRegion("Asia");
        authCabAndGo("wages/clients-edit/id/2012");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Saved value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPaymentMethodId(), "222",
                "FAIL -> Wallet ID for Asia!");
        authCabAndGo("wages/clients-payments?client_id=2012");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayments("intertipalti mgid asia, usd"),
                "FAIL -> Purse in requests!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть неудаленный кошелек InterTipalti MGID ASIA, USD (id=222), то
     * <ul>
     * <li>при выборе "Client's payout region = Not selected" необходимо обновлять кошелек клиента на
     * InterTipalti MGID Inc, USD (id=160)</li>
     * </li>и обновить его так же в заявках клиента (если они есть с кошельком id=222).</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionAsiaChangeClientWalletInterTipalti222() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2013");
        pagesInit.getCabPublisherClients().setClientsPayoutRegion("Not selected");
        authCabAndGo("wages/clients-edit/id/2013");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Not selected",
                "FAIL -> Saved value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPaymentMethodId(), "160",
                "FAIL -> Wallet ID for Asia!");
        authCabAndGo("wages/clients-payments?client_id=2013");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayments("intertipalti mgid inc, usd"),
                "FAIL -> Purse in requests!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть неудаленный кошелек PayPal MGID Inc, USD (id=89), то
     * <ul>
     * <li>при выборе "Client's payout region = Asia" необходимо обновлять кошелек клиента на PayPal MGID Asia, USD (id=223)</li>
     * </li>и обновить его так же в заявках клиента (если они есть с кошельком id=89).</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24497>Ticket TA-24497</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionAsiaChangeClientWalletPayPal89() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2016");
        pagesInit.getCabPublisherClients().setClientsPayoutRegion("Asia");
        authCabAndGo("wages/clients-edit/id/2016");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Asia",
                "FAIL -> Saved value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPaymentMethodId(), "223",
                "FAIL -> Wallet ID for Asia!");
        authCabAndGo("wages/clients-payments?client_id=2016");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayments("paypal mgid asia, usd"),
                "FAIL -> Purse in requests!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Если у клиента есть неудаленный кошелек PayPal MGID Asia, USD (id=223), то
     * <ul>
     * <li>при выборе "Client's payout region = Not selected" необходимо обновлять кошелек клиента на PayPal MGID Inc, USD (id=89)</li>
     * </li>и обновить его так же в заявках клиента (если они есть с кошельком id=223).</li>
     * </ul>
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-24497>Ticket TA-24497</a>
     * <p>Author AIA</p>
     */
    @Test
    public void payoutRegionAsiaChangeClientWalletPayPal223() {
        log.info("Test is started");
        authCabAndGo("wages/clients-edit/id/2017");
        pagesInit.getCabPublisherClients().setClientsPayoutRegion("Not selected");
        authCabAndGo("wages/clients-edit/id/2017");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPayoutRegionValue(), "Not selected",
                "FAIL -> Saved value!");
        softAssert.assertEquals(pagesInit.getCabPublisherClients().getPaymentMethodId(), "89",
                "FAIL -> Wallet ID for Asia!");
        authCabAndGo("wages/clients-payments?client_id=2017");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayments("paypal mgid inc, usd"),
                "FAIL -> Purse in requests!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Add wallet Paymaster24 MGID Asia, USD (id=249) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-28487">Ticket TA-28487</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addPaymaster24MgidAsiaUsdPurse() {
        log.info("Test is started");
        String purseId = "249";
        String purseName = "Paymaster24 MGID Asia, USD";
        String purseNumber = "Z431071031918";
        authCabAndGo(clientWagesEditUrl + CLIENT_MGID_PAYOUTS_ID);
        log.info("check if exist Paymaster24 purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("add new Paymaster24 purse");
        pagesInit.getCabPublisherClients().addPaymaster24Purse(purseId, purseNumber);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Paymaster24' doesn't created");
        log.info("check Paymaster24 detail");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkPaymaster24Detail(purseName),
                "FAIL -> Paymaster24 details!");
        log.info("Delete Paymaster24 WORLD purse");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("Test is finished");
    }

    /**
     * Add wallet Paymaster24 IDAA, USD (id=248) and check wallet params
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-28487">Ticket TA-28487</a>
     * <p>Author AIA</p>
     */
    @Test
    public void addPaymaster24MgidIDAAUsdPurse() {
        log.info("Test is started");
        String purseId = "248";
        String purseName = " Paymaster24 IDAA, USD ";
        String purseNumber = "Z431071031918";
        authCabAndGo(clientWagesEditUrl + CLIENT_ADSKEEPER_PAYOUTS_ID);
        log.info("check if exist Paymaster24 purse -> delete it");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("add new Paymaster24 purse");
        pagesInit.getCabPublisherClients().addPaymaster24Purse(purseId, purseNumber);
        Assert.assertNotNull(pagesInit.getCabPublisherClients().checkPurseDeleteButton(purseName),
                "FAIL -> purse 'Paymaster24' doesn't created");
        log.info("check Paymaster24 detail");
        Assert.assertTrue(pagesInit.getCabPublisherClients().checkPaymaster24Detail(purseName),
                "FAIL -> Paymaster24 details!");
        log.info("Delete Paymaster24 WORLD purse");
        pagesInit.getCabPublisherClients().ifExistCustomPurseDeleteIt(purseName);
        log.info("Test is finished");
    }

}
