package cab.publishers.clients;

import com.google.common.collect.ImmutableMap;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.HashMap;
import java.util.Map;

import static testData.project.EndPoints.clientWagesEditUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.Subnets.SubnetType.SCENARIO_ADSKEEPER;
import static testData.project.Subnets.SubnetType.SCENARIO_MGID;


/*
 *
 * 1. Can change ad_types in Dashboard (light/dark):
 *      - canChangeAdTypesInDashboard
 *
 * 2. Assign default teaser types for new client widgets:,
 *    Setting Ad types only for widgets:,
 *    Setting Landing types only for widgets:
 *      - assignDefaultTeaserTypesForNewClientWidgets_privilege
 *      - assignDefaultTeaserTypesForNewClientWidgets_adTypeValidation
 *      - assignDefaultTeaserTypesForNewClientWidgets_landingTypeValidation
 *      - assignDefaultTeaserTypesForNewClientWidgets_Mgid
 *      - assignDefaultTeaserTypesForNewClientWidgets_adskeeper
 */
public class CabPublisherClientsAdTypeAndLandingTypeTests extends TestBase {

    private final int clientMgidId          = 31;
    private final int websiteMgidId         = 31;
    private final int clientAdskeeperId     = 30;
    private final int websiteAdskeeperId    = 30;

    /**
     * check work privilege
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_change_widget_type_and_landing")
    @Test
    public void assignDefaultTeaserTypesForNewClientWidgets_privilege() {
        log.info("Test is started");
        try {
            log.info("Turn off rule for feature. Check that feature isn't displayed");
             helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_change_widget_type_and_landing");
            authCabForCheckPrivileges(clientWagesEditUrl + clientMgidId);
            Assert.assertFalse(pagesInit.getCabPublisherClients().isDisplayedAssignDefaultTeaserTypesForNewClientWidgets(), "FAIL -> element is visible");
        }
        finally {
            log.info("Turn on rule for feature");
             helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_change_widget_type_and_landing");
        }
        log.info("Test is finished");
    }

    /**
     * adType and Landing type: validation message
     * <p>RKO</p>
     */
    @Test
    public void assignDefaultTeaserTypesForNewClientWidgets_adTypeValidation() {
        Map<String, Boolean> adTypes = new HashMap<>() {{
            put("pg",    true);
            put("pg13", false);
            put("r",    false);
            put("nc17", false);
            put("nsfw",  true);
            put("b",     true);
            put("x",     true);
        }};

        Map<String, Boolean> landingTypes = new HashMap<>() {{
            put("g",    false);
            put("pg",   false);
            put("pg13", false);
            put("r",    false);
            put("nc17", false);
            put("nsfw", false);
            put("b",    false);
            put("x",    false);
        }};

        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + clientMgidId + "/");
        log.info("Choosing default landing and ad types");
        pagesInit.getCabPublisherClients().switchAssignDefaultTeaserTypesForNewClientWidgets(true);
        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabPublisherClients().saveSettings();
        log.info("Check error message about forbidden types for client");
        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab("Chosen ad types is forbidden for this subnet/mirror. Allowed values - pg, pg13, r, nc17, nsfw"), "FAIL -> message");
        log.info("Test is finished");
    }

    /**
     * adType and Landing type: validation message
     * <p>RKO</p>
     */
    @Test
    public void assignDefaultTeaserTypesForNewClientWidgets_landingTypeValidation() {
        Map<String, Boolean> adTypes = new HashMap<>() {{
            put("pg",   false);
            put("pg13", false);
            put("r",    false);
            put("nc17", false);
            put("nsfw", false);
            put("b",    false);
            put("x",    false);
        }};

        Map<String, Boolean> landingTypes = new HashMap<>() {{
            put("g",    false);
            put("pg",   false);
            put("pg13", false);
            put("r",     true);
            put("nc17", false);
            put("nsfw", false);
            put("b",     true);
            put("x",     true);
        }};

        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + clientMgidId + "/");
        log.info("Choosing default landing and ad types");
        pagesInit.getCabPublisherClients().switchAssignDefaultTeaserTypesForNewClientWidgets(true);
        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabPublisherClients().saveSettings();
        log.info("Check error message about forbidden types for client");
        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab("Chosen landing types is forbidden for this subnet. Allowed values - g, pg, pg13, r, nc17, nsfw, b"), "FAIL -> message");
        log.info("Test is finished");
    }

    /**
     * check set and edit adType and Landing type after create/edit widget for MGID subnet
     * <p>RKO</p>
     */
    @Test
    public void assignDefaultTeaserTypesForNewClientWidgets_mgid() {
        // initialize variables
        Map<String, Boolean> adTypes = new HashMap<>() {{
            put("pg",    true);
            put("pg13", false);
            put("r",    false);
            put("nc17", false);
            put("nsfw",  true);
            put("b",    false);
            put("x",    false);
        }};

        Map<String, Boolean> landingTypes = new HashMap<>() {{
            put("g",    false);
            put("pg",   false);
            put("pg13", false);
            put("r",     true);
            put("nc17", false);
            put("nsfw",  true);
            put("b",    false);
            put("x",    false);
        }};

        Map<String, Boolean> adTypes_edit, landingTypes_edit;
        subnetId = SCENARIO_MGID;

        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + clientMgidId);
        log.info("Choosing default landing and ad types");
        pagesInit.getCabPublisherClients().switchAssignDefaultTeaserTypesForNewClientWidgets(true);
        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Go to dash for creating widget");
        authDashAndGo("testEmail31@ex.ua","publisher/add-widget/site/" + websiteMgidId);
        pagesInit.getWidgetClass().createSimpleWidget();

        log.info("Check widget settings about default teaser and landing types");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types");

        log.info("EDIT landing and ad types for client");
        authCabAndGo(clientWagesEditUrl + clientMgidId);

        adTypes_edit = new HashMap<>(adTypes);
        adTypes_edit.put("nsfw", false);

        landingTypes_edit = new HashMap<>(landingTypes);
        landingTypes_edit.put("r", false);

        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes_edit);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes_edit);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Re save widget created earlier");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);

        log.info("Check widget adType and landing types after edit");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types after edit");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check set and edit adType and Landing type after create/edit widget for Adskeeper subnet
     * <p>RKO</p>
     */
    @Test
    public void assignDefaultTeaserTypesForNewClientWidgets_adskeeper() {
        // initialize variables
        Map<String, Boolean> adTypes = new HashMap<>() {{
            put("pg",    true);
            put("pg13", false);
            put("r",    false);
            put("nc17", false);
            put("nsfw",  true);
            put("b",    false);
            put("x",    false);
        }};

        Map<String, Boolean> landingTypes = new HashMap<>() {{
            put("g",    false);
            put("pg",   false);
            put("pg13", false);
            put("r",     true);
            put("nc17", false);
            put("nsfw", false);
            put("b",     true);
            put("x",    false);
        }};

        Map<String, Boolean> adTypes_edit, landingTypes_edit;
        subnetId = SCENARIO_ADSKEEPER;

        log.info("Test is started");

        log.info("Choosing default landing and ad types");
        authCabAndGo(clientWagesEditUrl + clientAdskeeperId);
        pagesInit.getCabPublisherClients().switchAssignDefaultTeaserTypesForNewClientWidgets(true);
        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Go to dash for creating widget");
        authDashAndGo("testEmail30@ex.ua","publisher/add-widget/site/" + websiteAdskeeperId);
        pagesInit.getWidgetClass().createSimpleWidget();

        log.info("Check widget settings about default teaser and landing types");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types");

        log.info("Choosing another default landing and ad types for client");
        authCabAndGo(clientWagesEditUrl + clientAdskeeperId);
        adTypes_edit = new HashMap<>(adTypes);
        adTypes_edit.put("nsfw", false);

        landingTypes_edit = new HashMap<>(landingTypes);
        landingTypes_edit.put("r", false);

        pagesInit.getCabPublisherClients().turnOnCustomAdTypes(adTypes_edit);
        pagesInit.getCabPublisherClients().turnOnCustomLandingTypes(landingTypes_edit);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Re save widget created earlier");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getWidgetClass().reSaveWithEditWidget();

        log.info("Check widget settings about default teaser and landing types.");
        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types after edit");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types after edit");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность отключения NSFW ad_type для виджетов Adskeeper в Dashboard
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-20972">TA-20972</a>
     */
    @Test
    public void canChangeAdTypesInDashboard() {
        subnetId = SCENARIO_ADSKEEPER;

        Map<String, Boolean> adTypes = new HashMap<>() {{
            put("pg",   true);
            put("pg13", true);
            put("r",    true);
            put("nc17", true);
            put("nsfw", true);
            put("b",    true);
            put("x",    true);
        }};

        Map<String, Boolean> landingTypes = new HashMap<>() {{
            put("g",   true);
            put("pg",   true);
            put("pg13", true);
            put("r",    true);
            put("nc17", true);
            put("nsfw", true);
            put("b",    true);
            put("x",    true);
        }};

        log.info("Test is started");
        authCabAndGo(clientWagesEditUrl + clientAdskeeperId);
        log.info("Включаем все типы лендинга и тизера для даного клиента и активируем управление опцией 'on off nsfw' для дешборда");
        pagesInit.getCabPublisherClients().switchAssignDefaultTeaserTypesForNewClientWidgets(true);
        pagesInit.getCabPublisherClients().turnOnAllAdTypes();
        pagesInit.getCabPublisherClients().turnOnAllLandingTypes();
        pagesInit.getCabPublisherClients().switchCanChangeAdTypesInDashboard(true);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Проверяем что включенная опция 'on off nsfw' отображается и что она включена");
        authDashAndGo("testEmail30@ex.ua","publisher/add-widget/site/" + websiteAdskeeperId);
        Assert.assertTrue(pagesInit.getWidgetClass()
                .setContentTypes(ImmutableMap.of("nsfw", true))
                .checkContentTypesState(), "FAIL -> nsfw is switch OFF");
        pagesInit.getWidgetClass().createSimpleWidget();

        authCabAndGo("wages/informers-edit/type/composite/id/" + pagesInit.getWidgetClass().getWidgetId());
        log.info("Проверяем что все типы включены у созданого виджета");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types");

        log.info("Вклычаем опцию 'custom styles' и проверяем что опция 'on off nsfw' включена в интерфейсе редактирования виджета");
        authCabAndGo("wages/informers-settings/id/" + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getCabEditCode().manuallyStyleSwitchOn(true);
        pagesInit.getCabEditCode().saveEditCodeInterface();
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertTrue(pagesInit.getWidgetClass()
                .setContentTypes(ImmutableMap.of("nsfw", true))
                .checkContentTypesState(), "FAIL -> nsfw doesn't show after turn on custom styles");

        log.info("Деактивируем управление опцией 'on off nsfw' для  дешборда");
        authCabAndGo(clientWagesEditUrl + clientAdskeeperId);
        pagesInit.getCabPublisherClients().switchCanChangeAdTypesInDashboard(false);
        pagesInit.getCabPublisherClients().saveSettings();

        log.info("Проверяем что функционал 'on off nsfw' не отображается");
        authDashAndGo("publisher/edit-widget/id/" + pagesInit.getWidgetClass().getWidgetId());
        softAssert.assertFalse(pagesInit.getWidgetClass().isDisplayedAdType("nsfw"), "FAIL -> nsfw is show after turn on switchCanChangeAdTypesInDashboard");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
