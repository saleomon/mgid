package cab.publishers.news;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.CabNews;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static libs.dockerClient.base.DockerKafka.Topics.GENERATE_IMAGE_HASH;
import static libs.hikari.tableQueries.partners.News1.selectShowOnTranzPage;
import static pages.cab.publishers.logic.CabNews.Statuses.DRAFT;
import static pages.cab.publishers.logic.CabNews.Statuses.QUARANTINE;
import static testData.project.AuthUserCabData.AuthorizationUsers.NEWS_MAKER_STAFFER_USER;
import static testData.project.AuthUserCabData.AuthorizationUsers.PRIVILEGE_USER;
import static testData.project.AuthUserCabData.*;
import static testData.project.CliCommandsList.Consumer.RELATED_IMAGES;
import static testData.project.ClientsEntities.EXCHANGE_CAMPAIGN_IDEALMEDIA_ID;
import static testData.project.ClientsEntities.WEBSITE_IDEALMEDIA_WAGES_DOMAIN;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabNewsTests extends TestBase {
    /**
     * create/edit/check news for Idealmedia
     * RKO
     */
    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("create/edit/check news\n" +
            "     <ul>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "create/edit/check news")
    public void createNewsForIdealmedia() {
        log.info("Test is started");

        log.info("create news");
        authCabAndGo("news-moderation/add-news/campaign_id/" + EXCHANGE_CAMPAIGN_IDEALMEDIA_ID);
        softAssert.assertNotNull(pagesInit.getCabNews()
                .setDomain(WEBSITE_IDEALMEDIA_WAGES_DOMAIN)
                .setIsOriginalTitle(true)
                .createNews(), "FAIL -> news doesn't create!!");
        log.info("check CREATED news in LIST Interface");
        softAssert.assertEquals(selectShowOnTranzPage(Integer.parseInt(pagesInit.getCabNews().getNewsId())), 1,
                "FAIL -> Created news have show_tranz_page_flag is 0!");
        authCabAndGo("wages/news?id=" + pagesInit.getCabNews().getNewsId());
        softAssert.assertTrue(pagesInit.getCabNews()
                        .setStatus(pages.cab.publishers.logic.CabNews.Statuses.ACTIVE)
                        .checkNewsInListInterface(),
                "FAIL -> CREATED news doesn't check in EDIT Interface");

        log.info("check CREATED news in EDIT Interface");
        authCabAndGo("news-moderation/creative-edit/id/" + pagesInit.getCabNews().getNewsId());
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsInEditInterface(), "FAIL -> CREATED news doesn't check in LIST Interface");
        log.info("edit news");
        pagesInit.getCabNews().isGenerateNewValues(true).setIsOriginalTitle(false).editNews();
        log.info("check EDITED news in LIST Interface");
        authCabAndGo("wages/news?id=" + pagesInit.getCabNews().getNewsId());
        softAssert.assertEquals(selectShowOnTranzPage(Integer.parseInt(pagesInit.getCabNews().getNewsId())), 1,
                "FAIL -> Created news have show_tranz_page_flag is 0!");
        softAssert.assertTrue(pagesInit.getCabNews()
                        .setStatus(pages.cab.publishers.logic.CabNews.Statuses.PENDING)
                        .checkNewsInListInterface(),
                "FAIL -> EDITED news doesn't check in EDIT Interface");

        log.info("check EDITED news in EDIT Interface");
        authCabAndGo("news-moderation/creative-edit/id/" + pagesInit.getCabNews().getNewsId());
        softAssert.assertEquals(selectShowOnTranzPage(Integer.parseInt(pagesInit.getCabNews().getNewsId())), 1,
                "FAIL -> Created news have show_tranz_page_flag is 0!");
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsInEditInterface(), "FAIL -> EDITED news doesn't check in LIST Interface");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23915">https://youtrack.mgid.com/issue/TA-23915</a>
     * Внедрение нового фильтра по куратору для интерфейса `wages/news`
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     * RKO
     */
    @Test
    public void newCuratorFilter() {
        log.info("Test is started");
        log.info("get random curator");
        String curator = helpersInit.getBaseHelper().getRandomFromArray(new String[]{indiaPublisherUser, apacPublisherUser, cisPublisherUser, europePublisherUser});
        authCabAndGo("wages/news");
        log.info("set chosen curator and search news");
        pagesInit.getCabNews().openCuratorFilterAndSearch(curator);
        authCabAndGo("news-moderation/creative-edit/id/" + pagesInit.getCabNews().getNewsIdFromLabel().get(0));
        Assert.assertEquals(pagesInit.getCabNews().getCurator(), curator, "FAIL -> news don't search by curator " + curator);
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-24058">https://youtrack.mgid.com/issue/TA-24058</a>
     * Нет возможности удалить новость на карантине не принятую в работу
     * Проверяем удаление новости (статус карантин) в списке тизеров
     * RKO
     */
    @Test
    public void deleteNews() {
        log.info("Test is started");
        int newsId = 20;
        operationMySql.getNews1().updateModerationStatus(null, newsId);
        authCabAndGo("wages/news?id=" + newsId);
        pagesInit.getCabNews().deleteNews(newsId);
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News has been successfully deleted"), "FAIL -> news doesn't deleted");
        log.info("Test is finished");
    }

    /**
     * Не обновляется дата в поле news_1.when_change при редактировании новостей инлайн в списке новостей
     * Cases
     * <ul>
     *     <li>Редактирование заголовка</li>
     *     <li>Редактирование описания</li>
     *     <li>Редактирование группы контента</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27410">Ticket TA-27410</a>
     * <p>Author AIA</p>
     */

    @Test
    public void checkTimeWhenChangedForEditingInline() {
        log.info("Test is started");
        String newsId = "41";
        String whenChangeTimeAtStart = operationMySql.getNews1().getWhenChange(newsId);
        authCabAndGo("wages/news?id=" + newsId);

        log.info("Edit news title inline");
        pagesInit.getCabNews().editTitleInline("News title new");
        sleep(1000);
        String whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for title!");
        whenChangeTimeAtStart = whenChangeTimeAtChecks;

        log.info("Edit news description inline");
        pagesInit.getCabNews().editDescriptionInline("News description new");
        sleep(1000);
        whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for description!");

        log.info("Edit news content group inline");
        pagesInit.getCabNews().changeContentGroupInline(newsId);
        sleep(1000);
        whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for content group!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Не обновляется дата в поле news_1.when_change при удалении новостей из списка новостей в корзину
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27410">Ticket TA-27410</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkMoveNewsToRecycleBinAndWhenChangedTime() {
        log.info("Test is started");
        String newsId = "2003";
        String whenChangeTimeAtStart = operationMySql.getNews1().getWhenChange(newsId);
        authCabAndGo("wages/news?id=" + newsId);
        pagesInit.getCabNews().deleteNews(Integer.parseInt(newsId));
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News has been successfully deleted"), "FAIL -> news doesn't deleted");
        softAssert.assertNotEquals(operationMySql.getNews1().getWhenChange(newsId), whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for deleting news!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Не обновляется дата в поле news_1.when_change при удалении новостей из списка новостей в 'мертвые'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27410">Ticket TA-27410</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkDeleteNewsPermanentlyAndWhenChangedTime() {
        log.info("Test is started");
        String newsId = "2004";
        String whenChangeTimeAtStart = operationMySql.getNews1().getWhenChange(newsId);
        authCabAndGo("wages/news?id=" + newsId);
        pagesInit.getCabNews().deleteNewsPermanently(Integer.parseInt(newsId));
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News has been successfully deleted"), "FAIL -> news doesn't deleted");
        softAssert.assertNotEquals(operationMySql.getNews1().getWhenChange(newsId), whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for deleting news permanently!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Status 'Draft' for news
     * Cases
     * <ul>
     *     <li>1. Cancel news moderation</li>
     *     <li>2. Send news on moderation</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51676">Ticket TA-51676</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkNewsCancelAndResendOnModeration() {
        log.info("Test is started");
        String newsId = "2018";
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news?id=" + newsId);
        softAssert.assertFalse(pagesInit.getCabNews().checkEditNewsIcon(),
                "FAIL -> edit news icon is present for news on moderation!");
        // todo AIA after fixes by https://jira.mgid.com/browse/MT-5784
        //  softAssert.assertTrue(pagesInit.getCabNews().checkCantEditImageInline(), "FAIL -> CAN edit image inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditTitleInline(), "FAIL -> CAN edit title inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditDescriptionInline(), "FAIL -> CAN edit description inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditCategoryInline(), "FAIL -> CAN edit category inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditLifetimeInline(), "FAIL -> CAN edit lifeTime inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditNewsTypeInline(), "FAIL -> CAN edit adType inline");
        softAssert.assertTrue(pagesInit.getCabNews().checkCantEditLandingInline(), "FAIL -> CAN edit landing inline");
        pagesInit.getCabNews().tryEditUrlInline("http://newssite.com");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("You can't edit teaser in the status \"On moderation\""),
                "FAIL -> don't correct message");

        log.info("Cancel news moderation");
        pagesInit.getCabNews().cancelNewsModeration();
        softAssert.assertFalse(pagesInit.getCabNews().checkCancelModerationIconIsHidden());
        softAssert.assertTrue(pagesInit.getCabNews().checkEditNewsIcon(),
                "FAIL -> edit news icon is not present for news in draft status!");
        log.info("Resend news on moderation");
        pagesInit.getCabNews().resendNewsOnModeration();
        log.info("Check 'Send news on moderation' icon is hidden");
        softAssert.assertFalse(pagesInit.getCabNews().checkSendOnModerationIconIsHidden());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Status 'Draft' for news
     * Case: check filter by status 'Draft'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51676">Ticket TA-51676</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkFilterByNewsStatusDraft() {
        log.info("Test is started");
        String newsId = "2008";
        log.info("Open news list");
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news");
        log.info("Check that news list is empty");
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsListIsEmpty(),
                "FAIL -> news list is not empty!");
        log.info("Filter news by status 'Draft'");
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news?status=drafts&id=" + newsId);
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> List with drafts news!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Status 'Draft' for news
     * Case: check editing news in status 'Draft'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51676">Ticket TA-51676</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkEditingNewsInStatusDraft() {
        log.info("Test is started");
        String newsId = "2009";
        log.info("Open news editing form");
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "news-moderation/creative-edit/id/" + newsId);
        pagesInit.getCabNews().saveNews();
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news?id=" + newsId);
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> news content wasn't changed but status was changed!");
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "news-moderation/creative-edit/id/" + newsId);
        pagesInit.getCabNews().setDomain("test-site-with-video.com")
                .editNews();
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news?id=" + newsId);
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(QUARANTINE),
                "FAIL -> news content was changed but status wasn't changed!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Status 'Draft' for news
     * Case: check inline editing news in status 'Draft'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-51676">Ticket TA-51676</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkEditingNewsInStatusDraftInline() {
        log.info("Test is started");
        String newsId = "2010";
        log.info("Open news list");
        authCabAndGo(NEWS_MAKER_STAFFER_USER, "wages/news?id=" + newsId);
        pagesInit.getCabNews().editTitleInline("News title new");
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit title");
        pagesInit.getCabNews().editDescriptionInline("News description new");
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit description");
        pagesInit.getCabNews().editCategoryInline();
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit category");
        pagesInit.getCabNews().editLifetimeInline();
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit news lifetime");
        pagesInit.getCabNews().editNewsTypeInline();
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit news type");
        pagesInit.getCabNews().editUrlInline();
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsStatus(DRAFT),
                "FAIL -> Inline edit URL");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Создание и редактирование новости с неразрывными пробелами заголовке и описании\n" +
            "     <ul>\n" +
            "      <li>в интерфейсе создания новости</li>\n" +
            "      <li>в интерфейсе редактирования новости</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test
    public void createNewsWithNbsp() {
        log.info("Test is started");
        String originalStringValue = "Test&nbsp;news&nbsp;with&nbsp;&nbsp.";
        String checkStringValue = "Test news with &nbsp.";

        log.info("Create news");
        authCabAndGo("news-moderation/add-news/campaign_id/" + 2001);
        softAssert.assertNotNull(pagesInit.getCabNews()
                .setDomain("https://test-site-with-video.com")
                .setTitle(originalStringValue)
                .setDescription(originalStringValue)
                .createNews(), "FAIL -> news doesn't create!!");

        log.info("check CREATED news in LIST Interface");
        authCabAndGo("wages/news?id=" + pagesInit.getCabNews().getNewsId());
        softAssert.assertTrue(pagesInit.getCabNews()
                        .checkNewsWithNbspInListInterface(checkStringValue),
                "FAIL -> Check for CREATED news in LIST Interface");

        log.info("edit news");
        authCabAndGo("news-moderation/creative-edit/id/" + pagesInit.getCabNews().getNewsId());
        pagesInit.getCabNews()
                .setTitle(originalStringValue)
                .setDescription(originalStringValue)
                .editNews();

        log.info("check EDITED news in EDIT Interface");
        authCabAndGo("news-moderation/creative-edit/id/" + pagesInit.getCabNews().getNewsId());
        softAssert.assertTrue(pagesInit.getCabNews().checkNewsWithNbspInEditInterface(checkStringValue),
                "FAIL -> Check for EDITED news doesn't check in EDIT Interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Редактирование заголовка и описания новости с неразрывными пробелами инлайн\n" +
            "     <ul>\n" +
            "      <li>в интерфейсе списка новостей</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Редактирование заголовка и описания новости с неразрывными пробелами инлайн в интерфейсе списка новостей")
    public void checkNbspForEditingInline() {
        log.info("Test is started");
        String newsId = "2017";
        String originalStringValue = "Test&nbsp;news&nbsp;with&nbsp;&nbsp";
        String checkStringValue = "Test news with &nbsp";
        authCabAndGo("wages/news?id=" + newsId);

        log.info("Edit news title inline");
        pagesInit.getCabNews().editTitleInline(originalStringValue);
        pagesInit.getCabNews().editDescriptionInline(originalStringValue);
        refresh();
        softAssert.assertEquals(pagesInit.getCabNews().getTitleInline(), checkStringValue,
                "FAIL -> Nbsp is still present for title!");
        softAssert.assertEquals(pagesInit.getCabNews().getDescriptionInline(), checkStringValue,
                "FAIL -> Nbsp is still present for description!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("Check change state original title by icon\n" +
            "     <ul>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check change state original title by icon")
    public void changeStateOriginalTitleByIcon() {
        log.info("Test is started");
        int newsId = 1000;
        authCabAndGo("wages/news?id=" + newsId);

        pagesInit.getCabNews().changeStateOriginalTitleIcon(true);
        softAssert.assertTrue(pagesInit.getCabNews().checkStateOriginalTitleIcon(true), "FAIL -> first change");

        pagesInit.getCabNews().changeStateOriginalTitleIcon(false);
        softAssert.assertTrue(pagesInit.getCabNews().checkStateOriginalTitleIcon(false), "FAIL -> second change");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("Check privileges for interfaces\n" +
            "     <ul>\n" +
            "      <li>List interface</li>\n" +
            "      <li>Edit interface</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check privileges original title")
    public void checkPrivilegeOriginalTitle() {
        log.info("Test is started");
        int newsId = 1000;

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "news/news-change-original-title-flag");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "news-moderation/can_manage_original_title_flag");

        authCabForCheckPrivileges("wages/news?id=" + newsId);
        softAssert.assertFalse(pagesInit.getCabNews().isDisplayedOriginalTitleIcon(), "FAIL -> icon");

        authCabForCheckPrivileges("news-moderation/creative-edit/id/" + newsId);
        softAssert.assertFalse(pagesInit.getCabNews().isDisplayedOriginalTitleCheckbox(), "FAIL -> checkbox");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("Check filter original title \n" +
            "     <ul>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check filter original title")
    public void checkFilterByOriginalTitle() {
        log.info("Test is started");

        authCabAndGo("wages/news");
        pagesInit.getCabNews().filterOriginalTitle("1");
        softAssert.assertEquals(pagesInit.getCabNews().getAmountOfNews(), pagesInit.getCabNews().getAmountOfNewsWithOriginalTitle(), "FAIL - first check");
        softAssert.assertNotEquals(pagesInit.getCabNews().getAmountOfNews(), 0, "FAIL - first check for empty list");

        pagesInit.getCabNews().filterOriginalTitle("0");
        softAssert.assertEquals(pagesInit.getCabNews().getAmountOfNews(), pagesInit.getCabNews().getAmountOfNewsWithoutOriginalTitle(), "FAIL - second check");
        softAssert.assertNotEquals(pagesInit.getCabNews().getAmountOfNews(), 0, "FAIL - second check for empty list");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Redirect users from error page \"Not Found\" on a transit page")
    @Story("Add hint to delete news option")
    @Description("Checking the setting of the flag \"Do not show news on transit\" after its deletion \n" +
            "     <ul>\n" +
            "      <li>Checking that a new flag is displayed in the delete news popup</li>\n" +
            "      <li>Checking that flag `news_1.show_on_tranz_page` set in value 0, if news was deleted</li>\n" +
            "      <li>Checking that flag `news_1.show_on_tranz_page` set in value 1, if news was restored</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-52025\">Ticket TA-52025</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test(description = "Check work with flag `show_on_tranz_page`")
    public void checkSetFlagShowOnTranzPageInNewsListInterface(){
        log.info("Test is started");
        int newsId = 2019;
        operationMySql.getNews1().updateModerationStatus(null, newsId);
        authCabAndGo("wages/news?id=" + newsId);
        pagesInit.getCabNews().deleteNewsWithHideOnTranzPage(newsId);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News has been successfully deleted"), "FAIL -> news doesn't deleted");
        softAssert.assertEquals(selectShowOnTranzPage(newsId), 0,
                "FAIL -> Created news have show_tranz_page_flag is 1!");
        pagesInit.getCabNews().restoreNews();
        softAssert.assertEquals(selectShowOnTranzPage(newsId), 1,
                "FAIL -> Created news have show_tranz_page_flag is 0!");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Redirect users from error page \"Not Found\" on a transit page")
    @Story("Add hint to delete news option")
    @Description("Checking the setting of the flag \"Do not show news on transit\" after its deletion \n in mass action" +
            "     <ul>\n" +
            "      <li>Checking that a new flag is displayed in the delete news popup</li>\n" +
            "      <li>Checking that flag `news_1.show_on_tranz_page` set in value 0, if news was deleted</li>\n" +
            "      <li>Checking that flag `news_1.show_on_tranz_page` set in value 1, if news was restored</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-52025\">Ticket TA-52025</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Test(description = "Check work with flag `show_on_tranz_page` in news mass actions")
    public void checkSetFlagShowOnTranzPageInNewsListInterfaceInMassActions(){
        log.info("Test is started");
        int firstNewsId = 2019;
        int secondNewsId = 2020;
        operationMySql.getNews1().updateModerationStatus(null, firstNewsId);
        operationMySql.getNews1().updateModerationStatus(null, secondNewsId);
        authCabAndGo("wages/news?id=" + firstNewsId + ", " +  secondNewsId + "&campaign_id=2008");
        pagesInit.getCabNews().clickOnMassActionCheckbox(firstNewsId, secondNewsId);
        pagesInit.getCabNews().chooseMassActions(CabNews.MassAction.MASS_ACTION_DELETE_NEWS_IN_BIN);
        pagesInit.getCabNews().setDeleteReason();
        pagesInit.getCabNews().submitMassActions();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Changes submitted successfully"), "FAIL -> news doesn't deleted");
        softAssert.assertEquals(selectShowOnTranzPage(firstNewsId), 0,
                "FAIL -> Created news have show_tranz_page_flag is 1!");
        softAssert.assertEquals(selectShowOnTranzPage(secondNewsId), 0,
                "FAIL -> Created news have show_tranz_page_flag is 1!");
        pagesInit.getCabNews().restoreEachNews(firstNewsId, secondNewsId);
        softAssert.assertEquals(selectShowOnTranzPage(firstNewsId), 1,
                "FAIL -> Created news have show_tranz_page_flag is 0!");
        softAssert.assertEquals(selectShowOnTranzPage(secondNewsId), 1,
                "FAIL -> Created news have show_tranz_page_flag is 1!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Redirect users from error page \"Not Found\" on a transit page")
    @Story("Add hint to delete news option")
    @Description("Check work rule `can_manage_show_on_tranz_page_flag`" +
            "     <ul>\n" +
            "      <li>Checking that if rule is off, checkbox `show_on_tranz_page` isn't displayed in manual deletion and in mass actions</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-52025\">Ticket TA-52025</a></li>\n" +
            "      <li><p>Author MVV</p></li>\n" +
            "     </ul>")
    @Privilege(name = "news/can_manage_show_on_tranz_page_flag")
    @Test(description = "Check work rule `news/can_manage_show_on_tranz_page_flag`")
    public void checkRuleCanManageShowOnTranzPageFlag(){
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "news/can_manage_show_on_tranz_page_flag");
        int newsId = 2019;
        operationMySql.getNews1().updateModerationStatus(null, newsId);
        authCabAndGo(PRIVILEGE_USER, "wages/news?id=" + newsId + "&campaign_id=2008");
        log.info("Check that flag isn't displayed in mass action");
        pagesInit.getCabNews().clickOnMassActionCheckbox(newsId);
        pagesInit.getCabNews().chooseMassActions(CabNews.MassAction.MASS_ACTION_DELETE_NEWS_IN_BIN);
        softAssert.assertFalse(pagesInit.getCabNews().tranzPageNotDisplayedFlagInMassAction(),
                "FAIL -> Flag `show_on_tranz_page` is displayed!");
        log.info("Check that flag isn't displayed in delete pop-up");
        softAssert.assertFalse(pagesInit.getCabNews().tranzPageNotDisplayedFlagInDeletePopup(newsId),
                "FAIL -> Flag `show_on_tranz_page` is displayed!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for fields:\n" +
            "     <ul>\n" +
            "      <li>Link</li>\n" +
            "      <li>Title</li>\n" +
            "      <li>Advertising text</li>\n" +
            "      <li>UPLOAD image link </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for fields create interface")
    public void checkResetButtonsCreateInterface() {
        log.info("Test is started");
        authCabAndGo("news-moderation/add-news/campaign_id/" + EXCHANGE_CAMPAIGN_IDEALMEDIA_ID);

        log.info("create teaser");
        pagesInit.getCabNews()
                .fillDomain(WEBSITE_IDEALMEDIA_WAGES_DOMAIN)
                .fillTitle("Some title")
                .fillAdvertText("Hi there")
                .fillImageField("space_journey.gif");

        softAssert.assertEquals(pagesInit.getCabNews().getDomainValue(), "https://" + WEBSITE_IDEALMEDIA_WAGES_DOMAIN, "FAIL - domain first");
        softAssert.assertEquals(pagesInit.getCabNews().getTitleValue(), "Some title", "FAIL - title first");
        softAssert.assertEquals(pagesInit.getCabNews().getAdvertValue(), "Hi there", "FAIL - Advert first");
        softAssert.assertTrue(pagesInit.getCabNews().getImageLinkValue().contains(".gif"), "FAIL - image link first");
        pagesInit.getCabNews().chooseCategory();
        softAssert.assertEquals(pagesInit.getCabNews().getAmountResetButtons(), 4,  "FAIL - first check first");

        pagesInit.getCabNews().resetAllFieldsByResetButtons();

        pagesInit.getCabNews().chooseCategory();

        softAssert.assertEquals(pagesInit.getCabNews().getDomainValue(), "https://", "FAIL - domain second");
        softAssert.assertEquals(pagesInit.getCabNews().getTitleValue(), "", "FAIL - title second");
        softAssert.assertEquals(pagesInit.getCabNews().getAdvertValue(), "", "FAIL - Advert second");
        softAssert.assertEquals(pagesInit.getCabNews().getImageLinkValue(), "", "FAIL - image link  second");
        softAssert.assertEquals(pagesInit.getCabNews().getAmountResetButtons(), 1,  "FAIL - second check");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for fields:\n" +
            "     <ul>\n" +
            "      <li>Link</li>\n" +
            "      <li>Title</li>\n" +
            "      <li>Advertising text</li>\n" +
            "      <li>UPLOAD image link </li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for fields edit interface")
    public void checkResetButtonsEditInterface() {
        log.info("Test is started");
        authCabAndGo("news-moderation/creative-edit/id/" + 1003);

        helpersInit.getMessageHelper().isSuccessMessagesCab(true);

        softAssert.assertEquals(pagesInit.getCabNews().getAmountResetButtons(), 4,  "FAIL - first check first");
        pagesInit.getCabNews().resetAllFieldsByResetButtons();

        pagesInit.getCabNews().chooseCategory();
        softAssert.assertEquals(pagesInit.getCabNews().getDomainValue(), "https://", "FAIL - domain second");
        softAssert.assertEquals(pagesInit.getCabNews().getTitleValue(), "", "FAIL - title second");
        softAssert.assertEquals(pagesInit.getCabNews().getAdvertValue(), "", "FAIL - Advert second");
        softAssert.assertEquals(pagesInit.getCabNews().getImageLinkValue(), "", "FAIL - image link  second");
        softAssert.assertEquals(pagesInit.getCabNews().getAmountResetButtons(), 1,  "FAIL - first check first  second");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Удалять содержимое в формах создания одним действием")
    @Description("Check reset buttons for image url inline:\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52017\">Ticket TA-52017</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check reset buttons for image url inline")
    public void checkResetButtonsImageLinkInline() {
        log.info("Test is started");
        authCabAndGo("wages/news?id=" + 1003);

        pagesInit.getCabNews().openPopupEditImageInline();

        pagesInit.getCabNews().resetImageFieldByResetButton();

        Assert.assertEquals(pagesInit.getCabNews().getImageLinkInlineValue(), "");
        log.info("Test is finished");
    }

    @Feature("Comparison system оf similar images for news")
    @Story("Comparison system оf similar images for news | Similarity Interface elements")
    @Description("Check comparison images icons:\n" +
            "     <ul>\n" +
            "      <li>clock-16.png</li>\n" +
            "      <li>no_relates.png</li>\n" +
            "      <li>has_relates.png</li>\n" +
            "      <li>check related news</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52079\">Ticket TA-52079</a></li>\n" +
            "      <li><p>NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check comparison images icons")
    public void checkComparisonImagesIcon() {
        log.info("Test is started");

        authCabAndGo("wages/news?id=" + 1004);
        softAssert.assertTrue(pagesInit.getCabNews().checkVisibilityIconOfStatusForComparisonImages("clock-16.png"), "Fail - clock-16");

        authCabAndGo("wages/news?id=" + 1005);
        softAssert.assertTrue(pagesInit.getCabNews().checkVisibilityIconOfStatusForComparisonImages("no_relates.png"), "Fail - no_relates");

        authCabAndGo("wages/news?id=" + 1006);
        softAssert.assertTrue(pagesInit.getCabNews().checkVisibilityIconOfStatusForComparisonImages("has_relates.png"), "Fail - has_relates");

        authCabAndGo(pagesInit.getCabNews().getlinkOfComparisonImagesIcon("has_relates.png"));
        softAssert.assertEquals(pagesInit.getCabNews().getNewsIdFromLabel().get(0), "1007", "Fail - readNewsId");

        authCabAndGo("wages/news?id=" + 1008);
        softAssert.assertTrue(pagesInit.getCabNews().checkVisibilityIconOfStatusForComparisonImages("clock-16.png"), "Fail - clock-16 2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Comparison system оf similar images for news")
    @Story("Comparison system оf similar images for news | Similarity Interface elements")
    @Description("Check filter related images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52079\">Ticket TA-52079</a></li>\n" +
            "      <li><p>NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check filter related images")
    public void checkFilterRelatedImages() {
        log.info("Test is started");
        List<String> ids = Arrays.asList("1007", "1008");

        authCabAndGo("wages/news");
        pagesInit.getCabNews().filterByRelatedImages("1006");
        softAssert.assertEquals(pagesInit.getCabNews().getAmountOfNews(), 2, "Fail - getAmountOfNews");
        softAssert.assertTrue(pagesInit.getCabNews().getNewsIdFromLabel().containsAll(ids), "Fail - ids");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Comparison system оf similar images for news")
    @Story("Comparison system оf similar images for news | Similarity Interface elements")
    @Description("Check filter related images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52079\">Ticket TA-52079</a></li>\n" +
            "      <li><p>NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check filter related images")
    public void checkCentrifugoRelatedImages() {
        try {
            log.info("Test is started");
            serviceInit.getDockerCli().runConsumer(RELATED_IMAGES, "-vvv");

            authCabAndGo("wages/news?id=1009");
            softAssert.assertTrue(pagesInit.getCabNews().checkVisibilityIconOfStatusForComparisonImages("clock-16.png"), "FAIL - first state");
            pagesInit.getCabNews().openPopupEditImageInline();
            pagesInit.getCabNews().loadImageInline("mount.jpg");
            serviceInit.getDockerKafka().waitUntilAllPartitionsFinished(GENERATE_IMAGE_HASH);
            softAssert.assertTrue(pagesInit.getCabNews().waitVisibilityIconOfStatusForComparisonImages("no_relates.png"), "FAIL - third state");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            serviceInit.getDockerCli().stopConsumer();
        }

    }
}
