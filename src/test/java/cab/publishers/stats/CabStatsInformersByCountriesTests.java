package cab.publishers.stats;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;

public class CabStatsInformersByCountriesTests extends TestBase {

    @Epic("Cab Publisher Statistics")
    @Story("Cab Publisher Statistics by countries")
    @Description("<ul>" +
            "<li>widget_clicks_goods</li>" +
            "<li>widget_statistics_shows_cpm_summ</li>" +
            "<li>widget_statistics_shows_rtb_summ</li>" +
            "<li>widget_statistics_summ</li>" +
            "<li>widget_statistics_video</li>" +
            "<li>widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "check stats informers by countries")
    public void checkStatsInformersByCountries(){
        log.info("Test is started");
        int composites = 535;
        String country = "Kyrgyzstan";
        authCabAndGo("wages/stats-informers-by-countries?composites=" + composites + "&timezone=Pacific%2FHonolulu");
        log.info("Set country name for getting it's statistics");
        pagesInit.getCabStatsInformersByCountries().setCountry(country);
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("impressions_1"), "2", "FAIL -> impressions_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("vrate_1"), "33.33", "FAIL -> vrate_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("clicks_1"), "3", "FAIL -> clicks_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("ctr_1"), "150.000", "FAIL -> ctr_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("cpm_1"), "22.569", "FAIL -> cpm_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("ecpm_1"), "67.707", "FAIL -> ecpm_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("wages_1"), "0.14", "FAIL -> wages_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("profit_1"), "0.07", "FAIL -> profit_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("profitP_1"), "35.00", "FAIL -> profitP_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("shows_2"), "11", "FAIL -> shows_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("impressions_2"), "5", "FAIL -> impressions_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("vrate_2"), "45.45", "FAIL -> vrate_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("clicks_2"), "5", "FAIL -> clicks_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("ctr_2"), "100.000", "FAIL -> ctr_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("cpm_2"), "19.499", "FAIL -> cpm_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("ecpm_2"), "42.898", "FAIL -> ecpm_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("wages_2"), "0.21", "FAIL -> wages_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("profit_2"), "0.09", "FAIL -> profit_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersByCountries().getValueByHeader("profitP_2"), "29.24", "FAIL -> profitP_2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Cab Publisher Statistics")
    @Story("Cab Publisher Statistics by countries")
    @Description("<ul>" +
            "<li>widget_clicks_goods</li>" +
            "<li>widget_statistics_shows_cpm_summ</li>" +
            "<li>widget_statistics_shows_rtb_summ</li>" +
            "<li>widget_statistics_summ</li>" +
            "<li>widget_statistics_video</li>" +
            "<li>widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "check export stats informers by countries")
    public void checkExportStatsInformersByCountries(){
        String[] headers = {
                "Ad requests", "Impressions", "Imp %", "Clicks", "CTR %", "CPM $", "eCPM $", "Wages $", "Profitability $", "Profitability %",
                "Ad requests", "Impressions", "Imp %", "Clicks", "CTR %", "CPM $", "eCPM $", "Wages $", "Profitability $", "Profitability %",
                "Ad requests %", "Ad requests", "Imp %", "Impressions", "Clicks %", "Clicks", "Wages %", "Wages $", "Profitability $", "Profitability %"};

        log.info("Test is started");
        int composites = 535;
        String country = "Kyrgyzstan";
        authCabAndGo("wages/stats-informers-by-countries?composites=" + composites + "&timezone=Pacific%2FHonolulu");
        log.info("Set country name for getting it's statistics");
        pagesInit.getCabStatsInformersByCountries().setCountry(country);
        pagesInit.getCabStatsInformersByCountries().clickDownloadStats();
        softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                .setCustomRowNumber(2)
                .loadExportedFileCab(), "FAIL -> download");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers, 1),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Impressions", "2"), "FAIL -> Impressions");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Imp %", "33.33"), "FAIL -> Imp %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Clicks", "3"), "FAIL -> Clicks");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR %", "150.000"), "FAIL -> CTR %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPM $", "22.569"), "FAIL -> CPM $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("eCPM $", "67.707"), "FAIL -> eCPM $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Wages $", "0.14"), "FAIL -> Wages $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Profitability $", "0.07"), "FAIL -> Profitability $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Profitability %", "35.00"), "FAIL -> Profitability %");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
