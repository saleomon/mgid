package cab.publishers.stats;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

public class CabStatsInformersBySitesTests extends TestBase {

    /**
     * Ошибка в консоли на графиках почасовых срезов
     * @see <a href="https://youtrack.mgid.com/issue/TA-24275">TA-24275</a>
     * <p>RKO</p>
     */
    @Test
    public void checkShowGraphOnErrors(){
        log.info("Test is started");
        authCabAndGo("wages/stats-informers-by-sites?domain=testsitemg.com&timezone=Pacific%2FHonolulu");
        pagesInit.getCabStatsInformersBySites().clickIconGraphForGetDataSite();
        Assert.assertTrue(pagesInit.getCabStatsInformersBySites().clickSomeDataInGraphAndCheckOnErrors());
        log.info("Test is finished");
    }

    @Epic("Cab Publisher Statistics")
    @Story("Cab Publisher Statistics by site")
    @Description("<ul>" +
            "<li>widget_clicks_goods</li>" +
            "<li>widget_statistics_shows_cpm_summ</li>" +
            "<li>widget_statistics_shows_rtb_summ</li>" +
            "<li>widget_statistics_summ</li>" +
            "<li>widget_statistics_video</li>" +
            "<li>widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "check stats informers by sites")
    public void checkStatsInformersBySites(){
        log.info("Test is started");
        int siteId = 136;
        String domain = "testsite136.com";
        authCabAndGo("wages/stats-informers-by-sites?domain=" + domain + "&timezone=Pacific%2FHonolulu");
        log.info("Set site id for getting it's statistics");
        pagesInit.getCabStatsInformersBySites().setSiteId(siteId);
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("shows_1"), "6", "FAIL -> shows_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("impressions_1"), "2", "FAIL -> impressions_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("vrate_1"), "33.33", "FAIL -> vrate_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("clicks_1"), "3", "FAIL -> clicks_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("ctr_1"), "150.000", "FAIL -> ctr_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("cpm_1"), "22.569", "FAIL -> cpm_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("ecpm_1"), "67.707", "FAIL -> ecpm_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("wages_1"), "0.14", "FAIL -> wages_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("profit_1"), "0.07", "FAIL -> profit_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("profitP_1"), "35.00", "FAIL -> profitP_1");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("shows_2"), "11", "FAIL -> shows_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("impressions_2"), "5", "FAIL -> impressions_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("vrate_2"), "45.45", "FAIL -> vrate_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("clicks_2"), "5", "FAIL -> clicks_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("ctr_2"), "100.000", "FAIL -> ctr_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("cpm_2"), "19.499", "FAIL -> cpm_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("ecpm_2"), "42.898", "FAIL -> ecpm_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("wages_2"), "0.21", "FAIL -> wages_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("profit_2"), "0.09", "FAIL -> profit_2");
        softAssert.assertEquals(pagesInit.getCabStatsInformersBySites().getValueByHeader("profitP_2"), "29.24", "FAIL -> profitP_2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Cab Publisher Statistics")
    @Story("Cab Publisher Statistics by site")
    @Description("<ul>" +
            "<li>widget_clicks_goods</li>" +
            "<li>widget_statistics_shows_cpm_summ</li>" +
            "<li>widget_statistics_shows_rtb_summ</li>" +
            "<li>widget_statistics_summ</li>" +
            "<li>widget_statistics_video</li>" +
            "<li>widget_statistics_google_ads</li>" +
            "</ul>")
    @Owner("RKO")
    @Test(description = "check export stats informers by sites")
    public void checkExportStatsInformersBySites(){
        String[] headers = {
                "Ad requests", "Impressions", "Imp %", "Clicks", "CTR %", "CPM $", "eCPM $", "Wages $", "Profitability $", "Profitability %",
                "Ad requests", "Impressions", "Imp %", "Clicks", "CTR %", "CPM $", "eCPM $", "Wages $", "Profitability $", "Profitability %",
                "Ad requests %", "Ad requests", "Imp %", "Impressions", "Clicks %", "Clicks", "Wages %", "Wages $", "Profitability $", "Profitability %"};

        log.info("Test is started");
        int siteId = 136;
        String domain = "testsite136.com";
        authCabAndGo("wages/stats-informers-by-sites?domain=" + domain + "&timezone=Pacific%2FHonolulu");
        log.info("Set site id for getting it's statistics");
        pagesInit.getCabStatsInformersBySites().setSiteId(siteId);
        pagesInit.getCabStatsInformersBySites().clickDownloadStats();
        softAssert.assertTrue(serviceInit.getExportFileTableStructure()
                .setCustomRowNumber(2)
                .loadExportedFileCab(), "FAIL -> download");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers, 1),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Ad requests", "6"), "FAIL -> Ad requests");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Impressions", "2"), "FAIL -> Impressions");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Imp %", "33.33"), "FAIL -> Imp %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Clicks", "3"), "FAIL -> Clicks");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CTR %", "150.000"), "FAIL -> CTR %");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("CPM $", "22.569"), "FAIL -> CPM $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("eCPM $", "67.707"), "FAIL -> eCPM $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Wages $", "0.14"), "FAIL -> Wages $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Profitability $", "0.07"), "FAIL -> Profitability $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Profitability %", "35.00"), "FAIL -> Profitability %");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
