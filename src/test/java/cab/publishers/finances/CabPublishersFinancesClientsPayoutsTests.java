package cab.publishers.finances;

import org.testng.annotations.*;
import testBase.TestBase;

public class CabPublishersFinancesClientsPayoutsTests extends TestBase {
    /**
     * Check payouts color if wallet details was changed
     * @see <a href=https://youtrack.mgid.com/issue/TA-24333>Ticket TA-24333</a>
     * <p>Author AIA</p>
     */
     @Test
    public void checkPayoutsColor() {
        log.info("Test is started");
        log.info("Auth in Cab and go to Clients Payments list");
        authCabAndGo("wages/clients-payments?status=1&client_id=" + 2005);
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPayoutsColor("#dfe5fe"),
                "FAIL -> pending transaction color!");
        authCabAndGo("wages/clients-payments?status=2&client_id=" + 2005);
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkPayoutsColor("#dfe5fe"),
                "FAIL -> approved transaction color!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Client's payout region
     * Добавить фильтр "Region of payout system" в интерфейс https://admin.mgid.com/cab/wages/clients-payments
     * <ul>
     * <li>Значения: all + Asia Client</li>
     * <li>По умолчанию all (выбираем всех клиентов)</li>
     * </ul>
     * @see <a href=https://youtrack.mgid.com/issue/TA-24496>Ticket TA-24496</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkFilterRegionOfPayoutSystemForAsiaClients() {
        log.info("Test is started");
        authCabAndGo("wages/clients-payments");
        softAssert.assertEquals(pagesInit.getCabPublisherPayouts().getValueForClientsPaymentsByRegion(), "All");
        log.info("* Select filter by Asia payout system *");
        pagesInit.getCabPublisherPayouts().findClientsPaymentsByRegion("1");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsPayments("asia"),
                "FAIL -> Filter for Region Of Payout System - Asia!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Добавить фильтр "Publisher currency" в интерфейс Clients payouts
     *
     * @see <a href=https://youtrack.mgid.com/issue/TA-27290>Ticket TA-27290</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkFilterByPublisherCurrency() {
        log.info("Test is started");
        authCabAndGo("wages/clients-payments");
        log.info("* Check default 'Publisher currency' value * ");
        softAssert.assertEquals(pagesInit.getCabPublisherPayouts().getValueForPublisherCurrency(), "All");
        log.info("* Select filter by 'USD' publishers *");
        pagesInit.getCabPublisherPayouts().findClientsPaymentsByPublisherCurrency("usd");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsCurrency("$"),
                "FAIL -> Filter by Publisher currency - USD!");
        log.info("* Select filter by 'EUR' publishers *");
        pagesInit.getCabPublisherPayouts().findClientsPaymentsByPublisherCurrency("eur");
        softAssert.assertTrue(pagesInit.getCabPublisherPayouts().checkClientsCurrency("€"),
                "FAIL -> Filter by Publisher currency - EUR!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
