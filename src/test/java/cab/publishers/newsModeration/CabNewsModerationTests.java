package cab.publishers.newsModeration;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.isEqualsLists;
import static testData.project.ClientsEntities.EXCHANGE_CAMPAIGN_IDEALMEDIA_ID;

public class CabNewsModerationTests extends TestBase {

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23933">https://youtrack.mgid.com/issue/TA-23933</a>
     * Ошибка при массовом изменении типа новости в интерфейсе карантина
     * mass change AdType
     * RKO
     */
    @Test
    public void massChangeAdType() {
        log.info("Test is started");
        authCabAndGo("news-moderation/quarantine?campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_ID);
        pagesInit.getCabNewsModeration().editAdTypeMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message doesn't show");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkAdTypeForAllTeasersOnPage(pagesInit.getCabNewsModeration().getAdType()), "FAIL -> adType don't correct");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23933">https://youtrack.mgid.com/issue/TA-23933</a>
     * Ошибка при массовом изменении типа новости в интерфейсе карантина
     * inline change AdType
     * RKO
     */
    @Test
    public void inlineChangeAdType() {
        log.info("Test is started");
        authCabAndGo("news-moderation/quarantine?id=" + 62);
        pagesInit.getCabNewsModeration().editAdTypeInline();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message doesn't show");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkAdTypeForAllTeasersOnPage(pagesInit.getCabNewsModeration().getAdType()), "FAIL -> adType don't correct");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check if there are not errors on the page after filtering by non-existent campaign
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24585">Ticket TA-24585</a>
     * <p>Author AIA</p>
     */
    @Test
    public void errorsCheckingAfterFiltering() {
        log.info("Test is started");
        int nonexistentCampaignId = operationMySql.getgPartners1().selectLastCampaignId() + 1;
        authCabAndGo("news-moderation/quarantine?campaign_id=" + nonexistentCampaignId);
        Assert.assertEquals(pagesInit.getCabNewsModeration().getEmptyTableText(), "No news",
                "FAIL -> There are errors on the page!");
        log.info("Test is finished");
    }

    /**
     * Не обновляется дата в поле news_1.when_change при редактировании новостей инлайн на модерации
     * Cases
     * <ul>
     *     <li>Редактирование заголовка</li>
     *     <li>Редактирование описания</li>
     *     <li>Редактирование группы контента</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27410">Ticket TA-27410</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkTimeWhenChangedForEditingInline() {
        log.info("Test is started");
        String newsId = "2015";
        String whenChangeTimeAtStart = operationMySql.getNews1().getWhenChange(newsId);
        authCabAndGo("news-moderation/quarantine?id=" + newsId);

        log.info("Edit news title inline");
        pagesInit.getCabNewsModeration().editTitleInline("News title new");
        sleep(1000);
        String whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for title!");
        whenChangeTimeAtStart = whenChangeTimeAtChecks;

        log.info("Edit news description inline");
        pagesInit.getCabNewsModeration().editDescriptionInline("News description new");
        sleep(1000);
        whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for description!");

        log.info("Edit news content group inline");
        pagesInit.getCabNewsModeration().changeContentGroupInline(newsId);
        sleep(1000);
        whenChangeTimeAtChecks = operationMySql.getNews1().getWhenChange(newsId);
        softAssert.assertNotEquals(whenChangeTimeAtChecks, whenChangeTimeAtStart,
                "FAIL -> Time whenChange is not changed for content group!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Проверка иконки для новости с неразрывными пробелами в заголовке и описании\n" +
            "     <ul>\n" +
            "      <li>в интерфейсе карантина</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Проверка иконки для новости с неразрывными пробелами в заголовке и описании в интерфейсе карантина")
    public void checkNbspInNews() {
        log.info("Test is started");
        String newsId = "2011";
        authCabAndGo("news-moderation/quarantine?id=" + newsId);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkErrorIconForNewsTitle(),
                "FAIL -> There is not icon for news title with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkTitleOfErrorIconForNewsTitle(
                "The title contains special characters! Go to edit to see and verify the spelling of the title."),
                "FAIL -> Error in icon's text title for news title with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkErrorIconForNewsDescription(),
                "FAIL -> There is not icon for news description with &nbsp!");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkTitleOfErrorIconForNewsDescription(
                "The description contains special characters! Go to edit to see and verify the spelling of the description."),
                "FAIL -> Error in icon's text title for news description with &nbsp!");

        softAssert.assertAll();
        log.info("Test is finished");

    }

    @Story("Валидация неразрывного пробела в тайтле новостей и тизеров")
    @Description("Редактирование заголовка и описания новости с неразрывными пробелами инлайн\n" +
            "     <ul>\n" +
            "      <li>в интерфейсе модерации</li>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51792\">Ticket TA-51792</a></li>\n" +
            "      <li><p>Author AIA</p></li>\n" +
            "     </ul>")
    @Test(description = "Редактирование заголовка и описания новости с неразрывными пробелами инлайн в интерфейсе модерации")
    public void checkNbspForEditingInline() {
        log.info("Test is started");
        String newsId = "2016";
        String originalStringValue = "Test&nbsp;news&nbsp;with&nbsp;&nbsp";
        String checkStringValue = "Test news with &nbsp";
        authCabAndGo("news-moderation/quarantine?id=" + newsId);

        log.info("Edit news title inline");
        pagesInit.getCabNewsModeration().editTitleInline(originalStringValue);
        pagesInit.getCabNewsModeration().editDescriptionInline(originalStringValue);
        refresh();
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getTitleInline(), checkStringValue,
                "FAIL -> Nbsp is still present for title!");
        softAssert.assertFalse(pagesInit.getCabNewsModeration().checkErrorIconForNewsTitle(),
                "FAIL -> There is error NBSP icon for teaser title without &nbsp!");
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getDescriptionInline(), checkStringValue,
                "FAIL -> Nbsp is still present for description!");
        softAssert.assertFalse(pagesInit.getCabNewsModeration().checkErrorIconForNewsDescription(),
                "FAIL -> There is error NBSP icon for teaser description without &nbsp!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("Check change state original title by icon\n" +
            "     <ul>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check change state original title by icon")
    public void changeStateOriginalTitleByIcon() {
        log.info("Test is started");
        int newsId = 1001;
        authCabAndGo("news-moderation/quarantine?id=" + newsId);

        pagesInit.getCabNewsModeration().changeStateOriginalTitleIcon(true);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkStateOriginalTitleIcon(true), "FAIL -> first change");

        pagesInit.getCabNewsModeration().changeStateOriginalTitleIcon(false);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkStateOriginalTitleIcon(false), "FAIL -> second change");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Original news with Native headlines for exchange")
    @Story("Original news with Native headlines for exchange")
    @Description("Check filter original title \n" +
            "     <ul>\n" +
            "      <li>@see <a href=\"https://jira.mgid.com/browse/TA-51945\">Ticket TA-51945</a></li>\n" +
            "      <li><p>Author NIO</p></li>\n" +
            "     </ul>")
    @Test(description = "Check filter original title")
    public void checkFilterByOriginalTitle() {
        log.info("Test is started");

        authCabAndGo("news-moderation/quarantine");
        pagesInit.getCabNewsModeration().filterOriginalTitle("1");
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getAmountOfNews(), pagesInit.getCabNewsModeration().getAmountOfNewsWithOriginalTitle(), "FAIL - first check");
        softAssert.assertNotEquals(pagesInit.getCabNewsModeration().getAmountOfNews(), 0, "FAIL - first check for empty list");

        pagesInit.getCabNewsModeration().filterOriginalTitle("0");
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getAmountOfNews(), pagesInit.getCabNewsModeration().getAmountOfNewsWithoutOriginalTitle(), "FAIL - second check");
        softAssert.assertNotEquals(pagesInit.getCabNewsModeration().getAmountOfNews(), 0, "FAIL - second check for empty list");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Comparison system оf similar images for news")
    @Story("Comparison system оf similar images for news | Similarity Interface elements")
    @Description("Check filter related images\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52079\">Ticket TA-52079</a></li>\n" +
            "      <li><p>NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check filter related images")
    public void checkFilterRelatedImages() {
        log.info("Test is started");
        List<String> ids = Arrays.asList("1007", "1008");

        authCabAndGo("news-moderation/quarantine");
        pagesInit.getCabNewsModeration().filterByRelatedImages("1006");
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getAmountOfNews(), 2, "Fail - getAmountOfNews");
        softAssert.assertTrue(isEqualsLists(pagesInit.getCabNewsModeration().getNewsIdFromLabel(),ids), "Fail - ids");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Comparison system оf similar images for news")
    @Story("Comparison system оf similar images for news | Similarity Interface elements")
    @Description("Check comparison images icons:\n" +
            "     <ul>\n" +
            "      <li>clock-16.png</li>\n" +
            "      <li>no_relates.png</li>\n" +
            "      <li>has_relates.png</li>\n" +
            "      <li>check related news</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52079\">Ticket TA-52079</a></li>\n" +
            "      <li><p>NIO</p></li>\n" +
            "     </ul>")
    @Test (description = "Check comparison images icons")
    public void checkComparisonImagesIcon() {
        log.info("Test is started");

        authCabAndGo("news-moderation/quarantine?id=" + 1006);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkVisibilityIconOfStatusForComparisonImages("has_relates.png"), "Fail - has_relates");

        authCabAndGo(pagesInit.getCabNewsModeration().getlinkOfComparisonImagesIcon("has_relates.png"));
        softAssert.assertEquals(pagesInit.getCabNews().getNewsIdFromLabel().get(0), "1007", "Fail - readNewsId");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
