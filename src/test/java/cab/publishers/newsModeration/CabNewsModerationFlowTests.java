package cab.publishers.newsModeration;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.CabNewsModeration;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static core.helpers.BaseHelper.waitForAjaxLoader;
import static testData.project.AuthUserCabData.AuthorizationUsers.*;
import static testData.project.ClientsEntities.EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CabNewsModerationFlowTests extends TestBase {

    @BeforeClass
    @Privilege(name = {"news/show_news_moderation_flow", "news-moderation/news-change-status"})
    public void switchOnPrivileges() {
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "news/show_news_moderation_flow", "news-moderation/news-change-status");
    }

    @BeforeMethod
    public void updateNews() {
        operationMySql.getNews1().updateModerationStatus("open", 2, 17, 19, 21, 22, 23, 61, 2005);
    }

    @AfterClass
    @Privilege(name = {"news/show_news_moderation_flow", "news-moderation/news-change-status"})
    public void switchOffPrivileges() {
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "news/show_news_moderation_flow", "news-moderation/news-change-status");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23942">https://youtrack.mgid.com/issue/TA-23942</a>
     * News moderation flow: start-stop
     * single action (start - stop (two manager))
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7264&group_by=cases:section_id&group_order=asc&group_id=1618">Test cases</a>
     * RKO
     */
    @Test
    public void startStopModerationSingleAction() {
        int news_1 = 2, news_2 = 21;
        log.info("Test is started");

        log.info("'user_auto' start moderation");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_1, news_2), "Fail -> don't all news 'in work' status to current user");

        log.info("check show news 'in work' status for other users(newsModerationUser)");
        logoutCab(GENERAL_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK_FOR_OTHER, news_1, news_2), "Fail -> don't all news 'in work' status show to other users");

        log.info("switch to user 'user_auto' and stop moderation");
        logoutCab(NEWS_MODERATION_USER);
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStopModerationIcon(news_1);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.OPEN, news_1), "fail -> news_1 don't 'open' status");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_2), "fail -> news_1 don't 'in_work' status");

        log.info("switch to user 'newsModerationUser' and try stop moderation");
        logoutCab(PRIVILEGE_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStopModerationIcon(news_2);
        softAssert.assertEquals(pagesInit.getCabNewsModeration().checkStopModerationDialogAndCloseHim(),
                "Are you sure you want to remove this news from another job?",
                "Fail -> message don't correct");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.OPEN, news_2), "fail -> news_2 don't 'open' status newsModerationUser");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23942">https://youtrack.mgid.com/issue/TA-23942</a>
     * News moderation flow: start-stop
     * check inline edit fields for news moderation flow
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7269&group_by=cases:section_id&group_order=asc&group_id=1618">Test cases</a>
     * @see <a href="https://youtrack.mgid.com/issue/MT-4382">Tiket MT-4382</a>
     * RKO
     */
    @Test
    public void startStopModerationInlineEditFields() {
        int news_1 = 61;
        log.info("Test is started");

        authCabForCheckPrivileges("news-moderation/quarantine?id=" + news_1);

        log.info("news in open status -> inline actions can't use");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditImageInline(), "FAIL -> CAN edit image inline");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("You can't edit the image of the news, because it's not taken to work or you don't have editing rights"),
                "FAIL -> don't correct message");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditTitleInline(), "FAIL -> CAN edit title inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditDescriptionInline(), "FAIL -> CAN edit description inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditCategoryInline(), "FAIL -> CAN edit category inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditLifetimeInline(), "FAIL -> CAN edit lifeTime inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditAdTypeInline(), "FAIL -> CAN edit adType inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditLandingInline(), "FAIL -> CAN edit landing inline");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCantEditUrlInline(), "FAIL -> CAN edit url inline");

        log.info("news in_work status -> inline actions can use");
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);
        authCabForCheckPrivileges("news-moderation/quarantine?id=" + news_1);

        //title
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editTitleInline(), pagesInit.getCabNewsModeration().readTitle(), "FAIL -> CAN'T edit title inline");
        //description
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editDescriptionInline(), pagesInit.getCabNewsModeration().readDescription(), "FAIL -> CAN'T edit description inline");
        //category
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editCategoryInline(), pagesInit.getCabNewsModeration().readCategory(), "FAIL -> CAN'T edit category inline");
        //lifetime
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editLifetimeInline(), pagesInit.getCabNewsModeration().readLifetime(), "FAIL -> CAN'T edit lifeTime inline");
        //adType
        pagesInit.getCabNewsModeration().editAdTypeInline();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ad type is changed"), "Fail -> don't correct ad type message");
        softAssert.assertEquals(pagesInit.getCabNewsModeration().getAdType(), pagesInit.getCabNewsModeration().readAdType(), "FAIL -> CAN'T edit adType inline");
        //landing
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editLandingInline(), pagesInit.getCabNewsModeration().readLanding(), "FAIL -> CAN'T edit landing inline");
        //url
        softAssert.assertEquals(pagesInit.getCabNewsModeration().editUrlInline(), pagesInit.getCabNewsModeration().readUrl(), "FAIL -> CAN'T edit url inline");
        //image
        softAssert.assertTrue(pagesInit.getCabNewsModeration().editImageInline(), "FAIL -> CAN'T edit image inline");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23942">https://youtrack.mgid.com/issue/TA-23942</a>
     * News moderation flow: start-stop
     * Centrifugo, смена состояний новости
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7266&group_by=cases:section_id&group_order=asc&group_id=1618">Test cases</a>
     * RKO
     */
    @Test
    public void startStopModerationApproveReject_Centrifugo() {
        int news_1 = 2, news_2 = 21, news_3 = 22;
        log.info("Test is started");

        log.info("'user_auto' start moderation and approve news");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);
        pagesInit.getCabNewsModeration().approveNews(news_1);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.APPROVE, news_1), "Fail -> news doesn't approve");

        log.info("'user_auto' reject news");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().rejectNews(news_2);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.REJECT, news_2), "Fail -> news doesn't reject");

        log.info("'user_auto' delete news");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().deleteNews(news_3);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News has been successfully deleted"), "Fail -> news doesn't delete");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * news 'open' status - can't edit mass actions for current user
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7283&group_by=cases:section_id&group_order=asc&group_id=1632">Test cases</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassCantEditFields() {
        log.info("Test is started");

        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);

        pagesInit.getCabNewsModeration().editCategoryMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Can't change news not in work."), "FAIL -> CAN edit category mass");

        pagesInit.getCabNewsModeration().editLifetimeMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Can't change news not in work."), "FAIL -> CAN edit lifeTime inline");

        pagesInit.getCabNewsModeration().editAdTypeMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Can't change news not in work."), "FAIL -> CAN edit adType inline");

        pagesInit.getCabNewsModeration().editLandingMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Can't change news not in work."), "FAIL -> CAN edit landing inline");

        pagesInit.getCabNewsModeration().editPoliticalPreferencesMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Can't change news not in work."), "FAIL -> CAN edit Political Preferences inline");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * news 'in_work' status - can edit mass actions for current user
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7282&group_by=cases:section_id&group_order=asc&group_id=1632">Test cases</a>
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7283&group_by=cases:section_id&group_order=asc&group_id=1632">Test cases</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassCanEditFields() {
        int news_1 = 61;
        log.info("Test is started");

        authCabForCheckPrivileges("news-moderation/quarantine?id=" + news_1);

        log.info("check in mass actions select has only custom fields");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkMassActionFilterHaveOnlyCustomField(CabNewsModeration.MassActions.MODERATION_START, CabNewsModeration.MassActions.MODERATION_STOP),
                "FAIL -> mass action filter doesn't have only MODERATION_START/MODERATION_STOP");

        log.info("news in_work status -> mass actions can use");
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);

        log.info("check filter 'Moderation status' has selected value 'in_work' (Taken to work)");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().moderationStatusFilterHasCustomSelectedValue(CabNewsModeration.ModerationFlowStatuses.IN_WORK));

        log.info("set campaign id filter for show other mass actions");
        pagesInit.getCabNewsModeration().chooseCampaignIdByFilter(EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);

        //category
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkCategoryValueForAllNewsOnPage(pagesInit.getCabNewsModeration().editCategoryMass()), "FAIL -> CAN'T edit category mass");
        //lifetime
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkLifetimeValueForAllNewsOnPage(pagesInit.getCabNewsModeration().editLifetimeMass()), "FAIL -> CAN'T edit lifeTime mass");
        //adType
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkAdTypeForAllTeasersOnPage(pagesInit.getCabNewsModeration().editAdTypeMass()), "FAIL -> CAN'T edit adType mass");
        //landing
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkLandingValueForAllNewsOnPage(pagesInit.getCabNewsModeration().editLandingMass()), "FAIL -> CAN'T edit landing mass");
        //Political Preferences
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkPoliticalPreferencesValueForAllNewsOnPage(pagesInit.getCabNewsModeration().editPoliticalPreferencesMass()), "FAIL -> CAN'T edit Political Preferences mass");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * news 'in_work' status - can't edit mass actions for other user
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7283&group_by=cases:section_id&group_order=asc&group_id=1632">Test cases</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassCantEditFieldsForOtherCurator() {
        int news_1 = 2;
        log.info("Test is started");

        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        log.info("news in_work status -> mass actions can use");
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);
        logoutCab(PRIVILEGE_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);


        pagesInit.getCabNewsModeration().editCategoryMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator."), "FAIL -> CAN edit category mass");

        pagesInit.getCabNewsModeration().editLifetimeMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator."), "FAIL -> CAN edit lifeTime inline");

        pagesInit.getCabNewsModeration().editLandingMass();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator."), "FAIL -> CAN edit landing inline");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * mass action (start - stop (two manager))
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7285&group_by=cases:section_id&group_order=asc&group_id=1632">CASE 2, 3</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassAction() {
        int news_1 = 2, news_2 = 21, news_3 = 22, news_other_curator = 23;
        log.info("Test is started");

        log.info("'user_auto' start moderation");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().startModerationMass(news_1, news_2);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_1, news_2, news_3), "Fail -> don't all news 'in work' status to current user");

        log.info("check show news 'in work' status for other users(newsModerationUser)");
        logoutCab(PRIVILEGE_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?news_type=r");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK_FOR_OTHER, news_1, news_2, news_3), "Fail -> don't all news 'in work' status show to other users");
        pagesInit.getCabNewsModeration().startModerationMass(news_1, news_other_curator);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK_FOR_OTHER, news_1, news_2, news_3), "Fail -> don't all news 'in work' status show to other users");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_other_curator), "Fail -> news_other_curator doesn't have 'in work' status");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator."), "FAIL -> CAN edit landing inline");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * mass action (start - stop (two manager)) for 2 ORK
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7285&group_by=cases:section_id&group_order=asc&group_id=1632">case 5</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassAction_2() {
        int news_1 = 2, news_2 = 21, news_3 = 22, news_2_1 = 17, news_2_2 = 19;
        log.info("Test is started");

        log.info("'user_auto' start moderation");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r");
        pagesInit.getCabNewsModeration().startModerationMass(news_1, news_2, news_2_1);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_1, news_2, news_3, news_2_1, news_2_2), "Fail -> don't all news 'in work' status to current user");

        pagesInit.getCabNewsModeration().stopModerationMass(news_1, news_2_2);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.OPEN, news_1, news_2_2), "Fail -> don't all news 'open' status to current user");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_2, news_3, news_2_1), "Fail -> don't all news 'in work' status after close one");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * mass action (start - stop (two manager))
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7285&group_by=cases:section_id&group_order=asc&group_id=1632">case 6</a>
     * RKO
     */
    @Test
    public void startStopModeration_MassAction_3() {
        int news_1 = 2, news_2 = 21, news_3 = 22;
        log.info("Test is started");

        log.info("'user_auto' start moderation");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r");
        pagesInit.getCabNewsModeration().startModerationMass(news_1, news_2, news_3);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, news_1, news_2, news_3), "Fail -> don't all news 'in work' status to current user");

        logoutCab(PRIVILEGE_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?news_type=r");
        pagesInit.getCabNewsModeration().stopModerationMass(news_1);
        softAssert.assertEquals(helpersInit.getBaseHelper().getTextFromAlert(),
                "Are you sure you want to remove this news from another job?",
                "Fail -> message don't correct");
        waitForAjaxLoader();
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.OPEN, news_1), "Fail -> don't all news 'open' status to other user");
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK_FOR_OTHER, news_2, news_3), "Fail -> don't all news 'in work' status to other user");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * Centrifugo, смена состояний новости mass
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7284&group_by=cases:section_id&group_order=asc&group_id=1632">CASE 1, 2, 3</a>
     * RKO
     */
    @Test
    public void startStopModerationApproveReject_Centrifugo_Mass() {
        int news_1 = 2, news_2 = 21, news_3 = 22;
        log.info("Test is started");

        log.info("'user_auto' start moderation and approve news");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);
        log.info("set campaign id filter for show other mass actions");
        pagesInit.getCabNewsModeration().chooseCampaignIdByFilter(EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);

        pagesInit.getCabNewsModeration().approveNewsMass(news_1);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.APPROVE, news_1), "Fail -> news doesn't approve");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News accepted successfully"), "Fail -> don't correct approve message");

        log.info("'user_auto' reject news");
        pagesInit.getCabNewsModeration().rejectNewsMass(news_2);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.REJECT, news_2), "Fail -> news doesn't reject");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("News declined successfully"), "Fail -> don't correct reject message");

        log.info("'user_auto' delete news");
        pagesInit.getCabNewsModeration().deleteNewsMass(news_3);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Changes submitted successfully"), "Fail -> don't correct delete message");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23938">https://youtrack.mgid.com/issue/TA-23938</a>
     * News moderation flow: Массовые действия
     * Centrifugo, смена состояний новости mass
     *
     * @see <a href="https://mirscom.testrail.io/index.php?/cases/view/7284&group_by=cases:section_id&group_order=asc&group_id=1632">CASE 5 (except delete)</a>
     * <p>
     * <a href="https://youtrack.mgid.com/issue/CP-54">https://youtrack.mgid.com/issue/CP-54</a>
     * Нет возможности удалить новость на карантине не принятую в работу
     * delete action: новость может удалить другой модератор
     * RKO
     */
    @Test
    public void startStopModerationApproveReject_Centrifugo_Mass_2() {
        int news_1 = 2, news_2 = 21, news_3 = 22;
        log.info("Test is started");

        log.info("'user_auto' start moderation and approve news");
        authCabForCheckPrivileges("news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);
        pagesInit.getCabNewsModeration().clickStartModerationIcon(news_1);

        logoutCab(PRIVILEGE_USER);
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?news_type=r&campaign_id=" + EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID);

        pagesInit.getCabNewsModeration().approveNewsMass(news_1);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator"), "Fail -> news is approve");

        log.info("'newsModerationUser' reject news");
        pagesInit.getCabNewsModeration().rejectNewsMass(news_2);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is currently being moderated by another moderator"), "Fail -> news is reject");

        log.info("'user_newsModeration' delete news");
        pagesInit.getCabNewsModeration().deleteNewsMass(news_3);
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Changes submitted successfully  (" + news_3 + ")"), "Fail -> news doesn't delete");

        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Cases
     * <ul>
     *     <li>Check forbidden editing when news has status 'in work' on moderation</li>
     *     <li>Check successful editing when news ends moderation</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27499">Ticket TA-27499</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkEditNewsAfterModeration() {
        log.info("Test is started");
        int newsId = 2005;
        String campaignId = "2001";

        log.info("'newsModerationUser' start moderation");
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?campaign_id=" + campaignId);
        pagesInit.getCabNewsModeration().clickStartModerationIcon(newsId);
        softAssert.assertTrue(pagesInit.getCabNewsModeration().checkNewsModerationStatus(CabNewsModeration.ModerationFlowStatuses.IN_WORK, newsId), "Fail -> don't all news 'in work' status to current user");
        logoutCab(NEWS_MODERATION_USER);

        log.info("check forbidden editing for other users(user_auto)");
        authCabForCheckPrivileges("wages/news?id=" + newsId);
        pagesInit.getCabNews().tryEditDescriptionInline("New description1");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The news is moderated. Editing is prohibited."),
                "FAIL -> editing in moderation!");
        logoutCab(GENERAL_USER);

        log.info("switch to user 'newsModerationUser' and approve news");
        authCabAndGo(NEWS_MODERATION_USER, "news-moderation/quarantine?campaign_id=" + campaignId);
        pagesInit.getCabNewsModeration().approveNews(newsId);
        logoutCab(NEWS_MODERATION_USER);

        log.info("switch to user 'user_auto' and try editing");
        authCabForCheckPrivileges("wages/news?id=" + newsId);
        pagesInit.getCabNews().editDescriptionInline("New description2");
        helpersInit.getBaseHelper().refreshCurrentPage();
        softAssert.assertTrue(pagesInit.getCabNews().checkDescription("New description2"),
                "FAIL -> editing after moderation!");

        softAssert.assertAll();

        log.info("Test is finished");
    }
}
