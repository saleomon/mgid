package cab.publishers.widgets.widgetListSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.List;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.EndPoints.cabLink;

public class WidgetListSettingsTests extends TestBase {

    private final int tickerCompositeId = 38;

    /**
     * check work block/unblock widget icon
     * <p>RKO</p>
     */
    @Test
    public void blockUnBlockWidgetIcon() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId);
        String oldColor = pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(tickerCompositeId);
        pagesInit.getCabWidgetsList().clickBlockUnblockWidget(tickerCompositeId);
        String newColor = pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(tickerCompositeId);
        Assert.assertNotEquals(oldColor, newColor);
        log.info("Test is finished");
    }

    /**
     * ZendDB. Установка флага запрета на удаление информера
     * Проверка возможности запрета на удаление информеров. Проверить для всех существующих типов
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21842">VT-21842</a>
     */
    @Test
    public void checkSuppressionDeleteFlagInCompositeWidgets() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId);
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkAllowDisallowDeleteIconLink());
        log.info("Test is finished");
    }

    /**
     * Вернуть поиск по g_blocks.id в списке виджетов
     * Проверка возможности фильтрации как по g_blocks.id так и по g_blocks.uid в интерфейсе списка виджетов
     * <p>MVV</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-27344">VT-27344</a>
     */
    @Test (dataProvider = "widgetIdList")
    public void checkFilterByIdAndUidInCompositeWidgetsList(String testEntity, int widgetId) {
        log.info("Test is started");
        log.info("Now checking filter parameter " + testEntity);
        authCabAndGo("wages/widgets/?g_id=" + widgetId);
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkDisplayedBlockUnblockButton(3001));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] widgetIdList() {
        return new Object[][]{
                {"g_blocks.id", 3006},
                {"g_blocks.uid", 3000006}
        };
    }

    /**
     * Не работает мгновенный предпросмотр информера в кабе
     * <ul>
     *  <li>Проверка отображение информера в интерфейсе мгновенного просмотра</li>
     *  <li>Проверка отображение информера в интерфейсе обычного просмотра просмотра</li>
     *  </ul>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23360">Ticket TA-23360</a>
     * <p>RKO</p>
     */
    @Test
    public void checkWidgetPreviews() {
        log.info("Test is started.");
        int widgetId = 240;
        String login = "testEmail49@ex.ua";
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
        authDashAndGo(login, "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);

        authCabAndGo("wages/widgets/?g_id=" + widgetId);
        serviceInit.getServicerMock().setStandName(cabLink + "wages/informers-preview/momentary/1/id/" + widgetId)
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR));
        log.info("Test is finished.");
    }

    @Story("Add icon with information about limit teasers amount for each ad category")
    @Description("Check icon limits by category filter on <a href='https://jira.mgid.com/browse/TA-52150'>TA-52150</a>")
    @Owner("RKO")
    @Test(description = "check icon limits by category filter on")
    public void checkIconLimitsByCategoryFilterOn() {
        log.info("Test is started");
        List<String> data = Arrays.asList(
                "Sexual Health - [ 1 ]",
                "Automotive - [ 1 ]",
                "Business and Finance - [ 2 ]",
                "News Russia - [ 1 ]");

        authCabAndGo("wages/widgets/?c_id=" + 2);
        pagesInit.getCabWidgetsList().clickIconLimitByCategoryFilterNews();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getLimitByCategoryFilterNews(), data);
        log.info("Test is finished");
    }

    @Story("Add icon with information about limit teasers amount for each ad category")
    @Description("Check icon limits by category filter off <a href='https://jira.mgid.com/browse/TA-52150'>TA-52150</a>")
    @Owner("RKO")
    @Test(description = "check icon limits by category filter off")
    public void checkIconLimitsByCategoryFilterOff() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + 1);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAttributeIconLimitByCategoryFilterNews("style"), "opacity: 0.5;", "FAIL -> style");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getAttributeIconLimitByCategoryFilterNews("title"), "Limits for the category filter are disabled", "FAIL -> title");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
