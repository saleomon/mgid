package cab.publishers.widgets.widgetListSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class ManagerAlertsTests extends TestBase {

    private final int widgetIdForValidation = 348;

    @BeforeMethod
    public void clearManagersAlertTypeAndDelta(){
        pagesInit.getCabWidgetsList().clearManagersAlertTypeAndDelta();
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("add manager alerts end-to-end.\n" +
            "     <ul>\n" +
            "      <li>add Manager Alerts with all settings in pop-up and set quantitative delta value</li>\n" +
            "      <li>possible to set number of alert types</li>\n" +
            "      <li>check in db/ui after create</li>\n" +
            "      <li>turn off alerts after add</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52152\">Ticket TA-52152</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "add manager alerts end-to-end")
    public void addManagerAlertEndToEnd() {
        int widgetId = 349;
        ArrayList<String> dataConcatDb;
        ArrayList<String> dataConcatUi;
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "disable"),
                "FAIL - Managers alert icon state after test has started!");
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        pagesInit.getCabWidgetsList().selectComparisonPeriod();
        pagesInit.getCabWidgetsList().selectTimePeriodFrom();
        pagesInit.getCabWidgetsList().selectTimePeriodTo();
        pagesInit.getCabWidgetsList().selectRandomTypeOfAlertAndDelta(3, true);
        pagesInit.getCabWidgetsList().saveManagerAlert();

        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message after create alerts");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "enable"),
                "FAIL - Managers alert icon state after create!");

        // concat all fields in db and in ui and check them
        dataConcatDb = operationMySql.getWidgetsSlackNotificationSettings().getAllDataWidgetsSlackNotificationByWidgetId(widgetId);
        dataConcatUi = pagesInit.getCabWidgetsList().getAllDataWidgetsSlackNotificationByWidgetId(widgetId);
        softAssert.assertTrue(dataConcatDb.containsAll(dataConcatUi) && dataConcatUi.containsAll(dataConcatDb), "FAIL -> check data in db with UI");

        // check managers alert in UI
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkComparisonPeriod(), "FAIL -> checkComparisonPeriod");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTimePeriodFrom(), "FAIL -> checkTimePeriodFrom");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTimePeriodTo(), "FAIL -> checkTimePeriodTo");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTypeOfAlertAndDelta(), "FAIL -> checkTypeOfAlertAndDelta");

        // turn off managers alert
        pagesInit.getCabWidgetsList().turnOffManagerAlert();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message after turn Off alerts");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "disable"),
                "FAIL - Managers alert icon state after turn off alerts!");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("delete custom manager alert.\n" +
            "     <ul>\n" +
            "      <li>delete custom Manager Alerts and check in ui</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "delete custom manager alert")
    public void deleteSomeAlert() {
        int widgetId = 350;
        pagesInit.getCabWidgetsList().setManagersAlertTypeAndDelta(
                new HashMap<>() {{
                    put("revenue_drop",       Map.of("1", "0"));
                    put("profitability_drop", Map.of("3", "0"));
                }}
        );

        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        pagesInit.getCabWidgetsList().deleteCustomAlert("revenue_increase");
        pagesInit.getCabWidgetsList().saveManagerAlert();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message after create alerts");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "enable"),
                "FAIL - Managers alert icon state after create!");

        // check managers alert in UI
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTypeOfAlertAndDelta(), "FAIL -> checkTypeOfAlertAndDelta");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("edit custom manager alert.\n" +
            "     <ul>\n" +
            "      <li>edit custom Manager Alerts and check in ui</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "edit custom manager alert")
    public void editSomeAlert() {
        int widgetId = 351;
        pagesInit.getCabWidgetsList().setManagersAlertTypeAndDelta(
                new HashMap<>() {{
                    put("revenue_drop",       Map.of("1", "0"));
                    put("revenue_increase",   Map.of("2", "0"));
                    put("profitability_drop", Map.of("3", "0"));
                }}
        );
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        pagesInit.getCabWidgetsList().addCustomAlert("revenue_increase", "2");
        pagesInit.getCabWidgetsList().saveManagerAlert();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message after create alerts");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "enable"),
                "FAIL - Managers alert icon state after create!");

        // check managers alert in UI
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTypeOfAlertAndDelta(), "FAIL -> checkTypeOfAlertAndDelta");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("check add manager alert when all fields are empty.\n" +
            "     <ul>\n" +
            "      <li>add manager alert when all fields are empty and check error red labels</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "check add manager alert when all fields are empty")
    public void allFieldsEmptyValidation() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetIdForValidation);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetIdForValidation);
        pagesInit.getCabWidgetsList().saveManagerAlert();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().isExistErrorOnManagerAlertsSelect());
        softAssert.assertTrue(pagesInit.getCabWidgetsList().isExistErrorOnManagerAlertsDelta());
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("add two equals manager alerts.\n" +
            "     <ul>\n" +
            "      <li>add two equals manager alerts and check error red labels</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "add two equals manager alerts")
    public void equalsAlertTypesValidation() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetIdForValidation);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetIdForValidation);
        pagesInit.getCabWidgetsList().addCustomAlert("revenue_increase", "2");
        pagesInit.getCabWidgetsList().addCustomAlert("revenue_increase", "2");
        pagesInit.getCabWidgetsList().saveManagerAlert();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Duplicated alert types"));
        log.info("Test is finished");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("Check rights for Manager's alerts.\n" +
            "     <ul>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51770\">Ticket TA-51770</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "Check rights for Manager's alerts")
    @Privilege(name = "widget-settings-slack-notifications")
    public void checkRightsForManagersAlerts() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/widget-settings-slack-notifications");
        authCabForCheckPrivileges("wages/widgets/?c_id=" + widgetIdForValidation);
        Assert.assertFalse(pagesInit.getCabWidgetsList().checkManagersAlertIcon(widgetIdForValidation),
                "FAIL -> Manager's Alerts with disabled rights!");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("check validation delta field.\n" +
            "     <ul>\n" +
            "      <li>add wrong delta and check error red labels</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(dataProvider = "wrongDeltaValuesForManagersAlerts", description = "check validation delta field")
    public void deltaValidation(String val, String deltaValue) {
        log.info("Test is started: " + val);
        authCabAndGo("wages/widgets/?c_id=" + widgetIdForValidation);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetIdForValidation);
        pagesInit.getCabWidgetsList().addCustomAlert("revenue_increase", deltaValue);
        pagesInit.getCabWidgetsList().saveManagerAlert();
        Assert.assertTrue(pagesInit.getCabWidgetsList().isExistErrorOnManagerAlertsDelta());
        log.info("Test is finished: " + val);
    }

    @DataProvider
    public Object[][] wrongDeltaValuesForManagersAlerts(){
        return new Object[][] {
                {"case-1", "abc"},
                {"case-2", "0"},
                {"case-3", "1001"}
        };
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("check validation quantitative delta value field.\n" +
            "     <ul>\n" +
            "      <li>add wrong quantitative delta value and check error red labels</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52152\">Ticket TA-52152</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "check validation delta field")
    public void dataQuantitativeValueValidation() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetIdForValidation);
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetIdForValidation);
        pagesInit.getCabWidgetsList().addCustomAlert("revenue_increase", "3", "10000000.10");
        pagesInit.getCabWidgetsList().saveManagerAlert();
        Assert.assertTrue(pagesInit.getCabWidgetsList().isExistErrorOnManagerAlertsQuantitativeValueDelta());
        log.info("Test is finished");
    }

    @Feature("Improvements of feature widgets drop increase alerts. P1")
    @Story("Improvements of feature widgets drop increase alerts. P1")
    @Description("add manager alerts end-to-end.\n" +
            "     <ul>\n" +
            "      <li>add Manager Alerts with all settings in pop-up without set quantitative delta value</li>\n" +
            "      <li>possible to set number of alert types</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51884\">Ticket TA-51884</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51885\">Ticket TA-51885</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51886\">Ticket TA-51886</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51888\">Ticket TA-51888</a></li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-52152\">Ticket TA-52152</a></li>\n" +
            "     </ul>")
    @Owner("RKO")
    @Test(description = "add manager alerts without set quantitative delta value")
    public void addManagerAlertWithoutQuantitativeValue() {
        int widgetId = 486;
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "disable"),
                "FAIL - Managers alert icon state after test has started!");
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        pagesInit.getCabWidgetsList().selectComparisonPeriod();
        pagesInit.getCabWidgetsList().selectTimePeriodFrom();
        pagesInit.getCabWidgetsList().selectTimePeriodTo();
        pagesInit.getCabWidgetsList().selectRandomTypeOfAlertAndDelta(3, false);
        pagesInit.getCabWidgetsList().saveManagerAlert();

        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message after create alerts");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkManagersAlertIconState(widgetId, "enable"),
                "FAIL - Managers alert icon state after create!");

        // check managers alert in UI
        pagesInit.getCabWidgetsList().openManagersAlertPopup(widgetId);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkComparisonPeriod(), "FAIL -> checkComparisonPeriod");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTimePeriodFrom(), "FAIL -> checkTimePeriodFrom");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTimePeriodTo(), "FAIL -> checkTimePeriodTo");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkTypeOfAlertAndDelta(), "FAIL -> checkTypeOfAlertAndDelta");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}