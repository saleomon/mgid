package cab.publishers.widgets.widgetListSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabCompositeSettings;
import pages.cab.publishers.logic.widgets.CabWidgetsList;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.*;

public class GroupChildWidgetsInsideParentTests extends TestBase {

    private final int parentId = 547,
                    childId1 = 548,
                    childId2 = 549;

    private final String domain = "testsite139.com";
    private int videoId, adBlockId;

    @BeforeClass
    public void addVideoNativeBackfillAndAdBlockToWidget(){
        //add adBlock
        authCabAndGo(compositeEditUrl + parentId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.UNDER_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        //add video with native backfill
        authDashAndGo("testEmail81@ex.ua", "publisher/edit-widget/id/" + parentId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        Assert.assertNotNull(pagesInit.getWidgetClass()
                        .setNativeBackfill(true)
                        .createVideoWidget(VideoFormat.INSTREAM, true, false),
                "FAIL -> Widget doesn't create");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("fill parentId in 'Composite widget ID' filter <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "fill parentId in 'Composite widget ID' filter")
    public void fillParentIdInCompositeFilter() {
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + parentId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 1, "FAIL -> load page with only parent");
        pagesInit.getCabWidgetsList().clickChildTreeWidgetsIcon(true);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 5, "FAIL -> open child tree");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check auto unfold sub-widgets and fill parentId <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check auto unfold sub-widgets and fill parentId")
    public void checkAutoUnfoldSubIdForParent() {
        log.info("Test is started");
        authCabAndGo(widgetsWidgetsUrl);
        pagesInit.getCabWidgetsList().fillCompositeIdFilter(parentId);
        pagesInit.getCabWidgetsList().fillAutoUnfoldSubWidgetsFilter(true);
        pagesInit.getCabWidgetsList().clickFilterButton();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 5, "FAIL -> load page with only parent");
        pagesInit.getCabWidgetsList().clickChildTreeWidgetsIcon(false);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 1, "FAIL -> open child tree");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("fill childId in 'Composite widget ID' filter <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "fill childId in 'Composite widget ID' filter")
    public void fillChildIdInCompositeFilter() {
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + childId2);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 5, "FAIL -> load page with child tree");
        pagesInit.getCabWidgetsList().clickChildTreeWidgetsIcon(false);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 1, "FAIL -> close tree with child tree");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check auto unfold sub-widgets and fill childId <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check auto unfold sub-widgets and fill childId")
    public void checkAutoUnfoldSubIdForChild() {
        log.info("Test is started");
        authCabAndGo(widgetsWidgetsUrl);
        pagesInit.getCabWidgetsList().fillCompositeIdFilter(childId2);
        pagesInit.getCabWidgetsList().fillAutoUnfoldSubWidgetsFilter(true);
        pagesInit.getCabWidgetsList().clickFilterButton();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 5, "FAIL -> load page with only parent");
        pagesInit.getCabWidgetsList().clickChildTreeWidgetsIcon(false);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), 1, "FAIL -> open child tree");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check function 'apply also to children widgets' -> turn on <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check function 'apply also to children widgets' -> turn on(filter on parentId)")
    public void checkFunctionApplyAlsoToChildrenWidgetsTurnOn() {
        adBlockId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        videoId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + parentId);
        sleep(2000);
        pagesInit.getCabWidgetsList().switchApplyToAllChildCheckbox(true);
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "his")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkDetailedActionLabel(), "id:" + childId2 + " " + domain + "; id:" + childId1 + " " + domain + "; id:" + adBlockId + " " + domain + "; id:" + videoId + " " + domain, "FAIL ->ok detailed message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "An ab test is in progress in the widget", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:" + parentId + " " + domain, "FAIL ->warn detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check function 'apply also to children widgets' -> turn off <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check function 'apply also to children widgets' -> turn off(filter on parentId)")
    public void checkFunctionApplyAlsoToChildrenWidgetsTurnOff() {
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + parentId);
        pagesInit.getCabWidgetsList().switchApplyToAllChildCheckbox(false);
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "his")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertFalse(pagesInit.getCabWidgetsList().isDisplayedWidgetOkActionLabel(), "FAIL -> displayed ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "An ab test is in progress in the widget", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:" + parentId + " " + domain, "FAIL -> detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("fill childId in 'Composite widget ID' filter <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check function 'apply also to children widgets' -> turn on (filter on childId)")
    public void checkFunctionApplyAlsoToChildrenWidgetsTurnOffFilterOnChildId() {
        adBlockId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        videoId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + childId2);
        pagesInit.getCabWidgetsList().waitLoadAllChildWidgets();
        pagesInit.getCabWidgetsList().switchApplyToAllChildCheckbox(false);
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "his")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkDetailedActionLabel(), "id:" + videoId + " " + domain + "; id:" + adBlockId + " " + domain + "; id:" + childId2 + " " + domain + "; id:" + childId1 + " " + domain, "FAIL -> detailed message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "An ab test is in progress in the widget", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:" + parentId + " " + domain, "FAIL -> detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check specify custom widgets popup 'apply also to children widgets' -> not valid data <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check specify custom widgets popup 'apply also to children widgets' -> not valid data", dataProvider = "dataCheckSpecifyCustomWidgetsPopupNotValid")
    public void checkSpecifyCustomWidgetsPopupNotValid(String name, String value) {
        log.info("Test is started: " + name);
        authCabAndGo(widgetsCustomIdUrl + childId2);
        pagesInit.getCabWidgetsList().selectCustomWidgets();
        pagesInit.getCabWidgetsList().fillSpecifyPopupWidgets(value);
        pagesInit.getCabWidgetsList().saveSpecifyPopupWidgets();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "No widgets specified, you should specify widget IDs separated by comma or space");
        log.info("Test is finished: " + name);
    }

    @DataProvider
    public Object[][] dataCheckSpecifyCustomWidgetsPopupNotValid(){
        return new Object[][]{
                {"empty value", ""},
                {"case 1", "qw"},
                {"case 2", "!@#$%^&*()_+asd"}
        };
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check specify custom widgets popup 'apply also to children widgets' -> turn on <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check specify custom widgets popup 'apply also to children widgets' -> turn on")
    public void checkSpecifyCustomWidgetsPopupNotApplyAlsoToChildrenWidgetsTurnOn() {
        log.info("Test is started");
        adBlockId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        videoId = operationMySql.getTickersCompositeBackfill().getChildTickersCompositeId(parentId);
        authCabAndGo(widgetsCustomIdUrl + 1);
        pagesInit.getCabWidgetsList().selectCustomWidgets();
        pagesInit.getCabWidgetsList().fillSpecifyPopupWidgets(parentId + ", 123");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkPopupApplyToAllChildCheckboxIsSelected(true), "FAIL -> ApplyToAllChildCheckbox isn't selected");
        pagesInit.getCabWidgetsList().saveSpecifyPopupWidgets();

        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "hi")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkDetailedActionLabel(), "id:124 testsite69.com; id:125 testsite69.com; id:" + childId2 + " " + domain + "; id:" + childId1 + " " + domain + "; id:" + adBlockId + " " + domain + "; id:" + videoId + " " + domain, "FAIL -> detailed message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "An ab test is in progress in the widget", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:" + parentId + " " + domain + "; id:123 testsite69.com", "FAIL ->warn detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Group child widgets inside parent in admin panel")
    @Story("Group child widgets inside parent in admin panel")
    @Description("check specify custom widgets popup 'apply also to children widgets' -> turn off <a href='https://jira.mgid.com/browse/KOT-2270'>KOT-2270</a>")
    @Owner("RKO")
    @Test(description = "check specify custom widgets popup 'apply also to children widgets' -> turn off")
    public void checkSpecifyCustomWidgetsPopupNotApplyAlsoToChildrenWidgetsTurnOff() {
        log.info("Test is started");
        authCabAndGo(widgetsCustomIdUrl + 1);
        pagesInit.getCabWidgetsList().selectCustomWidgets();
        pagesInit.getCabWidgetsList().fillSpecifyPopupWidgets(parentId + ", 123");
        pagesInit.getCabWidgetsList().turnOffPopupApplyToAllChildCheckbox();
        pagesInit.getCabWidgetsList().saveSpecifyPopupWidgets();

        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "his")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();

        softAssert.assertFalse(pagesInit.getCabWidgetsList().isDisplayedWidgetOkActionLabel(), "FAIL -> displayed ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "An ab test is in progress in the widget", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:" + parentId + " " + domain + "; id:123 testsite69.com", "FAIL -> warn detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
