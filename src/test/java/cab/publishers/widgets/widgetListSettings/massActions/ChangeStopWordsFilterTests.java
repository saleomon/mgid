package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.wagesWidgetsUrl;

public class ChangeStopWordsFilterTests extends TestBase {

    private final int clientId = 72;

    @Feature("Stop Words")
    @Story("Mass filtration :: Word filter")
    @Description("Try to add stop words filter without type: get message 'Choose one of the actions' <a href='https://jira.mgid.com/browse/TA-52003'>TA-52003</a>")
    @Test(description = "try to add stop words filter without type of bulk action")
    public void addStopWordsWithoutType() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_MGID, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "Choose one of the actions");
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Mass filtration :: Word filter")
    @Description("Добавляем в фильтр указанные слова. Если такие уже есть, тогда пропускаем их (повторно не вставляем такие же )" +
            " There are 2 widgets: 1 - without words; 2 - has 3 words(\"test\", \"fruit\", \"rest\"); " +
            " Try to add not valid word 'hi'; " +
            " Try to add same word 'test' for widgetId: 442; " +
            " <a href='https://jira.mgid.com/browse/TA-52003'>TA-52003</a>")
    @Test(description = "add stop words")
    public void addStopWords() {
        log.info("Test is started");
        ArrayList<String> listOfStopWordsCab = new ArrayList<>(Arrays.asList("test", "fruit", "rest", "milk", "Rubber"));

        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_MGID, clientId, "testsite122.com"));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.ADD_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("milk", "Rubber", "test", "hi")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo("wages/informers-edit/type/composite/id/" + 441);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), new ArrayList<>(Arrays.asList("milk", "Rubber", "test")),"Check stop words widgetId_1");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 442);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), listOfStopWordsCab,"Check stop words widgetId_2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Mass filtration :: Word filter")
    @Description("Добавляем в фильтр указанные слова. Если такие уже есть, тогда пропускаем их (повторно не вставляем такие же )" +
            " There are 3 widgets: 1 - without words; 2 - has 3 words('test', 'fruit', 'rest'); 3 - has 2 words('test', 'fruit') " +
            " <a href='https://jira.mgid.com/browse/TA-52003'>TA-52003</a>")
    @Test(description = "remove stop words")
    public void removeStopWords() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_MGID, clientId, "testsite119.com"));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_STOP_WORDS);
        pagesInit.getCabWidgetsList().chooseOptionStopWordsInPopUp(CabWidgetsList.ChangeStopWords.REMOVE_STOP_WORDS);
        pagesInit.getCabWidgetsList().setStopWords(new ArrayList<>(Arrays.asList("test", "fruit", "bank", "hi")));
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "Widget has disabled stop words filter", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:431 testsite119.com", "FAIL -> detailed message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo("wages/informers-edit/type/composite/id/" + 431);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkStateStopWordsCheckbox(false), "Check stop words widgetId_1");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 432);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), new ArrayList<>(List.of("rest")),"Check stop words widgetId_2");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 443);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkStateStopWordsCheckbox(false), "Check stop words widgetId_3");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
