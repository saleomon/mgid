package cab.publishers.widgets.widgetListSettings.massActions;

import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList.MassAction;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.AuthUserCabData.AuthorizationUsers.ADMIN_USER;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

/*
 * @see <a href="https://jira.mgid.com/browse/KOT-1999">Ticket KOT-1999</a>
 * <p>RKO</p>
 */
public class TurnOffGoodsCategoriesTests extends TestBase {

    private final int clientMgidId                  = 57;

    private final String goodsCategoryMessage = "You are gonna disable the filter of all goods categories for widgets (%s pcs.). After disabling the categories, you will not be able to restore the old settings";

    /**
     * берём 3 виджета одного клиента, для:
     * 1 - g_blocks.use_cat = 0
     * 2 - g_blocks.use_cat = 2
     * 3 - g_blocks.use_cat = 1
     * соверщаем массовое действие, после чего проверяем что все категории скинулись в use_cat = 0
     */
    @Privilege(name = {"wages/get-navbar-bulk-actions","wages/set-composite-bulk"})
    @Test
    public void turnOffGoodsCategories() {
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "wages/widgets/?client_id=" + clientMgidId);
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.TURN_OFF_WIDGETS_CATEGORIES);

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getPublisherCategoriesPopupText(),
                String.format(goodsCategoryMessage, 5),
                "Fail -> message");

        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Your operation will be completed within a few minutes"), "Fail -> submit");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBulkActionInProcess(), "1", "FAIL -> bulk_actions_in_process before cron");

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBulkActionInProcess(), "0", "FAIL -> bulk_actions_in_process after cron");

        //check widgets
        int gBlocksUidMgid_0 = 198;
        authCabAndGo(ADMIN_USER,"wages/informers-edit/type/goods/uid/" + gBlocksUidMgid_0);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("0"), "FAIL -> category filter after work cron gBlocksUidMgid_1");

        int gBlocksUidMgid_1 = 196;
        authCabAndGo(ADMIN_USER,"wages/informers-edit/type/goods/uid/" + gBlocksUidMgid_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("0"), "FAIL -> category filter after work cron gBlocksUidMgid_1");

        int gBlocksUidMgid_2 = 195;
        authCabAndGo(ADMIN_USER,"wages/informers-edit/type/goods/uid/" + gBlocksUidMgid_2);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("0"), "FAIL -> category filter after work cron gBlocksUidMgid_2");


        //pre-conditions
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/set-composite-bulk");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/get-navbar-bulk-actions");
        //check show bulk Action Icon For Other User
        logoutCab(ADMIN_USER);
        authCabForCheckPrivileges("wages/widgets/?client_id=" + clientMgidId);
        sleep(1000);
        softAssert.assertFalse(pagesInit.getCabWidgetsList().bulkActionInProcessIconIsDisplayed(), "FAIL -> show bulkActionInProcessIcon foe other user");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * берём 3 виджета одного клиента, с разными направлениями(частями):
     * 1 - ["exchange"]
     * 2 - ["exchange","int_exchange"] + video
     * 3 - ["wages","exchange"] + video
     * соверщаем массовое действие, и проверяем что в работу были взяты все виджеты и категории для gBlocksUidIdealmedia скинулись в use_cat = 0
     */
    @Test
    public void turnOffGoodsCategories_differentCooperationTypes() {
        log.info("Test is started");
        int clientIdealmediaId = 58;
        authCabAndGo(ADMIN_USER,"wages/widgets/?client_id=" + clientIdealmediaId);
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.TURN_OFF_WIDGETS_CATEGORIES);

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getPublisherCategoriesPopupText(),
                String.format(goodsCategoryMessage, 5),
                "Fail -> message");

        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Your operation will be completed within a few minutes"), "Fail -> submit");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBulkActionInProcess(), "1", "FAIL -> bulk_actions_in_process before cron");
        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBulkActionInProcess(), "0", "FAIL -> bulk_actions_in_process after cron");

        //check widgets
        int gBlocksUidIdealmedia = 197;
        authCabAndGo(ADMIN_USER,"wages/informers-edit/type/goods/uid/" + gBlocksUidIdealmedia);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("0"), "FAIL -> category filter after work cron");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Privilege(name = {"wages/get-navbar-bulk-actions","wages/set-composite-bulk"})
    @Test
    public void getNavbarBulkActions_privilege() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/set-composite-bulk");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/get-navbar-bulk-actions");
        authCabForCheckPrivileges("wages/widgets/?client_id=" + clientMgidId);
        softAssert.assertFalse(pagesInit.getCabWidgetsList().isDisplayedMassActions(), "FAIL -> mass action select is displayed");
        softAssert.assertFalse(pagesInit.getCabWidgetsList().bulkActionInProcessIconIsDisplayed(), "FAIL -> show bulkActionInProcessIcon");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
