package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.randomNumberFromRange;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.wagesWidgetsUrl;

/*
 * RKO
 */
public class ChangeWidgetContactPriceTests extends TestBase {

    public final int clientMgidId  = 64;
    public final int clientIdealId = 65;

    @BeforeClass
    public void setRotateDate(){
        LocalDateTime date = LocalDateTime.now(ZoneId.of("America/Los_Angeles"));
        operationMySql.getCurrentStatus().updateValue(date);
    }

    @Feature("Contracts")
    @Story("Mass filtration :: contract price ( RevShare )")
    @Description("Change widgets price e2e (Get all contract types for this case: eCPM, CPM, RS, Month)")
    @Test(dataProvider = "dataChangeRsPriceE2E")
    public void changeRsPriceLimitsE2EWithAllContractTypes(String price) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientMgidId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_CONTRACT_PRICE);
        pagesInit.getCabWidgetsList().massFillContractPrice(price);
        pagesInit.getCabWidgetsList().massContractPriceSubmit();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);
        refresh();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(366), "eCPM", "eCPM label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(366), "$0.000", "eCPM price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(367), "Revenue Share", "Revenue Share label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(367), "PW: " + price + " %", "Revenue Share price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(368), "Month", "Month label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(368), "$0.000", "Month price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(369), "CPM", "CPM label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(369), "$0.000", "CPM price");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataChangeRsPriceE2E() {
        return new Object[][]{
                {"0.001"},
                {"100.009"}
        };
    }

    @Feature("Contracts")
    @Story("Mass filtration :: contract price ( RevShare )")
    @Description("Change widgets price e2e (Get only contract types: RS")
    @Test
    public void changeRsPriceE2E() {
        log.info("Test is started");
        String price = randomNumberFromRange(0, 100);
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientMgidId, "testsite103.com"));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_CONTRACT_PRICE);
        pagesInit.getCabWidgetsList().massFillContractPrice(price);
        pagesInit.getCabWidgetsList().massContractPriceSubmit();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);
        refresh();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(370), "Revenue Share", "Revenue Share v.1 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(370), "PW: " + price + ".000 %", "Revenue Share v.1 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(371), "Revenue Share", "Revenue Share v.2 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(371), "PW: " + price + ".000 %", "Revenue Share v.1 price");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Contracts")
    @Story("Mass filtration :: contract price ( RevShare )")
    @Description("Change widgets price e2e (Get all contract types; Get different widget types(exchange; video+exchange; video+exchange+int_exchange;); Get some widgets with guaranteed contract for this case")
    @Test
    public void changeRsPriceE2EDifferentDirections() {
        log.info("Test is started");
        String price = randomNumberFromRange(0, 100);
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_IDEALMEDIA_IO, clientIdealId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_CONTRACT_PRICE);
        pagesInit.getCabWidgetsList().massFillContractPrice(price);
        pagesInit.getCabWidgetsList().massContractPriceSubmit();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);
        refresh();

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(372), "Revenue Share", "Revenue Share id:372 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(372), "PW: 11.000 %", "Revenue Share id:372 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getGuaranteedContract(372), "Guaranteed Monthly ($31.000)", "Guaranteed Monthly");
        softAssert.assertFalse(pagesInit.getCabWidgetsList().isContractTypeLabelExist(373), "Revenue Share id:373 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(374), "Revenue Share", "Revenue Share id:374 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(374), "PW: 12.000 %", "Revenue Share id:374 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getGuaranteedContract(374), "Guaranteed Daily ($41.000)", "Guaranteed Daily");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(375), "Revenue Share", "Revenue Share id:375 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(375), "PW: 13.000 %", "Revenue Share id:375 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getGuaranteedContract(375), "Guaranteed CPM ($51.000)", "Guaranteed CPM");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(376), "Month", "Month label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(376), "$0.000", "Month price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(377), "eCPM", "eCPM label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(377), "$0.000", "eCPM price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(378), "CPM", "CPM label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(378), "$0.000", "CPM price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(379), "Revenue Share", "Revenue Share id:379 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(379), "PW: 14.000 %", "Revenue Share id:379 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getGuaranteedContract(379), "Guaranteed eCPM ($61.000)", "Guaranteed eCPM");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(380), "Revenue Share", "Revenue Share id:380 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(380), "PW: " + price + ".000 %", "Revenue Share id:380 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(381), "Revenue Share", "Revenue Share id:381 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(381), "PW: " + price + ".000 %", "Revenue Share id:381 price");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractTypeLabel(382), "Revenue Share", "Revenue Share id:382 label");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getContractPriceLabel(382), "PW: " + price + ".000 %", "Revenue Share id:382 price");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Contracts")
    @Story("Mass filtration :: contract price ( RevShare )")
    @Description("Change widgets price: check validation input")
    @Test(dataProvider = "dataNotValidPrice")
    public void notValidPrice(String price) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientMgidId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_CONTRACT_PRICE);
        pagesInit.getCabWidgetsList().massFillContractPrice(price);
        pagesInit.getCabWidgetsList().massContractPriceSubmit();

        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "Input value must exceed 0 to 100 percent and contain maximum 3 figures after comma");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataNotValidPrice() {
        return new Object[][]{
                {"0.0001"},
                {"100.010"},
                {"test"},
                {"@#&.?54()+-"}
        };
    }
}
