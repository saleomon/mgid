package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList.*;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.goodWidgetSettingUrl;
import static testData.project.EndPoints.wagesWidgetsUrl;
import static testData.project.Subnets.SubnetType;

/**selectPublisherCategoryWithoutType
 * Bulk Actions. Update filter "Except" for product categories
 * <p>RKO</p>
 * @see <a href="https://jira.mgid.com/browse/TA-51818">TA-51818</a>
 */
public class ChangeWidgetsCategoriesTests extends TestBase {

    final int clientId                  = 60;
    final int clientIdealId             = 61;
    final int gBlocksIdExcept_1         = 212;
    final int gBlocksIdExcept_2         = 213;
    final int gBlocksIdOnly_1           = 303;
    final int gBlocksIdOnly_2           = 304;
    final int gBlocksIdealExceptId_1    = 214;
    final int gBlocksIdealExceptId_2    = 305;
    final int gBlocksIdealOnlyId_1      = 306;
    final int gBlocksIdealOnlyId_2      = 307;
    final String siteExceptId           = "testsite94.com";
    final String siteOnlyId             = "testsite105.com";
    final String siteIdealExceptId      = "testsite95.com";
    final String siteIdealOnlyId        = "testsite106.com";
    final String siteAddAdTypeWithoutLandingTypeId           = "testsite107.com";
    final String siteWithOnlyFilterId             = "testsite108.com";
    final String siteWithExceptFilterId           = "testsite109.com";

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Try to select publisher category without type: get message 'Choose one of the actions' <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void selectPublisherCategoryWithoutType() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();

        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "Choose one of the actions");
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Get widget without categories; add new random category to except; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void publisherCategoryAddToExceptFilterAndWidgetWithoutCategories() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdExcept_1, gBlocksIdExcept_2);
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabGoodsSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo(goodWidgetSettingUrl + gBlocksIdExcept_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("2"), "FAIL -> radiobutton");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Get widget without categories; add new random category to only; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryAddToOnlyFilterAndWidgetWithoutCategories() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdOnly_1, gBlocksIdOnly_2);
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabGoodsSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo(goodWidgetSettingUrl + gBlocksIdOnly_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Get widget with categories(Education, Nutrition, Movies); add new category to except; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void publisherCategoryAddToExceptFilterAndWidgetAlreadyHasCategories() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "World Cuisines",
                "Content Media Format",
                "Automotive",
                "News India",
                "Education",
                "Nutrition",
                "Movies"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdExcept_1, gBlocksIdExcept_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdExcept_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdExcept_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdExcept_1, 2);
        operationMySql.getGblocks().updateUseCat(gBlocksIdExcept_2, 2);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("World Cuisines");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Content Media Format");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Automotive");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("News India");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Get widget with categories(Education, Nutrition, Movies); add new category to only; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryAddToOnlyFilterAndWidgetAlreadyHasCategories() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "World Cuisines",
                "Content Media Format",
                "Automotive",
                "News India",
                "Education",
                "Nutrition",
                "Movies"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdOnly_1, gBlocksIdOnly_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdOnly_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdOnly_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdOnly_1, 1);
        operationMySql.getGblocks().updateUseCat(gBlocksIdOnly_2, 1);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("World Cuisines");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Content Media Format");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Automotive");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("News India");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Get widget with categories(Education, Nutrition, Movies); remove 2 categories: Education, Nutrition; check left 1 category: Movies <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void publisherCategoryRemoveFromExceptFilter() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories and add new categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdExcept_1, gBlocksIdExcept_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdExcept_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdExcept_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdExcept_1, 2);
        operationMySql.getGblocks().updateUseCat(gBlocksIdExcept_2, 2);

        log.info("base action -> remove some added categories");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.REMOVE_TO_EXCEPT_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Nutrition");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(new ArrayList<>(List.of("Movies"))));
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Get widget with categories(Education, Nutrition, Movies); remove 2 categories: Education, Nutrition; check left 1 category: Movies <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryRemoveFromOnlyFilter() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories and add new categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdOnly_1, gBlocksIdOnly_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdOnly_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdOnly_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdOnly_1, 1);
        operationMySql.getGblocks().updateUseCat(gBlocksIdOnly_2, 1);

        log.info("base action -> remove some added categories");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.REMOVE_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Nutrition");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(new ArrayList<>(List.of("Movies"))));
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Idealmedia/Get widget with categories(Nutrition); add new category to except; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void publisherCategoryAddToExceptFilterIdeal() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "World Cuisines",
                "Nutrition"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdealExceptId_1, gBlocksIdealExceptId_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealExceptId_1, 114);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealExceptId_2, 114);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealExceptId_1, 2);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealExceptId_2, 2);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_IDEALMEDIA_IO, clientIdealId, siteIdealExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("World Cuisines");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "There is not wages section", "FAIL -> message warn");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message ok");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Idealmedia/Get widget with categories(Nutrition); add new category to only; check all categories in goods settings <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryAddToOnlyFilterIdeal() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "World Cuisines",
                "Nutrition"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdealOnlyId_1, gBlocksIdealOnlyId_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealOnlyId_1, 114);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealOnlyId_2, 114);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealOnlyId_1, 1);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealOnlyId_2, 1);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_IDEALMEDIA_IO, clientIdealId, siteIdealOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("World Cuisines");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "There is not wages section", "FAIL -> message warn");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message ok");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(chooseCategoryInEditInterface), "FAIL -> check category");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Idealmedia/Get widget with categories(Education, Nutrition, Movies); remove 2 categories: Education, Nutrition; check left 1 category: Movies <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test
    public void publisherCategoryRemoveFromExceptFilterIdeal() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdealExceptId_1, gBlocksIdealExceptId_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealExceptId_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealExceptId_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealExceptId_1, 2);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealExceptId_2, 2);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_IDEALMEDIA_IO, clientIdealId, siteIdealExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.REMOVE_TO_EXCEPT_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Nutrition");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "There is not wages section", "FAIL -> message warn");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message ok");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(new ArrayList<>(List.of("Movies"))));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Idealmedia/Get widget with categories(Education, Nutrition, Movies); remove 2 categories: Education, Nutrition; check left 1 category: Movies <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryRemoveFromOnlyFilterIdeal() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories");
        operationMySql.getgBlocksCategories().deleteAllCategories(gBlocksIdealOnlyId_1, gBlocksIdealOnlyId_2);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealOnlyId_1, 114, 230, 264);
        operationMySql.getgBlocksCategories().insertCategories(gBlocksIdealOnlyId_2, 114, 230, 264);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealOnlyId_1, 1);
        operationMySql.getGblocks().updateUseCat(gBlocksIdealOnlyId_2, 1);

        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_IDEALMEDIA_IO, clientIdealId, siteIdealOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.REMOVE_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Nutrition");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "There is not wages section", "FAIL -> message warn");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message ok");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIconAfterMassAction(new ArrayList<>(List.of("Movies"))));
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Try to add categories with AdType but without LandingType <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test(dataProvider = "dataPublisherCategoryOnlyFilterAddAdTypeWithoutLandingType")
    public void publisherCategoryOnlyFilterAddAdTypeWithoutLandingType(ChangeWidgetsCategories type, boolean isAdType) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteAddAdTypeWithoutLandingTypeId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(type);
        pagesInit.getCabGoodsSettings().productPromotionsExpanderClick();
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Automotive");
        pagesInit.getCabGoodsSettings().clickAdTypeOrLandingTypeForCategory("Automotive", isAdType);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "At least 1 ad type and landing type should be chosen to the category");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataPublisherCategoryOnlyFilterAddAdTypeWithoutLandingType() {
        return new Object[][]{
                {ChangeWidgetsCategories.ADD_TO_ONLY_FILTER, true},
                {ChangeWidgetsCategories.ADD_TO_ONLY_FILTER, false}
        };
    }

    @Feature("Publisher category")
    @Story("Bulk Actions. Update filter \"Except\" for product categories")
    @Description("Try to add categories with AdType but without LandingType <a href='https://jira.mgid.com/browse/TA-51818'>TA-51818</a>")
    @Test(dataProvider = "dataPublisherCategoryExceptFilterAddAdTypeWithoutLandingType")
    public void publisherCategoryExceptFilterAddAdTypeWithoutLandingType(ChangeWidgetsCategories type, boolean isAdType) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteAddAdTypeWithoutLandingTypeId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(type);
        pagesInit.getCabGoodsSettings().productPromotionsExpanderClick();
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Automotive");
        pagesInit.getCabGoodsSettings().clickAdTypeOrLandingTypeForCategory("Automotive", isAdType);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataPublisherCategoryExceptFilterAddAdTypeWithoutLandingType() {
        return new Object[][]{
                {ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER, true},
                {ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER, false}
        };
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Try to add Only/Except categories for widget who has opposite categories <a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test(dataProvider = "dataPublisherCategoryTryToAddOnlyOrExceptFilterForWidgetWhoHasOppositeCategories")
    public void publisherCategoryTryToAddOnlyOrExceptFilterForWidgetWhoHasOppositeCategories(ChangeWidgetsCategories type, String domain, String message) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, domain));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(type);
        pagesInit.getCabGoodsSettings().productPromotionsExpanderClick();
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Automotive");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), message);
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataPublisherCategoryTryToAddOnlyOrExceptFilterForWidgetWhoHasOppositeCategories() {
        return new Object[][]{
                {ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER, siteWithOnlyFilterId, "Enabled filter by category \"only\""},
                {ChangeWidgetsCategories.ADD_TO_ONLY_FILTER, siteWithExceptFilterId, "Enabled filter by category \"except\""},
        };
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: category filter only")
    @Description("Check search(input) publisher category<a href='https://jira.mgid.com/browse/TA-51954'>TA-51954</a>")
    @Test
    public void publisherCategoryCheckSearchCategoryInFilter() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories.ADD_TO_EXCEPT_FILTER);
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkSearchCategoryFilter(true));
        log.info("Test is finished");
    }
}
