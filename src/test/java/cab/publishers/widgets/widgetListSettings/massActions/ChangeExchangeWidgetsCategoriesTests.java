package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.*;

public class ChangeExchangeWidgetsCategoriesTests extends TestBase {

    private final int clientId                      = 67;
    private final int clientMgidId                  = 60;
    private final int tickersIdOnly_1               = 109;
    private final int tickersIdOnly_2               = 110;
    private final int tickersIdOnly_e_w             = 112;
    private final int tickersIdOnly_e_i_v           = 113;
    private final int tickersIdOnly_e_w_v           = 114;
    final String siteMgidId                         = "testsite94.com";
    final String siteOnlyId                         = "testsite112.com";
    final String siteAddAdTypeWithoutLandingTypeId  = "testsite113.com";

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Try to select exchange category without type: get message 'Choose one of the actions' <a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "select exchange category without Type")
    public void selectExchangeCategoryWithoutType() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, 4));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();

        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "Choose one of the actions");
        log.info("Test is finished");
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Get widget without categories(all directions for this subnet(3)); add new random category to only; check all categories in exchange settings <a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "exchange category add to only filter and widget without categories")
    public void exchangeCategoryAddToOnlyFilterAndWidgetWithoutCategories() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories");
        operationMySql.getTickersCategories().deleteAllCategories(tickersIdOnly_1, tickersIdOnly_2, tickersIdOnly_e_w, tickersIdOnly_e_i_v, tickersIdOnly_e_w_v);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_1, 0);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_2, 0);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_e_w, 0);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_e_i_v, 0);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_e_w_v, 0);
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().chooseExchangeCategoriesTypeInPopUp(CabWidgetsList.ChangeExchangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("newsCategoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabExchangeSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton tickersIdOnly_1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category tickersIdOnly_1");

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_e_w);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton tickersIdOnly_e_w");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category tickersIdOnly_e_w");

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_e_i_v);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton tickersIdOnly_e_i_v");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category tickersIdOnly_e_i_v");

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_e_w_v);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton tickersIdOnly_e_w_v");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface), "FAIL -> check category tickersIdOnly_e_w_v");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Get widget with categories; add new category to only; check all categories in exchange settings <a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "publisher category add to only filter and widget already has categories")
    public void publisherCategoryAddToOnlyFilterAndWidgetAlreadyHasCategories() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "Business and Finance",
                "Careers",
                "Content Production",
                "Crime",
                "Education"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getTickersCategories().deleteAllCategories(tickersIdOnly_1, tickersIdOnly_2);
        operationMySql.getTickersCategories().insertCategories(tickersIdOnly_1, 33,72,115);
        operationMySql.getTickersCategories().insertCategories(tickersIdOnly_2, 33,72,115);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_1, 1);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_2, 1);

        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().chooseExchangeCategoriesTypeInPopUp(CabWidgetsList.ChangeExchangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("newsCategoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Crime");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");

        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> Categories aren't equal");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Get widget with categories(Education, Nutrition, Movies); remove 2 categories: Education, Nutrition; check left 1 category: Movies <a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "publisher category remove to only filter")
    public void publisherCategoryRemoveFromOnlyFilter() {
        log.info("Test is started");

        log.info("pre-condition -> clear old categories and add new categories");
        operationMySql.getTickersCategories().deleteAllCategories(tickersIdOnly_1, tickersIdOnly_2);
        operationMySql.getTickersCategories().insertCategories(tickersIdOnly_1, 33,72,115);
        operationMySql.getTickersCategories().insertCategories(tickersIdOnly_2, 33,72,115);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_1, 1);
        operationMySql.getTickers().updateUseCat(tickersIdOnly_2, 1);

        log.info("base action -> remove some added categories");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().chooseExchangeCategoriesTypeInPopUp(CabWidgetsList.ChangeExchangeWidgetsCategories.REMOVE_TO_ONLY_FILTER);
        helpersInit.getMultiFilterHelper().expandMultiFilter("newsCategoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Business and Finance");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Careers");
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo(exchangeWidgetSettingsUrl + tickersIdOnly_1);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> radiobutton");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(new ArrayList<>(List.of("Content Production"))), "FAIL -> Categories aren't equal");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Try to add categories with AdType but without LandingType <a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(dataProvider = "dataExchangeCategoryOnlyFilterAddAdTypeWithoutLandingType", description = "publisher category only filter add adType without landingType")
    public void publisherCategoryOnlyFilterAddAdTypeWithoutLandingType(CabWidgetsList.ChangeExchangeWidgetsCategories type, boolean isAdType) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteAddAdTypeWithoutLandingTypeId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().chooseExchangeCategoriesTypeInPopUp(type);
        helpersInit.getMultiFilterHelper().expandMultiFilter("newsCategoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Careers");
        pagesInit.getCabWidgetsList().clickAdTypeOrLandingTypeForExchangeCategory("Careers", isAdType);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "At least 1 ad type and landing type should be chosen to the category");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataExchangeCategoryOnlyFilterAddAdTypeWithoutLandingType() {
        return new Object[][]{
                {CabWidgetsList.ChangeExchangeWidgetsCategories.ADD_TO_ONLY_FILTER, true},
                {CabWidgetsList.ChangeExchangeWidgetsCategories.ADD_TO_ONLY_FILTER, false}
        };
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Check search(input) exchange category<a href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "publisher category check search category in filter")
    public void publisherCategoryCheckSearchCategoryInFilter() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteOnlyId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(CabWidgetsList.MassAction.CHANGE_EXCHANGE_WIDGETS_CATEGORIES);
        pagesInit.getCabWidgetsList().chooseExchangeCategoriesTypeInPopUp(CabWidgetsList.ChangeExchangeWidgetsCategories.ADD_TO_ONLY_FILTER);
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkSearchCategoryFilterInMass());
        log.info("Test is finished");
    }

    @Feature("Exchange category")
    @Story("Mass filtration :: exchange category filter only")
    @Description("Is disabled exchange category filter option href='https://jira.mgid.com/browse/TA-52011'>TA-52011</a>")
    @Test(description = "publisher category is displayed field for other subnets")
    public void publisherCategoryIsDisplayedFieldForOtherSubnets() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_MGID, clientMgidId, siteMgidId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        Assert.assertTrue(pagesInit.getCabWidgetsList().isDisabledMassAction("Change exchange category filter (not available for this subnet)"));
        log.info("Test is finished");
    }



}
