package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.Arrays;

import static pages.cab.publishers.logic.widgets.CabWidgetsList.MassAction.*;

public class GeneralSettingsTests extends TestBase {

    @Story("Mass filtration :: remove condition by client id")
    @Description("Check show mass actions without subnet filter")
    @Test
    public void showMassActionsWithoutSubnetFilter() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?is_test=0&contracts_type=%7B\"contractsType\"%3A+\"rs\"%7D&use_categories=0");
        pagesInit.getCabWidgetsList().selectAllWidgets();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getAllEnabledMassActionsOptions(),
                Arrays.asList(CHANGE_STOP_WORDS, TURN_OFF_WIDGETS_CATEGORIES, CHANGE_CONTRACT_PRICE, CHANGE_EXCHANGE_CAMPAIGN_FILTER));
        log.info("Test is finished");
    }

    @Story("Mass filtration :: remove condition by client id")
    @Description("Check show mass actions with subnet filter")
    @Test
    public void showMassActionsWithSubnetFilter() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?subnet={\"subnet\":0,\"mirror\":null}&is_test=0&contracts_type=%7B\"contractsType\"%3A+\"rs\"%7D&use_categories=0");
        pagesInit.getCabWidgetsList().selectAllWidgets();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getAllEnabledMassActionsOptions(),
                Arrays.asList(CONTENT_TYPES, CHANGE_STOP_WORDS, TURN_OFF_WIDGETS_CATEGORIES, CHANGE_CONTRACT_PRICE, CHANGE_WIDGETS_CATEGORIES, CHANGE_EXCHANGE_CAMPAIGN_FILTER));
        log.info("Test is finished");
    }

    @Story("Mass filtration :: remove condition by client id")
    @Description("Check show mass actions only subnet filter")
    @Test
    public void showMassActionsOnlySubnetFilter() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?subnet={\"subnet\":0,\"mirror\":null}");
        pagesInit.getCabWidgetsList().selectAllWidgets();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getAllEnabledMassActionsOptions(),
                Arrays.asList(CONTENT_TYPES, CHANGE_STOP_WORDS, TURN_OFF_WIDGETS_CATEGORIES, CHANGE_CONTRACT_PRICE, CHANGE_WIDGETS_CATEGORIES, CHANGE_EXCHANGE_CAMPAIGN_FILTER));
        log.info("Test is finished");
    }
}
