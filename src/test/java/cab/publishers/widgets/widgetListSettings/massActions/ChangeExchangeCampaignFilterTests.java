package cab.publishers.widgets.widgetListSettings.massActions;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList.ChangeExchangeCampaignFilter;
import pages.cab.publishers.logic.widgets.CabWidgetsList.MassAction;
import testBase.TestBase;
import testData.project.Subnets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.wagesWidgetsUrl;

public class ChangeExchangeCampaignFilterTests extends TestBase {

    private final int clientId = 66;
    private final String siteExceptId = "testsite111.com";

    private final int tickersId_5 = 107;
    private final int tickersId_6 = 108;

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Try to add exchange campaigns filter without type: get message 'Choose one of the actions' <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "try to add exchange campaign filter without type of bulk action")
    public void setExchangeCampaignFilterWithoutType() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertEquals(pagesInit.getCabWidgetsList().getMassActionErrorMessage(), "Choose one of the actions");
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("ADD_TO_EXCEPT_FILTER for widgets with different directions <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012: </a>\n" +
            "-exchange\n" +
            "-wages+exchange\n" +
            "-exchange+int_exchange+video\n" +
            "-exchange+wages+video")
    @Test(description = "add exchange campaign filter e2e")
    public void setExchangeCampaignFilterE2E() {
        List exchangeCampaigns;
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.ADD_TO_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        exchangeCampaigns = pagesInit.getCabWidgetsList().chooseRandomExchangeCampaigns(5);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        log.info("exchange");
        refresh();
        int tickersId_1 = 103;
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_1);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(exchangeCampaigns),
                "FAIL(Exchange) -> didn't equals exchange campaign in filter");

        log.info("wages+exchange");
        int tickersId_2 = 104;
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_2);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(exchangeCampaigns),
                "FAIL(wages+exchange) -> didn't equals exchange campaign in filter");

        log.info("exchange+int_exchange+video");
        int tickersId_3 = 105;
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_3);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(exchangeCampaigns),
                "FAIL(exchange+int_exchange+video) -> didn't equals exchange campaign in filter");

        log.info("exchange+wages+video");
        int tickersId_4 = 106;
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_4);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(exchangeCampaigns),
                "FAIL(exchange+wages+video) -> didn't equals exchange campaign in filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("ADD_TO_EXCEPT_FILTER for widgets with different directions but without Exchange <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012: </a>\n" +
            "-wages\n" +
            "-wages+int_exchange+video")
    @Test(description = "add exchange campaign filter e2e without exchange")
    public void setExchangeCampaignFilterE2EWithoutExchange() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_MGID, 1, "testsitemgtre.com"));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.ADD_TO_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        pagesInit.getCabWidgetsList().chooseRandomExchangeCampaigns(2);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "Widget has not exchange part", "FAIL -> base message");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel().contains("id:52 testsitemgtre.com; id:406 testsitemgtre.com"), "FAIL -> detailed message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Get 2 exchange widgets with different filter types (tickers_composite.filter_2 = 1/2) <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "add exchange campaign filter for 2 widgets one of them has Only filter")
    public void setExchangeCampaignFilterWidgetsHaveDifferentFilterType() {
        log.info("Test is started");
        log.info("pre-condition -> clear old categories");
        operationMySql.getTickersFilters().deleteAllFilters(tickersId_5, tickersId_6);
        operationMySql.getTickersFilters().insertFilters(tickersId_5, 5, 6);
        operationMySql.getTickersFilters().insertFilters(tickersId_6, 5, 6);
        operationMySql.getTickers().updateFilter2(tickersId_5, 1);
        operationMySql.getTickers().updateFilter2(tickersId_6, 2);


        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.ADD_TO_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(22);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnActionLabel(), "The filter will not be applied, because widget has \"only\" filter now", "FAIL -> warn message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetWarnDetailedActionLabel(), "id:404 testsite111.com", "FAIL -> warn detailed message");

        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> ok message");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkDetailedActionLabel(), "id:405 testsite111.com", "FAIL -> ok detailed message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        log.info("exchange");
        refresh();
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_5);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(new ArrayList<>(Arrays.asList(
                "5",
                "6"
        ))),
                "FAIL(tickersId_5) -> didn't equals exchange campaign in filter");

        log.info("wages+exchange");
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_6);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(new ArrayList<>(Arrays.asList(
                "5",
                "6",
                "22"
        ))),
                "FAIL(tickersId_6) -> didn't equals exchange campaign in filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Add Exchange Campaigns for widgets who has some campaigns in except filter <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "add exchange campaign filter e2e some widgets have exchange campaign filter in except")
    public void addExchangeCampaignFilterForWidgetWhoHasSomeCampaigns() {
        log.info("Test is started");
        List dumpCampaigns = new ArrayList<>(Arrays.asList(
                "5",
                "6",
                "19",
                "22",
                "2002"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getTickersFilters().deleteAllFilters(tickersId_5, tickersId_6);
        operationMySql.getTickersFilters().insertFilters(tickersId_5, 5, 6, 19);
        operationMySql.getTickersFilters().insertFilters(tickersId_6, 5, 6, 19);
        operationMySql.getTickers().updateFilter2(tickersId_5, 2);
        operationMySql.getTickers().updateFilter2(tickersId_6, 2);

        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.ADD_TO_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(22);
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(2002);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        log.info("exchange");
        refresh();
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_5);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(dumpCampaigns),
                "FAIL(tickersId_5) -> didn't equals exchange campaign in filter");

        log.info("wages+exchange");
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_6);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(dumpCampaigns),
                "FAIL(tickersId_6) -> didn't equals exchange campaign in filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Remove 2 Exchange Campaigns for widgets who has 3 campaigns in except filter and wait 1 Exchange Campaign <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "remove exchange campaign filter e2e")
    public void removeExchangeCampaignFilterForWidgetWhoHasSomeCampaigns() {
        log.info("Test is started");
        List dumpCampaigns = new ArrayList<>(List.of(
                "6"
        ));

        log.info("pre-condition -> clear old categories");
        operationMySql.getTickersFilters().deleteAllFilters(tickersId_5, tickersId_6);
        operationMySql.getTickersFilters().insertFilters(tickersId_5, 5, 6, 19);
        operationMySql.getTickersFilters().insertFilters(tickersId_6, 5, 6, 19);
        operationMySql.getTickers().updateFilter2(tickersId_5, 2);
        operationMySql.getTickers().updateFilter2(tickersId_6, 2);

        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.REMOVE_FROM_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(5);
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(19);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();

        //run cron
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        refresh();
        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_5);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(dumpCampaigns),
                "FAIL(tickersId_5) -> didn't equals exchange campaign in filter");

        pagesInit.getCabWidgetsList().clickExchangeCampaignFilterIcon(tickersId_6);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().getAllExchangeCampaignsFilterFromPopUp().containsAll(dumpCampaigns),
                "FAIL(tickersId_6) -> didn't equals exchange campaign in filter");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Exchange Campaigns -> check 'search' filter and 'clean' icon <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "exchange campaign filter check search")
    public void exchangeCampaignFilterCheckSearch() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.REMOVE_FROM_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        pagesInit.getCabWidgetsList().searchExchangeCampaignInInput("testsite2021.com");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkSearchResult(false,"testsite2021.com"), "FAIL -> search about campaign name");

        pagesInit.getCabWidgetsList().cleanSearchExchangeCampaignInput();
        pagesInit.getCabWidgetsList().searchExchangeCampaignInInput("2005");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkSearchResult(true,"p2005"), "FAIL -> search about campaign id");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Mass filtration :: Exchange campaign filter except")
    @Description("Exchange Campaigns -> check work checkbox: 'active'/'inactive'/'select all' <a href='https://jira.mgid.com/browse/TA-52012'>TA-52012</a>")
    @Test(description = "exchange campaign filter check work checkbox: 'active'/'inactive'/'select all'")
    public void exchangeCampaignFilterCheckWorkOtherCheckboxes() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, clientId, siteExceptId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CHANGE_EXCHANGE_CAMPAIGN_FILTER);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        pagesInit.getCabWidgetsList().chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter.REMOVE_FROM_EXCEPT_FILTER);
        pagesInit.getCabWidgetsList().clickUploadPartnersListButton();
        int countCampaignsInput = pagesInit.getCabWidgetsList().getCountExchangeCampaignInput();

        log.info("activate some src_id");
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(5);
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(19);
        pagesInit.getCabWidgetsList().clickCurrentExchangeCampaigns(6);

        log.info("click 'activate' c-box and left only activates");
        pagesInit.getCabWidgetsList().clickActivateCheckboxInExchangeCampaignsPopUp();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().ifLeftOnlyActivateCheckboxes(3), "FAIL -> activate c-box");

        log.info("click 'inactivate' c-box and left only inactivates");
        pagesInit.getCabWidgetsList().clickActivateCheckboxInExchangeCampaignsPopUp();
        pagesInit.getCabWidgetsList().clickInactivateCheckboxInExchangeCampaignsPopUp();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().ifLeftOnlyInActivateCheckboxes(countCampaignsInput - 3), "FAIL -> inactivate c-box");

        log.info("click 'inactivate' c-box and left only inactivates");
        pagesInit.getCabWidgetsList().clickSelectAllCheckboxInExchangeCampaignsPopUp();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().ifLeftOnlyActivateCheckboxes(countCampaignsInput - 3), "FAIL -> select all c-box(activate)");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
