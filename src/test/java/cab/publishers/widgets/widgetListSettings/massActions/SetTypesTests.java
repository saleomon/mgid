package cab.publishers.widgets.widgetListSettings.massActions;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabWidgetsList.MassAction;
import testBase.TestBase;
import testData.project.Subnets.SubnetType;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.BULK_ACTION;
import static testData.project.EndPoints.wagesWidgetsUrl;

/**
 * Массовые действия для настроек виджетов
 * <p>RKO</p>
 * @see <a href="https://youtrack.mgid.com/issue/TA-27521">TA-27521</a>
 */
public class SetTypesTests extends TestBase {

    private final int clientMgidId                  = 38,
                      tickerCompositeMgidId_1       = 110,
                      tickerCompositeMgidId_2       = 111,
                      clientAdskeeperId             = 39,
                      tickerCompositeAdskeeperId_1  = 112,
                      tickerCompositeAdskeeperId_2  = 113,
                      clientIdealId                 = 40,
                      tickerCompositeIdealId_1      = 114,
                      tickerCompositeIdealId_2      = 115;

    @Test(dataProvider = "dataSubnets")
    public void setAdTypesAndLandingTypes(int clientId, SubnetType subnetId, int tickerCompositeId_1, int tickerCompositeId_2,
                                          Map<String, Boolean> adTypes, Map<String, Boolean> landingTypes) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(subnetId, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabWidgetsList().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_1);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_2);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdTypesInCab(adTypes), "FAIL -> default ad types");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkLandingTypesInCab(landingTypes), "FAIL -> default landing types");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataSubnets() {
        return new Object[][]{
                {
                    clientMgidId,
                        SubnetType.SCENARIO_MGID,
                        tickerCompositeMgidId_1,
                        tickerCompositeMgidId_2,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",    false);
                            put("x",    false);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",    false);
                        }}
                },

                {
                        clientAdskeeperId,
                        SubnetType.SCENARIO_ADSKEEPER,
                        tickerCompositeAdskeeperId_1,
                        tickerCompositeAdskeeperId_2,
                        new HashMap<>() {{
                            put("pg",   true);
                            put("pg13", true);
                            put("r",    true);
                            put("nc17", true);
                            put("nsfw", true);
                            put("b",    true);
                            put("x",    true);
                        }},

                        new HashMap<>() {{
                            put("g",    true);
                            put("pg",   true);
                            put("pg13", true);
                            put("r",    true);
                            put("nc17", true);
                            put("nsfw", true);
                            put("b",    true);
                            put("x",    true);
                        }}
                },

                {
                        clientIdealId,
                        SubnetType.SCENARIO_IDEALMEDIA_IO,
                        tickerCompositeIdealId_1,
                        tickerCompositeIdealId_2,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw", false);
                            put("b",    false);
                            put("x",    false);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw", false);
                            put("b",    false);
                            put("x",    false);
                        }}
                },
        };
    }

    @Test
    public void setAdTypesAndLandingTypes_fillEmpty() {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(SubnetType.SCENARIO_MGID, clientMgidId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("No type selected"), "FAIL -> message");
        log.info("Test is finished");
    }

    @Test(dataProvider = "dataForSetAdTypesAndLandingTypes_AdTypesValidation")
    public void setAdTypesAndLandingTypes_AdTypesValidation(int clientId, SubnetType subnetId,
                                                            Map<String, Boolean> adTypes,
                                                            Map<String, Boolean> landingTypes,
                                                            String message) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(subnetId, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabWidgetsList().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> message");

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForSetAdTypesAndLandingTypes_AdTypesValidation() {
        return new Object[][]{
                {
                        clientMgidId,
                        SubnetType.SCENARIO_MGID,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",     true);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",    false);
                        }},
                        "Chosen ad types is forbidden for this subnet/mirror. Allowed values - pg, pg13, r, nc17, nsfw"
                },

                {
                        clientIdealId,
                        SubnetType.SCENARIO_IDEALMEDIA_IO,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",     true);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw", false);
                            put("b",    false);
                            put("x",    false);
                        }},
                        "Chosen ad types is forbidden for this subnet/mirror. Allowed values - pg, r, nc17, pg13"
                },
        };
    }

    @Test(dataProvider = "dataForSetAdTypesAndLandingTypes_AdLandingValidation")
    public void setAdTypesAndLandingTypes_LandingTypesValidation(int clientId, SubnetType subnetId,
                                                                 Map<String, Boolean> adTypes,
                                                                 Map<String, Boolean> landingTypes,
                                                                 String message) {
        log.info("Test is started");
        authCabAndGo(wagesWidgetsUrl(subnetId, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().turnOnCustomAdTypes(adTypes);
        pagesInit.getCabWidgetsList().turnOnCustomLandingTypes(landingTypes);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> message");

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForSetAdTypesAndLandingTypes_AdLandingValidation() {
        return new Object[][]{
                {
                        clientMgidId,
                        SubnetType.SCENARIO_MGID,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",    false);
                            put("x",    false);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",     true);
                        }},
                        "Chosen landing types is forbidden for this subnet. Allowed values - g, pg, pg13, r, nc17, nsfw, b"
                },

                {
                        clientIdealId,
                        SubnetType.SCENARIO_IDEALMEDIA_IO,
                        new HashMap<>() {{
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw", false);
                            put("b",    false);
                            put("x",    false);
                        }},

                        new HashMap<>() {{
                            put("g",     true);
                            put("pg",    true);
                            put("pg13",  true);
                            put("r",     true);
                            put("nc17",  true);
                            put("nsfw",  true);
                            put("b",     true);
                            put("x",     true);
                        }},
                        "Chosen landing types is forbidden for this subnet. Allowed values - g, pg, r, nc17, pg13"
                },
        };
    }

    /*
     * 1 - type(pg/nc17/pg13)
     * 2 - isSelected
     * 3 - Limit amount of teasers for selected type.
     */
    Table<String, Boolean, String> adTypesData = HashBasedTable.create();

    @Test
    public void checkAddTypesLimitCorrectValues_Mgid() {
        log.info("Test is started");
        adTypesData.put("pg",   true,   "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID, tickerCompositeMgidId_1, tickerCompositeMgidId_2);
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitCorrectValues_Idealmedia() {
        log.info("Test is started");
        adTypesData.put("pg",   true,   "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", false,  "0");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes(clientIdealId, SubnetType.SCENARIO_IDEALMEDIA_IO, tickerCompositeIdealId_1, tickerCompositeIdealId_2);
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitCorrectValues_Adskeeper() {
        log.info("Test is started");
        adTypesData.put("pg",   true,   "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true, "255");
        adTypesData.put("b",    true,   "5");
        adTypesData.put("x",    true,  "11");

        setValidAdTypes(clientAdskeeperId, SubnetType.SCENARIO_ADSKEEPER, tickerCompositeAdskeeperId_1, tickerCompositeAdskeeperId_2);
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitZeroValue() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true,   "0");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true,   "0");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID, tickerCompositeMgidId_1, tickerCompositeMgidId_2);
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitGetMessageForPgSuccess() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID, tickerCompositeMgidId_1, tickerCompositeMgidId_2);
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitNotValidValue() {
        log.info("Test is started");
        adTypesData.put("pg",   true,     "-");
        adTypesData.put("pg13", false,    "-");
        adTypesData.put("r",    true,   "-2");
        adTypesData.put("nc17", true,     "77");
        adTypesData.put("nsfw", true,    "5");
        adTypesData.put("b",    false,    "0");
        adTypesData.put("x",    false,    "0");

        setNotValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID,"The value must be greater then 0");
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitNotValidValue2() {
        log.info("Test is started");
        adTypesData.put("pg",   true,     "-");
        adTypesData.put("pg13", false,    "-");
        adTypesData.put("r",    true,   "-2");
        adTypesData.put("nc17", true,     "&&");
        adTypesData.put("nsfw", true,    "sds%4Dr5t");
        adTypesData.put("b",    false,    "0");
        adTypesData.put("x",    false,    "0");

        setNotValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID,"The value must be greater then 0");
        log.info("Test is finished");
    }


    @Test
    public void checkAddTypesLimitGetMessageForPg_Mgid() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setNotValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID, "Very low limit for selected ad types. Select PG or remove limit from other ad types (R, NC17, NSFW).");
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitGetMessageForPg_Idealmedia() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", false,  "0");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setNotValidAdTypes(clientIdealId, SubnetType.SCENARIO_IDEALMEDIA_IO, "Very low limit for selected ad types. Select PG or remove limit from other ad types (R, NC17, NSFW).");
        log.info("Test is finished");
    }

    @Test
    public void checkAddTypesLimitGetMessageForPg_Adskeeper() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true,   "4");
        adTypesData.put("b",    true,   "3");
        adTypesData.put("x",    true,   "2");

        setNotValidAdTypes(clientAdskeeperId, SubnetType.SCENARIO_ADSKEEPER, "Very low limit for selected ad types. Select PG or remove limit from other ad types (R, NC17, NSFW).");
        log.info("Test is finished");
    }

    @Test
    public void checkAdTypesLimitNotValidMaxValue() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true, "256");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setNotValidAdTypes(clientMgidId, SubnetType.SCENARIO_MGID, "The value must be lower then 256");
        log.info("Test is finished");
    }

    private void setValidAdTypes(int clientId, SubnetType subnetId, int tickerCompositeId_1, int tickerCompositeId_2){
        authCabAndGo(wagesWidgetsUrl(subnetId, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().setAdTypes(adTypesData);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getMassChangeCategoryWidgetOkActionLabel(), "Widgets that are okay for bulk action", "FAIL -> message");
        pagesInit.getCabWidgetsList().massActionPopupSubmit();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> message");
        serviceInit.getDockerCli().runAndStopCron(BULK_ACTION, "-vv");
        sleep(5000);

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_1);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdType(adTypesData), "FAIL -> tickerCompositeId_1");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_2);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdType(adTypesData), "FAIL -> tickerCompositeId_2");
        softAssert.assertAll();
        adTypesData.clear();
    }

    private void setNotValidAdTypes(int clientId, SubnetType subnetId, String message){
        authCabAndGo(wagesWidgetsUrl(subnetId, clientId));
        pagesInit.getCabWidgetsList().selectAllWidgets();
        pagesInit.getCabWidgetsList().chooseMassActions(MassAction.CONTENT_TYPES);
        pagesInit.getCabWidgetsList().setAdTypes(adTypesData);
        pagesInit.getCabWidgetsList().publisherCategoriesClickNext();
        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> custom message");
        adTypesData.clear();
    }
}
