package cab.publishers.widgets.widgetListSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.cab.publishers.variables.ContractsData;
import testBase.TestBase;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

public class ContractSettingsTests  extends TestBase {

    private final int tickerCompositeId = 38;
    private final int tickerCompositeId2 = 7002;

    @BeforeClass
    public void setRotateDate() {
        LocalDateTime date = LocalDateTime.now(ZoneId.of("Pacific/Honolulu"));
        operationMySql.getCurrentStatus().updateValue(date);
    }

    @Feature("Contracts")
    @Story("Adding new types of contracts")
    @Description("Checking editing contracts in the list of widgets (Checking different types of contracts) <a href=\"https://youtrack.mgid.com/issue/VT-21870\">VT-21870</a>, <a href=\"https://youtrack.mgid.com/issue/VT-23527\">VT-23527</a>")
    @Test(dataProvider = "contracts")
    public void checkWidgetContracts(int a, ContractsData.Contracts contractType, ContractsData.GuaranteedContracts guaranteedContractType) {
        log.info("Test is started -> " + contractType);
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        pagesInit.getCabWidgetsList().setContractType(contractType, guaranteedContractType);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkContractType(contractType), "FAIL: contract " + contractType.getContractType() + " wasn't set !!!");
        log.info("Test is finished -> " + contractType);
    }

    @DataProvider
    public Object[][] contracts() {
        return new Object[][]{
                {1, ContractsData.Contracts.CPM, null},
                {2, ContractsData.Contracts.ECPM, null},
                {3, ContractsData.Contracts.MONTH, null},
                {4, ContractsData.Contracts.RS, null},
                {5, ContractsData.Contracts.GUARANTEED,ContractsData.GuaranteedContracts.DAILY},
                {6, ContractsData.Contracts.GUARANTEED,ContractsData.GuaranteedContracts.MONTHLY},
                {7, ContractsData.Contracts.GUARANTEED,ContractsData.GuaranteedContracts.CPM},
                {8, ContractsData.Contracts.GUARANTEED,ContractsData.GuaranteedContracts.ECPM}
        };
    }

    @Feature("Contracts")
    @Story("Adding new types of contracts")
    @Description("Attempt to change contract during current_status.value != date.now() <a href=\"https://youtrack.mgid.com/issue/KOT-1660\">KOT-1660</a>")
    @Test(dataProvider = "contracts")
    public void checkWidgetContracts_VALID_WrongRotateDate(int count, ContractsData.Contracts contractType,ContractsData.GuaranteedContracts guaranteedContractType) {
        int wrongRotateDate = 0;
        log.info("Test is started -> " + contractType);
        if (count == 1) {
            wrongRotateDate = LocalDate.now(ZoneId.of("Pacific/Honolulu")).getDayOfMonth() - 1;
            log.info("wrongRotateDate: " + wrongRotateDate);
            operationMySql.getCurrentStatus().updateValue(wrongRotateDate);
        }

        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        pagesInit.getCabWidgetsList().setContractType(contractType, guaranteedContractType);
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("The rotation is not over yet, please try again in a few minutes"), "FAIL: message " + contractType.getContractType() + "doesn't work !!!");

        if (count == 8) operationMySql.getCurrentStatus().updateValue(wrongRotateDate + 1);
        log.info("Test is finished -> " + contractType);
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check validation for invalid guaranteed contract price")
    @Test(dataProvider = "notValidPrice")
    public void checkGuaranteedContractWithInvalidPrice(String invalidPrice, ContractsData.GuaranteedContracts guaranteedContracts) {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId2);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        pagesInit.getCabWidgetsList().clickGuaranteedContract();
        pagesInit.getCabWidgetsList().setGuaranteedType(guaranteedContracts.getGuaranteedContractType());
        pagesInit.getCabWidgetsList().setGuaranteedContractPrice(invalidPrice);
        pagesInit.getCabWidgetsList().setGuaranteedPercentage(null);
        pagesInit.getCabWidgetsList().saveContract();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Input value must exceed 0 to 1 000 000 and contain maximum 3 figures after comma"),"Fail -> Correct error message isn't displayed");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] notValidPrice() {
        return new Object[][]{
                {"-1",ContractsData.GuaranteedContracts.MONTHLY},
                {"1000000.010", ContractsData.GuaranteedContracts.CPM},
                {"0,0001", ContractsData.GuaranteedContracts.ECPM},
                {"@#&.?54()+-",ContractsData.GuaranteedContracts.DAILY},
                {"abc", ContractsData.GuaranteedContracts.DAILY}
        };
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check validation for invalid guaranteed contract percentage")
    @Test(dataProvider = "notValidPercentage")
    public void checkGuaranteedContractWithInvalidPercentage(String invalidPercentage, ContractsData.GuaranteedContracts guaranteedContracts) {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId2);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        pagesInit.getCabWidgetsList().clickGuaranteedContract();
        pagesInit.getCabWidgetsList().setGuaranteedType(guaranteedContracts.getGuaranteedContractType());
        pagesInit.getCabWidgetsList().setGuaranteedContractPrice(null);
        pagesInit.getCabWidgetsList().setGuaranteedPercentage(invalidPercentage);
        pagesInit.getCabWidgetsList().saveContract();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Input value must exceed 0 to 100  percent and contain maximum 3 figures after comma"),"Fail -> Correct error message isn't displayed");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] notValidPercentage() {
        return new Object[][]{
                {"-1",ContractsData.GuaranteedContracts.MONTHLY},
                {"100.010", ContractsData.GuaranteedContracts.CPM},
                {"0,0001", ContractsData.GuaranteedContracts.ECPM},
                {"@#&.?54()+-",ContractsData.GuaranteedContracts.DAILY},
                {"abc", ContractsData.GuaranteedContracts.DAILY}
        };
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check guaranteed contract price changes after switching guaranteed types")
    @Test(dataProvider = "guaranteedContractTypes")
    public void checkGuaranteedContractPriceLabelChanges(ContractsData.GuaranteedContracts guaranteedContracts) {
        log.info("Test is started");

        Map<String, String> priceLabels = new HashMap<>(){{
            put(ContractsData.GuaranteedContracts.DAILY.getGuaranteedContractType(), "Cost per 1 day");
            put(ContractsData.GuaranteedContracts.MONTHLY.getGuaranteedContractType(), "Cost per 1 month");
            put(ContractsData.GuaranteedContracts.CPM.getGuaranteedContractType(), "CPM");
            put(ContractsData.GuaranteedContracts.ECPM.getGuaranteedContractType(), "eCPM");
        }};

        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId2);
        pagesInit.getCabWidgetsList().clickWidgetsContracts();
        pagesInit.getCabWidgetsList().clickGuaranteedContract();
        pagesInit.getCabWidgetsList().setGuaranteedType(guaranteedContracts.getGuaranteedContractType());
        Assert.assertTrue(pagesInit.getCabWidgetsList().checkGuaranteedPriceLabel(priceLabels));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] guaranteedContractTypes() {
        return new Object[][]{
                {ContractsData.GuaranteedContracts.DAILY},
                {ContractsData.GuaranteedContracts.MONTHLY},
                {ContractsData.GuaranteedContracts.CPM},
                {ContractsData.GuaranteedContracts.ECPM}
        };
    }

    @Feature("Contracts")
    @Story("Add guaranteed contract type in dash and cab")
    @Description("Check contract type in widgets labels after filtering by Contracts type filter <a href=\"https://jira.mgid.com/browse/TA-51972\">TA-51972</a>")
    @Test(description = "check contract types in widget labels after filtering")
    public void checkContractTypesInWidgetLabelsAfterFiltering() {
        log.info("Test is started");
        int clientId = 7002;
        authCabAndGo("wages/widgets/?client_id=" + clientId);
        String randomSelectionOfContractsTypeFilter  = pagesInit.getCabWidgetsList().selectRandomContractTypeFromContractTypeFilter();
        pagesInit.getCabWidgetsList().clickFilterButton();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().isContractTypeFromWidgetEqualsToFilteredType(randomSelectionOfContractsTypeFilter), "Fail - > Wrong contract type in label is displayed");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getNumberOfDisplayedWidgets(), randomSelectionOfContractsTypeFilter.equals("{\"contractsType\": \"rs\"}") ? 6 : 1, "FAIL -> number Of displayed widgets");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
