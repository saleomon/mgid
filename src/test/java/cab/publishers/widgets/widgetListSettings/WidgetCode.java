package cab.publishers.widgets.widgetListSettings;

import org.testng.*;
import org.testng.annotations.*;
import testBase.TestBase;

public class WidgetCode extends TestBase {

    /**
     * получение кода информера из каба
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-20883">TA-20883</a>
     */
    @Test
    public void checkShortCode() {
        log.info("Test is started");
        authCabAndGo("wages/informers-code/id/" + 38);
        Assert.assertTrue(pagesInit.getCabInformersCode().checkShortCode(23, "testsitemg5.com", 38));
        log.info("Test is finished");
    }

    /**
     * флаг XML в композитном информере
     * Проверка изменения кода виджета при включенном xml/rss
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21756">VT-21756</a>
     */
    @Test
    public void checkXMLFlagInWidget() {
        log.info("Test is started");
        int tickersCompositeId = 40;
        String widgetToken = operationMySql.getTickersComposite().getTickersCompositeXmlWidgetToken(tickersCompositeId);
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings().switchXmlCheckbox(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo("wages/informers-code/id/" + tickersCompositeId);
        Assert.assertTrue(pagesInit.getCabInformersCode().checkXmlCode(tickersCompositeId, widgetToken));
        log.info("Test is finished");
    }
}
