package cab.publishers.widgets.widgetListSettings;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.variables.DfpPublisher;
import testBase.TestBase;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.ClientsEntities.MGID_PUSH_WIDGET_ID;

public class PushSettings extends TestBase {

    /**
     * тест создаёт виджет из которого делает пуш, путём проставления настроек (проверяем создание и редактирование)
     * <ul>
     *     <li>1. Интерфейс для создания правил пуш уведомлений</li>
     *     <li>2. Интерфейс настройки правил для пуш уведомлений</li>
     *     <li>3. Ошибка при создании из обычного информера push-виджета</li>
     *     <li>4. При создании пуш виджета устанавливать ему и чайлдам nodel =1</li>
     *     <li>5. Проставление sources_types = 14 и 15 для внутренних пушей</li>
     *     <li>6. Не добавляется пуш провайдер пуш-виджетам 0-3 и 3-10 при превращении виджета в пуш виджет</li>лад
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21256">TA-21256</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21331">TA-21331</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21301">TA-21301</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23610">TA-23610</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23837">TA-23837</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-23835">TA-23835</a>
     * <p>RKO</p>
     */
    @Test
    public void createEditPushWidget() {
        log.info("Test is started");
        int tickerCompositeId_0_3, tickerCompositeId_3_10;

        int tickerCompositeId_10 = 63;

        log.info("create and check push widget");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId_10);
        pagesInit.getCabWidgetsList().openPushPopUp(tickerCompositeId_10);
        Assert.assertTrue(pagesInit.getCabWidgetsList().createEditPushWidget(tickerCompositeId_10), "FAIL -> push widget doesn't created");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkPushWidgetHasNoDeleteFlag(tickerCompositeId_10), "FAIL -> push CREATED doesn't have tickers_composite.nodel =1");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkPushSettingsInForm(tickerCompositeId_10), "FAIL -> push CREATED doesn't check base settings");

        log.info("get CompositeLifetime id and g_blocks ids");
        tickerCompositeId_0_3 = operationMySql.getTickersCompositePushRules().getCompositeLifetime0(tickerCompositeId_10);
        tickerCompositeId_3_10 = operationMySql.getTickersCompositePushRules().getCompositeLifetime1(tickerCompositeId_10);

        log.info("Проставление sources_types = 14 и 15");
        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(tickerCompositeId_10), 14, "FAIL -> sources_types(push widget 10+)");
        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(tickerCompositeId_0_3), 15, "FAIL -> sources_types(push widget 0-3)");
        softAssert.assertEquals(operationMySql.getTickersComposite().getSourcesTypesId(tickerCompositeId_3_10), 15, "FAIL -> sources_types(push widget 3-10)");

        log.info("пуш провайдер пуш-виджетам 0-3 и 3-10");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(tickerCompositeId_10), "1", "FAIL -> push_providers_id(push widget 10+)");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(tickerCompositeId_0_3), "1", "FAIL -> push_providers_id(push widget 0-3)");
        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(tickerCompositeId_3_10), "1", "FAIL -> push_providers_id(push widget 3-10)");

        log.info("edit and check push widget");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().createEditPushWidget(tickerCompositeId_10), "FAIL -> push widget doesn't EDITED");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkPushWidgetHasNoDeleteFlag(tickerCompositeId_10), "FAIL -> push EDITED doesn't have tickers_composite.nodel =1");
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkPushSettingsInForm(tickerCompositeId_10), "FAIL -> push EDITED doesn't check base settings");

        log.info("check push widget logs");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    /**
     * https://youtrack.mgid.com/issue/TA-21331
     * Интерфейс настройки правил для пуш уведомлений
     * <p>
     * тест проверяет что поле "Placement" - не принимает невалидные значения
     * <p>
     * RKO
     */
    @Test(dataProvider = "dataForValidPushWidgetPlacement")
    public void validPushWidgetPlacement(String value) {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + MGID_PUSH_WIDGET_ID);
        pagesInit.getCabWidgetsList().openPushPopUp(MGID_PUSH_WIDGET_ID);
        softAssert.assertFalse(pagesInit.getCabWidgetsList()
                        .setPushCustomField(true)
                        .setPushPlacement(value)
                        .createEditPushWidget(MGID_PUSH_WIDGET_ID),
                "Fail -> push created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Order Content undefined format"), "Fail -> don't correct message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForValidPushWidgetPlacement() {
        return new Object[][]{
                {"tn"},
                {"Trn675#$754"},
                {""},
                {"12"}
        };
    }

    /**
     * https://youtrack.mgid.com/issue/TA-21331
     * Интерфейс настройки правил для пуш уведомлений
     * <p>
     * тест проверяет что поле "PushDelay" - не принимает невалидные значения
     * <p>
     * RKO
     */
    @Test(dataProvider = "dataForValidPushWidgetPushDelay")
    public void validPushWidgetPushDelay(String value, String error) {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + MGID_PUSH_WIDGET_ID);
        pagesInit.getCabWidgetsList().openPushPopUp(MGID_PUSH_WIDGET_ID);
        softAssert.assertFalse(pagesInit.getCabWidgetsList()
                        .setPushCustomField(true)
                        .setPushDelay(value)
                        .createEditPushWidget(MGID_PUSH_WIDGET_ID),
                "Fail -> push created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField(error), "Fail -> don't correct message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataForValidPushWidgetPushDelay() {
        return new Object[][]{
                {"-1", "Push delay wrong range"},
                {"1441", "Push delay wrong range"},
                {"asd", "Push delay undefined format"},
                {"", "Push delay undefined format"}
        };
    }

    /**
     * https://youtrack.mgid.com/issue/TA-21331
     * Интерфейс настройки правил для пуш уведомлений
     * <p>
     * тест проверяет что значение поля Start Hour не может больше чем значение поля End Hour
     * <p>
     * RKO
     */
    @Test
    public void validPushWidgetStartEndHour() {
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + MGID_PUSH_WIDGET_ID);
        pagesInit.getCabWidgetsList().openPushPopUp(MGID_PUSH_WIDGET_ID);
        softAssert.assertFalse(pagesInit.getCabWidgetsList()
                        .setPushCustomField(true)
                        .setPushStartHour("18")
                        .setPushEndHour("3")
                        .createEditPushWidget(MGID_PUSH_WIDGET_ID),
                "Fail -> push created");

        softAssert.assertTrue(helpersInit.getMessageHelper().checkErrorMessageCabForCustomField("Push End Hour Must Be Later Push Start Hour"), "Fail -> don't correct message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/VT-23448 (точного тикета Яценко не нашел)
     * Возможность указывать несколько доменов в правилах пушей
     * тест проверяет что у домена может быть несколько push-widget
     * RKO
     */
    @Test
    public void checkDomainCanHaveOnePushWidget() {
        int tickersCompositeId = 64;
        log.info("Test is started");

        log.info("get siteId for current MGID_PUSH_WIDGET_ID");
        authCabAndGo("wages/widgets/?c_id=" + tickersCompositeId);

        pagesInit.getCabWidgetsList().openPushPopUp(tickersCompositeId);
        Assert.assertTrue(pagesInit.getCabWidgetsList()
                        .createEditPushWidget(tickersCompositeId),
                "FAIL -> push widget doesn't created");

        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/TA-21314
     * Ошибка при сохранении пушового виджета
     * Тест проверяет пересохранение виджета(пуш) с проверкой отображения задизейбленого чекбокса xml
     * RKO
     */
    @Test
    public void editPushWidgetInEditWidgetInterface() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + MGID_PUSH_WIDGET_ID);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkEditPushWidget(), "Fail - checkbox 'XML / RSS' isn't disabled");
        log.info("Test is finished");
    }

    /**
     * https://youtrack.mgid.com/issue/VT-24639
     * Не создается пуш правило для домена если уже имеется правило для субдомена
     * Кейсы:
     * Предусловия:
     * - создан дамп сайта с субдоменом sub.testsitemg.com
     * - создан виджет для субдомена sub.testsitemg.com
     * - создан пуш виджет для домена sub.testsitemg.com
     * - создан виджет для домена testsitemg.com
     * Проверка:
     * - проверка успешного создания пуш виджета для домена testsitemg.com
     * При таких условиях создать пуш виджет не было возможности
     * NIO
     */
    @Test
    public void checkCreatingPushRuleWithDomainAndSubdomain() {
        log.info("Test is started");
        int widgetId = 28;
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        pagesInit.getCabWidgetsList().openPushPopUp(widgetId);
        log.info("Create push rule");
        Assert.assertTrue(pagesInit.getCabWidgetsList().createEditPushWidget(widgetId), "FAIL -> push widget doesn't created");
        log.info("Test is finished");
    }

    /**
     * TA-22948 Не корректно подтягивается пуш-провайдер
     * Проверка смены провайдера на 232/131
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-22948">TA-22948</a>
     */
    @Test(dataProvider = "dataPushProviders")
    public void checkPushProviderSetupTest(String subnetName, String pushProviderId) {
        log.info("Test is started for " + subnetName);

        //update pushProvider in default 1
        operationMySql.getGblocks().updatePushProvider(MGID_PUSH_WIDGET_ID);

        //change pushProvider
        authCabAndGo("wages/informers-edit/type/composite/id/" + MGID_PUSH_WIDGET_ID);
        pagesInit.getCabCompositeSettings()
                .setPushProvider(pushProviderId)
                .choosePushProvider()
                .saveWidgetSettings();

        //check pushProvider
        authCabAndGo("wages/informers-edit/type/composite/id/" + MGID_PUSH_WIDGET_ID);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkPushProvider());
        log.info("Test is finished for " + subnetName);
    }

    @DataProvider
    public static Object[][] dataPushProviders() {
        return new Object[][]{
                {"Idealmedia", "232"},
        };
    }

    /**
     * <ul>
     * <li>1. создание push widget и Проверка его child(clone)</li>
     * <li>2. Проверка клонирования настроек DFP-flag(partners.clients_sites_excluded_tags)</li>
     * <li>3. проставлять g_blocks.push_providers_id = push_providers.push_providers_id, где push_providers.subnet = сабнету виджета</li>
     * <li>4. Копировать g_blocks.spy_feed при создании чайлда: set g_blocks.spy_feed=0 after copy check  child = 0 </li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24140">TA-24140</a>
     * @see <a href="https://youtrack.mgid.com/issue/VT-25281">VT-25281</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27224">TA-27224</a>
     * <p>RKO</p>
     */
    @Test
    public void checkCopySettingsOnCreatePushWidget() {
        int tickerCompositeId_0_3, tickerCompositeId_3_10, tickerCompositeId_10 = 61;

        log.info("Test is started");
        log.info("create push widget");
        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId_10);
        pagesInit.getCabWidgetsList().openPushPopUp(tickerCompositeId_10);
        Assert.assertTrue(pagesInit.getCabWidgetsList().createEditPushWidget(tickerCompositeId_10), "FAIL -> push widget doesn't created");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("get CompositeLifetime id");
        tickerCompositeId_0_3 = operationMySql.getTickersCompositePushRules().getCompositeLifetime0(tickerCompositeId_10);
        tickerCompositeId_3_10 = operationMySql.getTickersCompositePushRules().getCompositeLifetime1(tickerCompositeId_10);

        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId_0_3);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpPublisher.DfpValues.ON, tickerCompositeId_0_3), "FAIL -> DFP flag doesn't copy for tickerCompositeId_0_3");

        authCabAndGo("wages/widgets/?c_id=" + tickerCompositeId_3_10);
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDfpIcon(DfpPublisher.DfpValues.ON, tickerCompositeId_3_10), "FAIL -> DFP flag doesn't copy for tickerCompositeId_3_10");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_0_3);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().setPushProvider("1").checkPushProvider(), "FAIL -> ui: g_blocks.push_providers_id for tickerCompositeId_0_3");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId_3_10);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().setPushProvider("1").checkPushProvider(), "FAIL -> ui: g_blocks.push_providers_id for tickerCompositeId_3_10");

        softAssert.assertEquals(operationMySql.getGblocks().getSpyFeed(tickerCompositeId_0_3), 0,"FAIL -> tickerCompositeId_0_3: g_blocks.spy_feed");
        softAssert.assertEquals(operationMySql.getGblocks().getSpyFeed(tickerCompositeId_3_10), 0,"FAIL -> tickerCompositeId_3_10: g_blocks.spy_feed");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
