package cab.publishers.widgets;

import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;

public class EditCodeTests extends TestBase {


    /**
     * Не работает предпросмотр информера в интерфейсе редактирования кода
     * @see <a href="https://jira.mgid.com/browse/TA-51726">Ticket TA-51726</a>
     * <p>RKO</p>
     */
    @Test
    public void checkWidgetView() {
        log.info("Test is started.");
        int widgetId = 241;
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
        authDashAndGo("testEmail49@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_RECTANGULAR);

        authCabAndGo("wages/informers-settings/id/" + widgetId);
        pagesInit.getCabEditCode().clickViewButton();
        pagesInit.getCabEditCode().switchToViewIframe();

        Assert.assertTrue(pagesInit.getWidgetClass().checkUnderArticleForViewInCab());
        log.info("Test is finished.");
    }
}
