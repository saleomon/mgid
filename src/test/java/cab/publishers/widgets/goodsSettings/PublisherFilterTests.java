package cab.publishers.widgets.goodsSettings;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.goodWidgetSettingUrl;

public class PublisherFilterTests extends TestBase {

    private final int goodsId = 39;

    @BeforeMethod
    public void getUpdatedTime() {
        updatedTime = operationMySql.getGblocks().getUpdatedTime(goodsId);
    }

    /**
     * Проверка фильтра по источникам в настройках товарной части информера
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21872">TA-21872</a>
     */
    @Test(dataProvider = "partnersFiltersCases")
    public void workPublishersFilter(int blockId, String filterType, int campaignId) {
        log.info("Test is started");
        authCabAndGo(goodWidgetSettingUrl + blockId);
        pagesInit.getCabGoodsSettings()
                .choosePublishersFilterType(filterType, campaignId);
        helpersInit.getMessageHelper().isSuccessMessagesCab();

        authCabAndGo(goodWidgetSettingUrl + blockId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkPublishersFilter(filterType, campaignId));
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] partnersFiltersCases() {
        return new Object[][]{
                {goodsId, "1", 7},
                {goodsId, "2", 8},
                {goodsId, "0", 8}
        };
    }
}
