package cab.publishers.widgets.goodsSettings;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.goodWidgetSettingUrl;

public class AdsFilterTests extends TestBase {

    /**
     * При выведении попапа не выводится список id новостей/тизеров уже проставленных в фильтр
     * CAB | Ошибка в консоли при добавлении большого кол-ва id в фильтр информера по тизерам.
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21872">Ticket TA-21872</a>
     * @see <a href="https://youtrack.mgid.com/issue/TA-21434">TA-21434</a>
     * <p>RKO</p>
     */

    private final int goodsId = 41;
    private final String teasersId = "1, 3, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, " +
            "21, 22, 23, 24, 25, 26, 27, 28, 29, 1001, 1003, 2001, 2002";

    @BeforeMethod
    public void getUpdatedTime(){
        updatedTime = operationMySql.getGblocks().getUpdatedTime(goodsId);
    }


    public void clearAdsFilter(){
        operationMySql.getgBlocksTeasersFilter().clearAllTeasersInFilter(goodsId);
        operationMySql.getGblocks().updateAdsFilter(goodsId, 0);
    }

    public void insertAdsFilter(){
        operationMySql.getgBlocksTeasersFilter().clearAllTeasersInFilter(goodsId);
        operationMySql.getgBlocksTeasersFilter().insertTeasersInAdsFilter(goodsId, 1, 3, 2);
        operationMySql.getGblocks().updateAdsFilter(goodsId, 1);
    }


    /**
     * check 'EXCEPT' radio in Ads filter
     * <p>RKO</p>
     */
    @Test
    public void adsFilterExcept() {
        log.info("Test is started");
        clearAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        log.info("Enable select 'EXCEPT' and set ads to ads container");
        pagesInit.getCabGoodsSettings().setOptionAdsFilterSelect("Except")
                .setOptionsAdsFilterContainer(teasersId)
                .changeStateAdsFilter();

        log.info("Check saved filter ads settings with 'EXCEPT' options");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdsFilterSetting(), "FAIL - Check saved filter ads settings with Except options");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check 'ONLY' radio in Ads filter
     * <p>RKO</p>
     */
    @Test
    public void adsFilterOnly() {
        log.info("Test is started");
        clearAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);

        pagesInit.getCabGoodsSettings().setOptionAdsFilterSelect("Only")
                .setOptionsAdsFilterContainer(teasersId)
                .changeStateAdsFilter();

        log.info("Check saved filter ads settings with 'ONLY' options");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdsFilterSetting(), "FAIL - Check saved filter ads settings with Only options");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check 'NO' radio in Ads filter
     * <p>RKO</p>
     */
    @Test
    public void adsFilterNo() {
        log.info("Test is started");
        insertAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings()
                .setOptionAdsFilterSelect("No")
                .changeStateAdsFilter(pagesInit.getCabGoodsSettings().getOptionAdsFilterSelect())
                .saveWidgetSettings();

        log.info("Check saved filter ads settings with 'NO' options");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdsFilterSelectedValue(pagesInit.getCabGoodsSettings().getOptionAdsFilterSelect()), "FAIL -> checkAdsFilterSelectedValue");
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().isDisplayedEditButton(), "FAIL - isDisplayedEditButton No options");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * edit Ads filter
     * <p>RKO</p>
     */
    @Test
    public void adsFilterEdit() {
        log.info("Test is started");
        insertAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);

        pagesInit.getCabGoodsSettings().setOptionAdsFilterSelect("Only")
                .setOptionsAdsFilterContainer("1, 3, 5")
                .editAdsFilter();

        log.info("Check saved filter ads settings with 'ONLY' options");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAdsFilterSetting(), "FAIL - Check saved filter ads settings with Only options");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check validation Ads filter in create
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataValidation")
    public void adsFilterCreateValidation(String value, String message) {
        log.info("Test is started");
        clearAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);

        pagesInit.getCabGoodsSettings().setOptionAdsFilterSelect("Only")
                .setOptionsAdsFilterContainer(value)
                .changeStateAdsFilter();

        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getAdsFilterErrorMessage(), message, "FAIL -> message");

        softAssert.assertAll();
        log.info("Test is finished");
    }


    /**
     * check validation Ads filter in edit
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataValidation")
    public void adsFilterEditValidation(String value, String message) {
        log.info("Test is started");
        insertAdsFilter();

        authCabAndGo(goodWidgetSettingUrl + goodsId);

        pagesInit.getCabGoodsSettings().setOptionAdsFilterSelect("Only")
                .setOptionsAdsFilterContainer(value)
                .editAdsFilter();

        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getAdsFilterErrorMessage(), message, "FAIL -> message");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataValidation(){
        return new Object[][]{
                {"<>", "No stated goods found"},
                {" ", "No stated goods found"},
                {"11111", "No stated goods found\n11111"}
        };
    }
}
