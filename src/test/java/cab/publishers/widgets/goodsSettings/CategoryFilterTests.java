package cab.publishers.widgets.goodsSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.List;

import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.EndPoints.goodWidgetSettingUrl;

public class CategoryFilterTests extends TestBase {

    private ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>();
    private final int goodsId = 40;

    public void getUpdatedTime() {
        updatedTime = operationMySql.getGblocks().getUpdatedTime(goodsId);
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Check search(input) publisher category<a href='https://jira.mgid.com/browse/MT-245'>MT-245</a>")
    @Test
    public void publisherCategoryCheckSearchCategoryInFilter() {
        log.info("Test is started");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType("1");
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkSearchCategoryFilter(false));
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Check publisher category in widgets list(pop-up)<a href='https://jira.mgid.com/browse/VT-22011'>VT-22011</a>")
    @Test
    public void publisherCategoryCheckIconCategoryInWidgetList() {
        log.info("Test is started");
        getUpdatedTime();

        authCabAndGo(goodWidgetSettingUrl + goodsId);

        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType("1");
        pagesInit.getCabGoodsSettings().clearAllSelectedCategoryInMultiFilter();
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        ArrayList<String> chooseCategoryInEditInterface = helpersInit.getMultiFilterHelper().chooseCustomValueInMultiFilterAndGetTheirData(4);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        pagesInit.getCabWidgetsList().clickCategoryFilterIcon();

        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoryFilterDataInIcon(chooseCategoryInEditInterface), "FAIL -> categoryFilter_checkIconCategoryInWidgetList");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Беремо віджет с g_blocks.use_cat=1 - переходимо в інтерфейс та змінюємо на use_cat=2 (перевіряємо вибрані категорії) <a href='https://jira.mgid.com/browse/TA-24427'>TA-24427</a>")
    @Test
    public void publisherCategoryEditOnlyOnExcept() {
        log.info("Test is started");
        insertCategoryFilterForWidget(1);
        checkCategoryFilter("2");
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Беремо віджет с g_blocks.use_cat=2 - переходимо в інтерфейс та змінюємо на use_cat=1 (перевіряємо вибрані категорії) <a href='https://jira.mgid.com/browse/TA-24427'>TA-24427</a>")
    @Test
    public void publisherCategoryEditExceptOnOnly() {
        log.info("Test is started");
        insertCategoryFilterForWidget(2);
        checkCategoryFilter("1");
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Беремо віджет с g_blocks.use_cat=2 - переходимо в інтерфейс та змінюємо на use_cat=0 (перевіряємо що блок категорій не відображається) <a href='https://jira.mgid.com/browse/TA-24427'>TA-24427</a>")
    @Test
    public void publisherCategoryEditExceptOnNo() {
        log.info("Test is started");
        insertCategoryFilterForWidget(2);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType("0");
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertFalse(pagesInit.getCabGoodsSettings().categoryFilterBlockIsDisplayed(), "FAIL -> category filter is visible");
        softAssert.assertEquals(operationMySql.getGblocks().getUseCat(goodsId), "0", "FAIL -> use_cat=1");
        softAssert.assertFalse(operationMySql.getgBlocksCategories().isExistCategoryInWidget(goodsId), "FAIL -> categories is exist");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Усі категорії мають значення g_category.disable_default_category=0 - створюємо віджет, віджету проставляється use_cat=0.\n" +
            " Проставляємо g_category.id=100 disable_default_category=1 - створюємо віджет, віджету проставляється use_cat=2,\n " +
            "а у g_blocks_categories для цього віджету проставляється категорія 100 <a href='https://jira.mgid.com/browse/VT-26588'>VT-26588</a>")
    @Test
    public void publisherCategoryDisableDefaultCategory() {
        int goodsId;
        log.info("Test is started");
        log.info("g_category.disable_default_category=0");
        Assert.assertFalse(operationMySql.getgCategory().isExistDisableDefaultCategory(), "FAIL -> g_category.disable_default_category=1 is have");
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
        pagesInit.getWidgetClass().createSimpleWidget();
        goodsId = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("0"), "FAIL -> categoryFilter_Only UI");
        softAssert.assertEquals(operationMySql.getGblocks().getUseCat(goodsId), "0", "FAIL -> use_cat=0");
        softAssert.assertFalse(operationMySql.getgBlocksCategories().isExistCategoryInWidget(goodsId), "FAIL -> categories is exist");

        try {
            log.info("g_category.disable_default_category=1");
            operationMySql.getgCategory().updateDisableDefaultCategoryValue(100, 1);
            chooseCategoryInEditInterface.clear();
            chooseCategoryInEditInterface.add("Automotive");
            authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
            pagesInit.getWidgetClass().createSimpleWidget();
            goodsId = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());
            authCabAndGo(goodWidgetSettingUrl + goodsId);
            helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
            softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue("2"), "FAIL -> categoryFilter_Only UI");
            softAssert.assertEquals(operationMySql.getGblocks().getUseCat(goodsId), "2", "FAIL -> use_cat!=2");
            softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> not equals categories");
            softAssert.assertEquals(operationMySql.getgBlocksCategories().getCategoriesCountForWidget(goodsId), chooseCategoryInEditInterface.size(), "FAIL -> g_blocks_categories.size > 1");
        } finally {
            operationMySql.getgCategory().updateDisableDefaultCategoryValue(100, 0);
        }
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Возможность исключать ad types и landing types в фильтре категорий виджета <a href='https://jira.mgid.com/browse/TA-24682'>TA-24682</a>")
    @Test(dataProvider = "categoryFilterValue")
    public void publisherCategoryExceptOnlyAdTypesAndLanding(String name, String categoryFilterVal) {
        log.info("Test is started: " + name);
        log.info("Setting preconditions");
        operationMySql.getgBlocksCategories().deleteAllCategories(goodsId);

        log.info("choose adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType(categoryFilterVal);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabGoodsSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(4);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check chosen adTypes and landingTypes in Category Filter");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface));
        log.info("Test is finished: " + name);
    }

    @DataProvider
    public Object[][] categoryFilterValue() {
        return new Object[][]{
                {"1", "1"},
                {"2", "2"}
        };
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: Publisher category filter")
    @Description("Check saving new category filter after presetted category with ad or landing types <a href='https://jira.mgid.com/browse/TA-52042'>TA-52042</a>")
    @Test(dataProvider = "categoryFilterAdOrLandingTypesData")
    public void publisherCategoryCheckCorrectSavingAfterPresettedValues(String type, Boolean isAdType) {
        log.info("Test is started");
        log.info("Setting precondition");
        int goodsIdForSavingTest = 7016;
        operationMySql.getgBlocksCategories().deleteAllCategories(goodsIdForSavingTest);
        operationMySql.getGblocks().updateUseCat(goodsIdForSavingTest, 0);

        log.info("Saving category with Ad or Landing type");
        authCabAndGo(goodWidgetSettingUrl + goodsIdForSavingTest);
        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType("2");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Travel");
        pagesInit.getCabGoodsSettings().clickAdTypeOrLandingTypeForCategory("Travel", isAdType);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        softAssert.assertNull(operationMySql.getgBlocksCategories().getAdOrLandingTypeForWidgetByCategoryID(goodsIdForSavingTest, 206, type), "FAIL - > Wrong Landing type was saved");
        softAssert.assertTrue(operationMySql.getgBlocksCategories().isExistCategoryInWidget(goodsIdForSavingTest), "Fail - > Category wasn't added");

        log.info("Adding new category without Ad and landing types");
        authCabAndGo(goodWidgetSettingUrl + goodsIdForSavingTest);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Pets");
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab());

        List list = operationMySql.getgBlocksCategories().getAdAndLandingTypeForWidgetByCategoryID(goodsIdForSavingTest, 251);
        softAssert.assertNull(list.get(0), "Fail - > Wrong l type was set");
        softAssert.assertNull(list.get(1), "Fail - > Wrong ad type was set");
        softAssert.assertEquals(operationMySql.getgBlocksCategories().getCategoriesCountForWidget(goodsIdForSavingTest), 2, "Fail - > Category wasn't added");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] categoryFilterAdOrLandingTypesData() {
        return new Object[][]{
                {"landing_types", true},
                {"ad_types", false}
        };
    }

    private void checkCategoryFilter(String filterValue) {
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().clearAllSelectedCategoryInMultiFilter();
        pagesInit.getCabGoodsSettings().chooseCategoriesFilterType(filterValue);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        chooseCategoryInEditInterface = helpersInit.getMultiFilterHelper().chooseCustomValueInMultiFilterAndGetTheirData(2);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCategoriesFilterTypeValue(filterValue), "FAIL -> categoryFilter_Only UI");
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> not equals categories");
        softAssert.assertEquals(operationMySql.getGblocks().getUseCat(goodsId), filterValue, "FAIL -> use_cat=1");
        softAssert.assertAll();
    }

    private void insertCategoryFilterForWidget(int filterValue) {
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface.add("Automotive");
        operationMySql.getgBlocksCategories().deleteAllCategories(goodsId);
        operationMySql.getgBlocksCategories().insertCategories(goodsId);
        operationMySql.getGblocks().updateUseCat(goodsId, filterValue);
    }
}

