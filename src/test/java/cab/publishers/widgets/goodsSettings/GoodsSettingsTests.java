package cab.publishers.widgets.goodsSettings;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static core.helpers.BaseHelper.randomNumberFromRange;
import static core.service.constantTemplates.ConstantsInit.NIO;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static testData.project.EndPoints.goodWidgetSettingUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;


/*
 * Campaign tier filter
 *      - checkCampaignTierFilter
 *
 * Show description
 * Description required
 *      - checkShowDescription_And_DescriptionRequired
 *
 * Include RTB teasers:
 *      - includeRtbTeasers
 *
 * Include Fake Teasers:
 *      - includeFakeTeasers
 *
 * Rotate only teasers of current subnet
 *      - rotateOnlyTeasersOfCurrentSubnet
 *
 * Rotate in Adskeeper subnet:
 *      - rotateInAdskeeperSubnet
 *
 * Do not include this widget for goodhits CTR calculation:
 *      - doNotIncludeThisWidgetForGoodhitsCtrCalculation
 *
 * Anti-duplication:
 *      - antiDuplication
 *
 * Pagination: Amount of pages:
 *      - paginationAmountOfPages
 *      - paginationAmountOfPages_validation
 *
 * Product transition page display number:
 *      - productTransitionPageDisplayNumber
 *      - productTransitionPageDisplayNumber_validation
 *
 * Exclude Agency fee from publisher wages:
 *      - excludeAgencyFeeFromPublisherWages
 *
 * Default minimal CPC decreasing factor:
 *      - defaultMinimalCpcDecreasingFactor
 *      - dataDefaultMinimalCpcDecreasingFactor
 *
 * Set minimal CPC:
 *      - setMinimalCpc_forAllRegions
 *      - setMinimalCpc_forCustomRegions
 *
 * Set minimal CPM:
 *      - setMinimalCpm
 *
 * Show teasers:
 *      - showTeasersFilterRadio
 *
 * Animated:
 *      - animationFilter
 *
 * Duplicates count:
 *      - duplicatesCount
 *
 * MGID inventory based widget:
 *      - mgidInventoryBasedWidget
 */
public class GoodsSettingsTests extends TestBase {

    private final int goodsId = 247;

    @BeforeMethod
    public void getUpdatedTime() {
        updatedTime = operationMySql.getGblocks().getUpdatedTime(goodsId);
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Exclude Agency fee from publisher wages")
    @Owner(RKO)
    @Test(description = "Exclude Agency fee from publisher wages")
    public void excludeAgencyFeeFromPublisherWages() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchExcludeAgencyFeeFromPublisherWages(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkExcludeAgencyFeeFromPublisherWages(true), "FAIL -> agency_fee_to_wages switch_on in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getAgencyFeeToWages(goodsId), 1, "FAIL -> agency_fee_to_wages switch_on in DB");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchExcludeAgencyFeeFromPublisherWages(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkExcludeAgencyFeeFromPublisherWages(false), "FAIL -> agency_fee_to_wages switch_off in UI");
        softAssert.assertEquals(operationMySql.getGblocks().getAgencyFeeToWages(goodsId), 0, "FAIL -> agency_fee_to_wages switch_on in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Default minimal CPC decreasing factor")
    @Owner(RKO)
    @Test(description = "Default minimal CPC decreasing factor")
    public void defaultMinimalCpcDecreasingFactor() {
        log.info("Test is started");

        String defaultMinimalCpc = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.1, 0.9);
        log.info("set defaultMinimalCpc -> " + defaultMinimalCpc);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setDefaultMinimalCpcDecreasingFactor(defaultMinimalCpc);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check defaultMinimalCpc");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getDefaultMinimalCpcDecreasingFactor(), defaultMinimalCpc);
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getDefaultMinCpcDecreasingFactor(goodsId), defaultMinimalCpc, "FAIL -> default_min_cpc_decreasing_factor");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Default minimal CPC decreasing factor: validation")
    @Owner(RKO)
    @Test(description = "Default minimal CPC decreasing factor: validation", dataProvider = "dataDefaultMinimalCpcDecreasingFactor")
    public void dataDefaultMinimalCpcDecreasingFactor(String defaultMinimalCpc, String message) {
        log.info("Test is started");

        log.info("set defaultMinimalCpc -> " + defaultMinimalCpc);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setDefaultMinimalCpcDecreasingFactor(defaultMinimalCpc);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check defaultMinimalCpc");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(message));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDefaultMinimalCpcDecreasingFactor() {
        return new Object[][]{
                {" ", "Default minimal CPC decreasing factor: can not be empty, can not be less than 0.1"},
                {"as^^", "Default minimal CPC decreasing factor: can not be empty, can not be less than 0.1"},
                {"0", "Default minimal CPC decreasing factor: can not be empty, can not be less than 0.1"},
                {"1.1", "Default minimal CPC decreasing factor: can not be more than 1"},
                {"-0.1", "Default minimal CPC decreasing factor: can not be less than 0.1"}
        };
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Set minimal CPC: for all regions")
    @Owner(RKO)
    @Test(description = "Set minimal CPC: for all regions")
    public void setMinimalCpc_forAllRegions() {
        log.info("Test is started");
        String cpc = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.1, 100.0);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setMinimalCpc(cpc);
        helpersInit.getBaseHelper().checkAlertAndClose();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().clickMinimalCpcIcon();
        pagesInit.getCabGoodsSettings().expandMinCpcCountries();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkMinimalCpcForAllRegions(cpc), "FAIL -> check CPC in form");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getMinimalPriceOfClick(goodsId),
                (cpc.contains(".") ? cpc.substring(0, cpc.indexOf(".")) : cpc),
                "FAIL -> minimal_price_of_click");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Set minimal CPC: for custom regions")
    @Owner(RKO)
    @Test(description = "Set minimal CPC: for custom regions")
    public void setMinimalCpc_forCustomRegions() {
        log.info("Test is started");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setMinimalCpc("0");
        helpersInit.getBaseHelper().checkAlertAndClose();
        pagesInit.getCabGoodsSettings().clickMinimalCpcIcon();
        pagesInit.getCabGoodsSettings().expandMinCpcCountries();
        pagesInit.getCabGoodsSettings().getRandomRegionGroup();
        pagesInit.getCabGoodsSettings().setMinimalCpcForCustomGroup();
        pagesInit.getCabGoodsSettings().switchMinimalCpcUpset(true);
        pagesInit.getCabGoodsSettings().clickMinimalCpcSubmit();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().clickMinimalCpcIcon();
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkMinimalCpcForCustomGroup(), "FAIL -> minimalCpc in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkMinimalCpcUpset(true), "FAIL -> upset");
        softAssert.assertTrue(operationMySql.getgBlocksGeoZonesPricesLimits().checkMinimalCpc(goodsId,
                pagesInit.getCabGoodsSettings().getRegionIdMinPrices()), "FAIL -> minimalCpc in db");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Animated filter <a href='https://jira.mgid.com/browse/KOT-3498'>KOT-3498</a>")
    @Owner(RKO)
    @Test(description = "Animated filter")
    public void animationFilter() {
        log.info("Test is started");
        String animation = "0", animationAddGranularity;
        authCabAndGo(goodWidgetSettingUrl + goodsId);

        log.info("Select random value for 'Animation'");
        animationAddGranularity = pagesInit.getCabGoodsSettings().chooseAnimation();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        if(animationAddGranularity.equals("-1")){
            animation = "2";
            animationAddGranularity = "0";
        }

        log.info("Check 'Animation' set value");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAnimationFilterValue(animationAddGranularity),
                "FAIL -> animation in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        int tickersCompositeId = 325;
        softAssert.assertEquals(operationMySql.getGblocks().getAnimation(tickersCompositeId), animation,
                "FAIL -> animation in DB");
        softAssert.assertEquals(operationMySql.getGblocks().getAnimationAddGranularity(goodsId), animationAddGranularity,
                "FAIL -> animation in DB");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Show teasers filter")
    @Owner(RKO)
    @Test(description = "Show teasers filter")
    public void showTeasersFilterRadio() {
        log.info("Test is started");
        authCabAndGo(goodWidgetSettingUrl + goodsId);

        log.info("Select random value for 'Show teasers'");
        pagesInit.getCabGoodsSettings().chooseShowTeasersRadio();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("Check 'Show teasers' set value");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkShowTeasersFilterValue(),
                "FAIL -> female_content in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getFemaleContent(goodsId), pagesInit.getCabGoodsSettings().getShowTeasersValue(),
                "FAIL -> female_content in DB");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Duplicates count filter")
    @Owner(RKO)
    @Test(description = "Duplicates count filter")
    public void duplicatesCount() {
        log.info("Test is started");

        String duplicatesValue = randomNumberFromRange(1, 200);
        log.info("set duplicatesValue -> " + duplicatesValue);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setDuplicatesCountInput(duplicatesValue);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check duplicatesValue");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getDuplicatesCountInput(), duplicatesValue);
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getDuplicatesCount(goodsId), duplicatesValue, "FAIL -> duplicates_count");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("MGID inventory based widget")
    @Owner(RKO)
    @Test(description = "MGID inventory based widget")
    public void mgidInventoryBasedWidget() {
        log.info("Test is started");

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().chooseMgidInventoryBasedWidget();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getSelectedMgidInventoryBasedWidget(), pagesInit.getCabGoodsSettings().getMgidInventoryBasedWidget());
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getOurBlockCount(goodsId), pagesInit.getCabGoodsSettings().getMgidInventoryBasedWidget(), "FAIL -> our_block");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Set minimal CPM")
    @Owner(RKO)
    @Test(description = "Set minimal CPM")
    public void setMinimalCpm() {
        log.info("Test is started");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().clickMinimalCpmIcon();
        pagesInit.getCabGoodsSettings().chooseCountryAndSetThemCpm();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkMinimalCpm(), "FAIL -> checkMinimalCpm UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertTrue(operationMySql.getgBlocksCountryProperties().checkCountryCpm(goodsId, pagesInit.getCabGoodsSettings().getCountryCpm()), "FAIL -> checkMinimalCpm db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Widgets")
    @Feature("Widget Settings")
    @Story("Goods settings :: general")
    @Description("Include RTB teasers (checkbox + privilege) <a href=\"https://youtrack.mgid.com/issue/VT-22702\">Ticket VT-22702</a>")
    @Owner(NIO)
    @Privilege(name = "can_see_rtb_enabled")
    @Test(description = "Include RTB teasers")
    public void onOffRtbOption() {
        try {
            log.info("Test is started");
            String widgetId = "1006";
            String goodsPartLink;
            authCabForCheckPrivileges("wages/widgets/?c_id=" + widgetId);
            authCabAndGo(goodsPartLink = pagesInit.getCabWidgetsList().getLinkEditProductSubWidget());
            log.info("Enable rtb option and save widget");
            pagesInit.getCabCompositeSettings().changeStateOfRtbOption(true);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();
            authCabAndGo(goodsPartLink);
            log.info("Check rtb option enabled");
            softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkStateOfRtbCheckbox(), "FAIL - Check rtb option enabled");

            log.info("Disable rtb option and save widget");
            pagesInit.getCabCompositeSettings().changeStateOfRtbOption(false);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();
            authCabAndGo(goodsPartLink);
            log.info("Check rtb option disabled");
            softAssert.assertFalse(pagesInit.getCabCompositeSettings().checkStateOfRtbCheckbox(), "FAIL - Check rtb option disabled");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_see_rtb_enabled");
            authCabAndGo(goodsPartLink);
            log.info("Check visibility of rtb option");
            softAssert.assertFalse(pagesInit.getCabCompositeSettings().checkVisibilityRtbOption(), "FAIL - Check visibility of rtb option");
            softAssert.assertAll();
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_see_rtb_enabled");
        }
    }

}
