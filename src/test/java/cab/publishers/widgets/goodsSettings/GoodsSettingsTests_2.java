package cab.publishers.widgets.goodsSettings;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.helpers.BaseHelper.randomNumberFromRange;
import static testData.project.ClientsEntities.CLIENTS_EMAIL;
import static testData.project.EndPoints.goodWidgetSettingUrl;

public class GoodsSettingsTests_2 extends TestBase {

    private final int goodsId = 33;

    @BeforeMethod
    public void getUpdatedTime() {
        updatedTime = operationMySql.getGblocks().getUpdatedTime(goodsId);
    }

    /**
     * В настройках виджета добавить фильтр Campaign Tier
     * <ul>
     * <li>1. Create new widget in dashboard and check in cab 'Campaign Tier Filter' value</li>
     * <li>2. Change and save in cab 'Campaign Tier Filter' value and next check saved settings</li>
     * <li>3. Change and save widget in dashboard and check in cab that value for 'Campaign Tier Filter' hasn`t changed</li>
     * <li>4. Check logging</li>
     * <li>5. Check tooltip</li>
     * </ul>
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23366">TA-23366</a>
     */
    @Test
    public void checkCampaignTierFilter() {
        log.info("Test is started");
        int tickersCompositeId = 37;
        authCabAndGo(goodWidgetSettingUrl + goodsId);

        log.info("Select random value for 'Campaign tier filter'");
        pagesInit.getCabGoodsSettings().chooseCampaignTierRadio();
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("Check 'Campaign tier filter' set value");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCampaignTierFilterValue(),
                "false -> tier filter after changing in cab");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertTrue(operationMySql.getgBlocksCampaignTier().checkCampaignTier(pagesInit.getCabGoodsSettings().getCampaignTierFilterValue(), goodsId),
                "FAIL -> campaign_tier switch_on");

        log.info("Check 'Campaign tier filter' set value after edit widget in Dashboard");
        authDashAndGo(CLIENTS_EMAIL,"publisher/edit-widget/id/" + tickersCompositeId);
        pagesInit.getWidgetClass().reSaveWithEditWidget();
        authCabAndGo(goodWidgetSettingUrl + goodsId);

        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkCampaignTierFilterValue(),
                "false -> tier filter after editing widget in dash");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Вывести фильтр description_required в интерфейс CAB
     * <ul>
     * <li>1. Если "Show description" не отмечен, то "Description required" неактивен.</li>
     * <li>2. Если "Show description" отмечен, то "Description required" активен.</li>
     * <li>3. Активный "Description required" по умолчанию не отмечен.</li>
     * <li>4. Включить "Description required" и сохранить виджет.</li>
     * <li>5. Зайти в настройки виджета и проверить, что "Description required" отмечен.</li>
     * <li>6. Выключить "Show description", при этом "Description required" тоже должен выключиться.</li>
     * <li>7. Проверить текст подсказки "i" при наведении: "If you enable this option, then teasers without a description will not be able to display in the widget".</li>
     * </ul>
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23135">TA-23135</a>
     */
    @Test
    public void checkShowDescription_And_DescriptionRequired() {
        log.info("Test is started.");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getDescriptionRequiredLabel(),
                "If you enable this option, then teasers without a description will not be able to display in the widget",
                "Tooltip text for 'Description required' checkbox doesn't match to expected result.");

        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkDescriptionRequiredDependency(),
                "'Description required' checkbox dependency from 'Show description checkbox' failed.");

        pagesInit.getCabGoodsSettings().switchDescriptionRequired(true);
        pagesInit.getCabGoodsSettings().switchShowDescription(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().descriptionRequiredIsSelected(true),
                "'Description required' checkbox didn't turn on.");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getShowDescription(goodsId), 1, "FAIL -> show_description switch_on");
        softAssert.assertEquals(operationMySql.getTickersComposite().getDescriptionRequired(goodsId), 1, "FAIL -> description_required switch_on");

        pagesInit.getCabGoodsSettings().switchDescriptionRequired(false);
        pagesInit.getCabGoodsSettings().switchShowDescription(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().descriptionRequiredIsSelected(false),
                "'Description required' checkbox didn't turn off.");
        softAssert.assertEquals(operationMySql.getGblocks().getShowDescription(goodsId), 0, "FAIL -> show_description switch_on");
        softAssert.assertEquals(operationMySql.getTickersComposite().getDescriptionRequired(goodsId), 0, "FAIL -> description_required switch_on");
        softAssert.assertAll();
        log.info("Test is finished.");
    }

    /**
     * Include RTB Teasers
     * <p>RKO</p>
     */
    @Test
    public void includeRtbTeasers() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchIncludeRtbTeasers(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkIncludeRtbTeasers(true), "FAIL -> switch_on rtb_enabled UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getRtbEnabled(goodsId), 1, "FAIL -> rtb_enabled switch_on in db");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchIncludeRtbTeasers(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkIncludeRtbTeasers(false), "FAIL -> switch_off rtb_enabled UI");
        softAssert.assertEquals(operationMySql.getGblocks().getRtbEnabled(goodsId), 0, "FAIL -> rtb_enabled switch_off in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Include Fake Teasers
     * <p>RKO</p>
     */
    @Test
    public void includeFakeTeasers() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchIncludeFakeTeasers(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkIncludeFakeTeasers(true));
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getIncludeFakeTeasers(goodsId), "1", "FAIL -> include_fake_teasers switch_on");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchIncludeFakeTeasers(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkIncludeFakeTeasers(false));
        softAssert.assertEquals(operationMySql.getGblocks().getIncludeFakeTeasers(goodsId), "0", "FAIL -> include_fake_teasers switch_off");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Rotate only teasers of current subnet
     * <p>RKO</p>
     */
    @Test
    public void rotateOnlyTeasersOfCurrentSubnet() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchRotateOnlyTeasersOfCurrentSubnet(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkRotateOnlyTeasersOfCurrentSubnet(true), "FAIL -> switch_on only_own_subnet UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getOnlyOwnSubnet(goodsId), 1, "FAIL -> only_own_subnet switch_on in db");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchRotateOnlyTeasersOfCurrentSubnet(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkRotateOnlyTeasersOfCurrentSubnet(false), "FAIL -> switch_off only_own_subnet UI");
        softAssert.assertEquals(operationMySql.getGblocks().getOnlyOwnSubnet(goodsId), 0, "FAIL -> only_own_subnet switch_on in db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Rotate in Adskeeper subnet
     * <p>RKO</p>
     */
    @Test
    public void rotateInAdskeeperSubnet() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchRotateInAdskeeperSubnet(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkRotateInAdskeeperSubnet(true), "FAIL -> switch_on g_blocks_subnets UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getgBlocksSubnets().isSetGBlocksSubnets(goodsId), true, "FAIL -> g_blocks_subnets switch_on in Db");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchRotateInAdskeeperSubnet(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkRotateInAdskeeperSubnet(false), "FAIL -> switch_off g_blocks_subnets UI");
        softAssert.assertEquals(operationMySql.getgBlocksSubnets().isSetGBlocksSubnets(goodsId), false, "FAIL -> g_blocks_subnets switch_off in Db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Do not include this widget for goodhits CTR calculation
     * <p>RKO</p>
     */
    @Test
    public void doNotIncludeThisWidgetForGoodhitsCtrCalculation() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchDoNotIncludeThisWidgetForGoodhitsCtrCalculation(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkDoNotIncludeThisWidgetForGoodhitsCtrCalculation(true), "FAIL -> switch_on no_calc_ghits_ctr UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getNoCalcGhitsCtr(goodsId), 1, "FAIL -> no_calc_ghits_ctr switch_on in db");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchDoNotIncludeThisWidgetForGoodhitsCtrCalculation(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkDoNotIncludeThisWidgetForGoodhitsCtrCalculation(false), "FAIL -> switch_off no_calc_ghits_ctr UI");
        softAssert.assertEquals(operationMySql.getGblocks().getNoCalcGhitsCtr(goodsId), 0, "FAIL -> no_calc_ghits_ctr switch_on in db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Anti-Duplication
     * <p>RKO</p>
     */
    @Test
    public void antiDuplication() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().switchAntiDuplication(true);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAntiDuplication(true), "FAIL -> switch_on block_duplicate UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getBlockDuplicate(goodsId), 1, "FAIL -> block_duplicate switch_on in db");

        log.info("switch off");
        pagesInit.getCabGoodsSettings().switchAntiDuplication(false);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertTrue(pagesInit.getCabGoodsSettings().checkAntiDuplication(false), "FAIL -> switch_off block_duplicate UI");
        softAssert.assertEquals(operationMySql.getGblocks().getBlockDuplicate(goodsId), 0, "FAIL -> block_duplicate switch_on in db");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Pagination: Amount of pages:
     * <p>RKO</p>
     */
    @Test
    public void paginationAmountOfPages() {
        log.info("Test is started");

        String paginationValue = randomNumberFromRange(1, 20);
        log.info("set pagination -> " + paginationValue);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setPagesCountInput(paginationValue);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check pagination");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getPagesCountInput(), paginationValue);
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getPagesCount(goodsId), paginationValue, "FAIL -> pages_count");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Pagination: Amount of pages: validation
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataPagesCount")
    public void paginationAmountOfPages_validation(String pagesCount) {
        log.info("Test is started");

        log.info("set pagination -> " + pagesCount);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setPagesCountInput(pagesCount);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Pagination: Amount of pages: Field error"));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataPagesCount() {
        return new Object[][]{
                {" "},
                {"0"},
                {"21"},
                {"-1"},
                {"as^^"}
        };
    }

    /**
     * Product transition page display number
     * <p>RKO</p>
     */
    @Test
    public void productTransitionPageDisplayNumber() {
        log.info("Test is started");

        String tranzPageValue = randomNumberFromRange(0, 127);
        log.info("set tranzPageValue -> " + tranzPageValue);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setTranzitPagePercentage(tranzPageValue);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check tranzPageValue");
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        softAssert.assertEquals(pagesInit.getCabGoodsSettings().getTranzitPagePercentage(), tranzPageValue);
        softAssert.assertNotEquals(updatedTime, operationMySql.getGblocks().getUpdatedTime(goodsId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getGblocks().getUseTranzPage(goodsId), tranzPageValue, "FAIL -> use_tranz_page");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Product transition page display number: validation
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataTranzPage")
    public void productTransitionPageDisplayNumber_validation(String tranzPageValue) {
        log.info("Test is started");
        log.info("set tranzPageValue -> " + tranzPageValue);
        authCabAndGo(goodWidgetSettingUrl + goodsId);
        pagesInit.getCabGoodsSettings().setTranzitPagePercentage(tranzPageValue);
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        log.info("check tranzPageValue");
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Product transition page display number"));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataTranzPage() {
        return new Object[][]{
                {" "},
                {"-1"},
                {"as^^"}
        };
    }
}
