package cab.publishers.widgets.exchangeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.exchangeWidgetSettingsUrl;

public class ExchangeSettingsTests extends TestBase {

    private final int tickersId = 14;

    @BeforeMethod
    public void getUpdatedTime() {
        updatedTime = operationMySql.getTickers().getUpdatedTime(tickersId);
    }

    @Story("Tickers block settings")
    @Description("Add filter to the widget setting for original titles <a href='https://jira.mgid.com/browse/TA-51942'>TA-51942</a>")
    @Test
    public void originalTitlesOnly() {
        log.info("Test is started");
        log.info("switch on");
        authCabAndGo(exchangeWidgetSettingsUrl + tickersId);
        pagesInit.getCabExchangeSettings().switchOriginalTitlesOnlyCheckbox(true);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        authCabAndGo(exchangeWidgetSettingsUrl + tickersId);
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkOriginalTitlesOnlyCheckbox(true), "FAIL -> is_original_title switch_on in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickers().getUpdatedTime(tickersId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickers().getIsOriginalTitle(tickersId), 1, "FAIL -> is_original_title switch_on in DB");

        log.info("switch off");
        pagesInit.getCabExchangeSettings().switchOriginalTitlesOnlyCheckbox(false);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        authCabAndGo(exchangeWidgetSettingsUrl + tickersId);
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkOriginalTitlesOnlyCheckbox(false), "FAIL -> is_original_title switch_off in UI");
        softAssert.assertEquals(operationMySql.getTickers().getIsOriginalTitle(tickersId), 0, "FAIL -> is_original_title switch_on in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
