package cab.publishers.widgets.exchangeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;

import static testData.project.EndPoints.*;

public class CategoryFilterTests extends TestBase {

    private ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>();
    private final int exchangeId = 7001;

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Adding random categories for exchange category filter without ad/l types")
    @Test
    public void publisherCategoryOnlyFilterAddingCategoryWithoutAdAndLandingTypes() {
        log.info("Test is started");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        pagesInit.getCabExchangeSettings().clearAllSelectedFilters(pagesInit.getCabExchangeSettings().getAllSelectedCheckboxesInMultiFilter("categoriesBox"));
        chooseCategoryInEditInterface.clear();
        chooseCategoryInEditInterface = helpersInit.getMultiFilterHelper().chooseCustomValueInMultiFilterAndGetTheirData(4);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),"FAIL -> Success message isn't displayed");

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkCategoriesFilterTypeValue("1"), "FAIL -> Category filter is switched off");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> Categories aren't equal");
        softAssert.assertEquals(operationMySql.getTickers().getUseCat(exchangeId),"1", "FAIL -> Use-cat = 0");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Adding random categories for exchange category with ad/l types")
    @Test
    public void publisherCategoryOnlyFilterAddingCategoryWithAdAndLandingTypes() {
        log.info("Test is started");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        pagesInit.getCabExchangeSettings().clearAllSelectedFilters(pagesInit.getCabExchangeSettings().getAllSelectedCheckboxesInMultiFilter("categoriesBox"));
        JSONArray chooseCategoryInEditInterface = pagesInit.getCabExchangeSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(5);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),"FAIL -> Success message isn't displayed");

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().checkAdTypesAndLandingTypesForCategory(chooseCategoryInEditInterface),"FAIL -> Wrong amount of Ad and Landing types was saved");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Adding additional categories for exchange category with active category filter")
    @Test
    public void publisherCategoryOnlyFilterAddAdditionalCategoriesForWidgetWithActiveCategoryFilter() {
        log.info("Test is started");
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>(Arrays.asList(
                "Business and Finance",
                "Careers",
                "Content Production",
                "Crime",
                "Education"
        ));

        log.info("Setting precondition");
        operationMySql.getTickersCategories().deleteAllCategories(exchangeId);
        operationMySql.getTickersCategories().insertCategories(exchangeId,33,72,115);
        operationMySql.getTickers().updateUseCat(exchangeId,1);

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Crime");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),"FAIL -> Success message isn't displayed");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        softAssert.assertTrue(helpersInit.getMultiFilterHelper().getNameAllSelectedValueInMultiFilter("categoriesBox").containsAll(chooseCategoryInEditInterface), "FAIL -> Categories aren't equal");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Turn off category filter for active exchange category without ad and landing types")
    @Test
    public void publisherCategoryOnlyFilterTurnOffWithActiveCategoryFilterWithoutAdAndLandingTypes() {
        log.info("Test is started");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        pagesInit.getCabExchangeSettings().clearAllSelectedFilters(pagesInit.getCabExchangeSettings().getAllSelectedCheckboxesInMultiFilter("categoriesBox"));
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Crime");
        pagesInit.getCabExchangeSettings().saveWidgetSettings();

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("0");
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),"FAIL -> Success message isn't displayed");
        softAssert.assertEquals(operationMySql.getTickers().getUseCat(exchangeId),"0", "FAIL -> Use-cat = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Turn off category filter for active exchange category with ad and landing types")
    @Test
    public void publisherCategoryOnlyFilterTurnOffWithActiveCategoryFilterWithAdAndLandingTypes() {
        log.info("Test is started");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("1");
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        pagesInit.getCabExchangeSettings().clearAllSelectedFilters(pagesInit.getCabExchangeSettings().getAllSelectedCheckboxesInMultiFilter("categoriesBox"));
        pagesInit.getCabExchangeSettings().chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(5);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("0");
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(),"FAIL -> Widget has been saved successfully isn't displayed");
        softAssert.assertEquals(operationMySql.getTickers().getUseCat(exchangeId),"0", "FAIL -> Use-cat = 1");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Adding random categories with AD type without Landing type for exchange category")
    @Test
    public void publisherCategoryOnlyFilterAddingCategoryWithAdTypeAndWithoutLandingTypes() {
        log.info("Test is started");

        log.info("Setting precondition");
        operationMySql.getTickersCategories().deleteAllCategories(exchangeId);
        operationMySql.getTickers().updateUseCat(exchangeId,1);

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Crime");
        pagesInit.getCabExchangeSettings().clickAdTypeOrLandingTypeForCategory("Crime",true);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Please set one of landing types"));
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Adding random categories with Landing type without Ad type for exchange category")
    @Test
    public void publisherCategoryOnlyFilterAddingCategoryWithLandingTypeAndWithoutAdTypes() {
        log.info("Test is started");

        log.info("Setting precondition");
        operationMySql.getTickersCategories().deleteAllCategories(exchangeId);
        operationMySql.getTickers().updateUseCat(exchangeId,1);

        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter("Education");
        pagesInit.getCabExchangeSettings().clickAdTypeOrLandingTypeForCategory("Education",false);
        pagesInit.getCabExchangeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Please set one of ad types"));
        log.info("Test is finished");
    }

    @Feature("Publisher category")
    @Story("Add ad types and landing types to exchange category filter")
    @Description("Checking search field")
    @Test
    public void publisherCheckSearchCategoryInFilter() {
        log.info("Test is started");
        authCabAndGo(exchangeWidgetSettingsUrl + exchangeId);
        pagesInit.getCabExchangeSettings().chooseCategoriesFilterType("1");
        Assert.assertTrue(pagesInit.getCabExchangeSettings().checkSearchCategoryFilter());
        log.info("Test is finished");
    }
}
