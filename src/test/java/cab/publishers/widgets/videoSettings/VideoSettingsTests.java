package cab.publishers.widgets.videoSettings;

import org.testng.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import testBase.TestBase;

public class VideoSettingsTests extends TestBase {

    /**
     * Проверка фильтра по источникам в настройках товарной части информера
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21872">TA-21872</a>
     */
    @Test(dataProvider = "partnersFiltersCases")
    public void workPublishersFilter(int blockId, String filterType, int campaignId) {
        log.info("Test is started");
        authCabAndGo("wages/informers-manage-video/id/" + blockId);
        pagesInit.getCabInformersManageVideo().expandVideoConfig();
        pagesInit.getCabGoodsSettings()
                .choosePublishersFilterType(filterType, campaignId);
        helpersInit.getMessageHelper().isSuccessMessagesCab();

        authCabAndGo("wages/informers-manage-video/id/" + blockId);
        Assert.assertTrue(pagesInit.getCabGoodsSettings().checkPublishersFilter(filterType, campaignId));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] partnersFiltersCases() {
        return new Object[][]{
                {1, "1", 7},
                {1, "2", 8},
                {1, "0", 8}
        };
    }

    /**
     * Check Video groups in Cab
     * <ul>
     *     <li>Check Video Groups States Name</li>
     *     <li>Save Video-widget with enabled Video Groups</li>
     *     <li>Check enabled Video groups after saving</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-24376">Ticket TA-24376</a>
     * <p>Author AIA</p>
     */
    @Test
    public void checkVideoGroupsInCab() {
        log.info("Test is started");
        authCabAndGo("wages/informers-manage-video/id/" + 2001);
        softAssert.assertEquals(new String[]{"Not set", "Include", "Exclude"},
                pagesInit.getCabInformersManageVideo().getVideoGroupStates(),
                "FAIL! -> Video group states are incorrect!");
        pagesInit.getCabInformersManageVideo().selectVideoGroups(5);
        authCabAndGo("wages/informers-manage-video/id/" + 2001);
        softAssert.assertTrue(pagesInit.getCabInformersManageVideo().checkVideoGroups(),
                "FAIL -> Video groups are not saved!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
