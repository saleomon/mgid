package cab.publishers.widgets.compositeSettings;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;

import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

/*
 * - editWidget_VALID_WithoutSourceId
 * - editWidget_WithSourceId
 * - editWidget_AddNewSourceId
 * - editWidget_VALID_WithCreateNewSourceId
 * - editWidget_changePushProviderOnSourcesId
 * - editWidget_changeSourcesIdOnPushProvider
 * - editWidget_changeCategoryPlatform
 * - editWidget_searchFunctionForSourcesId_haveNotValue
 * - editWidget_searchFunctionForSourcesId_isHaveValue
 */
public class SourcesIdTests extends TestBase {

    /**
     * 'Domain, that will be displayed to advertisers' - Поле обязательное для заполнения
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_VALID_WithoutSourceId() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 65);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Domain, that will be displayed to advertisers: The entered domain for advertisers is not valid"), "FAIL -> message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проставление виджету sources_id из таблицы sources
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_WithSourceId() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 66);
        pagesInit.getCabCompositeSettings().chooseSourceId();
        pagesInit.getCabCompositeSettings().getLowVolumeDomain();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDomainForAdvertiser(
                operationMySql.getSources().getSourcesName(
                        pagesInit.getCabCompositeSettings().getSourcesId())),
                "FAIL -> ui: Domain for advertisers:"
        );

        softAssert.assertTrue(pagesInit.getCabWidgetsList().isDisplayedDomainForAdvertiserIcon(
                pagesInit.getCabCompositeSettings().isLowVolumeDomain()),
                "FAIL -> ui: Domain for advertisers: ICON"
        );

        softAssert.assertEquals(operationMySql.getGblocks().getSourcesId(60),
                pagesInit.getCabCompositeSettings().getSourcesId(),
                "FAIL -> db: g_blocks.source_id");

        softAssert.assertEquals(
                operationMySql.getSources().getSourcesType(pagesInit.getCabCompositeSettings().getSourcesId()).equals("domain-low-volume"),
                pagesInit.getCabCompositeSettings().isLowVolumeDomain(),
                "FAIL -> db: sources.type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создание нового sources_id и проставление виджету
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_AddNewSourceId() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 66);
        pagesInit.getCabCompositeSettings().setSourcesId("-1").chooseSourceId();
        pagesInit.getCabCompositeSettings().addNewDomainForAdvertisers();
        pagesInit.getCabCompositeSettings().getLowVolumeDomain();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDomainForAdvertiser(
                operationMySql.getSources().getSourcesName(
                        pagesInit.getCabCompositeSettings().getSourcesId())),
                "FAIL -> ui: Domain for advertisers:"
        );

        softAssert.assertTrue(pagesInit.getCabWidgetsList().isDisplayedDomainForAdvertiserIcon(
                pagesInit.getCabCompositeSettings().isLowVolumeDomain()),
                "FAIL -> ui: Domain for advertisers: ICON"
        );

        softAssert.assertEquals(
                operationMySql.getSources().getSourcesType(pagesInit.getCabCompositeSettings().getSourcesId()).equals("domain-low-volume"),
                pagesInit.getCabCompositeSettings().isLowVolumeDomain(),
                "FAIL -> db: sources.type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Создание нового sources_id с невалидными значениями
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataDomain")
    public void editWidget_VALID_WithCreateNewSourceId(String value, String errorMessage) {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 66);
        pagesInit.getCabCompositeSettings()
                .setSourcesIdName(value)
                .setSourcesId("-1")
                .chooseSourceId();
        pagesInit.getCabCompositeSettings().addNewDomainForAdvertisers();
        pagesInit.getCabCompositeSettings().getLowVolumeDomain();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage), "FAIL -> message");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDomain() {
        return new Object[][]{
                {"", "The entered domain for advertisers is not valid"},
                {"sourceDomain_1", "Source already exists, please enter another name"},
                {"sourceDomain_2", "Source already exists, please enter another name"}
        };
    }

    /**
     * Изменяем виджету значение с PushProvider на SourcesId
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_changePushProviderOnSourcesId() {
        log.info("Test is started");
        operationMySql.getGblocks().updatePushProvidersId(130, 149, 61);
        operationMySql.getTickersComposite().updateCategoryPlatform(149, 67);

        authCabAndGo("wages/informers-edit/type/composite/id/" + 67);
        pagesInit.getCabCompositeSettings().setCategoryPlatform("108").chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().chooseSourceId();
        pagesInit.getCabCompositeSettings().getLowVolumeDomain();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDomainForAdvertiser(
                operationMySql.getSources().getSourcesName(
                        pagesInit.getCabCompositeSettings().getSourcesId())),
                "FAIL -> ui: Domain for advertisers:"
        );

        softAssert.assertTrue(pagesInit.getCabWidgetsList().isDisplayedDomainForAdvertiserIcon(
                pagesInit.getCabCompositeSettings().isLowVolumeDomain()),
                "FAIL -> ui: Domain for advertisers: ICON"
        );

        softAssert.assertEquals(operationMySql.getGblocks().getSourcesId(61),
                pagesInit.getCabCompositeSettings().getSourcesId(),
                "FAIL -> db: g_blocks.source_id");

        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(67),
                null,
                "FAIL -> db: g_blocks.push_providers_id");

        softAssert.assertEquals(
                operationMySql.getSources().getSourcesType(pagesInit.getCabCompositeSettings().getSourcesId()).equals("domain-low-volume"),
                pagesInit.getCabCompositeSettings().isLowVolumeDomain(),
                "FAIL -> db: sources.type");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Изменяем виджету значение с SourcesId на PushProvider
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_changeSourcesIdOnPushProvider() {
        log.info("Test is started");
        operationMySql.getGblocks().updateSourcesId(8820, 108, 62);
        operationMySql.getTickersComposite().updateCategoryPlatform(108, 68);

        authCabAndGo("wages/informers-edit/type/composite/id/" + 68);
        pagesInit.getCabCompositeSettings().setCategoryPlatform("150").chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        softAssert.assertEquals(operationMySql.getGblocks().getSourcesId(62),
                null,
                "FAIL -> db: g_blocks.source_id");

        softAssert.assertEquals(operationMySql.getGblocks().getPushProviderId(68),
                pagesInit.getCabCompositeSettings().getPushProvider(),
                "FAIL -> db: g_blocks.push_providers_id");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * проверка работы права wages/can_set_g_blocks_source
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_set_g_blocks_source")
    @Test
    public void editWidget_changeCategoryPlatform() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_set_g_blocks_source");

        authCabForCheckPrivileges("wages/informers-edit/type/composite/id/" + 108);
        pagesInit.getCabCompositeSettings().chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabForCheckPrivileges("wages/informers-edit/type/composite/id/" + 108);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkCategoryPlatform());
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_set_g_blocks_source");
        log.info("Test is finished");
    }

    /**
     * Проверка поля поиск(sources_id) - значения есть в таблице sources_id
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-26410">VT-26410</a>
     * <p>RKO</p>
     */
    @Test
    public void editWidget_searchFunctionForSourcesId_isHaveValue() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 39);
        pagesInit.getCabCompositeSettings().setSearchSourcesIdValue("_4");
        Assert.assertEquals(pagesInit.getCabCompositeSettings().getValueFromSourcesIdSearch(),
                new ArrayList<>(Arrays.asList("sourceDomain_4", "vrx_4")));

        log.info("Test is finished");
    }
}
