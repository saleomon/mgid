package cab.publishers.widgets.compositeSettings;

import org.testng.annotations.DataProvider;
import org.testng.annotations.*;
import testBase.TestBase;
import testData.project.Subnets.*;
import testData.project.publishers.WidgetTypes;

public class EditCodeSettingsTests extends TestBase {

    private final String manuallyStylesChangeErrorValue = "The widget settings cannot be changed because manual adjustments made";

    /**
     * Не блокировать редактирование всего шаблона при custom_styles = 1
     * Проверка что при custom_styles = 1 - блокируются не все поля, что они доступны для редактирования
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21951">VT-21951</a>
     */
    @Test(dataProvider = "dataForChangeCustomStylesForVariousWidgets")
    public void changeCustomStylesForVariousWidgets(WidgetTypes.Types widgetType, WidgetTypes.SubTypes subType, boolean isManuallyStale, int widgetId, SubnetType subnetId) {
        log.info("Test is started - " + subnetId + ": " + widgetType);
        this.subnetId = subnetId;

        if(isManuallyStale) {
            authDashAndGo("publisher/edit-widget/id/" + widgetId);
            pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(widgetType, subType);
            pagesInit.getWidgetClass().saveWidgetAndGetId();
        }

        authCabAndGo("wages/informers-settings/id/" + widgetId);
        pagesInit.getCabEditCode().manuallyStyleSwitchOn(isManuallyStale);
        softAssert.assertTrue(pagesInit.getCabEditCode().saveEditCodeInterface(), "FAIL -> switchManuallyStyleInEditCodeInterface");

        authDashAndGo("publisher/edit-widget/id/" + widgetId);

        softAssert.assertTrue(pagesInit.getWidgetClass().checkManuallyStyles(manuallyStylesChangeErrorValue, widgetType, isManuallyStale), "FAIL -> checkManuallyStyles");
        softAssert.assertAll();

        log.info("Test is finished - " + subnetId + ": " + widgetType);
    }

    @DataProvider
    public Object[][] dataForChangeCustomStylesForVariousWidgets() {
        return new Object[][]{
                {WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT, true,  33,  SubnetType.SCENARIO_ADSKEEPER},
                {WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_LIGHT, false, 33,  SubnetType.SCENARIO_ADSKEEPER},
                {WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS, true,  31,       SubnetType.SCENARIO_MGID},
                {WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS, false, 31,       SubnetType.SCENARIO_MGID},
                {  WidgetTypes.Types.EXIT_POP_UP,                WidgetTypes.SubTypes.NONE, true,  34,  SubnetType.SCENARIO_ADSKEEPER},
                {  WidgetTypes.Types.EXIT_POP_UP,                WidgetTypes.SubTypes.NONE, false, 34,  SubnetType.SCENARIO_ADSKEEPER},
                {       WidgetTypes.Types.MOBILE,                WidgetTypes.SubTypes.NONE, true,  32,       SubnetType.SCENARIO_MGID},
                {       WidgetTypes.Types.MOBILE,                WidgetTypes.SubTypes.NONE, false, 32,       SubnetType.SCENARIO_MGID},
                {       WidgetTypes.Types.MOBILE,                WidgetTypes.SubTypes.NONE, true,  35,  SubnetType.SCENARIO_ADSKEEPER},
                {       WidgetTypes.Types.MOBILE,                WidgetTypes.SubTypes.NONE, false, 35,  SubnetType.SCENARIO_ADSKEEPER}
        };
    }
}
