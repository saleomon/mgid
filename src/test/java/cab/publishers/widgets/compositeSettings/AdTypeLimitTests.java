package cab.publishers.widgets.compositeSettings;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.testng.*;
import org.testng.annotations.*;
import testBase.TestBase;


public class AdTypeLimitTests extends TestBase {

    private final int tickerCompositeId = 29;

    // 1 - type(pg/nc17/pg13)
    // 2 - isSelected
    // 3 - Limit amount of teasers for selected type.
    Table<String, Boolean, String> adTypesData = HashBasedTable.create();


    /**
     * Добавить возможность выставлять лимит для ad_types
     * Ввод корретных значений лимитов в текстовое поле
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAddTypesLimitCorrectValues() {
        log.info("Test is started");
        adTypesData.put("pg",   true,   "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Ввод нулевого значения
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAddTypesLimitZeroValue() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true,   "0");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true,   "0");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Снятие чекбокса для типов с установлеными лимитами
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAdTypesLimitNsfwFalse() {
        log.info("Test is started");
        adTypesData.put("pg",   true,     "-");
        adTypesData.put("pg13", false,    "-");
        adTypesData.put("r",    true,   "255");
        adTypesData.put("nc17", true,     "5");
        adTypesData.put("nsfw", false,    "2");
        adTypesData.put("b",    false,    "0");
        adTypesData.put("x",    false,    "0");

        setValidAdTypes();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Проверка отсутствия сообщения "Слишком узкий лимит для выбранного типа тизеров. Выберите тип PG или снимите лимит у одного из R, NC17, NSFW"
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAddTypesLimitGetMessageForPgSuccess() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setValidAdTypes();
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Ввод невалидных значений
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAddTypesLimitNotValidValue() {
        log.info("Test is started");
        adTypesData.put("pg",   true,     "-");
        adTypesData.put("pg13", false,    "-");
        adTypesData.put("r",    true,   "-2");
        adTypesData.put("nc17", true,     "0.2");
        adTypesData.put("nsfw", true,    "asda%#$@#^%$#^%");
        adTypesData.put("b",    false,    "0");
        adTypesData.put("x",    false,    "0");

        setNotValidAdTypes("The value can not be negative");
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Проверка вывода сообщения "Слишком узкий лимит для выбранного типа тизеров. Выберете тип PG или снимите лимит у одного из R, NC17, NSFW"
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAddTypesLimitGetMessageForPg() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "5");
        adTypesData.put("nsfw", true,   "2");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setNotValidAdTypes("Very low limit for selected ad types. Select PG or remove limit from other ad types (R, NC17, NSFW).");
        log.info("Test is finished");
    }

    /**
     * Добавить возможность выставлять лимит для ad_types
     * Проверка появления сообщения "The value can not be greater than 255"
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/VT-21991">VT-21991</a>
     */
    @Test
    public void checkAdTypesLimitNotValidMaxValue() {
        log.info("Test is started");
        adTypesData.put("pg",   false,  "-");
        adTypesData.put("pg13", false,  "-");
        adTypesData.put("r",    true, "255");
        adTypesData.put("nc17", true,   "0");
        adTypesData.put("nsfw", true, "256");
        adTypesData.put("b",    false,  "0");
        adTypesData.put("x",    false,  "0");

        setNotValidAdTypes("The value can not be greater than 255");
        log.info("Test is finished");
    }

    private void setValidAdTypes(){
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().setAdTypes(adTypesData);
        softAssert.assertTrue( helpersInit.getMessageHelper().isSuccessMessagesCab());
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdType(adTypesData));
        softAssert.assertAll();
        adTypesData.clear();
    }

    private void setNotValidAdTypes(String message){
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().setAdTypes(adTypesData);
        Assert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab(message), "FAIL -> custom message");
        adTypesData.clear();
    }
}
