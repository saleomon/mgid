package cab.publishers.widgets.compositeSettings;

import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;

import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class SubSourceTests extends TestBase {

    /**
     * Проверка валидации на пустые значения контейнера подисточников
     *
     * @see <a href="https://youtrack.mgid.com/issue/KOT-8">Ticket KOT-8</a>
     * <p>NIO</p>
     */
    @Test
    public void checkValidationEmptySubSourceForm() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 210);
        pagesInit.getCabCompositeSettings().changeStateSubSourceCheckbox(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Empty Publisher Source"));
        log.info("Test is finished");
    }

    /**
     * Добавление, Редактирование, Выключение опции по фильтру подисточников
     *
     * @see <a href="https://youtrack.mgid.com/issue/KOT-8">Ticket KOT-8</a>
     * <p>NIO</p>
     */
    @Test
    public void checkAddEditDeleteSubSource() {
        log.info("Test is started");
        ArrayList<String> listOfSubSource = new ArrayList<>(Arrays.asList("123", "2", "423545"));
        String widgetId = "1003";

        log.info("Add sources and check settings after saving");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().changeStateSubSourceCheckbox(true);
        pagesInit.getCabCompositeSettings().insertSubSource(listOfSubSource);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Widget has been saved successfully"), "FAIL - add sub source");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSavedSubSources(listOfSubSource), "FAIL - check saved sub source after adding");

        log.info("Edit sources and check settings after saving");
        pagesInit.getCabCompositeSettings().deleteFirstElement();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().checkSavedSubSources(listOfSubSource), "FAIL - check saved sub source after editing");

        log.info("Disable source filter and check it after settings");
        pagesInit.getCabCompositeSettings().changeStateSubSourceCheckbox(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().checkIsCheckboxSubSourceFilterSelected(), "FAIL - check state of checkbox");

        log.info("Check source container for self cleaning");
        pagesInit.getCabCompositeSettings().changeStateSubSourceCheckbox(false);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().checkIsEmptySubSourceContainer(), "FAIL - check that container is clear");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Проверка права для опции фильтра по площадкам
     *
     * @see <a href="https://youtrack.mgid.com/issue/KOT-8">Ticket KOT-8</a>
     * <p>NIO</p>
     */
    @Privilege(name = "can_edit_widget_source_filters")
    @Test
    public void checkPrivilegeSubSourceForWidget() {
        try {
            log.info("Test is started");
            String widgetId = "1003";
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_edit_widget_source_filters");
            authCabForCheckPrivileges("wages/informers-edit/type/composite/id/" + widgetId);
            Assert.assertFalse(pagesInit.getCabCompositeSettings().checkDisplayingSubSourceOption(), "FAIL - check displaying sub source");
            log.info("Test is finished");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_edit_widget_source_filters");
        }
    }
}
