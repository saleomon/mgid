package cab.publishers.widgets.compositeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.EndPoints;

import java.util.Arrays;

import static core.service.constantTemplates.ConstantsInit.RKO;

public class BackButtonSettingsTests extends TestBase {

    private final String backgroundColor = "#76923c",
            textColor = "#5f497a",
            bannerText = "[WIDGET_TITLE_SPONSORED_CONTENT]",
            header = "edit-button",
            smartWidgetId = "540";

    private final String backgroundColor2 = "#76923c",
            textColor2 = "#5f497a",
            bannerText2 = "[WIDGET_TITLE_SPONSORED_CONTENT]",
            header2 = "edit-button",
            trafficSource = "test-source.com",
            smartWidgetId2 = "541";

    private String jsonWithTrafficType, jsonWithTrafficSource;

    private final String backButtonJson = "{" +
                                            "\"display\": " +
                                                "[" +
                                                    "{" +
                                                     "\"type\": \"banner\", " +
                                                     "\"options\": " +
                                                        "{" +
                                                            "\"text\": \"%s\", " +
                                                            "\"bg_color\": \"%s\", " +
                                                            "\"text_color\": \"%s\"" +
                                                        "}" +
                                                    "}, " +
                                                    "{" +
                                                    "\"type\": \"header\", " +
                                                    "\"options\": " +
                                                        "{" +
                                                            "\"selector\": \"%s\"" +
                                                        "}" +
                                                    "}" +
                                                "], " +
                                            "\"filters\": " +
                                                "[" +
                                                    "{" +
                                                        "\"type\": \"%s\", " +
                                                        "\"value\": " +
                                                            "[%s]" +
                                                    "}, " +
                                                    "{" +
                                                        "\"type\": \"device\", " +
                                                        "\"value\": " +
                                                            "[%s]" +
                                                    "}" +
                                                "], " +
                                            "\"widget_id\": %s" +
                                            "}";

    @BeforeClass
    public void prepareJson(){
        jsonWithTrafficType = String.format(backButtonJson, bannerText, backgroundColor, textColor,
                                                            header,
                                                            "traffic_type", "\"Direct\", \"Organic\"",
                                                            "\"tablet\", \"mobile\"",
                                                            smartWidgetId);

        jsonWithTrafficSource = String.format(backButtonJson, bannerText2, backgroundColor2, textColor2,
                                                            header2,
                                                            "traffic_source", "\"test-source.com\"",
                                                            "\"desktop\", \"mobile\"",
                                                            smartWidgetId2);
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("set back button options and check them in db and ui <a href='https://jira.mgid.com/browse/TA-52305'>TA-52305</a>")
    @Owner(RKO)
    @Test(description = "set back button options and check them in db and ui")
    public void setBackButtonOptionsAndCheckThem() {
        log.info("Test is started");
        int widgetId = 539;

        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchStateBackButtonSettings(true);
        pagesInit.getCabCompositeSettings().fillBackButtonWidgetId(smartWidgetId);
        pagesInit.getCabCompositeSettings().switchStateBackButtonTrafficType(true, Arrays.asList("Direct", "Organic"));
        pagesInit.getCabCompositeSettings().switchStateBackButtonDevice(true, Arrays.asList("tablet", "mobile"));
        pagesInit.getCabCompositeSettings().switchStateBackButtonBanner(true, backgroundColor, textColor, bannerText);
        pagesInit.getCabCompositeSettings().switchStateBackButtonHeader(true, header);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success messages");
        softAssert.assertEquals(operationMySql.getTickersComposite().getBackButtonOptions(widgetId), jsonWithTrafficType, "FAIL -> back_button_options from db");

        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonCheckbox(true), "FAIL -> checkBackButtonCheckbox");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonWidgetId(smartWidgetId), "FAIL -> checkBackButtonWidgetId");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonTrafficType(true), "FAIL -> checkBackButtonTrafficType");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonDevices(true), "FAIL -> checkBackButtonDevices");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonBanner(true, backgroundColor, textColor, bannerText), "FAIL -> checkBackButtonBanner");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonHeader(true, header), "FAIL -> checkBackButtonHeader");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("edit back button options and check them in db and ui" +
            "widget had traffic type settings in db we edit him and set traffic source settings, " +
            "we checked after save: traffic type: false; traffic source: true;" +
            "this settings can't be enable both" +
            " <a href='https://jira.mgid.com/browse/TA-52305'>TA-52305</a>")
    @Owner(RKO)
    @Test(description = "edit back button options and check them in db and ui")
    public void editBackButtonOptionsAndCheckThem() {
        log.info("Test is started");
        int widgetId = 540;

        operationMySql.getTickersComposite().updateBackButtonOptions(widgetId, jsonWithTrafficType);
        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().fillBackButtonWidgetId(smartWidgetId2);
        pagesInit.getCabCompositeSettings().switchStateBackButtonTrafficSource(true, trafficSource);
        pagesInit.getCabCompositeSettings().switchStateBackButtonDevice(true, Arrays.asList("desktop", "mobile"));
        pagesInit.getCabCompositeSettings().switchStateBackButtonBanner(true, backgroundColor2, textColor2, bannerText2);
        pagesInit.getCabCompositeSettings().switchStateBackButtonHeader(true, header2);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success messages");
        softAssert.assertEquals(operationMySql.getTickersComposite().getBackButtonOptions(widgetId), jsonWithTrafficSource, "FAIL -> back_button_options from db");

        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonCheckbox(true), "FAIL -> checkBackButtonCheckbox");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonWidgetId(smartWidgetId2), "FAIL -> checkBackButtonWidgetId");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonTrafficType(false), "FAIL -> checkBackButtonTrafficType");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonTrafficSource(true, trafficSource), "FAIL -> checkBackButtonTrafficSource");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonDevices(true), "FAIL -> checkBackButtonDevices");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonBanner(true, backgroundColor2, textColor2, bannerText2), "FAIL -> checkBackButtonBanner");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonHeader(true, header2), "FAIL -> checkBackButtonHeader");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("turn off back button options and check them in db and ui <a href='https://jira.mgid.com/browse/TA-52305'>TA-52305</a>")
    @Owner(RKO)
    @Test(description = "turn off back button options and check them in db and ui")
    public void turnOffBackButtonOptionsAndCheckThem() {
        log.info("Test is started");
        int widgetId = 541;
        operationMySql.getTickersComposite().updateBackButtonOptions(widgetId, jsonWithTrafficType);

        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchStateBackButtonSettings(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success messages");
        softAssert.assertEquals(operationMySql.getTickersComposite().getBackButtonOptions(widgetId), null, "FAIL -> back_button_options from db");

        authCabAndGo(EndPoints.compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkBackButtonCheckbox(false), "FAIL -> checkBackButtonCheckbox");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("try to set widget id from another site id to 'Add Smart Widget ID' and get error <a href='https://jira.mgid.com/browse/TA-52305'>TA-52305</a>")
    @Owner(RKO)
    @Test(description = "set widget id from another site")
    public void setWidgetIdFromAnotherSite() {
        log.info("Test is started");
        authCabAndGo(EndPoints.compositeEditUrl + 1);
        pagesInit.getCabCompositeSettings().switchStateBackButtonSettings(true);
        pagesInit.getCabCompositeSettings().fillBackButtonWidgetId("540");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Add Smart Widget ID: Additional widget should be from the same site"));
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("try to save back button options with empty fields <a href='https://jira.mgid.com/browse/TA-52305'>TA-52305</a>")
    @Owner(RKO)
    @Test(description = "save back button options with empty fields")
    public void turnOnBackButtonWithEmptyFields() {
        log.info("Test is started");
        authCabAndGo(EndPoints.compositeEditUrl + 1);
        pagesInit.getCabCompositeSettings().switchStateBackButtonSettings(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Add Smart Widget ID: The value must be digital"));
        log.info("Test is finished");
    }
}
