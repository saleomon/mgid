package cab.publishers.widgets.compositeSettings;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.*;
import testBase.TestBase;

public class DoubleClickTests extends TestBase {

    private final int tickerCompositeId = 36;

    @BeforeMethod
    public void clearDoubleClickSettings() {
        operationMySql.getTickersComposite().clearDoubleClickSettings(tickerCompositeId);
    }

    /**
     * Проверка установки опций для даблклика по умолчанию и их смены на Обычный даблклик.
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21785">VT-21785</a>
     */
    @Test
    public void doubleClickDefault() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "6");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDesktopDoubleClickSettings("1", "4"), "FAIL -> check desktop doubleClick");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkMobileDoubleClickSettings("1", "6"), "FAIL -> check mobile doubleClick");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Попытка включения даблклика с невалидными значениями.
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21785">VT-21785</a>
     */
    @Test
    public void doubleClickNotValid() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 1);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDefaultSettingsForDoubleClick(), "FAIL -> default settings");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "-4; jfgj^(*!nn; 5.7");
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "sdas");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Doubleclick delay : Field error"), "FAIL -> error message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Попытка включения даблклика все поля пустые
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21785">VT-21785</a>
     */
    @Test
    public void doubleClickAllFieldsEmpty() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 1);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDefaultSettingsForDoubleClick(), "FAIL -> default settings");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "");
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Doubleclick delay : Field error"), "FAIL -> error message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

}
