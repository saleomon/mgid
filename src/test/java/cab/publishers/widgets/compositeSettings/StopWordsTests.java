package cab.publishers.widgets.compositeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StopWordsTests extends TestBase {

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("Добавлениевозможности копирования 'стоп-слов' в админке/настройках виджета " +
            "<ul>" +
            "<li>1. сетим стоп-слова 1-вому виджету(очищаем старые, если они есть)</li>"+
            "<li>2. копируем их</li>"+
            "<li>3. вставляем слова второму виджету(очищаем старые, если они есть)</li>"+
            "<li>4. сохраняем и проверяем что стоп-слова применились</li>" +
            "</ul>" +
            "<a href='https://jira.mgid.com/browse/VT-21881'>TA-22946</a>")
    @Test(description = "copy stop words(icon)")
    public void copyStopWords() {
        log.info("Test is started");

        log.info("очищаем стоп-слова у виджета(30) если они у него есть");
        operationMySql.getTickersComposite().clearStopWordsSettings(30);

        log.info("копируем стоп-слова 1-вого виджета");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 29);
        pagesInit.getCabCompositeSettings().clickCopyStopWords();

        log.info("вставляем слова второму виджету(очищаем старые, если они есть)");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 30);
        pagesInit.getCabCompositeSettings().changeStateStopWordsCheckbox(true);
        pagesInit.getCabCompositeSettings().pasteStopWords();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        helpersInit.getMessageHelper().isSuccessMessagesCab();

        log.info("проверяем что стоп-слова применились");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 30);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkSavedStopWords(new ArrayList<>(Arrays.asList("cat", "dog", "smile"))));
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("add stop words without value(only checkbox) " +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>" +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "add stop words without value")
    public void addStopWordsWithoutValue() {
        log.info("Test is started");
        int widgetId = 433;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().changeStateStopWordsCheckbox(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Word filter: You must specify \"stop words\" or turn off the filter"), "Saved empty stop word form 2");
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("add stop words e2e" +
            "Include check: " +
            "- add stop word 2 symbols " +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "add stop words e2e + check(add stop word 2 symbols)")
    public void addStopWords() {
        log.info("Test is started");
        int widgetId = 434;
        ArrayList<String> listOfStopWordsCab = new ArrayList<>(Arrays.asList("Fruit", "Rubber", "Ківі", "Їжак", "ґанокй", "Мʼята", "Різнотрав'я", "Єнот", "Ёлкаи"));

        log.info("Set stop words and check message after saving");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().changeStateStopWordsCheckbox(true);
        pagesInit.getCabCompositeSettings().setStopWords(new ArrayList<>(Arrays.asList("Fruit", "Rubber", "Ківі", "Їжак", "ґанокй", "Мʼята", "Різнотрав'я", "Єнот", "Ёлкаи", "hi")));
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Widget has been saved successfully"), "Check success message");

        log.info("Check saved settings of stop words after cab editing");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), listOfStopWordsCab, "Check options cab");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("edit stop words e2e " +
            "Include check: " +
            "- try to add the same word " +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "edit stop words e2e + check(try to add the same word)")
    public void editStopWords() {
        log.info("Test is started");
        int widgetId = 435;
        ArrayList<String> listOfStopWordsCab = new ArrayList<>(Arrays.asList("test", "fruit", "rest", "Fruit", "Rubber", "milk"));

        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().changeStateStopWordsCheckbox(true);
        pagesInit.getCabCompositeSettings().setStopWords(new ArrayList<>(Arrays.asList("Fruit", "Rubber", "milk", "test")));
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisplayedStopWordsError(), "FAIL -> error the same word");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Widget has been saved successfully"), "Check success message");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), listOfStopWordsCab, "Check options cab");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("Delete all stop words for 1 widget" +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "Delete all stop words for 1 widget")
    public void deleteAllStopWords() {
        log.info("Test is started");
        int widgetId = 436;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().deleteStopWords();
        pagesInit.getCabCompositeSettings().changeStateStopWordsCheckbox(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Widget has been saved successfully"), "Check success message");

        log.info("Check saved settings of stop words after cab editing");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkStateStopWordsCheckbox(false), "Check options cab");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    @Feature("Stop Words")
    @Story("Stop Words Cab")
    @Description("Widget have 3 words, delete 2 words and check 1 left" +
            "<a href='https://jira.mgid.com/browse/VT-21881'>VT-21881</a>, " +
            "<a href='https://jira.mgid.com/browse/VT-21812'>VT-21812</a>")
    @Test(description = "Delete some stop words for 1 widget and check 1 left")
    public void deleteSomeStopWords() {
        log.info("Test is started");
        int widgetId = 437;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().deleteStopWords(2);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Widget has been saved successfully"), "Check success message");

        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getStopWords(), new ArrayList<>(List.of("rest")), "Check options cab");
        softAssert.assertAll();

        log.info("Test is finished");
    }
}
