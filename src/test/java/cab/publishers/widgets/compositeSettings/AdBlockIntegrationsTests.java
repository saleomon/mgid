package cab.publishers.widgets.compositeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabCompositeSettings.AdBlockTemplates;
import testBase.TestBase;
import testData.project.Subnets;

import static libs.hikari.tableQueries.partners.TickersComposite.getCountNews;
import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.compositeEditUrl;
import static testData.project.EndPoints.widgetsCustomIdUrl;

public class AdBlockIntegrationsTests extends TestBase {

    private final int widgetForDeactivatedId = 452;
    private final int widgetIdealmediaIoForDeactivatedId = 453;
    private final int widgetForTurnOffAdBlockId = 454;
    private final int widgetIdealmediaIoForTurnOffAdBlockId = 455;
    private final int widgetForDeletedId = 495;
    private final int widgetIdealmediaIoForDeletedId = 496;

    @BeforeClass
    public void createChildAdBlock(){
        addAdBlockToWidget(widgetForDeactivatedId);
        addAdBlockToWidget(widgetIdealmediaIoForDeactivatedId);
        addAdBlockToWidget(widgetForTurnOffAdBlockId);
        addAdBlockToWidget(widgetIdealmediaIoForTurnOffAdBlockId);
        addAdBlockToWidget(widgetForDeletedId);
        addAdBlockToWidget(widgetIdealmediaIoForDeletedId);

        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);
    }

    @Feature("AdBlock")
    @Story("AdBlock Integrations")
    @Description("AdBlock Integrations + AdBlock template(check settings in UI and db)")
    @Owner(value="RKO")
    @Test(description = "AdBlock Integrations in cab e2e")
    public void adBlockIntegrations() {
        log.info("Test is started");
        int widgetId = 77;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        log.info("switch on AdBlock Integrations and AdBlock template");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(AdBlockTemplates.UNDER_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //run cron
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("check -> adblock_integration = true");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisabledAdBlockTemplates(), "FAIL -> AdBlock template is disabled");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdBlockIntegrations(true), "FAIL -> switch on checkAdBlockIntegrations: adblock_integration = true");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdBlockTemplates(AdBlockTemplates.UNDER_ARTICLE), "FAIL -> checkAdBlockTemplates (adblock_integration = true) ");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime (adblock_integration = true)");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAdblockIntegration(widgetId), "1", "FAIL -> adblock_integration 1 in DB");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAdblockTemplate(widgetId), "1", "FAIL -> adblock_template 1 in DB");

        //get child_tickers_composite_id
        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);
        softAssert.assertEquals(getCountNews(childId), 6, "FAIL -> count_news");
        softAssert.assertTrue(
                operationMySql.getTickersCompositePlacementConfiguration().getPlacementType(childId)
                        .stream().anyMatch(i -> i.equals("wages")),
                "FAIL -> wages placement type is absent");

        // check field 'AdBlock Integrations' is hidden
        authCabAndGo(compositeEditUrl + childId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> show adBlock integrations c-box");

        log.info("adblock_integration = false");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("check -> adblock_integration = false");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdBlockIntegrations(false), "FAIL -> switch on checkAdBlockIntegrations: adblock_integration = false");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime (adblock_integration = false)");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAdblockIntegration(widgetId), "0", "FAIL -> adblock_integration 0 in DB");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAdblockTemplate(widgetId), "1", "FAIL -> adblock_template 1 in DB (adblock_integration = false)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("AdBlock")
    @Story("AdBlock Integrations")
    @Description("AdBlock Integrations without choose template")
    @Owner(value="RKO")
    @Test(description = "AdBlock Integrations without choose template")
    public void adBlockIntegrationsWithNoneTemplate() {
        log.info("Test is started");
        int widgetId = 2;
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(AdBlockTemplates.NONE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("AdBlock template: Choose one of the adblock template"));
        log.info("Test is finished");
    }

    @Feature("adBlock")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Add Under-Article adBlock after delete him in dash and check that in cab <a href='https://jira.mgid.com/browse/TA-52137'>TA-52137</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataDeleteChildInDash", description = "Add Under-Article adBlock after delete him in dash and check that in cab")
    public void deleteChildInDash(Subnets.SubnetType subnetId, String email, int siteId, int parentId){
        this.subnetId = subnetId;
        authDashAndGo(email, "publisher/widgets/site/" + siteId);
        pagesInit.getWidgetClass().showWidgetChild(parentId);

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        pagesInit.getWidgetClass().setCloneId(childId);
        pagesInit.getWidgetClass().deleteClonedWidget();
        softAssert.assertTrue(pagesInit.getWidgetClass().checkDeletedClone(pagesInit.getWidgetClass().getCloneId()), "FAIL - delete cloned widget");

        authCabAndGo(widgetsCustomIdUrl + childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "red", "FAIL -> block/unblock icon");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity (action:false)");

        log.info("try to click icon 'Block/Unblock' child widget and check status icon");
        pagesInit.getCabWidgetsList().clickBlockUnblockWidgetWithoutWaiting(childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "red", "FAIL -> block/unblock icon after click");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity (action:false) after click");

        authCabAndGo(compositeEditUrl + childId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations(), "FAIL -> checkAdBlockIntegrations childId");
        log.info("check child settings in db");
        softAssert.assertEquals(operationMySql.getTickersComposite().getStatus(childId), "17", "FAIL -> child status");
        softAssert.assertEquals(operationMySql.getTickersComposite().getDroped(childId), "0", "FAIL -> child droped");
        softAssert.assertEquals(operationMySql.getTickersComposite().getEnabled(childId), "0", "FAIL -> child enabled");

        authCabAndGo(compositeEditUrl + parentId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAdBlockIntegrations(false), "FAIL -> checkAdBlockIntegrations parentId");
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockTemplates(), "FAIL -> isDisplayedAdBlockTemplates");
        log.info("check parent settings in db");
        softAssert.assertEquals(operationMySql.getTickersComposite().getStatus(parentId), "12", "FAIL -> parentId status");
        softAssert.assertEquals(operationMySql.getTickersComposite().getDroped(parentId), "0", "FAIL -> parentId droped");
        softAssert.assertEquals(operationMySql.getTickersComposite().getEnabled(parentId), "1", "FAIL -> parentId enabled");

        log.info("check adblock settings in db");
        softAssert.assertEquals(operationMySql.getTickersCompositeRelations().getDropedByParent(parentId), 1, "FAIL -> droped status");

        softAssert.assertAll();
    }

    @DataProvider
    public Object[][] dataDeleteChildInDash(){
        int siteId = 124;
        int siteIdealmediaIoId = 125;
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          "testEmail75@ex.ua", siteId, widgetForDeactivatedId},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, "testEmail76@ex.ua", siteIdealmediaIoId, widgetIdealmediaIoForDeactivatedId}
        };
    }

    @Feature("adBlock")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Add Under-Article adBlock after turn off adBlock in cab for parent and turn on again <a href='https://jira.mgid.com/browse/TA-52137'>TA-52137</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataTurnOffAdBlock", description = "Add Under-Article adBlock after turn off adBlock in cab for parent and turn on again")
    public void turnOffAdBlockAndTurnOnAgain(String val, int parentId){
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + parentId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        authCabAndGo(widgetsCustomIdUrl + childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "red", "FAIL -> block/unblock icon (action:false)");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity (action:false)");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("title", childId), "For disable or enable Adblock child widget please turn on/off the Adblock integration option in the settings of parent widget", "FAIL -> block/unblock title (action:false)");
        softAssert.assertEquals(operationMySql.getTickersCompositeRelations().getDropedByParent(parentId), 1, "FAIL -> droped status (action:false)");

        authCabAndGo(compositeEditUrl + parentId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().isDisabledAdBlockTemplates(), "FAIL -> AdBlock template is disabled");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo(widgetsCustomIdUrl + childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "green", "FAIL -> block/unblock icon(action:true)");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity(action:true)");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("title", childId), "For disable or enable Adblock child widget please turn on/off the Adblock integration option in the settings of parent widget", "FAIL -> block/unblock title (action:false)");
        softAssert.assertEquals(operationMySql.getTickersCompositeRelations().getDropedByParent(parentId), 0, "FAIL -> droped status(action:true)");

        log.info("try to click icon 'Block/Unblock' child widget and check status icon");
        pagesInit.getCabWidgetsList().clickBlockUnblockWidgetWithoutWaiting(childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "green", "FAIL -> block/unblock icon after click");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity (action:false) after click");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataTurnOffAdBlock(){
        return new Object[][]{
                {"a1", widgetForTurnOffAdBlockId},
                {"a2", widgetIdealmediaIoForTurnOffAdBlockId}
        };
    }

    @Feature("adBlock")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Turn on again AdBlock for widget who had deleted child <a href='https://jira.mgid.com/browse/TA-52137'>TA-52137</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataTurnOnAgainAdBlockForWidgetWhoHadDeletedChild", description = "Turn on again AdBlock for widget who had deleted child ")
    public void turnOnAgainAdBlockForWidgetWhoHadDeletedChild(String val, int parentId){
        log.info("Test is started");
        int deletedChildId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(parentId);
        authCabAndGo(compositeEditUrl + parentId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        operationMySql.getTickersComposite().updateDeletedWidget(deletedChildId);

        authCabAndGo(compositeEditUrl + parentId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisabledAdBlockTemplates(), "FAIL -> AdBlock template isn't disabled");
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(AdBlockTemplates.IN_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        int childId = operationMySql.getTickersCompositeRelations().getChildDontDropedTickersCompositeId(parentId);
        authCabAndGo(widgetsCustomIdUrl + childId);
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetColor(childId), "green", "FAIL -> block/unblock icon (action:true)");
        softAssert.assertEquals(pagesInit.getCabWidgetsList().getBlockUnblockWidgetAttr("style", childId), "display: inline; opacity: 0.5;", "FAIL -> block/unblock opacity (action:false)");
        softAssert.assertEquals(operationMySql.getTickersCompositeRelations().getDropedByChild(childId), 0, "FAIL -> droped status (action:true)");
        softAssert.assertEquals(operationMySql.getTickersComposite().getStatus(childId), "12", "FAIL -> childId status");
        softAssert.assertEquals(operationMySql.getTickersComposite().getDroped(childId), "0", "FAIL -> childId droped");
        softAssert.assertEquals(operationMySql.getTickersComposite().getEnabled(childId), "1", "FAIL -> childId enabled");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataTurnOnAgainAdBlockForWidgetWhoHadDeletedChild(){
        return new Object[][]{
                {"a1", widgetForDeletedId},
                {"a2", widgetIdealmediaIoForDeletedId}
        };
    }

    @Feature("adBlock")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Don't show adblock integration settings for Adskeeper <a href='https://jira.mgid.com/browse/TA-52137'>TA-52137</a>")
    @Owner(value="RKO")
    @Test(description = "Don't show adblock integration settings for Adskeeper")
    public void dontShowAdblockIntegrationForAdskeeper(){
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 7);
        Assert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations());
        log.info("Test is finished");
    }

    @Feature("adBlock")
    @Story("Disable adBlock integrations option if the adblock child is disabled")
    @Description("Don't show adblock integration settings for widgets without wages section <a href='https://jira.mgid.com/browse/TA-52137'>TA-52137</a>")
    @Owner(value="RKO")
    @Test(description = "Don't show adblock integration settings for widgets without wages section")
    public void dontShowAdblockIntegrationForWidgetsWithoutWages(){
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 12);
        Assert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedAdBlockIntegrations());
        log.info("Test is finished");
    }

    private void addAdBlockToWidget(int widgetId){
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(AdBlockTemplates.UNDER_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
    }
}
