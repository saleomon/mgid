package cab.publishers.widgets.compositeSettings;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * Load additional widgets on page without code integrations
 * @see <a href="https://jira.mgid.com/browse/TA-51791">TA-51791</a>
 * @see <a href="https://jira.mgid.com/browse/TA-51855">TA-51855</a>
 * <p>RKO</p>
 */
public class LoadAdditionalWidgetsTests extends TestBase {

    private final int widget_1        = 295;
    private final int widget_5        = 299;
    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    /**
     * end-to-and
     */
    @Test(dataProvider = "dataForLoadAdditionalWidgetsEndToEnd")
    public void loadAdditionalWidgetsEndToEnd(String testCase, int widget_1, int widget_2, String standName, String screenName) {
        log.info("Test is started: " + testCase);

        authCabAndGo("wages/informers-edit/type/composite/id/" + widget_1);
        pagesInit.getCabCompositeSettings().switchLoadAdditionalWidgets(true);
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsId(String.valueOf(widget_2));
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsSelector("#loadadd");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        authCabAndGo("wages/informers-edit/type/composite/id/" + widget_2);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/" + standName))
                .setWidgetIds(widget_1)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 2");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                        serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/" + screenName + ".png")),
               "FAIL -> check screen");
        softAssert.assertAll();
        log.info("Test is finished: " + testCase);
    }

    @DataProvider
    public Object[][] dataForLoadAdditionalWidgetsEndToEnd(){
        int widget_2 = 296;
        int widgetShadow_1 = 307;
        int widgetShadow_2 = 308;
        return new Object[][]{
                {"case: simple",        widget_1, widget_2,       "mgid_load_additional_widget",          "loadAdditionalWidgetsEndToEnd"},
                {"case: shadow DOM", widgetShadow_1, widgetShadow_2, "mgid_load_additional_widget_shadow",   "loadAdditionalWidgetsEndToEnd"}
        };
    }

    /**
     * Обеспечить валидацию css селектора: "латинские символы, пробел, цифры #.-[]" (пропускает)
     */
    @Test(dataProvider = "dataLoadAdditionalWidgetsCheckEmptyField")
    public void loadAdditionalWidgetsCheckEmptyField(String widgetId, String selector, String message) {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widget_1);
        pagesInit.getCabCompositeSettings().switchLoadAdditionalWidgets(true);
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsId(widgetId);
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsSelector(selector);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Additional widgets id: " + message), "FAIL -> message #1");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Additional widgets selector: Invalid characters entered"), "FAIL -> message #2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataLoadAdditionalWidgetsCheckEmptyField(){
        return new Object[][]{
                {"",    "",     "The value must be digital"},
                {"0",   "&&&",  "Specified widget doesn't exist"},
                {"-1",  "'''",  "The value must be digital"},
                {"%%&", "-+",   "The value must be digital"}
        };
    }

    /**
     * Валидация: id редактируемого и добавляемого виджета принадлежат одному сайту
     */
    @Test
    public void loadAdditionalWidgetsSetWidgetOtherSite() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widget_1);
        pagesInit.getCabCompositeSettings().switchLoadAdditionalWidgets(true);
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsId("1");
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsSelector("#loadadd");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Additional widgets id: Additional widget should be from the same site"), "FAIL -> message #1");
        log.info("Test is finished");
    }

    /**
     * В настройках дочерних виджетов (A/B , Subwidget) не выводить Load additional widget элементы
     */
    @Test
    public void loadAdditionalWidgetsCheckDisabledForChild() {
        log.info("Test is started");
        int widgetChild = 298;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetChild);
        Assert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedLoadAdditionalSettings());
        log.info("Test is finished");
    }

    /**
     * Lazy Loading
     */
    @Test(dataProvider = "dataForLoadAdditionalWidgetsLazyLoading")
    public void loadAdditionalWidgetsLazyLoading(String testCase, int widget_1, String standName, String screenName) {
        log.info("Test is started: " + testCase);

        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/" + standName))
                .setWidgetIds(widget_1)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> count widgets doesn't equals 1");

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1)
                .scrollTo()
                .countDownLatchAwait(5);

        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 2");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                              serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/" + screenName + ".png")),
                "FAIL -> check screen");
        softAssert.assertAll();
        log.info("Test is finished: " + testCase);
    }

    @DataProvider
    public Object[][] dataForLoadAdditionalWidgetsLazyLoading(){
        int widget_4 = 294;
        int widgetShadow_3 = 309;
        return new Object[][]{
                {"case: simple", widget_4,       "mgid_load_additional_widget_with_lazy_load",          "loadAdditionalWidgetsLazyLoading"},
                {"case: shadow DOM", widgetShadow_3, "mgid_load_additional_widget_with_lazy_load_shadow",   "loadAdditionalWidgetsLazyLoading"}
        };
    }

    /**
     * Элемент css селектора отсутствует на стенде
     */
    @Test
    public void loadAdditionalWidgetsWithoutLocator() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/mgid_load_additional_widget_without_locator"))
                .setWidgetIds(widget_5)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> count widgets doesn't equals 1");
        log.info("Test is finished");
    }

    /**
     * Элемент css селектора размещен более 1 раза
     */
    @Test
    public void loadAdditionalWidgetsLocatorPlace2Time() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/mgid_load_additional_widget_with_2_same_locators"))
                .setWidgetIds(widget_5)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 1");
        log.info("Test is finished");
    }

    /**
     * Размещение разных форматов: SMART + CAROUSEL
     */
    @Test
    public void loadAdditionalWidgetsCarouselAndSmart() {
        log.info("Test is started");
        int carouselId = 300;
        authDashAndGo("testEmail60@ex.ua", "publisher/edit-widget/id/" + carouselId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_CAROUSEL)
                .saveWidgetSettings();
        int smartId = 301;
        authDashAndGo("testEmail60@ex.ua", "publisher/edit-widget/id/" + smartId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/mgid_load_additional_widget_smart_carousel"))
                .setWidgetIds(carouselId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 1");
        softAssert.assertTrue(serviceInit.getScreenshotService().setDiffSizeTrigger(1250).compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), $("#section")),
                        serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/loadAdditionalWidgetsCarouselAndSmart.png")),
                "FAIL -> check screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * По отдельности у виджета с прямым размещением и additional: AUTOPLACEMENT
     */
    @Test(dataProvider = "dataForLoadAdditionalWidgetsAutoplacement")
    public void loadAdditionalWidgetsAutoplacement(String testCase, int parentId, int additionalWidgetId, String standName, String screenName) {
        log.info("Test is started: " + testCase);
        authDashAndGo("testEmail60@ex.ua", "publisher/edit-widget/id/" + additionalWidgetId);
        pagesInit.getWidgetClass()
                .setAutoplacement("top")
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT)
                .reSaveInArticleBrandformanceWidgetAndGetData()
                .saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().switchLoadAdditionalWidgets(true);
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsId(String.valueOf(additionalWidgetId));
        pagesInit.getCabCompositeSettings().fillLoadAdditionalWidgetsSelector("#loadadd");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/" + standName))
                .setWidgetIds(additionalWidgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(500);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 1");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/" + screenName + ".png")),
                "FAIL -> check screen");
        softAssert.assertAll();
        log.info("Test is finished: " + testCase);
    }

    @DataProvider
    public Object[][] dataForLoadAdditionalWidgetsAutoplacement(){
        int autoplacementId = 303;
        int widgetAutoplacementShadow = 312;
        return new Object[][]{
                {"case: simple",     302, autoplacementId,       "mgid_load_additional_widget_autoplacement",          "loadAdditionalWidgetsAutoplacement"},
                {"case: shadow DOM", 311, widgetAutoplacementShadow, "mgid_load_additional_widget_autoplacement_shadow",   "loadAdditionalWidgetsAutoplacement"}
        };
    }

    /**
     * По отдельности у виджета с прямым размещением и additional: AdBlock Integrations
     */
    @Test(dataProvider = "dataForLoadAdditionalWidgetsAdblock")
    public void loadAdditionalWidgetsAdblock(String testCase, int widget_1, String standName, String screenName) {
        log.info("Test is started: " + testCase);
        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/" + standName))
                .setWidgetIds(widget_1)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(500);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> count widgets doesn't equals 1");
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                        serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/" + screenName + ".png")),
                "FAIL -> check screen");
        softAssert.assertAll();
        log.info("Test is finished: " + testCase);
    }

    @DataProvider
    public Object[][] dataForLoadAdditionalWidgetsAdblock(){
        int adBlockId = 305;
        int widgetAdBlockShadow = 314;
        return new Object[][]{
                {"case: simple", adBlockId,      "mgid_load_additional_widget_adBlock",          "loadAdditionalWidgetsAdblock"},
                {"case: shadow DOM", widgetAdBlockShadow, "mgid_load_additional_widget_adBlock_shadow",   "loadAdditionalWidgetsAdblock"}
        };
    }
}
