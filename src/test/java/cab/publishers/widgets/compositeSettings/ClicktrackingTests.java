package cab.publishers.widgets.compositeSettings;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.widgetTemplateUrl;

public class ClicktrackingTests extends TestBase {

    private final int
            tickersCompositeId = 68,
            siteId = 51;

    private final String domain = "testsite52.com";
    private final String clientEmail = "testEmail29@ex.ua";

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }


    /**
     * set not valid data to Clicktracking macros
     * <p>RKO</p>
     */
    @Test
    public void clicktracking_noValidData() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings()
                .setClicktrackingMacros("")
                .switchClicktracking(true)
                .fillClicktrackingMacros()
                .saveWidgetSettings();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Clicktracking macros: Field error"), "FAIL -> error message");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Clicktracking turn on - check code(short code/Instant Articles code)
     * <p>RKO</p>
     */
    @Test
    public void clicktracking_turnOn_checkCode() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings()
                .setClicktrackingMacros("${CLICK_MACROS}")
                .switchClicktracking(true)
                .fillClicktrackingMacros()
                .saveWidgetSettings();

        authCabAndGo("wages/informers-code/id/" + tickersCompositeId);
        softAssert.assertTrue(pagesInit.getCabInformersCode().checkShortCodeWithClicktracking(tickersCompositeId, domain,
                siteId, pagesInit.getCabCompositeSettings().getClicktrackingMacros()), "FAIL -> short code");

        softAssert.assertTrue(pagesInit.getCabInformersCode().checkInstantArticlesCodeWithClicktracking(tickersCompositeId, domain,
                siteId, pagesInit.getCabCompositeSettings().getClicktrackingMacros()), "FAIL -> Instant Articles code");

        authDashAndGo(clientEmail,"publisher/widgets/site/" + siteId);
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkShortCodeWithClicktracking(tickersCompositeId, domain,
                        siteId, pagesInit.getCabCompositeSettings().getClicktrackingMacros()),
                "FAIL -> widget code in dash!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Clicktracking turn off - check code(short code/Instant Articles code)
     * <p>RKO</p>
     */
    @Test
    public void clicktracking_turnOff_checkCode() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings()
                .switchClicktracking(false)
                .saveWidgetSettings();

        //check code in cab
        authCabAndGo("wages/informers-code/id/" + tickersCompositeId);
        softAssert.assertTrue(pagesInit.getCabInformersCode().checkShortCode(siteId, domain, tickersCompositeId), "FAIL -> short code");

        softAssert.assertTrue(pagesInit.getCabInformersCode().checkInstantArticlesCode(tickersCompositeId, domain,
                siteId), "FAIL -> Instant Articles code");

        authDashAndGo(clientEmail,"publisher/widgets/site/" + siteId);
        softAssert.assertTrue(
                pagesInit.getWidgetClass().checkShortCode(tickersCompositeId, domain,
                        siteId),
                "FAIL -> widget code in dash!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Clicktracking turn on - check code short code in stand
     * <p>RKO</p>
     */
    @Test(dataProvider = "clicktrackingStands")
    public void clicktracking_turnOn_workShortCode(String stand) {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings()
                .setClicktrackingMacros("${CLICK_MACROS}")
                .switchClicktracking(true)
                .fillClicktrackingMacros()
                .saveWidgetSettings();

        serviceInit.getServicerMock()
                .setStandName(setStand(stand))
                .setWidgetIds(tickersCompositeId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertTrue(pagesInit.getWidgetClass().checkMctitleLink("http://local-apache.com/stands/automoto.com"));

        log.info("Test is finished");
    }

    /**
     * Clicktracking turn off - check code short code in stand
     * <p>RKO</p>
     */
    @Test(dataProvider = "clicktrackingStands")
    public void clicktracking_turnOff_workShortCode(String stand) {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickersCompositeId);
        pagesInit.getCabCompositeSettings()
                .switchClicktracking(false)
                .saveWidgetSettings();

        serviceInit.getServicerMock()
                .setStandName(setStand(stand))
                .setWidgetIds(tickersCompositeId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertTrue(pagesInit.getWidgetClass().checkMctitleLink("http://local-apache.com/stands/automoto.com"));

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] clicktrackingStands(){
        return new Object[][]{
                {"clicktrackingShortCode"},
                {"clicktrackingInstantArticlesCode"}
        };
    }
}
