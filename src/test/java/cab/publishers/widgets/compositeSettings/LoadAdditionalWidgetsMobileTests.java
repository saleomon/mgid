package cab.publishers.widgets.compositeSettings;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.widgetTemplateUrl;

public class LoadAdditionalWidgetsMobileTests extends TestBase {

    public LoadAdditionalWidgetsMobileTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void setMobileEmulation(){
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Description("load additional widget -> impact + passage")
    @Owner(value="RKO")
    @Test(description = "load additional widget -> impact + passage")
    public void loadAdditionalWidgetsImpactAndPassage() {
        log.info("Test is started");
        int impactId = 474;
        authDashAndGo("testEmail49@ex.ua", "publisher/edit-widget/id/" + impactId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT)
                .saveWidgetSettings();
        int passageId = 475;
        authDashAndGo("testEmail49@ex.ua", "publisher/edit-widget/id/" + passageId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.PASSAGE, WidgetTypes.SubTypes.NONE)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/mgid_load_additional_widget_and_passage"))
                .setWidgetIds(impactId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        sleep(1000);
        pagesInit.getWidgetClass().activatePageOnStand();
        sleep(1000);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown();
        sleep(1000);

        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                        serviceInit.getScreenshotService().getExpectedScreenshot("loadAdditionalWidgets/loadAdditionalWidgetsImpactAndPassage.png")));
        log.info("Test is finished");
    }
}
