package cab.publishers.widgets.compositeSettings;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import static core.helpers.BaseHelper.randomNumberFromRange;
import static testData.project.ClientsEntities.WEBSITE_MGID_WAGES_ID;
import static testData.project.EndPoints.compositeEditUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.Subnets.SubnetType.*;


/*
 * Flexible widget:
 * Page count:
 * Endless:
 *      - flexibleAndEndlessWidget
 *      - flexibleWidget_pageCount_validation
 *      - checkIncompatibleOptionDFPFlexWidget
 *
 * Publisher category:
 *      - checkCategoryPlatformFilter
 *
 * Subnet/Mirror
 *      - checkDontChangeSourceIdInChangeMirror
 *      - checkMirrorsInSubnetSelect
 *
 * Google DFP:
 *      - checkIncompatibleOptionDFPFlexWidget
 *
 * Auto-refresh ads by viewability
 *      - autoRefreshAdsByViewability
 *
 * Custom subnets: - не актуально, буде випилюватися
 *      - customSubnet
 *
 * Place multiple times on page:
 *      - placeMultipleTimesOnPage
 *
 * Delay of clickability:
 * Delay in ms:
 *      - delayOfClickability
 *      - delayOfClickability_validation
 *      - checkWorkFlagGoToTheTransit
 *      - displaySanctions
 *
 * SRC ID:
 *      - setSrcId
 *      - checkDontChangeSourceIdInChangeMirror
 *
 * Auto-refresh
 *      - autoRefresh
 *
 * Advertising link:
 * Widget title:
 * Widget logo:
 *      - checkSaveInformerWithOutCheckedWidgetTitleWidgetLogoTest
 *
 * XML / RSS / JSON:
 *      - clickRssAndCheckNoCalcNewsCtrInSwitchOn
 *
 * "Drum" effect:
 * Interval, ms:
 * Speed, ms
 *      - drumEffect
 *      - drumEffect_validation
 * Place reservation
 * AdBlock Integrations:
 * Change styles under AdBlock:
 *      - adBlockIntegrations
 *
 * Video SSP:
 *      - videoSSP
 */
public class CompositeSettingsTests extends TestBase {

    /**
     * Delay of clickability + Delay in ms
     * <p>RKO</p>
     */
    @Test
    public void delayOfClickability() {
        log.info("Test is started");
        int widgetId = 251;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        String delayVal = randomNumberFromRange(1, 3000);

        log.info("switch on option Delay of clickability and fill Delay in ms: " + delayVal);
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(true);
        pagesInit.getCabCompositeSettings().fillDelayInMsInput(delayVal);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("check Delay of clickability");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDelayOfClickability(true), "FAIL -> switch on option clickable_delay");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getActiveDelayValue(), delayVal, "FAIL -> delay in ms");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getActivateDelay(widgetId), delayVal, "FAIL -> switch on option activate_delay in DB");

        log.info("switch off Delay of clickability");
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkDelayOfClickability(false), "FAIL -> switch off option clickable_delay");
        softAssert.assertEquals(operationMySql.getTickersComposite().getActivateDelay(widgetId), "0", "FAIL -> switch on option activate_delay in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Delay of clickability + Delay in ms: validation
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataDelayOfClickability")
    public void delayOfClickability_validation(String delayVal) {
        log.info("Test is started");
        int widgetId = 251;
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(true);
        pagesInit.getCabCompositeSettings().fillDelayInMsInput(delayVal);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Delay in ms: Field error"));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDelayOfClickability() {
        return new Object[][]{
                {" "},
                {"-1"},
                {"as^^"}
        };
    }

    /**
     * Вывод санкций и % транзитки в КАБе в списке виджетов
     * <ul>
     *  <li>Настройка процента показа транзитки</li>
     *  <li>Настройка рандомных кликов</li>
     *  <li>Настройка рандомных кликов для десктопа</li>
     *  <li>Настройка задержки клика</li>
     *  <li>Проверка отображения настроек в интерфейсе списка виджетов</li>
     *  </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-22370">Ticket VT-22370</a>
     * <p>NIO</p>
     */
    @Privilege(name = "can_see_sanctions_widget")
    @Test
    public void displaySanctions() {
        log.info("Test is started");
        int widgetId = 252;
        String template = "Percentage of merchant transit page - 100.00%\n" +
                "Doubleclick desktop - Invisible\n" +
                "Doubleclick mobile - Default\n" +
                "Click delay - 1000 ms";
        authCabForCheckPrivileges("wages/widgets/?c_id=" + widgetId);
        authCabAndGo(pagesInit.getCabWidgetsList().getLinkEditProductSubWidget());

        pagesInit.getCabGoodsSettings().setTranzitPagePercentage("1");
        pagesInit.getCabGoodsSettings().saveWidgetSettings();

        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(true);
        pagesInit.getCabCompositeSettings().fillDelayInMsInput("1000");
        pagesInit.getCabCompositeSettings().selectMobileDoubleclick("1");
        pagesInit.getCabCompositeSettings().selectDesktopDoubleClick("1");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(pagesInit.getCabWidgetsList().checkDisplayedSanctionSettings(template), "FAIL - Check settings of sanctions");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_see_sanctions_widget");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        softAssert.assertFalse(pagesInit.getCabWidgetsList().checkVisibilityDisplayedSanctionIcon(), "FAIL - Check visibility");

        log.info("Test is finished");
    }

    /**
     * Проставляется source name s1 при смене зеркала КИ с mgid на steepto
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-23538">TA-23538</a>
     */
    @Test
    public void checkDontChangeSourceIdInChangeMirror() {
        log.info("Test is started");
        int widgetId = 249;
        operationMySql.getTickersComposite().updateMirror("marketgid", widgetId);
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchSourceId(false);
        pagesInit.getCabCompositeSettings().chooseMirror();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isSourceIdChecked(), "FAIL: source id isSelected");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkMirror(pagesInit.getCabCompositeSettings().getMirror()), "FAIL: mirror doesn't correct");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Выбор зеркал для сабнета")
    @Story("Show mirrors in the widget settings from its own subnet")
    @Description("Check mirrors for current subnet(0,2) before and after edit widget <a href='https://jira.mgid.com/browse/TA-51887'>TA-51887</a>")
    @Test(dataProvider = "dataCheckMirrorsInSubnetSelect")
    public void checkMirrorsInSubnetSelect(int widgetId, Subnets.SubnetType subnetId) {
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertEquals(
                pagesInit.getCabCompositeSettings().getAllMirrorsFromSubnetSelect(),
                operationMySql.getSubnetMirrors().getMirrorsName(subnetId.getTypeValue()),
                "FAIL -> before choose mirror"
        );
        pagesInit.getCabCompositeSettings().chooseMirror();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkMirror(pagesInit.getCabCompositeSettings().getMirror()), "FAIL: mirror doesn't correct");

        softAssert.assertEquals(
                pagesInit.getCabCompositeSettings().getAllMirrorsFromSubnetSelect(),
                operationMySql.getSubnetMirrors().getMirrorsName(subnetId.getTypeValue()),
                "FAIL -> before edit mirror"
        );
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataCheckMirrorsInSubnetSelect() {
        return new Object[][]{
                {383, SCENARIO_MGID},
                {385, SCENARIO_ADSKEEPER}
        };
    }

    /**
     * НА. Редактирование информеров (category platform)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/MT-320">MT-320</a>
     */
    @Test
    public void checkCategoryPlatformFilter() {
        log.info("Test is started");
        int widgetId = 248;
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().chooseCategoryPlatform();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkCategoryPlatform());
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23552">...</a>
     * Не разрешать ставить одновременно Google DFP + Flexible widget
     * - check save widget with two options Google DFP + Flexible widget
     * - check error message
     * - check state of options after attempt save this options
     * NIO
     */
    @Test
    public void checkIncompatibleOptionDFPFlexWidget() {
        log.info("Test is started");
        String widgetId = "1005";

        log.info("enable options DFP and flexible widget");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchFlexibleWidget(true);
        pagesInit.getCabCompositeSettings().switchGoogleDfp(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("check state of dfp and flex widget options");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Flexible widget: You cannot use Flexible widget option with Google DFP"), "Message isn't displayed - \"Flexible widget: You cannot use Flexible widget option with Google DFP\"");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkFlexibleWidget(false), "FAIL -> checkFlexibleWidget");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkGoogleDfp(false), "FAIL -> checkGoogleDfp");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Ставить флаг tickers.no_calc_news_ctr при включении РСС
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-22557">TA-22557</a>
     */
    @Test
    public void clickRssAndCheckNoCalcNewsCtrInSwitchOn() {
        int tickerCompositeId = 12;
        int tickersId = 1;
        log.info("Test is started");
        log.info("check tickers.no_calc_news_ctr = 1");
        authCabAndGo(compositeEditUrl + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchXmlCheckbox(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo("wages/informers-edit/type/news/id/" + tickersId);
        softAssert.assertTrue(pagesInit.getCabExchangeSettings().noCalcNewsCtrIsSelected(), "noCalcNewsCtr -> isn't selected");

        log.info("check tickers.no_calc_news_ctr = 0");
        authCabAndGo(compositeEditUrl + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchXmlCheckbox(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo("wages/informers-edit/type/news/id/" + tickersId);
        softAssert.assertFalse(pagesInit.getCabExchangeSettings().noCalcNewsCtrIsSelected(), "noCalcNewsCtr -> is selected");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Удалять бандл Impact при включении виджету xml
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24414">TA-24414</a>
     */
    @Test
    public void deleteBundlesIfImpactWidgetTurnOffXml() {
        log.info("Test is started");
        subnetId = SCENARIO_MGID;
        authDashAndGo("publisher/add-widget/site/" + WEBSITE_MGID_WAGES_ID);
        pagesInit.getWidgetClass().createWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        int g_blocks = operationMySql.getGblocks().getGBlocksId(pagesInit.getWidgetClass().getWidgetId());

        authCabAndGo(compositeEditUrl + pagesInit.getWidgetClass().getWidgetId());
        pagesInit.getCabCompositeSettings().switchXmlCheckbox(false);
        Assert.assertTrue(operationMySql.getBundlesGBlocks().selectIsExistBundlesId(g_blocks));
        log.info("Test is finished");
    }

    /**
     * <ul>
     * <li>1. Проверка сохранения информера с отключенными полями "Widget title" и "Widget logo" и выключенном флаге "Рекламный флаг/Advertising link"</li>
     * <li>2. Проверка сохранения информера с отключенным полем "Widget title" и отключенном "Widget logo" при включенном флаге "Рекламный флаг/Advertising link"</li>
     * <li>3. Проверка сохранения информера с включенным полем "Widget logo" и  отключенном "Widget title" при включенном флаге "Рекламный флаг/Advertising link"</li>
     * <li>4. Проверка сохранения информера с отключенным флагом "Рекламный флаг/Advertising link"</li>
     * </ul>
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-22218">VT-22218</a>
     */
    @Test
    public void checkSaveInformerWithOutCheckedWidgetTitleWidgetLogoTest() {
        log.info("Test is started");
        int widgetId = 250;
        authCabAndGo(compositeEditUrl + widgetId);

        log.info("1. Проверка сохранения информера с отключенными полями \"Widget title\" и \"Widget logo\" и включенном флаге \"Рекламный флаг/Advertising link\"");
        pagesInit.getCabCompositeSettings().switchWidgetLogoAndTitle(true, false, false);
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkWidgetLogoAndTitleSettings(false, false, false), "FAIL -> case#1");

        log.info("2. Проверка сохранения информера с отключенным полем \"Widget title\" и отключенном \"Widget logo\" при включенном флаге \"Рекламный флаг/Advertising link\"");
        pagesInit.getCabCompositeSettings().switchWidgetLogoAndTitle(true, false, true);
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkWidgetLogoAndTitleSettings(true, false, true), "FAIL -> case#2");

        log.info("3. Проверка сохранения информера с включенным полем \"Widget logo\" и  отключенном \"Widget title\" при включенном флаге \"Рекламный флаг/Advertising link\"");
        pagesInit.getCabCompositeSettings().switchWidgetLogoAndTitle(true, true, false);
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkWidgetLogoAndTitleSettings(true, true, false), "FAIL -> case#3");

        log.info("4. Проверка сохранения информера с отключенным флагом \"Рекламный флаг/Advertising link\"");
        pagesInit.getCabCompositeSettings().switchWidgetLogoAndTitle(false, false, false);
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkWidgetLogoAndTitleSettings(false, false, false), "FAIL -> case#4");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check parameters after edit widget
     * <ul>
     *     <li>Advertising link(tickers_composite.adLink)</li>
     * </ul>
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataWidgetsWithDifferentSubnets")
    public void checkStateBaseFunctionAfterEditWidget(String subnet, int widgetId) {
        log.info("Test is started: " + subnet);
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().chooseMirror();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkWidgetLogoAndTitleSettings(true, true, true));
        log.info("Test is finished: " + subnet);
    }

    @DataProvider
    public Object[][] dataWidgetsWithDifferentSubnets(){
        return new Object[][]{
                {"mgid",  103},
                {"IO",    105}
        };
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most relevant valid case.\n" +
            "     <ul>\n" +
            "      <li>most relevant valid case</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void autoRefreshAdsByViewability() {
        log.info("Test is started");
        int widgetId = 251;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);

        log.info("switch on Auto-refresh ads by viewability");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAutoRefreshAdsByViewability(true);
        pagesInit.getCabCompositeSettings().fillAutoRefreshAdsByViewabilityTime("3");
        pagesInit.getCabCompositeSettings().selectAutoRefreshAdsByViewabilityBy("1");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("check Auto-refresh ads by viewability");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAutoRefreshAdsByViewability(true), "FAIL -> switch on checkAutoRefreshAdsByViewability");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getAutoRefreshAdsByViewabilityTime(), "3", "FAIL -> getAutoRefreshAdsByViewabilityTime");
        softAssert.assertEquals(pagesInit.getCabCompositeSettings().getAutoRefreshAdsByViewabilityBy(), "1", "FAIL -> getAutoRefreshAdsByViewabilityBy");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getRefreshAdsTime(widgetId), "3", "FAIL -> getRefreshAdsTime in DB");
        softAssert.assertEquals(operationMySql.getTickersComposite().getRefreshAdsBy(widgetId), "1", "FAIL -> getRefreshAdsBy in DB");

        log.info("switch off Auto-refresh ads by viewability");
        pagesInit.getCabCompositeSettings().switchAutoRefreshAdsByViewability(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAutoRefreshAdsByViewability(false), "FAIL -> switch off checkAutoRefreshAdsByViewability");
        softAssert.assertEquals(operationMySql.getTickersComposite().getRefreshAdsBy(widgetId), "0", "FAIL -> getRefreshAdsBy in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most relevant validation.\n" +
            "     <ul>\n" +
            "      <li>most relevant validation</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test(dataProvider = "dataAutoRefreshAdsByViewabilityValidation")
    public void autoRefreshAdsByViewabilityValidation(String time) {
        log.info("Test is started");
        int widgetId = 251;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);

        log.info("switch on Auto-refresh ads by viewability");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAutoRefreshAdsByViewability(true);
        pagesInit.getCabCompositeSettings().fillAutoRefreshAdsByViewabilityTime(time);
        pagesInit.getCabCompositeSettings().selectAutoRefreshAdsByViewabilityBy("1");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Refresh time: The field must contain only numbers in the range from 0 to 3600"));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAutoRefreshAdsByViewabilityValidation() {
        return new Object[][]{
                {"-1"},
                {"3601"},
                {"as^^"}
        };
    }
}
