package cab.publishers.widgets.compositeSettings;

import core.service.customAnnotation.Privilege;
import org.junit.Assert;
import org.testng.annotations.*;
import testBase.TestBase;

import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

/*
 * Вывести возможность проставления зоны некликабельности в настройки информера CAB
 * <p>RKO</p>
 * @see <a href="https://youtrack.mgid.com/issue/TA-22058">TA-22058</a>
 *
 * - nonClickableArea
 * - nonClickableAreaOptionTest_switchOff
 * - nonClickableArea_privilege
 * - nonClickableArea_validation_withoutOptions
 * - nonClickableArea_validation_sumPercentMoreThan100
 */
public class NonClickableAreaTests extends TestBase {

    private final int tickerCompositeId = 42;

    /**
     * check privilege: wages/can_edit_informer_non_clickable_area
     * <p>RKO</p>
     */
    @Privilege(name = "wages/can_edit_informer_non_clickable_area")
    @Test
    public void nonClickableArea_privilege() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "wages/can_edit_informer_non_clickable_area");
            authCabForCheckPrivileges("wages/informers-edit/type/composite/id/" + tickerCompositeId);
            Assert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedNonClickableAreaCheckbox());
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "wages/can_edit_informer_non_clickable_area");
        }
        log.info("Test is finished");
    }

    /**
     * Проверяем негативный кейс со всеми значениями области в 0 (Установлены по умолчанию)
     * <p>RKO</p>
     */
    @Test
    public void nonClickableArea_validation_withoutOptions() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 1);
        pagesInit.getCabCompositeSettings().switchNonClickableAreaCheckbox(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Enable informer non-clickable area: All values \u200B\u200Bare set to \"0\""));
        log.info("Test is finished");
    }

    /**
     * sum percent areas more than 100%
     * <p>RKO</p>
     */
    @Test
    public void nonClickableArea_validation_sumPercentMoreThan100() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchNonClickableAreaCheckbox(true);

        log.info("Устанавливаем все обалсти, протягиваем вниз на 50 пикселей");
        pagesInit.getCabCompositeSettings().setAllAreasResizable_valid();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Enable informer non-clickable area: The sum of the percentages of the opposite parties can not be more than 100"));
        log.info("Test is finished");
    }

    /**
     * check work non clickable area
     * <p>RKO</p>
     */
    @Test
    public void nonClickableArea() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchNonClickableAreaCheckbox(true);

        log.info("Устанавливаем все обалсти, протягиваем вниз на 50 пикселей");
        pagesInit.getCabCompositeSettings().setAllAreasResizable();

        log.info("Получаем текущее значение области в процентах");
        pagesInit.getCabCompositeSettings().getAllResizableValue();
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("Проверяем что сохраненное значение равно тому что выставляли");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkAllAreasResizable());
        log.info("Test is finished");
    }

    /**
     * check turn off non clickable area after turn on
     * <p>RKO</p>
     */
    @Test(dependsOnMethods = "nonClickableArea")
    public void nonClickableAreaOptionTest_switchOff() {
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchNonClickableAreaCheckbox(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + tickerCompositeId);
        Assert.assertTrue(pagesInit.getCabCompositeSettings().checkNonClickableAreaCheckbox(false));
        log.info("Test is finished");
    }
}
