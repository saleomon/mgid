package cab.publishers.widgets.compositeSettings;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.ClientsEntities.CLIENTS_EMAIL;
import static testData.project.EndPoints.compositeEditUrl;

public class CompositeSettings2Tests extends TestBase {

    /**
     * Place multiple times on page
     * <p>RKO</p>
     */
    @Test
    public void placeMultipleTimesOnPage() {
        int widgetId = 6;
        log.info("Test is started");
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        log.info("switch on");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(true), "FAIL -> allow_multiple_widgets switch_on in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAllowMultipleWidgets(widgetId), "1", "FAIL -> allow_multiple_widgets switch_on in DB");

        log.info("re-save widget in Dash and check it in Cab");
        authDashAndGo(CLIENTS_EMAIL,"publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(true), "FAIL -> allow_multiple_widgets switch_on in UI after re-save in Dash");

        log.info("switch off");
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceMultipleTimesOnPage(false), "FAIL -> allow_multiple_widgets switch_off in UI");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAllowMultipleWidgets(widgetId), "0", "FAIL -> allow_multiple_widgets switch_off in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Auto-refresh
     * <p>RKO</p>
     */
    @Test
    public void autoRefresh() {
        log.info("Test is started");
        int widgetId = 247;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        log.info("switch on");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAutoRefresh(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAutoRefresh(true), "FAIL -> autorefresh switch_on in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAutorefresh(widgetId), "1", "FAIL -> autorefresh switch_on in DB");

        log.info("switch off");
        pagesInit.getCabCompositeSettings().switchAutoRefresh(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAutoRefresh(false), "FAIL -> autorefresh switch_off in UI");
        softAssert.assertEquals(operationMySql.getTickersComposite().getAutorefresh(widgetId), "0", "FAIL -> autorefresh switch_off in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Place reservation
     * <p>RKO</p>
     */
    @Test
    public void placeReservation() {
        log.info("Test is started");
        int widgetId = 260;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        log.info("switch off");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchPlaceReservation(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(false), "FAIL -> use_place_reservation switch_off in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getPlaceReservation(widgetId), "0", "FAIL -> use_place_reservation switch_off in DB");

        log.info("re-save widget in Dash and check it in Cab");
        authDashAndGo("testEmail33@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().saveWidgetAndGetId();

        log.info("check after dash");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(false), "FAIL -> use_place_reservation switch_off after re-save in dash in UI");

        log.info("switch on");
        pagesInit.getCabCompositeSettings().switchPlaceReservation(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkPlaceReservation(true), "FAIL -> use_place_reservation switch_on in UI");
        softAssert.assertEquals(operationMySql.getTickersComposite().getPlaceReservation(widgetId), "1", "FAIL -> use_place_reservation switch_on in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Video SSP
     * <p>RKO</p>
     */
    @Test
    public void videoSSP() {
        log.info("Test is started");
        int widgetId = 256;
        updatedTime = operationMySql.getTickersComposite().getUpdatedTime(widgetId);
        log.info("switch on");
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchVideoSsp(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkVideoSsp(true), "FAIL -> video_ssp switch_on in UI");
        softAssert.assertNotEquals(updatedTime, operationMySql.getTickersComposite().getUpdatedTime(widgetId), "FAIL -> updatedTime");
        softAssert.assertEquals(operationMySql.getTickersComposite().getVideoSsp(widgetId), "1", "FAIL -> video_ssp switch_on in DB");

        log.info("switch off");
        pagesInit.getCabCompositeSettings().switchVideoSsp(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkVideoSsp(false), "FAIL -> video_ssp switch_off in UI");
        softAssert.assertEquals(operationMySql.getTickersComposite().getVideoSsp(widgetId), "0", "FAIL -> video_ssp switch_off in DB");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    /**
     * Проставлять автоматически флаг "Go to the transit" при повышении времени некликабельности
     * тест проверяет что при значении "Delay in ms"  больше= 2000, флаг "Go to the transit" проставляется автоматом, а при значении меньшем - снимается
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21256">TA-21256</a>
     */
    @Test
    public void checkWorkFlagGoToTheTransit() {
        log.info("Test is started");
        int tickerCompositeId = 40;
        String transitOn = "2001";
        String transitOff = "1999";

        log.info("вводим значение задержки кликабельности: transitOn");
        authCabAndGo(compositeEditUrl + tickerCompositeId);
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(true);
        pagesInit.getCabCompositeSettings().fillDelayInMsInput(transitOn);

        log.info("проверяем что goToTheTransit isSelected");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().goToTheTransitIsSelected(), "FAIL -> goToTheTransit isSelected");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("проверяем что флаг goToTheTransit isSelected после сохранения виджета");
        authCabAndGo(compositeEditUrl + tickerCompositeId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().goToTheTransitIsSelected(), "FAIL -> goToTheTransit isSelected после сохранения виджета");

        log.info("вводим значение задержки кликабельности: transitOff");
        pagesInit.getCabCompositeSettings().switchDelayOfClickability(true);
        pagesInit.getCabCompositeSettings().fillDelayInMsInput(transitOff);

        log.info("проверяем что goToTheTransit !isSelected");
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().goToTheTransitIsSelected(), "FAIL -> goToTheTransit !isSelected");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        log.info("проверяем что флаг goToTheTransit !isSelected после сохранения виджета");
        authCabAndGo(compositeEditUrl + tickerCompositeId);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().goToTheTransitIsSelected(), "FAIL -> goToTheTransit !isSelected после сохранения виджета");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Не записывается кастомный source_name при включении SRC ID у композита
     * проверка включения srcID и проставления source_name с последующим их изменением
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/VT-21930">VT-21930</a>
     */
    @Test
    public void setSrcId() {
        log.info("Test is started");
        int widgetId = 255;
        authCabAndGo(compositeEditUrl + widgetId);

        log.info("switch on SrcId + source name: test");
        pagesInit.getCabCompositeSettings().switchAndSetSrcId(true, "test");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSrcId(true, "test"), "FAIL->check switch on SrcId with source name: test");

        log.info("switch on SrcId + source name: test2");
        pagesInit.getCabCompositeSettings().switchAndSetSrcId(true, "test2");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSrcId(true, "test2"), "FAIL->check switch on SrcId with source name: test2");

        log.info("switch off SrcId");
        pagesInit.getCabCompositeSettings().switchAndSetSrcId(false, "");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSrcId(false, ""), "FAIL->check switch off SrcId");

        log.info("switch on SrcId + source name: test3");
        pagesInit.getCabCompositeSettings().switchAndSetSrcId(true, "test3");
        authCabAndGo(compositeEditUrl + widgetId);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkSrcId(true, "test3"), "FAIL->check switch on SrcId with source name: test3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Ошибка при сохранении резинового информера
     * Тест проверяет функции (резиновый и бесконечный) доступные из каба. Виджет сохраняется в кабе с разными комбинациями этих функций и пересохраняется в деше
     * <p>RKO</p>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-21490">TA-21490</a>
     */
    @Test
    public void flexibleAndEndlessWidget() {
        log.info("Test is started");
        int widgetId = 253;
        log.info("выбираем обе функции - резиновый и бесконечный, пересохраняем виджет в дешборде");
        authCabAndGo(compositeEditUrl + widgetId);
        switchElasticAndUnlimitedCheckboxesGoDashAndReSaveWidget(widgetId,true, true);
        log.info("проверяем что обе функции - резиновый и бесконечный не слетели");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkFlexibleWidget(true), "FAIL -> резиновый(выбираем обе функции - резиновый и бесконечный)");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkEndless(true), "FAIL -> бесконечный(выбираем обе функции - резиновый и бесконечный)");

        log.info("выбираем одну функцию - резиновый");
        authCabAndGo(compositeEditUrl + widgetId);
        switchElasticAndUnlimitedCheckboxesGoDashAndReSaveWidget(widgetId,true, false);
        log.info("проверяем что обе функции - резиновый и бесконечный не слетели");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkFlexibleWidget(true), "FAIL -> резиновый(выбираем одну функцию - резиновый)");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkEndless(false), "FAIL -> бесконечный(выбираем одну функцию - резиновый)");

        log.info("обе функции отключены");
        authCabAndGo(compositeEditUrl + widgetId);
        switchElasticAndUnlimitedCheckboxesGoDashAndReSaveWidget(widgetId,false, false);
        log.info("проверяем что обе функции - резиновый и бесконечный не слетели");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkFlexibleWidget(false), "FAIL -> резиновый(обе функции отключены)");
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkEndless(false), "FAIL -> бесконечный(обе функции отключены)");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Flexible widget + Page count
     * <p>RKO</p>
     */
    @Test(dataProvider = "dataElasticWidgetPageCount")
    public void flexibleWidget_pageCount_validation(String pageCount) {
        log.info("Test is started");
        int widgetId = 253;
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchFlexibleWidget(true);
        pagesInit.getCabCompositeSettings().fillPageCount(pageCount);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Page count: Field error"));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataElasticWidgetPageCount() {
        return new Object[][]{
                {" "},
                {"0"},
                {"20000"},
                {"-1"},
                {"as^^"}
        };
    }

    private void switchElasticAndUnlimitedCheckboxesGoDashAndReSaveWidget(int widgetId, boolean isElasticTurn, boolean isUnlimitedTurn) {
        authCabAndGo(compositeEditUrl + widgetId);

        pagesInit.getCabCompositeSettings().switchFlexibleWidget(isElasticTurn);
        if (isElasticTurn) {
            pagesInit.getCabCompositeSettings().switchEndless(isUnlimitedTurn);
            pagesInit.getCabCompositeSettings().fillPageCount("2");
        }
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        helpersInit.getAuthorizationHelper().goByGogol();
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().saveWidgetAndGetId();
        authCabAndGo(compositeEditUrl + widgetId);
    }

    @Epic("Edit tickers composite")
    @Feature("MCM configurations in the widget settings")
    @Story("Edit tickers composite form in Cab")
    @Description("Check enable, set, edit and disable GPT options." +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52654\">Ticket TA-52654</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check enable, set, edit and disable GPT options.")
    public void checkSaveEditGptIntegration() {
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 2011);
        pagesInit.getCabCompositeSettings().enableGptIntegration(true);
        pagesInit.getCabCompositeSettings().fillGptSlotValue();
        pagesInit.getCabCompositeSettings().fillGptDivIdValue();
        pagesInit.getCabCompositeSettings().fillGptAdUnitIdValue();
        pagesInit.getCabCompositeSettings().fillPlacementForDesktopValue();
        pagesInit.getCabCompositeSettings().fillPlacementForMobileValue();
        pagesInit.getCabCompositeSettings().enableGptLazyLoad(true);
        pagesInit.getCabCompositeSettings().enableGptDisplayOnSubwidgets(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + 2011);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkGptIntegration(),
                "FAIL -> GPT settings after first set!");
        pagesInit.getCabCompositeSettings().fillGptSlotValue();
        pagesInit.getCabCompositeSettings().fillGptDivIdValue();
        pagesInit.getCabCompositeSettings().fillGptAdUnitIdValue();
        pagesInit.getCabCompositeSettings().fillPlacementForDesktopValue();
        pagesInit.getCabCompositeSettings().fillPlacementForMobileValue();
        pagesInit.getCabCompositeSettings().enableGptLazyLoad(false);
        pagesInit.getCabCompositeSettings().enableGptDisplayOnSubwidgets(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + 2011);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkGptIntegration(),
                "FAIL -> GPT settings after editing!");
        pagesInit.getCabCompositeSettings().enableGptIntegration(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(compositeEditUrl + 2011);
        pagesInit.getCabCompositeSettings().enableGptIntegration(true);
        softAssert.assertTrue(pagesInit.getCabCompositeSettings().checkAllGptSettingsIsEmpty(),
                "FAIL -> clear GPT settings!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Edit tickers composite")
    @Feature("MCM configurations in the widget settings")
    @Story("Edit tickers composite form in Cab")
    @Description("Check validations for empty required fields." +
            "<ul>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52654\">Ticket TA-52654</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check validations for empty required fields.")
    public void checkGptIntegrationEmptyFieldValidations() {
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 2012);
        pagesInit.getCabCompositeSettings().enableGptIntegration(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("GPT slot: FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> empty 'Gpt slot' validation!");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ad Unit ID: FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> empty 'Ad Unit' validation!");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("GPT divId: FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> empty 'GPT divId' validation!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Edit tickers composite")
    @Feature("MCM configurations in the widget settings")
    @Story("Edit tickers composite form in Cab")
    @Description("Check validations for wrong data in fields." +
            "<ul>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52654\">Ticket TA-52654</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check validations for wrong data in fields.")
    public void checkGptIntegrationTypeFieldValidation() {
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 2012);
        pagesInit.getCabCompositeSettings().enableGptIntegration(true);
        String gptSlotValue = "GPT slot";
        String gptDivId = "GPT DivId";
        String gptAdUnitId = "GPT Ad Unit ID";
        String placementForDesktop = "one";
        String placementForMobile = "two";
        pagesInit.getCabCompositeSettings().enableGptIntegration(true);
        pagesInit.getCabCompositeSettings().fillGptSlotValue(gptSlotValue);
        pagesInit.getCabCompositeSettings().fillGptDivIdValue(gptDivId);
        pagesInit.getCabCompositeSettings().fillGptAdUnitIdValue(gptAdUnitId);
        pagesInit.getCabCompositeSettings().fillPlacementForDesktopValue(placementForDesktop);
        pagesInit.getCabCompositeSettings().fillPlacementForMobileValue(placementForMobile);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Ad Unit ID: Validation error. Please check your data"),
                "FAIL -> Wrong type 'GPT Ad Unit ID' validation!");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Placement for desktop: Validation error. Please check your data"),
                "FAIL -> Wrong type 'GPT Ad Unit ID' validation!");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Placement for mobile: Validation error. Please check your data"),
                "FAIL -> Wrong type 'GPT Ad Unit ID' validation!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Edit tickers composite")
    @Feature("MCM configurations in the widget settings")
    @Story("Edit tickers composite form in Cab")
    @Description("Check that GPT options are unavailable for child widgets." +
            "<ul>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52654\">Ticket TA-52654</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that GPT options are unavailable for child widgets.")
    public void checkGptIntegrationIsNotAvailableForChildWidget() {
        log.info("Test is started");
        authCabAndGo(compositeEditUrl + 2015);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedGptIntegrations(),
                "FAIL -> GPT is available for AB test widget!");
        authCabAndGo(compositeEditUrl + 2016);
        softAssert.assertFalse(pagesInit.getCabCompositeSettings().isDisplayedGptIntegrations(),
                "FAIL -> GPT is available for child widget!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
