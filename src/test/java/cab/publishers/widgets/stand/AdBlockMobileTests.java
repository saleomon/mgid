package cab.publishers.widgets.stand;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.cab.publishers.logic.widgets.CabCompositeSettings;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.compositeEditUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

public class AdBlockMobileTests extends TestBase {

    private final int widgetUnderArticleAdblockId = 446;
    private final int widgetInArticleAdblockId = 467;
    private final int widgetHeaderAdblockId = 482;
    private final int widgetSidebarAdblockId = 491;
    private final int widgetUnderArticleAdblockIdealmediaIoId = 447;
    private final int widgetInArticleAdblockIdealmediaIoId = 468;
    private final int widgetHeaderAdblockIdealmediaIoId = 483;
    private final int widgetSidebarAdblockIdealmediaIoId = 492;

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeClass
    public void createChildAdBlock(){
        authCabAndGo(compositeEditUrl + widgetUnderArticleAdblockId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.UNDER_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo(compositeEditUrl + widgetUnderArticleAdblockIdealmediaIoId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.UNDER_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //in-article
        authCabAndGo(compositeEditUrl + widgetInArticleAdblockId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.IN_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo(compositeEditUrl + widgetInArticleAdblockIdealmediaIoId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.IN_ARTICLE);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //header
        authCabAndGo(compositeEditUrl + widgetHeaderAdblockId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.HEADER);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo(compositeEditUrl + widgetHeaderAdblockIdealmediaIoId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.HEADER);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //sidebar
        authCabAndGo(compositeEditUrl + widgetSidebarAdblockId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.SIDEBAR);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo(compositeEditUrl + widgetSidebarAdblockIdealmediaIoId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.SIDEBAR);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("run cron");
        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        log.info("update some fields for childId");
        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetUnderArticleAdblockId);
        operationMySql.getTickersComposite().updateOptions(childId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("update some fields for childId");
        int childIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetUnderArticleAdblockIdealmediaIoId);
        operationMySql.getTickersComposite().updateOptions(childIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childIdealmediaIoId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childIdealmediaIoId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //in-article
        log.info("update some fields for childId");
        int childInArticleId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetInArticleAdblockId);
        operationMySql.getTickersComposite().updateOptions(childInArticleId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childInArticleId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childInArticleId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("update some fields for childId");
        int childInArticleIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetInArticleAdblockIdealmediaIoId);
        operationMySql.getTickersComposite().updateOptions(childInArticleIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childInArticleIdealmediaIoId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childInArticleIdealmediaIoId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //header
        log.info("update some fields for childId");
        int childHeaderId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetHeaderAdblockId);
        operationMySql.getTickersComposite().updateOptions(childHeaderId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childHeaderId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childHeaderId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("update some fields for childId");
        int childHeaderIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetHeaderAdblockIdealmediaIoId);
        operationMySql.getTickersComposite().updateOptions(childHeaderIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childHeaderIdealmediaIoId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childHeaderIdealmediaIoId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //sidebar
        log.info("update some fields for childId");
        int childSidebarId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetSidebarAdblockId);
        operationMySql.getTickersComposite().updateOptions(childSidebarId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childSidebarId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childSidebarId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        log.info("update some fields for childId");
        int childSidebarIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetSidebarAdblockIdealmediaIoId);
        operationMySql.getTickersComposite().updateOptions(childSidebarIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        operationMySql.getTickersComposite().updateDoubleClickSettings(childSidebarIdealmediaIoId, false, 0, true, 3);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childSidebarIdealmediaIoId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
    }

    @BeforeMethod
    public void setMobileEmulation(){
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration(mobile)")
    @Story("Add In-Article template for adblock integration")
    @Story("Add header template for adblock integration")
    @Story("Add sidebar template for adblock integration")
    @Description("Add Under-Article/In-Article/Header/Sidebar adBlock childId and check screenshot <a href='https://jira.mgid.com/browse/TA-52007'>TA-52007, </a><a href='https://jira.mgid.com/browse/TA-52008'>TA-52008, </a><a href='https://jira.mgid.com/browse/TA-52070'>TA-52009</a><a href='https://jira.mgid.com/browse/TA-52009'>TA-52009</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockCheckScreen", description = "Add Under-Article/In-Article/Header/Sidebar adBlock childId and check screenshot")
    public void adBlockCheckScreen(Subnets.SubnetType subnetId, int widgetId, String stand, String screen) {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().takeScreenshotSimple(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot("adBlockWidgets/" + screen + ".png")
        ));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockCheckScreen(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-mobile-under-article-mgid",          "adBlockUnderArticleCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-mobile-under-article-idealmedia-io", "adBlockUnderArticleIdealmediaIoCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-mobile-in-article-mgid",             "adBlockInArticleCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-mobile-in-article-idealmedia-io",    "adBlockInArticleIdealmediaIoCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-mobile-header-mgid",                 "adBlockHeaderCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-mobile-header-idealmedia-io",        "adBlockHeaderIdealmediaIoCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-mobile-sidebar-mgid",                "adBlockSidebarCheckScreenMobile"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-mobile-sidebar-idealmedia-io",       "adBlockSidebarIdealmediaIoCheckScreenMobile"}
        };
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration(mobile)")
    @Story("Add header template for adblock integration")
    @Story("Add sidebar template for adblock integration")
    @Description("Add Under-Article/Header/Sidebar adBlock childId and check work Doubleclick <a href='https://jira.mgid.com/browse/TA-52007'>TA-52007, </a><a href='https://jira.mgid.com/browse/TA-52008'>TA-52008, </a><a href='https://jira.mgid.com/browse/TA-52009'>TA-52009</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockCheckDoubleclick", description = "Add Under-Article/Header/Sidebar adBlock childId and check work Doubleclick")
    public void adBlockCheckDoubleclick(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().setShadowDom(true).clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 2с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(4000);
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(3), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockCheckDoubleclick(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-mobile-under-article-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-mobile-under-article-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-mobile-header-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-mobile-header-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-mobile-sidebar-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-mobile-sidebar-idealmedia-io"}
        };
    }

    @Feature("adBlock")
    @Story("Add In-Article template for adblock integration")
    @Description("Add In-Article adBlock childId and check work Doubleclick <a href='https://jira.mgid.com/browse/TA-52070'>TA-52070</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockInArticleCheckDoubleclick", description = "Add In-Article adBlock childId and check work Doubleclick")
    public void adBlockInArticleCheckDoubleclick(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().setShadowDom(true).clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 11с, время даблклика закончилось! кликаем по 1-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(11000);
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockInArticleCheckDoubleclick(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,             "adblock-mobile-in-article-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId, "adblock-mobile-in-article-idealmedia-io"}
        };
    }
}
