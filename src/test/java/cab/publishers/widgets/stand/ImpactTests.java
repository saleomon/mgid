package cab.publishers.widgets.stand;

import libs.devtools.ServicerMock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.List;

import static testData.project.EndPoints.widgetTemplateUrl;

public class ImpactTests extends TestBase {

    private final int widgetId = 138;

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    private final List<String> teasersList = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"A young millionaire from the city of Kiev\",\"Tired of living in poverty? Do you want to receive an award? Find out how to earn more and how not to deny yourself anything\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser Altz rsd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser Altz dom\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser Altz ltg\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser Altz mqa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser Altz dtz\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    @BeforeClass
    public void reSaveWidget(){
        authDashAndGo("testEmail41@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
    }

    /**
     * Не корректно обрезает title в impacte
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-27292">TA-27292</a>
     */
    @Test(dataProvider = "dataForChangeCustomStylesForVariousWidgets")
    public void titleLimitAndDescLimit_inImpact(int title, int description, String titleValue, String descriptionValue) {
        log.info("Test is started");

        authCabAndGo("wages/informers-settings/id/" + widgetId);
        pagesInit.getCabEditCode().fillTitleLimit(title);
        pagesInit.getCabEditCode().fillDescriptionLimit(description);
        pagesInit.getCabEditCode().saveEditCodeInterface();

        serviceInit.getServicerMock().setStandName(setStand("mgid_impact_title_limit"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(teasersList)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(pagesInit.getWidgetClass().getTitleText(),
                 titleValue + " ...",
                "FAIL -> title length");

        softAssert.assertEquals(pagesInit.getWidgetClass().getDescriptionText(),
                descriptionValue + " ...",
                "FAIL -> description length");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * title = "Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион";
     * description = "Надоело жить в нищете? Хотите получить премию? Узнай как заработать больше и как себе ни в чём не отказывать";
     */
    @DataProvider
    public Object[][] dataForChangeCustomStylesForVariousWidgets() {
        return new Object[][]{
                { 10,  10, "A young", "Tired of"},
                { 37,  75, "A young millionaire from the city of", "Tired of living in poverty? Do you want to receive an award? Find out how"},
                { 37, 100, "A young millionaire from the city of", "Tired of living in poverty? Do you want to receive an award? Find out how to earn more and how not"}
        };
    }
}
