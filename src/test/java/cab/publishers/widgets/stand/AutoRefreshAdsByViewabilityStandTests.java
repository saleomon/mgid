package cab.publishers.widgets.stand;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

public class AutoRefreshAdsByViewabilityStandTests extends TestBase {

    private final int mostRelevantId        = 315;
    private final int newAdsId              = 316;
    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");
    public final SelenideElement locatorForAmp = $("#amp_widget");

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most relevant valid case.\n" +
            "     <ul>\n" +
            "      <li>most relevant valid case</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevant() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/most_relevant"))
                .setWidgetIds(mostRelevantId)
                .setServicerCountDown(5)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 5, "FAIL -> count widgets doesn't equals 5");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 1), "1", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 2), "1", "FAIL -> request#3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 3), "1", "FAIL -> request#4");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 4), "1", "FAIL -> request#5");

        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevant.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most relevant valid case without visibility.\n" +
            "     <ul>\n" +
            "      <li>most relevant valid case without visibility</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevantWithoutVisibility() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/most_relevant_without_visibility"))
                .setWidgetIds(mostRelevantId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(7);

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1);
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds valid case\n" +
            "     <ul>\n" +
            "      <li>newAds valid case</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAds() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/new_ads"))
                .setWidgetIds(newAdsId)
                .setServicerCountDown(5)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 5, "FAIL -> count widgets doesn't equals 5");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 1), "2", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 2), "2", "FAIL -> request#3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 3), "2", "FAIL -> request#4");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 4), "2", "FAIL -> request#5");

        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevant.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds valid case without visibility.\n" +
            "     <ul>\n" +
            "      <li>most relevant valid case without visibility</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAdsWithoutVisibility() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/new_ads_without_visibility"))
                .setWidgetIds(newAdsId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(7);

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1);
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds valid case parent child(tickers_composite_relations.sql) \n" +
            "     <ul>\n" +
            "      <li>newAds valid case parent child(tickers_composite_relations.sql)</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevantParentChild() {
        log.info("Test is started");

        int mostRelevantChildId = 318;
        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/most_relevant_parent_child"))
                .setWidgetIds(mostRelevantChildId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setServicerCountDown(3)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(10);

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 3, "FAIL -> count widgets doesn't equals 3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantChildId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantChildId, "rsh", 1), "1", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantChildId, "rsh", 2), "1", "FAIL -> request#3");

        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevant.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds valid case parent child(tickers_composite_ab_tests.sql) \n" +
            "     <ul>\n" +
            "      <li>newAds valid case parent child(tickers_composite_ab_tests.sql)</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAdsParentChild() {
        log.info("Test is started");

        int newAdsChildId = 320;
        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/new_ads_parent_child"))
                .setWidgetIds(newAdsChildId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setServicerCountDown(3)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 3, "FAIL -> count widgets doesn't equals 3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsChildId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsChildId, "rsh", 1), "2", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsChildId, "rsh", 2), "2", "FAIL -> request#3");

        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevant.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most Relevant case infiniteScroll\n" +
            "     <ul>\n" +
            "      <li>most Relevant case infiniteScroll</li>\n" +
            "      <li>rsh=2: infinite scroll; rsh=1: most relevant</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevantInfiniteScroll() {
        log.info("Test is started");

        int smartId = 321;
        authDashAndGo("testEmail62@ex.ua", "publisher/edit-widget/id/" + smartId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);
        pagesInit.getWidgetClass()
                .setInfiniteScroll(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN);

        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/most_relevant_infinite_scroll"))
                .setWidgetIds(smartId)
                .setServicerCountDown(3)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollTo()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 3, "FAIL -> count widgets doesn't equals 3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(smartId, "rsh", 0), null, "FAIL -> request#1");
        String rsh_1 = serviceInit.getServicerMock().parseServicerRequest(smartId, "rsh", 1);
        assert rsh_1 != null;
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(smartId, "rsh", 2), rsh_1, "FAIL -> request#3");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds + lazy load\n" +
            "     <ul>\n" +
            "      <li>newAds + lazy load</li>\n" +
            "      <li>rsh=2: infinite scroll; rsh=1: most relevant</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAdsLazyLoad() {
        log.info("Test is started");

        int lazyLoadId = 322;
        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/new_ads_lazy_load"))
                .setWidgetIds(lazyLoadId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(7);

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0, "FAIL -> count widgets doesn't equals 0");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds + adBlock\n" +
            "     <ul>\n" +
            "      <li>newAds + adBlock</li>\n" +
            "      <li>rsh=2: infinite scroll; rsh=1: most relevant</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevantAdBlock() {
        log.info("Test is started");

        int adBlockId = 323;
        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/most_relevant_adBlock"))
                .setWidgetIds(adBlockId)
                .setServicerCountDown(3)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(7);

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 3, "FAIL -> count widgets doesn't equals 3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(adBlockId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(adBlockId, "rsh", 1), "1", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(adBlockId, "rsh", 2), "1", "FAIL -> request#3");

        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevantAdBlock.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds + shadowDOM\n" +
            "     <ul>\n" +
            "      <li>newAds + shadowDOM</li>\n" +
            "      <li>rsh=2: infinite scroll; rsh=1: most relevant</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAdsShadowDOM() {
        log.info("Test is started");

        int shadowDomId = 324;
        serviceInit.getServicerMock().setStandName(setStand("auto_refresh_ads_by_viewability/new_ads_shadowDOM"))
                .setWidgetIds(shadowDomId)
                .setServicerCountDown(3)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(7);

        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 3, "FAIL -> count widgets doesn't equals 3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(shadowDomId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(shadowDomId, "rsh", 1), "2", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(shadowDomId, "rsh", 2), "2", "FAIL -> request#3");

        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/newAdsShadowDOM.png")), "FAIL -> screen");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("most relevant valid case AMP code.\n" +
            "     <ul>\n" +
            "      <li>most relevant valid case AMP code</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void mostRelevantAmp() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setAmpStand("most_relevant_amp"))
                .setWidgetIds(mostRelevantId)
                .setServicerCountDown(4)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/mostRelevantAmp.png")), "FAIL -> screen");
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 4, "FAIL -> count widgets doesn't equals 4");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 1), "1", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 2), "1", "FAIL -> request#3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(mostRelevantId, "rsh", 3), "1", "FAIL -> request#4");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Опция 'Auto-refresh ads by viewability' в настройках виджета ( админка )")
    @Description("newAds valid case AMP \n" +
            "     <ul>\n" +
            "      <li>newAds valid case AMP</li>\n" +
            "      <li>@see <a href \"https://jira.mgid.com/browse/TA-51816\">Ticket TA-51816</a></li>\n" +
            "      <li><p>Author RKO</p></li>\n" +
            "     </ul>")
    @Test
    public void newAdsAmp() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setAmpStand("new_ads_amp"))
                .setWidgetIds(newAdsId)
                .setServicerCountDown(4)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForAmp),
                serviceInit.getScreenshotService().getExpectedScreenshot("autoRefreshAdsByViewability/newAdsAmp.png")), "FAIL -> screen");
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 4, "FAIL -> count widgets doesn't equals 4");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 0), null, "FAIL -> request#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 1), "2", "FAIL -> request#2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 2), "2", "FAIL -> request#3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(newAdsId, "rsh", 3), "2", "FAIL -> request#4");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
