package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.widgetTemplateUrl;

public class DoubleClickMobileTests extends TestBase {

    private final int parentId = 75;
    private final int childId  = 76;

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeClass
    public void firstSaveParentWidgetFromDamp(){
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on mobile emulation")
    @Description("parent: random_clicks_disabled_desktop=0; child: random_clicks_disabled=1, desktop_doubleclick_delay = 4")
    @Owner("RKO")
    @Test(description = "parent: random_clicks_disabled_desktop=0; child: random_clicks_disabled=1, desktop_doubleclick_delay = 4")
    public void doubleclickMobileParentChildCase1() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick_mobile"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка десктопного даблклика 4с, но так как у нас мобильный девайс, то кликаем на 1-вое место и проверяем что НЕ появился блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on mobile emulation")
    @Description("parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=11, random_clicks_disabled=1, mobile_doubleclick_delay=9;" +
            "child: random_clicks_disabled=1, mobile_doubleclick_delay=4")
    @Owner("RKO")
    @Test(description = "parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=11, random_clicks_disabled=1, mobile_doubleclick_delay=9;" +
            "child: random_clicks_disabled=1, mobile_doubleclick_delay=4")
    public void doubleclickMobileParentChildCase2() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "2");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        //parent
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "9");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "11");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick_mobile"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setResponseParams("\"dcb\":1")
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 4с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(4000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on mobile emulation")
    @Description("parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=11, random_clicks_disabled=1, mobile_doubleclick_delay=4;" +
            "child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=2, random_clicks_disabled=1, mobile_doubleclick_delay=7")
    @Owner("RKO")
    @Test(description = "parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=11, random_clicks_disabled=1, mobile_doubleclick_delay=4;" +
            "child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=2, random_clicks_disabled=1, mobile_doubleclick_delay=7")
    public void doubleclickMobileParentChildCase3() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "7");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "2");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        //parent
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "11");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick_mobile"))
                .setWidgetIds(parentId)
                .setSubnetType(subnetId)
                .setResponseParams("\"dcb\":1")
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 1с, время даблклика закончилось! кликаем по 2-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(4000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 1.1: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on mobile emulation")
    @Description("child: random_clicks_disabled=1, mobile_doubleclick_delay=4")
    @Owner("RKO")
    @Test(description = "child: random_clicks_disabled=1, mobile_doubleclick_delay=4")
    public void doubleclickMobileParentChildCase4() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick_mobile"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setResponseParams("\"dcb\":1")
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick DOESN'T show");

        log.info("ждём ещё 4с, время даблклика закончилось! кликаем по 2-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(4000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 2: doubleClick show");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
