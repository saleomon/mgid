package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * 1. Child:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 4
 *    Parent:
 *      - random_clicks_disabled_desktop = 0
 *    Response:
 *      - child
 * 2. Child:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 4
 *    Parent:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 10
 *    Response:
 *      - child
 * 3. Child:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 10
 *    Parent:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 4
 *    Response:
 *      - parent
 * 4. Child:
 *      - random_clicks_disabled = 1
 *      - desktop_doubleclick_delay = 4
 *    Parent:
 *      - random_clicks_disabled_desktop = 1
 *      - desktop_doubleclick_delay = 10
 *    Response:
 *      - child
 */
public class DoubleClickDesktopTests extends TestBase {

    private final int parentId = 73;
    private final int childId  = 74;

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Feature("Doubleclick")
    @Story("Doubleclick on desktop")
    @Description("parent: random_clicks_disabled_desktop=0; child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    @Owner("RKO")
    @Test(description = "parent: random_clicks_disabled_desktop=0; child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    public void doubleclickDesktopParentChildCase1() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");


        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём 2с и кликаем по 2-ому месту в виджете и проверяем что на нём появляется блюр на тизере и кнопка GO!");
        sleep(2000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 2: doubleClick show");

        log.info("ждём ещё 2с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(3000);
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(3), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on desktop")
    @Description("parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    @Owner("RKO")
    @Test(description = "parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    public void doubleclickDesktopParentChildCase2() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        //parent
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "10");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setResponseParams("\"dcb\":1")
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём 2с и кликаем по 2-ому месту в виджете и проверяем что на нём появляется блюр на тизере и кнопка GO!");
        sleep(2000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 2: doubleClick show");

        log.info("ждём ещё 2с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(3000);
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(3), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on desktop")
    @Description("child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    @Owner("RKO")
    @Test(description = "child: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=4")
    public void doubleclickDesktopParentChildCase3() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "10");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        //parent
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick"))
                .setWidgetIds(parentId)
                .setSubnetType(subnetId)
                .setResponseParams("\"dcb\":1")
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём 2с и кликаем по 2-ому месту в виджете и проверяем что на нём появляется блюр на тизере и кнопка GO!");
        sleep(2000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 2: doubleClick show");

        log.info("ждём ещё 2с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(3000);
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(3), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Doubleclick")
    @Story("Doubleclick on desktop")
    @Description("child: random_clicks_disabled=1, desktop_doubleclick_delay=4; parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; Response: child")
    @Owner("RKO")
    @Test(description = "child: random_clicks_disabled=1, desktop_doubleclick_delay=4; parent: random_clicks_disabled_desktop=1, desktop_doubleclick_delay=10; Response: child")
    public void doubleclickDesktopParentChildCase4() {
        log.info("Test is started");

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("1", "4");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("0", "10");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        //parent
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().setMobileDoubleClickSettings("0", "10");
        pagesInit.getCabCompositeSettings().setDesktopDoubleClickSettings("1", "10");
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("parent_child_doubleclick"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка мобильного даблклика 4с, Но так как у нас десктоп, то кликаем на 1-вое место и проверяем что НЕ появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: mobile doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
