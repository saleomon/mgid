package cab.publishers.widgets.stand;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.Screenshot;
import testBase.baseTemplate.ScreenshotWidgetStandCheckingTemplate;
import testData.project.EndPoints;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.constantTemplates.ConstantsInit.RKO;
import static testData.project.EndPoints.widgetTemplateUrl;

public class BackButtonDesktopTests extends ScreenshotWidgetStandCheckingTemplate {

    private final int widgetId = 543,
                widgetHeaderId = 550;

    private final String baseScreenPackage = "back-button/desktop/";

    private final String smartWidgetId = "544";
    private final String headerSelector = "#header-button";
    private final String textColor = "#5f497a";
    private final String backgroundColor = "#76923c";

    private final String backButtonDefaultHeaderAndDefaultBannerJson = "{\"display\": [{\"type\": \"banner\"}, {\"type\": \"header\"}], \"widget_id\": " + smartWidgetId + "}";


    public String setStand(String stand){ return String.format(widgetTemplateUrl, "back-button/desktop/" + stand); }

    @BeforeClass
    public void prepareJson(){
        String backButtonJson = "{\"display\": [{\"type\": \"banner\", \"options\": {\"text\": \"%s\", \"bg_color\": \"%s\", \"text_color\": \"%s\"}}, {\"type\": \"header\", \"options\": {\"selector\": \"%s\"}}], \"widget_id\": %s}";
        String bannerText = "[WIDGET_TITLE_SPONSORED_CONTENT]";
        String jsonWithoutTrafficAndDesktop = String.format(backButtonJson,
                bannerText, backgroundColor, textColor,
                headerSelector,
                smartWidgetId);

        operationMySql.getTickersComposite().updateBackButtonOptions(widgetId, jsonWithoutTrafficAndDesktop);

        authDashAndGo("testEmail80@ex.ua","publisher/edit-widget/id/" + smartWidgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .switchInfiniteScrollTumbler(false)
                .saveWidgetSettings();

        authDashAndGo("testEmail80@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS)
                .saveWidgetSettings();
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("set back button options and check them in db and ui; " +
            " check: Display Banner/Display Header " +
            " <a href='https://jira.mgid.com/browse/TA-52303'>TA-52303</a>")
    @Owner(RKO)
    @Test(description = "set back button options and check them in db and ui")
    public void backButton() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("back-button-start-page"))
                .setWidgetIds(widgetId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        softAssert.assertEquals(serviceInit.getServicerMock().getSessionStorage().get("_mgStartUrl"), null, "FAIL->get _mgStartUrl without user events");
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        softAssert.assertEquals(serviceInit.getServicerMock().getSessionStorage().get("_mgStartUrl"), "http://local-apache.com/stands/back-button/desktop/back-button.html", "FAIL->get _mgStartUrl after user events");
        Selenide.back();
        sleep(1000);

        softAssert.assertTrue(serviceInit.getScreenshotService().setDiffSizeTrigger(1100).compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + "back-button.png")), "FAIL -> screen simple");
        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("check back button without user events <a href='https://jira.mgid.com/browse/TA-52303'>TA-52303</a>")
    @Owner(RKO)
    @Test(description = "check back button without user events")
    public void backButtonWithoutUserEvents() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("back-button-start-page"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        sleep(5000);
        Assert.assertNull(serviceInit.getServicerMock().getSessionStorage().get("_mgStartUrl"));
        serviceInit.getServicerMock().tearDown();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add back button settings to the admin panel")
    @Description("check back button with two transition <a href='https://jira.mgid.com/browse/TA-52303'>TA-52303</a>")
    @Owner(RKO)
    @Test(description = "check back button with two transition")
    public void backButtonWithTwoTransition() {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("back-button-start-page"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);

        Selenide.back();
        pagesInit.getWidgetClass().hoverToFooterStand();
        Selenide.back();
        sleep(1000);

        Screenshot actualScreen = serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen);
        Screenshot expectedScreen1 = serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + "back-button.png");
        Screenshot expectedScreen2 = serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + "back-button-1.png");
        Assert.assertTrue((serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen1) ||
                serviceInit.getScreenshotService().compareScreenshots(actualScreen, expectedScreen2)));
        serviceInit.getServicerMock().tearDown();
        log.info("Test is finished");
    }

    @Feature("Back button")
    @Story("Add header and popup settings to back button options")
    @Description("<ul>Cases:" +
            "<li>1.back button with default header (by tag 'header')</li>" +
            "<li>2.back button with custom header (by attribute: class/id)</li>" +
            "<li>3.back button with default banner (only checkbox activate)</li>" +
            "<li>4.back button with default banner and custom color</li>" +
            "<li>5.back button without banner and header</li>" +
            "<li>6.back button with default header and default banner</li>" +
            "<li><a href='https://jira.mgid.com/browse/TA-52304'>TA-52304</a></li>" +
            "</ul>")
    @Owner(RKO)
    @Test(description = "back button with different banner and header settings", dataProvider = "backButtonHeaders")
    public void backButtonHeaderAndBanner(String json, String expectedScreen) {
        log.info("Test is started");

        //pre-condition
        operationMySql.getTickersComposite().updateBackButtonOptions(widgetHeaderId, json);
        authCabAndGo(EndPoints.compositeEditUrl + widgetHeaderId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //test
        serviceInit.getServicerMock().setStandName(setStand("back-button-start-page"))
                .setWidgetIds(widgetId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        Selenide.back();
        sleep(1000);

        Assert.assertTrue(serviceInit.getScreenshotService().setDiffSizeTrigger(1600).compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + expectedScreen)));
        serviceInit.getServicerMock().tearDown();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] backButtonHeaders(){
        String backButtonDefaultHeaderJson = "{\"display\": [{\"type\": \"header\"}], \"widget_id\": " + smartWidgetId + "}";
        String backButtonCustomHeaderJson = "{\"display\": [{\"type\": \"header\", \"options\": {\"selector\": \"" + headerSelector + "\"}}], \"widget_id\": " + smartWidgetId + "}";
        String backButtonDefaultBannerJson = "{\"display\": [{\"type\": \"banner\"}], \"widget_id\": " + smartWidgetId + "}";
        String backButtonDefaultBannerWithCustomColorJson = "{\"display\": [{\"type\": \"banner\", \"options\": {\"bg_color\": \"" + backgroundColor + "\", \"text_color\": \"" + textColor + "\"}}], \"widget_id\": " + smartWidgetId + "}";
        String backButtonWithoutHeaderAndBannerJson = "{\"widget_id\": " + smartWidgetId + "}";
        return new Object[][]{
                {backButtonDefaultHeaderJson, "back-button-default-header.png"},
                {backButtonCustomHeaderJson, "back-button-custom-header.png"},
                {backButtonDefaultBannerJson, "back-button-default-banner.png"},
                {backButtonDefaultBannerWithCustomColorJson, "back-button-default-banner-with-custom-color.png"},
                {backButtonWithoutHeaderAndBannerJson, "back-button-without-header-and-banner.png"},
                {backButtonDefaultHeaderAndDefaultBannerJson, "back-button-default-header-and-default-banner.png"}
        };
    }

    @Feature("Back button")
    @Story("Add header and popup settings to back button options")
    @Description("back button check close banner <a href='https://jira.mgid.com/browse/TA-52304'>TA-52304</a>")
    @Owner(RKO)
    @Test(description = "back button check close banner")
    public void backButtonCloseBanner() {
        log.info("Test is started");
        operationMySql.getTickersComposite().updateBackButtonOptions(widgetHeaderId, backButtonDefaultHeaderAndDefaultBannerJson);
        authCabAndGo(EndPoints.compositeEditUrl + widgetHeaderId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        //test
        serviceInit.getServicerMock().setStandName(setStand("back-button-start-page"))
                .setWidgetIds(widgetId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem();
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(2);
        serviceInit.getServicerMock().countDownLatchAwaitWithoutTearDown(2);
        pagesInit.getWidgetClass().clickCustomLinkInterstitial(1);
        Selenide.back();
        sleep(1000);

        softAssert.assertTrue(serviceInit.getScreenshotService().setDiffSizeTrigger(1600).compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + "back-button-default-header-and-default-banner.png")), "FAIL -> show banner and header");

        //close banner
        pagesInit.getWidgetClass().clickBackButtonBannerIcon();
        sleep(1000);

        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot(baseScreenPackage + "back-button-close-banner.png")), "FAIL -> show header");
        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
