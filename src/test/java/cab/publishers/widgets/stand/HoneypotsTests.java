package cab.publishers.widgets.stand;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;


public class HoneypotsTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "honeypot/" + stand); }

    @BeforeClass
    public void preCondition(){
        authDashAndGo("testEmail77@ex.ua","publisher/edit-widget/id/" + 456);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR)
                .saveWidgetSettings();

        authDashAndGo("testEmail77@ex.ua","publisher/edit-widget/id/" + 457);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.HEADER, WidgetTypes.SubTypes.HEADER_RECTANGULAR)
                .saveWidgetSettings();

        authDashAndGo("testEmail77@ex.ua","publisher/edit-widget/id/" + 458);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .saveWidgetSettings();
    }

    @Story("Honeypots in Widgets")
    @Description("Check count links for one place without Honeypot<a href='https://jira.mgid.com/browse/TA-52069'>TA-52069</a>")
    @Test(description = "Check count links without Honeypot")
    public void widgetWithoutHoneypot() {
        int widgetId = 456;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("without-honeypot"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getCountLinksInMcimg(), 1);
        log.info("Test is finished");
    }

    @Story("Honeypots in Widgets")
    @Description("Check count links for one place with Honeypot <a href='https://jira.mgid.com/browse/TA-52069'>TA-52069</a>")
    @Test(description = "Check count links with Honeypot")
    public void widgetWithHoneypot() {
        int widgetId = 457;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("with-honeypot"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getCountLinksInMcimg(), 3);
        log.info("Test is finished");
    }

    @Story("Honeypots in Widgets")
    @Description("Check show 2 widgets with Honeypot (smart/header)<a href='https://jira.mgid.com/browse/TA-52069'>TA-52069</a>")
    @Test(description = "Check show 2 widgets with Honeypot (smart/header)")
    public void widgetsWithHoneypot() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("two-widgets-with-honeypots"))
                .setWidgetIds(457, 458)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver()),
                serviceInit.getScreenshotService().getExpectedScreenshot("widgetsWithHoneypot.png")
        ));
        log.info("Test is finished");
    }

}
