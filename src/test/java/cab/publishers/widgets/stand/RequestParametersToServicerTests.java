package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.ClientsEntities.MGID_WIDGET_UNDER_ARTICLE_ID;
import static testData.project.EndPoints.cabLink;
import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * ogtitle
 */
public class RequestParametersToServicerTests extends TestBase {
    public final String login = "testEmail38@ex.ua";

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    private final List<String> requestParametersToServicerInitialComposite_teasers = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион\",\"Надоело жить в нищете? Хотите получить премию? Узнай как заработать больше и как себе ни в чём не отказывать\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser Altz rsd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser Altz dom\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser Altz ltg\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser Altz mqa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser Altz dtz\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    private final List<String> requestParametersToServicerDisclaimer = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион\",\"Надоело жить в нищете? Хотите получить премию? Узнай как заработать больше и как себе ни в чём не отказывать\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\",\"cdt\":\"Disclaimer text\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser Altz rsd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\",\"cdt\":\"\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser Altz dom\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser Altz ltg\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser Altz mqa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser Altz dtz\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//www.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    @BeforeMethod
    public void setDefaultSubnet() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: pv/nit/niet/nisd/ref/lu/npa/pageView/pvid/dpr/uspString <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>, <a href='https://jira.mgid.com/browse/VT-28024'>VT-28024</a>")
    @Test(description = "check servicer parameters: pv/nit/niet/nisd/ref/lu/npa/pageView/pvid/dpr/uspString")
    public void requestParametersToServicer_staticParams() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .setEvaluateJsBeforeLoadScript("navigator.connection.type = 'wifi';")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "pv"), "5", "FAIL -> pv");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "nit"), "wifi", "FAIL -> wifi");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "niet"), "4g", "FAIL -> niet");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "nisd"), "false", "FAIL -> nisd");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "ref"), "", "FAIL -> ref");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "lu"), "http%3A%2F%2Flocal-apache.com%2Fstands%2Fmgid_under_article.html", "FAIL -> lu");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "npa"), null, "FAIL -> npa");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "pageView"), "1", "FAIL -> pageView");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "pvid"), null, "FAIL -> pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "dpr"), "1", "FAIL -> dpr");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "uspString"), "test_usp_string", "FAIL -> uspString");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: ogtitle(заголовок с мета тега og:title передаем в случае если у виджета включен внутренний обмен (tickers_composite.enabled_cooperation_types))")
    @Test
    public void requestParametersToServicer_ogtitle() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_under_article"))
                .setWidgetIds(MGID_WIDGET_UNDER_ARTICLE_ID)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(MGID_WIDGET_UNDER_ARTICLE_ID, "ogtitle"), "Test%20autotest%20SOK%20title%20import%20autocretative", "FAIL -> ogtitle");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: ogtitle = null")
    @Test
    public void requestParametersToServicer_ogtitle_null() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("load_additional_widget/mgid_load_additional_widget"))
                .setWidgetIds(295)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertNull(serviceInit.getServicerMock().parseServicerRequest(295, "ogtitle"), "FAIL -> ogtitle");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: limitads/adb/ad/src_id/rtb_disabled")
    //todo RKO @Test
    public void requestParametersToServicer_withAdBlock() {
        int widgetId = 116;
        log.info("Test is started");
        authDashAndGo(login,"publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_with_adblock"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "limitads"), "3", "FAIL -> limitads");
        // adb - 1 или 0 в зависимости включен adblock или нет.
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "adb"), "1", "FAIL -> adb");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ad"), "pg,r", "FAIL -> ad");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "src_id"), null, "FAIL -> src_id");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rtb_disabled"), null, "FAIL -> rtb_disabled");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters (adBlock = true; enable_source = 1): limitads/adb/ad/src_id/rtb_disabled")
    @Test
    public void requestParametersToServicer_withAdBlock_and_enableSource() {
        int widgetId = 118;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_with_adblock_and_enableSource"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "limitads"), "3", "FAIL -> limitads");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "adb"), "1", "FAIL -> adb");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ad"), "pg,r", "FAIL -> ad");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "src_id"), "eyeo", "FAIL -> src_id");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rtb_disabled"), "1", "FAIL -> rtb_disabled");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters (adBlock = false; enable_source = 1): limitads/adb/ad")
    @Test
    public void requestParametersToServicer_withoutAdBlock() {
        int widgetId = 117;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_without_adblock"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "limitads"), null, "FAIL -> limitads");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "adb"), null, "FAIL -> adb");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ad"), null, "FAIL -> ad");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: npa")
    @Test
    public void requestParametersToServicer_npa() {
        int widgetId = 117;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_without_adblock"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setEvaluateJsBeforeLoadScript("window.MG_setRequestNonPersonalizedAds=1;")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "npa"), "1", "FAIL -> npa");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: iso/token")
    @Test
    public void requestParametersToServicer_IsoAndToken() {
        int widgetId = 1;
        log.info("Test is started");
        authCabAndGo("wages/widgets/?c_id=" + widgetId);
        serviceInit.getServicerMock().setStandName(cabLink + "wages/informers-preview/momentary/1/id/" + widgetId)
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        serviceInit.getServicerMock().clearServicerMockData();
        pagesInit.getCabInformersPreviewMomentary().regionSelectOpen().fillRegionSearch("Ukr");

        sleep(1000);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "iso"), "UA", "FAIL -> iso");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "token"), "c0fcce02ef996b38f0f112f13e39a942", "FAIL -> token");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: childs(tickers_composite_relations)")
    @Test
    public void requestParametersToServicer_childs() {
        int widgetId = 87;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_childs"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "childs"), "88,89", "FAIL -> childs");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(tickers_composite.enabled(childs) = 1): childs(tickers_composite_ab_tests)")
    @Test
    public void requestParametersToServicer_childs_ab() {
        int widgetId = 123;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_childs_abtest"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "childs"), "124,125", "FAIL -> childs");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(tickers_composite.enabled(childs) = 0; tickers_composite.options = {\"use_root_ab_id\": true}): childs(tickers_composite_ab_tests)")
    @Test
    public void requestParametersToServicer_childs_ab_use_root_ab_id() {
        int widgetId = 126;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_childs_abtest_use_root_ab"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "childs"), "126", "FAIL -> childs");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(tickers_composite.enabled(childs) = 0): childs(tickers_composite_ab_tests) = null")
    @Test
    public void requestParametersToServicer_childs_ab_enabled() {
        int widgetId = 128;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_childs_abtest_disabled"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertNull(serviceInit.getServicerMock().parseServicerRequest(widgetId, "childs"), "FAIL -> childs");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: rsh")
    @Test
    public void requestParametersToServicer_rshElastic() {
        int widgetId = 130;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_rsh"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), null, "FAIL -> rsh #1");

        serviceInit.getServicerMock()
                .clearServicerMockData()
                .setWidgetIds(widgetId)
                .scrollTo()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), "2", "FAIL -> rsh #2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(tickers_composite.paginator): rsh/pageView")
    @Test
    public void requestParametersToServicer_rshPagination() {
        int widgetId = 131;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_rsh_pagination"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), null, "FAIL -> rsh #1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "1", "FAIL -> pageView=1");

        serviceInit.getServicerMock().clearServicerMockData();

        pagesInit.getWidgetClass().clickNextPageInPaginatorOnStand();

        serviceInit.getServicerMock()
                .setWidgetIds(widgetId)
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), "2", "FAIL -> rsh #2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "0", "FAIL -> pageView=0");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(InSiteNotification (reRun option, if true than second request contains 'rsh=2')): rsh/pageView")
    @Test
    public void requestParametersToServicer_rshOnSiteNotification() {
        subnetId = Subnets.SubnetType.SCENARIO_ADSKEEPER;
        int widgetId = 132;
        log.info("Test is started");
        authDashAndGo("testEmail39@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.NONE);
        pagesInit.getWidgetClass()
                .setRerunAds(true)
                .changeDataAndSaveWidget(WidgetTypes.Types.IN_SITE_NOTIFICATION, WidgetTypes.SubTypes.NONE);
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_rsh_onSiteNotification"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), null, "FAIL -> rsh #1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "1", "FAIL -> pageView=1");

        pagesInit.getWidgetClass().clickCloseIconInSiteNotification();

        serviceInit.getServicerMock()
                .clearServicerMockData()
                .setWidgetIds(widgetId)
                .countDownLatchAwait(70);
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), "2", "FAIL -> rsh #2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "0", "FAIL -> pageView=0");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: \n" +
            "- initial_composite - для резиновых информеров передаем идентификатор виджета tickers_composite.id.\n" +
            "                    Для первого запроса передаем идентификатор композита, если от servicer приходит id другого виджета то во всех последующих запросах передаем id от servicer\n" +
            "- pageView\n" +
            "- exclude_int_exchange - це коли видача від servicer приходить там є “type” :  “w”, ‘i’\n" +
            "                          тип тизера, wages, internal ads і тд\n" +
            "                          ми шукаємо ті, які з “type” : ‘i’, запам’ятовуємо і прокидаємо далі ці ід шки в exclude_int_exchange\n" +
            "                          щоб в наступній видачі (резиновий віджет) вони вже не попалися\n" +
            "- muid - muidn пользователя восстановленный из localStorage")
    @Test
    public void requestParametersToServicer_initial_composite() {
        int parentId = 133;
        int childId = 134;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_initial_composite"))
                .setWidgetIds(parentId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(requestParametersToServicerInitialComposite_teasers)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "initial_composite"), null, "FAIL -> initial_composite #1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "pageView"), "1", "FAIL -> pageView=1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "exclude_int_exchange"), null, "FAIL -> exclude_int_exchange #1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "muid"), null, "FAIL -> muidn #1");

        serviceInit.getServicerMock()
                .clearServicerMockData()
                .setWidgetIds(childId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .scrollToSlow()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(childId, "initial_composite"), String.valueOf(parentId), "FAIL -> initial_composite #2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(childId, "pageView"), "0", "FAIL -> pageView=0");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(childId, "exclude_int_exchange"), "7,8,9", "FAIL -> exclude_int_exchange #2");

        serviceInit.getServicerMock()
                .clearServicerMockData()
                .setWidgetIds(parentId)
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "initial_composite"), String.valueOf(childId), "FAIL -> initial_composite #3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "pageView"), "0", "FAIL -> pageView=0");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "exclude_int_exchange"), "7,8,9", "FAIL -> exclude_int_exchange #3");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(parentId, "muid"), serviceInit.getServicerMock().getMuid(), "FAIL -> muidn #3");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters(set og:url): cxurl")
    @Test
    public void requestParametersToServicer_cxurl_ogUrl() {
        int widgetId = 135;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_cxurl_ogUrl"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "cxurl"), "https%3A%2F%2Fog_url.com", "FAIL -> cxurl");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: \n" +
            "- pr - це primary referrer.\n" +
            "      Типу перша сторінка з якої ти попав на сайт.\n" +
            "      Наприклад ти нагуглив якийсь сайт, переходиш на нього.\n" +
            "      В pr - записується гугл. Потім ти перехдиш в глуб сайту, і реферер в тебе вже той сайт внутрішній. Але в  pr передається все так же гугл\n" +
            "- ref")
    @Test
    public void requestParametersToServicer_pr() {
        int widgetId = 135;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_pr"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setEvaluateJsBeforeLoadScript("setTimeout(function() {" +
                        "document.getElementById('pr_link').click();" +
                        "}, 3000);")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pr"), "local-apache.com", "FAIL -> pr");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ref"), "http%3A%2F%2Flocal-apache.com%2Fstands%2Fserviser_param_pr.html", "FAIL -> ref");
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestHeader("Referer"), "http://local-apache.com/", "FAIL -> Referer");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: " +
            "- afp - платформа браузера пользователя передаем в случае если в tickers_composite.antifraud_options установлен флаг ssp_sanctions_enabled,\n" +
            "        данные получаем navigator.platform")
    @Test
    public void requestParametersToServicer_afp() {
        int widgetId = 136;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("serviser_param_afp"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "afp"), "linux%20x86_64");
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: " +
            "- cdt:Check text of disclaimer in teasers in widget <a href='https://jira.mgid.com/browse/TA-51727'> TA-51727</a>")
    @Test
    public void checkDisclaimerInWidget() {
        log.info("Test is started");
        int widgetId = 2004;
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_with_disclaimer"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(requestParametersToServicerDisclaimer)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisclaimerText("1"), "Disclaimer text",
                "FAIL -> Disclaimer text is absent in teaser #1");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisclaimerText("2"), "",
                "FAIL -> Disclaimer text is absent in teaser #2");
        softAssert.assertEquals(pagesInit.getWidgetClass().getDisclaimerText("3"), "",
                "FAIL -> Disclaimer text is absent in teaser #3");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=true; 'eventStatus': 'tcloaded'; consentData != null <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=true; consentData=test_consent_data; 'eventStatus': 'tcloaded'")
    public void requestParametersToServicerGdprAppliesTrueAndConsentData() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-true"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "gdprApplies"), "1", "FAIL -> gdprApplies");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "consentData"), "test_consent_data", "FAIL -> consentData");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=false; 'eventStatus': 'tcloaded'; consentData != null <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=false; consentData=test_consent_data; 'eventStatus': 'tcloaded'")
    public void requestParametersToServicerGdprAppliesFalseAndConsentData() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-false"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "gdprApplies"), "0", "FAIL -> gdprApplies");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "consentData"), "test_consent_data", "FAIL -> consentData");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=false; without 'eventStatus'; consentData != null <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=false; consentData=test_consent_data;")
    public void requestParametersToServicerGdprAppliesFalseWithoutEventStatus() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-false-without-eventStatus"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(15);
        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0);
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=false; not valid 'eventStatus'; consentData != null <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=false; consentData=test_consent_data; eventStatus=cmpuishown")
    public void requestParametersToServicerGdprAppliesFalseEventStatusCmpuishown() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-false-eventStatus-cmpuishown"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(15);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> servicer count request");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "gdprApplies"), null, "FAIL -> gdprApplies");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "consentData"), null, "FAIL -> consentData");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=true; 'eventStatus'=cmpuishown;  <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=true; eventStatus=cmpuishown")
    public void requestParametersToServicerGdprAppliesTrueEventStatusCmpuishown() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-true-eventStatus-cmpuishown"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(15);
        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0);
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: gdprApplies=false; 'eventStatus' with timeout; consentData != null <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "gdprApplies=false; consentData=test_consent_data_2; eventStatus=useractioncomplete")
    public void requestParametersToServicerGdprAppliesFalseEventStatusWithTimeout() {
        int widgetId = 423;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("servicer-gdprApplies/gdprApplies-false-eventStatus-with-timeout"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown(5);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 0, "FAIL -> servicer count #1");
        sleep(6000);
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> servicer count #1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "gdprApplies"), "0", "FAIL -> gdprApplies");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "consentData"), "test_consent_data_2", "FAIL -> consentData");
        serviceInit.getServicerMock().tearDown();
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
