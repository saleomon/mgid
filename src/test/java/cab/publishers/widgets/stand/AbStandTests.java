package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.*;

public class AbStandTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Feature("AB tests")
    @Story("Не применяются стили на чайлде")
    @Description("change child styles for Ab tests <a href='https://jira.mgid.com/browse/TA-52090'>TA-52090</a> \n" +
            "<ul>\n" +
                "<li>simple AB test</li>\n" +
                "<li>subwidget(tc_relations) who has AB test</li>\n" +
            "</ul>")
    @Test(dataProvider = "dataChangeChildAbTestStyle", description = "change child styles for Ab tests")
    public void changeChildAbTestStyle(String name, int parentAb, int childAb, String stand) {
        log.info("Test is started: " + name);

        log.info("unblock child Ab widget");
        if(stand.equals("relationsAbTestChangeChildStyle")){
            authCabAndGo(compositeEditUrl + 427);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();
        }
        else {
            authCabAndGo(widgetsCustomIdUrl + childAb);
            pagesInit.getCabWidgetsList().clickBlockUnblockWidget(childAb);
        }

        log.info("go to stand + and set childId to servicer response");
        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(childAb)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("check default css styles for childId");
        softAssert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getTitleStyle("font-size"), "16px", "FAIL: TITLE(.mctitle>a) -> font-size (without change style)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgHeadStyle("font-size"), "17px", "FAIL: HEAD(.mghead) -> font-size (without change style)");

        log.info("edit child css styles in cab");
        authCabAndGo(editCodeUrl + childAb);
        pagesInit.getCabEditCode().switchToCssRadio();
        pagesInit.getCabEditCode().changeStyle(".mctitle", "2", "14");
        pagesInit.getCabEditCode().changeStyle(".mghead", "3", "19");
        pagesInit.getCabEditCode().saveEditCodeInterface();

        log.info("block child Ab widget + re-save parent widget + unblock child Ab widget");
        authCabAndGo(widgetsCustomIdUrl + childAb);
        pagesInit.getCabWidgetsList().clickBlockUnblockWidget(childAb);
        authCabAndGo(compositeEditUrl + parentAb);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        authCabAndGo(widgetsCustomIdUrl + childAb);
        pagesInit.getCabWidgetsList().clickBlockUnblockWidget(childAb);
        if(stand.equals("relationsAbTestChangeChildStyle")){
            authCabAndGo(compositeEditUrl + 427);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();
        }

        log.info("get child Ab widget: tickers_composite.template and tickers_composite.style");
        String t = operationMySql.getTickersComposite().getTemplate(childAb)
                .replaceAll("\"", "\\\\\"")
                .replaceAll("\n", "n")
                .replaceAll("\r", "\\\\");
        String css = operationMySql.getTickersComposite().getStyles(childAb)
                .replaceAll("\"", "\\\\\"")
                .replaceAll("\n", "n")
                .replaceAll("\r", "\\\\");

        log.info("go to stand + and set childId and custom response data(tickers_composite.template and tickers_composite.style) to servicer response");
        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(childAb)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"t\":\"" + t + "\",\"css\":\"" + css + "\"")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("check new childId styles");
        softAssert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getTitleStyle("font-size"), "14px", "FAIL: TITLE(.mctitle>a) -> font-size (with change style)");
        softAssert.assertEquals(pagesInit.getWidgetClass().getMgHeadStyle("font-size"), "19px", "FAIL: HEAD(.mghead) -> font-size (with change style)");
        softAssert.assertAll();
        log.info("Test is finished: " + name);
    }

    @DataProvider
    public Object[][] dataChangeChildAbTestStyle(){
        return new Object[][]{
                {"simple AB test",                          425, 426, "abTestChangeChildStyle"},
                {"subwidget(tc_relations) who has AB test", 428, 429, "relationsAbTestChangeChildStyle"}
        };
    }
}
