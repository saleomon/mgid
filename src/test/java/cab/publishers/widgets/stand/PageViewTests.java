package cab.publishers.widgets.stand;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.ampTemplateUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

/*
 * https://youtrack.mgid.com/issue/VT-27146
 * https://youtrack.mgid.com/issue/VT-27168
 * https://youtrack.mgid.com/issue/VT-27358
 * https://youtrack.mgid.com/issue/VT-27431
 */
public class PageViewTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    public String setAmpStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @BeforeClass
    public void reSaveWidget(){
        authCabAndGo("wages/informers-edit/type/composite/id/" + 235);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 236);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 237);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 239);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 531);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 532);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 533);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authCabAndGo("wages/informers-edit/type/composite/id/" + 534);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        authDashAndGo("testEmail52@ex.ua", "publisher/edit-widget/id/" + 238);
        pagesInit.getWidgetClass()
                .setAutoplacement("top")
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_MAIN);
        pagesInit.getWidgetClass().saveWidgetSettings();
    }

    /**
     * check IA code for same widget -> one site
     * check IA code for same widget -> one site (autoplacement)
     * check IA code for same widget -> one site (Lazy Loading)
     */
    @Test(dataProvider = "pageView_IaCode")
    public void pageView_IaCodeForBothOneSite(String description, int widget_1, int widget_2, String stand) {
        int siteId = 80;
        String localStorage_1, localStorage_2, localStorage_3;
        String pvid_widget_1, pvid_widget_2;

        log.info("Test is started: " + description);

        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(widget_1, widget_2)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_1 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        pvid_widget_1 = serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid");
        pvid_widget_2 = serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid");
        softAssert.assertTrue(comparePageView(widget_1, widget_2), "FAIL -> stage#1");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(siteId), "FAIL -> stage#1 check data in c.mgid.com");
        softAssert.assertEquals(pvid_widget_1, pvid_widget_2, "FAIL -> stage#1 pvid");

        sleep(5000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .setServicerCountDown(2)
                .clearServicerMockData()
                .refresh()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_2 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "0", "FAIL -> stage#2 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "0", "FAIL -> stage#2 widget_2");
        softAssert.assertEquals(localStorage_1, localStorage_2, "FAIL -> stage#2 localStorage");
        softAssert.assertFalse(serviceInit.getServicerMock().getRequestList().contains("c.mgid.com"), "FAIL -> stage#2 check data in c.mgid.com");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1, "FAIL -> stage#2 pvid_widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid"), pvid_widget_1, "FAIL -> stage#2 pvid_widget_2");

        sleep(11000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .setServicerCountDown(2)
                .clearServicerMockData()
                .refresh()
                .countDownLatchAwait();

        localStorage_3 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        softAssert.assertTrue(comparePageView(widget_1, widget_2), "FAIL -> stage#3");
        softAssert.assertNotEquals(localStorage_3, localStorage_2, "FAIL -> stage#3 localStorage");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(siteId), "FAIL -> stage#3 check data in c.mgid.com");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1, "FAIL -> stage#3 pvid_widget_1");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid"), pvid_widget_1, "FAIL -> stage#3 pvid_widget_2");
        softAssert.assertAll();
        log.info("Test is finished: " + description);
    }

    @DataProvider
    public Object[][] pageView_IaCode(){
        return new Object[][]{
                {"IaCodeForBothWidgets", 235, 236, "page_view_IaCodeForBoth"},
                {"IaCodeOneWidgetWithAutoplacement", 235, 238, "page_view_with_autoplacement"},
                {"IaCodeOneWidgetWithLazyLoading", 235, 239, "page_view_with_lazy_load"}
        };
    }

    /**
     * check IA code And Simple code
     */
    @Test
    public void pageView_IaCodeAndSimpleCode() {
        int widget_1 = 235, widget_2 = 236; int siteId = 80;
        String localStorage_1, localStorage_2, localStorage_3;
        String pvid_widget_1, pvid_widget_2, pvid_widget_1_stage2, pvid_widget_2_stage2;

        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("page_view_IaCodeAndSimpleCode"))
                .setWidgetIds(widget_1, widget_2)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_1 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        pvid_widget_1 = serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid");
        pvid_widget_2 = serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "1", "FAIL -> stage#1 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "1", "FAIL -> stage#1 widget_2");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(siteId), "FAIL -> stage#1 check data in c.mgid.com for site=80");
        softAssert.assertEquals(serviceInit.getServicerMock().getCountCurrentRequest("https://c.mgid.com/pv/"), 1, "FAIL -> stage#1 count size c.mgid.com for site=80");
        softAssert.assertNotEquals(pvid_widget_1, pvid_widget_2, "FAIL -> stage#1 pvid");

        sleep(5000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .clearServicerMockData()
                .setServicerCountDown(2)
                .refresh()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_2 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        pvid_widget_1_stage2 = serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid");
        pvid_widget_2_stage2 = serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "0", "FAIL -> stage#2 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "1", "FAIL -> stage#2 widget_2");
        softAssert.assertEquals(localStorage_1, localStorage_2, "FAIL -> stage#2 localStorage");
        // don't correct case (0/1) softAssert.assertEquals(serviceInit.getServicerMock().getCountCurrentRequest("https://c.mgid.com/pv/"), 1, "FAIL -> stage#2 count size c.mgid.com for site=80");
        //softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1, "FAIL -> stage#2 pvid_widget_1");
        softAssert.assertNotEquals(pvid_widget_2_stage2, pvid_widget_2, "FAIL -> stage#2 pvid_widget_2");

        sleep(11000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .clearServicerMockData()
                .setServicerCountDown(2)
                .refresh()
                .countDownLatchAwait();

        localStorage_3 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "1", "FAIL -> stage#3 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "1", "FAIL -> stage#3 widget_2");
        softAssert.assertNotEquals(localStorage_3, localStorage_2, "FAIL -> stage#3 localStorage");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(siteId), "FAIL -> stage#3 check data in c.mgid.com for site=80");
        softAssert.assertEquals(serviceInit.getServicerMock().getCountCurrentRequest("https://c.mgid.com/pv/"), 1, "FAIL -> stage#3 count size c.mgid.com for site=80");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1_stage2, "FAIL -> stage#3 pvid_widget_1");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid"), pvid_widget_2_stage2, "FAIL -> stage#3 pvid_widget_2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check IA code for same widgets -> Different Site
     */
    @Test
    public void pageView_IaCodeForDifferentSite() {
        int widget_1 = 235, widget_2 = 237;
        String localStorage_site80_stage1, localStorage_site80_stage2, localStorage_site80_stage3;
        String localStorage_site76_stage1, localStorage_site76_stage2, localStorage_site76_stage3;
        String pvid_widget_1, pvid_widget_2;

        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("page_view_IaCodeForDifferentSites"))
                .setWidgetIds(widget_1, widget_2)
                .setSubnetType(subnetId)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_site80_stage1 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        localStorage_site76_stage1 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView76");
        pvid_widget_1 = serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid");
        pvid_widget_2 = serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "1", "FAIL -> stage#1 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "1", "FAIL -> stage#1 widget_2");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(80), "FAIL -> stage#1 check data in c.mgid.com for site=80");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(76), "FAIL -> stage#1 check data in c.mgid.com for site=76");
        softAssert.assertEquals(pvid_widget_1, pvid_widget_2, "FAIL -> stage#1 pvid");

        sleep(5000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .clearServicerMockData()
                .setServicerCountDown(2)
                .refresh()
                .countDownLatchAwaitWithoutTearDown();

        localStorage_site80_stage2 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        localStorage_site76_stage2 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView76");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "0", "FAIL -> stage#2 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "0", "FAIL -> stage#2 widget_2");
        softAssert.assertEquals(localStorage_site80_stage1, localStorage_site80_stage2, "FAIL -> stage#2 localStorage siteId=80");
        softAssert.assertEquals(localStorage_site76_stage1, localStorage_site76_stage2, "FAIL -> stage#2 localStorage siteId=76");
        softAssert.assertFalse(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(80), "FAIL -> stage#3 check data in c.mgid.com for site=80");
        softAssert.assertFalse(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(76), "FAIL -> stage#3 check data in c.mgid.com for site=76");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1, "FAIL -> stage#2 pvid_widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid"), pvid_widget_2, "FAIL -> stage#2 pvid_widget_2");

        sleep(11000);

        serviceInit.getServicerMock()
                .setWidgetIds(widget_1, widget_2)
                .clearServicerMockData()
                .setServicerCountDown(2)
                .refresh()
                .countDownLatchAwait();

        localStorage_site80_stage3 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView80");
        localStorage_site76_stage3 = serviceInit.getServicerMock().getLocalStorage().get("_mgPageView76");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView"), "1", "FAIL -> stage#3 widget_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView"), "1", "FAIL -> stage#3 widget_2");
        softAssert.assertNotEquals(localStorage_site80_stage3, localStorage_site80_stage2, "FAIL -> stage#3 localStorage siteId=80");
        softAssert.assertNotEquals(localStorage_site76_stage3, localStorage_site76_stage2, "FAIL -> stage#3 localStorage siteId=76");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(80), "FAIL -> stage#3 check data in c.mgid.com for site=80");
        softAssert.assertTrue(serviceInit.getServicerMock().modifyAndCheckServicerRequestWithCmgidRequest(76), "FAIL -> stage#3 check data in c.mgid.com for site=76");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_1, "pvid"), pvid_widget_1, "FAIL -> stage#3 pvid_widget_1");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widget_2, "pvid"), pvid_widget_2, "FAIL -> stage#3 pvid_widget_2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    //@Test
    public void pageView_1_Amp() {
        log.info("Test is started");
        int widgetId_1 = 531;
        int widgetId_2 = 532;

        serviceInit.getServicerMock().setStandName(setStand("mgid-pageImpl_1"))
                .setWidgetIds(widgetId_1)
                .setServicerCountDown(4)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait();

        String pageView_1 = serviceInit.getServicerMock().parseServicerRequest(widgetId_1, "pageView");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId_2, "pageView"), pageView_1, "FAIL -> pageView#2");

        softAssert.assertEquals(serviceInit.getServicerMock().parseCMgidComCRequest(widgetId_1, "pageImp"), "1", "FAIL -> pageImp#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseCMgidComCRequest(widgetId_2, "pageImp"), "0", "FAIL -> pageImp#2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    //@Test
    public void pageView_0_Amp() {
        log.info("Test is started");
        int widgetId_1 = 533;
        int widgetId_2 = 534;

        serviceInit.getServicerMock().setStandName(setAmpStand("mgid-pageImpl_0"))
                .setWidgetIds(widgetId_1)
                .setServicerCountDown(4)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait();

        String pageView_1 = serviceInit.getServicerMock().parseServicerRequest(widgetId_1, "pageView");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId_2, "pageView"), pageView_1, "FAIL -> pageView#2");

        softAssert.assertEquals(serviceInit.getServicerMock().parseCMgidComCRequest(widgetId_1, "pageImp"), "1", "FAIL -> pageImp#1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseCMgidComCRequest(widgetId_2, "pageImp"), "0", "FAIL -> pageImp#2");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    public boolean comparePageView(int widget_1, int widget_2){
        String pageView_1 = serviceInit.getServicerMock().parseServicerRequest(widget_1, "pageView");
        String pageView_2 = serviceInit.getServicerMock().parseServicerRequest(widget_2, "pageView");

        return !pageView_1.equals(pageView_2);
    }
}
