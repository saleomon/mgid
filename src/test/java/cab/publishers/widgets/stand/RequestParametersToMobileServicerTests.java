package cab.publishers.widgets.stand;

import libs.devtools.MobileEmulationSettings;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.widgetTemplateUrl;

public class RequestParametersToMobileServicerTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }


    @BeforeClass
    public void reSaveWidget(){
        authCabAndGo("wages/informers-edit/type/composite/id/" + 135);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
    }

    @Feature("Request parameters to servicer")
    @Story("Mobile device")
    @Description("Check passing parameters: dpr (request to servicer)\n" +
            "devices: iPad/Galaxy S5/Pixel 2/Pixel 2 XL/Surface Duo")
    @Test(dataProvider = "dataDpr")
    public void requestParametersToServicerDpr(MobileEmulationSettings.Device device, String dpr) {
        int widgetId = 135;
        log.info("Test is started");
        mobileEmulationSettings.setDefaultDevice(device);

        serviceInit.getServicerMock().setStandName(setStand("mgid_cxurl_ogUrl"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setMobileEmulation(true)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "dpr"), dpr);
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataDpr(){
        return new Object[][]{
                { MobileEmulationSettings.Device.NEXUS_S,        "1.500" },
                { MobileEmulationSettings.Device.GALAXY_S4,      "3" },
                { MobileEmulationSettings.Device.IPHONE_6,       "2" },
                { MobileEmulationSettings.Device.LG_OPTIMUS_LTE, "1.700" },
        };
    }

}
