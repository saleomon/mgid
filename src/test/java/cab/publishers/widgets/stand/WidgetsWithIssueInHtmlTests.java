package cab.publishers.widgets.stand;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.testng.Assert;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static com.codeborne.selenide.Selenide.*;
import static testData.project.EndPoints.widgetTemplateUrl;

public class WidgetsWithIssueInHtmlTests extends TestBase {
    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");

    private String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    /**
     * Форма для проверки работоспособности виджетов
     * Проверка работы виджета с подстановкой(моком) выдачи в html stand
     * @see <a href="https://jira.mgid.com/browse/TA-27395">TA-27395</a>
     * <p>RKO</p>
     */
    @Test
    public void widgetWithIssueInHtmlCode() {
        int widgetId = 242;
        log.info("Test is started");
        authDashAndGo("testEmail53@ex.ua", "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().saveWidgetSettings();
        open(setStand("mgid_widget_with_issue"));
        pagesInit.getWidgetClass().hoverToFooterStand();
        sleep(1000);
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("widgetWithIssueInHtmlCode.png")
                ));
        log.info("Test is finished");
    }

    /**
     * Форма для проверки работоспособности виджетов
     * Проверка работы виджета с подстановкой(моком) выдачи и баннера в html stand
     * @see <a href="https://jira.mgid.com/browse/TA-27395">TA-27395</a>
     * <p>RKO</p>
     */
    @Test
    public void widgetWithIssueAndBannerInHtmlCode() {
        int widgetId = 243;
        log.info("Test is started");
        authDashAndGo("testEmail53@ex.ua", "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .saveWidgetSettings();
        Assert.assertTrue(widgetWithIssueAndBannerInHtmlCodeHelper());
        log.info("Test is finished");
    }

    public boolean widgetWithIssueAndBannerInHtmlCodeHelper(){
        int count = 0;
        boolean result;
        do {
            open(setStand("mgid_widget_with_issue_and_banner"));
            pagesInit.getWidgetClass().hoverToFooterStand();
            sleep(1000);
            result = serviceInit.getScreenshotService().compareScreenshots(
                    serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                    serviceInit.getScreenshotService().getExpectedScreenshot("widgetWithIssueAndBannerInHtmlCode.png"));
            log.info("widgetWithIssueAndBannerInHtmlCodeHelper: " + count);
            count++;
        }
        while (!result && count < 5);

        return result;
    }
}
