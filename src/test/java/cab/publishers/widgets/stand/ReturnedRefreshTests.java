package cab.publishers.widgets.stand;

import com.codeborne.selenide.WebDriverRunner;
import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * <p>RKO</p>
 * @see <a href="https://jira.mgid.com/browse/TA-27365">TA-27365</a>
 */
public class ReturnedRefreshTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    /**
     * tickers_composite.returned_refresh = 1 or 2
     */
    @Test(dataProvider = "returned_refresh_data")
    public void requestParametersToServicer_returned_refresh(int widgetId, String rsh, String standName) {
        log.info("Test is started");

        //mock response
        serviceInit.getServicerMock().setStandName(setStand(standName))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        // prepare mock for next response after click
        serviceInit.getServicerMock()
                .clearServicerMockData()
                .countDownLatchAwaitWithoutTearDown();

        ArrayList<String> tabs = new ArrayList<>(WebDriverRunner.getWebDriver().getWindowHandles());
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        WebDriverRunner.getWebDriver().switchTo().window(tabs.get(0));
        sleep(500);
        serviceInit.getServicerMock().tearDown();

        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rsh"), rsh, "FAIL -> rsh");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "initial_composite"), String.valueOf(widgetId), "FAIL -> initial_composite");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] returned_refresh_data(){
        return new Object[][]{
                {261, "2", "mgid_returned_refresh"},
                {326, "1", "mgid_returned_refresh_326"}
        };
    }

    /**
     * tickers_composite.returned_refresh = 0
     */
    @Test
    public void requestParametersToServicer_returned_refresh_0() {
        int widgetId = 327;
        log.info("Test is started");

        //mock response
        serviceInit.getServicerMock().setStandName(setStand("mgid_returned_refresh_0"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        // prepare mock for next response after click
        serviceInit.getServicerMock()
                .countDownLatchAwaitWithoutTearDown();

        ArrayList<String> tabs = new ArrayList<>(WebDriverRunner.getWebDriver().getWindowHandles());
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        WebDriverRunner.getWebDriver().switchTo().window(tabs.get(0));
        sleep(500);
        serviceInit.getServicerMock().tearDown();

        Assert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> count servicer");

        log.info("Test is finished");
    }

    /**
     * tickers_composite.returned_refresh tests on parent/child -> AB and relations
     * equals value for both
     */
    @Test(dataProvider = "childsTests")
    public void requestParametersToServicer_returned_refresh_Childs_1(String rsh, int widgetIdForResponse, String stand) {
        log.info("Test is started");

        //mock response
        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(widgetIdForResponse)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        // prepare mock for next response after click on news
        serviceInit.getServicerMock()
                .clearServicerMockData()
                .countDownLatchAwaitWithoutTearDown();

        ArrayList<String> tabs = new ArrayList<>(WebDriverRunner.getWebDriver().getWindowHandles());
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        WebDriverRunner.getWebDriver().switchTo().window(tabs.get(0));
        sleep(500);
        serviceInit.getServicerMock().tearDown();

        // check base param after change news
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "rsh"), rsh, "FAIL -> rsh");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "initial_composite"), String.valueOf(widgetIdForResponse), "FAIL -> initial_composite");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] childsTests(){
        int parentRelationsId = 262;
        int childRelationsId = 263;
        int parentRelationsId_2 = 328;
        int childRelationsId_2 = 329;
        int parentAbTestId = 264;
        int childAbTestId = 265;
        int parentAbTestId_2 = 330;
        int childAbTestId_2 = 331;
        return new Object[][]{
                {"2", parentRelationsId,    "mgid_returned_refresh_relations"},
                {"2", childRelationsId,     "mgid_returned_refresh_relations"},
                {"2", parentAbTestId,       "mgid_returned_refresh_abtest"},
                {"2", childAbTestId,        "mgid_returned_refresh_abtest"},
                {"1", parentRelationsId_2,  "mgid_returned_refresh_relations_2"},
                {"1", childRelationsId_2,   "mgid_returned_refresh_relations_2"},
                {"1", parentAbTestId_2,     "mgid_returned_refresh_abtest_2"},
                {"1", childAbTestId_2,      "mgid_returned_refresh_abtest_2"},
        };
    }

    /**
     * tickers_composite.returned_refresh tests on parent/child -> AB and relations
     * different value for both
     */
    @Test(dataProvider = "childsTests2")
    public void requestParametersToServicer_returned_refresh_Childs_2(String rsh,
                                                                      int widgetIdForResponse, String stand) {
        log.info("Test is started");

        //mock response
        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(widgetIdForResponse)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        // prepare mock for next response after click on news
        serviceInit.getServicerMock()
                .clearServicerMockData()
                .countDownLatchAwaitWithoutTearDown();

        ArrayList<String> tabs = new ArrayList<>(WebDriverRunner.getWebDriver().getWindowHandles());
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        WebDriverRunner.getWebDriver().switchTo().window(tabs.get(0));
        sleep(500);
        serviceInit.getServicerMock().tearDown();

        // check base param after change news
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "rsh"), rsh, "FAIL -> rsh");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "initial_composite"), String.valueOf(widgetIdForResponse), "FAIL -> initial_composite");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] childsTests2(){
        return new Object[][]{
                {"2",  332,  "mgid_returned_refresh_relations_1_0"},
                {"1",  335,  "mgid_returned_refresh_relations_0_2"},
                {"1",  336,  "mgid_returned_refresh_abtest_2_0"},
                {"2",  339,  "mgid_returned_refresh_abtest_0_1"},
                {"1",  340,  "mgid_returned_refresh_relations_2_1"},
                {"1",  343,  "mgid_returned_refresh_relations_1_2"},
                {"2",  344,  "mgid_returned_refresh_abtest_1_2"},
                {"2",  347,  "mgid_returned_refresh_abtest_2_1"},
        };
    }

    /**
     * tickers_composite.returned_refresh tests on parent/child -> relations
     * doubleclick
     */
    @Test(dataProvider = "childs_doubleclick")
    public void requestParametersToServicer_doubleclick(String rsh, int widgetIdForResponse, String stand) {
        log.info("Test is started");

        //mock response
        serviceInit.getServicerMock().setStandName(setStand(stand))
                .setWidgetIds(widgetIdForResponse)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        // prepare mock for next response after click on news
        serviceInit.getServicerMock()
                .clearServicerMockData()
                .countDownLatchAwaitWithoutTearDown();

        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        log.info("ждём 2с и кликаем по 2-ому месту в виджете и проверяем что на нём появляется блюр на тизере и кнопка GO!");
        sleep(1000);
        pagesInit.getWidgetClass().clickTeaserOnStand(2);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(2), "FAIL -> teaser 2: doubleClick show");

        sleep(2000);
        ArrayList<String> tabs = new ArrayList<>(WebDriverRunner.getWebDriver().getWindowHandles());
        pagesInit.getWidgetClass().clickTeaserOnStand(5);
        WebDriverRunner.getWebDriver().switchTo().window(tabs.get(0));
        sleep(500);
        serviceInit.getServicerMock().tearDown();

        // check base param after change news
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "rsh"), rsh, "FAIL -> rsh");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetIdForResponse, "initial_composite"), String.valueOf(widgetIdForResponse), "FAIL -> initial_composite");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] childs_doubleclick(){
        int parentDoubleClickId = 266;
        int childDoubleClickId = 267;
        return new Object[][]{
                {"2", parentDoubleClickId,  "mgid_returned_refresh_doubleclick"},
                {"2", childDoubleClickId,   "mgid_returned_refresh_doubleclick"}
        };
    }
}
