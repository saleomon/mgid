package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.url;
import static testData.project.EndPoints.widgetTemplateUrl;

public class ClickUrlParamsTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    private final List<String> clickUrlParamsCeMuid = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион\",\"Надоело жить в нищете? Хотите получить премию? Узнай как заработать больше и как себе ни в чём не отказывать\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"dpd\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser Altz rsd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PA\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser Altz dom\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PB\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"i\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser Altz ltg\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PC\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser Altz mqa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PD\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"dpd\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser Altz dtz\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PE\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//click.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"dpd\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    @Feature("clicks URL params")
    @Story("direct publisher demand")
    @Description("Check clicks params: ce/miud on different type (servicer response): w/e/i/dpd <a href='https://jira.mgid.com/browse/VT-28195'>VT-28195</a>")
    @Test(dataProvider = "dataClickUrlParamsCeMuid", description = "Check clicks params: ce/miud on different type (servicer response): w/e/i/dpd")
    public void clickUrlParamsCeMuid(int placeNumber, String ce, String muid) {
        int widgetId = 430;
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("clickUrlParamsCeMuid"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(clickUrlParamsCeMuid)
                .setMuid(muid)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();

        log.info("click placement and move to new window and get params from url");
        pagesInit.getWidgetClass().setShadowDom(true).clickTeaserOnStand(placeNumber);
        if(placeNumber != 3) switchTo().window(1);
        String url = url();
        log.info("URL: " + url);

        softAssert.assertEquals(serviceInit.getServicerMock().parseClickUrlRequest(url, "ce"), ce, "FAIL -> ce");
        softAssert.assertEquals(serviceInit.getServicerMock().parseClickUrlRequest(url, "muid"), muid, "FAIL -> muid");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataClickUrlParamsCeMuid(){
        String muid = "m3izOyXvA4U1";
        return new Object[][]{
                {1, null,        null},
                {2, "IMG.mcimg", muid},
                {3, null,        null},
                {4, "IMG.mcimg", muid}
        };
    }
}
