package cab.publishers.widgets.stand;

import libs.devtools.ServicerMock;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.Collections;

import static com.codeborne.selenide.Selenide.$x;
import static testData.project.EndPoints.widgetTemplateUrl;

/*
 * https://youtrack.mgid.com/issue/TA-27310
 * https://youtrack.mgid.com/issue/TA-27420
 * RKO
 */
public class ImptTrackerFromServicerTests extends TestBase {

    private final String login = "testEmail41@ex.ua";
    private final int widgetId = 137;

    /*
     * %s:
     * 1 - type
     * 2 - code
     * 3 - only_viewable
     * 4 - iframe
     */
    private final String bodyResponse = "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Юная миллионерша из города Киев проболталась, как разбогатела, как купила бентли, как заработала первый миллион\",\"\",\"0\",\"\",\"\",\"\",\"NFrL-mThL2IxdAKVUvw9yrMAb93i1wJ-pIQpthGvxtHkSMvnEHo8GwC9c2XrRV6r\",{\n" +
            "\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\n" +
            "\"l\":\"//www.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\n" +
            "\"adc\":[],\n" +
            "\"sdl\":0,\n" +
            "\"dl\":\"\",\n" +
            "\"type\":\"w\",\n" +
            "\"clicktrackers\":[],\n" +
            "\"cta\": \"\"," +
            "\"extimg\": 1," +
            "\"impt\": " +
            "[{" +
            "\"type\":\"%s\"," +
            "\"code\":\"%s\"," +
            "\"only_viewable\":%s," +
            "\"iframe\":%s" +
            "}],\n" +
            "\"cta\":\"Learn more\"" +
            "}]";

    private String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeClass
    public void reSaveWidget(){
        authDashAndGo(login, "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.IN_ARTICLE, WidgetTypes.SubTypes.IN_ARTICLE_IMPACT);
    }

    @Test(dataProvider = "impt_type_i_frame")
    public void imptFromServicer_type_i(int only_viewable, int frame) {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "i",
                                        "http://testimpt.com",
                                        only_viewable,
                                        frame)
                        )
                )
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait("http://testimpt.com/?mgbuster=");

        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("http://testimpt.com/?mgbuster=")), "FAIL -> request list doesn't contains http://testimpt.com/?mgbuster=");
        softAssert.assertFalse($x(".//*[contains(text(), 'http://testimpt.com')]").exists(), "FAIL -> DOM element");
        softAssert.assertFalse($x(".//iframe[contains(@srcdoc, 'http://testimpt.com')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] impt_type_i_frame(){
        return new Object[][]{
                {0, 0},
                {0, 1},
                {1, 0},
                {1, 1}
        };
    }

    @Test
    public void imptFromServicer_type_i_withoutScroll() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "i",
                                        "http://testimpt.com",
                                        1,
                                        1
                                )
                        )
                )
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertFalse(serviceInit.getServicerMock().getRequestList().stream().anyMatch(i -> i.contains("http://testimpt.com%26mgbuster%")));
        softAssert.assertFalse($x(".//*[contains(text(), 'http://testimpt.com')]").exists(), "FAIL -> DOM element");
        softAssert.assertFalse($x(".//iframe[contains(@srcdoc, 'http://testimpt.com')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void imptFromServicer_type_j_onlyViewable_1_frame_1() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "j",
                                        "<script async src='https://new.fqtag.com/implement.js'></script>",
                                        1,
                                        1
                                )
                        )
                )
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().contains("https://new.fqtag.com/implement.js"), "FAIL -> request");
        softAssert.assertFalse($x(".//div[contains(@class, 'mgline')]/script[contains(@src, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> DOM element");
        softAssert.assertTrue($x(".//iframe[contains(@srcdoc, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void imptFromServicer_type_j_onlyViewable_1_frame_1_withoutScroll() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "j",
                                        "<script async src='https://new.fqtag.com/implement.js'></script>",
                                        1,
                                        1)
                        )
                )
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertFalse(serviceInit.getServicerMock().getRequestList().contains("https://new.fqtag.com/implement.js"), "FAIL -> request");
        softAssert.assertFalse($x(".//div[contains(@class, 'mgline')]/script[contains(@src, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> DOM element");
        softAssert.assertFalse($x(".//iframe[contains(@srcdoc, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void imptFromServicer_type_j_onlyViewable_0_frame_0() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "j",
                                        "<script async src='https://new.fqtag.com/implement.js'></script>",
                                        0,
                                        0)
                        )
                )
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().contains("https://new.fqtag.com/implement.js"), "FAIL -> request");
        softAssert.assertTrue($x(".//div[contains(@class, 'mgline')]/script[contains(@src, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> DOM element");
        softAssert.assertFalse($x(".//iframe[contains(@srcdoc, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void imptFromServicer_type_j_onlyViewable_0_frame_1() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "j",
                                        "<script async src='https://new.fqtag.com/implement.js'></script>",
                                        0,
                                        1)
                        )
                )
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().contains("https://new.fqtag.com/implement.js"), "FAIL -> request");
        softAssert.assertFalse($x(".//div[contains(@class, 'mgline')]/script[contains(@src, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> DOM element");
        softAssert.assertTrue($x(".//iframe[contains(@srcdoc, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Test
    public void imptFromServicer_type_j_onlyViewable_1_frame_0() {
        log.info("Test is started");
        serviceInit.getServicerMock().setStandName(setStand("mgid_impt"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(
                        Collections.singletonList(
                                String.format(bodyResponse,
                                        "j",
                                        "<script async src='https://new.fqtag.com/implement.js'></script>",
                                        1,
                                        0)
                        )
                )
                .interceptionNetworkAndMockThem()
                .scrollToSlow()
                .countDownLatchAwait("https://new.fqtag.com/implement.js");

        softAssert.assertTrue(serviceInit.getServicerMock().getRequestList().contains("https://new.fqtag.com/implement.js"), "FAIL -> request");
        softAssert.assertTrue($x(".//div[contains(@class, 'mgline')]/script[contains(@src, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> DOM element");
        softAssert.assertFalse($x(".//iframe[contains(@srcdoc, 'https://new.fqtag.com/implement.js')]").exists(), "FAIL -> iframe element");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
