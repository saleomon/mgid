package cab.publishers.widgets.stand;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.Subnets;

import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.EndPoints.widgetTemplateUrl;


/*
 * - implVersion
 * - uniqId
 */
public class WidgetsOnStandMgidOtherTests extends TestBase {

    public WidgetsOnStandMgidOtherTests() {
        subnetId = Subnets.SubnetType.SCENARIO_MGID;
    }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("publisher/edit-widget/id/" + widgetId);
    }

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    /**
     * Instant Articles code (servicerRequest has param implVersion=14)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24450">TA-24450</a>
     */
    @Test
    public void servicerRequestImplVersion() {
        log.info("Test is started");
        log.info("re-save first widget with Instant Articles code");
        int widgetUnderArticleId = 54;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetUnderArticleId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        int widgetInstantArticlesId = 50;
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetInstantArticlesId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("mgid_instant_articles_code"))
                .setWidgetIds(widgetUnderArticleId, widgetInstantArticlesId)
                .setServicerCountDown(2)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("check implVersion param in servicer request");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetUnderArticleId, "implVersion"), "11", "FAIL -> implVersion 11");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetInstantArticlesId, "implVersion"), "14", "FAIL -> implVersion 14");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Place multiple times on page
     * allow_multiple_widgets=1 (розміщуємо на стенді 2 віджети: повинно бути 2 request on servicer and param uniqId)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24360">TA-24360</a>
     */
    @Test
    public void placeMultipleTimesOnPage_true(){
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + 39);
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("place_on_multiple_times"))
                .setWidgetIds(39)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("size: " + serviceInit.getServicerMock().getRequestsMap().size());
        String uniqId_1 = serviceInit.getServicerMock().getRequestsMap().keySet().toArray()[0].toString();
        String uniqId_2 = serviceInit.getServicerMock().getRequestsMap().keySet().toArray()[1].toString();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> size != 2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(uniqId_1, "uniqId"), uniqId_1, "FAIL -> uniqId_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(uniqId_2, "uniqId"), uniqId_2, "FAIL -> uniqId_2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Place multiple times on page
     * allow_multiple_widgets=0 (розміщуємо на стенді 2 віджети: повинно бути 1 request on servicer and without param uniqId)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24360">TA-24360</a>
     */
    @Test
    public void placeMultipleTimesOnPage_false(){
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + 39);
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("place_on_multiple_times"))
                .setWidgetIds(39)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("size: " + serviceInit.getServicerMock().getRequestsMap().size());
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> size != 1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(39, "uniqId"), null, "FAIL -> uniqId is has");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Place multiple times on page
     * allow_multiple_widgets=1 (розміщуємо на стенді 2 віджети: повинно бути 2 request on servicer and param uniqId)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24360">TA-24360</a>
     */
    @Test
    public void placeMultipleTimesOnPageShadow_true(){
        int widgetId = 193;
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("place_on_multiple_times_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("size: " + serviceInit.getServicerMock().getRequestsMap().size());
        String uniqId_1 = serviceInit.getServicerMock().getRequestsMap().keySet().toArray()[0].toString();
        String uniqId_2 = serviceInit.getServicerMock().getRequestsMap().keySet().toArray()[1].toString();
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 2, "FAIL -> size != 2");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(uniqId_1, "uniqId"), uniqId_1, "FAIL -> uniqId_1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(uniqId_2, "uniqId"), uniqId_2, "FAIL -> uniqId_2");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Place multiple times on page
     * allow_multiple_widgets=0 (розміщуємо на стенді 2 віджети: повинно бути 1 request on servicer and without param uniqId)
     * <p>RKO</p>
     * @see <a href="https://youtrack.mgid.com/issue/TA-24360">TA-24360</a>
     */
    @Test
    public void placeMultipleTimesOnPageShadow_false(){
        int widgetId = 193;
        log.info("Test is started");
        authCabAndGo("wages/informers-edit/type/composite/id/" + widgetId);
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("place_on_multiple_times_shadow"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("size: " + serviceInit.getServicerMock().getRequestsMap().size());
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestsMap().size(), 1, "FAIL -> size != 1");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "uniqId"), null, "FAIL -> uniqId is has");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
