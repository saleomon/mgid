package cab.publishers.widgets.stand;

import libs.devtools.ServicerMock;
import org.testng.annotations.Test;
import testBase.TestBase;

import static testData.project.EndPoints.widgetTemplateUrl;

public class DefaultJsTests extends TestBase {

    private final int parentId = 97;
    private final int childId  = 98;
    private final int parentAbId = 99;
    private final int childAbId  = 100;

    private final String default_js_parent = "(function(){" +
            "var div = document.createElement(`DIV`);" +
            "div.id = `div_default_js`;" +
            "div.style.cssText = `background: lightgreen;border:5px solid black;display: inline-block;margin: 12px;float: left;position: relative;width: 200px;height: 200px;`;" +
            "document.getElementById(window._mgDefaultJs%s.shift()).appendChild(div);" +
            "})();";

    private final String default_js_child = "(function(){" +
            "var div = document.createElement('DIV');" +
            "div.id = 'div_default_js_child';" +
            "div.style.cssText = 'background: red;border:5px solid black;display: inline-block;margin: 12px;float: left;position: relative;width: 200px;height: 200px;';" +
            "document.getElementById(window._mgDefaultJs%s.shift()).appendChild(div);" +
            "})();";

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * Parent:
     *  - use_default_js=1
     *  @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     *  <p>RKO</p>
     */
    @Test
    public void defaultJs__parent__use_default_js_1() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(true);
        pagesInit.getCabCompositeSettings().fillDefaultJs(String.format(default_js_parent, parentId));
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");


        serviceInit.getServicerMock().setStandName(setStand("default_js"))
                .setWidgetIds(parentId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDefaultJsBlock(), "FAIL -> doesn't work default_js code");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * Parent:
     *  - use_default_js=0
     *  @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     *  <p>RKO</p>
     */
    @Test
    public void defaultJs__parent__use_default_js_0() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + parentId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");


        serviceInit.getServicerMock().setStandName(setStand("default_js"))
                .setWidgetIds(parentId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(5);

        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDefaultJsBlock(), "FAIL -> doesn't work default_js code");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * Child:
     *  - use_default_js=1
     *  @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     *  <p>RKO</p>
     */
    @Test
    public void defaultJs__child__use_default_js_1() {
        log.info("Test is started");

        operationMySql.getTickersComposite().updateDefaultJs("1", String.format(default_js_parent, parentId), parentId);

        //child
        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(true);
        pagesInit.getCabCompositeSettings().fillDefaultJs(String.format(default_js_child, childId));
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");


        serviceInit.getServicerMock().setStandName(setStand("default_js"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDefaultJsBlockChild(), "FAIL -> doesn't work default_js code");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * Child:
     *  - use_default_js=0
     *  @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     *  <p>RKO</p>
     */
    @Test
    public void defaultJs__child__use_default_js_0() {
        log.info("Test is started");

        authCabAndGo("wages/informers-edit/type/composite/id/" + childId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(false);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().isSuccessMessagesCab(), "FAIL -> success message");

        serviceInit.getServicerMock().setStandName(setStand("default_js"))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(2)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait(5);

        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDefaultJsBlockChild(), "FAIL -> doesn't work default_js code");
        softAssert.assertAll();
        log.info("Test is finished");
    }


    //////////////////// place on multiple ////////////////////

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * place multiple times - parent
     * @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     * <p>RKO</p>
     */
    @Test
    public void placeMultipleTimesOnPage_parent(){
        log.info("Test is started");

        operationMySql.getTickersComposite().updateEnabled(0, childAbId);
        authCabAndGo("wages/informers-edit/type/composite/id/" + parentAbId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(true);
        pagesInit.getCabCompositeSettings().fillDefaultJs(String.format(default_js_parent, parentAbId));
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
        operationMySql.getTickersComposite().updateEnabled(1, childAbId);

        serviceInit.getServicerMock().setStandName(setStand("default_js_place_on_multiple"))
                .setWidgetIds(parentAbId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(pagesInit.getWidgetClass().getCountDefaultJsBlock(), 2, "FAIL -> doesn't work default_js code");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Доработать функционал default_js, чтобы работало для чайлдов
     * place multiple times - child
     * @see <a href="https://youtrack.mgid.com/issue/TA-27530">TA-27530</a>
     * <p>RKO</p>
     */
    @Test(dependsOnMethods = "placeMultipleTimesOnPage_parent")
    public void placeMultipleTimesOnPage_child(){
        log.info("Test is started");

        operationMySql.getTickersComposite().updateDefaultJs("1", String.format(default_js_parent, parentAbId), parentAbId);

        authCabAndGo("wages/informers-edit/type/composite/id/" + childAbId);
        pagesInit.getCabCompositeSettings().switchDefaultJs(true);
        pagesInit.getCabCompositeSettings().fillDefaultJs(String.format(default_js_child, childAbId));
        pagesInit.getCabCompositeSettings().switchPlaceMultipleTimesOnPage(true);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("default_js_place_on_multiple"))
                .setWidgetIds(childAbId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseType(ServicerMock.ResponseType.EMPTY_RESPONSE)
                .setServicerCountDown(3)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        softAssert.assertEquals(pagesInit.getWidgetClass().getCountDefaultJsBlockChild(), 2, "FAIL -> doesn't work default_js code");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
