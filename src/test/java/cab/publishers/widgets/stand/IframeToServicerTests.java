package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * <a href="https://youtrack.mgid.com/issue/TA-51668">...</a>
 * RKO
 */
public class IframeToServicerTests extends TestBase {

    private final String login = "testEmail38@ex.ua";

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: iframe(without src) = null; pv=5 <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "Check passing parameters: iframe(without src) = null; pv=5")
    public void requestParametersToServicerIframeNULL() {
        int widgetId = 120;
        log.info("Test is started");
        authDashAndGo(login, "publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_iframe_0"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pv"), "5", "FAIL -> pv");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "iframe"), null, "FAIL -> niet");
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestHeader("Referer"), "http://local-apache.com/", "FAIL -> Referer");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Simple code")
    @Description("Check passing parameters: iframe=1; pv=5; cxurl; ref; pageView=1; pvid!=null; dpr=1 <a href='https://jira.mgid.com/browse/TA-51668'>TA-51668</a>")
    @Test(description = "Check passing parameters: iframe=1; pv=5; cxurl; ref; pageView=1; pvid!=null; dpr=1")
    public void requestParametersToServicerIframe1() {
        int widgetId = 121;
        log.info("Test is started");
        authDashAndGo(login,"publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_iframe_1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pv"), "5", "FAIL -> pv");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ref"), "http%3A%2F%2Flocal-apache.com%2Fstands%2Fmgid_iframe_1.html", "FAIL -> ref");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "cxurl"), "http%3A%2F%2Flocal-apache.com%2Fstands%2Fmgid_iframe_1.html", "FAIL -> niet");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "1", "FAIL -> pageView");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "iframe"), "1", "FAIL -> niet");
        softAssert.assertEquals(serviceInit.getServicerMock().getRequestHeader("Referer"), "http://local-apache.com/", "FAIL -> Referer");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pvid"), null, "FAIL -> pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "dpr"), "1", "FAIL -> dpr");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
