package cab.publishers.widgets.stand;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import libs.devtools.ServicerMock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.widgetDataMgid;
import static testData.project.EndPoints.widgetTemplateUrl;

/**
 * @see <a href="https://jira.mgid.com/browse/TA-51775">TA-51775</a>
 * <p>RKO</p>
 */
public class PaginatorTests extends TestBase {
    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");

    private final List<String> teasersList = Arrays.asList(
            "[\"Campaign PaGe#2\",\"6\",\"1\",\"Teaser PAGE 2 Aa\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign PaGe#2\",\"5\",\"1\",\"Teaser PAGE 2 Bb\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign PaGe#2\",\"4\",\"1\",\"Teaser PAGE 2 Cc\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/4/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign PaGe#2\",\"3\",\"1\",\"Teaser PAGE 2 Dd\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/3/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign PaGe#2\",\"2\",\"1\",\"Teaser PAGE 2 Ee\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/2/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign PaGe#2\",\"1\",\"1\",\"Teaser PAGE 2 Ff\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/mount.jpg\",\"l\":\"//www.mgid.com/ghits/1/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeMethod
    public void initializeVariables(){
        pagesInit.getWidgetClass().getPathToWidgetsJsonData(widgetDataMgid);
    }

    /**
     * Покрыть автотестами отображение виджета с пагинатором "paginator = 1"
     */
    @Test
    public void requestParametersToServicer_rshPagination() {
        int widgetId = 284;
        log.info("Test is started");
        operationMySql.getTickersComposite().updatePaginator(1, widgetId);
        authDashAndGo("testEmail57@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().saveWidgetSettings();
        serviceInit.getServicerMock().setStandName(setStand("mgid_paginator"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        pagesInit.getWidgetClass().hoverToFooterStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                            serviceInit.getScreenshotService().getExpectedScreenshot("requestParametersToServicer_rshPagination.png")), "FAIL -> check screen #1");

        softAssert.assertTrue(pagesInit.getWidgetClass().checkWidgetInStand(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS), "FAIL -> check data page#1");

        serviceInit.getServicerMock()
                .clearServicerMockData()
                .setResponseType(ServicerMock.ResponseType.CUSTOM_BODY)
                .setTeasersForBodyResponse(teasersList);

        pagesInit.getWidgetClass().clickNextPageInPaginatorOnStand();
        pagesInit.getWidgetClass().hoverToHeaderStand();
        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("requestParametersToServicer_rshPagination_1.png")), "FAIL -> check screen #2");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
