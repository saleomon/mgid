package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.ampTemplateUrl;

public class RequestParametersOnAmpToServicerTests extends TestBase {

    public String setStand(String stand){ return String.format(ampTemplateUrl, stand); }

    @Feature("Request parameters to servicer")
    @Story("AMP code")
    @Description("Check passing parameters(enable_source=1): limitads/adb/ad/src_id/rtb_disabled/implVersion/ref/cxurl/iframe/gdprApplies/consentData/pageView/dpr/pvid")
    @Test
    public void requestParametersToServicer_withAdBlock_and_enableSource() {
        int widgetId = 118;
        log.info("Test is started");
        authDashAndGo("testEmail38@ex.ua","publisher/edit-widget/id/" + widgetId);
        pagesInit.getWidgetClass().setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        pagesInit.getWidgetClass().changeDataAndSaveWidget(WidgetTypes.Types.UNDER_ARTICLE, WidgetTypes.SubTypes.UNDER_ARTICLE_CARDS);
        serviceInit.getServicerMock().setStandName(setStand("mgid_widget_with_adblock_and_enableSource"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "limitads"), null, "FAIL -> limitads");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "adb"), "1", "FAIL -> adb");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ad"), "pg,r", "FAIL -> ad");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "src_id"), "1277", "FAIL -> src_id");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "rtb_disabled"), "1", "FAIL -> rtb_disabled");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "implVersion"), "12", "FAIL -> implVersion");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ref"), "http%3A%2F%2Flocal-amp.com%3A8000%2Fexamples%2Fmgid_widget_with_adblock_and_enableSource.html", "FAIL -> ref");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "cxurl"), "https%3A%2F%2Famp.dev%2Fdocumentation%2Fguides-and-tutorials%2Fstart%2Fcreate%2Fbasic_markup%2F", "FAIL -> niet");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "iframe"), "3", "FAIL -> iframe");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "gdprApplies"), null, "FAIL -> gdprApplies");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "consentData"), null, "FAIL -> consentData");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pageView"), "1", "FAIL -> pageView");
        softAssert.assertNotEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "pvid"), null, "FAIL -> pvid");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "dpr"), "1", "FAIL -> dpr");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}
