package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabCompositeSettings;
import testBase.TestBase;
import testData.project.Subnets;

import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.compositeEditUrl;
import static testData.project.EndPoints.widgetTemplateUrl;

public class AdBlockMobile2Tests extends TestBase {
    private final int widgetUnderArticleAdblockId = 448;
    private final int widgetInArticleAdblockId = 469;
    private final int widgetHeaderAdblockId = 484;
    private final int widgetSidebarAdblockId = 493;
    private final int widgetUnderArticleAdblockIdealmediaIoId = 449;
    private final int widgetInArticleAdblockIdealmediaIoId = 470;
    private final int widgetHeaderAdblockIdealmediaIoId = 485;
    private final int widgetSidebarAdblockIdealmediaIoId = 494;
    private static boolean isSavedWidget = false;
    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }


    @BeforeClass
    public void createChildAdBlock(){
        if(!isSavedWidget) {
            authCabAndGo(compositeEditUrl + widgetUnderArticleAdblockId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.UNDER_ARTICLE);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            authCabAndGo(compositeEditUrl + widgetUnderArticleAdblockIdealmediaIoId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.UNDER_ARTICLE);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //in-article
            authCabAndGo(compositeEditUrl + widgetInArticleAdblockId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.IN_ARTICLE);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            authCabAndGo(compositeEditUrl + widgetInArticleAdblockIdealmediaIoId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.IN_ARTICLE);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //header
            authCabAndGo(compositeEditUrl + widgetHeaderAdblockId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.HEADER);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            authCabAndGo(compositeEditUrl + widgetHeaderAdblockIdealmediaIoId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.HEADER);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //sidebar
            authCabAndGo(compositeEditUrl + widgetSidebarAdblockId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.SIDEBAR);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            authCabAndGo(compositeEditUrl + widgetSidebarAdblockIdealmediaIoId);
            pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
            pagesInit.getCabCompositeSettings().selectAdBlockTemplates(CabCompositeSettings.AdBlockTemplates.SIDEBAR);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            log.info("run cron");
            serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

            log.info("update some fields for childId");
            int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetUnderArticleAdblockId);
            operationMySql.getTickersComposite().updateOptions(childId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            log.info("update some fields for childId");
            int childIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetUnderArticleAdblockIdealmediaIoId);
            operationMySql.getTickersComposite().updateOptions(childIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childIdealmediaIoId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childIdealmediaIoId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //in-article
            log.info("update some fields for childId");
            int childInArticleId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetInArticleAdblockId);
            operationMySql.getTickersComposite().updateOptions(childInArticleId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childInArticleId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childInArticleId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            log.info("update some fields for childId");
            int childInArticleIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetInArticleAdblockIdealmediaIoId);
            operationMySql.getTickersComposite().updateOptions(childInArticleIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childInArticleIdealmediaIoId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childInArticleIdealmediaIoId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //header
            log.info("update some fields for childId");
            int childHeaderId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetHeaderAdblockId);
            operationMySql.getTickersComposite().updateOptions(childHeaderId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childHeaderId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childHeaderId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            log.info("update some fields for childId");
            int childHeaderIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetHeaderAdblockIdealmediaIoId);
            operationMySql.getTickersComposite().updateOptions(childHeaderIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childHeaderIdealmediaIoId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childHeaderIdealmediaIoId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            //sidebar
            log.info("update some fields for childId");
            int childSidebarId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetSidebarAdblockId);
            operationMySql.getTickersComposite().updateOptions(childSidebarId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childSidebarId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childSidebarId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

            log.info("update some fields for childId");
            int childSidebarIdealmediaIoId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetSidebarAdblockIdealmediaIoId);
            operationMySql.getTickersComposite().updateOptions(childSidebarIdealmediaIoId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
            operationMySql.getTickersComposite().updateDoubleClickSettings(childSidebarIdealmediaIoId, false, 0, true, 3);

            log.info("re-save child widget");
            authCabAndGo(compositeEditUrl + childSidebarIdealmediaIoId);
            pagesInit.getCabCompositeSettings().saveWidgetSettings();

           isSavedWidget = true;
        }
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration(mobile)")
    @Description("Add Under-Article/In-Article/Header/Sidebar adBlock childId and check limitAds for different height screen<a href='https://jira.mgid.com/browse/TA-52007'>TA-52007</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockCheckLimitAds", description = "Add Under-Article/In-Article/Header/Sidebar adBlock childId and check limitAds for different height screen")
    public void adBlockUnderArticleCheckLimitAds(int height, String limitAds, Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);

        mobileEmulationSettings.setHeight(height);

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .setMobileEmulation(true)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(childId, "limitads"), limitAds);

        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockCheckLimitAds(){
        return new Object[][]{
                {100, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-mobile-under-article-limitAds-mgid"},
                {640, "3", Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-mobile-under-article-limitAds-mgid"},
                {900, "5", Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-mobile-under-article-limitAds-mgid"},
                {100, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-mobile-under-article-limitAds-idealmedia-io"},
                {640, "2", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-mobile-under-article-limitAds-idealmedia-io"},
                {900, "3", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-mobile-under-article-limitAds-idealmedia-io"},
                {100, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-mobile-in-article-limitAds-mgid"},
                {640, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-mobile-in-article-limitAds-mgid"},
                {900, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-mobile-in-article-limitAds-mgid"},
                {100, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-mobile-in-article-limitAds-idealmedia-io"},
                {640, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-mobile-in-article-limitAds-idealmedia-io"},
                {900, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-mobile-in-article-limitAds-idealmedia-io"},
                {100, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-mobile-header-limitAds-mgid"},
                {640, "3", Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-mobile-header-limitAds-mgid"},
                {900, "3", Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-mobile-header-limitAds-mgid"},
                {100, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-mobile-header-limitAds-idealmedia-io"},
                {640, "3", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-mobile-header-limitAds-idealmedia-io"},
                {900, "3", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-mobile-header-limitAds-idealmedia-io"},
                {100, "1", Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-mobile-sidebar-limitAds-mgid"},
                {640, "2", Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-mobile-sidebar-limitAds-mgid"},
                {900, "3", Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-mobile-sidebar-limitAds-mgid"},
                {100, "1", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-mobile-sidebar-limitAds-idealmedia-io"},
                {640, "2", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-mobile-sidebar-limitAds-idealmedia-io"},
                {900, "3", Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-mobile-sidebar-limitAds-idealmedia-io"}
        };
    }
}
