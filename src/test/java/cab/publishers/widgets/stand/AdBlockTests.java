package cab.publishers.widgets.stand;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Story;
import libs.devtools.ServicerMock;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.cab.publishers.logic.widgets.CabCompositeSettings.AdBlockTemplates;
import testBase.TestBase;
import testData.project.Subnets;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static testData.project.CliCommandsList.Cron.CLONE_WIDGETS;
import static testData.project.EndPoints.*;

public class AdBlockTests extends TestBase {

    private final int widgetUnderArticleAdblockId = 444;
    private final int widgetInArticleAdblockId = 463;
    private final int widgetHeaderAdblockId = 478;
    private final int widgetSidebarAdblockId = 487;
    private final int widgetUnderArticleAdblockIdealmediaIoId = 445;
    private final int widgetInArticleAdblockIdealmediaIoId = 464;
    private final int widgetHeaderAdblockIdealmediaIoId = 479;
    private final int widgetSidebarAdblockIdealmediaIoId = 488;
    private final int widgetUnderArticleAdblockForEditStyleId = 450;
    private final int widgetInArticleAdblockForEditStyleId = 465;
    private final int widgetHeaderAdblockForEditStyleId = 480;
    private final int widgetSidebarAdblockForEditStyleId = 489;
    private final int widgetUnderArticleAdblockIdealmediaIoForEditStyleId = 451;
    private final int widgetInArticleAdblockIdealmediaIoForEditStyleId = 466;
    private final int widgetHeaderAdblockIdealmediaIoForEditStyleId = 481;
    private final int widgetSidebarAdblockIdealmediaIoForEditStyleId = 490;
    public final SelenideElement locatorForScreen = $("[id*='ScriptRoot']");

    public String setStand(String stand){ return String.format(widgetTemplateUrl, stand); }

    @BeforeClass
    public void createChildAdBlock(){
        //under-article adBlock
        addAdBlockToWidget(widgetUnderArticleAdblockId,                             AdBlockTemplates.UNDER_ARTICLE);
        addAdBlockToWidget(widgetUnderArticleAdblockIdealmediaIoId,                 AdBlockTemplates.UNDER_ARTICLE);
        addAdBlockToWidget(widgetUnderArticleAdblockForEditStyleId,                 AdBlockTemplates.UNDER_ARTICLE);
        addAdBlockToWidget(widgetUnderArticleAdblockIdealmediaIoForEditStyleId,     AdBlockTemplates.UNDER_ARTICLE);

        //in-article adBlock
        addAdBlockToWidget(widgetInArticleAdblockId,                            AdBlockTemplates.IN_ARTICLE);
        addAdBlockToWidget(widgetInArticleAdblockIdealmediaIoId,                AdBlockTemplates.IN_ARTICLE);
        addAdBlockToWidget(widgetInArticleAdblockForEditStyleId,                AdBlockTemplates.IN_ARTICLE);
        addAdBlockToWidget(widgetInArticleAdblockIdealmediaIoForEditStyleId,    AdBlockTemplates.IN_ARTICLE);

        //header adBlock
        addAdBlockToWidget(widgetHeaderAdblockId,                            AdBlockTemplates.HEADER);
        addAdBlockToWidget(widgetHeaderAdblockIdealmediaIoId,                AdBlockTemplates.HEADER);
        addAdBlockToWidget(widgetHeaderAdblockForEditStyleId,                AdBlockTemplates.HEADER);
        addAdBlockToWidget(widgetHeaderAdblockIdealmediaIoForEditStyleId,    AdBlockTemplates.HEADER);

        //sidebar adBlock
        addAdBlockToWidget(widgetSidebarAdblockId,                            AdBlockTemplates.SIDEBAR);
        addAdBlockToWidget(widgetSidebarAdblockIdealmediaIoId,                AdBlockTemplates.SIDEBAR);
        addAdBlockToWidget(widgetSidebarAdblockForEditStyleId,                AdBlockTemplates.SIDEBAR);
        addAdBlockToWidget(widgetSidebarAdblockIdealmediaIoForEditStyleId,    AdBlockTemplates.SIDEBAR);

        serviceInit.getDockerCli().runCronWithoutStop(CLONE_WIDGETS);

        reSaveChild(widgetUnderArticleAdblockId, true);
        reSaveChild(widgetUnderArticleAdblockIdealmediaIoId, true);
        reSaveChild(widgetInArticleAdblockId, true);
        reSaveChild(widgetInArticleAdblockIdealmediaIoId, true);
        reSaveChild(widgetHeaderAdblockId, true);
        reSaveChild(widgetHeaderAdblockIdealmediaIoId, true);
        reSaveChild(widgetSidebarAdblockId, true);
        reSaveChild(widgetSidebarAdblockIdealmediaIoId, true);
        reSaveChild(widgetUnderArticleAdblockForEditStyleId, false);
        reSaveChild(widgetUnderArticleAdblockIdealmediaIoForEditStyleId, false);
        reSaveChild(widgetInArticleAdblockForEditStyleId, false);
        reSaveChild(widgetInArticleAdblockIdealmediaIoForEditStyleId, false);
        reSaveChild(widgetHeaderAdblockForEditStyleId, false);
        reSaveChild(widgetHeaderAdblockIdealmediaIoForEditStyleId, false);
        reSaveChild(widgetSidebarAdblockForEditStyleId, false);
        reSaveChild(widgetSidebarAdblockIdealmediaIoForEditStyleId, false);
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration")
    @Story("Add In-Article template for adblock integration")
    @Story("Add header template for adblock integration")
    @Story("Add sidebar template for adblock integration")
    @Description("Add Under-Article/In-Article/Header/Sidebar adBlock childId and check screenshot <a href='https://jira.mgid.com/browse/TA-52007'>TA-52007, </a><a href='https://jira.mgid.com/browse/TA-52008'>TA-52008, </a><a href='https://jira.mgid.com/browse/TA-52070'>TA-52070, </a><a href='https://jira.mgid.com/browse/TA-52009'>TA-52009</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockCheckScreen", description = "Add Under-Article/In-Article/Header/Sidebar adBlock childId and check screenshot")
    public void adBlockCheckScreen(Subnets.SubnetType subnetId, int widgetId, String stand, String screen) {
        log.info("Test is started");

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(
                serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), locatorForScreen),
                serviceInit.getScreenshotService().getExpectedScreenshot("adBlockWidgets/" + screen + ".png")
        ));
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockCheckScreen(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-under-article-mgid",          "adBlockUnderArticleCheckScreen"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-under-article-idealmedia-io", "adBlockUnderArticleIdealmediaIoCheckScreen"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-in-article-mgid",             "adBlockInArticleCheckScreen"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-in-article-idealmedia-io",    "adBlockInArticleIdealmediaIoCheckScreen"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-header-mgid",                 "adBlockHeaderCheckScreen"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-header-idealmedia-io",        "adBlockHeaderIdealmediaIoCheckScreen"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-sidebar-mgid",                "adBlockSidebarCheckScreen"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-sidebar-idealmedia-io",       "adBlockSidebarIdealmediaIoCheckScreen"}
        };
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration")
    @Story("Add header template for adblock integration")
    @Story("Add sidebar template for adblock integration")
    @Description("Add Under-Article/Header/Sidebar adBlock childId and check work Doubleclick <a href='https://jira.mgid.com/browse/TA-52007'>TA-52007, </a><a href='https://jira.mgid.com/browse/TA-52008'>TA-52008, </a><a href='https://jira.mgid.com/browse/TA-52009'>TA-52009</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockCheckDoubleclick", description = "Add Under-Article/Header/Sidebar adBlock childId and check work Doubleclick")
    public void adBlockCheckDoubleclick(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().setShadowDom(true).clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 2с, время даблклика закончилось! кликаем по 3-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(4000);
        pagesInit.getWidgetClass().clickTeaserOnStand(3);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(3), "FAIL -> teaser 3: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockCheckDoubleclick(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockId,             "adblock-under-article-mgid"         },
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoId, "adblock-under-article-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockId,                   "adblock-header-mgid"         },
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoId,       "adblock-header-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockId,                  "adblock-sidebar-mgid"         },
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoId,      "adblock-sidebar-idealmedia-io"}
        };
    }

    @Feature("adBlock")
    @Story("Add In-Article template for adblock integration")
    @Description("Add In-Article adBlock childId and check work Doubleclick <a href='https://jira.mgid.com/browse/TA-52070'>TA-52070</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockInArticleCheckDoubleclick", description = "Add In-Article adBlock childId and check work Doubleclick")
    public void adBlockInArticleCheckDoubleclick(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(childId)
                .setSubnetType(subnetId)
                .setRequestType(ServicerMock.RequestType.REPLACE_ID)
                .setResponseParams("\"dcb\":1")
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        log.info("задержка даблклика 4с, как только отрисовался виджет на 6 мест, то кликаем на 1-вое место и проверяем что появляется блюр на тизере и кнопка GO!");
        pagesInit.getWidgetClass().setShadowDom(true).clickTeaserOnStand(1);
        softAssert.assertTrue(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick show");

        log.info("ждём ещё 11с, время даблклика закончилось! кликаем по 1-му месту в виджете и проверяем что на нём НЕ появляется блюр");
        sleep(11000);
        pagesInit.getWidgetClass().clickTeaserOnStand(1);
        softAssert.assertFalse(pagesInit.getWidgetClass().isShowDoubleClickForm(1), "FAIL -> teaser 1: doubleClick DOESN'T show");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockInArticleCheckDoubleclick(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockId,                "adblock-in-article-mgid"            },
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoId,    "adblock-in-article-idealmedia-io"   }
        };
    }

    @Feature("adBlock")
    @Story("Add Under-Article template for adblock integration")
    @Story("Add header template for adblock integration")
    @Story("Add sidebar template for adblock integration")
    @Description("Add Under-Article/Header/Sidebar adBlock after edit styles in cab and check in stand for parent <a href='https://jira.mgid.com/browse/TA-52007'>TA-52007, </a><a href='https://jira.mgid.com/browse/TA-52008'>TA-52008, </a><a href='https://jira.mgid.com/browse/TA-52009'>TA-52009</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockEditStyleAndCheck", description = "Add Under-Article/Header/Sidebar adBlock after edit styles in cab and check in stand for parent")
    public void adBlockEditStyleAndCheck(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);
        authCabAndGo(editCodeUrl + childId);
        pagesInit.getCabEditCode().switchToCssRadio();
        pagesInit.getCabEditCode().changeStyle(".mctitle", "2", "15");
        pagesInit.getCabEditCode().saveEditCodeInterface();

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getTitleStyle("font-size"), "15px");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockEditStyleAndCheck(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetUnderArticleAdblockForEditStyleId,                "adblock-under-article-for-edit-style-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetUnderArticleAdblockIdealmediaIoForEditStyleId,    "adblock-under-article-for-edit-style-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetHeaderAdblockForEditStyleId,                      "adblock-header-for-edit-style-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetHeaderAdblockIdealmediaIoForEditStyleId,          "adblock-header-for-edit-style-idealmedia-io"},
                {Subnets.SubnetType.SCENARIO_MGID,          widgetSidebarAdblockForEditStyleId,                     "adblock-sidebar-for-edit-style-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetSidebarAdblockIdealmediaIoForEditStyleId,         "adblock-sidebar-for-edit-style-idealmedia-io"}
        };
    }

    @Feature("adBlock")
    @Story("Add In-Article template for adblock integration")
    @Description("Add In-Article adBlock after edit styles in cab and check in stand for parent <a href='https://jira.mgid.com/browse/TA-52070'>TA-52070</a>")
    @Owner(value="RKO")
    @Test(dataProvider = "dataAdBlockInArticleEditStyleAndCheck", description = "Add In-Article adBlock after edit styles in cab and check in stand for parent")
    public void adBlockInArticleEditStyleAndCheck(Subnets.SubnetType subnetId, int widgetId, String stand) {
        log.info("Test is started");

        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);
        authCabAndGo(editCodeUrl + childId);
        pagesInit.getCabEditCode().switchToCssRadio();
        pagesInit.getCabEditCode().changeStyle(".mctitle", "7", "15");
        pagesInit.getCabEditCode().saveEditCodeInterface();

        serviceInit.getServicerMock().setStandName(setStand("adblock/" + stand))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwait();

        Assert.assertEquals(pagesInit.getWidgetClass().setShadowDom(true).getTitleStyle("font-size"), "15px", "FAIL: TITLE(.mctitle>a) -> font-size");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] dataAdBlockInArticleEditStyleAndCheck(){
        return new Object[][]{
                {Subnets.SubnetType.SCENARIO_MGID,          widgetInArticleAdblockForEditStyleId,                   "adblock-in-article-for-edit-style-mgid"},
                {Subnets.SubnetType.SCENARIO_IDEALMEDIA_IO, widgetInArticleAdblockIdealmediaIoForEditStyleId,       "adblock-in-article-for-edit-style-idealmedia-io"}
        };
    }

    private void addAdBlockToWidget(int widgetId, AdBlockTemplates template){
        authCabAndGo(compositeEditUrl + widgetId);
        pagesInit.getCabCompositeSettings().switchAdBlockIntegrations(true);
        pagesInit.getCabCompositeSettings().selectAdBlockTemplates(template);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
    }

    private void reSaveChild(int widgetId, boolean isDoubleClickNeed){
        log.info("update some fields for childId");
        int childId = operationMySql.getTickersCompositeRelations().getChildTickersCompositeId(widgetId);
        operationMySql.getTickersComposite().updateOptions(childId, "\"baseDomain\": \"http://local.s3.eu-central-1.amazonaws.com/local\"");
        if(isDoubleClickNeed) operationMySql.getTickersComposite().updateDoubleClickSettings(childId, true, 3, false, 0);

        log.info("re-save child widget");
        authCabAndGo(compositeEditUrl + childId);
        pagesInit.getCabCompositeSettings().saveWidgetSettings();
    }
}
