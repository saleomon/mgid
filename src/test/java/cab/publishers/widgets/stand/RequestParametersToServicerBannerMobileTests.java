package cab.publishers.widgets.stand;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import testBase.TestBase;
import testData.project.publishers.WidgetTypes;

import static testData.project.EndPoints.widgetTemplateUrl;

public class RequestParametersToServicerBannerMobileTests extends TestBase {

    public String setStand(String stand){ return String.format(widgetTemplateUrl, "serviser_request_banner/" + stand); }

    public void goToCreateWidget(int widgetId){
        authDashAndGo("testEmail68@ex.ua","publisher/edit-widget/id/" + widgetId);
    }

    @BeforeMethod
    public void setMobileEmulation(){
        serviceInit.getServicerMock().setMobileEmulation(true);
    }

    @AfterMethod
    public void closeCdtConnection(){
        serviceInit.getServicerMock().tearDown();
    }

    @Feature("Request parameters to servicer")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_PLUS, infinite-scroll=false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_PLUS, infinite-scroll=false")
    public void smartPlusCheckBannerParams() {
        log.info("Test is started");
        int widgetId = 424;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_PLUS)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("banner1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxw_7"), "344", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxh_7"), "269", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ident_p"), "true", "FAIL -> ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_MAIN, infinite-scroll=false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_MAIN, infinite-scroll=false")
    public void smartMainCheckBannerParams() {
        log.info("Test is started");
        int widgetId = 424;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_MAIN)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("banner1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxw_7"), "344", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxh_7"), "274", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ident_p"), null, "FAIL -> ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("Request parameters to servicer")
    @Story("Send teaser size information to the servicer in the widget(Banner, mobile device)")
    @Description("Types.SMART, SubTypes.SMART_BLUR, infinite-scroll=false <a href='https://jira.mgid.com/browse/TA-52091'>TA-52091</a>")
    @Test(description = "Types.SMART, SubTypes.SMART_BLUR, infinite-scroll=false")
    public void smartBlurCheckBannerParams() {
        log.info("Test is started");
        int widgetId = 424;
        goToCreateWidget(widgetId);
        pagesInit.getWidgetClass()
                .setTypeAndSubTypeWidgetIfTheyDontSet(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .setInfiniteScroll(false)
                .changeDataAndSaveWidget(WidgetTypes.Types.SMART, WidgetTypes.SubTypes.SMART_BLUR)
                .saveWidgetSettings();

        serviceInit.getServicerMock().setStandName(setStand("banner1"))
                .setWidgetIds(widgetId)
                .setSubnetType(subnetId)
                .interceptionNetworkAndMockThem()
                .countDownLatchAwaitWithoutTearDown();
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxw_6"), "344", "FAIL -> maxw_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "maxh_6"), "280", "FAIL -> maxh_7");
        softAssert.assertEquals(serviceInit.getServicerMock().parseServicerRequest(widgetId, "ident_p"), "true", "FAIL -> ident_p");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
