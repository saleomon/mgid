package cab.administration;

import org.testng.*;
import org.testng.annotations.*;
import testBase.TestBase;

public class BlacklistDfpTests extends TestBase {

    /**
     * create and delete new DFP domain
     * RKO
     */
    @Test
    public void addNewDfpDomain(){
        log.info("Test is started");
        authCabAndGo("blacklist-dfp/blacklisted-domains-dfp");
        softAssert.assertTrue(pagesInit.getBlacklistDfp().addDfpDomain(), "fail -> dfp domain doesn't add");
        softAssert.assertTrue(pagesInit.getBlacklistDfp().deleteDfpDomain(), "fail -> dfp domain doesn't delete");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * check set equals domain
     * RKO
     */
    @Test
    public void checkSetEqualsDomain(){
        log.info("Test is started");
        authCabAndGo("blacklist-dfp/blacklisted-domains-dfp");
        softAssert.assertTrue(pagesInit.getBlacklistDfp().addDfpDomain(), "FAIL -> dfp domain doesn't add");
        softAssert.assertFalse(pagesInit.getBlacklistDfp()
                .setDomain(pagesInit.getBlacklistDfp().getDomain())
                .addDfpDomain(),
                "FAIL -> dfp domain doesn't add");
        softAssert.assertTrue( helpersInit.getMessageHelper().checkCustomMessagesCab("Domain already blacklisted"), "FAIL -> message doesn't correct");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * add domain with spaces
     * RKO
     */
    @Test
    public void addDfpDomainWithSpaces(){
        log.info("Test is started");
        authCabAndGo("blacklist-dfp/blacklisted-domains-dfp");

        Assert.assertEquals(pagesInit.getBlacklistDfp()
                .setDomain("   testGreta.com   ")
                .getDomainValueFromInput(),
                "testGreta.com",
                "fail -> dfp domain doesn't add");

        log.info("Test is finished");
    }

    /**
     * add domain - 100 symbols
     * result - domain trim to 100 symbols
     * RKO
     */
    @Test
    public void addDfpDomain_101_symbols(){
        log.info("Test is started");
        authCabAndGo("blacklist-dfp/blacklisted-domains-dfp");

        Assert.assertEquals(pagesInit.getBlacklistDfp()
                .setDomain("Standing on the River Thames, London has been a major settlement for two millennia," +
                        "its history going back to its founding by the Romans, who named it Londinium.")
                .getDomainValueFromInput(),
                "Standing on the River Thames, London has been a major settlement for two millennia,its history going",
                "fail -> dfp domain doesn't add");

        log.info("Test is finished");
    }
}
