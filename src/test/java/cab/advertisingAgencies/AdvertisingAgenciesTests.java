package cab.advertisingAgencies;

import core.service.customAnnotation.Privilege;
import io.qameta.allure.*;
import org.testng.*;
import org.testng.annotations.*;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;
import static testData.project.Subnets.SUBNET_MGID_NAME;

public class AdvertisingAgenciesTests extends TestBase {

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23830">...</a>
     * Не работает история генерации Хешей
     * check show link registration history pop-up
     * RKO
     */
    @Test
    public void checkLinkRegistrationHistory(){
        log.info("Test is started");
        authCabAndGo("advertising-agency");
        Assert.assertTrue(pagesInit.getAdvertisingAgencies().checkLinkRegistrationHistory(), "fail -> link registration history doesn't open");
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(BLOCK_AUTHORIZATION)
    @Description("users.login='weird.ant', role_users=53, users.is_blocked=0")
    @Owner(NIO)
    @Test(description = "Check change blocked flag")
    public void checkChangeBlockedFlag(){
        log.info("Test is started");

        authCabAndGo("advertising-agency/index?curator=mariush.werpakovick");

        pagesInit.getAdvertisingAgencies().blockUnblockAccessToCab("block");
        helpersInit.getMessageHelper().checkMessagesCab("Flag successfully changed!");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkDisplayingIconForAccess("block"), "Fail - icon thing one");
        softAssert.assertEquals(operationMySql.getUsers().getIsBlockedFlag("weird.ant"), "1", "Fail - getIsBlockedFlag first");

        pagesInit.getAdvertisingAgencies().blockUnblockAccessToCab("unblock");
        helpersInit.getMessageHelper().checkMessagesCab("Flag successfully changed!");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkDisplayingIconForAccess("unblock"), "Fail - icon thing two");
        softAssert.assertEquals(operationMySql.getUsers().getIsBlockedFlag("weird.ant"), "0", "Fail - getIsBlockedFlag first");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(FILTERS)
    @Feature(AA_MCM)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>managers_accounts.mcm_aa = 1</li>\n" +
            "   <li>managers_accounts.mcm_aa = 0</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52407\">Ticket TA-52407</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check work 'Filter by MCM' filter in advertising agencies interface")
    public void checkFilterByMcmAgency(){
        log.info("test is started");
        int amountDisplayedAgencies;
        int amountOfMcmAgencyIcons;
        int amountOfAllAgenciesViaDb;
        int amountNotMcmAgenciesViaDb;
        authCabAndGo("advertising-agency/index");

        pagesInit.getAdvertisingAgencies()
                .chooseMcmFilter("MCM AA")
                .clickFilterButton();

        amountDisplayedAgencies = pagesInit.getAdvertisingAgencies().getAmountDisplayedAgencies();
        amountOfMcmAgencyIcons = pagesInit.getAdvertisingAgencies().getAmountMcmAgencyIcons();
        log.info("Check amount all agencies and MCM agency icons - " + amountDisplayedAgencies + " == " + amountOfMcmAgencyIcons);
        softAssert.assertEquals(amountDisplayedAgencies, amountOfMcmAgencyIcons,"There are not correct number of agencies and MCM agency icons");

        pagesInit.getAdvertisingAgencies()
                .chooseMcmFilter("NOT MCM AA")
                .clickFilterButton();

        amountDisplayedAgencies = pagesInit.getAdvertisingAgencies().getAmountDisplayedAgencies();
        amountNotMcmAgenciesViaDb = operationMySql.getUsers().selectAmountOfEnabledAgenciesWithoutMCM();
        softAssert.assertEquals(amountDisplayedAgencies, amountNotMcmAgenciesViaDb,"There are not correct number agencies without MCM");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().checkMcmAgencyIconVisibility(), "There is 'MCM AA' icon visibility");

        authCabAndGo("advertising-agency/index");
        log.info("Check MCM filter with value all");
        amountDisplayedAgencies = pagesInit.getAdvertisingAgencies().getAmountDisplayedAgencies();
        amountOfAllAgenciesViaDb = operationMySql.getUsers().selectAmountOfEnabledAgencies();
        softAssert.assertEquals(amountDisplayedAgencies, amountOfAllAgenciesViaDb, "There are not correct number of AA and agencies at DB");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Roles privileges for add, edit bonus conditions icons</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Privilege(name = "advertising-agency/manage-individual-bonus-conditions")
    @Test(description = "Check privilege for 'manage-individual-bonus-conditions' icons")
    public void checkPrivilegeManageIndividualBonusConditions() {
        log.info("test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE,
                "advertising-agency/manage-individual-bonus-conditions");

        authCabForCheckPrivileges("advertising-agency/index?agency=SOK_RD");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().checkAddBonusIconVisibility(),
                "There is add bonus icon without individual conditions");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE,
                "advertising-agency/manage-individual-bonus-conditions");

        authCabForCheckPrivileges("advertising-agency/index?agency=SOK_RD");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkAddBonusIconVisibility(),
                "There is no add bonus icon without individual conditions");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>Roles privilege for AA individual bonus conditions icon</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Privilege(name = {"advertising-agency/manage-individual-bonus-conditions", "advertising-agency/can_show_individual_conditions_popup"})
    @Test(description = "Check privilege for 'can_show_individual_conditions_popup' icon")
    public void checkPrivilegeShowIndividualConditionsPopup() {
        log.info("test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE,
                "advertising-agency/manage-individual-bonus-conditions");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE,
                "advertising-agency/can_show_individual_conditions_popup");

        authCabForCheckPrivileges("advertising-agency/index?agency=SOK_RB");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().checkEditBonusIconVisibility(),
                "There is edit bonus icon with enabled individual conditions");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkAgencyIndividualBonusIconVisibility(),
                "There is no AA individual bonus icon");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE,
                "advertising-agency/can_show_individual_conditions_popup");
        authCabForCheckPrivileges("advertising-agency/index?agency=SOK_RB");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().checkEditBonusIconVisibility(),
                "There is edit bonus icon with enabled individual conditions");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().checkAgencyIndividualBonusIconVisibility(),
                "There is AA individual bonus icon");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check AA settings without individual bonus")
    public void checkAgencySettingsWithoutIndividualBonus() {
        log.info("test is started");

        authCabAndGo("advertising-agency/index?agency=SOK_RD");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(false), "FAIL -> checkPaymentBonusAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedSubmitButton(), "There is no Submit button");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedCancelButton(), "There is no Cancel button");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().isDisplayedDisableButton(), "There is Disable button");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 1</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check AA settings with enabled individual bonus")
    public void checkAgencySettingsWithEnabledIndividualBonus() {
        log.info("test is started");

        authCabAndGo("advertising-agency/index?agency=SOK_RB");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getUserAALogin(), "SOK_RB", "FAIL -> get user AA login!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getUserAACurrency(), "eur", "FAIL -> get user AA currency!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "1000", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "10", "FAIL -> get bonus for first grade!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(true), "FAIL -> checkPaymentBonusAccountState(true)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "test", "FAIL -> get comment text!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedSubmitButton(), "There is no Submit button");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedCancelButton(), "There is no Cancel button");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedDisableButton(), "There is no Disable button");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getDisableTooltipText(), "Disable and delete all individual bonus conditions",
                "FAIL -> get tooltip text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 0</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check AA settings with disabled individual bonus")
    public void checkAgencySettingsWithDisabledIndividualBonus() {
        log.info("test is started");
        String warningMessage = "Warning! Individual bonuses conditions disabled!";

        authCabAndGo("advertising-agency/index?agency=SOK_RA");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(false), "FAIL -> checkPaymentBonusAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(true), "FAIL -> checkPaymentExternalAccountState(true)");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedSubmitButton(), "There is no Submit button");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().isDisplayedCancelButton(), "There is no Cancel button");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().isDisplayedDisableButton(), "There is Disable button");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getWarningMessage(),
                warningMessage, "There is incorrect warning text present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check cancel creating settings individual bonus")
    public void checkCancelCreatingSettingsIndividualBonus() {
        log.info("test is started");

        authCabAndGo("advertising-agency/index?agency=SOK_RD");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("100")
                .setBonusFirstGrade("3")
                .addRate()
                .setSpentSecondGrade("200")
                .setBonusSecondGrade("5")
                .addRate()
                .setSpentThirdGrade("300")
                .setBonusThirdGrade("7")
                .setPaymentBonusAccount()
                .setComment("testComment")
                .clickCancelButton();

        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().getSpentFirstGrade().isEmpty(), "FAIL -> get empty spent for first grade!");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().getBonusFirstGrade().isEmpty(), "FAIL -> get empty bonus for first grade!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(false), "FAIL -> checkPaymentBonusAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().getCommentText().isEmpty(),  "FAIL -> get empty comment!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check error message with missing individual bonus")
    public void checkErrorMessageWithMissingIndividualBonus() {
        log.info("test is started");
        String errorMessage = "Invalid spending value . Invalid percentage value";

        authCabAndGo("advertising-agency/index?agency=SOK_RD");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setPaymentBonusAccount()
                .setComment("testComment")
                .clickSubmitButton();

        Assert.assertEquals(pagesInit.getAdvertisingAgencies().getErrorMessage(),
                errorMessage, "There is incorrect error text present");
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check creating individual bonus with first grade for bonus account)")
    public void checkCreatingIndividualBonusWithFirstGradeForBonusAccount() {
        log.info("test is started");
        String AA_login = "weird.ant";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("100.89")
                .setBonusFirstGrade("4.8")
                .addRate()
                .setSpentSecondGrade("200.01")
                .setBonusSecondGrade("5.0")
                .addRate()
                .setSpentThirdGrade("300.50")
                .setBonusThirdGrade("7.5")
                .setPaymentBonusAccount()
                .setComment("testCommentBonus")
                .removeRate()
                .removeRate()
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "100.89", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "4.8", "FAIL -> get bonus for first grade!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentSecondGradeIsDisplayed(), "FAIL -> Spent second grade is visible!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentThirdGradeIsDisplayed(), "FAIL -> Spent third grade is visible!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(true), "FAIL -> checkPaymentBonusAccountState(true)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "testCommentBonus", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check creating individual bonus with first grade for external account)")
    public void checkCreatingIndividualBonusWithFirstGradeForExternalAccount() {
        log.info("test is started");
        String AA_login = "SOK_RE";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("10.11")
                .setBonusFirstGrade("8.8")
                .setPaymentExternalAccount()
                .setComment("testCommentExternalBonus")
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "10.11", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "8.8", "FAIL -> get bonus for first grade!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentSecondGradeIsDisplayed(), "FAIL -> Spent second grade is visible!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentThirdGradeIsDisplayed(), "FAIL -> Spent third grade is visible!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(false), "FAIL -> checkPaymentBonusAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(true), "FAIL -> checkPaymentExternalAccountState(true)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "testCommentExternalBonus", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check creating individual bonus with second grades for external account)")
    public void checkCreatingIndividualBonusWithSecondGradesForExternalAccount() {
        log.info("test is started");
        String AA_login = "SOK_RF";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("100.11")
                .setBonusFirstGrade("8.8")
                .addRate()
                .setSpentSecondGrade("200.01")
                .setBonusSecondGrade("5.0")
                .setPaymentExternalAccount()
                .setComment("testCommentExternalBonus")
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "100.11", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "8.8", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentSecondGrade(), "200.01", "FAIL -> get spent for second grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusSecondGrade(), "5", "FAIL -> get bonus for second grade!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentThirdGradeIsDisplayed(), "FAIL -> Spent third grade is visible!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(false), "FAIL -> checkPaymentBonusAccountState(false)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(true), "FAIL -> checkPaymentExternalAccountState(true)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "testCommentExternalBonus", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check creating individual bonus with third grades for bonus account)")
    public void checkCreatingIndividualBonusWithThirdGradesForBonusAccount() {
        log.info("test is started");
        String AA_login = "SOK_RG";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("100.11")
                .setBonusFirstGrade("8.8")
                .addRate()
                .setSpentSecondGrade("200.01")
                .setBonusSecondGrade("5.0")
                .addRate()
                .setSpentThirdGrade("300.50")
                .setBonusThirdGrade("7.5")
                .setPaymentBonusAccount()
                .setComment("testCommentBonus")
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "100.11", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "8.8", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentSecondGrade(), "200.01", "FAIL -> get spent for second grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusSecondGrade(), "5", "FAIL -> get bonus for second grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentThirdGrade(), "300.5", "FAIL -> get spent for third grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusThirdGrade(), "7.5", "FAIL -> get bonus for third grade!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(true), "FAIL -> checkPaymentBonusAccountState(true)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "testCommentBonus", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>there is no record in partners.individual_bonuses_conditions_aa</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check error message where value of spending is less than the previous one in individual bonus")
    public void checkErrorMessageCurrentValueOfSpentLessThanPreviousOne() {
        log.info("test is started");
        String errorMessage = "The current value of spending is less than the previous one";

        authCabAndGo("advertising-agency/index?agency=SOK_RD");
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("100.50")
                .setBonusFirstGrade("10.0")
                .addRate()
                .setSpentSecondGrade("100.49")
                .setBonusSecondGrade("15.0")
                .addRate()
                .setSpentThirdGrade("100.50")
                .setBonusThirdGrade("10.5")
                .setPaymentBonusAccount()
                .setComment("testCommentBonus")
                .clickSubmitButton();

        Assert.assertEquals(pagesInit.getAdvertisingAgencies().getErrorMessage(),
                errorMessage, "There is incorrect error text present");
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 0</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check editing AA with disabled individual bonus")
    public void checkEditingAgencyWithDisabledIndividualBonus() {
        log.info("test is started");
        String AA_login = "SOK_RC";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "1000", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "10", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "test", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 1</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check editing AA with enabled individual bonus")
    public void checkEditingAgencyWithEnabledIndividualBonus() {
        log.info("test is started");
        String AA_login = "AA_agency";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .clickSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "1", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "1000", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "10", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "test", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 1</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check editing settings individual bonus")
    public void checkEditingSettingsIndividualBonus() {
        String AA_login = "SOK_RH";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .removeRate()
                .setSpentFirstGrade("5100.11")
                .setBonusFirstGrade("4.1")
                .setSpentSecondGrade("9876.22")
                .setBonusSecondGrade("5.9")
                .setPaymentBonusAccount()
                .setComment("Account name: тест3")
                .clickSubmitButton();

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "5100.11", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "4.1", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentSecondGrade(), "9876.22", "FAIL -> get spent for second grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusSecondGrade(), "5.9", "FAIL -> get bonus for second grade!");
        softAssert.assertFalse(pagesInit.getAdvertisingAgencies().spentThirdGradeIsDisplayed(), "FAIL -> Spent third grade is visible!");

        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentBonusAccountState(true), "FAIL -> checkPaymentBonusAccountState(true)");
        softAssert.assertTrue(pagesInit.getAdvertisingAgencies().checkPaymentExternalAccountState(false), "FAIL -> checkPaymentExternalAccountState(false)");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "Account name: тест3", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 1</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check cancel editing settings individual bonus")
    public void checkCancelEditingSettingsIndividualBonus() {
        String AA_login = "SOK_RB";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .setSpentFirstGrade("30.11")
                .setBonusFirstGrade("4")
                .setComment("Account name: SOK_RB")
                .clickCancelButton();

        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();

        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getSpentFirstGrade(), "1000", "FAIL -> get spent for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getBonusFirstGrade(), "10", "FAIL -> get bonus for first grade!");
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getCommentText(), "test", "FAIL -> get comment text!");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_BONUSES)
    @Story(ADVERTISER_AGENCIES_LIST_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>partners.individual_bonuses_conditions_aa = 1</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52445\">Ticket TA-52445</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check disable individual bonus request")
    public void checkDisableIndividualBonusRequest() {
        String AA_login = "SOK_RI";
        String warningMessage = "Warning! Individual bonuses conditions disabled!";

        authCabAndGo("advertising-agency/index?agency=" + AA_login);
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        pagesInit.getAdvertisingAgencies()
                .clickDisableButton()
                .clickDisableFormSubmitButton();

        softAssert.assertEquals(operationMySql.getIndividualBonusesConditionsAA().getStatus(AA_login), "0", "FAIL -> AA_login status");

        helpersInit.getBaseHelper().refreshCurrentPage();
        pagesInit.getAdvertisingAgencies().openAddBonusConditionPopup();
        softAssert.assertEquals(pagesInit.getAdvertisingAgencies().getWarningMessage(),
                warningMessage, "There is incorrect warning text present");

        softAssert.assertAll();
        log.info("Test is finished");
    }

}
