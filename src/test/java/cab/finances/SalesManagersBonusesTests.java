package cab.finances;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.$;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class SalesManagersBonusesTests extends TestBase {

    private final SelenideElement tableArea = $("[id='content']");
    private final SelenideElement popupArea = $("[class*=popup-geo-team-countries-config-grades]");

    @Epic("Sales Bonuses")
    @Feature("Filters")
    @Story("Sales Managers Bonuses interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check Filtering data at interface</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52172\">Ticket TA-52172</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Test(description = "Check Filtering data at interface")
    public void checkFilteringDataSalesBonuses() {
        log.info("Test is started");

        log.info("Filter data");
        authCabAndGo("finance/sales-bonuses");
        pagesInit.getSalesManagersBonuses().chooseFilterUser("wild.elephant")
                                            .chooseFilterMonth("July")
                                            .chooseFilterYear("2022")
                                            .filterData();

        log.info("check displaying filtered data");
        Assert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), tableArea),
                serviceInit.getScreenshotService().getExpectedScreenshot("checkFilteringDataSalesBonuses.png")), "FAIL -> screen simple");

        log.info("Test is finished");
    }

    @Epic("Sales Bonuses")
    @Feature("Export")
    @Story("Sales Managers Bonuses interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check export data</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52172\">Ticket TA-52172</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Test(description = "Check export data at file")
    public void checkExportDataSalesBonuses() {
        log.info("Test is started");
        String[] headers = {"Manager login", "Client ID:", "Date", "Bonus days", "Priority Spent $", "Priority Bonus $", "Not priority Spent $", "Not priority Bonus $", "Total Spent $", "Total bonus:"};

        log.info("Filter data");
        authCabAndGo("finance/sales-bonuses");
        pagesInit.getSalesManagersBonuses().chooseFilterUser("wild.elephant")
                                            .chooseFilterMonth("March")
                                            .chooseFilterYear("2022")
                                            .filterData();

        pagesInit.getSalesManagersBonuses().clickButtonCsvSalesBonusesCab();
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().setCustomRowNumber(2).loadExportedFileCab(), "FAIL -> load");

        log.info("check export of filtered data");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkShowAllColumnNames(headers),
                "FAIL -> Some of column headers have incorrect names!");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Manager login", "wild.elephant"), "FAIL -> Manager login");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Client ID:", "1140"), "FAIL -> Client ID:");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Client login", "masstestEmail1140@ex.ua"), "FAIL -> Client login");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Date", "2022-03-23"), "FAIL -> Date");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Bonus days", "8"), "FAIL -> Bonus days");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Priority Spent $", "0,00"), "FAIL -> Priority Spent $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Priority Bonus $", "0,00"), "FAIL -> Priority Bonus $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Not priority Spent $", "2,00"), "FAIL -> Not priority Spent $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Not priority Bonus $", "0,10"), "FAIL -> Not priority Bonus $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Total Spent $", "2,00"), "FAIL -> Total Spent $");
        softAssert.assertTrue(serviceInit.getExportFileTableStructure().checkColumnForCustomRow("Total bonus:", "0,10"), "FAIL -> Total bonus:");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Sales Bonuses")
    @Feature("Popup with grades")
    @Story("Sales Managers Bonuses interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check popup with grades</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52141\">Ticket TA-52141</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Test(description = "Check popup with grades")
    public void checkPopupWithGrades() {
        log.info("Test is started");

        log.info("Filter data");
        authCabAndGo("finance/sales-bonuses");
        pagesInit.getSalesManagersBonuses().chooseFilterUser("tiny.hippo")
                .chooseFilterMonth("July")
                .chooseFilterYear("2022")
                .filterData();

        log.info("open popup");
        pagesInit.getSalesManagersBonuses().openPopupWithGrades();

        softAssert.assertTrue(serviceInit.getScreenshotService().compareScreenshots(serviceInit.getScreenshotService().takeScreenshot(WebDriverRunner.getWebDriver(), popupArea),
                serviceInit.getScreenshotService().getExpectedScreenshot("checkPopupWithGrades.png")), "FAIL -> screen simple");

        pagesInit.getSalesManagersBonuses().confirmPopup();
        softAssert.assertFalse(pagesInit.getSalesManagersBonuses().isDisplayPopupWithGrades(popupArea), "Fail - popup is visible");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Sales Bonuses")
    @Feature("Privilege for icon of popup grades")
    @Story("Sales Managers Bonuses interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check privilege for icon of popup grades</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52141\">Ticket TA-52141</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Privilege(name="get-geo-team-countries-config-grades")
    @Test(description = "Check privilege for icon of popup grades")
    public void checkPrivilegeOfIconPopupGrades() {
        log.info("Test is started");

        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "finance/get-geo-team-countries-config-grades");

        log.info("Filter data");
        authCabForCheckPrivileges("finance/sales-bonuses");
        pagesInit.getSalesManagersBonuses().chooseFilterUser("tiny.hippo")
                .chooseFilterMonth("July")
                .chooseFilterYear("2022")
                .filterData();

        Assert.assertFalse(pagesInit.getSalesManagersBonuses().isDisplayIconPopupWithGrades(), "Fail - popup is visible");

        log.info("Test is finished");
    }

    @Epic("Sales Bonuses")
    @Feature("Messages")
    @Story("Sales Managers Bonuses interface")
    @Description("\n" +
            "<ul>\n" +
            "   <li>Check messages for no data</>" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52141\">Ticket TA-52141</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Test(description = "Check messages for no data")
    public void checkMessagesNoData() {
        log.info("Test is started");

        log.info("Filter data");
        authCabAndGo("finance/sales-bonuses");
        pagesInit.getSalesManagersBonuses().chooseFilterUser("tiny.hippo")
                .chooseFilterMonth("April")
                .chooseFilterYear("2022")
                .filterData();

        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("No records were found"), "Fail - no message after filtering");

        pagesInit.getSalesManagersBonuses().clickButtonCsvSalesBonusesCab();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("No records were found"), "Fail - no message after export data");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
