package cab.finances;

import io.qameta.allure.*;
import org.testng.annotations.Test;
import testBase.TestBase;

import static core.service.constantTemplates.ConstantsInit.*;
import static testData.project.Subnets.SUBNET_MGID_NAME;

public class AdvertisingAgencyBonusesTests extends TestBase {

    @Epic(FILTERS)
    @Feature(AA_MCM)
    @Story(ADVERTISER_AGENCY_BONUSES_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>managers_accounts.mcm_aa = 1</li>\n" +
            "   <li>managers_accounts.mcm_aa = 0</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52407\">Ticket TA-52407</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check work 'Filter by MCM' filter in advertising agency bonuses interface")
    public void checkFilterByMcmAgency(){
        log.info("test is started");
        int amountDisplayedAgenciesWithBonuses;
        int amountOfMcmAgencyIcons;
        int amountOfAllAgenciesWithBonusesViaDb;
        authCabAndGo("overdraft/bonus-approval");

        pagesInit.getAgencyBonuses()
                .chooseStatusFilter("All")
                .chooseSubnetFilter(SUBNET_MGID_NAME)
                .chooseMcmFilter("MCM AA")
                .clickFilterButton();

        amountDisplayedAgenciesWithBonuses = pagesInit.getAgencyBonuses().getAmountDisplayedAgencies();
        amountOfMcmAgencyIcons = pagesInit.getAgencyBonuses().getAmountMcmAgencyIcons();
        log.info("Check amount all agencies and MCM agency icons - " + amountDisplayedAgenciesWithBonuses + " == " + amountOfMcmAgencyIcons);
        softAssert.assertEquals(amountDisplayedAgenciesWithBonuses, amountOfMcmAgencyIcons,"There are not correct number of agencies and MCM agency icons");

        pagesInit.getAgencyBonuses()
                .chooseMcmFilter("NOT MCM AA")
                .clickFilterButton();

        amountDisplayedAgenciesWithBonuses = pagesInit.getAgencyBonuses().getAmountDisplayedAgencies();
        softAssert.assertEquals(amountDisplayedAgenciesWithBonuses, 2,"There are not correct number agencies");
        softAssert.assertFalse(pagesInit.getAgencyBonuses().checkMcmAgencyIconVisibility(), "There is 'MCM AA' icon visibility");

        pagesInit.getAgencyBonuses()
                .chooseMcmFilter("All")
                .clickFilterButton();

        amountDisplayedAgenciesWithBonuses = pagesInit.getAgencyBonuses().getAmountDisplayedAgencies();
        amountOfAllAgenciesWithBonusesViaDb = operationMySql.getUsers().selectAmountOfAgenciesWithBonuses();
        softAssert.assertEquals(amountDisplayedAgenciesWithBonuses, amountOfAllAgenciesWithBonusesViaDb, "There are not correct number of AA and agencies at DB");

        softAssert.assertAll();
        log.info("Test is finished");
    }
}
