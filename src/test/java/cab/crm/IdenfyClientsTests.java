package cab.crm;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.open;
import static libs.devtools.IdenfyMock.Response.*;
import static testData.project.CliCommandsList.Cron.IDENFY_SUSPECTED_CRON;
import static testData.project.ClientsEntities.NEW_CLIENT_LOGIN_MGID;
import static testData.project.ClientsEntities.NEW_CLIENT_PASSWORD;
import static testData.project.EndPoints.mgidLink;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class IdenfyClientsTests extends TestBase {


    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Advertisers. Add Dashboard iframe and API")
    @Description("Check rights for enabling Idenfy verification\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52167\">Ticket TA-52167</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check rights for enabling Idenfy verification")
    @Privilege(name = "crm/can_kyc_verify_idenfy")
    public void checkIdenfyRights() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_kyc_verify_idenfy");
        authCabForCheckPrivileges("crm/edit-client/id/2001");
        Assert.assertFalse(pagesInit.getCrmClients().checkIdenfyIsDisplayed(),
                "FAIL -> Check rights Idenfy for client!");
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Advertisers. Add Dashboard iframe and API")
    @Description("Check that Idenfy verification settings is saved correctly\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52167\">Ticket TA-52167</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that Idenfy verification settings is saved correctly")
    public void checkIdenfyEnableDisableForClient() {
        log.info("Test is started");
        int clientId = 2051;
        authCabAndGo("crm/edit-client/id/" + clientId);
        pagesInit.getCrmClients().setStateIdenfyVerificationForClient(true);
        authCabAndGo("crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(true),
                "FAIL -> Check Idenfy for client after enabling!");
        authDashAndGo("idenfy-client-1@mgid.com", "profile/users");
        softAssert.assertTrue(pagesInit.getUserProfile().checkProfileIdenfyTabIsDisplayed(),
                "FAIL -> Idenfy tab isn`t visible with enabled option!");
        authCabAndGo("crm/edit-client/id/" + clientId);
        pagesInit.getCrmClients().setStateIdenfyVerificationForClient(false);
        authCabAndGo("crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(false),
                "FAIL -> Check Idenfy for client after disabling!");
        authDashAndGo("idenfy-client-1@mgid.com", "profile/users");
        softAssert.assertFalse(pagesInit.getUserProfile().checkProfileIdenfyTabIsDisplayed(),
                "FAIL -> Idenfy tab is visible with disabled option!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Add few labels into the CAB project")
    @Description("Check clients Idenfy label icons and statuses\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52169\">Ticket TA-52169</a></li>\n" +
            "</ul>")
    @Test(dataProvider = "idenfyClients", description = "Check clients Idenfy label icons and statuses")
    public void checkIdenfyLabels(int clientId, String labelColor, String labelTitle) {
        log.info("Test is started");
        authCabAndGo("crm/index/?client_id=" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyLabelIconIsDisplayed(labelColor),
                "FAIL -> Wrong Idenfy label icon!");
        softAssert.assertEquals(pagesInit.getCrmClients().getIdenfyLabelTitle(labelColor), labelTitle,
                "FAIL -> Wrong Idenfy label title!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] idenfyClients() {
        return new Object[][]{
                {2052, "blue", "APPROVED"},
                {2053, "red", "DENIED"},
                {2054, "red", "SUSPECTED"},
                {2055, "red", "EXPIRED"},
                {2056, "red", "IN_PROGRESS"}
        };
    }


    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Add few labels into the CAB project")
    @Description("Check rights for Idenfy labels visibility\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52169\">Ticket TA-52169</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check rights for Idenfy labels visibility", dataProvider = "idenfyClients")
    @Privilege(name = "crm/can_kyc_verify_idenfy")
    public void checkIdenfyLabelsRights(int clientId, String labelColor, String labelTitle) {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_kyc_see_idenfy_info");
        authCabForCheckPrivileges("crm/index/?client_id=" + clientId);
        Assert.assertFalse(pagesInit.getCrmClients().checkIdenfyLabelIconIsDisplayed(labelColor),
                "FAIL -> Check rights Idenfy labels for client " + clientId);
        log.info("Test is finished");
    }

    @Epic("Idenfy")
    @Feature("Verifying client via Idenfy")
    @Story("CRM")
    @Description("Check rights of ability to AML verification from Cab\n" +
            "<ul>\n" +
            "  <li>Rights has disabled - button 'AML' has hidden</li>\n" +
            "  <li>Rights has enabled - button 'AML' has visible</li>\n" +
            "  <li>@see <a href=\"https://jira.mgid.com/browse/TA-52258\">Ticket TA-52258</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check rights of ability to AML verification from Cab")
    @Privilege(name = "crm/can_see_aml_verification")
    public void checkIdenfyButton() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_see_aml_verification");
        authCabForCheckPrivileges("crm");
        softAssert.assertFalse(pagesInit.getCrmClients().amlButtonIsDisplayed(),
                "FAIL -> AML button is visible with disabled rights!");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/can_see_aml_verification");
        authCabForCheckPrivileges("crm");
        softAssert.assertTrue(pagesInit.getCrmClients().amlButtonIsDisplayed(),
                "FAIL -> AML button isn't visible with enabled rights!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Idenfy")
    @Feature("Verifying client via Idenfy")
    @Story("CRM")
    @Description("Check AML verification by client`s name and surname\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52258\">Ticket TA-52258</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check AML verification by client`s name and surname")
    public void checkAmlPopupAmlVerification() {
        log.info("Test is started");
        String serviceType = "AML";
        String firstName = "Elon";
        String surname = "Musk";
        authCabAndGo("crm");
        pagesInit.getCrmClients().clickAmlButton();
        pagesInit.getCrmClients().chooseAmlVariant();
        pagesInit.getCrmClients().fillClientName(firstName);
        pagesInit.getCrmClients().fillClientSurname(surname);
        serviceInit.getIdenfyMock().prepareIdenfyResponse(AML_CHECK, "*verify-aml*");
        pagesInit.getCrmClients().clickVerifyButton();
        serviceInit.getIdenfyMock().countDownLatchAwait(1);
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(serviceType), serviceType,
                "FAIL -> Service type check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(firstName), firstName,
                "FAIL -> First name check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(surname), surname,
                "FAIL -> Surname check!");
        softAssert.assertTrue(pagesInit.getCrmClients().checkAmlResponseData("AML"),
                "FAIL -> AML response is empty!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Idenfy")
    @Feature("Verifying client via Idenfy")
    @Story("CRM")
    @Description("Check AML Company verification\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52258\">Ticket TA-52258</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check AML Company verification")
    public void checkAmlPopupCompanyVerification() {
        log.info("Test is started");
        String serviceType = "COMPANY";
        String companyName = "Mgid";
        authCabAndGo("crm");
        pagesInit.getCrmClients().clickAmlButton();
        pagesInit.getCrmClients().chooseCompanyIdentification();
        pagesInit.getCrmClients().fillCompanyName(companyName);
        String countryValue = pagesInit.getCrmClients().selectCountryValue();
        serviceInit.getIdenfyMock().prepareIdenfyResponse(COMPANY_CHECK, "*verify-aml*");
        pagesInit.getCrmClients().clickVerifyButton();
        serviceInit.getIdenfyMock().countDownLatchAwait(1);
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(serviceType), serviceType,
                "FAIL -> Service type check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(companyName), companyName,
                "FAIL -> Company name check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(countryValue), countryValue,
                "FAIL -> Country value check!");
        softAssert.assertTrue(pagesInit.getCrmClients().checkAmlResponseData("COMPANY"),
                "FAIL -> AML Company response is empty!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("Idenfy")
    @Feature("Verifying client via Idenfy")
    @Story("CRM")
    @Description("Check AML LID verification\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52258\">Ticket TA-52258</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check AML LID verification")
    public void checkAmlPopupLidVerification() {
        log.info("Test is started");
        String serviceType = "LID";
        String documentType = "PASSPORT";
        String documentNumber = "74587539";
        String countryValue = "LT";
        authCabAndGo("crm");
        pagesInit.getCrmClients().clickAmlButton();
        pagesInit.getCrmClients().chooseLidIdentification();
        pagesInit.getCrmClients().fillDocumentNumber(documentNumber);
        serviceInit.getIdenfyMock().prepareIdenfyResponse(LID_CHECK, "*verify-aml*");
        pagesInit.getCrmClients().clickVerifyButton();
        serviceInit.getIdenfyMock().countDownLatchAwait(1);
        log.info("documentNumber - " + serviceInit.getIdenfyMock().getRequestBody());
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(serviceType), serviceType,
                "FAIL -> Service type check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(documentType), documentType,
                "FAIL -> Document type check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(documentNumber), documentNumber,
                "FAIL -> Document number check!");
        softAssert.assertEquals(serviceInit.getIdenfyMock().getRequestedParameter(countryValue), countryValue,
                "FAIL -> Country value check!");
        softAssert.assertTrue(pagesInit.getCrmClients().checkAmlResponseData("LID"),
                "FAIL -> AML LID response is empty!");
        softAssert.assertAll();
    log.info("Test is finished");
    }

    @Epic("Idenfy")
    @Feature("Turn on Idenfy's verification if client is self-registered")
    @Story("CRM")
    @Description("Check that Idenfy's verification is turned on if client is self-registered\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52289\">Ticket TA-52289</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check Idenfy's verification is turned on if client is self-registered")
    public void checkTurnOnIdenfyVerificationForSelfRegisterClient() {
        log.info("Test is started");
        operationMySql.getMailPull().getCountLettersByClientEmail(NEW_CLIENT_LOGIN_MGID);
        log.info("Register new Advertiser!");
        open(mgidLink + "/user/signup");
        pagesInit.getSignUp().registerAdvertiser(NEW_CLIENT_LOGIN_MGID);
        log.info("Get activation link from email and follow it");
        open(mgidLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(NEW_CLIENT_LOGIN_MGID)).get(0).substring(mgidLink.length()));
        pagesInit.getSignUp().createPassword(NEW_CLIENT_PASSWORD);
        Assert.assertEquals(operationMySql.getClients().getCanVerifyIdenfyByName(NEW_CLIENT_LOGIN_MGID), "1",
                "FAIL -> Idenfy verification is not turned on for self register client!");
        log.info("Test is started");
    }

    @Epic("Idenfy")
    @Feature("IDenfy. Suspected friends")
    @Story("CRM")
    @Description("Check that Idenfy verification is turned on for clients with Suspected friends\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52288\">Ticket TA-52288</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check that Idenfy verification is turned on for clients with Suspected friends")
    public void checkEnablingIdenfyVerificationForClientWithSuspectedFriends() {
        log.info("Test is started");
        int clientId = 2062;
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(false),
                "FAIL -> Default Idenfy state!");
        serviceInit.getDockerCli().runAndStopCron(IDENFY_SUSPECTED_CRON, "-vvv");
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(true),
                "FAIL -> Idenfy state for Suspected friends!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("IDenfy")
    @Feature("Idenfy. Suspected friends with enabled auto moderation")
    @Story("CRM")
    @Description("Check that Idenfy verification is turned Off for clients with Suspected friends but with enabled Auto Moderation\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52288\">Ticket TA-52288</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check that Idenfy verification is turned Off for clients with Suspected friends but with enabled Auto Moderation")
    public void checkEnablingIdenfyVerificationForClientWithSuspectedFriends_AutoModeration() {
        log.info("Test is started");
        int clientId = 2063;
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(false),
                "FAIL -> Default Idenfy state!");
        serviceInit.getDockerCli().runAndStopCron(IDENFY_SUSPECTED_CRON, "-vvv");
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkIdenfyState(false),
                "FAIL -> Idenfy state for Suspected friends with Auto Moderation enabled!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("IDenfy")
    @Feature("Idenfy. Auto moderation for suspected friends")
    @Story("CRM")
    @Description("Check editing and saving 'Auto moderation for (suspected friends)' settings for client\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52288\">Ticket TA-52288</a></li>\n" +
            "</ul>")
    @Owner(value = "AIA")
    @Test(description = "Check editing and saving 'Auto moderation for (suspected friends)' settings for client")
    public void checkIdenfyVerificationAutoModeration() {
        log.info("Test is started");
        int clientId = 2063;
        authCabAndGo("/crm/edit-client/id/" + clientId);
        pagesInit.getCrmClients().changeStateAutoModerationCheckbox(false).saveSettings();
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkAutoModerationCheckboxState(false),
                "FAIL -> Auto Moderation for Suspected friends is enabled after disabling!");
        pagesInit.getCrmClients().changeStateAutoModerationCheckbox(true).saveSettings();
        authCabAndGo("/crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkAutoModerationCheckboxState(true),
                "FAIL -> Auto Moderation for Suspected friends is disabled after enabling!");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Exception client group. Add new item Liability")
    @Description("Check rights for disabling Liability\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52419\">Ticket TA-52419</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check rights for disabling Liability")
    @Privilege(name = "crm/can_edit_liability")
    public void checkLiabilityRights() {
        log.info("Test is started");
        try {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_edit_liability");
            authCabForCheckPrivileges("crm/edit-client/id/2001");
            Assert.assertFalse(pagesInit.getCrmClients().checkLiabilityIsDisplayed(),
                    "FAIL -> Check rights Idenfy for client!");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/can_edit_liability");
        }
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Exception client group. Add new item Liability")
    @Description("Check that Liability settings is saved correctly\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52419\">Ticket TA-52419</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that Liability settings is saved correctly")
    public void checkEditLiabilitySettingsForClient() {
        log.info("Test is started");
        int clientId = 2069;
        authCabAndGo("crm/edit-client/id/" + clientId);
        pagesInit.getCrmClients().setLiabilitySettings().saveSettings();
        authCabAndGo("crm/edit-client/id/" + clientId);
        Assert.assertTrue(pagesInit.getCrmClients().checkLiabilitySettings(),
                "FAIL -> Check Liability for client after editing!");
        log.info("Test is finished");
    }

    @Feature("IDenfy integration.KYC")
    @Story("Idenfy. Exception client group. Add new item Liability")
    @Description("Check that Postponed liability settings is saved correctly\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52419\">Ticket TA-52419</a></li>\n" +
            "</ul>")
    @Owner("AIA")
    @Test(description = "Check that Liability settings is saved correctly - Postponed")
    public void checkEditLiabilitySettingsForClient_Postponed() {
        log.info("Test is started");
        int clientId = 2069;
        authCabAndGo("crm/edit-client/id/" + clientId);
        pagesInit.getCrmClients().setLiabilitySettings("Postponed")
                .setLiabilityDate()
                .setLiabilitySpend()
                .saveSettings();
        authCabAndGo("crm/edit-client/id/" + clientId);
        softAssert.assertTrue(pagesInit.getCrmClients().checkLiabilitySettings(),
                "FAIL -> Check Liability for client after editing!");
        softAssert.assertTrue(pagesInit.getCrmClients().checkLiabilityPostponedDate(),
                "FAIL -> Check Liability postponed date for client after editing!");
        softAssert.assertTrue(pagesInit.getCrmClients().checkLiabilityPostponedSpent(),
                "FAIL -> Check Liability postponed spent for client after editing!");
        softAssert.assertAll();
        log.info("Test is finished");
    }
}