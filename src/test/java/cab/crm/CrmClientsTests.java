package cab.crm;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import core.service.customAnnotation.Privilege;
import testBase.TestBase;

import java.util.Map;

import static core.service.constantTemplates.ConstantsInit.*;
import static pages.cab.crm.helper.CrmAddHelper.PaymentTerms.*;
import static pages.cab.crm.variables.CrmVariables.*;
import static testData.project.AuthUserCabData.AuthorizationUsers.AA_USER;
import static testData.project.AuthUserCabData.advertisingAgencyUser;
import static testData.project.EndPoints.clientWagesEditUrl;
import static testData.project.RoleIdsCab.AUTOTEST_ROLE_PRIVILEGE;

public class CrmClientsTests extends TestBase {
    /**
     * Create client with default settings
     * Check in edit interface:
     * <ul>
     * <li>Don't displayed block 'Cooperation type' (client created without site
     *         @see <a href="https://youtrack.mgid.com/issue/MT-4243">MT-4243</a>)</li>
     * <li>Enabled option "Can work with Payoneer system"</li>
     * </ul>
     * <p>Author RKO/AIA<p/>
     */
    @Test
    public void createClient() {
        log.info("Test is started");

        log.info("create client");
        authCabAndGo("crm/add");
        Assert.assertNotNull(pagesInit.getCrmClients().setPaymentTerms(MIXED_PAYMENT).createClient(), "FAIL -> client isn't create");

        log.info("check crm client in list interface");
        softAssert.assertTrue(pagesInit.getCrmClients().checkClientInListInterface(), "FAIL -> check crm client in list interface");

        log.info("check crm client in edit interface");
        softAssert.assertFalse(operationMySql.getClients().getOptions(pagesInit.getCrmClients().getClientId()).contains("allowRussianDomains"), "FAIL - rotate options");

        log.info("check presence of rotation option");
        authCabAndGo("crm/edit-client/id/" + pagesInit.getCrmClients().getClientId());
        softAssert.assertTrue(pagesInit.getCrmClients().checkClientInEditInterface(), "FAIL -> check crm client in edit interface");
        authCabAndGo(clientWagesEditUrl + pagesInit.getCrmClients().getClientId());
        softAssert.assertTrue(pagesInit.getCabPublisherClients().checkClientCanWorkWithPayoneerSystemOption(),
                "FAIL -> work with Payoneer system disabled!");
        softAssert.assertFalse(pagesInit.getCabPublisherClients().checkDisableAdTypesAfterCreateClient(),
                "FAIL -> checkbox Ad Types is enabled!");
        softAssert.assertAll();

        log.info("Test is finished");
    }

    /**
     * Create client with Adskeeper and check use_cloudinary flag
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27506">TA-27506</a>
     * Update @see <a href="https://youtrack.mgid.com/issue/MT-5148">MT-5148</a>
     * <p>NIO<p/>
     */
    @Test
    public void checkCloudinaryFlagAdskeeper() {
        log.info("Test is started");
        log.info("create client");
        authCabAndGo("crm/add");
        Assert.assertNotNull(pagesInit.getCrmClients().setSubnet("{\"subnet\":2,\"mirror\":null}").setPaymentTerms(PRE_PAYMENT).createClient(), "FAIL -> client isn't create");
        log.info("check client's flag");
        Assert.assertEquals(operationMySql.getClients().getCloudinaryFlag(pagesInit.getCrmClients().getClientId()), "1");
        log.info("Test is finished");
    }

    /**
     * <a href="https://youtrack.mgid.com/issue/TA-23873">https://youtrack.mgid.com/issue/TA-23873</a>
     * Ошибки на странице CRM - Клиенты - Добавить клиента под РА
     * <p>
     * create client RA with webSite
     * check client RA in list and edit 'Product client' interface
     * <p>
     * RKO
     */
    @Test
    public void createClientRa() {
        log.info("Test is started");

        log.info("create client RA");
        authCabAndGo(AA_USER, "crm/add");
        pagesInit.getCrmClients()
                .setProductsCurator(advertisingAgencyUser)
                .setPaymentTerms(MIXED_PAYMENT)
                .createClientRa();

        Assert.assertNotNull(pagesInit.getCabProductClients().readClientId(), "FAIL -> client RA doesn't create");

        pagesInit.getCabProductClients().setEmail(pagesInit.getCrmClients().getEmail())
                .setLogin(pagesInit.getCrmClients().getLogin());

        log.info("check client RA in list interface");
        softAssert.assertTrue(pagesInit.getCabProductClients().checkClientRaInListInterface(), "FAIL -> check client RA in 'product client' list interface");

        log.info("check client RA in edit interface");
        authCabAndGo(AA_USER, "goodhits/clients-edit/id/" + pagesInit.getCabProductClients().getClientId());
        softAssert.assertTrue(pagesInit.getCabProductClients().checkClientRaInEditInterface(), "FAIL -> check client RA in 'product client' edit interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    /**
     * Check creating client with disabled privilege 'crm/client_subnet_can_see'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27302">TA-27302</a>
     * <p>AIA</p>
     */
    @Privilege(name = "crm/client_subnet_can_see")
    @Test
    public void createClientWithoutSeeSubnet() {
        log.info("Test is started!");
        try {
            log.info("create first client");
            log.info("switch OFF privilege 'crm/client_subnet_can_see'");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/client_subnet_can_see");

            authCabForCheckPrivileges("crm/add");
            Assert.assertNotNull(pagesInit.getCrmClients().setCanSeeSubnet(false).setPaymentTerms(PRE_PAYMENT).createClient(), "FAIL -> client isn't create");

            log.info("check first crm client in list interface");
            Assert.assertTrue(pagesInit.getCrmClients().checkClientInListInterface(), "FAIL -> check first crm client in list interface");

        } finally {
            log.info("switch ON privilege 'crm/client_subnet_can_see'");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/client_subnet_can_see");
        }
        log.info("Test is finished");
    }

    /**
     * Check creating client with disabled privilege 'crm/crm_add_inherit_user_subnet'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27302">TA-27302</a>
     * <p>AIA</p>
     */
    @Privilege(name = "crm/crm_add_inherit_user_subnet")
    @Test
    public void createClientWithoutInheritSubnet() {
        log.info("Test is started!");
        log.info("create second client");
        authCabForCheckPrivileges("crm/add");
        Assert.assertNotNull(pagesInit.getCrmClients().setCanSeeSubnet(false).setPaymentTerms(MIXED_PAYMENT).createClient(), "FAIL -> client isn't create");

        log.info("check second crm client in list interface");
        Assert.assertTrue(pagesInit.getCrmClients().checkClientInListInterface(), "FAIL -> check second crm client in list interface");
        log.info("Test is finished!");
    }

    /**
     * Check creating client with disabled both privileges 'crm/client_subnet_can_see' and 'crm/crm_add_inherit_user_subnet'
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27302">TA-27302</a>
     * <p>AIA</p>
     */
    @Privilege(name = {"crm/client_subnet_can_see", "crm/crm_add_inherit_user_subnet"})
    @Test
    public void createClientWithoutSubnet() {
        log.info("Test is started!");
        try {
            log.info("create 3rd client");
            log.info("switch OFF privileges 'crm/client_subnet_can_see', 'crm/crm_add_inherit_user_subnet'");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/client_subnet_can_see");

            authCabForCheckPrivileges("crm/add");
            Assert.assertNotNull(pagesInit.getCrmClients().setCanSeeSubnet(false).setPaymentTerms(POST_PAYMENT).createClient(), "FAIL -> 3rd client isn't create");

            log.info("check 3rd crm client in list interface");
            Assert.assertTrue(pagesInit.getCrmClients().checkClientInListInterface(), "FAIL -> check 3rd crm client in list interface");
        } finally {
            log.info("switch ON privilege 'crm/client_subnet_can_see'");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/client_subnet_can_see");
        }
        log.info("Test is finished!");
    }

    /**
     * Блокирование/разблокирование клиента
     * <ul>
     * <li>Проверка иконки блокировки</li>
     * <li>Проверка отправки письма куротору о блокировке</li>
     * <li>Проверка отправки письма клиенту о блокировке</li>
     * <li>Проверка смены флага авто выплат</li>
     * <li>Проверка отправки письма клиенту о разблокировке</li>
     * </ul>
     *
     * @see <a href="https://youtrack.mgid.com/issue/TA-27223">Ticket TA-27223</a>
     * <p>NIO</p>
     */
    @Test
    public void checkBlockingClient() {
        log.info("Test is started!");

        log.info("Change option visibility");
        operationMySql.getMailPull().getCountLettersBySubject("Your client was blocked", "Account has been blocked");
        authCabAndGo("crm/edit-client/id/1128");
        pagesInit.getCrmClients().setBlockedClient(true).saveSettings();
        softAssert.assertTrue(pagesInit.getCrmClients().checkBlockedIconVisibility(), "FAIL - icon visibility after block");

        log.info("Check mail curator");
        softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMail("Your client was blocked"), messageForCuratorBlockedClient, "FAIL -> message for curator");

        log.info("Check mail client");
        softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMail("Account has been blocked"), messageForClientBlockedAccount, "FAIL -> message for client block");

        log.info("Check auto payment flag");
        softAssert.assertEquals(operationMySql.getClientsAutoBilling().getAutoPaymentFlag("1128"), 0, "FAIL -> auto payment");

        log.info("Check mail after unblock client");
        operationMySql.getMailPull().getCountLettersBySubject("Account has been unblocked");
        authCabAndGo("crm/edit-client/id/1128");
        pagesInit.getCrmClients().setBlockedClient(false).saveSettings();
        softAssert.assertFalse(pagesInit.getCrmClients().checkBlockedIconVisibility(), "FAIL - icon visibility after unblock");
        softAssert.assertEquals(operationMySql.getMailPull().getBodyFromMail("Account has been unblocked"), messageForClientUnBlockedAccount, "FAIL -> message for client unblock");
        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_CRM_LIST_INTERFACE)
    @Description("Check prepayment terms fields validation\n" +
            "<ul>\n" +
            "   <li>'Payment terms' field for empty value</li>\n" +
            "   <li>'Payment terms NET' field for empty value</li>\n" +
            "   <li>'Add terms files' upload files for empty value</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52121\">Ticket TA-52121</a></li>\n" +
            "</ul>")
    @Owner("NIO")
    @Test(description = "Check prepayment terms fields validation")
    public void checkPrePaymentOptionValidation() {
        log.info("Test is started!");

        log.info("check 'Payment terms' field validation for empty value");
        authCabAndGo("crm/add");
        pagesInit.getCrmClients().createClient();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Payment terms can't be empty"), "FAIL -> 'Payment terms'");

        log.info("check 'Payment terms NET' field validation for empty value with POST_PAYMENT");
        pagesInit.getCrmClients().choosePaymentTerms(POST_PAYMENT);
        pagesInit.getCrmClients().setPaymentTermsNet("2");
        pagesInit.getCrmClients().saveSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Payment terms file(s) and payment terms NET value can't be empty"), "FAIL -> 'Payment terms NET'");

        log.info("check 'Add terms files' field validation for empty value with POST_PAYMENT");
        authCabAndGo("crm/add");
        softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsField(), "Fail - displayed payment terms");
        pagesInit.getCrmClients().enableProductDirection();
        pagesInit.getCrmClients().choosePaymentTerms(POST_PAYMENT);
        pagesInit.getCrmClients().openPaymentTermFilesPopup();
        pagesInit.getCrmClients().setPaymentTermsFile("skill.pdf");
        pagesInit.getCrmClients().saveSettingsOfPaymentTerms();
        pagesInit.getCrmClients().createClient();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Payment terms file(s) and payment terms NET value can't be empty"), "FAIL -> 'Add terms files' POST_PAYMENT");

        log.info("check 'Add terms files' field validation for empty value with MIXED_PAYMENT");
        pagesInit.getCrmClients().choosePaymentTerms(MIXED_PAYMENT);
        pagesInit.getCrmClients().saveSettings();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("Payment terms file(s) can't be empty"), "FAIL -> 'Add terms files' MIXED_PAYMENT");

        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_CRM_LIST_INTERFACE)
    @Description("Check prepayment terms privileges\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52121\">Ticket TA-52121</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Privilege(name = {"crm/remove-payment-terms-file", "crm/upload-payment-terms-file", "crm/can_set_payment_terms", "load-data-dialog-payment-terms", "save-data-dialog-payment-terms"})
    @Test(description = "Check prepayment terms privileges")
    public void checkPrivilegePrePaymentOption() {
        log.info("Test is started!");

        try {

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_edit_payment_terms");

            authCabForCheckPrivileges("crm/index/?client_id=1128");
            softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsIcon(), "Fail - can_edit_payment_terms");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/can_edit_payment_terms");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/upload-payment-terms-file");
            authCabForCheckPrivileges("crm/index/?client_id=1128");
            softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsIcon(), "Fail - upload-payment-terms-file");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/upload-payment-terms-file");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/remove-payment-terms-file");
            authCabForCheckPrivileges("crm/index/?client_id=1128");
            softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsIcon(), "Fail - remove-payment-terms-file");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/remove-payment-terms-file");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/save-data-dialog-payment-terms");
            authCabForCheckPrivileges("crm/index/?client_id=1128");
            softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsIcon(), "Fail - save-data-dialog-payment-terms");

            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/save-data-dialog-payment-terms");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/load-data-dialog-payment-terms");
            authCabForCheckPrivileges("crm/index/?client_id=1128");
            softAssert.assertFalse(pagesInit.getCrmClients().isDisplayedPaymentTermsIcon(), "Fail - load-data-dialog-payment-terms");

            softAssert.assertAll();
            log.info("Test is finished!");
        } finally {
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/can_edit_payment_terms");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/upload-payment-terms-file");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/remove-payment-terms-file");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/save-data-dialog-payment-terms");
            helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(true, AUTOTEST_ROLE_PRIVILEGE, "crm/load-data-dialog-payment-terms");
        }
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_CRM_LIST_INTERFACE)
    @Description("Check edit options prepayment terms by icon \n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52121\">Ticket TA-52121</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check edit options prepayment terms by icon")
    public void checkEditPaymentTermsOptionByIcon() {
        log.info("Test is started!");
        Map<String, String> data = Map.of("file", "table.pdf", "net", "5");

        authCabAndGo("crm/index/?client_id=1132");
        pagesInit.getCrmClients().clickPaymentTermsIcon();

        pagesInit.getCrmClients().deletePaymentTermsFile();
        pagesInit.getCrmClients().choosePaymentTerms(MIXED_PAYMENT);
        pagesInit.getCrmClients().setPaymentTermsNet(data.get("net"));
        pagesInit.getCrmClients().setPaymentTermsFile(data.get("file"));
        pagesInit.getCrmClients().saveSettingsOfPaymentTermsPopup();

        authCabAndGo("crm/index/?client_id=1132");
        pagesInit.getCrmClients().clickPaymentTermsIcon();
        Assert.assertTrue(pagesInit.getCrmClients().checkPaymentTerms(MIXED_PAYMENT, data));
        log.info("Test is finished!");
    }

    @Epic(CLIENTS)
    @Feature(PAYMENT_INFORMATION)
    @Story(CLIENT_CRM_LIST_INTERFACE)
    @Description("Check prepayment terms fields validation at popup\n" +
            "<ul>\n" +
            "   <li>'Payment terms NET' field for empty value</li>\n" +
            "   <li>'Add terms files' upload files for empty value</li>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52121\">Ticket TA-52121</a></li>\n" +
            "   <li><p>Author NIO</p></li>\n" +
            "</ul>")
    @Test(description = "Check prepayment terms fields validation at popup")
    public void checkPrePaymentOptionValidationAtPopup() {
        log.info("Test is started!");

        log.info("check 'Payment terms' field validation for empty value");
        authCabAndGo("crm/index/?client_id=1135");
        pagesInit.getCrmClients().clickPaymentTermsIcon();

        log.info("check 'Add terms files' field validation for empty value with MIXED_PAYMENT");
        pagesInit.getCrmClients().choosePaymentTerms(MIXED_PAYMENT);
        pagesInit.getCrmClients().saveSettingsOfPaymentTermsPopup();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is mandatory to add a PDF postpaid document and, if necessary, a NET value"), "FAIL -> 'Add terms files' MIXED_PAYMENT");

        log.info("check 'Payment terms NET' field validation for empty value with POST_PAYMENT");
        pagesInit.getCrmClients().choosePaymentTerms(POST_PAYMENT);
        pagesInit.getCrmClients().setPaymentTermsFile("skill.pdf");
        pagesInit.getCrmClients().saveSettingsOfPaymentTermsPopup();
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is mandatory to add the postpaid PDF document and the NET value"), "FAIL -> 'Payment terms NET'");

        log.info("check 'Add terms files' field validation for empty value with POST_PAYMENT");
        pagesInit.getCrmClients().choosePaymentTerms(POST_PAYMENT);
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkPrePaymentOptionValidationAtPopup_1");
        pagesInit.getCrmClients().deletePaymentTermsFile();
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkPrePaymentOptionValidationAtPopup_2");
        pagesInit.getCrmClients().setPaymentTermsNet("3");
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkPrePaymentOptionValidationAtPopup_3");
        pagesInit.getCrmClients().saveSettingsOfPaymentTermsPopup();
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkPrePaymentOptionValidationAtPopup_4");
        softAssert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab("It is mandatory to add the postpaid PDF document and the NET value"), "FAIL -> 'Add terms files' POST_PAYMENT");
        serviceInit.getScreenshotService().takeScreenshotOfFailedTest("checkPrePaymentOptionValidationAtPopup_5");

        softAssert.assertAll();
        log.info("Test is finished!");
    }

    @Story("Filters")
    @Description("Check work 'Filter By Login' filter in crm clients interface\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52124\">Ticket TA-52124</a></li>\n" +
            "</ul>")
    @Owner(value = "OHV")
    @Test(description = "Check work 'Filter By Login' filter in crm clients interface")
    public void checkFilterByLogin() {
        log.info("Test is started!");
        String login = "testemail1000@ex.ua";
        authCabAndGo("crm");
        pagesInit.getCrmClients().filterClientsByLogin(login);

        softAssert.assertEquals(pagesInit.getCrmClients().getClientLogin(),
                login, "FAIL -> get client login in crm clients interface");
        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                1, "FAIL -> get client amount in crm clients interface, first method");

        pagesInit.getCrmClients().filterClientsByLogin(login + "\uD83D\uDE02");

        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                0, "FAIL -> get client amount in crm clients interface, second method");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Filters")
    @Description("Check work 'Filter By Domain' filter in crm clients interface\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52124\">Ticket TA-52124</a></li>\n" +
            "</ul>")
    @Owner(value = "OHV")
    @Test(description = "Check work 'Filter By Domain' filter in crm clients interface")
    public void checkFilterByDomain() {
        log.info("Test is started!");
        String domain = "testsite7001.com";
        authCabAndGo("crm");
        pagesInit.getCrmClients().filterClientsByDomain(domain);

        softAssert.assertEquals(pagesInit.getCrmClients().getClientWebsite(),
                domain, "FAIL -> get client website in crm clients interface");
        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                1, "FAIL -> get client amount in crm clients interface, first method");

        pagesInit.getCrmClients().filterClientsByDomain(domain + "\uD83D\uDE02");

        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                0, "FAIL -> get client amount in crm clients interface, second method");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Story("Filters")
    @Description("Check work 'Source' filter in crm clients interface\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52124\">Ticket TA-52124</a></li>\n" +
            "</ul>")
    @Owner(value = "OHV")
    @Test(description = "Check work 'Source' filter in crm clients interface")
    public void checkSourceFilter() {
        log.info("Test is started!");
        String source = "mgid";
        authCabAndGo("crm");
        pagesInit.getCrmClients().filterClientsBySource(source);

        softAssert.assertEquals(pagesInit.getCrmClients().getClientSource(),
                source, "FAIL -> get client source in crm clients interface");
        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                1, "FAIL -> get client amount in crm clients interface, first method");

        pagesInit.getCrmClients().filterClientsBySource(source + "\uD83D\uDE02");

        softAssert.assertEquals(pagesInit.getCrmClients().getAmountClients(),
                0, "FAIL -> get client amount in crm clients interface, second method");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(LEGAL_ENTITY)
    @Story(CLIENT_CRM_CREATE_INTERFACE)
    @Description("Check client legal entity adding\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52382\">Ticket TA-52382</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Test(description = "Check client legal entity adding")
    public void createClientLegalEntity() {
        log.info("Test is started");
        log.info("create client");
        authCabAndGo("crm/add");
        pagesInit.getCrmClients().chooseLegalRelation("legal_entity");
        pagesInit.getCrmClients()
                .inputLegalEntityCompanyName()
                .selectLegalEntityBusinessType()
                .inputLegalEntityPersonName()
                .inputLegalEntityPersonEmail()
                .inputLegalEntityPersonPhone();

        Assert.assertNotNull(pagesInit.getCrmClients().setPaymentTerms(MIXED_PAYMENT).createClient(),
                "FAIL -> client isn't create");

        authCabAndGo("crm/edit-client/id/" + pagesInit.getCrmClients().getClientId());
        Assert.assertTrue(pagesInit.getCrmClients().checkClientLegalEntityInEditInterface(),
                "FAIL -> check crm client in edit interface");
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(LEGAL_ENTITY)
    @Story(CLIENT_CRM_EDIT_INTERFACE)
    @Description("Check self register legal entity client in cab CRM\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52382\">Ticket TA-52382</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Test(description = "Check self register legal entity client in cab CRM")
    public void checkSelfRegisteredClientLegalEntity() {
        log.info("Test is started");
        pagesInit.getCrmClients().setLegalEntityBusinessType("Publisher")
                .setLegalEntityContactPersonEmail("theodore.hane@yahoo.com")
                .setLegalEntityContactPersonName("Herman")
                .setLegalEntityCompanyName("Hamill-Morar");
        authCabAndGo("crm/edit-client/id/2070");
        Assert.assertTrue(pagesInit.getCrmClients().checkClientLegalEntityInEditInterface(),
                "FAIL -> check crm client in edit interface");
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(LEGAL_ENTITY)
    @Story(CLIENT_CRM_CREATE_INTERFACE)
    @Description("Check validations for legal entity client with empty fields\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52382\">Ticket TA-52382</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Test(description = "Check validations for legal entity client with empty fields")
    public void checkSelfRegisteredClientLegalEntityValidations() {
        log.info("Test is started");
        authCabAndGo("crm/add");
        pagesInit.getCrmClients().chooseLegalRelation("legal_entity");
        pagesInit.getCrmClients().setLegalEntityBusinessType("")
                .setLegalEntityContactPersonEmail("")
                .setLegalEntityContactPersonName("")
                .setLegalEntityCompanyName("");
        Assert.assertNull(pagesInit.getCrmClients().setPaymentTerms(MIXED_PAYMENT).createClient(),
                "FAIL -> Client was created with empty fields for legal entity!");

        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("The value cannot be empty"),
                "FAIL -> The value cannot be empty!");
        Assert.assertTrue(helpersInit.getMessageHelper().checkMessagesCab("FILL ALL FIELDS WITH APPROPRIATE VALID VALUES"),
                "FAIL -> FILL ALL FIELDS WITH APPROPRIATE VALID VALUES!");
        log.info("Test is finished");
    }

    @Epic(CLIENTS)
    @Feature(LEGAL_ENTITY)
    @Story(CLIENT_CRM_EDIT_INTERFACE)
    @Description("Check rights for legal entity block in client settings in cab CRM\n" +
            "<ul>\n" +
            "   <li>@see <a href=\"https://jira.mgid.com/browse/TA-52382\">Ticket TA-52382</a></li>\n" +
            "</ul>")
    @Owner(AIA)
    @Privilege(name = "crm/can_see_client_legal_entity_info")
    @Test(description = "Check rights for legal entity")
    public void checkSelfRegisteredClientLegalEntityPrivilege() {
        log.info("Test is started");
        helpersInit.getPrivilegesHelper().changeRolesPrivilegeCab(false, AUTOTEST_ROLE_PRIVILEGE, "crm/can_see_client_legal_entity_info");
        authCabForCheckPrivileges("crm/edit-client/id/2070");
        Assert.assertFalse(pagesInit.getCrmClients().checkLegalEntityIsDisplayed(),
                "FAIL -> Legal entity is displayed with disabled rights!");
        log.info("Test is finished");
    }
}