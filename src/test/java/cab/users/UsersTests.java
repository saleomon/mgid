package cab.users;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testBase.TestBase;

import static com.codeborne.selenide.Selenide.open;
import static core.service.constantTemplates.ConstantsInit.*;
import static libs.hikari.tableQueries.admin.Users.selectBankLegalEntitiesId;
import static testData.project.AuthUserCabData.AuthorizationUsers.*;
import static testData.project.EndPoints.cabLink;

public class UsersTests extends TestBase {
    private final String textForBlockedUserAA = "We have determined that you have not logged into MGID for more than 90 days. To ensure the security of your account, access to the MGID has been temporarily blocked. To continue working with your MGID account, contact please your Accompaining Manager\n" +
            "serg.Kovac@ex.ua";
    private final String textForBlockedUser = "Your account has been blocked. Please contact your administrator";
    @Epic("User profile")
    @Feature("User profile setting interface in CAB")
    @Story("Disabling Advertising Agency subnet after selection")
    @Description("Disabling Advertising Agency subnet after selection")
    @Owner(value = "Vlad Matkovskyi")
    @Test
    public void checkDisablingAdvertiserAgencySubnetTest(){
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "admin/edit/login/AA_MVV_test");
        pagesInit.getUsers().setAdvertiserAgencyManager("serg.Kovac")
                .setAdvertiserAgencySubnet()
                .saveUserSettings();
        softAssert.assertTrue(pagesInit.getUsers().checkDisablingSelectAaSubnet(), "Select Advertising Agency Subnet isn't disabled");
        softAssert.assertEquals(pagesInit.getUsers().getSubnetEntityId(), selectBankLegalEntitiesId("AA_MVV_test"), "Entities from interface and from DB isn't match");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic("User profile")
    @Feature("Bonuses system for Sales")
    @Story("Bonus Geo Team select")
    @Description("Check saved chosen options")
    @Owner(value = "NIO")
    @Test(description = "Check saved chosen options")
    public void checkEditBonusGeoTeamSelect(){
        log.info("Test is started");
        authCabAndGo( "admin/edit/login/user_auto");
        String[] countries = helpersInit.getBaseHelper().getRandomValuesFromList(operationMySql.getMaxmindGeoipCountries().getListCountries(), 4);
        pagesInit.getUsers().chooseBonusGeoTeam("sales_geo");
        helpersInit.getMultiFilterHelper().expandMultiFilter("geo-team-countries-config-tree");
        helpersInit.getMultiFilterHelper().clickValueInMultiFilter(countries);
        pagesInit.getUsers().saveUserSettings();

        authCabAndGo( "admin/edit/login/user_auto");
        Assert.assertTrue(pagesInit.getUsers().checkSelectedGeoTeamOptions(countries));
        log.info("Test is finished");
    }

    @Epic("User profile")
    @Feature("Bonuses system for Sales")
    @Story("Bonus Geo Team select")
    @Description("Check searching region by keyword")
    @Owner(value = "NIO")
    @Test(description = "Check searching region by keyword")
    public void checkSearchingByFieldBonusGeoTeamSelect(){
        log.info("Test is started");
        authCabAndGo( "admin/edit/login/user_auto");
        String searchKeyword = helpersInit.getBaseHelper().getRandomFromArray(new String[]{"dor", "alt", "ven", "rland"});

        pagesInit.getUsers().chooseBonusGeoTeam("sales_geo");
        pagesInit.getUsers().searchFieldOfBonusGeoTeam(searchKeyword);

        Assert.assertTrue(pagesInit.getUsers().checkMatchingSearchResultForGeoTeam(searchKeyword));
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(BLOCK_AUTHORIZATION)
    @Story(AUTHORIZATION_INTERFACE)
    @Description("users.login='fat.cat', role_users=53. Try to enable/disable access to Admin for this user")
    @Owner(NIO)
    //toDo NIO https://jira.mgid.com/browse/BT-6220 @Test(description = "Edit 'Access to Admin Panel'")
    public void checkEditingAccessToAdminPanel(){
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "admin/edit/login/fat.cat");
        pagesInit.getUsers().setAccessToAdminPanel(false)
                .setAdvertiserAgencyManager("serg.Kovac")
                .setAdvertiserAgencySubnet()
                .saveUserSettings();
        softAssert.assertTrue(pagesInit.getUsers().checkIsEnableAccessToAdminPanel(false), "Fail - checkIsEnableAccecToAdminPanel(false)");

        logoutCab(ADMIN_USER);
        pagesInit.getAuthorization().authorizeWithForm(AA_FAT_CAT);
        softAssert.assertEquals(pagesInit.getAuthorization().getDisplayedMessage(), textForBlockedUserAA, "Fail - getDisplayedMessage");

        authCabAndGo(ADMIN_USER, "admin/edit/login/fat.cat");
        pagesInit.getUsers().setAccessToAdminPanel(true)
                .saveUserSettings();
        softAssert.assertTrue(pagesInit.getUsers().checkIsEnableAccessToAdminPanel(true), "Fail - checkIsEnableAccecToAdminPanel(true)");

        logoutCab(ADMIN_USER);
        pagesInit.getAuthorization().authorizeWithForm(AA_FAT_CAT);
        softAssert.assertTrue(componentsInit.getAccountMenu().isDisplayedUserAccountLabel(), "Fail - isDisplayedUserAccountLabel");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(BLOCK_AUTHORIZATION)
    @Story(AUTHORIZATION_INTERFACE)
    @Description("users.login='brave.racoon', role_users=25. Try to log in to Admin for this user")
    @Owner(NIO)
    //toDo NIO https://jira.mgid.com/browse/BT-6220 @Test(description = "Edit 'Access to Admin Panel'")
    public void checkBlockedAccessToAdminPanelForAnyUserExceptAA(){
        log.info("Test is started");

        open(cabLink);
        pagesInit.getAuthorization().authorizeWithForm(BRAVE_RACOON);
        Assert.assertEquals(pagesInit.getAuthorization().getDisplayedMessage(), textForBlockedUser, "Fail - getDisplayedMessage");

        log.info("Test is finished");
    }

    @Epic("User profile")
    @Feature("Access to Admin Panel")
    @Description("users.login='happy.avocado', role_users=53, users.is_blocked=1")
    @Owner("NIO")
    @Test(description = "Check erase flag is blocked due changing AA role")
    public void checkEraseFlagIsBlockedClientDueChangingAARole(){
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "admin/edit/login/happy.avocado");
        pagesInit.getUsers().disableRole(53)
                .saveUserSettings();

        Assert.assertEquals(operationMySql.getUsers().getIsBlockedFlag("happy.avocado"), "1", "Fail - getIsBlockedFlag(false)");
        log.info("Test is finished");
    }

    @Epic("Restore password")
    @Feature("Password restoring in MGID cab")
    @Story("Main admin page")
    @Description("Check password restoring in MGID cab and login with new password")
    @Owner("AIA")
    //toDo AIA https://jira.mgid.com/browse/BT-6220 @Test(description = "Check password restoring in MGID cab and login with new password")
    public void checkRestorePasswordCab() {
        log.info("Test is started");
        String adminUserEmail = "user_for_recovery_pass@ukr.net";
        operationMySql.getMailPull().getCountLettersByClientEmail(adminUserEmail);
        open(cabLink + "auth/get-link-for-restore-password");
        pagesInit.getAuthorization().enterEmailForRestorePasswordAndSabmitIt(adminUserEmail);
        open(cabLink + helpersInit.getBaseHelper().extractUrls(
                operationMySql.getMailPull().getBodyFromMailByClientEmail(adminUserEmail)).get(0).substring(cabLink.length()));
                String emailBody = operationMySql.getMailPull().getListOfBodyFromMail("Change password in MGID system").get(0);
        pagesInit.getAuthorization().authorizeWithForm(RECOVERY_PASS, pagesInit.getAuthorization().getPasswordFromEmail(emailBody));
        Assert.assertTrue(componentsInit.getAccountMenu().isDisplayedUserAccountLabel(),
                "FAIL -> User is not login after restore password!");
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_MCM)
    @Story(USER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>managers_accounts.manager = 'SOK_RC'</li>\n" +
            "   <li>managers_accounts.mcm_aa = 0</li>\n" +
            "   <li>users.ra_currency = 'eur'</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52406\">Ticket TA-52406</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check agency changes to MCM agency with valid currency and rebate percent")
    public void checkAgencyChangesToMcmAgency() {
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "admin/edit/login/SOK_RC");
        pagesInit.getUsers()
                .useMcmAgency(true)
                .setMcmRebatePercent("15")
                .setMcmAdvertiserName("Strong AA 3")
                .saveUserSettings();

        helpersInit.getBaseHelper().refreshCurrentPage();

        softAssert.assertEquals(pagesInit.getUsers().getMcmRebatePercent(), "15" , "FAIL -> MCM Rebate Percent is displayed");
        softAssert.assertEquals(pagesInit.getUsers().getMcmAdvertiserName(), "Strong AA 3" , "FAIL -> MCM Advertiser Name is displayed");
        softAssert.assertTrue(pagesInit.getUsers().checkMcmAgencyCheckboxDisableState(true), "FAIL -> MCM AA checkbox is disable state");
        softAssert.assertTrue(pagesInit.getUsers().checkMcmAdvertiserNameInputDisableState(true), "FAIL -> MCM AA advertiser name input is disable state");
        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_MCM)
    @Story(USER_EDIT_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>managers_accounts.manager = 'AA_agency'</li>\n" +
            "   <li>managers_accounts.mcm_aa = 0</li>\n" +
            "   <li>users.ra_currency = 'usd'</li>\n" +
            "   <li>managers_accounts.manager = 'SOK_RD'</li>\n" +
            "   <li>managers_accounts.mcm_aa = 0</li>\n" +
            "   <li>users.ra_currency = 'eur'</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52406\">Ticket TA-52406</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(dataProvider = "notValidDataAgencyForChangeToMcmAgency",
            description = "Check agency changes to MCM agency with invalid currency, rebate percent and advertiser name")
    public void checkAgencyChangesToMcmAgencyWithInvalidData(String userLogin, String rebatePercentage, String mcmAdvertiser, String errorMessage) {
        log.info("Test is started");
        authCabAndGo(ADMIN_USER, "admin/edit/login/" + userLogin);
        pagesInit.getUsers()
                .useMcmAgency(true)
                .setMcmRebatePercent(rebatePercentage)
                .setMcmAdvertiserName(mcmAdvertiser)
                .saveUserSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage), "FAIL -> don't correct error message");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] notValidDataAgencyForChangeToMcmAgency() {
        return new Object[][]{
                {"AA_agency", "50", "Strong man", "Сurrency for MСM AA should be only EUR"},
                {"SOK_RD", "", "Strong man", "Only digits are allowed"},
                {"SOK_RD", "0.1", "Strong man", "Only digits are allowed"},
                {"SOK_RD", "test", "Strong man", "Only digits are allowed"},
                {"SOK_RD", "100", "Strong man", "MCM Rebate Percent can't be greater than 99%"},
                {"SOK_RD", "25", "", "MCM advertiser is required"},
                {"SOK_RD", "25", "Strong AA", "MCM advertiser name already existed"}
        };
    }

    @Epic(USERS)
    @Feature(AA_MCM)
    @Story(USER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>managers_accounts.mcm_aa = 1</li>\n" +
            "   <li>users.ra_currency = 'eur'</li>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52406\">Ticket TA-52406</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(description = "Check create user RA MCM in user edit interface")
    public void checkCreateUserRaMcm() {
        log.info("Test is started");
        String userLogin = "SOK_UA";
        String userEmail = "sok_ua@ex.ua";

        log.info("create user RA MCM");
        authCabAndGo(ADMIN_USER, "admin/add");

        pagesInit.getUsers().setNewUserLogin(userLogin)
                .checkNewUserLogin();
        softAssert.assertEquals(pagesInit.getUsers().getDisplayedMessage(),"Login is free", "FAIL -> get displayed success message" );

        pagesInit.getUsers().createNewUserAccount();
        pagesInit.getUsers().enableRole(53)
                .setEmail(userEmail)
                .setFullName(userLogin)
                .setUserActiveStatus(true)
                .setAdvertiserAgencyManager("prikhodko.alexander")
                .setAdvertiserAgencyType()
                .setAdvertiserAgencyCurrency("EUR")
                .setAdvertiserAgencySubnet()
                .setAccessToAdminPanel(true)
                .useMcmAgency(true)
                .setMcmRebatePercent("15")
                .setMcmAdvertiserName("Strong AA 5")
                .saveUserSettings();

        log.info("check user RA MCM in user edit interface");
        authCabAndGo(ADMIN_USER, "admin/edit/login/" + userLogin);
        softAssert.assertTrue(pagesInit.getUsers().checkStateMcmAgencyCheckbox(true), "FAIL -> checkStateMcmAgencyCheckbox");
        softAssert.assertEquals(pagesInit.getUsers().getMcmRebatePercent(), "15" , "FAIL -> MCM Rebate Percent is displayed");
        softAssert.assertEquals(pagesInit.getUsers().getMcmAdvertiserName(), "Strong AA 5" , "FAIL -> MCM Advertiser Name is displayed");
        softAssert.assertEquals(pagesInit.getUsers().getUserRaEmailInEdit(), userEmail, "FAIL -> get user email in user edit interface");

        softAssert.assertAll();
        log.info("Test is finished");
    }

    @Epic(USERS)
    @Feature(AA_MCM)
    @Story(USER_CREATE_CAB_INTERFACE)
    @Description("\n" +
            "<ul>\n" +
            "   <li>@see <a href \"https://jira.mgid.com/browse/TA-52406\">Ticket TA-52406</a></li>\n" +
            "</ul>")
    @Owner(HOV)
    @Test(dataProvider = "notValidDataForCreateMcmAgency",
            description = "Check create user RA MCM  with invalid currency, rebate percent and advertiser name")
    public void checkCreateUserRaMcmWithInvalidData(String userCurrency, String rebatePercentage, String mcmAdvertiser, String errorMessage) {
        log.info("Test is started");
        String userLogin = "SOK_UK";
        String userEmail = "sok_uk@ex.ua";

        authCabAndGo(ADMIN_USER, "admin/add");

        pagesInit.getUsers().setNewUserLogin(userLogin)
                .checkNewUserLogin();
        softAssert.assertEquals(pagesInit.getUsers().getDisplayedMessage(),"Login is free", "FAIL -> get displayed success message" );

        pagesInit.getUsers().createNewUserAccount();
        pagesInit.getUsers().enableRole(53)
                .setEmail(userEmail)
                .setFullName(userLogin)
                .setUserActiveStatus(true)
                .setAdvertiserAgencyManager("prikhodko.alexander")
                .setAdvertiserAgencyType()
                .setAdvertiserAgencyCurrency(userCurrency)
                .setAdvertiserAgencySubnet()
                .setAccessToAdminPanel(true)
                .useMcmAgency(true)
                .setMcmRebatePercent(rebatePercentage)
                .setMcmAdvertiserName(mcmAdvertiser)
                .saveUserSettings();

        Assert.assertTrue(helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage), "FAIL -> don't correct error message");
        log.info("Test is finished");
    }

    @DataProvider
    public Object[][] notValidDataForCreateMcmAgency() {
        return new Object[][]{
                {"UAH", "50", "Strong man", "Сurrency for MСM AA should be only EUR"},
                {"EUR", "", "Strong man", "Only digits are allowed"},
                {"EUR", "0.1", "Strong man", "Only digits are allowed"},
                {"EUR", "test", "Strong man", "Only digits are allowed"},
                {"EUR", "100", "Strong man", "MCM Rebate Percent can't be greater than 99%"},
                {"EUR", "25", "", "MCM advertiser is required"},
                {"EUR", "25", "Strong AA", "MCM advertiser name already existed"}
        };
    }

}
