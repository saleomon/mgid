package pages.dash.userProfile.logic;

import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.dash.userProfile.locators.UserProfileLocators.*;
import static core.helpers.BaseHelper.*;

public class UserProfile {
    private final HelpersInit helpersInit;

    public UserProfile (HelpersInit helpersInit){
        this.helpersInit = helpersInit;
    }

    public String getCurrentFirstDayOfTheWeek(){
        return FIRST_DAY_OF_THE_WEEK_TEXT.getText();
    }

    public void saveSettings(){
        SAVE_PROFILE_BUTTON.click();
        waitForAjax();
    }

    public void selectAnotherStartDayOfTheWeek(String day){
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(FIRST_DAY_OF_THE_WEEK_SELECT, day);
    }

    public String checkFirstCalendarDayInPublishersAndAdvertInterfaces(){
        CALENDAR_LOCATOR.shouldBe(visible).click();
        return CALENDAR_DAYS.first().text();
    }

    public void enableDisableNotificationCloakingCheckbox(boolean state){
        markUnMarkCheckbox(state, NOTIFICATION_DUE_BLOCKING_CLOAKING);
    }

    public boolean checkStateNotificationCloakingCheckbox(boolean state){
        return checkIsCheckboxSelected(state, NOTIFICATION_DUE_BLOCKING_CLOAKING);
    }

    public boolean checkProfileIdenfyTabIsDisplayed() {
        return IDENFY_TAB.isDisplayed();
    }

    public void goToProfileIdenfyTab() {
        IDENFY_TAB.shouldBe(visible).click();
    }

    public String getIdenfyVerificationStatus() {
        return IDENFY_STATUS_FIELD.shouldBe(visible).text();
    }
}
