package pages.dash.userProfile.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class UserProfileLocators {

    public static final SelenideElement ADVERTISERS_TAB = $("[href='/advertisers']");
    public static final SelenideElement PUBLISHERS_TAB = $("[href='/publisher']");

    ///////////////////////////////////////  General tab  /////////////////////////////////////////
    public static final SelenideElement FIRST_DAY_OF_THE_WEEK_TEXT = $("#cuselFrame-dashboardFirstDayWeek [class='cuselText']");
    public static final SelenideElement FIRST_DAY_OF_THE_WEEK_SELECT = $("#cusel-scroll-dashboardFirstDayWeek");

    public static final SelenideElement SAVE_PROFILE_BUTTON = $(".set-wrapper_btn a");

    public static final SelenideElement CALENDAR_LOCATOR = $(".calendar");
    public static final ElementsCollection CALENDAR_DAYS = $$(".jCalMo [class='dow']");

    public static final SelenideElement NOTIFICATION_DUE_BLOCKING_CLOAKING = $("[id='notificationBlockByCloaking']");

    ///////////////////////////////////////  Users tab  /////////////////////////////////////////

    ///////////////////////////////////////  Idenfy tab  /////////////////////////////////////////
    public static final SelenideElement IDENFY_TAB = $(".t8.verification");
    public static final SelenideElement IDENFY_STATUS_FIELD = $x(".//div/p[contains(text(), 'Verification status:')]");
    public static final SelenideElement INFORMATION_TEXT_FIELD = $("div.profile.tab8 p");

    ///////////////////////////////////////  Legal entity  /////////////////////////////////////////
    public static final SelenideElement LEGAL_RELATION_FIELD = $("#legalRelation");
    public static final SelenideElement ACCOUNT_CURRENCY_FIELD = $("#clientCurrency");
    public static final SelenideElement COUNTRY_OF_INCORPORATION_FIELD = $("#cuselFrame-billingCountry .cuselText");
    public static final SelenideElement BUSINESS_TYPE_FIELD = $("#businessType");
    public static final SelenideElement COMPANY_NAME_FIELD = $("#companyName");
    public static final SelenideElement CONTACT_PERSON_EMAIL_FIELD = $("#contactEmail");
    public static final SelenideElement CONTACT_PERSON_NAME_FIELD = $("#name");
}
