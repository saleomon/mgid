package pages.dash.signup.helpers;

import com.codeborne.selenide.Condition;
import core.base.HelpersInit;

import static core.helpers.BaseHelper.waitForAjaxLoader;
import static pages.dash.signup.locators.SignUpLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.ErrorsHelper.checkErrors;

public class SignUpHelper {
    private final HelpersInit helpersInit;

    public SignUpHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * Choose clients cooperation type
     */
    public void selectCooperationType(String cooperationType) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(COOPERATION_TYPE_SELECT, cooperationType);
    }

    /**
     * Fill domain value
     */
    public String inputDomain(String domainValue) {
        clearAndSetValue(DOMAIN_INPUT, domainValue);
        return domainValue;
    }

    /**
     * Fill login (email) value
     */
    public String inputEmail(String loginValue) {
        clearAndSetValue(EMAIL_INPUT, loginValue);
        return loginValue;
    }

    /**
     * Accept 'Terms and Conditions' and 'Privacy policy' checkbox
     */
    public void acceptTermsAndPolicyCheckbox() {
        ACCEPT_POLICY_CHECKBOX.setSelected(true);
    }

    /**
     * Accept 'Publishers Requirements' and 'Widgets Requirements' checkbox
     */
    public void acceptPublisherAndWidgetsCheckbox() {
        ACCEPT_PUBLISHER_REQUIREMENTS_CHECKBOX.setSelected(true);
    }

    /**
     * Submit popup
     */
    public void applySignUpForm() {
        APPLY_BUTTON.click();
        checkErrors();
    }

    /**
     * Get text from popup
     */
    public String getConfirmationPopupText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CONFIRMATION_MESSAGE.shouldBe(Condition.visible).getText());
    }

    /**
     * Fill password input
     */
    public void setPassword(String passwordValue) {
        clearAndSetValue(CREATE_PASSWORD_INPUT, passwordValue);
    }

    /**
     * Fill confirm password input
     */
    public void confirmPassport(String passwordValue) {
        clearAndSetValue(CONFIRM_PASSWORD_INPUT, passwordValue);
    }

    /**
     * Send password
     */
    public void savePassword() {
        SAVE_PASSWORD_BUTTON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    public void activatePassword() {
        ACTIVATE_PASSWORD_BUTTON.click();
        checkErrors();
    }
}
