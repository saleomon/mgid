package pages.dash.signup.variables;

public class SignUpPageVariables {
    public static String subjectRegistrationLetterIdealmediaIO = "Registration in Idealmedia Dashboard";
    public static String confirmationPopupTextMgid = "Congratulations! Your Account: %s has been registered successfully. We've sent you an email to activate your account. If it's not in your inbox, check your spam folder.";
    public static String confirmationPopupTextIdealmediaIO = "Congratulations! Your Account: %s has been registered successfully. We've sent you an email to activate your account. If it's not in your inbox, check your spam folder.";
    public static String confirmationPopupTextAdskeeper = "Congratulations! Your Account: %s has been registered successfully. We've sent you an email to activate your account. If it's not in your inbox, check your spam folder.";
}
