package pages.dash.signup.logic;

import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;
import pages.dash.signup.helpers.SignUpHelper;
import testData.project.Subnets;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static core.helpers.HelperLocators.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.signup.locators.SignUpLocators.*;
import static testData.project.ClientsEntities.*;

public class SignUp {
    private final HelpersInit helpersInit;
    private final SignUpHelper signUpHelper;
    private final Logger log;
    private String usersLogin;

    public SignUp(HelpersInit helpersInit, Logger log) {
        this.helpersInit = helpersInit;
        this.signUpHelper = new SignUpHelper(helpersInit);
        this.log = log;
    }

    /**
     * Select and fill all fields corresponding to Publisher cooperation type
     */
    public void registerPublisher(String domainValue, String loginValue) {
        signUpHelper.selectCooperationType("wages");
        signUpHelper.inputDomain(domainValue);
        usersLogin = signUpHelper.inputEmail(loginValue);
        signUpHelper.acceptTermsAndPolicyCheckbox();
        signUpHelper.acceptPublisherAndWidgetsCheckbox();
        signUpHelper.applySignUpForm();
    }

    /**
     * Select and fill all fields corresponding to Publisher cooperation type
     */
    public void registerNewClient(String loginValue) {
        usersLogin = signUpHelper.inputEmail(loginValue);
        signUpHelper.acceptTermsAndPolicyCheckbox();
        signUpHelper.applySignUpForm();
    }

    /**
     * Check text in confirmation popup
     */
    public boolean checkConfirmationPopup(String textValue) {
        return helpersInit.getBaseHelper().checkDatasetContains(
                String.format(textValue, usersLogin), signUpHelper.getConfirmationPopupText());
    }

    /**
     * Create password for new client
     */
    public void createPassword(String clientsPassword) {
        signUpHelper.setPassword(clientsPassword);
        signUpHelper.confirmPassport(clientsPassword);
        signUpHelper.savePassword();
    }

    public void activatePassword(String clientsPassword) {
        signUpHelper.setPassword(clientsPassword);
        signUpHelper.confirmPassport(clientsPassword);
        signUpHelper.activatePassword();
    }

    /**
     * Select and fill all fields corresponding to Advertiser cooperation type
     */
    public void registerAdvertiser(String loginValue) {
        signUpHelper.selectCooperationType("goodhits");
        usersLogin = signUpHelper.inputEmail(loginValue);
        signUpHelper.acceptTermsAndPolicyCheckbox();
        signUpHelper.applySignUpForm();
    }

    public void restorePassword(String email) {
        EMAIL_INPUT.sendKeys(email);
        RESTORE_PASSWORD_BUTTON.click();
        checkErrors();
    }

    public boolean isShowLoginDash() {
        return EMAIL_DASHBOARD_INPUT.isDisplayed();
    }

    public boolean waitVisibilityShowLoginDash() {
        EMAIL_DASHBOARD_INPUT.shouldBe(visible);
        return EMAIL_DASHBOARD_INPUT.isDisplayed();
    }
    public boolean isShowProfileDash() {
        return PROFILE_BLOCK.isDisplayed();
    }

    public void signInDash(String email, String password){
        EMAIL_DASHBOARD_INPUT.shouldBe(visible, ofSeconds(10)).sendKeys(email);
        PASSWORD_DASHBOARD_INPUT.sendKeys(password);
        SIGNIN_BUTTON.click();
        checkErrors();
        waitForAjax();
        TUI_LOADER.shouldBe(hidden, ofSeconds(20));
    }

    public void authDash(String userLogin, String link, Subnets.SubnetType subnet) {
        helpersInit.getAuthorizationHelper().goLinkDashboard(link, subnet);
        checkErrors();
        if (!isShowProfileDash()) {
            signInDash(userLogin, subnet);
            checkErrors();
        }
    }

    private void signInDash(String userLogin, Subnets.SubnetType subnet){
        String email;
        if(userLogin.equals(CLIENTS_EMAIL)){
            email = switch (subnet) {
                case SCENARIO_ADGAGE -> CLIENTS_ADGAGE_EMAIL;
                case SCENARIO_EPEEX -> CLIENTS_EPEEX_EMAIL;
                default -> userLogin;
            };
        }
        else {
            email = userLogin;
        }

        EMAIL_DASHBOARD_INPUT.shouldBe(visible, ofSeconds(10)).sendKeys(email);
        PASSWORD_DASHBOARD_INPUT.sendKeys(CLIENTS_PASSWORD);
        SIGNIN_BUTTON.click();
        PROFILE_BLOCK.shouldBe(visible, ofSeconds(15));
        log.info("user profile is displayed after authorization");
    }
}
