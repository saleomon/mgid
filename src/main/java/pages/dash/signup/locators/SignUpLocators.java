package pages.dash.signup.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SignUpLocators {
    public static final SelenideElement COOPERATION_TYPE_SELECT = $("#cusel-scroll-section");
    public static final SelenideElement DOMAIN_INPUT = $("#site");
    public static final SelenideElement EMAIL_INPUT = $("#email");
    public static final SelenideElement ACCEPT_POLICY_CHECKBOX = $("#accept");
    public static final SelenideElement ACCEPT_PUBLISHER_REQUIREMENTS_CHECKBOX = $("#acceptPublisherRequirements");
    public static final SelenideElement APPLY_BUTTON = $("#signup");

    public static final SelenideElement CREATE_PASSWORD_INPUT = $("#password");
    public static final SelenideElement CONFIRM_PASSWORD_INPUT = $("#passwordConfirm");
    public static final SelenideElement SAVE_PASSWORD_BUTTON = $("input#activate");
    public static final SelenideElement RESTORE_PASSWORD_BUTTON = $("[id=submit]");
    public static final SelenideElement ACTIVATE_PASSWORD_BUTTON = $("#activate");

    //Confirmation popup
    public static final SelenideElement TUI_LOADER = $("[automation-id='tui-loader__loader']");
    public static final SelenideElement CONFIRMATION_MESSAGE = $("[automation-id='tui-notification-alert__content']");
}
