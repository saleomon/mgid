package pages.dash.advertiser.helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.sorted.SortedType;

import java.util.Objects;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.TrafficInsightsLocators.TRAFFIC_INSIGHTS_ROWS;
import static core.helpers.BaseHelper.randomNumbersInt;

public class TrafficInsightsHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public TrafficInsightsHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public Object[] sortColumnAndGetDataFromColumn(SortedType.SortType sortType, String customColumn) {
        // choose sorted column header
        SelenideElement sortedColumnHeader = $x("//span[contains(.,'" + customColumn + "')]");
        sortedColumnHeader.shouldBe(Condition.visible).click();
        $("[role='progressbar']").shouldBe(hidden);
        for (int i = 0; i < 3; i++) {
            String attr = $("[class*='mat-column-" + customColumn.toLowerCase() + "'][aria-sort]").getAttribute("aria-sort");
            log.info(attr);
            if (!Objects.requireNonNull(attr).contains(sortType.getSortValue())) {
                log.info("click");
                sortedColumnHeader.click();
            }
        }
        return $$("td[class*='mat-column-" + customColumn.toLowerCase() + "']").texts().toArray();
    }

    public boolean checkSortedData(Object[] data, SortedType.SortType sortType) {
        return helpersInit.getSortedHelper().checkSortedData(sortType, data);
    }

    public int selectCountryFromList() {
        return randomNumbersInt(TRAFFIC_INSIGHTS_ROWS.size());
    }

    public String addCampaignForCountry(int rowId) {
        String countryName = TRAFFIC_INSIGHTS_ROWS.get(rowId).findElement(By.cssSelector(".mat-column-geo")).getText();
        log.info("countryName - " + countryName);
        TRAFFIC_INSIGHTS_ROWS.get(rowId).findElement(By.cssSelector(".mat-column-action")).click();
        return countryName;
    }
}