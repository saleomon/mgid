package pages.dash.advertiser.helpers;

import core.base.HelpersInit;

import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static core.helpers.BaseHelper.clearAndSetValue;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.*;
import static pages.dash.advertiser.locators.CampaignListLocators.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.dash.userProfile.locators.UserProfileLocators.*;

public class CampaignListHelper {
    private final HelpersInit helpersInit;

    public CampaignListHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }


    /**
     * блокирование кампании
     */
    public void blockCampaign(String id) {
        if ($("[class*='actions'] [class*='icon-pause'][data-id='" + id + "']").isDisplayed()) {
            $("[class*='actions'] [class*='icon-pause'][data-id='" + id + "']").click();
        }
//        waitForAjaxLoader();
    }

    /**
     * удаление кампании
     */
    public void deleteCampaign(String id) {
        $("[class*='actions'] [class*='icon-delete'][data-id='" + id + "']").shouldBe(visible).click();
        CONFIRM_BUTTON.shouldBe(visible).click();
        POPUP_TEXT.shouldBe(visible);
        POPUP_CLOSE.click();
        checkErrors();
//        waitForAjax();
    }

    /**
     * проверка отображения кампании в списке кампаний
     */
    public boolean isDisplayedRestoreCampaign(String id) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($("[class*='actions'] [class*='restore-campaign'][data-id='" + id + "']").isDisplayed());
    }

    /**
     * проверка отображения кампании в списке кампаний
     */
    public boolean isDisplayedCampaign(String name) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($("[title='" + name + "']").isDisplayed());
    }

    /**
     * Choose client country
     */
    public String setClientsCountry() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValSelectListDashJs(COUNTRY_SELECT));
    }

    /**
     * Choose legal entity client country except forbidden one
     */
    public String selectCorporateClientCountry(List<String> forbiddenCountries) {
        String selectedCountry;
        int i = 0;
        do {
            selectedCountry = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(COUNTRY_OF_INCORPORATION_SELECT);
            i++;
        } while(forbiddenCountries.contains(selectedCountry) && i < 3);
        return helpersInit.getBaseHelper().getTextAndWriteLog(selectedCountry);
    }

    public String selectCurrencyOfProfile() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValSelectListDashJs(CURRENCY_OF_PROFILE_SELECT));
    }

    public String selectBusinessType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValSelectListDashJs(BUSINESS_TYPE_SELECT));
    }

    public String setCompanyName() {
        String companyName = helpersInit.getGenerateDataHelper().generateCompanyName();
        clearAndSetValue(NAME_INPUT, companyName);
        return helpersInit.getBaseHelper().getTextAndWriteLog(companyName);
    }

    public String setContactPersonName() {
        String contactPerson = helpersInit.getGenerateDataHelper().generateName();
        clearAndSetValue(CONTACT_PERSON_NAME_INPUT, contactPerson);
        return helpersInit.getBaseHelper().getTextAndWriteLog(contactPerson);
    }

    public String setContactPersonEmail() {
        String contactPersonEmail = helpersInit.getGenerateDataHelper().generateEmail();
        clearAndSetValue(CONTACT_PERSON_EMAIL_INPUT, contactPersonEmail);
        return helpersInit.getBaseHelper().getTextAndWriteLog(contactPersonEmail);
    }

    /**
     * Save client data
     */
    public void saveAndFinish() {
        SAVE_AND_FINISH_BUTTON.click();
        checkErrors();
    }

    /**
     * Check clients login into dashboard on pages
     */
    public boolean checkLoginOnPage(String loginValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(USERS_LOGIN_FIELD.shouldBe(visible).text().contains(loginValue));
    }

    /**
     * Choose client cooperation type
     */
    public void chooseCooperationType(boolean isAdvertiser) {
        if (isAdvertiser) ADVERTISER_BUTTON.parent().shouldBe(visible).click();
        else PUBLISHER_BUTTON.parent().shouldBe(visible).click();
    }

    public boolean checkContactPersonName(String contactPersonNameValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(contactPersonNameValue, Objects.requireNonNull(CONTACT_PERSON_NAME_FIELD.getValue()));
    }

    public boolean checkContactPersonEmail(String contactPersonEmailValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(contactPersonEmailValue, Objects.requireNonNull(CONTACT_PERSON_EMAIL_FIELD.getValue()));
    }

    public boolean checkCompanyName(String companyNameValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(companyNameValue, Objects.requireNonNull(COMPANY_NAME_FIELD.getValue()));
    }

    public boolean checkCorporateClientCountry(String countryValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(countryValue, Objects.requireNonNull(COUNTRY_OF_INCORPORATION_FIELD.ancestor("div").find("input").getValue()));
    }

    public boolean checkBusinessType(String businessTypeValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(businessTypeValue, Objects.requireNonNull(BUSINESS_TYPE_FIELD.getValue()));
    }

    public boolean checkCurrencyOfProfile(String currencyValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(currencyValue, Objects.requireNonNull(ACCOUNT_CURRENCY_FIELD.getValue()));
    }

    public boolean checkLegalRelation(String relationValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(relationValue, Objects.requireNonNull(LEGAL_RELATION_FIELD.shouldBe(visible).getValue()));
    }
}
