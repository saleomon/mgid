package pages.dash.advertiser.helpers;

import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static pages.dash.advertiser.locators.AudiencesListLocators.AUDIENCE_ADD_BUTTON;
import static pages.dash.advertiser.locators.AudiencesListLocators.POP_UP_CLOSE;
import static core.helpers.BaseHelper.clickInvisibleElementJs;

public class AudiencesListHelper {

    private final HelpersInit helpersInit;

    public AudiencesListHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public String readAudienceId(String name) {
        if(POP_UP_CLOSE.isDisplayed()) {
            helpersInit.getBaseHelper().closePopup();
            POP_UP_CLOSE.shouldBe(hidden);
            return helpersInit.getBaseHelper().getTextAndWriteLog($x(".//tr[td[text()='" + name + "']]/td[1]").shouldBe(visible).text());
        }
        return null;
    }

    public void isAudienceEdit() {
        AUDIENCE_ADD_BUTTON.isDisplayed();
    }

    public boolean deleteAudience(String id) {
        clickInvisibleElementJs($(".trash-audience[data-id='" + id + "']").shouldBe(visible));
        helpersInit.getBaseHelper().isConfirmDisplayed();
        helpersInit.getBaseHelper().closePopup();
        return !$(".trash-audience[data-id='" + id + "']").exists();
    }
}
