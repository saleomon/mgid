package pages.dash.advertiser.helpers;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.AudiencesCreateLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class AudiencesCreateHelper {

    private final Logger log;
    private final HelpersInit helpersInit;

    public AudiencesCreateHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public String setName(String name) {
        String audienceName = name != null ? name : "audience name |-@!?(){}[]_+" + BaseHelper.getRandomWord(3);
        clearAndSetValue(NAME_INPUT.shouldBe(visible), audienceName);
        return audienceName;
    }

    public boolean checkName(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(NAME_INPUT.shouldBe(visible).val(), name);
    }

    public String setDescription(String description) {
        String audienceDescription = description != null ? description : "audience description ;:,.%#№" + BaseHelper.getRandomWord(2);
        clearAndSetValue(DESCRIPTION_INPUT, audienceDescription);
        return audienceDescription;
    }

    public boolean checkDescription(String description) {
        return helpersInit.getBaseHelper().checkDatasetEquals(DESCRIPTION_INPUT.val(), description);
    }

    public String chooseValidityPeriod(String period) {
        if (period != null) {
            helpersInit.getBaseHelper().selectCurrentValInSelectListJS(EXPIRE_SELECT, period);
            return period;
        } else {
            return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(EXPIRE_SELECT);
        }
    }

    public boolean checkValidityPeriod(String period) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(EXPIRE_SELECT, period);
    }

    public void chooseTargetType(String type) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(TARGET_TYPE_SELECT, type);
    }

    public String setEvent(String event) {
        String targetEvent = event != null ? event : "click" + BaseHelper.getRandomWord(4);
        clearAndSetValue(TARGET_EVENT_INPUT, targetEvent);
        return targetEvent;
    }

    public String setPagesToVisit(String pagesToVisit) {
        String targetPagesToVisit = pagesToVisit != null ? pagesToVisit : randomNumberFromRange(1, 100000);
        clearAndSetValue(MINIMUM_PAGES_TO_VISIT_INPUT, targetPagesToVisit);
        return targetPagesToVisit;
    }

    public String setDomainForTracking(String domainForTracking) {
        String targetDomainForTracking = domainForTracking != null ? domainForTracking : "domainForTracking" + BaseHelper.getRandomWord(2) + ".com";
        clearAndSetValue(DOMAIN_FOR_TRACKING_INPUT, targetDomainForTracking);
        return targetDomainForTracking;
    }

    public String getJsCode() {
        String result = "";
        try {
            JS_COPY_BUTTON.click();
            executeJavaScript("$('#js-copy').parent()" +
                    ".append($('<input/>')" +
                    ".attr('id', 'copy'))");

            SelenideElement copy = $("#copy");

            copy.sendKeys(Keys.CONTROL, "v");
            result = copy.val();
            executeJavaScript("$('#copy').remove();");
        }
        catch (Exception e){
            log.error("Catch " + e);
        }
        return result;
    }


    public void clickCreateAudience() {
        SAVE_BUTTON.scrollTo().click();
        checkErrors();
    }

    public LinkedHashMap<String, String> setUrlTypeAndValue(LinkedHashMap<String, String> clientUrlData) {
        String type, value;
        int countStepUrl = clientUrlData.size() == 0 ? randomNumberFromRangeInt(1, 3) : clientUrlData.size();
        DELETE_URL_FIELDS_BUTTON.filter(visible).asDynamicIterable().forEach(SelenideElement::click);

        LinkedHashMap<String, String> urlData = new LinkedHashMap<>();
        for (int i = 0; i < countStepUrl - 1; i++) {
            ADD_LINK_BUTTON.click();
        }

        ElementsCollection typeList = $$(URL_TYPE_STRING_SELECT);
        ElementsCollection valList = $$(URL_VALUE_STRING_INPUT);
        for (int i = 0; i < countStepUrl; i++) {

            if (clientUrlData.size() == 0) {
                type = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(typeList.get(i));
                value = setUrlValue(type);
            } else {
                value = (new ArrayList<>(clientUrlData.values())).get(i);
                type = (new ArrayList<>(clientUrlData.keySet())).get(i);
                helpersInit.getBaseHelper().selectCurrentValInSelectListJS(typeList.get(i), type);
            }

            clearAndSetValue(valList.get(i), value);
            urlData.put(value, type);
        }
        return urlData;
    }

    public List<String> setAdTarget(SelenideElement addCampaignButton, String targetCampaignSelect) {
        int amountOfCampaigns = randomNumberFromRangeInt(1, 3);

        List<String> listValues = new ArrayList<>();
        DELETE_URL_FIELDS_BUTTON.filter(Condition.visible).asDynamicIterable().forEach(SelenideElement::click);

        for (int i = 0; i < amountOfCampaigns; i++) {
            if(i == 0) {
                listValues.add(helpersInit.getBaseHelper().selectRandomValSelectListDashJs($$(targetCampaignSelect).get(0)));
            } else {
                addCampaignButton.click();
                listValues.add(helpersInit.getBaseHelper().selectRandomValSelectListDashJs($$(targetCampaignSelect).get(i)));
            }
        }
        return listValues;
    }

    public String setUrlValue(String urlType) {
        String val = switch (urlType) {
            case "starts" -> "https://urlval";
            case "contain" -> "contain";
            case "ends" -> "com.";
            default -> "";
        };
        return val + BaseHelper.getRandomWord(2);
    }

    public List<Map.Entry<String, String>> setTargetsData(List<Map.Entry<String, String>> targetsData) {
        String type, value;
        ArrayList<Integer> ranges = new ArrayList<>();
        DELETE_TARGET_FIELDS_BUTTON.filter(visible).asDynamicIterable().forEach(SelenideElement::click);

        int countTarget = targetsData.size() == 0 ? randomNumberFromRangeInt(1, 3) : targetsData.size();

        //включаем нужное количество целей(countTarget)
        List<Map.Entry<String, String>> clientTargetsData = new ArrayList<>();
        for (int i = 0; i < countTarget - 1; i++) {
            TARGET_ADD_BUTTON.click();
        }

        // после включения нужного количества целей(countTarget) - получаем все селекты type/targetId для дальнейшей работы
        ElementsCollection targetTypeList = $$("[id=container-targets] [class='cusel-scroll-pane']:not([id*='targetId'])");
        ElementsCollection targetValList = $$("[id=container-targets] [class='cusel-scroll-pane']:not([id*='type'])");

        // что бы избежать выбора одинаковых таргетов - выбираем рандомные таргеты
        if (targetsData.size() == 0) {
            int countOptions = targetValList.get(0).$$("span:not([style])").size();
            ranges = helpersInit.getBaseHelper().getRandomCountValue(countOptions, countTarget);
        }

        for (int i = 0; i < countTarget; i++) {

            if (targetsData.size() == 0) {
                type = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(targetTypeList.get(i));
                value = selectValueJsByIndex(targetValList.get(i), ranges.get(i));
            } else {
                value = targetsData.get(i).getValue();
                type = targetsData.get(i).getKey();
                helpersInit.getBaseHelper().selectCurrentValInSelectListJS(targetTypeList.get(i), type);
                helpersInit.getBaseHelper().selectCurrentValInSelectListJS(targetValList.get(i), value);
            }

            clientTargetsData.add(new AbstractMap.SimpleEntry<>(value, type));
        }
        return clientTargetsData;
    }

    public boolean checkTargetsSettings(List<Map.Entry<String, String>> targetsData) {
        ElementsCollection targetTypeList = $$("[id^='cusel-scroll-target'][id*='type']>span.cuselActive");
        ElementsCollection targetValList = $$("[id^='cusel-scroll-targets'][id*='targetId']>span.cuselActive");
        List<String> targetType = new ArrayList<>();
        for(SelenideElement s : targetTypeList){
            targetType.add(s.attr("val"));
        }

        List<String> targetVal = new ArrayList<>();
        for(SelenideElement s : targetValList){
            targetVal.add(s.attr("val"));
        }

        return targetsData.size() == targetTypeList.size()
                & targetsData.size() == targetValList.size()
                & new HashSet<>(targetsData.stream().map(Map.Entry::getValue).collect(Collectors.toList())).containsAll(targetType)
                & new HashSet<>(targetsData.stream().map(Map.Entry::getKey).collect(Collectors.toList())).containsAll(targetVal);
    }

    public boolean switchFulfilmentOfAllConditions() {
        boolean isSwitchOn = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        if ((isSwitchOn && FULFILMENT_OF_ALL_CONDITIONS_TUMBLER.text().equalsIgnoreCase("off")) ||
                (!isSwitchOn && FULFILMENT_OF_ALL_CONDITIONS_TUMBLER.text().equalsIgnoreCase("on"))) {
            clickInvisibleElementJs(FULFILMENT_OF_ALL_CONDITIONS_TUMBLER);
        }
        return isSwitchOn;
    }

    private String selectValueJsByIndex(SelenideElement element, int index) {
        ElementsCollection list = element.$$("span:not([style])");
        executeJavaScript("arguments[0].click()", list.get(index));
        return list.get(index).attr("val");
    }

}
