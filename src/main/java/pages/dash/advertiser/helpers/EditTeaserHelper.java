package pages.dash.advertiser.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.advertiser.locators.EditTeaserLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class EditTeaserHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    private boolean isGenerateNewValues = false;
    private boolean isNeedToEditImage = false;
    private boolean useTitle = false;
    private boolean isNeedToEditCategory = true;
    private boolean useDescription = false;
    private boolean useDiscount = false;

    public EditTeaserHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public EditTeaserHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    public void setIsNeedToEditImage(boolean isNeedToEditImage) {
        this.isNeedToEditImage = isNeedToEditImage;
    }

    public EditTeaserHelper setUseTitle(boolean useTitle) {
        this.useTitle = useTitle;
        return this;
    }

    public void setNeedToEditCategory(boolean needToEditCategory) {
        isNeedToEditCategory = needToEditCategory;
    }

    public EditTeaserHelper setUseDescription(boolean useDescription) {
        this.useDescription = useDescription;
        return this;
    }

    public EditTeaserHelper setUseDiscount(boolean useDiscount) {
        this.useDiscount = useDiscount;
        return this;
    }

    /**
     * Заполнение поля для url
     */
    public String editUrl(String url) {
        String teasersUrl;
        if (isGenerateNewValues) {
            teasersUrl = "https://testUrlSok" + BaseHelper.getRandomWord(3) + "com.test";
            URL_INPUT.scrollIntoView("{display: \"inline-block\"}");
            clearAndSetValue(URL_INPUT, teasersUrl);
            return teasersUrl;
        } else if (url != null) {
            teasersUrl = url;
            URL_INPUT.scrollIntoView("{display: \"inline-block\"}");
            clearAndSetValue(URL_INPUT, url);
        } else {
            return null;
        }
        return teasersUrl;
    }

    /**
     * Заполнение поля для title
     */
    public String editTitle(String title) {
        String teasersTitle = null;
        if (useTitle) {
            teasersTitle = title == null ? "edit title's " + BaseHelper.getRandomWord(2) + "\uD83D\uDE00\u2600" : title;
            helpersInit.getBaseHelper().setAttributeValueJS(TITLE_INPUT.scrollTo().shouldBe(visible), teasersTitle.replace("'", "\\'"));
        }
        log.info("Edit title was filled - " + teasersTitle);
        return teasersTitle;
    }

    /**
     * Заполнение поля для description
     */
    public String editDescription(String description) {
        String teasersDescription = null;
        if (useDescription) {
            teasersDescription = description == null ? "edit description " + BaseHelper.getRandomWord(2) + "\uD83D\uDE00\u2600" : description;
            helpersInit.getBaseHelper().setAttributeValueJS(DESCRIPTION_INPUT, teasersDescription);
        }
        log.info("Edit description was filled - " + teasersDescription);
        return teasersDescription;
    }

    /**
     * Выбор категории для тизера edit interface
     */
    public String editCategory(String categoryId) {
        if (isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
        } else if (categoryId != null) {
            helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
            return categoryId;
        } else {
            return null;
        }
    }

    /**
     * edit image by local
     */
    public String editImageByLocal(String imageName) {
        if (imageName != null && (isGenerateNewValues || isNeedToEditImage)) {
            String image = LINK_TO_RESOURCES_IMAGES + imageName;
            IMAGE_LOCATOR.sendKeys(image);
            waitForAjax();
            return helpersInit.getBaseHelper().getTextAndWriteLog(image);
        }
        return null;
    }

    /**
     * Редактирование валюти в блоке цен тизера
     */
    public String editCurrency(String currency) {
        if (useDiscount) {
            if (isGenerateNewValues) {
                return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(CURRENCY_SELECT);
            } else if (currency != null) {
                helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CURRENCY_SELECT, currency);
                return currency;
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * click checkbox 'use discount'
     */
    public void useDiscount() {
        if (useDiscount) markUnMarkCheckbox(true, SHOW_PRICE_CHECKBOX);
    }

    /**
     * Редактирование цены тизера в блоке цен
     */
    public String editProductPrice(String price) {
        String productPrice = null;
        if (useDiscount) {
            if (isGenerateNewValues) {
                productPrice = randomNumberFromRange(1, 50);
                clearAndSetValue(PRICE_INPUT.shouldBe(visible), productPrice);
            } else if (price != null) {
                productPrice = price;
                clearAndSetValue(PRICE_INPUT.shouldBe(visible), price);
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(productPrice);
    }

    /**
     * Редактирование старой цены тизера в блоке цен,
     */
    public String editOldProductPrice(String price) {
        String oldProductPrice = null;
        if (useDiscount) {
            if (isGenerateNewValues) {
                oldProductPrice = randomNumberFromRange(51, 100);
                clearAndSetValue(PRICE_OLD_INPUT.shouldBe(visible), oldProductPrice);
            } else if (price != null) {
                oldProductPrice = price;
                clearAndSetValue(PRICE_OLD_INPUT.shouldBe(visible), price);
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(oldProductPrice);
    }

    /**
     * Редактирование скидки тизера в блоке цен
     */
    public String editProductDiscount(String discount) {
        if (useDiscount) {
            if (discount != null && !isGenerateNewValues) {
                clearAndSetValue(DISCOUNT_INPUT, discount);
                return helpersInit.getBaseHelper().getTextAndWriteLog(discount);
            }
            return helpersInit.getBaseHelper().getTextAndWriteLog(DISCOUNT_INPUT.val());
        }
        return null;
    }

    public boolean checkProductPrice(String productPrice) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(PRICE_INPUT.val()).equalsIgnoreCase(productPrice);
    }

    public boolean checkOldProductPrice(String oldProductPrice) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(PRICE_OLD_INPUT.val()).equalsIgnoreCase(oldProductPrice);
    }

    public boolean checkProductDiscount(String productDiscount) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(DISCOUNT_INPUT.val()).equalsIgnoreCase(productDiscount);
    }

    public boolean checkProductCurrency(String productCurrency) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(CURRENCY_SELECT, productCurrency));
    }

    public boolean checkTitle(String title) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TITLE_INPUT.shouldBe(visible).val()).equalsIgnoreCase(title);
    }

    public boolean checkDescription(String description) {
        return !useDescription || helpersInit.getBaseHelper().getTextAndWriteLog(DESCRIPTION_INPUT.val()).equalsIgnoreCase(description);
    }

    public boolean checkCategory(String category) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORY_SELECT.$("[data-id='" + category + "']").attr("class").contains("selected-cat"));
    }

    public boolean checkUrl(String url) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(URL_INPUT.val()).equalsIgnoreCase(url);
    }

    /**
     * Сохранение тизера в интерфейсе редактирования
     */
    public void saveSettings() {
        SAVE_SETTINGS_BUTTON.click();
        log.info("saveSettings");
        checkErrors();
        waitForAjax();
    }
}
