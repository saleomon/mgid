package pages.dash.advertiser.helpers;

import com.codeborne.selenide.ElementsCollection;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TeaserAddLocators.MAX_CPC_POPUP_AGREE_PRICE;
import static pages.cab.products.locators.TeaserAddLocators.MAX_PRICE_OF_CLICK_POPUP;
import static pages.dash.advertiser.locators.CreateTeaserLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CreateTeaserHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    private boolean useDescription = false;
    private boolean useDiscount = false;
    private boolean isNeedToEditImage = true;
    private boolean isNeedToEditCategory = false;
    private boolean setIsGenerateAutoTitle = false;


    public CreateTeaserHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void setNeedToEditImage(boolean needToEditImage) {
        isNeedToEditImage = needToEditImage;
    }
    public void setNeedToEditCategory(boolean needToEditCategory) {
        isNeedToEditCategory = needToEditCategory;
    }

    public void setIsGenerateAutoTitle(boolean isGenerateAutoTitle) {
        setIsGenerateAutoTitle = isGenerateAutoTitle;
    }

    public CreateTeaserHelper setUseDescription(boolean useDescription) {
        this.useDescription = useDescription;
        return this;
    }

    public CreateTeaserHelper setUseDiscount(boolean useDiscount) {
        this.useDiscount = useDiscount;
        return this;
    }

    /**
     * Заполнение поля для url
     */
    public String setUrl(String url) {
        String teasersUrl;
        teasersUrl = Objects.requireNonNullElseGet(url, () -> "https://testurlsok" + BaseHelper.getRandomWord(3) + "com.test");
        clearAndSetValue(URL_INPUT, teasersUrl);
        TITLE_INPUT.click();
        log.info("Url was filled - " + teasersUrl);
        return teasersUrl;
    }

    /**
     * Заполнение поля для title
     */
    public String setTitle(String title) {
        if (setIsGenerateAutoTitle) return title;
        String teasersTitle;
        teasersTitle = Objects.requireNonNullElseGet(title, () -> "title's " + randomNumberFromRange(1, 100000) + "\uD83D\uDE00\u2600");
        helpersInit.getBaseHelper().setAttributeValueJS(TITLE_INPUT.shouldBe(visible), teasersTitle.replace("'", "\\'"));
        log.info("Title was filled - " + teasersTitle);
        return teasersTitle;
    }

    /**
     * Заполнение поля для description
     */
    public String setDescription(String description) {
        String teasersDescription = null;
        if (useDescription) {
            teasersDescription = Objects.requireNonNullElseGet(description, () -> "description " + randomNumberFromRange(1, 100000) + "\uD83D\uDE00\u2600");
            helpersInit.getBaseHelper().setAttributeValueJS(DESCRIPTION_INPUT, teasersDescription);
            log.info("Description was filled - " + teasersDescription);
        }
        return teasersDescription;
    }

    /**
     * Выбор категории для тизера - create interface
     */
    public String selectCategoryInCreateInterface(String categoryId) {
        if(!isNeedToEditCategory) return categoryId;
        if (categoryId != null) {
            helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
            return helpersInit.getBaseHelper().getTextAndWriteLog(categoryId);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
    }

    /**
     * load local image
     */
    public String loadImageByLocal(String imageName) {
        String image;
        if (!isNeedToEditImage) return null;
        if (imageName != null && imageName.equals("")) {
            return imageName;
        } else {
            image = imageName != null ? imageName : "Stonehenge.jpg";
            IMAGE_LOCATOR.sendKeys(LINK_TO_RESOURCES_IMAGES + image);
            waitForAjax();
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(image);
    }

    /**
     * get CPC
     */
    public String getCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CPC_LABEL.val());
    }

    /**
     * set price for click (one price - for all regions)
     */
    public String setPriceForClick(String priceForClick, boolean isPriceForAllRegions) {
        CPC_INPUT.scrollTo().click();
        sleep(1000);
        sendKey(CPC_INPUT, priceForClick);
        int time = 0;
        if (isPriceForAllRegions) {
            String price = priceForClick != null ? priceForClick : helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(13.5, 14.7);
            PRICE_FOR_ALL_REGIONS_CHECKBOX.scrollTo();
            if (VERY_LOW_CPC_ICON.exists()) {
                while (!VERY_LOW_AND_REACH_DATA_LABEL.exists() && time < 10) {
                    sleep(500);
                    time++;
                }
            }
            sendKey(CPC_INPUT, price);

            return helpersInit.getBaseHelper().getTextAndWriteLog(price);
        }
        return null;
    }

    /**
     * set prices for click (set different price - for each region)
     */
    public Map<String, String> setPricesForClick(boolean isPriceForAllRegions) {
        Map<String, String> regionPrices = new HashMap<>();
        String randomValue;
        if (!isPriceForAllRegions) {
            if (PRICE_FOR_ALL_REGIONS_CHECKBOX.isSelected()) {
                PRICE_FOR_ALL_REGIONS_CHECKBOX.click();
            }
            while (TREES_BUTTON.isDisplayed()) {
                TREES_BUTTON.click();
            }
            helpersInit.getBaseHelper().waitDisappearingElement(TREES_BUTTON);
            ElementsCollection regionsNames = $$(REGION_NAMES);
            ElementsCollection fieldsOfPrices = $$(FIELDS_OF_PRICES);

            for (int i = 0; i < fieldsOfPrices.size(); i++) {
                randomValue = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(280.2, 400.3);//13.5 -> 14.7
                sendKey(fieldsOfPrices.get(i), randomValue);
                if (!regionsNames.get(i).text().contains("Other")) {
                    regionPrices.put(regionsNames.get(i).text(), randomValue);
                }
            }
        }
        log.info("Prices were set for regions");
        return regionPrices;
    }

    /**
     * Сохранение тизера с последующим переходом в интерфейс списка тизеров
     */
    public void saveButtonAndGoToTeasersList() {
        waitForAjaxLoader();
        GO_TO_LIST_INTERFACE_BUTTON.shouldBe(visible).scrollTo().click();
        waitForAjaxLoader();
        checkErrors();
        checkShowErrorInEditForm();
    }

    /**
     * Закрытие попапа
     */
    public void closePopup() {
        if (MAX_PRICE_OF_CLICK_POPUP.isDisplayed()){
            MAX_CPC_POPUP_AGREE_PRICE.click();
        }
        else if (CLOSE_POPUP_BUTTON.isDisplayed()) {
            log.info(CLOSE_POPUP_BUTTON.parent().text());
            CLOSE_POPUP_BUTTON.click();
            log.info("click popup");
            CLOSE_POPUP_BUTTON.shouldBe(hidden);
        }
    }

    /**
     * Выбор валюти в блоке цен тизера
     */
    public String selectCurrency(String currency) {
        if (useDiscount) {
            if (currency != null) {
                helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CURRENCY_SELECTOR, currency);
                return currency;
            } else {
                return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(CURRENCY_SELECTOR);
            }
        }
        return null;
    }

    /**
     * Заполнение цены тизера в блоке цен
     */
    public String fillProductPrice(String price) {
        if (useDiscount) {
            String productPrice = price != null ? price : randomNumberFromRange(1, 50);
            clearAndSetValue(PRICE_INPUT, productPrice);
            return helpersInit.getBaseHelper().getTextAndWriteLog(productPrice);
        }
        return null;
    }

    /**
     * Заполнение старой цены тизера в блоке цен
     */
    public String fillOldProductPrice(String oldPrice) {
        if (useDiscount) {
            String productOldPrice = oldPrice != null ? oldPrice : randomNumberFromRange(51, 100);
            clearAndSetValue(PRICE_OLD_INPUT, productOldPrice + Keys.ENTER);
            return helpersInit.getBaseHelper().getTextAndWriteLog(productOldPrice);
        }
        return null;
    }

    /**
     * click checkbox 'use discount'
     */
    public void useDiscount() {
        if (useDiscount) markUnMarkCheckbox(true, SHOW_PRICE_CHECKBOX);
    }

    /**
     * Заполнение скидки тизера в блоке цен
     */
    public String setProductDiscount(String discount) {
        if (useDiscount) {
            if (discount != null) {
                clearAndSetValue(DISCOUNT_INPUT, discount);
                return discount;
            }
            return helpersInit.getBaseHelper().getTextAndWriteLog(DISCOUNT_INPUT.val());
        }
        return null;
    }

    /**
     * Проверяем что после редактирования тизера в форме нету ошибок
     */
    public void checkShowErrorInEditForm() {
        if (ERROR_MESSAGE_CONTAINER.size() > 1) {
            if (TEXT_MESSAGE.isDisplayed()) log.error("Error message - " + TEXT_MESSAGE.text());
        } else if (ERROR_MESSAGES.size() > 0) {
            log.error("Error message - " + ERROR_MESSAGES.get(0).text());
        }
    }

    /**
     * check show block 'TEASER REACH'
     */
    public boolean isShowTeaserReachBlock() {
        TITLE_INPUT.shouldBe(visible, ofSeconds(8));
        return helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_REACH_NAMES_LABEL.isDisplayed()) &&
                helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_REACH_METER_LABEL.isDisplayed());
    }

    /**
     * click 'very low' icon
     */
    public void veryLowCpcIconClick() {
        VERY_LOW_CPC_ICON.click();
        checkErrors();
    }

    /**
     * click 'low' icon
     */
    public void lowCpcIconClick() {
        LOW_CPC_ICON.click();
    }

    /**
     * click 'high' icon
     */
    public void highCpcIconClick() {
        HIGH_CPC_ICON.scrollTo().click();
        checkErrors();
    }

    /**
     * click 'very high' icon
     */
    public void veryHighCpcIconClick() {
        VERY_HIGH_CPC_ICON.click();
    }

    /**
     * get cpc 'TEASER REACH' block
     */
    public Double getPriceForTeaserReachBlock(String imageName) {
        loadImageByLocal(imageName);
        VERY_LOW_AND_REACH_DATA_LABEL.shouldBe(exist, ofSeconds(12));
        return helpersInit.getBaseHelper().parseDouble(getCpc());
    }
}
