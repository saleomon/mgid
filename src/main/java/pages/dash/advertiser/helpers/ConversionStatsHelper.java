package pages.dash.advertiser.helpers;

import com.codeborne.selenide.CollectionCondition;
import core.base.HelpersInit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static java.lang.Integer.parseInt;
import static pages.dash.advertiser.locators.ConversionStatsLocators.*;
import static core.helpers.BaseHelper.waitForAjax;

public class ConversionStatsHelper {
    private final HelpersInit helpersInit;

    public ConversionStatsHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * метод выбирает тип конверсии отображаемой статистики из радио листа
     */
    public void chooseInterestStageRadio(String stage) {
        STAGES_RADIO.selectRadio(stage);
        waitForAjax();
    }

    /**
     * метод вибирает параметр первого фильтра
     */
    public void selectFirstFilterBy(String stage) {
        FIRST_TABLE_FILTER_LOCATOR.click();
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(FIRST_TABLE_FILTER_SELECT, stage);
        waitForAjax();
    }

    /**
     * метод вибирает параметр второго фильтра
     */
    public void selectSecondFilterBy(String stage) {
        SECOND_TABLE_FILTER_LOCATOR.click();
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(SECOND_TABLE_FILTER_SELECT, stage);
        waitForAjax();
    }

    /**
     * метод вибирает опцию отображения статистики по состоянию конверсии
     */
    public void chooseTypeOfCondition(String stage) {
        if (TYPE_OF_WIDGET_CONVERSIONS_RADIO.isDisplayed()) {
            if (!$("[name='service_gblocks_filter_radio'][value='" + stage + "']").has(attribute("checked"))) {
                TYPE_OF_WIDGET_CONVERSIONS_RADIO.selectRadio(stage);
                waitForAjax();
            }
        } else {
            if (!$("[name='service_teaser_filter_radio'][value='" + stage + "']").has(attribute("checked"))) {
                TYPE_OF_TEASER_CONVERSIONS_RADIO.selectRadio(stage);
                waitForAjax();
            }
        }
    }

    /**
     * метод проверяет статистику которая передается в график интерфейса
     */
    public boolean checkChartData(List<String> data, String conversion, String conversionPrice) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(data.contains(conversion)) &&
                helpersInit.getBaseHelper().getTextAndWriteLog(data.contains(conversionPrice));
    }

    /**
     * метод делает выборку даних графика по заданому значению
     */
    public String getValueFromChart(String value) {
        return getSumString(getJsQuery(value));
    }

    /**
     * получаем js запрос для получения из графика данных по переданной метрике
     */
    public ArrayList<Object> getJsQuery(String param){
        return executeJavaScript("var arrayMass2 = []; for(var i = 0; i < Mgid.view.data['sensors-stat']['stat'].length; i++) { arrayMass2.push(Mgid.view.data['sensors-stat']['stat'][i]['"+ param + "']);} return arrayMass2");
    }

    /**
     * метод проверяет статистику общего блока интерфейса статистики
     */
    public boolean checkSummaryData(List<String> data) {
        return SUMMARY_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0)).texts().stream()
                .allMatch(el -> data.contains(el.replace(" €", "").replace(" ", "").trim()));
    }

    /**
     * метод подсчитывает клики в отображаемой таблице
     */
    public Integer getSumClicksFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumInteger(CLICK_DATAS);
    }

    /**
     * метод подсчитывает траты в отображаемой таблице
     */
    public Double getSumSpentFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(SPENT_DATAS);
    }

    /**
     * метод подсчитывает процент количества конверсий в отображаемой таблице
     */
    public Integer getSumConversionCountFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumInteger(CONVERSION_COUNT_DATAS);
    }

    /**
     * метод подсчитывает стоимость конверсий в отображаемой таблице
     */
    public Double getSumConversionCostFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(CONVERSION_COST_DATAS);
    }

    /**
     * метод подсчитывает количество конверсий в отображаемой таблице
     */
    public Double getSumConversionsFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(CONVERSION_DATAS);
    }

    /**
     * метод подсчитывает зароботок в отображаемой таблице
     */
    public Double getSumRevenueFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(REVENUE_DATAS);
    }

    /**
     * метод подсчитывает ROI в отображаемой таблице
     */
    public Double getSumRoiFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(ROI_DATAS);
    }

    /**
     * метод подсчитывает EPC в отображаемой таблице
     */
    public Double getSumEpcFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(EPC_DATAS);
    }

    /**
     * метод подсчитывает EPC в отображаемой таблице
     */
    public Double getSumProfitFromTableColumn() {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().getSumDouble(PROFIT_DATAS);
    }

    /**
     * метод проверяет количество кликов  в отображаемой таблице
     */
    public boolean checkClicksTable(Object clicks, Integer clickstable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(clicks.toString(), clickstable.toString());
    }

    /**
     * метод проверяет количество конверсий  в отображаемой таблице
     */
    public boolean checkConversionsTable(Object conversion, Double conversionTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(conversion.toString(), conversionTable.toString());
    }

    /**
     * метод проверяет количество трат  в отображаемой таблице
     */
    public boolean checkSpentTable(Object spent, Double spentTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(spent.toString(), String.format("%.2f", spentTable).replace(",", "."));
    }

    /**
     * метод проверяет процент количества конверсий  в отображаемой таблице
     */
    public boolean checkConversionCountTable(Object conversionCount, Integer conversionCountTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(conversionCount.toString(), conversionCountTable.toString());
    }

    /**
     * метод проверяет количество стоимости конверсий  в отображаемой таблице
     */
    public boolean checkConversionCostTable(Object conversionCost, Double conversionCostTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(conversionCost.toString(), conversionCostTable.toString());
    }

    /**
     * метод проверяет количество зароботка  в отображаемой таблице
     */
    public boolean checkRevenueTable(Object revenue, Double revenueTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(revenue.toString(), revenueTable.toString());
    }

    /**
     * метод проверяет количество EPC  в отображаемой таблице
     */
    public boolean checkEpcTable(Object epc, Double epcTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(epc.toString(), String.format("%.2f", epcTable).replace(",", "."));
    }

    /**
     * метод проверяет количество ROI  в отображаемой таблице
     */
    public boolean checkRoiTable(Object roi, Double roiTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(roi.toString(), String.format("%.2f", roiTable).replace(",", "."));
    }

    /**
     * метод проверяет количество ROI  в отображаемой таблице
     */
    public boolean checkProfitTable(Object profit, Double profitTable) {
        return helpersInit.getBaseHelper().checkDatasetEquals(profit.toString(), String.format("%.2f", profitTable).replace(",", "."));
    }

    /**
     * метод проверяет значения фильтров 1 в отображаемой таблице
     */
    public boolean checkFilter1(Object filter1) {
        return new HashSet<>(DATAS_FILTER_1.texts()).containsAll((Collection<?>) filter1);
    }

    /**
     * метод проверяет значения фильтров 2 в отображаемой таблице
     */
    public boolean checkFilter2(Object filter2) {
        return new HashSet<>(DATAS_FILTER_2.texts()).containsAll((Collection<?>) filter2);
    }

    /**
     * метод проверяет статистику по конверсиям в таблице
     */
    public boolean checkTableData(List<List<String>>  data) {
        TABLE_DATA_PRESENCE.shouldHave(CollectionCondition.sizeGreaterThan(0));
        //get data from table
        List<String> firstColumn = DATAS_FILTER_1.texts();
        double spent = helpersInit.getBaseHelper().getSumDouble(SPENT_DATAS);
        int clicks = CLICK_DATAS.texts().stream().mapToInt(el -> parseInt(el.trim())).sum();
        Double revenue = helpersInit.getBaseHelper().getSumDouble(REVENUE_DATAS);

        //get data from parameter
        Integer clicksSelect = data.stream().mapToInt(el -> Integer.parseInt(el.get(1))).sum();
        String revenueSelect = String.format("%.2f", data.stream().mapToDouble(el -> Double.parseDouble(el.get(6))).sum()).replace(",", ".");
        List<String> filterFirst = new ArrayList<>();
        for (List<String> dataList : data) {
            filterFirst.add(dataList.get(0).trim());
        }

        return helpersInit.getBaseHelper().checkDataset(clicksSelect, clicks) &&
                helpersInit.getBaseHelper().comparisonData(data.stream().mapToDouble(el -> Double.parseDouble(el.get(2))).sum(), spent, 0.2) &&
                helpersInit.getBaseHelper().checkDatasetContains(revenueSelect, String.valueOf(revenue)) &&
                new HashSet<>(firstColumn).containsAll(filterFirst);
    }

    public boolean checkSecondColumnOfConversionStatsTable(List<List<String>> dataFromDB, List<String> dataText) {
        List<String> secondColumn = DATAS_FILTER_2.texts();
        List<String> filterSecond = new ArrayList<>();
        if (dataFromDB != null) {
            for (List<String> dataList : dataFromDB) {
                filterSecond.add(dataList.get(7).trim());
            }
        } else {
            filterSecond.addAll(dataText);
        }
        return new HashSet<>(secondColumn).containsAll(filterSecond);
    }

    /**
     * Получаем сумму массива елементов (Object)
     */
    public String getSumString(ArrayList<Object> mass) {
        int temp = 0;
        for (Object s : mass) {
            if (s instanceof String) {
                temp += parseInt(s.toString());
            }
        }
        return String.valueOf(temp);
    }
}
