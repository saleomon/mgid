package pages.dash.advertiser.helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.BaseHelper;
import testData.project.Subnets;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.*;
import static pages.dash.advertiser.variables.AddEditCampaignVariables.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.TargetingType.Targeting.*;


public class AddEditCampaignHelper {
    private final HelpersInit helpersInit;
    private final Logger log;
    private boolean isNeedToEdit = true;
    int targetCounter;
    String[] typeOfTargetting = {"include", "exclude"};


    public AddEditCampaignHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void setIsNeedToEdit(boolean state) {
        isNeedToEdit = state;
    }

    /**
     * вытаскивает ай ди по имени кампании
     */
    public String getCampaignIdFromList(String companyName) {
        helpersInit.getBaseHelper().waitDisplayedElement($x(".//span[contains(text(), '" + companyName + "')]/ancestor::tr[@data-id]"));
        return $x(".//span[contains(text(), '" + companyName + "')]/ancestor::tr[@data-id]").attr("data-id");
    }

    /**
     * заполнение имени кампании
     */
    public String fillCampaignName(String name) {
        if (name == null && !isNeedToEdit) return null;
        name = name == null ? "CampaignTest " + BaseHelper.getRandomWord(4) : name;
        clearAndSetValue(CAMPAIGN_NAME, name);
        return helpersInit.getBaseHelper().getTextAndWriteLog(name);
    }

    public String fillCampaignKeyword(String keyword) {
        if (keyword == null) return null;
        clearAndSetValue(CAMPAIGN_KEYWORD, keyword);
        return helpersInit.getBaseHelper().getTextAndWriteLog(keyword);
    }

    /**
     * выбор типа кампании
     */
    public String selectCampaignType(String type) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(type.equals("") ? helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CAMPAIGN_TYPE, "product") : helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CAMPAIGN_TYPE, type));
    }

    /**
     * выбор категории кампании
     */
    public String selectCampaignCategory(String type) {
        if (type == null && !isNeedToEdit) return null;
        List<String> forbiddenCategory = Arrays.asList("254", "149", "148", "105");
        String category;
        if (type == null) {
            for (int i = 0; i < 10; i++) {
                category = helpersInit.getBaseHelper().selectValueFromParagraphListJs(CAMPAIGN_CATEGORY);
                if (!forbiddenCategory.contains(category)) {
                    return helpersInit.getBaseHelper().getTextAndWriteLog(category);
                }
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CAMPAIGN_CATEGORY, type));
    }

    /**
     * выбор языка кампании
     */
    public String selectCampaignLanguage(String language) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(language == null ? helpersInit.getBaseHelper().selectRandomValSelectListDashJs(CAMPAIGN_LANGUAGE) : helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CAMPAIGN_LANGUAGE, language));
    }

    /**
     * изменение состояния чекбокса по блокированию тизеров после создания кампании
     */
    public void changeStateBlockBeforeShow(boolean state) {
        if (isNeedToEdit) {
            String val = Objects.requireNonNull(BLOCK_AFTER_CREATE_SWITCH.getAttribute("class"));
            if ((state && val.equalsIgnoreCase("off")) || (!state && val.equalsIgnoreCase("on")))
                clickInvisibleElementJs(BLOCK_AFTER_CREATE_SWITCH);
        }
    }

    /**
     * method choose targets for all section or can choose target for one section
     */
    public JsonObject selectTargeting(boolean isEnableTargeting, String campaignType, Subnets.SubnetType...subnets) {
        if (!isEnableTargeting) return null;
        if (!isNeedToEdit) return null;
        JsonObject jsonTargetingElements = new JsonObject();
        targetCounter = 0;

        jsonTargetingElements.add(GEO.getTypeValue(), locationTarget());
        targetCounter++;
        jsonTargetingElements.add(DEVICES_OS.getTypeValue(), osTarget());
        targetCounter++;
        jsonTargetingElements.add(BROWSER.getTypeValue(), browserTarget());
        targetCounter++;
        jsonTargetingElements.add(BROWSER_LANGUAGE.getTypeValue(), browserLanguageTarget());
        targetCounter++;
        jsonTargetingElements.add(TRAFFIC_TYPE.getTypeValue(), trafficTarget());
        targetCounter++;
        jsonTargetingElements.add(MOBILE_CONNECTION.getTypeValue(), connectionTypeTarget());
        targetCounter++;
        switch (campaignType) {
            case "product", "content", "search_feed" -> {
                jsonTargetingElements.add(DEMOGRAPHICS.getTypeValue(), demographicsTarget());
                targetCounter++;
                 jsonTargetingElements.add(INTERESTS.getTypeValue(), interestsTarget());
                 targetCounter++;
                jsonTargetingElements.add(PHONE_PRICE_RANGES.getTypeValue(), phonePriceRangeTarget());
                targetCounter++;
            }
            default -> log.info("There are no matched campaign types");
        }

        for(Subnets.SubnetType subnet:subnets) {
            switch (subnet) {
                case SCENARIO_ADGAGE -> { }
                case SCENARIO_ADSKEEPER, SCENARIO_MGID -> {
                    jsonTargetingElements.add(AUDIENCES.getTypeValue(), audienceTarget());
                    targetCounter++;
                }
                default -> log.info("There are no matched targets");
            }
        }
        return jsonTargetingElements;
    }

    public JsonObject selectTargeting(boolean useTargeting, String...neededTargetting) {
        if (!useTargeting) return null;
        if (!isNeedToEdit) return null;
        JsonObject jsonTargetingElements = new JsonObject();
        targetCounter = 0;

        for (String parameter : neededTargetting) {
            switch (parameter) {
                case "location" -> {
                    jsonTargetingElements.add(GEO.getTypeValue(), locationTarget());
                    targetCounter++;
                }
                case "os" -> {
                    jsonTargetingElements.add(DEVICES_OS.getTypeValue(), osTarget());
                    targetCounter++;
                }
                case "connection" -> {
                    jsonTargetingElements.add(MOBILE_CONNECTION.getTypeValue(), connectionTypeTarget());
                    targetCounter++;
                }
                case "browser" -> {
                    jsonTargetingElements.add(BROWSER.getTypeValue(), browserTarget());
                    targetCounter++;
                }
                case "browserLanguage" -> {
                    jsonTargetingElements.add(BROWSER_LANGUAGE.getTypeValue(), browserLanguageTarget());
                    targetCounter++;
                }
                case "audience" -> {
                    jsonTargetingElements.add(AUDIENCES.getTypeValue(), audienceTarget());
                    targetCounter++;
                }
                case "trafficType" -> {
                    jsonTargetingElements.add(TRAFFIC_TYPE.getTypeValue(), trafficTarget());
                    targetCounter++;
                }
                case "context" -> {
                    jsonTargetingElements.add(CONTEXT.getTypeValue(), contextTarget());
                    targetCounter++;
                }
                case "sentiments" -> {
                    jsonTargetingElements.add(SENTIMENTS.getTypeValue(), sentimentsTarget());
                    targetCounter++;
                }
                case "phonePriceRange" -> {
                    jsonTargetingElements.add(PHONE_PRICE_RANGES.getTypeValue(), phonePriceRangeTarget());
                    targetCounter++;
                }
                default -> log.info("There are no matched targets");
            }
        }
        return jsonTargetingElements;
    }

    /**
     * method choose location target
     */
    public JsonObject locationTarget() {
        //location
        JsonObject dataLocation;
        String chosenType = getTypeTargeting();

        LOCATION_TARGET.click();
        dataLocation = chooseRandomTarget(chosenType, GEO.getTypeValue());

        LOCATION_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Locaton selected - " + dataLocation.get("data-id").toString());

        return dataLocation;
    }

    /**
     * method choose os target
     */
    public JsonObject osTarget() {
        //os
        JsonObject dataOs;
        String chosenType = getTypeTargeting();

        OS_TARGET.click();
        dataOs = chooseRandomTarget(chosenType, DEVICES_OS.getTypeValue());

        OS_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected OS - " + dataOs.get("data-id").toString());

        return dataOs;
    }

    /**
     * method choose random target elements
     */
    public JsonObject chooseRandomTarget(String typeOfTarget, String kindOfTarget) {
        JsonObject data = new JsonObject();
        int indexOfElement;
        ElementsCollection listOptions;
        String data_id;
        String element = String.format(TARGET_ELEMENTS, kindOfTarget);
        int counter = 0;
        //this block evaluated if target has expanded elements and open its
        while ($$(NOT_EXPANDED_ELEMENTS).size() > 0 && counter < 3) {
            counter++;
            expandAllTargetElements();
        }

        listOptions = $$(String.format(element, kindOfTarget));
        data_id = listOptions.get(indexOfElement = randomNumbersInt(listOptions.size())).attr("data-id");
        //choose random target element

        SelenideElement targetingItem = $$(String.format(element, kindOfTarget)).get(indexOfElement);
        targetingItem.find("div").hover().find(String.format(TARGET_ELEMENTS_TYPE_TARGETING_BUTTON, typeOfTarget)).shouldBe(visible).click();
        data.addProperty("data-id", data_id);
        data.addProperty("typeTarget", typeOfTarget);
        checkErrors();
        return data;
    }

    private void expandAllTargetElements() {
        executeJavaScript("var v = $('.targeting-expand');" + "for(var i = 0; i < v.length; i++){ v[i].click(); }");
    }

    /**
     * method check selected section of targets for all section
     */
    public boolean checkSelectedTargets(boolean useTargeting, JsonObject targetingElements) {
        if(!useTargeting) return true;
        Iterator<String> keys = targetingElements.keySet().iterator();
        String currentKey;
        String dataId, typeTargeting;
        int counter = 0;
        while (keys.hasNext()) {
            currentKey = keys.next().replaceAll("\"", "");
            switch (currentKey)  {
                case "location", "browser", "os", "language", "connectionType", "trafficType", "context", "sentiments",
                        "phonePriceRange", "socdem", "interest" -> {
                    typeTargeting = targetingElements.get(currentKey).getAsJsonObject().get("typeTarget").getAsString();
                    dataId = targetingElements.get(currentKey).getAsJsonObject().get("data-id").getAsString();
                }
                case "audience" -> {
                    typeTargeting = targetingElements.get(currentKey).getAsJsonObject().get("include").getAsJsonObject().get("typeTarget").getAsString();
                    dataId = targetingElements.get(currentKey).getAsJsonObject().get("exclude").getAsJsonObject().get("data-id").getAsString();

                }
                default -> throw new IllegalStateException("Unexpected value: " + currentKey);
            }
            if (helpersInit.getBaseHelper().getTextAndWriteLog(
                    $(String.format(CHOSEN_TARGET_ELEMENT, currentKey, typeTargeting, dataId)).isDisplayed() ||
                    $(String.format(CHOSEN_TARGET_ELEMENT_WITHOUT_TYPE, currentKey, dataId)).exists()) ||
                    $(String.format(CHOSEN_TARGET_ELEMENT_MULTIPLE, currentKey, dataId)).exists()) {
                counter++;
            }
        }
        log.info(counter + " == " + targetCounter);
        return helpersInit.getBaseHelper().getTextAndWriteLog(counter == targetCounter);
    }

    public String getTypeTargeting() {
        return typeOfTargetting[randomNumbersInt(typeOfTargetting.length)];
    }

    /**
     * method choose browser target
     */
    public JsonObject browserTarget() {
        //browser
        JsonObject dataBrowser;
        String chosenType = getTypeTargeting();

        BROWSER_TARGET.click();
        dataBrowser = chooseRandomTarget(chosenType, BROWSER.getTypeValue());

        BROWSER_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected browser - " + dataBrowser.get("data-id").toString());

        return dataBrowser;
    }

    /**
     * method choose browser language target
     */
    public JsonObject browserLanguageTarget() {
        ////browser language
        JsonObject dataBrowserLanguage;
        String chosenType = getTypeTargeting();

        BROWSER_LANGUAGE_TARGET.click();
        dataBrowserLanguage = chooseRandomTarget(chosenType, BROWSER_LANGUAGE.getTypeValue());

        LANGUAGE_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected browser language - " + dataBrowserLanguage.get("data-id").toString());

        return dataBrowserLanguage;
    }

    public JsonObject trafficTarget(String... useTargetingValue) {
        //traffic
        JsonObject dataProvider;
        String chosenType;
        chosenType = useTargetingValue.length > 0 ? useTargetingValue[0] : getTypeTargeting();

        TRAFFIC_TYPE_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, TRAFFIC_TYPE.getTypeValue());

        TRAFFIC_TYPE_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected traffic type - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject contextTarget(String... useTargetingValue) {
        //context
        JsonObject dataProvider;
        String chosenType;
        chosenType = useTargetingValue.length > 0 ? useTargetingValue[0] : getTypeTargeting();

        CONTEXT_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, CONTEXT.getTypeValue());

        CONTEXT_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected context - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject sentimentsTarget() {
        //sentiments
        JsonObject dataProvider;
        String chosenType = "include";

        SENTIMENTS_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, SENTIMENTS.getTypeValue());

        SENTIMENTS_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected sentiments - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject phonePriceRangeTarget(String... useTargetingValue) {
        //phonePriceRange
        JsonObject dataProvider;
        String chosenType = useTargetingValue.length > 0 ? useTargetingValue[0] : getTypeTargeting();

        PHONE_PRICE_RANGES_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, PHONE_PRICE_RANGES.getTypeValue());

        PHONE_PRICE_RANGES_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected phone price range - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose audiences
     */
    public JsonObject audienceTarget() {
        //provider
        JsonObject dataProvider = new JsonObject();

        AUDIENCE_TAB.click();
        dataProvider.add("exclude", chooseRandomTarget("exclude", AUDIENCES.getTypeValue()));
        dataProvider.add("include", chooseRandomTarget("include", AUDIENCES.getTypeValue()));
        AUDIENCE_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected audience exclude - " + dataProvider.get("exclude").getAsJsonObject().get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("exclude").getAsJsonObject().get("typeTarget").toString());
        log.info("Selected audience include - " + dataProvider.get("include").getAsJsonObject().get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("include").getAsJsonObject().get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose provider target
     */
    public JsonObject connectionTypeTarget() {
        //provider
        JsonObject dataProvider;
        String chosenType = "include";

        CONNECTION_TARGET.click();
        dataProvider = chooseRandomTarget(chosenType, MOBILE_CONNECTION.getTypeValue());

        CONNECTION_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected Connection type target - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose Demographics target
     */
    public JsonObject demographicsTarget() {
        //provider
        JsonObject dataProvider;
        String chosenType = "include";

        DEMOGRAPHICS_TARGET.click();
        dataProvider = chooseRandomTarget(chosenType, DEMOGRAPHICS.getTypeValue());

        DEMOGRAPHICS_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected Demographics target - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose Interests target
     */
    public JsonObject interestsTarget() {
        //provider
        JsonObject dataProvider;
        String chosenType = getTypeTargeting();

        INTERESTS_TARGET.click();
        dataProvider = chooseRandomTarget(chosenType, INTERESTS.getTypeValue());

        INTERESTS_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected Interests target - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public void chooseRandomInterestTarget(String targetingType) {
        SelenideElement selectedTargetItem = INTERESTS_TARGET_LEVEL_0_ITEM.get(randomNumbersInt(INTERESTS_TARGET_LEVEL_0_ITEM.size()));
        log.info("Selected target item is - " + selectedTargetItem.text());
        selectedTargetItem.hover().find(String.format(TARGET_ELEMENTS_TYPE_TARGETING_BUTTON, targetingType)).shouldBe(visible).click();
        checkErrors();
    }

    /**
     * сохранение блока основных настроек кампании
     */
    public void saveGeneralSettings() {
        SAVE_GENERAL_SETTINGS.click();
        checkErrors();
        if (CONFIRM_BUTTON.isDisplayed()) {
            CONFIRM_BUTTON.click();
        }
        waitForAjax();
    }

    /**
     * сохранение всех настроек кампании
     */
    public void saveCampaign() {
        SAVE_CAMPAIGN_BUTTON.scrollTo().click();
        checkErrors();
        waitForAjaxLoader();
        sleep(1000);
        if (CONFIRM_POPUP_BUTTON.isDisplayed()) {
            CONFIRM_POPUP_BUTTON.click();
            waitForAjaxLoader();
        }
        sleep(1000);
        CONFIRM_POPUP_BUTTON.shouldBe(hidden);
        if (CONFIRM_BUTTON.isDisplayed()) {
            CONFIRM_BUTTON.click();
        }
        CONFIRM_BUTTON.shouldBe(hidden);
    }

    /**
     * заполнение поля лимитов
     */
    public String fillLimitField(SelenideElement element, String value, int start, int end) {
        if (value == null && !isNeedToEdit) return null;
        String val = value == null ? randomNumberFromRange(start, end) : value;
        clearAndSetValue(element, helpersInit.getBaseHelper().getTextAndWriteLog(val));
        return val;
    }

    /**
     * изменения состояния чекбокса по показу всех тизеров после конверсии и
     * заполнение поля для периода блокировки тизов
     */
    public String isShowAllTeaserAfterConversion(boolean state, String val) {
        if (state) {
            markUnMarkCheckbox(state, IS_SHOW_ALL_TEASER_AFTER_CONVERSION_CHECKBOX);
            val = val == null ? randomNumberFromRange(1, 20) : val;
            clearAndSetValue(PERIOD_OF_TIME_NOT_SHOW_TEASERS, val);
            return val;
        }
        return null;
    }

    /**
     * изменения состояния чекбокса по показу всех тизеров после конверсии и
     * заполнение поля для периода блокировки тизов
     */
    public boolean checkLimitSubOption(boolean state, String val) {
        return !state || helpersInit.getBaseHelper().checkDatasetContains(PERIOD_OF_TIME_NOT_SHOW_TEASERS.val(), val);
    }

    /**
     * изменение состояния свича UTM  тегов
     */
    public void changeStateTrackingsTag(boolean isEnableUtms) {
        if (isEnableUtms) {
            if (UTM_SWITCHER_STATE_OFF.isDisplayed()) UTM_SWITCHER_STATE_OFF.click();
        } else {
            if (UTM_SWITCHER_STATE_ON.isDisplayed()) UTM_SWITCHER_STATE_ON.click();
        }
    }

    /**
     * заполнение тега по ресурсу
     */
    public void chooseSourceTag(String tag) {
        if (tag == null && !isNeedToEdit) return;
        if (tag != null) {
            clearAndSetValue(UTM_SOURCE, tag);
        }
    }

    /**
     * заполнение поля по какому то медиум тегу
     */
    public void chooseMediumTag(String tag) {
        if (tag == null && !isNeedToEdit) return;
        if (tag != null) {
            clearAndSetValue(UTM_MEDIUM, tag);
        }
    }

    /**
     * опять же заполнение поля тега по кампании
     */
    public void chooseCampaignTag(String tag) {
        if (tag == null && !isNeedToEdit) return;
        if (tag != null) {
            clearAndSetValue(UTM_CAMPAIGN, tag);
        }
    }

    /**
     * просто заполнение поля кастомного тега
     */
    public void chooseCustomTag(String tag) {
        if (tag == null && !isNeedToEdit) return;
        if (tag != null) {
            markUnMarkCheckbox(true, UTM_CUSTOM);
            clearAndSetValue(UTM_CUSTOM_FIELD, tag);
        }
    }

    /**
     * заполнение всех конверсий
     */
    public Map<String, String> addAllRandomStages(boolean isEnabledSensors) {
        Map<String, String> map = new HashMap<>();
        String cpa;
        if (isEnabledSensors) {
            if (CONVERSION_SWITCHER_STATE_OFF.isDisplayed()) CONVERSION_SWITCHER_STATE_OFF.click();
            for (int i = 0; i < 3 && ADD_CONVERSION_BUTTON.isDisplayed(); i++) ADD_CONVERSION_BUTTON.click();

            helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(GOAL_SELECT_BUY, "1");
            map.put("buyTarget", helpersInit.getBaseHelper().selectRandomValSelectListDashJs(TARGET_SELECT_BUY));
            helpersInit.getBaseHelper().selectRandomValSelectListDashJs(BUY_CONVERSION_COUNTING);
            map.put("buyCount", BUY_CONVERSION_COUNTING_ELEMENT.text());
            map.put("buyCpa", cpa = helpersInit.getBaseHelper().randomNumbersString(2));
            clearAndSetValue(CPA_ACTION, cpa);

            helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(GOAL_SELECT_DECISION, "1");
            map.put("desireTarget", helpersInit.getBaseHelper().selectRandomValSelectListDashJs(TARGET_SELECT_DECISION));
            helpersInit.getBaseHelper().selectRandomValSelectListDashJs(DESIRE_CONVERSION_COUNTING);
            map.put("desireCount", DECISION_CONVERSION_COUNTING_ELEMENT.text());
            map.put("desireCpa", cpa = helpersInit.getBaseHelper().randomNumbersString(2));
            clearAndSetValue(CPA_DECISION, cpa);

            helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(GOAL_SELECT_INTEREST, "1");
            map.put("interestTarget", helpersInit.getBaseHelper().selectRandomValSelectListDashJs(TARGET_SELECT_INTEREST));
            helpersInit.getBaseHelper().selectRandomValSelectListDashJs(INTEREST_CONVERSION_COUNTING);
            map.put("interestCount", INTEREST_CONVERSION_COUNTING_ELEMENT.text());
            map.put("interestCpa", cpa = helpersInit.getBaseHelper().randomNumbersString(2));
            clearAndSetValue(CPA_INTEREST, cpa);
            return map;
        } else if(CONVERSION_SWITCHER_STATE_ON.isDisplayed()) CONVERSION_SWITCHER_STATE_ON.click();
        return null;
    }

    /**
     * выбор расписания показа тизера
     * - выбор блокирования по дню недели
     * - выбор блокирования по времени в сутках
     * - выбор блокирования одной рандомной ячейки
     */
    public Map<String, String> setScheduleRandom(boolean isEnabledSchedule) {
        if (isEnabledSchedule) {
            if (SCHEDULE_SWITCHER_STATE_OFF.isDisplayed()) SCHEDULE_SWITCHER_STATE_OFF.click();
            RESET_SCHEDULE_SETTINGS_BUTTON.click();
            int chosenDay = randomNumbersInt(WEEK_DAYS.size());
            Map<String, String> elements = new HashMap<>();

            START_DATE_SELECT.click();
            NEXT_START_MONTH_BUTTON.click();
            clickInvisibleElementJs(AMOUNT_OF_DAY_FIRST.get(randomNumbersInt(AMOUNT_OF_DAY_FIRST.size())));
            END_DATE_SELECT.click();
            NEXT_END_MONTH_BUTTON.click();
            clickInvisibleElementJs(AMOUNT_OF_DAY_SECOND.get(randomNumbersInt(AMOUNT_OF_DAY_SECOND.size())));
            CALENDAR_LABEL.click();
            //choose blocked by hour - hourItem
            clickInvisibleElementJs(HOUR_ITEMS.get(randomNumbersInt(HOUR_ITEMS.size())));
            //choose blocked by weer day - daysItem
            clickInvisibleElementJs(WEEK_ITEMS.get(chosenDay));
            clickInvisibleElementJs(WEEK_ITEMS.get(chosenDay));
            //choose blocked by independent item - 1
            clickInvisibleElementJs(ACTIVE_ITEMS.get(randomNumbersInt(ACTIVE_ITEMS.size())));
            elements.put("start", START_DATE_SELECT.text());
            elements.put("end", END_DATE_SELECT.text());
            elements.put("items", Integer.valueOf(generalItem - daysItem - hourItem - 1).toString());
            return elements;
        } else {
            if (SCHEDULE_SWITCHER_STATE_ON.isDisplayed()) SCHEDULE_SWITCHER_STATE_ON.click();
        }
        return null;
    }

    /**
     * заполнение расписания блокирования с помощью управляющих временем чекбоксов
     */
    public String setScheduleByCheckboxes(boolean isEnabledSchedule) {
        if (isEnabledSchedule) {
            if (SCHEDULE_SWITCHER_STATE_OFF.isDisplayed()) SCHEDULE_SWITCHER_STATE_OFF.click();
            int choosenCheckbox;
            RESET_SCHEDULE_SETTINGS_BUTTON.click();
            clickInvisibleElementJs(DRIVE_SCHEDULE_CHECKBOXES.get(choosenCheckbox = randomNumbersInt(DRIVE_SCHEDULE_CHECKBOXES.size())));
            return DRIVE_SCHEDULE_CHECKBOXES.get(choosenCheckbox).getAttribute("class");

        } else {
            if (SCHEDULE_SWITCHER_STATE_ON.isDisplayed()) SCHEDULE_SWITCHER_STATE_ON.click();
        }
        return null;
    }

    /**
     * проверка имени кампании
     */
    public boolean checkCampaignName(String name) {
        return helpersInit.getBaseHelper().checkDatasetContains(CAMPAIGN_NAME.val(), name);
    }

    public boolean checkCampaignKeyword(String keyword) {
        return keyword == null || helpersInit.getBaseHelper().checkDatasetContains(CAMPAIGN_KEYWORD.val(), keyword);
    }

    /**
     * проверка категории кампании
     */
    public boolean checkCampaignCategory(String type) {
        return helpersInit.getBaseHelper().checkDatasetContains(CAMPAIGN_CATEGORY.text(), type);
    }

    /**
     * проверка языка кампании
     */
    public boolean checkCampaignLanguage(String language) {
        return helpersInit.getBaseHelper().checkDatasetContains(CAMPAIGN_LANGUAGE.findElement(By.cssSelector("[selected]")).getText(), language);
    }

    /**
     * проверка расписания кампании
     */
    public boolean checkScheduleOption(Map<String, String> schedule) {
        if (schedule.isEmpty()) return true;
        return helpersInit.getBaseHelper().checkDatasetEquals(schedule.get("start"), START_DATE_SELECT.text()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(schedule.get("end"), END_DATE_SELECT.text()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(schedule.get("items"), String.valueOf(ACTIVE_ITEMS.size()));
    }

    /**
     * проверка расписания кампании чекбоксы
     */
    public boolean checkScheduleByCheckboxes(String value) {
        if (value == null) return true;
        return switch (value) {
            case "only-business-time-checkbox" -> helpersInit.getBaseHelper().checkDataset(50, ACTIVE_ITEMS.size());
            case "business-time-checkbox" -> helpersInit.getBaseHelper().checkDataset(118, ACTIVE_ITEMS.size());
            case "days-checkbox" -> helpersInit.getBaseHelper().checkDataset(48, ACTIVE_ITEMS.size());
            case "weekends-checkbox" -> helpersInit.getBaseHelper().checkDataset(120, ACTIVE_ITEMS.size());
            default -> false;
        };
    }

    /**
     * проверка конверсий
     */
    public boolean checkConversionStages(Map<String, String> obj) {
        if (obj != null) {
            return helpersInit.getBaseHelper().checkDatasetContains(obj.get("buyTarget"), Objects.requireNonNull(TARGET_BUY.val())) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("buyCount"), BUY_SELECTED_COUNT.text()) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("buyCpa"), Objects.requireNonNull(CPA_ACTION.val())) &&

                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("desireTarget"), Objects.requireNonNull(TARGET_DECISION.val())) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("desireCount"), DECISION_SELECTED_COUNT.text()) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("desireCpa"), Objects.requireNonNull(CPA_DECISION.val())) &&

                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("interestTarget"), Objects.requireNonNull(TARGET_INTEREST.val())) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("interestCount"), INTEREST_SELECTED_COUNT.text()) &&
                    helpersInit.getBaseHelper().checkDatasetContains(obj.get("interestCpa"), Objects.requireNonNull(CPA_INTEREST.val()));
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    /**
     * проверка лимитов
     */
    public boolean checkLimitOption(String type, String day, String general) {
        return helpersInit.getBaseHelper().checkDatasetContains(type, Objects.requireNonNull(SELECTED_LIMIT_TYPE.val())) &&
                helpersInit.getBaseHelper().checkDatasetContains(day, LIMIT_PER_DAY.text()) &&
                helpersInit.getBaseHelper().checkDatasetContains(general, LIMIT_GENERAL.text());
    }

    /**
     * проверка сорс тега
     */
    public boolean checkSourceTag(String type) {
        return helpersInit.getBaseHelper().checkDatasetContains(type, Objects.requireNonNull(UTM_SOURCE.val()));
    }

    /**
     * проверка медиум тега
     */
    public boolean checkMediumTag(String type) {
        return helpersInit.getBaseHelper().checkDatasetContains(type, Objects.requireNonNull(UTM_MEDIUM.val()));
    }

    /**
     * проверка кампайн тега
     */
    public boolean checkCampaignTag(String type) {
        return helpersInit.getBaseHelper().checkDatasetContains(type, Objects.requireNonNull(UTM_CAMPAIGN.val()));
    }

    /**
     * проверка кастомного тега
     */
    public boolean checkCustomTag(String type) {
        return helpersInit.getBaseHelper().checkDatasetContains(type, Objects.requireNonNull(UTM_CUSTOM_FIELD.val()));
    }

    /**
     * включение опции дублирования пуш кампании
     */
    public void usePushDuplicate(boolean useDuplicate) {
        if (useDuplicate) {
            String val = Objects.requireNonNull(PUSH_DUPLICATE_SWITCHER.getAttribute("class"));
            if (val.equalsIgnoreCase("off")) {
                clickInvisibleElementJs(PUSH_DUPLICATE_SWITCHER);
            }
        }
    }

    /**
     * включение опции дублирования пуш кампании
     */
    public void setGoogleTagManagerConversion() {
        GOOGLE_TAG_TAB.click();
        if (Objects.requireNonNull(GOOGLE_TAG_MANAGER_SWITCHER.getAttribute("class")).equalsIgnoreCase("off"))
            clickInvisibleElementJs(GOOGLE_TAG_MANAGER_SWITCHER);
        checkErrors();
    }

    /**
     * включение опции дублирования пуш кампании
     */
    public void enableTabGoogleAnalyticsConversion() {
        GOOGLE_ANALYTICS_TAB.click();
        checkErrors();
    }
    
    /**
     * проверка отображения опции конверсии
     */
    public boolean checkIsEnabledConversion(SelenideElement elem) {
        return helpersInit.getBaseHelper().checkDatasetContains(elem.getAttribute("class"), "on");
    }

    /**
     * проверка отображения опции конверсии google analytics
     */
    public boolean checkIsVisibleGoogleAnalytics(SelenideElement elem) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(elem.isDisplayed());
    }

    /**
     * проверка отображения опции конверсии яндекс метрики
     */
    public boolean checkIsVisibleYandexMetrix(SelenideElement elem) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(elem.isDisplayed());
    }

    /**
     * clear chosen targeting
     */
    public void clearTargetingSettings(boolean state) {
        while (REMOVE_SELECTED_TARGET_BUTTONS.size() > 0 && state) {
            clickInvisibleElementJs(REMOVE_SELECTED_TARGET_BUTTON);
        }
    }

    public void enableTargeting(boolean state) {
       if(state && DYNAMIC_RETARGETING_SWITCHER.text().equalsIgnoreCase("off")) {
           DYNAMIC_RETARGETING_SWITCHER.click();
        } else if(!state && DYNAMIC_RETARGETING_SWITCHER.text().equalsIgnoreCase("on")){
           DYNAMIC_RETARGETING_SWITCHER.click();
       }
       waitForAjax();
    }

    /**
     * check name of campaign at campaign list interface
     */
    public void selectLocationTarget(String typeTargeting, String... countries) {
        LOCATION_TARGET.click();
        Arrays.asList(countries).forEach(el -> clickInvisibleElementJs($x("//*[text()='" + el + "']/../*[@class='targeting-btn targeting-" + typeTargeting + "']")));
    }

    /**
     * Select target by city
     */
    public JsonObject selectCityTargeting() {
        JsonObject dataLocation;
        String chosenType = getTypeTargeting();

        LOCATION_TARGET.click();
        dataLocation = chooseRandomCityTarget(chosenType);

        LOCATION_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Location selected - " + dataLocation.get("data-id").toString());

        return dataLocation;
    }

    /**
     * Choose random city target
     */
    private JsonObject chooseRandomCityTarget(String typeOfTarget) {
        JsonObject data = new JsonObject();
        int indexOfElement;
        ElementsCollection listOptions;
        String data_id;

        expandAllTargetElements();

        listOptions = LOCATION_TARGET_CITY_ELEMENTS;
        data_id = listOptions.get(indexOfElement = randomNumbersInt(listOptions.size())).closest("li").attr("data-id");
        //choose random target element

        SelenideElement targetingItem = listOptions.get(indexOfElement);
        targetingItem.hover().find(By.xpath(String.format(TARGET_CITY_TYPE_TARGETING_BUTTON_, typeOfTarget))).shouldBe(visible).click();
        data.addProperty("data-id", data_id);
        data.addProperty("typeTarget", typeOfTarget);
        checkErrors();
        return data;
    }

    /**
     * Switch Conversion tracking settings
     */
    public void changeStateConversionTracking(boolean switcherState) {
        if (switcherState) {
            if (CONVERSION_SWITCHER_STATE_OFF.isDisplayed()) CONVERSION_SWITCHER_STATE_OFF.click();
        } else {
            if (CONVERSION_SWITCHER_STATE_ON.isDisplayed()) CONVERSION_SWITCHER_STATE_ON.click();
        }
    }


    /**
     * Method check selected city target
     */
    public boolean checkSelectedCityTarget(JsonObject targetingElements) {
        String currentKey = "location";
        String dataId, typeTargeting;
        typeTargeting = targetingElements.getAsJsonObject().get("typeTarget").getAsString();
        dataId = targetingElements.getAsJsonObject().get("data-id").getAsString();
        return helpersInit.getBaseHelper().getTextAndWriteLog($(String.format(CHOSEN_TARGET_ELEMENT, currentKey, typeTargeting, dataId)).isDisplayed());
    }

    /**
     * Choose language for campaign without context targeting
     */
    public String chooseLanguageWithoutContext(List<String> languagesForContext) {
        List<String> languagesAll = new ArrayList<>();
        for (SelenideElement element : CAMPAIGN_LANGUAGE.findAll("span")) {
            languagesAll.add(element.getAttribute("val"));
        }
        List<String> differences = languagesAll
                .stream().filter(i -> !languagesForContext.contains(i))
                .collect(Collectors.toList());
        return differences.get(randomNumbersInt(differences.size()));
    }
}
