package pages.dash.advertiser.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.sorted.SortedType;

import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.advertiser.locators.SelectiveBiddingNewLocators.*;
import static core.helpers.BaseHelper.*;

public class SelectiveBiddingNewHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public SelectiveBiddingNewHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * выбор рандомных чекбоксов елементов
     */
    public void chooseRandomSources() {
        int sizeOfNotCheckerElements;
        for (int i = 0; i < 3; i++) {
            sizeOfNotCheckerElements = NOT_CHECKED_ELEMENTS_CHECKBOXES.size();
            NOT_CHECKED_ELEMENTS_CHECKBOXES.get(randomNumbersInt(sizeOfNotCheckerElements));
        }
    }

    /**
     * нажатие на кнопку блокировать/активировать для массовых действий
     */
    public void clickBlockUnblockButtonMass(String state) {
        String buttonIdentifier = state.equalsIgnoreCase("block") ? "lock" : "lock_open";
        $x("//*[text()='" + buttonIdentifier + "']").click();
        sleep(5000);
    }

    /**
     * Фильтр по статусу блокировки
     */
    public void filterByStatus(String option) {
        FILTER_STATUS.click();
        $x("//*[text()='" + option + "']").click();
    }

    /**
     * Фильтр по статусу коефициента
     */
    public void filterByCoefficient(String option) {
        FILTER_COEFFICIENT.click();
        $x("//*[text()='" + option + "']").click();
    }

    /**
     * Производим активирование елементов в заданом количестве
     */
    public void unblockRandomSources(int amount) {
        for (int i = 0; i < amount; i++) BLOCKED_SOURCE.click();
        ACTIVE_SOURCES.shouldHave(CollectionCondition.size(amount));
    }

    /**
     * Производим блокировку елементов в заданом количестве
     */
    public void blockRandomSources(int amount) {
        for (int i = 0; i < amount; i++) ACTIVE_SOURCE.click();
    }

    /**
     * Производим изменение коефициентов посредством провайдера
     */
    public void changePriceByProvider(String price) {
        EDIT_COEFFICIENT_PROVIDER_BUTTON.hover().click();
        COEFFICIENT_PROVIDER_INPUT.doubleClick();
        COEFFICIENT_PROVIDER_INPUT.sendKeys(price);
        COEFFICIENT_PROVIDER_SUBMIT_BUTTON.click();
        waitForAjax();
        COEFFICIENT_PROVIDER_SUBMIT_BUTTON.shouldBe(hidden, ofSeconds(16));
    }

    /**
     * Производим изменение коефициента внутри провайдера
     */
    public void changePriceBySource(String price) {
        EDIT_COEFFICIENT_SOURCE_BUTTON.hover();
        EDIT_COEFFICIENT_SOURCE_BUTTON.hover().click();
        COEFFICIENT_SOURCE_INPUT.doubleClick();
        COEFFICIENT_SOURCE_INPUT.sendKeys(price);
        COEFFICIENT_SOURCE_SUBMIT_BUTTON.click();
        waitForAjax();
        COEFFICIENT_SOURCE_SUBMIT_BUTTON.shouldBe(hidden, ofSeconds(8));
        COEFFICIENT_SOURCE_INPUT.shouldBe(hidden, ofSeconds(8));
        sleep(1000);
    }

    /**
     * Выборка елементов c одинаковым коефициентом
     */
    public int getAmountOfSourcesWithPrice(String price) {
        COEFFICIENT_DATA_SOURCES.get(0).shouldBe(visible, ofSeconds(8));
        return COEFFICIENT_DATA_SOURCES.filter(Condition.text(price)).size();
    }

    /**
     * Производим проверку сортированных данных
     */
    public boolean checkSortedSelective(Object[] data, SortedType.SortType sortType) {
        return helpersInit.getSortedHelper().checkSortedData(sortType, data);
    }

    /**
     * Производим сортировку внутри столбца
     * и возвращаем отсортированые данные
     */
    public Object[] sortColumnAndGetDataProviders(SortedType.SortType sortType, String customColumn) {
        // choose sorted column by random header
        // без первоначального клика по нужному елементу $("[class='selective-header'] [aria-label='Change sorting for " +  customColumn + "']")
        // отсутствует нужный атрибут елемента который используется для выбора заданной сортировки из за этого и производим начальный клик
        // число 3 в цикле обозначает полный цикл состояния сортировки для нужного елемента(ASC, DESC, отсутствует сортировка)
        $x("//mat-header-cell[contains(.,'" + customColumn + "')]").shouldBe(Condition.visible).click();
        sleep(2000);
        $("[role='progressbar']").shouldBe(hidden);
        for (int i = 0; i < 3; i++) {
            // attr хранит тип сортировки
            String attr = $("[class*='mat-column-" + customColumn.toLowerCase() + "'][aria-sort]").getAttribute("aria-sort");
            log.info(attr);
            //кликаем по иконке смены типа сортировки при условии что текущий атрибут не соответствует нужной сортировки
            if (!Objects.requireNonNull(attr).contains(sortType.getSortValue())) {
                log.info("click");
                $x("//mat-header-cell[contains(.,'" + customColumn + "')]").click();
                sleep(2000);

            }
        }
        return $$("mgx-selective-sources [class*='mat-column-" + customColumn.toLowerCase() + "'] [class*='mat-sort-header-content']").texts().toArray();
    }

    /**
     * Производим сортировку внутри провайдера
     * и возвращаем отсортированые данные
     */
    public Object[] sortColumnAndGetDataSources(SortedType.SortType sortType, String customColumn) {
        // choose sorted column by random header
        // без первоначального клика по нужному елементу $("[class='selective-header'] [aria-label='Change sorting for " +  customColumn + "']")
        // отсутствует нужный атрибут елемента который используется для выбора заданной сортировки из за этого и производим начальный клик
        // число 3 в цикле обозначает полный цикл состояния сортировки для нужного елемента(ASC, DESC, отсутствует сортировка)
//        $x("//*[text()='" + customColumn + "']").shouldBe(Condition.visible).click();
        $x("//*[contains(span,'" + customColumn + "')]").shouldBe(Condition.visible).click();
        for (int i = 0; i < 3; i++) {
            // attr хранит тип сортировки
            String attr = $("[class*='cdk-column-" + customColumn.toLowerCase() + "']").isDisplayed() ? $("[class*='cdk-column-" + customColumn.toLowerCase() + "']").getAttribute("aria-sort") : "";
            //кликаем по иконке смены типа сортировки при условии что текущий атрибут не соответствует нужной сортировки
            if (!Objects.requireNonNull(attr).contains(sortType.getSortValue())) {
//                $x("//*[text()='" + customColumn + "']").click();
                $x("//*[contains(span,'" + customColumn + "')]").click();
            }
        }
        sleep(2000);
        expandAllProviders(1);
        return $$("section [class*='cdk-cell cdk-column-" + customColumn.toLowerCase() + "']").shouldHave(CollectionCondition.sizeGreaterThan(2)).texts().toArray();
    }

    public void expandAllProviders(int  amountProviders) {
        EXPAND_BUTTON.shouldBe(Condition.visible);
        EXPAND_BUTTONS.asDynamicIterable().forEach(el-> {clickInvisibleElementJs(el); waitForAjax();});
        STATE_EXPAND_BUTTON_SHRINK.shouldNotBe(Condition.exist);
        SOURCES_SECTIONS.shouldHave(CollectionCondition.size(amountProviders));
    }
}
