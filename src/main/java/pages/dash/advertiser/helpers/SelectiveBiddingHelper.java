package pages.dash.advertiser.helpers;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.WebElement;
import core.base.HelpersInit;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.SelectiveBiddingLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.OthersData.LINK_TO_RESOURCES_FILES;

public class SelectiveBiddingHelper {
    private final HelpersInit helpersInit;
    String getBasePath;

    public SelectiveBiddingHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public void setGetBasePath(String getBasePath) {
        this.getBasePath = getBasePath;
    }

    public String getGetBasePath() {
        return getBasePath;
    }

    /**
     * метод возвращает базовую часть пути к widget/source в селективном аукционе
     * на вход принимает widget-row / source-row
     *
     * @param param
     * @return
     */
    public String getBasePathToWidget(String param, Integer... count) {
        if (param.equals("")) {
            return param + "[data-widget-uid='" + $$("tr[data-widget-uid]"/*:not([data-has-sources])*/).get(count.length > 0 ? count[0] : 0).shouldBe(visible).attr("data-widget-uid") + "']";
        }
        return param + "[data-widget-uid='" + $$(param).get(count.length > 0 ? count[0] : 0).shouldBe(visible).attr("data-widget-uid") + "']";
    }

    public String getBasePathAboutWidget(String param, String widgetId) {
        return param + "[data-widget-uid='" + widgetId + "']";
    }

    public String getBasePathToSource(String source) {
        String id = $$(".source-row").get(0).shouldBe(visible).getAttribute("data-widget-uid");
        return ".source-row[data-widget-uid='" + id + "'][data-source-id='" + source + "']";
    }

    public String getBasePathToSource(String widgetId, String source) {
        return ".source-row[data-widget-uid='" + widgetId + "'][data-source-id='" + source + "']";
    }

    public String getQfValue(String... basePath) {
        return $((basePath.length > 0 ? basePath[0] : getBasePath) + PRICE_QF_VALUE).text();
    }

    public void setNewQfValue(String coefficientValue, boolean isClickSource) {
        if (isClickSource) $(getBasePath + EDIT_QF_ICON).hover().click();
        waitForAjax();
        $(getBasePath + EDIT_QF_FORM).shouldBe(visible);
        if(POPUP_COEF.isDisplayed()) {
            POPUP_COEF.click();
        }
        sendKey($(getBasePath + EDIT_QF_INPUT), coefficientValue);
        $(getBasePath + SAVE_QF_BUTTON).click();
    }

    public String getToolTipValue(String basePath) {
        return $(basePath + " .campaignQualityAnalysisQFTooltip").getAttribute("title");
    }

    public void clickWidgetCheckbox(String basePath) {
        clickInvisibleElementJs($(basePath + WIDGET_OR_SOURCE_CHECKBOX));
    }

    public void setQfValueWithMassAction(String value) {
        EDIT_QF_VALUE_MASS_ACTION.shouldBe(visible).click();
        INPUT_QF_VALUE_MASS_ACTION.shouldBe(visible);
        sendKey(INPUT_QF_VALUE_MASS_ACTION, value);
        SAVE_QF_VALUE_MASS_ACTION.click();
    }

    /**
     * import platforms for Product campaign -> Publishers efficiency
     */
    public void setFileForImport(String file) {
        IMPORT_BUTTON.shouldBe(visible).click();
        IMPORT_SELECTIVE_COEFFICIENTS.shouldBe(visible).click();
        IMPORT_INPUT.shouldBe(visible).sendKeys(LINK_TO_RESOURCES_FILES + file);
        IMPORT_SAVE.shouldBe(visible).click();
        sleep(2000);
        waitForAjax();
        helpersInit.getBaseHelper().getTextAndWriteLog(file);
        $(EXTEND_ICON).shouldBe(visible);
    }

    public void expandSubSourceTrees() {
        waitForAjax();

        //        waitForAjaxLoader();
        if ($(EXTEND_ICON).exists()) {
            ElementsCollection list = $$(EXTEND_ICON);
            if (list.size() > 0) {
                for (WebElement v : list) {
                    clickInvisibleElementJs($(v).shouldBe(visible));
                    waitForAjax();
                }
            }
        }
        $(EXTEND_ICON).shouldBe(hidden);

    }

    /**
     * select current status in filter 'STATUS'
     */
    public void selectStatusFilter(String status) {
        helpersInit.getBaseHelper().selectCurrentTextInSelectListJS(STATUS_FILTER_SELECT, status);
    }

    public void filterByStatus() {
        FILTER_BUTTON.click();
        checkErrors();
    }

    public String getCoefficientInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($(PRICE_QF_VALUE).text().trim());
    }

    public boolean checkCoefficientInCab(String[] massSource, String[] coef) {
        List<String> arr = Arrays.stream(massSource).map(el -> $x(".//tr[@data-uid='" + el + "']//span[@class='qf_value']").text().replace(",", ".")).collect(Collectors.toList());
        return helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(arr).containsAll(Arrays.asList(coef)));
    }

    public boolean switchSource(String[] mass, boolean flag) {
        int count = 0;
        for (String s : mass) {
            if (!$x(".//tr[@data-widget-uid='" + s + "']//input[@type='checkbox']").isSelected()) {
                $x(".//tr[@data-widget-uid='" + s + "']//input[@type='checkbox']").click();
            }
        }
        $x(".//a[contains(@class, '" + (flag ? "icons3" : "icons2") + "')]").click();
        waitForAjax();

        for (String s : mass) {
            if ($x(".//tr[@data-widget-uid='" + s + "']//*[@class='switch']/div").getAttribute("class").equals(flag ? "on" : "off")) {
                count++;
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(count == mass.length);
    }

    public boolean switchSubSource(String[] mass, boolean flag) {
        int count = 0;
        for (String s : mass) {
            if (!$x(".//tr[@data-source-id and td[contains(text(), '" + s + "')]]//input[@type='checkbox']").isSelected()) {
                $x(".//tr[@data-source-id and td[contains(text(), '" + s + "')]]//input[@type='checkbox']").click();
            }
        }
        $x(".//a[contains(@class, '" + (flag ? "icons3" : "icons2") + "')]").click();
        waitForAjax();

        for (String s : mass) {
            if ($x(".//tr[@data-source-id and td[contains(text(), '" + s + "')]]//*[@class='switch']/div").getAttribute("class").equals(flag ? "on" : "off")) {
                count++;
            }
        }
        return count == mass.length;
    }
}
