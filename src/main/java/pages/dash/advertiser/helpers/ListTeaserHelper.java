package pages.dash.advertiser.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.time.Duration;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.ListTeaserLocators.*;
import static pages.dash.advertiser.variables.TeaserVariables.clickPrice;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;

public class ListTeaserHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public ListTeaserHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public String getStatusTeaser() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(STATUS_TEASER.text());
    }

    public String getTitleTeaser() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(TITLE_LIST).get(0).text());
    }

    public String getCpcTeaser() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_CPC.text());
    }

    public String getTypeTeaser() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_TYPE.text());
    }

    public String getCallToActionTeaser() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CALL_TO_ACTION.text());
    }

    /**
     * Проверка заголовка тизера
     */
    public boolean checkTeaserTitle(int teaserId, String expectedTeaserTitle) {
        return expectedTeaserTitle == null || helpersInit.getBaseHelper().checkDatasetEquals($x(".//div[contains(@class, 'checkDirectionEl') and @id='tit_" + teaserId + "']").shouldBe(visible).getText(), expectedTeaserTitle);
    }

    /**
     * check teaser description
     */
    public boolean checkTeaserDescription(int teaserId, String expectedTeaserDescription) {
        return expectedTeaserDescription == null || helpersInit.getBaseHelper().checkDatasetEquals($x(".//div[contains(@class, 'checkDirectionEl') and @id='text_" + teaserId + "']").shouldBe(visible).getText(), expectedTeaserDescription);
    }

    /**
     * check teaser call to action
     */
    public boolean checkCallToAction(String callToAction) {
        return callToAction == null || helpersInit.getBaseHelper().checkDatasetEquals(CALL_TO_ACTION.text(), callToAction);
    }

    public int getTeaserId() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().parseInt(TEASER_ID_LOCATOR.attr("data-id")));
    }

    /**
     * check teasers (we check where we found - create/list interface)
     */
    public boolean isTeaserDisplayed() {
        return TEASER_ID_LOCATOR.exists();
    }

    /**
     * Открытие интерфейса редактирования тизера(попап)
     */
    public void openTeasersEditInterface(int teaserId) {
        log.info("открываем форму TeasersEditInterface");
        helpersInit.getBaseHelper().waitDisplayedElement($("[data-id='" + teaserId + "'] a[class*=edit][data-id]"));
        clickInvisibleElementJs($("[data-id='" + teaserId + "'] a[class*=edit][data-id]"));
        isConfirmDisplayed();

        SAVE_FORM.shouldBe(visible, Duration.ofSeconds(10));

        checkErrors();
    }

    public boolean checkTeaserDomain(String domain) {
        return domain == null || helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_DOMAIN.attr("href")).contains(domain);
    }

    /**
     * get link for preview in informer
     */
    public String getLinkForPreviewTeaserInWidget() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PREVIEW_IN_INFORMER_LINK.shouldBe(visible).attr("href"));
    }

    /**
     * show push campaign image
     */
    public boolean showPushCampaignImageInListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUSH_CAMPAIGN_IMAGE.shouldBe(visible).isDisplayed());
    }

    /**
     * Unblock teaser by icon
     */
    public void unBlockTeaser() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        if (TEASER_UNBLOCK_ICON.isDisplayed()) {
            log.info("unblock teaser");
            TEASER_UNBLOCK_ICON.hover().click();
            TEASER_BLOCK_ICON.shouldBe(visible);
        }
    }

    /**
     * Block teaser by icon
     */
    public void blockTeaser() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        if (TEASER_BLOCK_ICON.isDisplayed()) {
            log.info("block teaser");
            TEASER_BLOCK_ICON.hover().click();
            waitForAjax();
            TEASER_UNBLOCK_ICON.shouldBe(visible);
        }
    }

    /**
     * Check if teaser is Blocked
     */
    public boolean checkIsTeaserBlocked() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        return TEASER_UNBLOCK_ICON.isDisplayed();
    }

    /**
     * Unblock teaser by Mass actions
     */
    public void unBlockTeaserMassAction() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        if (TEASER_UNBLOCK_ICON.isDisplayed()) {
            log.info("Unblock teaser Mass Action");
            clickMassAction();
            UNBLOCK_MASS_ACTION_ICON.hover().click();
            waitForAjax();
            TEASER_BLOCK_ICON.shouldBe(visible);
        }
    }

    /**
     * Choose teaser if it not chosen yet
     */
    public void clickMassAction() {
        if (!FOR_TEASERS_MASS_ACTIONS_CHECKBOX.isSelected()) FOR_TEASERS_MASS_ACTIONS_CHECKBOX.click();
    }

    /**
     * Check if teaser is Unblocking and block it by Mass actions
     */
    public void blockUnblockedTeaserByMassAction() {
        helpersInit.getBaseHelper().waitDisplayedElement(TEASER_ID_LOCATOR);
        if (TEASER_BLOCK_ICON.isDisplayed()) {
            log.info("block teaser Mass Action");
            clickMassAction();
            BLOCK_MASS_ACTION_ICON.hover().click();
            waitForAjax();
            TEASER_UNBLOCK_ICON.shouldBe(visible);
        }
    }

    /**
     * Block teaser by mass action
     */
    public void reblockBlockedTeaserByMassAction() {
        log.info("block teaser Mass Action");
        clickMassAction();
        BLOCK_MASS_ACTION_ICON.hover().click();
        waitForAjax();
    }

    /**
     * Import teaser
     */
    public void createTeaserByImport(String nameFileForImportTeaser) {
        log.info("открываем поп-ап для импорта тизеров и передаём в него файл, цену и сохраняем");
        IMPORT_TEASER_BUTTON.shouldBe(visible).click();
        IMPORT_TEASER_LOAD_BUTTON.shouldBe(visible).val(LINK_TO_RESOURCES_FILES + nameFileForImportTeaser);
        clearAndSetValue(IMPORT_TEASER_PRICE_INPUT, clickPrice);
        IMPORT_TEASER_SAVE_FORM.shouldBe(visible).click();

        // ожидаем окончания загрузки файла
        IMPORT_TEASER_LOAD_INDICATOR.shouldBe(hidden, Duration.ofSeconds(16));
        checkErrors();

        log.info("получаем ответ о импорте тизера - " + NOTIFICATION_TEXT.text());
    }

    /**
     * Import teaser with setting CPC
     */
    public void createTeaserByImportWithCPC(String nameFileForImportTeaser, String cpcValue) {
        log.info("открываем поп-ап для импорта тизеров и передаём в него файл, цену и сохраняем");
        IMPORT_TEASER_BUTTON.shouldBe(visible).click();
        IMPORT_TEASER_LOAD_BUTTON.shouldBe(visible).val(LINK_TO_RESOURCES_FILES + nameFileForImportTeaser);
        clearAndSetValue(IMPORT_TEASER_PRICE_INPUT, cpcValue);
        IMPORT_TEASER_SAVE_FORM.shouldBe(visible).click();

        // ожидаем окончания загрузки файла
        IMPORT_TEASER_LOAD_INDICATOR.shouldBe(hidden, Duration.ofSeconds(16));
        checkErrors();
    }
}
