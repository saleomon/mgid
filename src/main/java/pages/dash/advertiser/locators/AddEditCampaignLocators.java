package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class AddEditCampaignLocators {

    ///////////////////////////////////////  General block    /////////////////////////////////////////
    public static final SelenideElement CAMPAIGN_NAME = $("#name");
    public static final SelenideElement CAMPAIGN_KEYWORD = $("[id='searchFeedKeyword']");
    public static final SelenideElement CAMPAIGN_TYPE = $("[id='cusel-scroll-campaignTypes']");
    public static final SelenideElement CAMPAIGN_TYPE_FIELD = $("#cuselFrame-campaignTypes");
    public static final SelenideElement CAMPAIGN_CATEGORY = $("[id='category_sel_dropdown']");
    public static final SelenideElement CAMPAIGN_LANGUAGE = $("#cusel-scroll-campaignLang");
    public static final SelenideElement BLOCK_AFTER_CREATE_SWITCH = $("[class='box tour-step'] [class='switch'] [class]");

    ///////////////////////////////////////  Schedule block    /////////////////////////////////////////
    public static final SelenideElement SCHEDULE_SWITCHER_STATE_OFF = $("#schedule_switch .off");
    public static final SelenideElement SCHEDULE_SWITCHER_STATE_ON = $("#schedule_switch .on");
    public static final SelenideElement RESET_SCHEDULE_SETTINGS_BUTTON = $("[class='reset-button']");
    public static final ElementsCollection WEEK_DAYS = $$("[class='week-day'][id]");
    public static final SelenideElement START_DATE_SELECT = $("[class='field start']");
    public static final SelenideElement END_DATE_SELECT = $("[class='field end']");
    public static final SelenideElement NEXT_START_MONTH_BUTTON = $("[class*='popup start'] [class='right']");
    public static final SelenideElement NEXT_END_MONTH_BUTTON = $("[class*='popup end'] [class='right']");
    public static final ElementsCollection AMOUNT_OF_DAY_FIRST = $$("[id*='c1d_'][class='day']");
    public static final ElementsCollection AMOUNT_OF_DAY_SECOND = $$("[id*='c2d_'][class='day']");
    public static final SelenideElement CALENDAR_LABEL = $("[class='calendar-label']");
    public static final ElementsCollection HOUR_ITEMS = $$("[class*='hour-th']");
    public static final ElementsCollection WEEK_ITEMS = $$("[class='week-day'][id]");
    public static final ElementsCollection ACTIVE_ITEMS = $$("[class*='day-button']:not(.blocked)");
    public static final ElementsCollection DRIVE_SCHEDULE_CHECKBOXES = $$("[class='presets'] [type='checkbox']");

    ///////////////////////////////////////  Limits block    /////////////////////////////////////////
    public static final SelenideElement SELECT_LIMITS = $("#cusel-scroll-limit_type");
    public static final SelenideElement LIMIT_CLICKS_PER_DAY = $("#limitClicksPerDay");
    public static final SelenideElement LIMIT_CLICKS_GENERAL = $("#limitClicks");
    public static final SelenideElement LIMIT_BUDGET_GENERAL = $("#limitPerCampaign-visible");
    public static final SelenideElement LIMIT_BUDGET_PER_DAY = $("#limitPerDay-visible");
    public static final SelenideElement SELECTED_LIMIT_TYPE = $("[id='cuselFrame-limit_type'] input");
    public static final SelenideElement LIMIT_GENERAL = $("[id='limitPerCampaign-visible']");
    public static final SelenideElement LIMIT_PER_DAY = $("[id='limitPerDay-visible']");
    public static final SelenideElement IS_SHOW_ALL_TEASER_AFTER_CONVERSION_CHECKBOX = $("[id='useConversionCapping']");
    public static final SelenideElement PERIOD_OF_TIME_NOT_SHOW_TEASERS = $("[id='conversionCappingDayCount']");

    ///////////////////////////////////////  UTM block    /////////////////////////////////////////
    public static final SelenideElement UTM_SWITCHER_STATE_OFF = $("#tracking_switch .off");
    public static final SelenideElement UTM_SWITCHER_STATE_ON = $("#tracking_switch .on");
    public static final SelenideElement UTM_SOURCE = $("#utm_source");
    public static final SelenideElement UTM_MEDIUM = $("#utm_medium");
    public static final SelenideElement UTM_CAMPAIGN = $("#utm_campaign");
    public static final SelenideElement UTM_CUSTOM = $("#utm_custom_tracking_tag");
    public static final SelenideElement UTM_CUSTOM_FIELD = $("#utm_custom");

    ///////////////////////////////////////  Conversion block    /////////////////////////////////////////
    public static final SelenideElement CPA_INTEREST = $("#cpa-interest");
    public static final SelenideElement CPA_DECISION = $("#cpa-decision");
    public static final SelenideElement CPA_ACTION = $("#cpa-buy");
    public static final SelenideElement TARGET_BUY = $("[id='sensorTarget-buy']");
    public static final SelenideElement TARGET_DECISION = $("[id='sensorTarget-decision']");
    public static final SelenideElement TARGET_INTEREST = $("[id='sensorTarget-interest']");
    public static final SelenideElement ADD_CONVERSION_BUTTON = $("[class*='conversion mg'] [class*='conversion_btn add-step top']");
    public static final SelenideElement GOAL_SELECT_BUY = $("[id='cusel-scroll-conditionKind-buy']");
    public static final SelenideElement TARGET_SELECT_BUY = $("[id='cusel-scroll-sensorTarget-buy']");
    public static final SelenideElement INTEREST_CONVERSION_COUNTING = $("[id='cuselFrame-deduplicateConversions-interest']");
    public static final SelenideElement INTEREST_SELECTED_COUNT = $("[id='cuselFrame-deduplicateConversions-interest'] [class='cuselText']");
    public static final SelenideElement DECISION_SELECTED_COUNT = $("[id='cuselFrame-deduplicateConversions-decision'] [class='cuselText']");
    public static final SelenideElement BUY_SELECTED_COUNT = $("[id='cuselFrame-deduplicateConversions-buy'] [class='cuselText']");
    public static final SelenideElement DESIRE_CONVERSION_COUNTING = $("[id='cuselFrame-deduplicateConversions-decision']");
    public static final SelenideElement BUY_CONVERSION_COUNTING = $("[id='cuselFrame-deduplicateConversions-buy']");
    public static final SelenideElement DECISION_CONVERSION_COUNTING_ELEMENT = $("[id=cuselFrame-deduplicateConversions-decision] [class='cuselText']");
    public static final SelenideElement BUY_CONVERSION_COUNTING_ELEMENT = $("[id=cuselFrame-deduplicateConversions-buy] [class='cuselText']");
    public static final SelenideElement INTEREST_CONVERSION_COUNTING_ELEMENT = $("[id=cuselFrame-deduplicateConversions-interest] [class='cuselText']");
    public static final SelenideElement GOAL_SELECT_DECISION = $("[id='cusel-scroll-conditionKind-decision']");
    public static final SelenideElement TARGET_SELECT_DECISION = $("[id='cusel-scroll-sensorTarget-decision']");
    public static final SelenideElement GOAL_SELECT_INTEREST = $("[id='cusel-scroll-conditionKind-interest']");
    public static final SelenideElement TARGET_SELECT_INTEREST = $("[id='cusel-scroll-sensorTarget-interest']");
    public static final SelenideElement CONVERSION_SWITCHER_STATE_OFF = $("#conversion_switch .off");
    public static final SelenideElement CONVERSION_SWITCHER_STATE_ON = $("#conversion_switch .on");
    public static final SelenideElement GOOGLE_TAG_TAB = $("[class*='gtmStyle']");
    public static final SelenideElement GOOGLE_ANALYTICS_TAB = $("[class*='gaStyle']");
    public static final SelenideElement YANDEX_METRICS_TAB = $("[class*='yandexStyle']");
    public static final SelenideElement GOOGLE_TAG_MANAGER_SWITCHER = $("[class='sensor-block block conversion gtm'] [class='switch'] [class]");
    public static final SelenideElement GOOGLE_ANALYTICS_SWITCHER = $("[id='conversion_ga_switch'] [class]");
    public static final SelenideElement YANDEX_METRIX_SWITCHER = $("[id='conversion_ym_switch'] [class]");

    ///////////////////////////////////////  Dynamic retargeting block    /////////////////////////////////////////
    public static final SelenideElement DYNAMIC_RETARGETING_SWITCHER = $("[id='targeting_switch'] [class]");
    public static final SelenideElement FILE_FIELD = $("[id='fileARetargeting']");
    public static final SelenideElement PRICE_FIELD = $("[id='price-for-all-locations'] [type='text']");

    ///////////////////////////////////////  Targeting block    /////////////////////////////////////////
    public static final SelenideElement AVAILABLE_TARGETINGS_BLOCK = $(".targeting_available");
    public static final SelenideElement LOCATION_OUT_BLOCK = $("[class='location-out']");
    public static final SelenideElement OS_OUT_BLOCK = $("[class='os-out']");
    public static final SelenideElement BROWSER_OUT_BLOCK = $("[class='browser-out']");
    public static final SelenideElement LANGUAGE_OUT_BLOCK = $("[class='language-out']");
    public static final SelenideElement CONNECTION_OUT_BLOCK = $("[class='connectionType-out']");
    public static final SelenideElement DEMOGRAPHICS_OUT_BLOCK = $("[class='socdem-out']");
    public static final SelenideElement INTERESTS_OUT_BLOCK = $("[class='interest-out']");
    public static final SelenideElement AUDIENCE_OUT_BLOCK = $("[class='audience-out']");
    public static final SelenideElement TRAFFIC_TYPE_OUT_BLOCK = $("[class='trafficType-out']");
    public static final SelenideElement TRAFFIC_TYPE_TAB = $("[id='trafficType']");
    public static final SelenideElement CONTEXT_TAB = $("[id='context']");
    public static final SelenideElement CONTEXT_OUT_BLOCK = $("[class='context-out']");
    public static final SelenideElement SENTIMENTS_TAB = $("[id='sentiments']");
    public static final SelenideElement SENTIMENTS_OUT_BLOCK = $("[class='sentiments-out']");
    public static final SelenideElement PHONE_PRICE_RANGES_TAB = $("[id='phonePriceRange']");
    public static final SelenideElement PHONE_PRICE_RANGES_OUT_BLOCK = $("[class='phonePriceRange-out']");
    public static final SelenideElement AUDIENCE_TAB = $("[id='audience']");
    public static final SelenideElement LOCATION_TARGET = $("#location");
    public static final ElementsCollection LOCATION_TARGET_CITY_ELEMENTS = $$x(".//label[contains(text(), 'City')]");
    public static final SelenideElement OS_TARGET = $("#os");
    public static final SelenideElement BROWSER_TARGET = $("#browser");
    public static final SelenideElement BROWSER_LANGUAGE_TARGET = $("#language");
    public static final SelenideElement CONNECTION_TARGET = $("#connectionType");
    public static final SelenideElement DEMOGRAPHICS_TARGET = $("#socdem");
    public static final SelenideElement INTERESTS_TARGET = $("#interest");
    public static final ElementsCollection INTERESTS_TARGET_LEVEL_0_ITEM = $$("#interest-in li[level='0']");
    public static final String CHOSEN_TARGET_ELEMENT = "[id='%s-out'] [class*='targeting-list-el %s'][data-id='%s']";
    public static final String CHOSEN_TARGET_ELEMENT_WITHOUT_TYPE = "[id='%s-out'] [class*='targeting-list-el'][data-id='%s']";
    public static final String CHOSEN_TARGET_ELEMENT_MULTIPLE = "[id='%s-out'] [class='targeting-list-elmultiple'][data-id='%s']";
    public static final String NOT_EXPANDED_ELEMENTS = "[class='targeting-in'] [class='targeting-list-el'] [class*='targeting-expand']";
    public static final String TARGET_ELEMENTS_LEVEL_2 = "[id='%s-in'] [level='2']";
    public static final String TARGET_ELEMENTS_LEVEL_1 = "[id='%s-in'] [level='1']";
    public static final String TARGET_ELEMENTS_LEVEL_0 = "[id='%s-in'] [level='0'][class=targeting-list-el]";
    public static final String TARGET_ELEMENTS = "[id='%s-in'] li.targeting-list-el:not(.chk)";
    public static final String TARGET_ELEMENTS_TYPE_TARGETING_BUTTON = "[class='targeting-btn targeting-%s']";
    public static final String TARGET_CITY_TYPE_TARGETING_BUTTON_ = "following-sibling::span[@class='targeting-btn targeting-%s']";
    public static final ElementsCollection REMOVE_SELECTED_TARGET_BUTTONS = $$("[class='targeting-out-title'] [class='targeting-btn targeting-remove-all']");
    public static final SelenideElement REMOVE_SELECTED_TARGET_BUTTON = $("[class='targeting-out-title'] [class='targeting-btn targeting-remove-all']");
    public static final String ROTATION_IN_UKRAINE_TARGETING_BLOCK_TEXT = "[class*='targeting-alert']";
    public static final ElementsCollection GEO_TARGETING_LABELS = $$("[id='location-out'] [class*='targeting-list-el include'] label");
    public static final ElementsCollection PLATFORM_TARGETING_LABELS = $$("[id='os-out'] [class*='targeting-list-el'] label");

    ///////////////////////////////////////  Rich meter block    /////////////////////////////////////////
    public static final SelenideElement REACH_METER_PROGRESS_BAR = $(".targeting_progressbar-inner");
    public static final SelenideElement REACH_METER_IMPRESSIONS_COUNTER = $(".impressions__count");

    ///////////////////////////////////////  Certificate block    /////////////////////////////////////////
    public static final ElementsCollection UPLOADED_IMAGES = $$("[id='uploaded-files'] span,[id='uploaded-files'] a");
    public static final SelenideElement UPLOAD_IMAGE_INPUT = $("[id=fileUploader]");
    public static final SelenideElement DELETE_CERTIFICATE_BUTTON = $("[id*='remove_']");
    public static final SelenideElement BLOCK_OF_CERTIFICATE = $("[class='block certificate']");

    ///////////////////////////////////////  Mixed block    /////////////////////////////////////////
    public static final SelenideElement SAVE_GENERAL_SETTINGS = $("[id='confirm-campaign']");
    public static final SelenideElement SAVE_CAMPAIGN_BUTTON = $("[id='save-campaign']");
    public static final SelenideElement CONFIRM_BUTTON = $("#confirm-ok");
    public static final SelenideElement SAVE_AND_CONTINUE = $("[id=confirmation] [class*='green save']");
    public static final SelenideElement CONFIRM_POPUP_BUTTON = $x("//*[text()='Ok']");
    public static final SelenideElement POPUP_CLOSE = $("[class='popup_close']");
    public static final SelenideElement POPUP_TEXT = $("[class='popup']");
    public static final SelenideElement PUSH_DUPLICATE_SWITCHER = $("[id='createPushCopyContainer'] [class='switch'] [class]");
    public static final String UNBLOCK_CAMPAIGN_BUTTON = "[class*='actions'] [data-id='%s'][class*='unblock-partner icon-play']";

}
