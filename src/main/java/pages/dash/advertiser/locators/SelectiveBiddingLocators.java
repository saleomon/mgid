package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class SelectiveBiddingLocators {
    ///////////////////////////////////// Filters  //////////////////////////////
    public static final SelenideElement SEARCH_FIELD = $("[id=search]");
    public static final SelenideElement STATUS_FILTER_SELECT = $("#cusel-scroll-status");
    public static final SelenideElement FILTER_BUTTON = $("[class='button green apply']");
    public static final ElementsCollection STATUS_ELEMENTS = $$x(".//tr[@data-widget-uid and not(@data-sort-group)]//div[@class='switch']/div");
    public static final SelenideElement COEFFICIENT_FILTER = $("[id=cusel-scroll-qualityFactor]");

    //////////////////////////////////// Source / SubSource /////////////////////
    public static final SelenideElement SUB_SOURCE_ID = $("[class='td-widget-sources-id']");
    public static final ElementsCollection WIDGET_SOURCES_IDS = $$(".td-widget-sources-id");
    public static final SelenideElement SOURCE_ID = $("[class='identifier  aright ']");
    public static final SelenideElement CHANGED_COEFFICIENT_ELEMENT = $x(".//div[contains(@class, 'campaignQualityAnalysisQFTooltip')]");
    public static final SelenideElement INITIAL_COEFFICIENT_ELEMENT = $x(".//tr[@data-widget-uid]//div[@class='informers-group' and not(descendant::div)]");
    public static final ElementsCollection ALL_DISPLAYED_ELEMENTS = $$("tr[data-widget-uid]");
    public static final String ALL_ACTIVE_ELEMENTS = "[data-widget-uid] [class=%s]";
    public static final SelenideElement SOURCE_CHECKBOX = $("[class='sortable'] [class='analysis-checkbox ']");
    public static final String BLOCK_MASS_ACTION_BUTTON = ".//a[contains(@class, 'icons%s')]";
    public static final SelenideElement EDIT_SUBSOURCE_COEFFICIENT_BUTTON = $("[class='source-row'] [class='qf-icon']");
    public static final SelenideElement EDIT_SOURCE_COEFFICIENT_BUTTON = $("[class='sortable'] [class='qf-icon']");
    public static final SelenideElement COEFFICIENT_SUBSOURCE_INPUT = $("[class='source-row'] [class*='edit-qf-value']");
    public static final SelenideElement COEFFICIENT_SOURCE_INPUT = $("[class='sortable'] [class*='edit-qf-value']");
    public static final SelenideElement SAVE_SUBSOURCE_COEFFICIENT = $("[class='source-row'] [class='qf-value-save']");
    public static final SelenideElement SAVE_SOURCE_COEFFICIENT = $("[class='sortable'] [class='qf-value-save']");
    public static final SelenideElement APPLY_FOR_SUBSOURCES_CHECKBOX = $("[class='sortable'] [class='apply-to-sources'] input");
    public static final SelenideElement SUBSOURCE_CHECKBOX = $("[class='source-row'] [class*='analysis-checkbox']");
    public static final ElementsCollection ALL_CHECKBOXES = $$(".analysis-checkbox");
    public static final SelenideElement MARK_ALL_SOURCES = $(".selectOnAllPages");
    public static final SelenideElement UNMARK_ALL_SOURCES = $(".deselectAllItems");
    public static final ElementsCollection REACH_INFORMATION = $$x("//*[text()='The data is insufficient']");
    public static final ElementsCollection ALL_SOURCES = $$("tr[data-widget-uid]:not([class='source-row'])");
    public static final ElementsCollection ACTIVE_SUB_SOURSE = $$("[class='source-row'] [class=on]");
    public static final SelenideElement ACTIVE_SOURSE = $("[class='sortable'] [class=on]");
    public static final ElementsCollection SUB_SOURCES = $$x(".//tr[@data-widget-uid and not(@data-sort-group)]");
    public static final ElementsCollection SOURCES = $$x(".//tr[@data-sort-group]");
    public static final SelenideElement WIDGET_UID_CELL = $x(".//td[count(//th[.=' Widget UID']/preceding-sibling::th)]/div[1]");

    ///////////////////////////////// Other //////////////////////////////////
    public static final String POPUP_BUTTON = "[id=confirm-%s]";
    public static final SelenideElement POPUP_COEF = $("[class*='green ok']");
    public static final SelenideElement IMPORT_SELECTIVE_COEFFICIENTS = $(".selective_bidding");
    public static final SelenideElement IMPORT_BLACKLIST = $(".blacklist");


    ///////////////////////////////// BUTTONS FOR MASS ACTIONS ////////////////////////
    public static final SelenideElement CHANGE_COEFFICIENT_MASS_BUTTON = $("[class*='icons1']");
    public static final SelenideElement COEFFICIENT_MASS_INPUT = $("[id='valuePoc-visible']");
    public static final SelenideElement SUBMIT_COEFFICIENT_MASS_BUTTON = $("[id='setPoc']");

    public static final SelenideElement EDIT_QF_VALUE_MASS_ACTION = $(".icons1");
    public static final SelenideElement INPUT_QF_VALUE_MASS_ACTION = $("#valuePoc-visible");

    public static final SelenideElement SAVE_QF_VALUE_MASS_ACTION = $("#setPoc");

    public static final SelenideElement IMPORT_BUTTON = $(".button.blue.import");

    public static final SelenideElement IMPORT_INPUT = $(".popup .file-name");

    public static final SelenideElement IMPORT_SAVE = $(".popup .file-save");

    // все елементы из этого блока будут работать только  в связке с bathPath
    // тоесть, для примера $(basePath + PRICE_VALUE);
    public static String PRICE_QF_VALUE = " [class*='qf-price']";

    public static String EDIT_QF_ICON = " .qf-icon";

    public static String EDIT_QF_FORM = " .edit-qf-form";

    public static String EDIT_QF_INPUT = " .edit-qf-value";

    public static String SAVE_QF_BUTTON = " .qf-value-save";
    public static String WIDGET_OR_SOURCE_CHECKBOX = " .analysis-checkbox";

    public static final SelenideElement COEFFICITNT_LABEL = $("[for='qualityFactor']");

    public static final String EXTEND_ICON = ".icon-extend";

    ///////////////////////////////// RULES BASE OPTIMIZATION ////////////////////////
    public static final SelenideElement OPTIMIZATION_RULES_ADD_RULE_BUTTON = $("#new-bid-rule");
    public static final SelenideElement OPTIMIZATION_RULES_AUTOBLOCKING_RULE_ITEM = $("[data-action=new-block-rule]");
    public static final SelenideElement OPTIMIZATION_RULES_FIXED_BID_RULE_ITEM = $("[data-action=new-fixed-bid-rule]");
    public static final SelenideElement OPTIMIZATION_RULES_DYNAMIC_BID_RULE_ITEM = $("[data-action=new-dynamic-bid-rule]");
    public static final SelenideElement OPTIMIZATION_RULES_OPT_RULE_SET_ITEM = $("[data-action=new-optimization_rule_preset]");
    public static final SelenideElement OPTIMIZATION_RULES_COPY_RULE_ITEM = $("[data-action=rules-copy]");
    public static final SelenideElement OPTIMIZATION_RULES_NEW_ROW = $(".rules-based__row_new");
    public static final SelenideElement OPTIMIZATION_RULES_ROW = $(".rules-based__row");
    public static final ElementsCollection OPTIMIZATION_RULES_ROWS = $$(".rules-based__row");
    public static final SelenideElement OPTIMIZATION_RULES_AMOUNT_INPUT = $(".rules-based__input_amount");
    public static final SelenideElement OPTIMIZATION_RULES_INDICATOR_INPUT = $(".rules-based__input_indicator-value");
    public static final SelenideElement OPTIMIZATION_RULES_SAVE_ICON = $("button.rules-based__save");
    public static final SelenideElement OPTIMIZATION_RULES_DELETE_ICON = $(".rules-based__delete");
    public static final SelenideElement OPTIMIZATION_RULES_ADD_RULE_SET_POPUP = $("div[class=popup]");
    public static final SelenideElement OPTIMIZATION_RULES_CONVERSION_COST_INPUT = $("input.target-conversion-cost");
    public static final SelenideElement OPTIMIZATION_RULES_ADD_RULE_PERIOD_SELECT = $("#cusel-scroll-cuSel-1");
    public static final SelenideElement OPTIMIZATION_RULES_ADD_RULE_SET_POPUP_OK_BUTTON = $("#rbo-preset-confirm-ok");
    public static final String ON_OFF_SWITCHER = "[data-widget-uid='%s'] .switch div";
    public static final String ON_OFF_LOCK_ICON = "[data-widget-uid='%s'] .widget-wrap span";
    public static final SelenideElement OPTIMIZATION_RULES_CONFIRM_POPUP = $("div.popup_confirm");
    public static final SelenideElement OPTIMIZATION_RULES_CONFIRM_POPUP_OK_BUTTON = $("#confirm-ok");
    public static final SelenideElement OPTIMIZATION_RULES_COPY_POPUP = $("#rules-copy-popup");
    public static final SelenideElement OPTIMIZATION_RULES_COPY_MULTISELECT = $("#accepting-campaigns");
    public static final SelenideElement OPTIMIZATION_RULES_COPY_CONFIRM_POPUP = $("div[class=popup]");
    public static final SelenideElement OPTIMIZATION_RULES_COPY_POPUP_OK_BUTTON = $("#confirm-ok");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_BUTTON = $(".history-log-btn");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_POPUP = $(".history-logs-popup");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_POPUP_CLOSE_BUTTON = $(".history-logs-popup .popup_close");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_UID_INPUT = $("#historyLogsSearch");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_ACTION_SELECT = $("#cusel-scroll-historyLogsFilterActions");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_ACTION_INPUT = $("#cuselFrame-historyLogsFilterActions .cuselText");
    public static final SelenideElement OPTIMIZATION_RULES_HISTORY_LOG_APPLY_BUTTON = $("#historyLogsApplyBtn");
    public static final ElementsCollection OPTIMIZATION_RULES_HISTORY_LOG_TABLE_ROW = $$("tbody tr[class='history-logs-popup-table-tr']");
    public static final ElementsCollection OPTIMIZATION_RULES_HISTORY_LOG_UID_FIELD = $$("td.history-logs-popup-table-td-widget");
    public static final ElementsCollection OPTIMIZATION_RULES_HISTORY_LOG_ACTION_FIELD = $$("span.history-logs-popup-table-action");
}
