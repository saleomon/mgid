package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ConversionStatsLocators {
    //filters
    public static SelenideElement STAGES_RADIO = $("[class='radio'] [type='radio']");
    public static SelenideElement TYPE_OF_WIDGET_CONVERSIONS_RADIO = $("[name='service_gblocks_filter_radio']");
    public static SelenideElement TYPE_OF_TEASER_CONVERSIONS_RADIO = $("[name='service_teaser_filter_radio']");
    public static SelenideElement FIRST_TABLE_FILTER_LOCATOR = $("[class='tableFloatingHeaderOriginal'] [id='cuselFrame-cuSel-1']");
    public static SelenideElement SECOND_TABLE_FILTER_LOCATOR = $("[class='tableFloatingHeaderOriginal'] [id='cuselFrame-cuSel-2']");
    public static SelenideElement FIRST_TABLE_FILTER_SELECT = $("[id='cusel-scroll-cuSel-1']");
    public static SelenideElement SECOND_TABLE_FILTER_SELECT = $("[id='cusel-scroll-cuSel-2']");

    //table
    public static final ElementsCollection TABLE_DATA_PRESENCE = $$("tbody [class='sort-col']");
    public static final ElementsCollection DATAS_FILTER_1 = $$("[class*='table convStat '] tbody tr td [class*='sensors_selector_first_row']");
    public static final ElementsCollection DATAS_FILTER_2 = $$("[class*='table convStat '] tbody tr td:nth-child(2)");
    public static final ElementsCollection SPENT_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(4)");
    public static final ElementsCollection CLICK_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(3)");
    public static final ElementsCollection REVENUE_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(8)");
    public static final ElementsCollection CONVERSION_COUNT_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(5)");
    public static final ElementsCollection CONVERSION_COST_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(6)");
    public static final ElementsCollection CONVERSION_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(7)");
    public static final ElementsCollection ROI_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(9)");
    public static final ElementsCollection EPC_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(10)");
    public static final ElementsCollection PROFIT_DATAS = $$("[class*='table convStat '] tbody tr td:nth-child(11)");
    public static final ElementsCollection FILTER_2_OPTIONS = $$("[class='tableFloatingHeaderOriginal'] [id='cusel-scroll-cuSel-2'] span");

    //summary block
    public static final ElementsCollection SUMMARY_DATA_PRESENCE = $$("[class='val_count']");
}
