package pages.dash.advertiser.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AudiencesListLocators {

    public static final SelenideElement AUDIENCE_ADD_BUTTON = $("a[href*='audience-add']");
    public static final SelenideElement POP_UP_CLOSE = $(".popup_close");

}