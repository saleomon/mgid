package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class SelectiveBiddingNewLocators {

    //prividers
    public static final SelenideElement EXPAND_BUTTON = $("[class*='expand-button']");
    public static final ElementsCollection EXPAND_BUTTONS = $$("[class*='expand-button']");
    public static final SelenideElement STATE_EXPAND_BUTTON_SHRINK = $x("//*[text()='keyboard_arrow_right']");
    public static final ElementsCollection SOURCES_SECTIONS = $$("[class*='ng-trigger-bodyExpansion expanded']");
    public static final ElementsCollection PROVIDERS_CHECKBOXES = $$("mat-header-cell mat-checkbox[id*='mat-checkbox']");
    public static final SelenideElement BLOCK_BUTTON = $x("//*[text()='lock']");
    public static final SelenideElement PROVIDERS_CHECKBOX = $("mat-header-cell mat-checkbox[id*='mat-checkbox']");
    public static final SelenideElement STATUS_PROVIDER_BUTTON = $("header [class='mat-slide-toggle-content']");
    public static final ElementsCollection ACTIVE_ELEMENTS= $$x("//*[@class='mat-slide-toggle-content' and contains(.,'On')]");
    public static final SelenideElement EDIT_COEFFICIENT_PROVIDER_BUTTON = $x("//*[text()='edit']");
    public static final SelenideElement COEFFICIENT_PROVIDER_INPUT = $("header [id*='mat-input']");
    public static final SelenideElement COEFFICIENT_PROVIDER_SUBMIT_BUTTON = $x("//*[text()='check']");

    //sources
    public static final ElementsCollection SELECTED_SOURCES = $$("[class*='mat-checkbox-checked'],[class*='mat-checkbox-indeterminate']");
    public static final ElementsCollection SOURCES_COEFFICIENTS= $$("section [class*='column-qualityFactor-cell-content']");
    public static final ElementsCollection NOT_CHECKED_ELEMENTS_CHECKBOXES = $$("section [class*='mat-checkbox mass-select']:not([class*='checked'])");
    public static final SelenideElement BLOCKED_SOURCE = $x("//section//*[@class='mat-slide-toggle-content' and contains(.,'Off')]");
    public static final SelenideElement ACTIVE_SOURCE = $x("//section//*[@class='mat-slide-toggle-content' and contains(.,'On')]");
    public static final ElementsCollection ACTIVE_SOURCES = $$x("//section//*[@class='mat-slide-toggle-content' and contains(.,'On')]");
    public static final SelenideElement EDIT_COEFFICIENT_SOURCE_BUTTON = $x("//section//*[text()='edit']");
    public static final SelenideElement COEFFICIENT_SOURCE_INPUT = $("section [id*='mat-input']");
    public static final SelenideElement COEFFICIENT_SOURCE_SUBMIT_BUTTON = $x("//section//*[text()='check']");

    //filters
    public static final SelenideElement FILTER_BUTTON = $("[class*='selective-filters-submit']");
    public static final SelenideElement FILTER_STATUS = $("[class*='selective-filters-status']");
    public static final SelenideElement FILTER_COEFFICIENT = $("[class*='selective-filters-quality-factor']");

    //data's
    public static final ElementsCollection COEFFICIENT_DATA_SOURCES = $$("section [class*='column-qualityFactor-cell-content']");

    //others
    public static final SelenideElement PROGRESS_BAR = $("mat-progress-bar[class*='ng-star-inserted']");
}
