package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CampaignListLocators {

    //Self register popup
    public static final SelenideElement COUNTRY_SELECT = $("#cuselFrame-billingCountry");
    public static final SelenideElement SAVE_AND_FINISH_BUTTON = $("#submit");

    //self register popup
    public static final SelenideElement COUNTRY_OF_INCORPORATION_SELECT = $("#cuselFrame-country");
    public static final SelenideElement CURRENCY_OF_PROFILE_SELECT = $("#cuselFrame-currencyId");
    public static final SelenideElement BUSINESS_TYPE_SELECT = $("#cuselFrame-businessType");
    public static final SelenideElement NAME_INPUT = $("#name");
    public static final SelenideElement CONTACT_PERSON_NAME_INPUT = $("#contactPersonName");
    public static final SelenideElement CONTACT_PERSON_EMAIL_INPUT = $("#contactPersonEmail");
    public static final SelenideElement COMPANY_AUTHORIZED_PERSON_INPUT = $("#companyPersonRole");

    public static final SelenideElement USERS_LOGIN_FIELD = $(".user p");

    //Status
    public static final SelenideElement CAMPAIGN_STATUS_BLOCK = $("div.wrap_statustext");
    public static final String CAMPAIGN_STATUS_FIELD = ".status";
    public static final String CAMPAIGN_STATUS_LIMIT_ICON = ".n4.hint";

    //Table
    public static final ElementsCollection CAMPAIGN_STATISTICS_TABLE_HEADERS = $$(".tableFloatingHeaderOriginal .sort a");
    public static final ElementsCollection DAILY_CAMPAIGN_STATISTICS_TABLE_HEADERS = $$(".tableFloatingHeaderOriginal th");
    public static final String CAMPAIGN_STATISTICS_TABLE_VALUES = ".//tr[@data-id='%s']/td[%s]";
    public static final String DAILY_CAMPAIGN_STATISTICS_TABLE_VALUES = ".//td[@class='aright'][%s]";

    //Adskeeper
    public static final SelenideElement ADVERTISER_BUTTON = $("#cooperationType-goodhits");
    public static final SelenideElement PUBLISHER_BUTTON = $("#cooperationType-wages");

    //payment block
    public static final SelenideElement ADD_FUNDS_BUTTON = $(".fillup");
    public static final SelenideElement PAYMENT_AMOUNT_INPUT = $("#paymentAmount");
    public static final SelenideElement PAYMENT_SUM_FIELD = $(".name_payment");
    public static final SelenideElement PAYMENT_ERROR_MESSAGE = $(".comment_pay");
    public static final SelenideElement CHECKOUT_BUTTON = $("#checkOut");
    public static final ElementsCollection PAYMENT_SYSTEMS_RADIO_INPUTS = $$("#payment-type input[type=radio]");
    public static final SelenideElement IDENFY_BLOCK = $("#idenfyBlock");
    public static final SelenideElement IDENFY_INFORMATION_POPUP = $(".popup");

    public static final SelenideElement IDENFY_VERIFICATION_TAB_TEXT = $(".profile.tab8");
    public static final SelenideElement IDENFY_INFORMATION_POPUP_SUBMIT_BUTTON = $("[id='idenfy-confirmation-popup'] [class*='button green']");
    public static final SelenideElement IDENFY_INFORMATION_POPUP_CLOSE_BUTTON = $(".popup_close");

    //Filter
    public static final SelenideElement FILTER_BY_DROPDOWN = $("#cuselFrame-status");
    public static final SelenideElement FILTER_BY_APPLY_BUTTON = $(".button.green.apply");

    //Icons
    public static final SelenideElement TEASER_ICON = $("span.icon-teaser");
    public static final SelenideElement STATISTICS_LINK_ICON = $("a.icon-statistics");
    public static final SelenideElement SELECTIVE_BIDDING_LINK_ICON = $("a.icon-selective-bidding");
    public static final SelenideElement COPY_CAMPAIGN_LINK_ICON = $("a.copy-campaign");
    public static final String COPY_CAMPAIGN_ICON = "a.copy-campaign[data-id='%s']";
    public static final SelenideElement RESTORE_CAMPAIGN_LINK_ICON = $("a.restore-campaign");

    //Edit campaign
    public static final SelenideElement MEDIA_SOURCE_SELECT = $("#mediaSource");

    //Copy popup
    public static final SelenideElement COPY_BLACK_LIST_CHECKBOX = $("#copyBlackList");
    public static final SelenideElement COPY_BLACK_LIST_OK_BUTTON = $("#confirm-ok");

    //complient popup
    public static final SelenideElement COMPLIANT_POPUP = $("[class=popup]");

    //notification
    public static final SelenideElement NOTIFICATION = $("#mgid-notifications div.text");

    public static final SelenideElement START_VERIFICATION_BUTTON = $("#idenfyStartButton");
}
