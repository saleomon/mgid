package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class AudiencesCreateLocators {

    public static final SelenideElement NAME_INPUT = $("#name");
    public static final SelenideElement DESCRIPTION_INPUT = $("#description");
    public static final SelenideElement TARGET_TYPE_SELECT = $("#cusel-scroll-targetType");
    public static final SelenideElement EXPIRE_SELECT = $("#cusel-scroll-expire");
    public static final SelenideElement ADD_LINK_BUTTON = $("#add-link");
    public static final SelenideElement ADD_CAMPAIGNS_CLICKERS_BUTTON = $("[id='add-target-campaign-ad_clickers']");
    public static final SelenideElement ADD_CAMPAIGNS_VIEWER_BUTTON = $("[id='add-target-campaign-ad_viewers']");
    public static final SelenideElement FULFILMENT_OF_ALL_CONDITIONS_TUMBLER = $(".switch.audience div");
    public static final String URL_TYPE_STRING_SELECT = "[id*='container-urls'] [id*='cusel-scroll-url']";
    public static final String CLICKERS_CAMPAIGNS_SELECT = "[id*='container-client-campaigns-ad_clickers'] [class='cusel-scroll-pane']";
    public static final String VIEWERS_CAMPAIGNS_SELECT = "[id*='container-client-campaigns-ad_viewers'] [class='cusel-scroll-pane']";
    public static final ElementsCollection DELETE_CLICKERS_FIELDS_BUTTON = $$("[id*='clickers'] [class*='delete_object']");
    public static final ElementsCollection DELETE_URL_FIELDS_BUTTON = $$("[id='container-urls'] [class*='delete_object']");
    public static final ElementsCollection DELETE_TARGET_FIELDS_BUTTON = $$("[id='container-targets'] [class*='delete_object']");

    public static final String URL_VALUE_STRING_INPUT = "[name*='[urlValue]']";
    public static final SelenideElement TARGET_EVENT_INPUT = $("#identificator");
    public static final SelenideElement JS_COPY_BUTTON = $("#js-copy");

    public static final SelenideElement MINIMUM_PAGES_TO_VISIT_INPUT = $("#value");
    public static final SelenideElement DOMAIN_FOR_TRACKING_INPUT = $("#domain");
    public static final SelenideElement TARGET_ADD_BUTTON = $("#add-target");
    public static final SelenideElement SAVE_BUTTON = $("#save-audience");
}
