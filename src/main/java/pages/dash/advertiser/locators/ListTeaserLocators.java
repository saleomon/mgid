package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ListTeaserLocators {
    public static final SelenideElement TEASER_ID_LOCATOR = $x(".//*[@class='teaser' and @data-id]");
    public static final SelenideElement TEASER_DOMAIN = $("[id*=url_]");
    public static final SelenideElement FOR_TEASERS_MASS_ACTIONS_CHECKBOX = $(".ghits-checkbox");
    public static final SelenideElement BLOCK_MASS_ACTION_ICON = $("[data-action-type=block]");
    public static final SelenideElement UNBLOCK_MASS_ACTION_ICON = $("[data-action-type=unblock]");
    public static final SelenideElement TEASER_BLOCK_ICON = $(".icon-pause");
    public static final SelenideElement TEASER_UNBLOCK_ICON = $("[class*=icon-play]");

    public static final SelenideElement PREVIEW_IN_INFORMER_LINK = $(".preview_in_informer");
    public static final String TITLE_LIST = ".tit";
    public static final SelenideElement CALL_TO_ACTION = $(".call-to-action");
    public static final SelenideElement TEASER_CPC = $(".cpc");
    public static final SelenideElement TEASER_TYPE = $("span.ad_type");
    public static final SelenideElement FILTER_BUTTON = $(".button.green.apply");
    public static final ElementsCollection STATUS_TEASERS = $$(".teaser-status-text");
    public static final SelenideElement STATUS_TEASER = $(".teaser-status-text");
    public static final SelenideElement PUSH_CAMPAIGN_IMAGE = $(".push-icon");
    public static final SelenideElement TEASER_BLOCKS = $("[class='teaser']");
    public static final SelenideElement EDIT_TEASER_POPUP = $("[class*='popup'] [id='ghits_new_add']");
    public static final SelenideElement VIDEO_LINK_LOCATOR = $("[class='teaser'] video source");

    public static final SelenideElement IMPORT_TEASER_BUTTON = $("a.import_teasers_goods");
    public static final SelenideElement IMPORT_TEASER_LOAD_BUTTON = $(".popup #teasers-import-file-name");
    public static final SelenideElement IMPORT_TEASER_PRICE_INPUT = $("#poc_import_value-visible");
    public static final SelenideElement IMPORT_TEASER_SAVE_FORM = $(".popup #teasers-import-file-save");
    public static final SelenideElement CONFIRM_OK = $("[id='confirm-ok']");
    public static final SelenideElement SAVE_FORM = $("[id='ghits_new_add'] [class*='save']");
    public static final SelenideElement IMPORT_TEASER_LOAD_INDICATOR = $(".popup .progress-indicator-img");
    public static final SelenideElement IMPORT_TEASER_MAX_PRICE_ERROR_TEXT = $(".popup .error-max-price-of-click-import");

    public static final SelenideElement NOTIFICATION_TEXT = $(".notification .text");

    public static final ElementsCollection TEASERS_LIST_STATISTICS_TABLE_HEADERS = $$(".tableFloatingHeaderOriginal th");
    public static final String TEASER_LIST_STATISTICS_TABLE_VALUES = ".//tr[@class='teaser']/td[%s]";
    public static final ElementsCollection TEASERS_DAILY_STATISTICS_TABLE_HEADERS = $$(".tableFloatingHeaderOriginal th");
    public static final String TEASER_DAILY_STATISTICS_TABLE_VALUES = ".//tr[2]/td[%s]";
    public static final SelenideElement LINK_FOR_PREVIEW_TEASER = $("[class*='preview_in_informer']");
    public static final ElementsCollection IMAGE_BLOCKS_IN_PREVIEW = $$("[class='teaser-manual-preview-img']");
    public static final ElementsCollection LINKS_OF_REDIRECTIONS = $$("[class*=teaser-manual-preview] a");

    public static final SelenideElement CSR_MEDIUM_ICON = $(".ad_safety_ranking_icon_medium");
    public static final SelenideElement CSR_HIGH_ICON = $(".ad_safety_ranking_icon_high");

    //focal point
    public static final SelenideElement MANUAL_FOCAL_POINT_BUTTON = $("[id='manual-control']");
    public static final SelenideElement FOCAL_POINT_ICON = $("[id='crosshair']");
    public static final SelenideElement MANUAL_FOCAL_CONFIRM_BUTTON = $("[id='manual-control-confirm']");
    public static final SelenideElement MANUAL_FOCAL_CANCEL_BUTTON = $("[id='manual-control-cancel']");
    public static final SelenideElement NOTIFICATION_BLOCK = $("[class='notification']");


}
