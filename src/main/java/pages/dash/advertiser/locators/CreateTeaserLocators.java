package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CreateTeaserLocators {
    public static final SelenideElement CATEGORY_SELECT = $("#category_select_dropdown");
    public static final SelenideElement IMAGE_LOCATOR = $("#image");
    public static final SelenideElement PRICE_FOR_ALL_REGIONS_CHECKBOX = $("#one_price_for_all_locations");
    public static final SelenideElement TREES_BUTTON = $x(".//img[contains(@src, 'tree-closed.png')]");
    public static final SelenideElement CLOSE_POPUP_BUTTON = $x(".//div[@class='popup']/a[@class='popup_close']");
    public static final SelenideElement GO_TO_LIST_INTERFACE_BUTTON = $("#save-go-to");
    public static final ElementsCollection ERROR_MESSAGE_CONTAINER = $$x(".//*[contains(@class, 'incorrect')]");
    public static final ElementsCollection ERROR_MESSAGES = $$("div.notification .text");
    public static final SelenideElement TEXT_MESSAGE = $(".text");
    public static final String FIELDS_OF_PRICES = ".geo-prices-tree .inp2.number_initialized";
    public static final String REGION_NAMES = ".node-label span";
    public static final SelenideElement CURRENCY_SELECTOR = $("#cusel-scroll-currencyId");
    public static final SelenideElement SHOW_PRICE_CHECKBOX = $("#showPrice");
    public static final SelenideElement PRICE_INPUT = $("#price");
    public static final SelenideElement PRICE_OLD_INPUT = $("#price_old");
    public static final SelenideElement DISCOUNT_INPUT = $("#discount");
    public static final SelenideElement URL_INPUT = $("#url");
    public static final SelenideElement TITLE_INPUT = $("#title");
    public static final SelenideElement TEASER_TITLE_LIST_INTERFACE = $("[class*='black_bold_title']");
    public static final SelenideElement DESCRIPTION_INPUT = $("#advert_text");
    public static final SelenideElement CPC_INPUT = $("#price_of_click-visible");
    public static final ElementsCollection GETTY_IMAGES = $$("[class*='stock-gallery-elements-item_']");
    public static final SelenideElement FORMAT_VIEW_BLOCK = $("[class*='stock-gallery-type-options']");
    public static final SelenideElement UPLOAD_IMAGE_TAB = $("[class='ghits-tabs'] [data-tab='upload']");
    public static final SelenideElement SORT_BY_FOR_VIEW_BLOCK = $("[class*='stock-gallery-sortBy-options']");
    public static final SelenideElement ACTIVE_PAGE_GALLERY = $("[class='stock-gallery-pagination-item active']");
    public static final SelenideElement NEXT_PAGE_GALLERY_BUTTON = $("[class='stock-gallery-pagination-item-btn next']");
    public static final SelenideElement VIDEO_BLOCK_TEASER_LIST = $("[class*='preview'] video");

    public static final SelenideElement CPC_LABEL = $("#price_of_click");
    public static final SelenideElement TEASER_REACH_NAMES_LABEL = $(".reach-names");
    public static final SelenideElement TEASER_REACH_METER_LABEL = $("div.reach-meter");
    public static final SelenideElement VERY_LOW_CPC_ICON = $("[data-reach=very-low]");
    public static final SelenideElement LOW_CPC_ICON = $("[data-reach=low]");
    public static final SelenideElement HIGH_CPC_ICON = $("[data-reach=high]");
    public static final SelenideElement VERY_HIGH_CPC_ICON = $("[data-reach=very-high]");
    public static final SelenideElement VERY_LOW_AND_REACH_DATA_LABEL = $("div.very-low.active");
    public static final SelenideElement URL_ERROR_LABEL = $(".urlError");
    public static final SelenideElement STOCK_GETTY_IMAGES_TAB = $("[class='ghits-tabs'] [data-tab='stock']");
    public static final ElementsCollection GETTY_IMAGES_PHRASES_DELETE_BUTTON = $$("[class='stock-gallery-search-item-remove-btn']");
    public static final SelenideElement GETTY_IMAGE_PHRASES_DELETE_BUTTON = $("[class='stock-gallery-search-item-remove-btn']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_PHRASE = $("[class='stock-gallery-search-item-text']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_INPUT = $("[class='stock-gallery-image-search']");
    public static final SelenideElement GALLERY_NOTIFICATION_BLOCK = $("[class='stock-gallery-notification']");
    public static final SelenideElement IMAGE_FRAME = $("[class='mcimg'] img[src]");
    public static final SelenideElement MANUAL_FOCAL_POINT_BUTTON = $("[id='manual-control']");
    public static final SelenideElement MANUAL_FOCAL_CONFIRM_BUTTON = $("[id='manual-control-confirm']");
    public static final SelenideElement MANUAL_FOCAL_CANCEL_BUTTON = $("[id='manual-control-cancel']");
    public static final SelenideElement MANUAL_FOCAL_DEFAULT_BUTTON = $("[id='manual-control-default']");
    public static final SelenideElement MANUAL_FOCAL_EDIT_BUTTON = $("[id='manual-control-edit']");
    public static final SelenideElement FOCAL_POINT_ICON = $("[id='crosshair']");

    //creative safety ranking
    public static final SelenideElement CSR_TITLE_FIELD = $(".increase-traffic__title");
    public static final SelenideElement CSR_TEXT_FIELD = $(".increase-traffic__text");
    public static final SelenideElement CSR_REACH_METER = $("#increase-traffic-circle");
    public static final SelenideElement CSR_REACH_METER_VALUE_FIELD = $(".increase-traffic-circle__percent span");
    public static final SelenideElement CSR_REACH_METER_NOTE_FIELD = $(".increase-traffic__note");
    public static final String CSR_HIGH_SAFETY_LINK = "//a[contains(text(), '%s')]";
    public static final SelenideElement CSR_MEDIUM_SAFETY_LINK = $x("//a[contains(text(), 'Medium Safety')]");

    public static final SelenideElement GET_AUTO_TITLE_BUTTON = $("[id='getAutoTitlesBtn']");
    public static final SelenideElement AUTO_TITLE_TOOLTIP_LABEL = $("[class*='suggestionSelect'] [class*='tooltip']");
}
