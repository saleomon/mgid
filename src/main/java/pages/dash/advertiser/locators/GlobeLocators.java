package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class GlobeLocators {

    public final static SelenideElement EXPAND_ALL_TREE = $(".tree-expander");
    public final static SelenideElement GEO_PRICE_INPUT = $("[class*='number_initialized']");
    public final static SelenideElement SAVE_MASS_POC_BUTTON = $("[id='setMassPOC']");
    public final static SelenideElement SAVE_HIGHER_PRICE_CONFIRMATION = $("[id='continueSetMassPOC']");
    public final static ElementsCollection REGIONS_IN_GEO_CHECKBOXES = $$("[class*='leaf'] input[type='checkbox']");
}
