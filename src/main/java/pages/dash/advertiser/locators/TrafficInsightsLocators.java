package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class TrafficInsightsLocators {
    public static final SelenideElement PAGE_TITLE = $("h2.audience-title");
    public static final SelenideElement PLATFORM_ALL_BUTTON = $("#mat-button-toggle-1-button");
    public static final SelenideElement PLATFORM_DESKTOP_BUTTON = $("#mat-button-toggle-2-button");
    public static final SelenideElement PLATFORM_MOBILE_BUTTON = $("#mat-button-toggle-3-button");
    public static final SelenideElement TRAFFIC_TYPE_NATIVE_BUTTON = $("#mat-button-toggle-5-button");
    public static final SelenideElement TRAFFIC_TYPE_PUSH_BUTTON = $("#mat-button-toggle-6-button");
    public static final SelenideElement FIND_COUNTRY_INPUT = $("#mat-input-0");
    public static final SelenideElement FIND_COUNTRY_TEXT = $("span.mat-option-text");
    public static final SelenideElement PAGINATION_PREVIOUS_BUTTON = $("button.mat-paginator-navigation-previous");
    public static final SelenideElement PAGINATION_NEXT_BUTTON = $("button.mat-paginator-navigation-next");
    public static final SelenideElement CAMPAIGN_TYPE_SELECT = $("#mat-select-0");
    public static final SelenideElement CAMPAIGN_TYPE_SELECT_VALUE_1 = $("#mat-option-0");
    public static final SelenideElement CAMPAIGN_TYPE_SELECT_VALUE_2 = $("#mat-option-1");

    public static final ElementsCollection TRAFFIC_INSIGHTS_ROWS = $$("table.traffic-insights-table tr.mat-row");
}