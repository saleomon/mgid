package pages.dash.advertiser.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class EditTeaserLocators {

    public static final SelenideElement URL_INPUT = $(".popup #url");
    public static final SelenideElement TITLE_INPUT = $(".popup #title");
    public static final SelenideElement IMAGE_INPUT = $("#image_link");
    public static final SelenideElement DESCRIPTION_INPUT = $(".popup #advert_text");
    public static final SelenideElement IMAGE_LOCATOR = $(".popup #image");
    public static final SelenideElement CATEGORY_SELECT = $("#categore_sel_dropdown");
    public static final SelenideElement IMAGE_FILTERS_SELECTED = $(".popup [id='cuselFrame-imageEffects'] [class=cuselText]");
    public static final SelenideElement CURRENCY_SELECT = $(".popup #cusel-scroll-currencyId");
    public static final SelenideElement PRICE_INPUT = $(".popup #price");
    public static final SelenideElement PRICE_OLD_INPUT = $(".popup #price_old");
    public static final SelenideElement DISCOUNT_INPUT = $(".popup #discount");
    public static final SelenideElement SHOW_PRICE_CHECKBOX = $(".popup #showPrice");
    public static final SelenideElement SAVE_SETTINGS_BUTTON = $(".popup .button.save");
    public static final SelenideElement IMAGE_BLOCK_VIEW = $("[class*=popup] [id='imagePreviewHolder']");
}
