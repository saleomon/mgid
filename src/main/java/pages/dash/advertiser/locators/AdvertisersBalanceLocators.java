package pages.dash.advertiser.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AdvertisersBalanceLocators {
    public static final SelenideElement GENERATE_INVOICE_BUTTON = $(".generate_invoice");
}
