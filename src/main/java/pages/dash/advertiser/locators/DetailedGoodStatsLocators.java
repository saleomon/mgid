package pages.dash.advertiser.locators;

import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.$$;

public class DetailedGoodStatsLocators {
    public static final ElementsCollection CLICK_DATAS = $$("[class*='table'] tbody tr td:nth-child(2)");
    public static final ElementsCollection SPENT_DATAS = $$("[class*='table'] tbody tr td:nth-child(3)");
    public static final ElementsCollection PERCENT_DATAS = $$("[class*='table'] tbody tr td:nth-child(4)");
    public static final ElementsCollection SECTION_DATAS = $$("[class*='table'] tbody tr td:nth-child(1)");

}
