package pages.dash.advertiser.logic;

import com.codeborne.selenide.SelenideElement;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.ConversionStatsHelper;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static pages.dash.advertiser.locators.ConversionStatsLocators.FILTER_2_OPTIONS;

public class ConversionStats {

    ConversionStatsHelper conversionStatsHelper;
    String filterName1;
    String filterName2;
    String conversions;
    String conversionsPrice;
    Double profitData;
    Integer clicksTable;
    Integer conversionCountTable;
    Double conversionCostTable;
    Double conversionsTable;
    Double spentTable;
    Double revenueTable;
    Double epcTable;
    Double roiTable;


    public ConversionStats(HelpersInit helpersInit) {
        conversionStatsHelper = new ConversionStatsHelper(helpersInit);
    }

    public enum ConversionStages {
        INTEREST("interest"),
        BUY("buy"),
        DECISION("decision");

        private final String conversion;

        ConversionStages(String conversion) {
            this.conversion = conversion;
        }

        public ConversionStages getConversionStage(String stage) {
            for (ConversionStages t : ConversionStages.values()) {
                if (t.getConversionVal().equals(stage)) {
                    return t;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getConversionVal() {
            return conversion;
        }

        public static ConversionStages getRandomConversionStage() {
            Random random = new Random();
            int randomIndex = random.nextInt(ConversionStages.values().length);
            int loopCounter = 0;

            for (ConversionStages t : ConversionStages.values()) {
                if (loopCounter == randomIndex) {
                    return t;
                }
                loopCounter++;
            }
            throw new RuntimeException("unknown type");
        }
    }

    public String getFilterName1() {
        return filterName1;
    }

    public String getFilterName2() {
        return filterName2;
    }

    /**
     * метод выбирает тип конверсии отображаемой статистики из радио листа
     */
    public void chooseStageRadio(ConversionStats.ConversionStages stageConversion) {
        conversionStatsHelper.chooseInterestStageRadio(stageConversion.getConversionVal());
    }

    /**
     * метод вибирает параметр первого фильтра
     */
    public void selectFirstFilterBy(String option) {
        filterName1 = switch (option) {
            case "ticker" -> "Widget UID";
            case "teaser" -> "Teaser UID";
            default -> throw new IllegalStateException("Unexpected value: " + option);
        };
        conversionStatsHelper.selectFirstFilterBy(option);
    }

    /**
     * метод вибирает параметр второго фильтра
     */
    public void selectSecondFilterBy(String stage) {
        filterName2 = switch (stage) {
            case "country" -> "Country";
            case "device_os" -> "OS";
            case "uid_subuid" -> "Widget uid/subuid";
            case "connection_type" -> "Connection type";
            case "browser" -> "Browser";
            case "region" -> "Region";
            case "adv_traffic_type" -> "Traffic type";
            case "phones_price_range" -> "Phone price ranges";
            default -> throw new IllegalStateException("Unexpected value: " + stage);
        };
        conversionStatsHelper.selectSecondFilterBy(stage);
    }

    /**
     * метод вибирает опцию отображения статистики по состоянию конверсии
     */
    public void chooseTypeOfCondition(String typeConversion) {
        conversionStatsHelper.chooseTypeOfCondition(typeConversion);
    }

    /**
     * метод проверяет статистику по конверсиям в таблице
     */
    public boolean checkStatisticsWidgetWithConversionTable(List<List<String>>  data) {
        return conversionStatsHelper.checkTableData(data);
    }

    /**
     * метод проверяет статистику общего блока интерфейса статистики
     */
    public boolean checkStatisticsWidgetSummaryData(List<String> data) {
        return conversionStatsHelper.checkSummaryData(data);
    }

    /**
     * метод проверяет статистику которая передается в график интерфейса
     */
    public boolean checkStatisticsWidgetWithConversionChart(List<String> data) {
        return conversionStatsHelper.checkChartData(data, conversions, conversionsPrice);
    }

    /**
     * метод делает выборку конверсий из данных графика
     */
    public void getConversionChartData() {
        conversions = conversionStatsHelper.getValueFromChart("stageSum");
    }

    /**
     * метод делает выборку конверсий из данных графика
     */
    public void getConversionPriceChartData() {
        conversionsPrice = conversionStatsHelper.getValueFromChart("spent");
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsClicksFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkClicksTable(data.get("clicks"), clicksTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsConversionFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkConversionsTable(data.get("conversion"), conversionsTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsSpentFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkSpentTable(data.get("spent"), spentTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsRevenueFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkRevenueTable(data.get("revenue"), revenueTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsConversionCountFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkConversionCountTable(data.get("conversionCount"), conversionCountTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsConversionCostFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkConversionCostTable(data.get("conversionCost"), conversionCostTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsRoiFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkRoiTable(data.get("roi"), roiTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkProfitTable(Map<String, Object> data) {
        return conversionStatsHelper.checkProfitTable(data.get("profit"), profitData);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkEqualsEpcFromTable(Map<String, Object> data) {
        return conversionStatsHelper.checkEpcTable(data.get("epc"), epcTable);
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkFilter1(Map<String, Object> data) {
        return conversionStatsHelper.checkFilter1(data.get("filter1"));
    }

    /**
     * метод проверяет статистику по кликам в таблице
     */
    public boolean checkFilter2(Map<String, Object> data) {
        return conversionStatsHelper.checkFilter2(data.get("filter2"));
    }

    public boolean checkVisibilityOfFilterBySubid() {
        for(SelenideElement s : FILTER_2_OPTIONS){
            if(Objects.requireNonNull(s.attr("val")).equals("uid_subuid")){
                return true;
            }
        }
       return false;
    }

    public void getSumColumnsTableData() {
        clicksTable = conversionStatsHelper.getSumClicksFromTableColumn();
        conversionCountTable = conversionStatsHelper.getSumConversionCountFromTableColumn();
        conversionCostTable = conversionStatsHelper.getSumConversionCostFromTableColumn();
        conversionsTable = conversionStatsHelper.getSumConversionsFromTableColumn();
        spentTable = conversionStatsHelper.getSumSpentFromTableColumn();
        revenueTable = conversionStatsHelper.getSumRevenueFromTableColumn();
        epcTable = conversionStatsHelper.getSumEpcFromTableColumn();
        roiTable = conversionStatsHelper.getSumRoiFromTableColumn();
        profitData = conversionStatsHelper.getSumProfitFromTableColumn();
    }

    public boolean checkSecondColumnOfConversionStatsTable(List<List<String>>  dataFromDB, List<String> dataText) {
        return conversionStatsHelper.checkSecondColumnOfConversionStatsTable(dataFromDB, dataText);
    }
}
