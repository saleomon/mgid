package pages.dash.advertiser.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.SelectiveBiddingHelper;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.advertiser.locators.SelectiveBiddingLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.OthersData.LINK_TO_RESOURCES_FILES;

public class SelectiveBidding {
    SelectiveBiddingHelper selectiveBiddingHelper1;
    private final HelpersInit helpersInit;
    private final Logger log;

    public SelectiveBidding(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        selectiveBiddingHelper1 = new SelectiveBiddingHelper(helpersInit);
    }

    public void filterBySourceSubSource(String value) {
        clearAndSetValue(SEARCH_FIELD, value + Keys.ENTER);
        checkErrors();
        waitForAjax();
    }

    public boolean checkDisplayedSource(String value) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        return helpersInit.getBaseHelper().checkDatasetContains(SOURCE_ID.text(), value);
    }

    public boolean checkDisplayedSubSource(String value) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        return helpersInit.getBaseHelper().checkDatasetContains(SUB_SOURCE_ID.text(), value);
    }

    public void importSources(String fileName) {
        selectiveBiddingHelper1.setFileForImport(fileName);
    }

    public boolean checkCoefficientInCab(String[] massSource, String[] coefficient) {
        return selectiveBiddingHelper1.checkCoefficientInCab(massSource, coefficient);
    }

    public boolean checkCoefficientAndDateInTooltip(String oldValue, String xpathSources) {
        helpersInit.getBaseHelper().refreshCurrentPage();
        selectiveBiddingHelper1.expandSubSourceTrees();
        SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd");
        ff.setTimeZone(TimeZone.getTimeZone("GMT-10"));
        String date = ff.format(new Date());
        String value = selectiveBiddingHelper1.getToolTipValue(xpathSources);
        log.info(value + " == " + oldValue);
        log.info(value + " == " + date);
        return value.contains(oldValue) && value.contains(date);
    }

    public boolean editCoefficientInImport(String link, String values, String widget_id, String typeSource, String subSourceId1) {
        boolean flag = false;
        String oldValue, param = "";
        try {
            if (typeSource.equals("subSource")) {
                selectiveBiddingHelper1.expandSubSourceTrees();
                selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathToSource(widget_id, subSourceId1));
            } else {
                selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathAboutWidget(param, widget_id));
            }

            oldValue = selectiveBiddingHelper1.getQfValue();
            selectiveBiddingHelper1.setFileForImport(link);

            selectiveBiddingHelper1.expandSubSourceTrees();
            $("[class='identifier  aright ']").scrollTo();
            if (selectiveBiddingHelper1.getQfValue().equals(values) &&
                    (oldValue.equals(values) || checkCoefficientAndDateInTooltip(oldValue, selectiveBiddingHelper1.getGetBasePath()))) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("Catch editCoefficientInImport" + e);
        }
        return flag;
    }

    public void filterByStatus(String value) {
        selectiveBiddingHelper1.selectStatusFilter(value);
        selectiveBiddingHelper1.filterByStatus();
        checkErrors();
        waitForAjax();
    }

    public void filterByCoefficient(String value) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(COEFFICIENT_FILTER, value);
        selectiveBiddingHelper1.filterByStatus();
        checkErrors();
        waitForAjax();
    }

    public boolean checkFilterStatus(String value) {
        String classId = value.contains("Active") ? "on" : "off";
        selectiveBiddingHelper1.expandSubSourceTrees();
        ElementsCollection listAllElements = STATUS_ELEMENTS;
        List<SelenideElement> listWithStatus = new ArrayList<>();
        for(SelenideElement s : listAllElements){
            if(Objects.requireNonNull(s.attr("class")).contains(classId)) listWithStatus.add(s);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(listAllElements.size() == listWithStatus.size());
    }

    public boolean checkFilterCoefficient(String value) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        SelenideElement currElement = value.contains("changed") ? CHANGED_COEFFICIENT_ELEMENT : INITIAL_COEFFICIENT_ELEMENT;
        return helpersInit.getBaseHelper().getTextAndWriteLog(currElement.isDisplayed());
    }

    public boolean editSourceInImportWithNotValidCoefficient(String link, String widget_id, String typeSource, String subSourceId1) {
        boolean fl = false;
        String old_value;

        if (typeSource.equals("source")) {
            selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathAboutWidget("", widget_id));
        } else {
            selectiveBiddingHelper1.expandSubSourceTrees();
            selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathToSource(widget_id, subSourceId1));
        }

        old_value = selectiveBiddingHelper1.getQfValue();

        selectiveBiddingHelper1.setFileForImport(link);

        if (!typeSource.equals("source")) {
            selectiveBiddingHelper1.expandSubSourceTrees();
        }

        if (selectiveBiddingHelper1.getQfValue().equals(old_value)) {
            fl = true;
        }
        return fl;
    }

    public boolean checkMaxMinCoefficientInSubPlatform(String coefficientValue, String typeSource) {
        boolean flag = false;
        String param = "", oldValue;
        try {
            if (typeSource.equals("subSource")) {
                param = ".source-row";
                selectiveBiddingHelper1.expandSubSourceTrees();
            }
            selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathToWidget(param));

            oldValue = selectiveBiddingHelper1.getQfValue();
            selectiveBiddingHelper1.setNewQfValue(coefficientValue, true);

            isConfirmDisplayed();
            waitForAjax();

            if (selectiveBiddingHelper1.getQfValue().equals(coefficientValue) &&
                    (oldValue.equals(coefficientValue) || checkCoefficientAndDateInTooltip(oldValue, selectiveBiddingHelper1.getGetBasePath()))) {
                flag = true;
            }

        } catch (Exception e) {
            log.error("Catch " + e);
        }
        return flag;
    }

    public boolean checkNotValidCoefficientInSubPlatform(String coefficientValue, String old_value, String typeSource) {
        boolean flag = false;
        String param = "";
        try {
            if (typeSource.equals("subSource")) {
                param = ".source-row";
                selectiveBiddingHelper1.expandSubSourceTrees();
            }
            selectiveBiddingHelper1.setGetBasePath(selectiveBiddingHelper1.getBasePathToWidget(param));

            selectiveBiddingHelper1.setNewQfValue(coefficientValue, true);

            isConfirmDisplayed();
            waitForAjax();

            if (selectiveBiddingHelper1.getQfValue().equals(old_value)) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("Catch " + e);
        }

        return flag;
    }

    public boolean checkMaxMinCoefficientInMassActionInSubPlatform(String coefficientValue, String typeSource, String subSourceId2, String subSourceId1) {
        boolean flag = false;
        String oldValue_1, oldValue_2, param = "",
                basePath_1, basePath_2;
        try {
            if (typeSource.equals("source")) {
                basePath_1 = selectiveBiddingHelper1.getBasePathToWidget(param);
                basePath_2 = selectiveBiddingHelper1.getBasePathToWidget(param, 1);
            } else {
                selectiveBiddingHelper1.expandSubSourceTrees();
                basePath_1 = selectiveBiddingHelper1.getBasePathToSource(subSourceId2);
                basePath_2 = selectiveBiddingHelper1.getBasePathToSource(subSourceId1);
            }

            selectiveBiddingHelper1.clickWidgetCheckbox(basePath_1);
            selectiveBiddingHelper1.clickWidgetCheckbox(basePath_2);
            oldValue_1 = selectiveBiddingHelper1.getQfValue(basePath_1);
            oldValue_2 = selectiveBiddingHelper1.getQfValue(basePath_2);

            selectiveBiddingHelper1.setQfValueWithMassAction(coefficientValue);
            isConfirmDisplayed();

            if (!typeSource.equals("source")) {
                selectiveBiddingHelper1.expandSubSourceTrees();
            }

            if (selectiveBiddingHelper1.getQfValue(basePath_1).equals(coefficientValue) &&
                    selectiveBiddingHelper1.getQfValue(basePath_2).equals(coefficientValue) &&
                    (oldValue_1.equals(coefficientValue) || checkCoefficientAndDateInTooltip(oldValue_1, basePath_1)) &&
                    (oldValue_2.equals(coefficientValue) || checkCoefficientAndDateInTooltip(oldValue_2, basePath_2))) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("Catch " + e);
        }
        return flag;
    }

    public boolean checkNotValidCoefficientInMassActionInSubPlatform(String coefficientValue, String old_value, String typeSource, String subSourceId2, String subSourceId1) {
        boolean flag = false;
        String basePath_1, basePath_2, param = "";
        try {
            if (typeSource.equals("source")) {
                basePath_1 = selectiveBiddingHelper1.getBasePathToWidget(param);
                basePath_2 = selectiveBiddingHelper1.getBasePathToWidget(param, 1);
            } else {
                selectiveBiddingHelper1.expandSubSourceTrees();
                basePath_1 = selectiveBiddingHelper1.getBasePathToSource(subSourceId1);
                basePath_2 = selectiveBiddingHelper1.getBasePathToSource(subSourceId2);
            }

            selectiveBiddingHelper1.clickWidgetCheckbox(basePath_1);
            selectiveBiddingHelper1.clickWidgetCheckbox(basePath_2);

            selectiveBiddingHelper1.setQfValueWithMassAction(coefficientValue);
            isConfirmDisplayed();

            if (!typeSource.equals("source")) {
                selectiveBiddingHelper1.expandSubSourceTrees();
            }

            if (selectiveBiddingHelper1.getQfValue(basePath_1).equals(old_value) &&
                    selectiveBiddingHelper1.getQfValue(basePath_2).equals(old_value)) {
                flag = true;
            }
        } catch (Exception e) {
            log.error("Catch " + e);
        }

        return flag;
    }

    public boolean checkBlockUnblockSourcesInMassAction() {
        String[] mass = new String[2];
        selectiveBiddingHelper1.expandSubSourceTrees();
        for (int i = 0; i < mass.length; i++) {
            mass[i] = SUB_SOURCES.get(i).getAttribute("data-widget-uid");
        }
        return selectiveBiddingHelper1.switchSource(mass, true) && selectiveBiddingHelper1.switchSource(mass, false);
    }

    public boolean checkBlockUnblockSubSourcesInMassAction() {
        String[] mass = new String[2];
        selectiveBiddingHelper1.expandSubSourceTrees();
        String idSources = SOURCES.get(0).attr("data-sort-group");
        ElementsCollection list = $$x(".//tr[@data-sort-group-item='" + idSources + "' and not(@data-source-id='0')]//td[2]");
        for (int i = 0; i < mass.length; i++) {
            mass[i] = list.get(i).text().trim();
        }
        return selectiveBiddingHelper1.switchSubSource(mass, true) && selectiveBiddingHelper1.switchSubSource(mass, false);
    }

    public void blockUnblockSource(String state) {
        String stateElement = state.equals("block") ? "on" : "off";
        selectiveBiddingHelper1.expandSubSourceTrees();
        if ($("[class='sortable'] [class=" + stateElement + "]").isDisplayed())
            $("[class='sortable'] [class=" + stateElement + "]").click();
        checkErrors();
    }

    public void blockUnblockSubSource(String state) {
        String stateElement = state.equals("block") ? "on" : "off";
        selectiveBiddingHelper1.expandSubSourceTrees();
        $("[class='source-row'] [class=" + stateElement + "]").click();
        waitForAjax();
        checkErrors();
    }

    public void blockUnblockSourceMassAction(String state) {
        String stateElement = state.equals("block") ? "2" : "3";
        selectiveBiddingHelper1.expandSubSourceTrees();
        markUnMarkCheckbox(true, SOURCE_CHECKBOX);
        $x(String.format(BLOCK_MASS_ACTION_BUTTON, stateElement)).click();
        waitForAjax();
        checkErrors();
    }

    public boolean checkblockUnblockSource(String state) {
        String stateElement = state.equals("block") ? "off" : "on";
        return helpersInit.getBaseHelper().checkDataset(ALL_DISPLAYED_ELEMENTS.size(), $$(String.format(ALL_ACTIVE_ELEMENTS, stateElement)).size());
    }

    public int getAmountActiveSubSource() {
        selectiveBiddingHelper1.expandSubSourceTrees();
        return helpersInit.getBaseHelper().getTextAndWriteLog(ACTIVE_SUB_SOURSE.size());
    }

    public boolean isActiveSource() {
        selectiveBiddingHelper1.expandSubSourceTrees();
        return helpersInit.getBaseHelper().getTextAndWriteLog(ACTIVE_SOURSE.isDisplayed());
    }

    public void excludeSpecificSubIds(boolean state) {
        String stateElement = state ? "ok" : "cancel";
        $(String.format(POPUP_BUTTON, stateElement)).click();
        checkErrors();
        waitForAjax();
    }

    public void changeCoefficientSubSource(String coef) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        EDIT_SUBSOURCE_COEFFICIENT_BUTTON.hover().click();
        COEFFICIENT_SUBSOURCE_INPUT.doubleClick();
        sendKey(COEFFICIENT_SUBSOURCE_INPUT, coef);
        SAVE_SUBSOURCE_COEFFICIENT.click();
        waitForAjax();
        checkErrors();
        SAVE_SUBSOURCE_COEFFICIENT.shouldBe(hidden, ofSeconds(10));
    }

    public void changeCoefficientSource(String coef) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        EDIT_SOURCE_COEFFICIENT_BUTTON.hover().click();
        /*if (POPUP_COEF.isDisplayed()) */POPUP_COEF.shouldBe(visible).click();
        COEFFICIENT_SOURCE_INPUT.doubleClick();
        sendKey(COEFFICIENT_SOURCE_INPUT, coef);
        SAVE_SOURCE_COEFFICIENT.click();
        waitForAjax();
        checkErrors();
        SAVE_SOURCE_COEFFICIENT.shouldBe(hidden, ofSeconds(10));
    }

    public void changeCoefficientSourceForAll(String coef) {
        EDIT_SOURCE_COEFFICIENT_BUTTON.hover().click();
        sleep(1000);
        if (POPUP_COEF.isDisplayed()) POPUP_COEF.click();
        COEFFICIENT_SOURCE_INPUT.doubleClick();
        sendKey(COEFFICIENT_SOURCE_INPUT, coef);
        APPLY_FOR_SUBSOURCES_CHECKBOX.click();
        SAVE_SOURCE_COEFFICIENT.click();
        waitForAjax();
        checkErrors();
    }

    public void changeCoefficientSubSourceMass(String coef) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        SUBSOURCE_CHECKBOX.click();
        CHANGE_COEFFICIENT_MASS_BUTTON.click();
        COEFFICIENT_MASS_INPUT.doubleClick();
        sendKey(COEFFICIENT_MASS_INPUT, coef);
        SUBMIT_COEFFICIENT_MASS_BUTTON.click();
        waitForAjaxLoader();
        $(String.format(POPUP_BUTTON, "ok")).shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public void changeCoefficientSourceMass(String coef) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        SOURCE_CHECKBOX.click();
        CHANGE_COEFFICIENT_MASS_BUTTON.click();
        COEFFICIENT_MASS_INPUT.doubleClick();
        sendKey(COEFFICIENT_MASS_INPUT, coef);
        SUBMIT_COEFFICIENT_MASS_BUTTON.click();
        $(String.format(POPUP_BUTTON, "ok")).click();
        waitForAjax();
        checkErrors();
    }

    public boolean checkVisibilitySubSourceWithCoefficient(String coefficient) {
        selectiveBiddingHelper1.expandSubSourceTrees();
        return helpersInit.getBaseHelper().getTextAndWriteLog($x("//span[contains(.,'" + coefficient + "')]").isDisplayed());
    }

    public boolean checkFunctionChooseAllCheckBox() {
        ElementsCollection list = ALL_CHECKBOXES;
        list.get(randomNumbersInt(list.size())).click();
        MARK_ALL_SOURCES.shouldBe(visible).click();
        waitForAjax();

        int count = list.filter(selected).size();
        if (count == list.size()) {
            UNMARK_ALL_SOURCES.shouldBe(visible).click();
            waitForAjax();
            count = list.filter(selected).size();
        }
        return count == 0;
    }

    public boolean checkReachBlock() {
        REACH_INFORMATION.shouldHave(CollectionCondition.sizeGreaterThan(0));
        ALL_SOURCES.shouldHave(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().checkDataset(REACH_INFORMATION.size(), ALL_SOURCES.size());
    }

    /**
     * get publisher coefficient
     */
    public String getCoefficient() {
        return selectiveBiddingHelper1.getCoefficientInTable();
    }

    public String getSelectiveBiddingWidgetUID() {
        return WIDGET_UID_CELL.shouldBe(visible).text().trim();
    }

    public void importBlockList(String fileName) {
        IMPORT_BUTTON.shouldBe(visible).click();
        IMPORT_BLACKLIST.shouldBe(visible).click();
        IMPORT_INPUT.shouldBe(visible).sendKeys(LINK_TO_RESOURCES_FILES + fileName);
        IMPORT_SAVE.shouldBe(visible).click();
        sleep(2000);
        waitForAjax();
        helpersInit.getBaseHelper().getTextAndWriteLog(fileName);
    }

    public List<String> getWidgetsListFromPage() {
        return WIDGET_SOURCES_IDS.texts();
    }

    public boolean checkImportBlockListIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IMPORT_BUTTON.isDisplayed());
    }
}
