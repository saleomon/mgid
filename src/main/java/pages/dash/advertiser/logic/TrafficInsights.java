package pages.dash.advertiser.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.TrafficInsightsHelper;
import core.helpers.sorted.SortedType;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.GEO_TARGETING_LABELS;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.PLATFORM_TARGETING_LABELS;
import static pages.dash.advertiser.locators.TrafficInsightsLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;

public class TrafficInsights {

    private final HelpersInit helpersInit;
    private final TrafficInsightsHelper trafficInsightsHelper;
    private final Logger log;

    public TrafficInsights(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        trafficInsightsHelper = new TrafficInsightsHelper(log, helpersInit);
    }

    private String geoTargetingCountry;

    public String getGeoTargetingCountry() {
        return geoTargetingCountry;
    }

    public String getTrafficInsightsTitle() {
        return PAGE_TITLE.shouldBe(visible).getText();
    }

    public void enableAllPlatform() {
        PLATFORM_ALL_BUTTON.click();
    }

    public void enableDesktopPlatform() {
        PLATFORM_DESKTOP_BUTTON.click();
    }

    public void enableMobilePlatform() {
        PLATFORM_MOBILE_BUTTON.click();
    }

    public void enablePushTrafficType() {
        TRAFFIC_TYPE_PUSH_BUTTON.click();
    }

    public void enableNativeTrafficType() {
        TRAFFIC_TYPE_NATIVE_BUTTON.click();
    }

    public void searchCountry(String countryValue) {
        clearAndSetValue(FIND_COUNTRY_INPUT, countryValue);
        FIND_COUNTRY_TEXT.shouldBe(visible).click();
        FIND_COUNTRY_TEXT.shouldBe(hidden);
    }

    public void clickPaginationNextButton() {
        PAGINATION_NEXT_BUTTON.shouldBe(visible).click();
    }

    public void clickPaginationPreviousButton() {
        PAGINATION_PREVIOUS_BUTTON.shouldBe(visible).click();
    }

    public void selectCampaignTypeValue(String campaignTypeValue) {
        CAMPAIGN_TYPE_SELECT.shouldBe(visible).click();
        switch (campaignTypeValue) {
            case "product" -> CAMPAIGN_TYPE_SELECT_VALUE_1.shouldBe(visible).click();
            case "content" -> CAMPAIGN_TYPE_SELECT_VALUE_2.shouldBe(visible).click();
            default -> log.info("default");
        }
    }

    public int getCountryListSizeAfterSearch() {
        return TRAFFIC_INSIGHTS_ROWS.size();
    }

    public String getCountryNameFromList() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TRAFFIC_INSIGHTS_ROWS.get(0).find("td").text());
    }

    public boolean checkSortedColumns(SortedType.SortType type, String column) {
        Object[] data = trafficInsightsHelper.sortColumnAndGetDataFromColumn(type, column);
        return trafficInsightsHelper.checkSortedData(data, type);
    }

    public void addCampaignByCountry() {
        int rowId = trafficInsightsHelper.selectCountryFromList();
        geoTargetingCountry = trafficInsightsHelper.addCampaignForCountry(rowId);
        switchTo().window("Adding new campaign :: Advertisers :: Dashboard");
    }

    public boolean checkGeoTargeting() {
        return helpersInit.getBaseHelper().checkDatasetEquals(geoTargetingCountry, GEO_TARGETING_LABELS.first().text());
    }

    public String getPlatformTargeting() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PLATFORM_TARGETING_LABELS.first().text());
    }
}