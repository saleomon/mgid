package pages.dash.advertiser.logic;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static pages.dash.advertiser.locators.DetailedGoodStatsLocators.*;
import static core.helpers.BaseHelper.waitForAjax;

public class DetailedGoodStats {
    private int clicksTable;
    private double spentTable;
    private double percentTable;
    private List<String> sliceTable;

    public enum StatsSection {
        DOMAIN("informer_uid"),
        REGION("country_id-region_id"),
        COUNTRY("country_id"),
        OS("os_id"),
        BROWSER("browser_id"),
        CONNECTION_TYPE("connection_type"),
        ADS("teaser_id"),
        ADV_TRAFFIC_TYPE("adv_traffic_type"),
        PHONE_PRICE_RANGES("phones_price_range");

        private final String statsSectionValue;

        StatsSection(String statsSectionValue) {
            this.statsSectionValue = statsSectionValue;
        }

        public String getTypeValue() {
            return statsSectionValue;
        }
    }

    public DetailedGoodStats() {
    }
    public int getClicksTable() {
        return clicksTable;
    }

    public double getSpentTable() {
        return spentTable;
    }

    public double getPercentTable() {
        return percentTable;
    }

    public List<String> getSliceTable() {
        return sliceTable;
    }

    public void chooseStatBy(StatsSection section) {
        $("[data-by='" + section.getTypeValue() + "']").click();
        waitForAjax();
    }

    public void getSumColumnsTableData() {
        clicksTable = CLICK_DATAS.texts().stream().mapToInt(el -> Integer.parseInt(el.trim())).sum();
        spentTable = SPENT_DATAS.texts().stream().mapToDouble(el -> Double.parseDouble(el.trim())).sum();
        percentTable = PERCENT_DATAS.texts().stream().mapToDouble(el -> Double.parseDouble(el.trim())).sum();
        sliceTable = SECTION_DATAS.texts();
    }

}
