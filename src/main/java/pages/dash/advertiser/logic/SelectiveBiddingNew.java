package pages.dash.advertiser.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.SelectiveBiddingNewHelper;
import core.helpers.sorted.SortedType;

import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.SelectiveBiddingNewLocators.*;
import static core.helpers.BaseHelper.randomNumbersInt;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class SelectiveBiddingNew {
    private final HelpersInit helpersInit;
    private final SelectiveBiddingNewHelper selectiveBindingHelper;

    public SelectiveBiddingNew(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        selectiveBindingHelper = new SelectiveBiddingNewHelper(log, helpersInit);
    }

    int randomIndexProvider;


    public void expandAllProviders(int amountProviders) {
        selectiveBindingHelper.expandAllProviders(amountProviders);
    }

    public void chooseRandomProvider() {
        PROVIDERS_CHECKBOX.shouldBe(visible);
        randomIndexProvider = randomNumbersInt(PROVIDERS_CHECKBOXES.size() - 1);
        BLOCK_BUTTON.scrollTo();
        PROVIDERS_CHECKBOXES.get(randomIndexProvider).click();
    }

    public void selectAllProviders() {
        PROVIDERS_CHECKBOX.shouldBe(visible);
        PROVIDERS_CHECKBOXES.asFixedIterable().forEach(SelenideElement::click);
    }

    public void filterByStatus(String option) {
        selectiveBindingHelper.filterByStatus(option);
        clickFilterButton();
        sleep(2000);
    }

    public void filterByCoefficient(String option) {
        selectiveBindingHelper.filterByCoefficient(option);
        clickFilterButton();
        sleep(2000);
    }

    public void clickFilterButton() {
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void chooseRandomSources() {
        selectiveBindingHelper.chooseRandomSources();
    }

    public void clickBlockUnblockButtonMass(String state) {
        selectiveBindingHelper.clickBlockUnblockButtonMass(state);
    }

    public int getNumberOfSelectedSources() {
        return SELECTED_SOURCES.size();
    }

    public void unblockRandomSources(int amount) {
        selectiveBindingHelper.unblockRandomSources(amount);
    }

    public void blockRandomSources(int amount) {
        selectiveBindingHelper.blockRandomSources(amount);
    }

    public int getNumberOfUnblockedElements() {
        PROGRESS_BAR.shouldNotBe(visible);
        return ACTIVE_ELEMENTS.shouldHave(CollectionCondition.sizeGreaterThan(1)).size();
    }

    public int getNumberOfDisplayedCoefIcons() {
        return $$("span[class*='column-qualityFactor']")/*CHANGED_COEFFICIENT_ICONS*/.size();
    }

    public int getNumberOfBlockedElements() {
        /*BLOCKED_ELEMENTS*/$$x("//mat-slide-toggle[not(contains(@class, 'mat-disabled'))]//*[@class='mat-slide-toggle-content' and contains(.,'Off')]").shouldHave(CollectionCondition.sizeGreaterThan(1));
        return $$x("//mat-slide-toggle[not(contains(@class, 'mat-disabled'))]//*[@class='mat-slide-toggle-content' and contains(.,'Off')]")/*BLOCKED_ELEMENTS*/.size();
    }

    public int getNumberOfBlockedSources() {
        $$x("//section//*[@class='mat-slide-toggle-content' and contains(.,'Off')]")/*BLOCKED_SOURCES*/.shouldHave(CollectionCondition.sizeGreaterThan(1));
        return $$x("//section//*[@class='mat-slide-toggle-content' and contains(.,'Off')]")/*BLOCKED_SOURCES*/.size();
    }

    public int getNumberOfUnblockedSources() {
        /*ACTIVE_SOURCES*/$$x("//section//*[@class='mat-slide-toggle-content' and contains(.,'On')]").shouldHave(CollectionCondition.sizeGreaterThan(1));
        return /*ACTIVE_SOURCES*/$$x("//section//*[@class='mat-slide-toggle-content' and contains(.,'On')]").size();
    }

    public void clickBlockUnblockProviderButton() {
        STATUS_PROVIDER_BUTTON.click();
    }

    public int getNumberOfAllSources() {
        /*ALL_DISPLAYED_SOURCES*/$$("section [class='mat-slide-toggle-content']").get(0).shouldBe(visible);
        return /*ALL_DISPLAYED_SOURCES*/$$("section [class='mat-slide-toggle-content']").size();
    }

    public int getNumberOfAllProviders() {
        $$("header [role='row']")/*DISPLAYED_PROVIDERS*/.get(0).shouldBe(visible);
        return $$("header [role='row']")/*DISPLAYED_PROVIDERS*/.size();
    }

    public int getProviderColumnDataInt(String column) {
        return helpersInit.getBaseHelper().parseInt($("header [role='columnheader'][class*='mat-column-" + column + "']"));
    }

    public String getSourceCoefficient(int index) {
        return SOURCES_COEFFICIENTS.get(index).text();
    }

    public Double getProviderColumnDataDouble(String column) {
        return Double.parseDouble($("header [role='columnheader'][class*='mat-column-" + column + "']").text().trim());
    }

    public List<String> getSourcesDataFromColumn(String column) {
        return $$("section [class*='cdk-cell cdk-column-" + column + "']").shouldHave(CollectionCondition.size(5)).texts();
    }

    public int getSumSourcesColumnDataInt(String column) {
        return helpersInit.getBaseHelper().getSumInteger($$("section [class*='cdk-cell cdk-column-" + column + "']").shouldHave(CollectionCondition.size(5)));
    }

    public double getSourcesColumnDataDouble(String column) {
        return helpersInit.getBaseHelper().getSumDouble($$("section [class*='cdk-cell cdk-column-" + column + "']"));
    }

    public double getSourcesColumnDataDoubleAveradge(String column) {
        double averadge = helpersInit.getBaseHelper().getSumDouble($$("section [class*='cdk-cell cdk-column-" + column + "']"));
        return averadge / $$("section [class*='cdk-cell cdk-column-" + column + "']").size();
    }

    public int getAllSourcesWithPrice(String price) {
        return selectiveBindingHelper.getAmountOfSourcesWithPrice(price);
    }

    public void changePriceByProvider(String price) {
        selectiveBindingHelper.changePriceByProvider(price);
    }

    public void changePriceBySource(String price) {
        selectiveBindingHelper.changePriceBySource(price);
    }

    public boolean checkSortedSelectiveProviders(SortedType.SortType type, String column) {
        Object[] data = selectiveBindingHelper.sortColumnAndGetDataProviders(type, column);
        return selectiveBindingHelper.checkSortedSelective(data, type);
    }

    public boolean checkSortedSelectiveSources(SortedType.SortType type, String column) {
        Object[] data = selectiveBindingHelper.sortColumnAndGetDataSources(type, column);
        return selectiveBindingHelper.checkSortedSelective(data, type);
    }

}
