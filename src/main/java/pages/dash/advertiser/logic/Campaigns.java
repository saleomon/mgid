package pages.dash.advertiser.logic;

import com.codeborne.selenide.CollectionCondition;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.AddEditCampaignHelper;
import pages.dash.advertiser.helpers.CampaignListHelper;
import core.helpers.statCalendar.CalendarForStatistics;
import testData.project.Subnets;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.*;
import static pages.dash.advertiser.locators.CampaignListLocators.*;
import static pages.dash.advertiser.locators.ListTeaserLocators.TEASERS_LIST_STATISTICS_TABLE_HEADERS;
import static pages.dash.advertiser.locators.ListTeaserLocators.TEASER_LIST_STATISTICS_TABLE_VALUES;
import static pages.dash.advertiser.variables.AddEditCampaignVariables.endLimit;
import static pages.dash.advertiser.variables.AddEditCampaignVariables.startLimit;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.dash.publisher.locators.WebsiteListLocators.*;

public class Campaigns {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final AddEditCampaignHelper addEditCampaignHelper;
    private final CampaignListHelper campaignListHelper;
    private String billingCountry;
    private boolean targetingSwitcher = true;
    private String contactPersonName;
    private String currencyOfProfile;
    private String corporateClientCountry;
    private String businessType;
    private String companyName;
    private String contactPersonEmail;

    public Campaigns(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        addEditCampaignHelper = new AddEditCampaignHelper(log, helpersInit);
        campaignListHelper = new CampaignListHelper(helpersInit);
    }

    private Map<String, String> stages;
    private Map<String, String> schedule = null;
    private String campaignName, campaignCategory, campaignLanguage, dailyLimit, overallLimit, periodOfTimeTeasersNotShown, campaignId, scheduleCheckboxValue;
    private String campaignKeyword = null;
    private boolean blockBeforeShow;
    private boolean isShowAllTeaserAfterConversion;
    private boolean usePushDuplicate;
    private Subnets.SubnetType subnetType;
    private String sourcesUtm = "sour";
    private String[] setTargetOption;
    private boolean useTargeting = true;
    private String campaignUtm = "camp";
    private String mediumUtm = "medi";
    private String campaignType = "";
    private String customUtm = "campaign={campaign_id}";
    private boolean isEnabledSensors = true;
    private String certificate;
    private String limitType = "budget_limits";
    private boolean isUseSchedule = true;
    public boolean isEnableUtms = true;
    JsonObject targeting;
    private boolean copyBlackList = true;

    public Campaigns setLimitType(String limitType) {
        this.limitType = limitType;
        return this;
    }

    public Campaigns setDailyLimit(String dailyLimit) {
        this.dailyLimit = dailyLimit;
        return this;
    }

    public Campaigns setOverallLimit(String overallLimit) {
        this.overallLimit = overallLimit;
        return this;
    }

    public Campaigns setSourcesUtm(String sourcesUtm) {
        this.sourcesUtm = sourcesUtm;
        return this;
    }

    public Campaigns setIsNeedToEdit(boolean state) {
        addEditCampaignHelper.setIsNeedToEdit(state);
        return this;
    }

    public Campaigns setSubnetType(Subnets.SubnetType subnetType) {
        this.subnetType = subnetType;
        return this;
    }

    public Campaigns setCustomUtm(String customUtm) {
        this.customUtm = customUtm;
        return this;
    }

    public String getCustomUtm() {
        return customUtm;
    }

    public Campaigns setMediumUtm(String mediumUtm) {
        this.mediumUtm = mediumUtm;
        return this;
    }

    public Campaigns setCampaignUtm(String campaignUtm) {
        this.campaignUtm = campaignUtm;
        return this;
    }

    public Campaigns setCampaignName(String campaignName) {
        this.campaignName = campaignName;
        return this;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public Campaigns setCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    public Campaigns setCampaignCategory(String campaignCategory) {
        this.campaignCategory = campaignCategory;
        return this;
    }

    public Campaigns setCampaignKeyword(String campaignKeyword) {
        this.campaignKeyword = campaignKeyword;
        return this;
    }

    public Campaigns setCampaignLanguage(String campaignLanguage) {
        this.campaignLanguage = campaignLanguage;
        return this;
    }

    public Campaigns setBlockBeforeShow(boolean blockBeforeShow) {
        this.blockBeforeShow = blockBeforeShow;
        return this;
    }

    public Campaigns setTargetingOption(String... setTargetOption) {
        this.setTargetOption = setTargetOption;
        return this;
    }

    public Campaigns setUsePushDuplicate(boolean usePushDuplicate) {
        this.usePushDuplicate = usePushDuplicate;
        return this;
    }

    public Campaigns setIsUseSchedule(boolean isUseSchedule) {
        this.isUseSchedule = isUseSchedule;
        return this;
    }

    public Campaigns setIsEnabledSensors(boolean isEnabledSensors) {
        this.isEnabledSensors = isEnabledSensors;
        return this;
    }

    public Campaigns setIsEnableUtms(boolean isEnableUtms) {
        this.isEnableUtms = isEnableUtms;
        return this;
    }

    public Campaigns setTargetingSwitcher(boolean targetingSwitcher) {
        this.targetingSwitcher = targetingSwitcher;
        return this;
    }

    public Campaigns setCertificate(String certificate) {
        this.certificate = certificate;
        return this;
    }

    public Campaigns setDynamicRetargeting(boolean dynamicRetargeting) {
        return this;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public Campaigns clearAllSettings() {
        campaignName = null;
        campaignType = "";
        campaignCategory = null;
        campaignLanguage = null;
        sourcesUtm = null;
        mediumUtm = null;
        campaignUtm = null;
        customUtm = null;
        limitType = null;
        dailyLimit = null;
        overallLimit = null;
        campaignKeyword = null;
//        isEnableUtms = false;
        if (stages != null) {
            stages.clear();
        }
        if (schedule != null) {
            schedule.clear();
        }
        return this;
    }

    public Campaigns setUseTargeting(boolean useTargeting) {
        this.useTargeting = useTargeting;
        return this;
    }

    public String getCampaignLanguage() {
        return campaignLanguage;
    }

    public void createCampaignSimple() {
        campaignName = addEditCampaignHelper.fillCampaignName(campaignName);
        campaignType = addEditCampaignHelper.selectCampaignType(campaignType);
        campaignCategory = addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        campaignKeyword = addEditCampaignHelper.fillCampaignKeyword(campaignKeyword);
        campaignLanguage = addEditCampaignHelper.selectCampaignLanguage(campaignLanguage);
        addEditCampaignHelper.changeStateBlockBeforeShow(blockBeforeShow);
        targeting = addEditCampaignHelper.selectTargeting(useTargeting, campaignType, subnetType);
        uploadCertificates(certificate);
        addEditCampaignHelper.saveGeneralSettings();
        chooseUtms();
        log.info("1");
        stages = addEditCampaignHelper.addAllRandomStages(isEnabledSensors);
        log.info("2");

        setLimits(limitType);
        log.info("3");

        schedule = addEditCampaignHelper.setScheduleRandom(isUseSchedule);
        log.info("4");

        addEditCampaignHelper.usePushDuplicate(usePushDuplicate);
        log.info("5");

        addEditCampaignHelper.saveCampaign();
        log.info("6");

        campaignId = addEditCampaignHelper.getCampaignIdFromList(campaignName);
        log.info("7");
    }

    public void editCampaign() {
        campaignName = addEditCampaignHelper.fillCampaignName(campaignName);
        campaignCategory = addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        campaignKeyword = addEditCampaignHelper.fillCampaignKeyword(campaignKeyword);
        addEditCampaignHelper.changeStateBlockBeforeShow(blockBeforeShow);
        addEditCampaignHelper.clearTargetingSettings(useTargeting);
        targeting = addEditCampaignHelper.selectTargeting(useTargeting, setTargetOption);
        chooseUtms();
        stages = addEditCampaignHelper.addAllRandomStages(isEnabledSensors);
        setLimits(limitType);
        scheduleCheckboxValue = addEditCampaignHelper.setScheduleByCheckboxes(isUseSchedule);
        addEditCampaignHelper.saveCampaign();
    }

    public boolean checkCampaignSettingsAfterCreating() {
        return addEditCampaignHelper.checkCampaignName(campaignName) &&
                addEditCampaignHelper.checkCampaignCategory(campaignCategory) &&
                addEditCampaignHelper.checkCampaignKeyword(campaignKeyword) &&
                addEditCampaignHelper.checkCampaignLanguage(campaignLanguage) &&
                checkUtms() &&
                addEditCampaignHelper.checkConversionStages(stages) &&
                addEditCampaignHelper.checkSelectedTargets(useTargeting, targeting) &&
                addEditCampaignHelper.checkLimitOption(limitType, dailyLimit, overallLimit) &&
                addEditCampaignHelper.checkLimitSubOption(isShowAllTeaserAfterConversion, periodOfTimeTeasersNotShown) &&
                addEditCampaignHelper.checkScheduleOption(schedule) &&
                addEditCampaignHelper.checkScheduleByCheckboxes(scheduleCheckboxValue) &&
                addEditCampaignHelper.checkConversionStages(stages);
    }

    public boolean checkCampaignSettingsAfterEditing() {
        return addEditCampaignHelper.checkCampaignName(campaignName) &&
                addEditCampaignHelper.checkCampaignCategory(campaignCategory) &&
                checkUtms() &&
                addEditCampaignHelper.checkConversionStages(stages) &&
                addEditCampaignHelper.checkLimitOption(limitType, dailyLimit, overallLimit) &&
                addEditCampaignHelper.checkScheduleByCheckboxes(scheduleCheckboxValue) &&
                addEditCampaignHelper.checkConversionStages(stages);
    }

    public void setLimits(String limitType) {
        if (limitType != null) {
            switch (limitType) {
                case "budget_limits" -> {
                    helpersInit.getBaseHelper().selectCurrentValInSelectListJS(SELECT_LIMITS, limitType);
                    dailyLimit = addEditCampaignHelper.fillLimitField(LIMIT_BUDGET_PER_DAY, dailyLimit, startLimit, endLimit);
                    overallLimit = addEditCampaignHelper.fillLimitField(LIMIT_BUDGET_GENERAL, overallLimit, Integer.parseInt(dailyLimit) + 1, endLimit);
                }
                case "clicks_limits" -> {
                    helpersInit.getBaseHelper().selectCurrentValInSelectListJS(SELECT_LIMITS, limitType);
                    dailyLimit = addEditCampaignHelper.fillLimitField(LIMIT_CLICKS_PER_DAY, dailyLimit, startLimit, endLimit);
                    overallLimit = addEditCampaignHelper.fillLimitField(LIMIT_CLICKS_GENERAL, overallLimit, Integer.parseInt(dailyLimit) + 1, endLimit);
                }
            }
            periodOfTimeTeasersNotShown = addEditCampaignHelper.isShowAllTeaserAfterConversion(isShowAllTeaserAfterConversion, periodOfTimeTeasersNotShown);
        }
    }

    public void chooseUtms() {
        if(isEnableUtms) {
            addEditCampaignHelper.changeStateTrackingsTag(isEnableUtms);
            addEditCampaignHelper.chooseSourceTag(sourcesUtm);
            addEditCampaignHelper.chooseMediumTag(mediumUtm);
            addEditCampaignHelper.chooseCustomTag(customUtm);
            addEditCampaignHelper.chooseCampaignTag(campaignUtm);
        } else {
            addEditCampaignHelper.changeStateTrackingsTag(isEnableUtms);
        }
    }

    public boolean checkUtms() {
        return addEditCampaignHelper.checkSourceTag(sourcesUtm) &&
                addEditCampaignHelper.checkMediumTag(mediumUtm) &&
                addEditCampaignHelper.checkCampaignTag(campaignUtm) &&
                addEditCampaignHelper.checkCustomTag(customUtm);
    }

    public void deleteCampaign(String campaignId) {
        campaignListHelper.blockCampaign(campaignId);
        campaignListHelper.deleteCampaign(campaignId);
    }

    public boolean isDisplayedRestoreCampaign(String campaignId) {
        return campaignListHelper.isDisplayedRestoreCampaign(campaignId);
    }

    public boolean isDisplayedCampaign(String campaignName) {
        return campaignListHelper.isDisplayedCampaign(campaignName);
    }

    public void getCampaignIdFromListByName(String name) {
        campaignId = addEditCampaignHelper.getCampaignIdFromList(name);
    }

    public void setGoogleTagManagerConversion() {
        addEditCampaignHelper.setGoogleTagManagerConversion();
        addEditCampaignHelper.saveCampaign();
    }
    
    public void setGoogleAnalyticsConversion() {
        addEditCampaignHelper.enableTabGoogleAnalyticsConversion();
    }

    public boolean checkSavedConversion(String typeConversion) {
        return switch (typeConversion) {
            case "manager" -> addEditCampaignHelper.checkIsEnabledConversion(GOOGLE_TAG_MANAGER_SWITCHER);
            case "analytics" -> addEditCampaignHelper.checkIsVisibleGoogleAnalytics(GOOGLE_ANALYTICS_SWITCHER);
            case "metrics" -> addEditCampaignHelper.checkIsVisibleYandexMetrix(YANDEX_METRIX_SWITCHER);
            default -> false;
        };
    }

    /**
     * unblock campaign
     */
    public void unBlockCampaign(String campaignId) {
        $(String.format(UNBLOCK_CAMPAIGN_BUTTON, campaignId)).click();
        waitForAjax();
    }

    /**
     * check popup message
     */
    public boolean checkPopupMessage(String message) {
        return helpersInit.getBaseHelper().checkDatasetEquals(message, POPUP_TEXT.text());
    }

    /**
     * check visibility unblock icon
     */
    public boolean checkVisibilityUnblockIcon(String campaignId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($("[class*='actions'] [data-id='" + campaignId + "'][class*='unblock-partner icon-play']").isDisplayed());
    }

    public Campaigns selectLocationTarget(String typeTarget, String... countries) {
        addEditCampaignHelper.enableTargeting(targetingSwitcher);
        addEditCampaignHelper.clearTargetingSettings(true);
        addEditCampaignHelper.selectLocationTarget(typeTarget, countries);
        return this;
    }

    public void saveCampaign() {
        addEditCampaignHelper.saveCampaign();
    }

    public void fillFirstBlockCreateCampaign() {
        campaignName = addEditCampaignHelper.fillCampaignName(campaignName);
        campaignType = addEditCampaignHelper.selectCampaignType(campaignType);
        campaignCategory = addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        campaignLanguage = addEditCampaignHelper.selectCampaignLanguage(campaignLanguage);
        addEditCampaignHelper.saveGeneralSettings();
    }

    /**
     * Check if 'Self registered popup' appears
     */
    public boolean checkSelfRegisterPopup() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SELF_REGISTER_POPUP.shouldBe(appear, Duration.ofSeconds(10)).isDisplayed());
    }

    public void chooseIndividualInSelfRegisterPopup() {
        SELF_REGISTER_POPUP_INDIVIDUAL_BUTTON.click();
        checkErrors();
    }

    public void chooseLegalEntityInSelfRegisterPopup() {
        SELF_REGISTER_POPUP_LEGAL_ENTITY_BUTTON.click();
        checkErrors();
        SELF_REGISTER_POPUP_LEGAL_ENTITY_BUTTON.shouldNotBe(visible, Duration.ofSeconds(10));
    }

    /**
     * Set settings for self registered Advertiser
     */
    public void setupProfileInPopup() {
        chooseIndividualInSelfRegisterPopup();
        SETUP_PROFILE_POPUP.shouldBe(visible, Duration.ofSeconds(10));
        billingCountry = campaignListHelper.setClientsCountry();
        campaignListHelper.saveAndFinish();
    }

    public void setupProfileInPopupLegalEntity(List<String> forbiddenCountries) {
        chooseLegalEntityInSelfRegisterPopup();
        SETUP_PROFILE_POPUP.shouldBe(visible, Duration.ofSeconds(10));
        currencyOfProfile = campaignListHelper.selectCurrencyOfProfile();
        corporateClientCountry = campaignListHelper.selectCorporateClientCountry(forbiddenCountries);
        businessType = campaignListHelper.selectBusinessType();
        companyName = campaignListHelper.setCompanyName();
        contactPersonName = campaignListHelper.setContactPersonName();
        contactPersonEmail = campaignListHelper.setContactPersonEmail();
        SETUP_PROFILE_POPUP.click();
        campaignListHelper.saveAndFinish();
        SETUP_PROFILE_POPUP.shouldNotBe(visible, Duration.ofSeconds(10));
    }

    public boolean checkProfileOfLegalEntity(String emailValue) {
        return campaignListHelper.checkLegalRelation("legal_entity") &&
                campaignListHelper.checkCurrencyOfProfile(currencyOfProfile) &&
                campaignListHelper.checkCorporateClientCountry(corporateClientCountry) &&
                campaignListHelper.checkBusinessType(businessType) &&
                campaignListHelper.checkCompanyName(companyName) &&
                campaignListHelper.checkContactPersonName(emailValue) &&
                campaignListHelper.checkContactPersonEmail(contactPersonEmail);
    }

    /**
     * Set settings for self registered Advertiser
     */
    public void setupProfileInPopupForAdskeeper(boolean isAdvertiser) {
        campaignListHelper.chooseCooperationType(isAdvertiser);
        billingCountry = campaignListHelper.setClientsCountry();
        campaignListHelper.saveAndFinish();
    }

    /**
     * Check clients login into dashboard on pages
     */
    public boolean checkLoginOnPage(String loginValue) {
        return campaignListHelper.checkLoginOnPage(loginValue);
    }

    public boolean isDisableKeywordField() {
        return CAMPAIGN_KEYWORD.shouldBe(visible).has(attribute("disabled"));
    }

    public String getKeyword() {
        return CAMPAIGN_KEYWORD.val();
    }

    public void checkReachMeterImpressionsCounter(String impressionsValue) {
        REACH_METER_IMPRESSIONS_COUNTER.shouldHave(ownText(impressionsValue), Duration.ofSeconds(8));
    }

    public void checkReachMeterPercent(String percentValue) {
        REACH_METER_PROGRESS_BAR.shouldHave(attribute("style", percentValue));
    }

    public void selectTrafficTypeTargeting(String targetingType) {
        addEditCampaignHelper.trafficTarget(targetingType);
    }

    public void selectPhonePriceTargeting(String targetingType) {
        addEditCampaignHelper.phonePriceRangeTarget(targetingType);
    }

    public void selectInterestsTargeting(String targetingType) {
        INTERESTS_TARGET.click();
        addEditCampaignHelper.chooseRandomInterestTarget(targetingType);
    }

    /**
     * Create campaign with targeting by City
     */
    public void createCampaignWithCityTargeting() {
        campaignName = addEditCampaignHelper.fillCampaignName(campaignName);
        campaignType = addEditCampaignHelper.selectCampaignType(campaignType);
        campaignCategory = addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        campaignKeyword = addEditCampaignHelper.fillCampaignKeyword(campaignKeyword);
        campaignLanguage = addEditCampaignHelper.selectCampaignLanguage(campaignLanguage);
        addEditCampaignHelper.changeStateBlockBeforeShow(blockBeforeShow);
        targeting = addEditCampaignHelper.selectCityTargeting();
        addEditCampaignHelper.saveGeneralSettings();
        chooseUtms();
        addEditCampaignHelper.changeStateConversionTracking(false);
        setLimits(limitType);
        addEditCampaignHelper.saveCampaign();
        campaignId = addEditCampaignHelper.getCampaignIdFromList(campaignName);

    }

    /**
     * Check targeting by city settings
     */
    public boolean checkSelectedCityTargets() {
        return addEditCampaignHelper.checkSelectedCityTarget(targeting);
    }

    public void createCampaignWithId() {
        createCampaign();
        campaignId = addEditCampaignHelper.getCampaignIdFromList(campaignName);
    }

    public void createCampaign() {
        campaignName = addEditCampaignHelper.fillCampaignName(campaignName);
        campaignType = addEditCampaignHelper.selectCampaignType(campaignType);
        campaignCategory = addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        campaignLanguage = addEditCampaignHelper.selectCampaignLanguage(campaignLanguage);
        campaignKeyword = addEditCampaignHelper.fillCampaignKeyword(campaignKeyword);
        targeting = addEditCampaignHelper.selectTargeting(useTargeting, setTargetOption);
        addEditCampaignHelper.saveGeneralSettings();
        chooseUtms();
        setLimits(limitType);
        schedule = addEditCampaignHelper.setScheduleRandom(isUseSchedule);
        stages = addEditCampaignHelper.addAllRandomStages(isEnabledSensors);
        addEditCampaignHelper.usePushDuplicate(usePushDuplicate);
        addEditCampaignHelper.saveCampaign();
    }

    /**
     * Check targeting settings
     */
    public boolean checkSelectedTargets() {
        return addEditCampaignHelper.checkSelectedTargets(useTargeting, targeting);
    }

    public String chooseLanguageWithoutContext(List<String> languageForContext) {
        return addEditCampaignHelper.chooseLanguageWithoutContext(languageForContext);
    }

    public boolean isDisplayedContextTargetingTab() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CONTEXT_TAB.scrollTo().isDisplayed());
    }

    public boolean isDisplayedSentimentsTargetingTab() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SENTIMENTS_TAB.isDisplayed());
    }

    public Campaigns fillCampaignName(String campaignName) {
        addEditCampaignHelper.fillCampaignName(campaignName);
        return this;
    }

    public Campaigns chooseCampaignLanguage(String campaignsLanguage) {
            addEditCampaignHelper.selectCampaignLanguage(campaignsLanguage);
            return this;
    }

    public Campaigns chooseCampaignType(String campaignType) {
        addEditCampaignHelper.selectCampaignType(campaignType);
        return this;
    }

    public Campaigns chooseCampaignCategory(String campaignCategory) {
        addEditCampaignHelper.selectCampaignCategory(campaignCategory);
        return this;
    }

    public void confirmCampaignGeneralSettings() {
        addEditCampaignHelper.saveGeneralSettings();
    }

    public int checkCampaignTypeIsPresent(String campaignType) {
        CAMPAIGN_TYPE_FIELD.shouldBe(visible);
        return CAMPAIGN_TYPE.shouldBe(exist).findAll("span").filterBy(attribute("val", campaignType)).size();
    }

    public boolean checkDuplicatePushCampaignIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUSH_DUPLICATE_SWITCHER.isDisplayed());
    }

    public String getCampaignStatus() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_STATUS_BLOCK.find(CAMPAIGN_STATUS_FIELD).text());
    }

    public boolean checkLimitIconIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_STATUS_BLOCK.find(CAMPAIGN_STATUS_LIMIT_ICON).isDisplayed());
    }

    public String getCampaignStatusTitle() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_STATUS_BLOCK.find(CAMPAIGN_STATUS_LIMIT_ICON).attr("title"));
    }

    public Campaigns uploadCertificates(String images) {
        if(images != null) {
            BLOCK_OF_CERTIFICATE.shouldBe(visible);
            UPLOAD_IMAGE_INPUT.sendKeys(images);
            waitForAjaxLoader();
        }
        return this;
    }

    public boolean checkDisplayingUploadedCertificate(String image) {
        return UPLOADED_IMAGES.texts().stream().anyMatch(el->el.contains(image));
    }

    public void deleteCertificate() {
        DELETE_CERTIFICATE_BUTTON.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
    }

    public boolean checkDisplayingBlockUploadCertificate() {
        return BLOCK_OF_CERTIFICATE.isDisplayed();
    }

    public boolean checkPhonePriceRangeTargetingIsDisplayed() {
        AVAILABLE_TARGETINGS_BLOCK.shouldBe(visible).scrollTo();
        return PHONE_PRICE_RANGES_TAB.isDisplayed();
    }

    public boolean checkReachMetrics(String checkedColumnName) {
        return CAMPAIGN_STATISTICS_TABLE_HEADERS.texts().contains(checkedColumnName);
    }

    public void selectPeriodInCalendar(CalendarForStatistics.CalendarPeriods periodValue) {
        ArrayList<String> startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(periodValue);
        helpersInit.getBaseHelper().selectCalendarPeriodJs(periodValue, startEndDate);
    }

    public boolean checkReachMetricsValues(int campaignId, int columnNumber, String checkedColumnValue) {
        return $x(String.format(CAMPAIGN_STATISTICS_TABLE_VALUES, campaignId, columnNumber)).text().equals(checkedColumnValue);
    }

    public boolean checkReachMetricsInDailyCampaignStat(String checkedColumnName) {
        return DAILY_CAMPAIGN_STATISTICS_TABLE_HEADERS.texts().contains(checkedColumnName);
    }

    public boolean checkReachMetricsInDailyCampaignStatValues(int columnNumber, String checkedColumnValue) {
        return $x(String.format(DAILY_CAMPAIGN_STATISTICS_TABLE_VALUES, columnNumber)).text().equals(checkedColumnValue);
    }

    public boolean checkReachMetricsInTeasersListStat(String checkedColumnName) {
        return TEASERS_LIST_STATISTICS_TABLE_HEADERS.texts().contains(checkedColumnName);
    }

    public boolean checkReachMetricsInTeasersListStatValues(int columnNumber, String checkedColumnValue) {
        return $x(String.format(TEASER_LIST_STATISTICS_TABLE_VALUES, columnNumber)).text().equals(checkedColumnValue);
    }

    public boolean checkWarningAboutRotationInUkraine(String text) {
        return $$(ROTATION_IN_UKRAINE_TARGETING_BLOCK_TEXT).filter(visible).texts().contains(text);
    }

    public void clickAddFundsButton() {
        ADD_FUNDS_BUTTON.shouldBe(visible).click();
        log.info("Add funds button was clicked!");
    }

    public void setPaymentSum(String amountValue) {
        PAYMENT_AMOUNT_INPUT.shouldBe(visible);
        clearAndSetValue(PAYMENT_AMOUNT_INPUT, String.valueOf(amountValue));
        PAYMENT_SUM_FIELD.click();
    }

    public boolean checkMinPaymentSumValidation(String errorMessage) {
        return helpersInit.getBaseHelper().checkDatasetEquals(errorMessage, PAYMENT_ERROR_MESSAGE.text().trim()) &&
                Objects.requireNonNull(PAYMENT_AMOUNT_INPUT.getAttribute("class")).contains("incorrect") &&
                helpersInit.getBaseHelper().getTextAndWriteLog(CHECKOUT_BUTTON.getAttribute("disabled") != null);
    }

    public boolean checkPaymentSystemsAlert() {
        PAYMENT_SYSTEMS_RADIO_INPUTS.shouldBe(CollectionCondition.allMatch("checkPaymentSystemsAlert", elem -> {
            elem.click();
            checkErrors();
            return helpersInit.getBaseHelper().alertIsShow() && helpersInit.getBaseHelper().getTextFromAlert().equals("Incorrect value");
        }));
        return true;
    }

    public boolean checkPaymentsSystemValidationAfterChangeSum(String errorMessage, String correctPaymentValue, String incorrectPaymentValue) {
        PAYMENT_SYSTEMS_RADIO_INPUTS.shouldBe(CollectionCondition.allMatch("checkPaymentsSystemValidationAfterChangeSum", elem -> {
            setPaymentSum(correctPaymentValue);
            elem.click();
            setPaymentSum(incorrectPaymentValue);
            checkErrors();
            return checkMinPaymentSumValidation(errorMessage);
        }));

        return true;
    }

    public void filterCampaignsByCustomValue(String filterValue) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(FILTER_BY_DROPDOWN, filterValue);
        FILTER_BY_APPLY_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public boolean checkTeaserIconTitle(String titleValue) {
        return Objects.equals(TEASER_ICON.getAttribute("title"), titleValue);
    }

    public boolean checkStatisticsIconIsDisplayed() {
        return STATISTICS_LINK_ICON.isDisplayed();
    }

    public boolean checkSelectiveBiddingIconIsDisplayed() {
        return SELECTIVE_BIDDING_LINK_ICON.isDisplayed();
    }

    public boolean checkCopyCampaignIconIsDisplayed() {
        return COPY_CAMPAIGN_LINK_ICON.isDisplayed();
    }

    public boolean checkRestoreCampaignIconIsDisplayed() {
        return RESTORE_CAMPAIGN_LINK_ICON.isDisplayed();
    }

    public boolean checkMediaSourceSelectIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MEDIA_SOURCE_SELECT.isDisplayed());
    }

    public void isNeedToCopyBlocklist(boolean state) {
        copyBlackList = state;
    }

    public void copyCampaignWithBlockList(int campaignId) {
        clickCopyCampaignIcon(campaignId);
        if (copyBlackList) markUnMarkCheckbox(copyBlackList, COPY_BLACK_LIST_CHECKBOX);
        COPY_BLACK_LIST_OK_BUTTON.shouldBe(visible).click();
    }

    public String getCampaignIdByName(String campaignName) {
        return addEditCampaignHelper.getCampaignIdFromList(campaignName);
    }

    public void clickCopyCampaignIcon(int campaignId) {
        $(String.format(COPY_CAMPAIGN_ICON, campaignId)).shouldBe(visible).click();
    }

    public boolean checkCopyBlockListCheckboxIsDisplayed() {
        COPY_BLACK_LIST_OK_BUTTON.shouldBe(visible);
        return COPY_BLACK_LIST_CHECKBOX.isDisplayed();
    }

    public String getCampaignNameFromInput() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_NAME.getValue());
    }

    public String getComplaintPopupMessage() {
        return COMPLIANT_POPUP.text();
    }

    public boolean paymentsIsAvailable() {
        CHECKOUT_BUTTON.shouldBe(visible);
        log.info("Checkout button is loaded!");
        return helpersInit.getBaseHelper().getTextAndWriteLog(CHECKOUT_BUTTON.isDisplayed() &&
                PAYMENT_AMOUNT_INPUT.isDisplayed() &&
                PAYMENT_SUM_FIELD.isDisplayed() &&
                PAYMENT_SYSTEMS_RADIO_INPUTS.size() == 4);
    }

    public boolean idenfyBlockIsVisible() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IDENFY_BLOCK.is(visible));
    }

    public String getSelectedCampaignType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().getSelectedValue(CAMPAIGN_TYPE));
    }

    public boolean idenfyPopupIsDisplayed() {
        return IDENFY_INFORMATION_POPUP.shouldBe(visible).isDisplayed();
    }

    public String getIdenfyPopupText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IDENFY_INFORMATION_POPUP.shouldBe(visible).text());
    }

    public String getIdenfyProfileText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IDENFY_VERIFICATION_TAB_TEXT.shouldBe(visible).text());
    }

    public void submitIdenfyPopup() {
        IDENFY_INFORMATION_POPUP_SUBMIT_BUTTON.click();
    }

    public void closeIdenfyPopup() {
        IDENFY_INFORMATION_POPUP_CLOSE_BUTTON.click();
    }

    public void copyCampaign(int campaignId) {
        clickCopyCampaignIcon(campaignId);
        COPY_BLACK_LIST_OK_BUTTON.shouldBe(visible).click();
    }

    public String getNotificationText() {
        return NOTIFICATION.shouldBe(visible).text();
    }

    public boolean checkNotificationIsDisplayed() {
        return NOTIFICATION.isDisplayed();
    }

    public void clickHereLinkInNotification() {
        NOTIFICATION.find("a:nth-child(1)").click();
    }

    public boolean isStartVerificationButtonDisplayed(){
        return START_VERIFICATION_BUTTON.isDisplayed();
    }
}
