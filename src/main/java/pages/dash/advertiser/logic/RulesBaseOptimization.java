package pages.dash.advertiser.logic;

import core.helpers.statCalendar.CalendarForStatistics;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.ArrayList;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static pages.dash.advertiser.locators.SelectiveBiddingLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.dash.publisher.locators.DirectDemandLocators.CALENDAR_LABEL;

public class RulesBaseOptimization {

    private final HelpersInit helpersInit;
    private final Logger log;

    String amountValue;
    String actionValue;

    public RulesBaseOptimization(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public boolean checkDisplayingAddRuleButton() {
        return OPTIMIZATION_RULES_ADD_RULE_BUTTON.isDisplayed();
    }

    public void clickAddRule() {
        OPTIMIZATION_RULES_ADD_RULE_BUTTON.shouldBe(visible).click();
    }
    public void createRule(String ruleType) {
        amountValue = randomNumberFromRange(1, 50);
        clickAddRule();
        switch (ruleType) {
            case "autoBlocking" -> OPTIMIZATION_RULES_AUTOBLOCKING_RULE_ITEM.click();
            case "autoBidding" -> OPTIMIZATION_RULES_DYNAMIC_BID_RULE_ITEM.click();
            case "fixedBid" -> OPTIMIZATION_RULES_FIXED_BID_RULE_ITEM.click();
        }
        OPTIMIZATION_RULES_NEW_ROW.shouldBe(visible);
        clearAndSetValue(OPTIMIZATION_RULES_AMOUNT_INPUT, amountValue);
        clearAndSetValue(OPTIMIZATION_RULES_INDICATOR_INPUT, amountValue);
        OPTIMIZATION_RULES_SAVE_ICON.click();
        waitForAjax();
        OPTIMIZATION_RULES_SAVE_ICON.shouldNotBe(visible);
        checkErrors();
    }

    public void editRule() {
        amountValue = randomNumberFromRange(51, 100);
        clearAndSetValue(OPTIMIZATION_RULES_AMOUNT_INPUT, amountValue);
        clearAndSetValue(OPTIMIZATION_RULES_INDICATOR_INPUT, amountValue);
        OPTIMIZATION_RULES_SAVE_ICON.shouldBe(visible).click();
        waitForAjax();
        OPTIMIZATION_RULES_SAVE_ICON.shouldNotBe(visible);
        checkErrors();
    }

    public boolean checkRule() {
        return OPTIMIZATION_RULES_AMOUNT_INPUT.has(value(amountValue)) &&
                OPTIMIZATION_RULES_INDICATOR_INPUT.has(value(amountValue)) &&
                checkDisplayingDeleteRuleButton();
    }

    public void deleteRule() {
        OPTIMIZATION_RULES_DELETE_ICON.hover().shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public boolean checkDisplayingDeleteRuleButton() {
        return OPTIMIZATION_RULES_DELETE_ICON.isDisplayed();
    }

    public boolean checkRuleIsDisplayed() {
        OPTIMIZATION_RULES_ADD_RULE_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(OPTIMIZATION_RULES_ROW.isDisplayed());
    }

    public boolean autoBlockingRuleIsDisplayed() {
        return OPTIMIZATION_RULES_AUTOBLOCKING_RULE_ITEM.isDisplayed();
    }

    public boolean autoBiddingRuleIsDisplayed() {
        return OPTIMIZATION_RULES_DYNAMIC_BID_RULE_ITEM.isDisplayed();
    }

    public boolean fixedBidRuleIsDisplayed() {
        return OPTIMIZATION_RULES_FIXED_BID_RULE_ITEM.isDisplayed();
    }

    public boolean checkWidgetSwitchersState(String widgetUid, String switcherState) {
        return $(String.format(ON_OFF_SWITCHER, widgetUid)).shouldBe(visible).has(cssClass(switcherState));
    }

    public boolean checkLockIsVisible(String widgetUid) {
        return $(String.format(ON_OFF_LOCK_ICON, widgetUid)).has(cssClass("rules-based__manual-locked"));
    }

    public void clickSwitcher(String widgetUid) {
        $(String.format(ON_OFF_SWITCHER, widgetUid)).shouldBe(visible).click();
    }

    public String getRulesBasedPopupText() {
        OPTIMIZATION_RULES_CONFIRM_POPUP_OK_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(OPTIMIZATION_RULES_CONFIRM_POPUP.text());
    }

    public void confirmPopup() {
        OPTIMIZATION_RULES_CONFIRM_POPUP_OK_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void addOptimizationRuleSet() {
        OPTIMIZATION_RULES_OPT_RULE_SET_ITEM.click();
        OPTIMIZATION_RULES_ADD_RULE_SET_POPUP.shouldBe(visible);
        clearAndSetValue(OPTIMIZATION_RULES_CONVERSION_COST_INPUT, "1");
        helpersInit.getBaseHelper().selectRandomValSelectListDashJs(OPTIMIZATION_RULES_ADD_RULE_PERIOD_SELECT);
        OPTIMIZATION_RULES_ADD_RULE_SET_POPUP_OK_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public int getRuleSetSize() {
        OPTIMIZATION_RULES_ADD_RULE_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(OPTIMIZATION_RULES_ROWS.size());
    }

    public void copyRules(String optionValue) {
        clickAddRule();
        OPTIMIZATION_RULES_COPY_RULE_ITEM.click();
        OPTIMIZATION_RULES_COPY_POPUP.shouldBe(visible);
        helpersInit.getBaseHelper().selectCustomValue(OPTIMIZATION_RULES_COPY_MULTISELECT, optionValue);
        OPTIMIZATION_RULES_COPY_POPUP_OK_BUTTON.click();
        waitForAjax();
        checkErrors();
        log.info("Rules has been copied");
    }

    public String getRulesCopySuccessPopupText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(OPTIMIZATION_RULES_COPY_CONFIRM_POPUP.shouldBe(visible).text());
    }

    public void openHistoryLog() {
        OPTIMIZATION_RULES_HISTORY_LOG_BUTTON.shouldBe(visible).click();
        waitForAjax();
        OPTIMIZATION_RULES_HISTORY_LOG_POPUP.shouldBe(visible);
    }

    public void closeHistoryLogPopup() {
        OPTIMIZATION_RULES_HISTORY_LOG_POPUP_CLOSE_BUTTON.click();
    }

    public void filterHistoryLogByUid(String uidValue) {
        clearAndSetValue(OPTIMIZATION_RULES_HISTORY_LOG_UID_INPUT, uidValue);
        OPTIMIZATION_RULES_HISTORY_LOG_APPLY_BUTTON.click();
        waitForAjax();
    }

    public void filterHistoryLogByAction() {
        helpersInit.getBaseHelper().selectRandomValSelectListDashJs(OPTIMIZATION_RULES_HISTORY_LOG_ACTION_SELECT);
        OPTIMIZATION_RULES_HISTORY_LOG_APPLY_BUTTON.click();
        waitForAjax();
        actionValue = OPTIMIZATION_RULES_HISTORY_LOG_ACTION_INPUT.text().toLowerCase();
        log.info("Action is " + actionValue);
    }

    public void selectDate() {
        CalendarForStatistics.CalendarPeriods period = CalendarForStatistics.CalendarPeriods.YESTERDAY;
        ArrayList<String> startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);
        CALENDAR_LABEL.shouldBe(visible);
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);
    }

    public int getFilterHistoryLogResultTableSize() {
     return helpersInit.getBaseHelper().getTextAndWriteLog(OPTIMIZATION_RULES_HISTORY_LOG_TABLE_ROW.size());
    }

    public boolean checkFilterHistoryLogByActionResult() {
        return OPTIMIZATION_RULES_HISTORY_LOG_ACTION_FIELD.texts().stream().allMatch(i -> i.toLowerCase().contains(actionValue));
    }

    public boolean checkFilterHistoryLogByUidResult(String uidValue) {
        return OPTIMIZATION_RULES_HISTORY_LOG_UID_FIELD.texts().stream().allMatch(i -> i.contains(uidValue));
    }
}
