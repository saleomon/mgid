package pages.dash.advertiser.logic;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.advertiser.helpers.AudiencesCreateHelper;
import pages.dash.advertiser.helpers.AudiencesListHelper;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.advertiser.locators.AudiencesCreateLocators.*;

public class Audiences {

    private final HelpersInit helpersInit;
    AudiencesCreateHelper audiencesCreateHelper;
    AudiencesListHelper audiencesListHelper;

    private String name, description, expire, audienceType;
    private String audienceId;

    //event
    private String targetEvent, targetJsCode;
    private String pagesToVisit, domainForTracking;
    boolean fulfilmentOfAllConditions;
    LinkedHashMap<String, String> urlData = new LinkedHashMap<>();
    List<Map.Entry<String, String>> targetsData = new ArrayList<>();
    List clickersData = new ArrayList<>();
    List viewersData = new ArrayList<>();

    public Audiences(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        audiencesCreateHelper = new AudiencesCreateHelper(log, helpersInit);
        audiencesListHelper = new AudiencesListHelper(helpersInit);
    }

    public String getAudienceId() {
        return audienceId;
    }

    public Audiences setTargetEvent(String targetEvent) {
        this.targetEvent = targetEvent;
        return this;
    }

    public Audiences setAudienceType(String audienceType) {
        this.audienceType = audienceType;
        return this;
    }

    public Audiences setPagesToVisit(String pagesToVisit) {
        this.pagesToVisit = pagesToVisit;
        return this;
    }

    public Audiences setDomainForTracking(String domainForTracking) {
        this.domainForTracking = domainForTracking;
        return this;
    }

    public Audiences setUrlData(LinkedHashMap<String, String> urlData) {
        this.urlData = urlData;
        return this;
    }

    public Audiences setTargetsData(List<Map.Entry<String, String>> targetsData) {
        this.targetsData = targetsData;
        return this;
    }

    public Audiences setName(String name) {
        this.name = name;
        return this;
    }

    public Audiences setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getTargetEvent() {
        return targetEvent;
    }

    public boolean isFulfilmentOfAllConditions() {
        return fulfilmentOfAllConditions;
    }

    public String getPagesToVisit() {
        return pagesToVisit;
    }

    public String getDomainForTracking() {
        return domainForTracking;
    }

    public String getTargetJsCode() {
        return targetJsCode;
    }

    public LinkedHashMap<String, String> getUrlData() {
        return urlData;
    }

    public List getClickersData() {
        return clickersData;
    }

    public List getViewersData() {
        return viewersData;
    }

    public String createAudienceWithTarget() {
        name = audiencesCreateHelper.setName(name);
        description = audiencesCreateHelper.setDescription(description);
        expire = audiencesCreateHelper.chooseValidityPeriod(expire);
        audiencesCreateHelper.chooseTargetType(audienceType);
        editTarget();

        audiencesCreateHelper.clickCreateAudience();

        audienceId = audiencesListHelper.readAudienceId(name);
        return audienceId;
    }

    public boolean checkAudience() {
        return audiencesCreateHelper.checkName(name) &
                audiencesCreateHelper.checkDescription(description) &
                audiencesCreateHelper.checkValidityPeriod(expire) &
                audiencesCreateHelper.checkTargetsSettings(targetsData);
    }

    public boolean checkClickersTarget() {
        return clickersData.stream().allMatch(el->$("[id*='cusel-scroll-ad_clickers'] [val='" + el + "'][selected]").exists());
    }

    public boolean checkViewersTarget() {
        return viewersData.stream().allMatch(el->$("[id*='cusel-scroll-ad_viewers'] [val='" + el + "'][selected]").exists());
    }

    public void editAudience() {
        name = audiencesCreateHelper.setName(name);
        description = audiencesCreateHelper.setDescription(description);
        expire = audiencesCreateHelper.chooseValidityPeriod(expire);
        editTarget();
        audiencesCreateHelper.clickCreateAudience();
        helpersInit.getBaseHelper().closePopup();

        audiencesListHelper.isAudienceEdit();
    }

    public Audiences clearData() {
        name = null;
        description = null;
        expire = null;
        urlData.clear();
        targetEvent = null;
        targetJsCode = null;
        pagesToVisit = null;
        domainForTracking = null;
        targetsData.clear();
        clickersData.clear();
        viewersData.clear();
        return this;
    }

    public void editTarget() {
        switch (audienceType) {
            case "url" -> {
                urlData = audiencesCreateHelper.setUrlTypeAndValue(urlData);
                fulfilmentOfAllConditions = audiencesCreateHelper.switchFulfilmentOfAllConditions();
            }
            case "event" -> {
                targetEvent = audiencesCreateHelper.setEvent(targetEvent);
                targetJsCode = audiencesCreateHelper.getJsCode();
            }
            case "visited" -> {
                pagesToVisit = audiencesCreateHelper.setPagesToVisit(pagesToVisit);
                domainForTracking = audiencesCreateHelper.setDomainForTracking(domainForTracking);
            }
            case "targets" -> targetsData = audiencesCreateHelper.setTargetsData(targetsData);
            case "ad_clickers" -> clickersData = audiencesCreateHelper.setAdTarget(ADD_CAMPAIGNS_CLICKERS_BUTTON, CLICKERS_CAMPAIGNS_SELECT);
            case "ad_viewers" -> viewersData = audiencesCreateHelper.setAdTarget(ADD_CAMPAIGNS_VIEWER_BUTTON, VIEWERS_CAMPAIGNS_SELECT);
        }
    }

    public boolean deleteAudience(String audienceId) {
        return audiencesListHelper.deleteAudience(audienceId);
    }

    public void deleteAllTargets(){
        DELETE_CLICKERS_FIELDS_BUTTON.filter(visible).asDynamicIterable().forEach(SelenideElement::click);
    }

    public void saveAudience(){
        audiencesCreateHelper.clickCreateAudience();
    }

    public int getAmountOfTargets(){
        return $$(URL_TYPE_STRING_SELECT).size();
    }
}
