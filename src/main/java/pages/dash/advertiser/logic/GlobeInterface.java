package pages.dash.advertiser.logic;

import org.openqa.selenium.Keys;

import java.util.Random;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.advertiser.locators.GlobeLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class GlobeInterface {

    public GlobeInterface(){
    }

    public void showAllGeoRegions(){
        EXPAND_ALL_TREE.shouldBe(visible).click();
    }

    public void clickToRandomGeoRegion(){
        int size = REGIONS_IN_GEO_CHECKBOXES.size();
        int item = new Random().nextInt(size);
        REGIONS_IN_GEO_CHECKBOXES.get(item).click();
    }

    public void setTeaserMaxCpcAndConfirmPrice(String maxCPC){
//        GEO_PRICE_INPUT.clear();
        GEO_PRICE_INPUT.shouldBe(visible).sendKeys(maxCPC);
        GEO_PRICE_INPUT.sendKeys(Keys.ENTER);
        SAVE_MASS_POC_BUTTON.shouldBe(visible).click();
        SAVE_HIGHER_PRICE_CONFIRMATION.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

}
