package pages.dash.advertiser.logic;

import com.codeborne.selenide.CollectionCondition;
import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import pages.cab.publishers.logic.CabNews;
import pages.dash.advertiser.helpers.CreateTeaserHelper;
import pages.dash.advertiser.helpers.EditTeaserHelper;
import pages.dash.advertiser.helpers.ListTeaserHelper;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.BaseHelper.waitForAjaxLoader;
import static core.helpers.ErrorsHelper.checkErrors;
import static libs.hikari.tableQueries.partners.GHits1.selectTeaserByTitle;
import static pages.dash.advertiser.locators.CreateTeaserLocators.*;
import static pages.dash.advertiser.locators.EditTeaserLocators.IMAGE_BLOCK_VIEW;
import static pages.dash.advertiser.locators.EditTeaserLocators.IMAGE_INPUT;
import static pages.dash.advertiser.locators.ListTeaserLocators.FOCAL_POINT_ICON;
import static pages.dash.advertiser.locators.ListTeaserLocators.MANUAL_FOCAL_CANCEL_BUTTON;
import static pages.dash.advertiser.locators.ListTeaserLocators.MANUAL_FOCAL_CONFIRM_BUTTON;
import static pages.dash.advertiser.locators.ListTeaserLocators.MANUAL_FOCAL_POINT_BUTTON;
import static pages.dash.advertiser.locators.ListTeaserLocators.*;

/**
 * base boolean params:
 * - useDiscount() - available fields: productPrice, productOldPrice, productDiscount, productCurrency
 * - isGenerateNewValues() (only edit action) - use only before edit method in case when you called create method before it - true
 * <p>
 * - useDescription() - use this method for default description - true
 * <p>
 * - setPriceForAllRegions()
 * - true(by default) - one price for all region - return data to priceOfClick
 * - false - different price for each regions - return data to Map pricesOfClick
 */
public class Teasers {

    private final HelpersInit helpersInit;
    private final Logger log;

    CreateTeaserHelper createTeaserHelper;
    EditTeaserHelper editTeaserHelper;
    ListTeaserHelper listTeaserHelper;

    private int teaserId;
    private String title;
    private String description;
    private String categoryId;
    private String callToAction;
    private String domain;
    private String image;
    private String productPrice;
    private String productOldPrice;
    private String productDiscount;
    private String productCurrency;
    private String priceOfClick;

    private Double priceOfClickDouble;
    private boolean isPriceForAllRegions = false;
    private boolean isNeedToEditCategory = false;
    private Boolean focalPointState = false;
    private int focalPointLeft;
    private int focalPointTop;
    private boolean needToEditCategory = true;

    public Teasers setNeedToEditImage(boolean needToEditImage) {
        createTeaserHelper.setNeedToEditImage(needToEditImage);
        editTeaserHelper.setIsNeedToEditImage(needToEditImage);
        return this;
    }

    public Teasers isGenerateAutoTitle(boolean useDiscount) {
        this.createTeaserHelper.setIsGenerateAutoTitle(useDiscount);
        return this;
    }

    public Teasers setNeedToEditCategory(boolean needToEditCategory) {
        isNeedToEditCategory = needToEditCategory;
        this.createTeaserHelper.setNeedToEditCategory(needToEditCategory);
        return this;
    }

    public Teasers(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        createTeaserHelper = new CreateTeaserHelper(log, helpersInit);
        editTeaserHelper = new EditTeaserHelper(log, helpersInit);
        listTeaserHelper = new ListTeaserHelper(log, helpersInit);
    }

    public void resetAllVariables() {
        title = null;
        description = null;
        domain = null;
        image = null;
        productPrice = null;
        productOldPrice = null;
        productDiscount = null;
        productCurrency = null;
        priceOfClick = null;
    }

    public int getTeaserId() {
        return teaserId;
    }

    public String getTitle() {
        return title;
    }

    public Teasers setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Teasers setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getCallToAction() {
        return callToAction;
    }

    public Teasers setCallToAction(String callToAction) {
        this.callToAction = callToAction;
        return this;
    }

    public String getDomain() {
        return domain;
    }

    public Teasers setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public Teasers fillDomain(String value) {
        createTeaserHelper.setUrl(value);
        return this;
    }

    public String getImage() {
        return image;
    }

    public Teasers setImage(String image) {
        this.image = image;
        return this;
    }

    public Teasers setPriceOfClick(String priceOfClick) {
        this.priceOfClick = priceOfClick;
        return this;
    }

    public Teasers useDiscount(boolean useDiscount) {
        this.createTeaserHelper.setUseDiscount(useDiscount);
        this.editTeaserHelper.setUseDiscount(useDiscount);
        return this;
    }

    public Teasers isGenerateNewValues(boolean generateNewValues) {
        editTeaserHelper.setGenerateNewValues(generateNewValues);
        return this;
    }
    public Teasers useTitle(boolean useTitle) {
        this.editTeaserHelper.setUseTitle(useTitle);
        return this;
    }

    public Teasers useDescription(boolean useDescription) {
        this.createTeaserHelper.setUseDescription(useDescription);
        this.editTeaserHelper.setUseDescription(useDescription);
        return this;
    }

    public void loadImage(String image) {
        createTeaserHelper.loadImageByLocal(image);
    }

    /**
     * create teaser
     */
    public int createTeaser() {
        image = createTeaserHelper.loadImageByLocal(image);
        indicateFocalPoint(focalPointLeft, focalPointTop);
        description = createTeaserHelper.setDescription(description);
        categoryId = createTeaserHelper.selectCategoryInCreateInterface(categoryId);
        domain = createTeaserHelper.setUrl(domain);
        title = createTeaserHelper.setTitle(title);
        priceOfClick = createTeaserHelper.setPriceForClick(priceOfClick, isPriceForAllRegions);


        // discount block
        createTeaserHelper.useDiscount();
        productPrice = createTeaserHelper.fillProductPrice(productPrice);
        productOldPrice = createTeaserHelper.fillOldProductPrice(productOldPrice);
        productDiscount = createTeaserHelper.setProductDiscount(productDiscount);
        productCurrency = createTeaserHelper.selectCurrency(productCurrency);

        createTeaserHelper.saveButtonAndGoToTeasersList();
        createTeaserHelper.closePopup();
        teaserId = getTeaserIdByTitle(title);
        return teaserId;
    }

    /**
     * edit teaser
     */
    public void editTeaser() {
        title = editTeaserHelper.editTitle(title);
        domain = editTeaserHelper.editUrl(domain);
        image = editTeaserHelper.editImageByLocal(image);
        indicateFocalPoint(focalPointLeft, focalPointTop);
        description = editTeaserHelper.editDescription(description);

        // discount block
        editTeaserHelper.useDiscount();
        productPrice = editTeaserHelper.editProductPrice(productPrice);
        productOldPrice = editTeaserHelper.editOldProductPrice(productOldPrice);
        productCurrency = editTeaserHelper.editCurrency(productCurrency);
        productDiscount = editTeaserHelper.editProductDiscount(productDiscount);

        editTeaserHelper.saveSettings();
    }

    /**
     * check teaser settings in teasers list interface
     */
    public boolean checkTeaserSettingsInListInterface(int teaserId) {
        log.info("check settings teasers in list interface");
        return listTeaserHelper.checkTeaserTitle(teaserId, title) &
                listTeaserHelper.checkTeaserDescription(teaserId, description) &
                listTeaserHelper.checkCallToAction(callToAction) &
                listTeaserHelper.checkTeaserDomain(domain);
    }

    /**
     * is teaser on moderation
     */
    public boolean teaserStatus(Teasers.Statuses status) {
        switch (status) {
            case ON_MODERATION -> {
                return STATUS_TEASERS.texts().stream().allMatch(el -> el.equalsIgnoreCase("on moderation"));
            }
            case NEW -> {
                return STATUS_TEASERS.texts().stream().allMatch(el -> el.equalsIgnoreCase("new")) &&
                        !$x("//a[contains(@class, 'cant-edit')]").exists();
            }
            case ACTIVE -> {
                return STATUS_TEASERS.texts().stream().allMatch(el -> el.equalsIgnoreCase("active")) &&
                        !$x("//a[contains(@class, 'cant-edit')]").exists();
            }
        }
        return false;
    }

    public enum Statuses {
        ON_MODERATION("On moderation"),
        NEW("new"),
        ACTIVE("active");

        Statuses(String statuses) {
        }

        static public CabNews.Statuses getType(String pType) {
            for (CabNews.Statuses type : CabNews.Statuses.values()) {
                if (type.getStatusesValue().equals(pType)) {
                    return type;
                }
            }
            throw new RuntimeException("unknown type");
        }

    }


    /**
     * search teaser by 'title' and get its id
     */
    public boolean checkTeaserSettingsInEditInterface() {
        return editTeaserHelper.checkTitle(title) &&
                editTeaserHelper.checkDescription(description) &&
                editTeaserHelper.checkUrl(domain) &&
                editTeaserHelper.checkProductPrice(productPrice) &&
                editTeaserHelper.checkOldProductPrice(productOldPrice) &&
                editTeaserHelper.checkProductDiscount(productDiscount) &&
                editTeaserHelper.checkProductCurrency(productCurrency);
    }

    /**
     * search teaser by 'title' and get his id
     */
    public int getTeaserIdByTitle(String title) {
        if (listTeaserHelper.isTeaserDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(selectTeaserByTitle(title));
        }
        return 0;
    }

    public boolean isTeaserDisplayed() {
        return  listTeaserHelper.isTeaserDisplayed();
    }

    /**
     * open edit form
     */
    public void openTeasersEditInterface(int teaserId) {
        listTeaserHelper.openTeasersEditInterface(teaserId);
    }

    public String getMessageFromPreviewBlock() {
        return NOTIFICATION_BLOCK.text().trim();
    }

    public void setFocalPoint(boolean isSaveData) {
        MANUAL_FOCAL_POINT_BUTTON.click();
        FOCAL_POINT_ICON.shouldBe(visible);
        sleep(500);
        executeJavaScript("document.getElementById('crosshair').style.left = '" + focalPointLeft + "px'; document.getElementById('crosshair').style.top = '-" + focalPointTop + "px'");
        sleep(500);
        waitForAjax();
        if (isSaveData) MANUAL_FOCAL_CONFIRM_BUTTON.click();
    }

    /**
     * get link for preview in informer
     */
    public String getLinkForPreviewTeaserInWidget() {
        return listTeaserHelper.getLinkForPreviewTeaserInWidget();
    }

    /**
     * check show description input for product
     */
    public boolean showDescriptionInEditFormForProductTeaser() {
        TITLE_INPUT.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(!DESCRIPTION_INPUT.exists());
    }

    /**
     * show push campaign image
     */
    public boolean showPushCampaignImageInListInterface() {
        return listTeaserHelper.showPushCampaignImageInListInterface();
    }

    /**
     * get Status teaser
     */
    public String getStatusTeaser() {
        return listTeaserHelper.getStatusTeaser();
    }

    /**
     * get Title teaser
     */
    public String getTitleTeaser() {
        return listTeaserHelper.getTitleTeaser();
    }

    /**
     * get Cpc teaser
     */
    public String getCpcTeaser() {
        return listTeaserHelper.getCpcTeaser();
    }

    /**
     * get Type teaser
     */
    public String getTypeTeaser() {
        return listTeaserHelper.getTypeTeaser();
    }

    /**
     * get 'Call to action' teaser
     */
    public String getCallToActionTeaser() {
        return listTeaserHelper.getCallToActionTeaser();
    }

    /**
     * Check validations for custom fields in create and edit interface
     */
    public boolean checkErrorSelectionField(String locator, String... isEditInterface) {
        return helpersInit.getBaseHelper().checkErrorSelectionField(locator, isEditInterface);
    }

    /**
     * Unblock teaser if it blocked and block it again
     */
    public void blockTeaserInDashboard() {
        if (listTeaserHelper.checkIsTeaserBlocked()) {
            log.info("Unblock blocked teaser");
            listTeaserHelper.unBlockTeaser();
        }
        log.info("Block teaser in Dashboard");
        listTeaserHelper.blockTeaser();
    }

    public void unBlockTeaser() {
        TEASER_UNBLOCK_ICON.hover().click();
        waitForAjax();
    }

    /**
     * Unblock teaser if it blocked and block it again by Mass actions
     */
    public void blockTeaserInDashByMassActionWithUnblocking() {
        if (listTeaserHelper.checkIsTeaserBlocked()) {
            log.info("Unblock blocked teaser by mass actions");
            listTeaserHelper.unBlockTeaserMassAction();
        }
        log.info("Block teaser in Dashboard (by Mass actions)");
        listTeaserHelper.blockUnblockedTeaserByMassAction();
    }

    /**
     * Reblock teaser in dashboard by mass action
     */
    public void reblockTeaserInDashByMassAction() {
        log.info("Block teaser in Dashboard (by Mass actions)");
        listTeaserHelper.reblockBlockedTeaserByMassAction();
    }

    /**
     * get cpc 'TEASER REACH' block
     */
    public void getPriceForTeaserReachBlock() {
        priceOfClickDouble = createTeaserHelper.getPriceForTeaserReachBlock("mount.jpg");
    }

    /**
     * get cpc after click teaser reach icon
     */
    public boolean checkReachBlockAfterChangeLevel(String level) {
        boolean result = false;
        double high = 0;
        Double newCpc = helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().parseDouble(createTeaserHelper.getCpc()));
        switch (level) {
            case "very_high" -> result = newCpc > high;
            case "high" -> {
                high = newCpc;
                result = high > priceOfClickDouble;
            }
            case "low", "very_low" -> result = newCpc <= priceOfClickDouble;
        }

        return result;
    }

    /**
     * high: click
     */
    public void highCpcIconClick() {
        createTeaserHelper.highCpcIconClick();
    }

    /**
     * low: click
     */
    public void lowCpcIconClick() {
        createTeaserHelper.lowCpcIconClick();
    }

    /**
     * very high: click
     */
    public void veryHighCpcIconClick() {
        createTeaserHelper.veryHighCpcIconClick();
    }

    /**
     * very low: click
     */
    public void veryLowCpcIconClick() {
        createTeaserHelper.veryLowCpcIconClick();
    }

    public boolean checkVisibilityTeaser() {
        return TEASER_BLOCKS.isDisplayed();
    }

    public boolean isActiveCrop(String cropParametr) {
        String srcAttribute = Objects.requireNonNull(IMAGE_BLOCK_VIEW.attr("src"));
        String urlRegex = "\\d{3}x\\d{3}";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(srcAttribute);
        String currentFormat = urlMatcher.find() ? srcAttribute.substring(urlMatcher.start(0), urlMatcher.end(0)) : "";

        switch (cropParametr) {
            case "16-9" -> {
                return $(".popup img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x540") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"), "236") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "420");
            }
            case "3-2" -> {
                return $(".popup img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x640") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"), "280") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "420");
            }
            case "1-1" -> {
                return $(".popup img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x960") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"), "420") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "420");
            }
        }
        return false;
    }

    public void enableCropFormat(String cropParametr) {
        $(".popup [src*='" + cropParametr + "']").click();
        checkErrors();
    }

    public boolean isDisableCrop(String cropParametr) {
        return $(".popup img[src*='" + cropParametr + "_red.png']").isDisplayed();
    }

    public void saveSettingsEdit() {
        editTeaserHelper.saveSettings();
    }

    public void openStockGettyImageForm() {
        STOCK_GETTY_IMAGES_TAB.click();
        waitForAjaxLoader();
    }
    public String getCategoryNameAtGettyImagesSearchForm() {
        return GETTY_IMAGES_SEARCH_PHRASE.text();
    }

    public String getStockGalleryNotification() {
        return GALLERY_NOTIFICATION_BLOCK.text();
    }

    public void deleteGettyImageSearchPhrases() {
        GETTY_IMAGES_PHRASES_DELETE_BUTTON.shouldHave(CollectionCondition.sizeGreaterThan(0));
        GETTY_IMAGE_PHRASES_DELETE_BUTTON.click();
        GETTY_IMAGE_PHRASES_DELETE_BUTTON.shouldBe(hidden);
    }

    public void inputGettyImageSearchPhrase(String phrase) {
        GETTY_IMAGES_SEARCH_INPUT.sendKeys(phrase + Keys.ENTER);
    }

    public boolean isDisplayedImageFrame() {
        return Objects.requireNonNull(IMAGE_FRAME.shouldBe(visible).attr("width")).equals("492") &&
                Objects.requireNonNull(IMAGE_FRAME.shouldBe(visible).attr("height")).equals("277");
    }
    public String getVideoLink() {
        String wholeLink = $("[class='mcimg'] video [src]").attr("src");
        return Objects.requireNonNull(wholeLink).substring(0, wholeLink.indexOf("?"));
    }

    public boolean checkTitleInTeasersListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(listTeaserHelper.checkTeaserTitle(teaserId, title));
    }

    public boolean checkDescriptionInTeasersListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(listTeaserHelper.checkTeaserDescription(teaserId, description));
    }

    public boolean checkTitleInTeasersEditInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(editTeaserHelper.checkTitle(title));
    }

    public boolean checkDescriptionInTeasersEditInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(editTeaserHelper.checkDescription(description));
    }

    public boolean checkReachMetricsInTeasersDailyStat(String checkedColumnName) {
        return TEASERS_DAILY_STATISTICS_TABLE_HEADERS.texts().contains(checkedColumnName);
    }

    public boolean checkReachMetricsInTeasersDailyStatValues(int columnNumber, String checkedColumnValue) {
        return $x(String.format(TEASER_DAILY_STATISTICS_TABLE_VALUES, columnNumber)).text().replaceAll("\\s+","").equals(checkedColumnValue);
    }

    public boolean checkShowAllColumnNames(String[] headers) {
        List<String> columnNames = TEASERS_DAILY_STATISTICS_TABLE_HEADERS.texts();
        boolean state = Arrays.stream(headers).allMatch(columnNames::contains);
        return helpersInit.getBaseHelper().getTextAndWriteLog(state);
    }

    public String getLinkForPreview() {
        return LINK_FOR_PREVIEW_TEASER.attr("href");
    }

    public int checkAmountOfImageBlocks() {
        return IMAGE_BLOCKS_IN_PREVIEW.size();
    }

    public boolean checkLinksOfRedirection(String value) {
        LINKS_OF_REDIRECTIONS.shouldBe(CollectionCondition.allMatch("checkLinksOfRedirection", el->el.getAttribute("href").contains(value)));
        return true;
    }

    public Teasers useManualFocalPoint(boolean usePoint) {
        focalPointState = usePoint;
        return this;
    }

    public Teasers setFocalPoint(int focaLeft, int focalTop) {
        this.focalPointLeft = focaLeft;
        this.focalPointTop = focalTop;
        return this;
    }

    public boolean isActiveCropVideo(String cropParameter) {
        return $("img[src*='" + cropParameter + "_blue.png']").isDisplayed();
    }

    public void cancelManualFocalPoint() {
        MANUAL_FOCAL_CANCEL_BUTTON.click();
        waitForAjax();
        checkErrors();
        MANUAL_FOCAL_CANCEL_BUTTON.shouldBe(hidden);
    }

    public void indicateFocalPoint(int left, int top) {
        if(focalPointState) {
            MANUAL_FOCAL_POINT_BUTTON.click();
            executeJavaScript("document.getElementById('crosshair').style.left = '" + left + "px'; document.getElementById('crosshair').style.top = '-" + top + "px'");
            MANUAL_FOCAL_CONFIRM_BUTTON.click();
            waitForAjax();
        }
    }

    public void indicateFocalPoint(int left, int top, boolean isSaveData) {
        if(focalPointState) {
            MANUAL_FOCAL_POINT_BUTTON.click();
            executeJavaScript("document.getElementById('crosshair').style.left = '" + left + "px'; document.getElementById('crosshair').style.top = '-" + top + "px'");
            if (isSaveData) MANUAL_FOCAL_CONFIRM_BUTTON.click();
            waitForAjax();
        }
    }

    public boolean isDisplayedFocalPointManualButton() {
        return MANUAL_FOCAL_POINT_BUTTON.isDisplayed();
    }

    public boolean isDisplayedFocalPointEditButton() {
        return MANUAL_FOCAL_EDIT_BUTTON.isDisplayed();
    }

    public boolean isDisplayedFocalPointDefaultButton() {
        return MANUAL_FOCAL_DEFAULT_BUTTON.isDisplayed();
    }

    public void clickFocalPointDefaultButton() {
         MANUAL_FOCAL_DEFAULT_BUTTON.click();
    }

    public boolean checkCsrMediumIconIsDisplayed() {
        return CSR_MEDIUM_ICON.isDisplayed();
    }

    public boolean checkCsrHighIconIsDisplayed() {
        return CSR_HIGH_ICON.isDisplayed();
    }

    public String getCsrMediumTitle() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_MEDIUM_ICON.getAttribute("title"));
    }

    public String getCsrHighTitle() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_HIGH_ICON.getAttribute("title"));
    }

    public String getCsrTitle() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_TITLE_FIELD.text());
    }

    public String getCsrText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_TEXT_FIELD.text());
    }

    public boolean checkCsrReachMeterIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_REACH_METER.isDisplayed());
    }

    public String getCsrLinksValue(String linkText) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($x(String.format(CSR_HIGH_SAFETY_LINK, linkText)).getAttribute("href"));
    }

    public String getCsrReachMeterValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CSR_REACH_METER_VALUE_FIELD.text());
    }

    public boolean checkCsrReachMeterNoteIsDisplayed(String noteValue) {
        return CSR_REACH_METER_NOTE_FIELD.isDisplayed() &&
               helpersInit.getBaseHelper().checkDatasetEquals(noteValue, CSR_REACH_METER_NOTE_FIELD.text());
    }

    public void clickGetAutoTitlesButton() {
        GET_AUTO_TITLE_BUTTON.click();
        checkErrors();
    }

    public boolean isDisabledGetAutoTitlesButton() {
        return GET_AUTO_TITLE_BUTTON.has(attribute("disabled"));
    }

    public boolean checkTooltipOfGetAutoTitlesButton(String text) {
        return helpersInit.getBaseHelper().checkDatasetEquals(AUTO_TITLE_TOOLTIP_LABEL.attr("title"), text);
    }

    public String getTextFromTitleField() {
        return TITLE_INPUT.val();
    }

    public boolean isDisplayedGetAutogenerateTitleButton() {
        return GET_AUTO_TITLE_BUTTON.isDisplayed();
    }

    public boolean isDisplayedEditTeaserPopup() {
        return EDIT_TEASER_POPUP.isDisplayed();
    }

    public String getVideoLinkFromTeaserList() {
        return VIDEO_LINK_LOCATOR.attr("src");
    }

    public void clickFirstImageGettyStock() {
        GETTY_IMAGES.first().click();
    }

    public void selectTypeOfFormatTeaserViewBlock(String value) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(FORMAT_VIEW_BLOCK, value);
    }

    public void openUploadImageForm() {
        waitForAjaxLoader();
        UPLOAD_IMAGE_TAB.click();
    }

    public void selectSortFilesForViewBlockBy(String value) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(SORT_BY_FOR_VIEW_BLOCK, value);
    }

    public String getActiveGettyImageMediaPage() {
        return ACTIVE_PAGE_GALLERY.text();
    }

    public void clickNextPageOfGalleryFiles() {
        NEXT_PAGE_GALLERY_BUTTON.click();
    }
    public boolean isDisplayedTeaserVideoBlock() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VIDEO_BLOCK_TEASER_LIST.isDisplayed());
    }

    public String getImageLinkValue() {
        return IMAGE_INPUT.val();
    }
}