package pages.dash.advertiser.logic;

import com.codeborne.selenide.Condition;

import static pages.dash.advertiser.locators.AdvertisersBalanceLocators.GENERATE_INVOICE_BUTTON;
import static core.helpers.ErrorsHelper.checkErrors;

public class AdvertisersBalance {

    public AdvertisersBalance() {
    }

    public void generateInvoice() {
        GENERATE_INVOICE_BUTTON.shouldBe(Condition.visible).click();
        checkErrors();
    }
}
