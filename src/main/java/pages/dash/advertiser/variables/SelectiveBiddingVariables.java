package pages.dash.advertiser.variables;

import java.util.Arrays;
import java.util.List;

public class SelectiveBiddingVariables {
    public static final List<String> sourcesDataIntClicks = Arrays.asList("1", "4", "3", "0", "0");
    public static final List<String> sourcesDataIntSpent = Arrays.asList("0.03", "0.02", "0.02", "0", "0");
    public static final List<String> sourcesDataIntCpc = Arrays.asList("2.5", "0.45", "0.8", "0", "0");
    public static final String campaignId = "1008";


}
