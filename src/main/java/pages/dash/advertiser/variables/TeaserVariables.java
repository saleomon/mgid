package pages.dash.advertiser.variables;

public class TeaserVariables {
    public static final String successImportTextForMgid = "1 ADS HAVE BEEN SUCCESSFULLY ADDED. 0 ADS HAVE NOT BEEN ADDED. DETAILS:…";
    public static final String successImportTextForIdealmedia = "УСПЕШНО СОЗДАНО 1 НОВОСТЕЙ. НЕ УДАЛОСЬ СОЗДАТЬ 0 НОВОСТЕЙ. ПОДРОБНЕЕ";
    public static final String fileNameForImportTeaser = "import_teaser.csv";
    public static final String autocreativeTeaserTitle = "Популярность сайта зависит от его содержания!";
    public static final String autocreativeTeaserDescription = "Качественный, интересный и уникальный контент сайта!";
    public static final String autocreativeNewsTitle = "Популярность сайта зависит от его содержания!";
    public static final String autocreativeNewsDescription = "Качественный, интересный и уникальный контент сайта!";
    public static final String clickPrice = "13";
    public static final String teaserCsrMediumTitle = "Ad %s has <a href=\"https://help.mgid.com/compliance-and-creatives#medium-safety-ranking\" target=\"_blank\">Medium</a> creative safety ranking and this cause a limited potential reach due to most of premium publisher allow only High creative safety ads on their placements";
    public static final String teaserCsrHighTitle = "Ad %s has <a href=\"https://help.mgid.com/compliance-and-creatives#high-safety-ranking\" target=\"_blank\">High</a> creative safety ranking and meets the standards of the majority of our publishers";
    public static final String teaserCsrHighLink = "https://help.mgid.com/compliance-and-creatives#high-safety-ranking";
    public static final String teaserCsrMediumLink = "https://help.mgid.com/compliance-and-creatives#medium-safety-ranking";
    public static final String teaserCsrReadMoreLink = "https://help.mgid.com/compliance-and-creatives";
}
