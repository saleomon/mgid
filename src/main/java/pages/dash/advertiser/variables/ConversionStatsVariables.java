package pages.dash.advertiser.variables;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConversionStatsVariables {
    public  static Map<String, Object> mapAllWidgetsInterest = Stream.of(
            new AbstractMap.SimpleEntry<>("clicks","52"),
            new AbstractMap.SimpleEntry<>("spent", "1.14"),
            new AbstractMap.SimpleEntry<>("conversionCount", "10"),
            new AbstractMap.SimpleEntry<>("conversionCost", "0.83"),
            new AbstractMap.SimpleEntry<>("conversion", "188.02"),
            new AbstractMap.SimpleEntry<>("roi", "154228.93"),
            new AbstractMap.SimpleEntry<>("revenue", "141.0"),
            new AbstractMap.SimpleEntry<>("epc", "2520.76"),
            new AbstractMap.SimpleEntry<>("profit", "139.87"),
            new AbstractMap.SimpleEntry<>("filter1", Arrays.asList("15", "0", "2", "2", "15")),
            new AbstractMap.SimpleEntry<>("filter2", Arrays.asList("mobile Android 6.xx", "desktop Other Desktop OS", "mobile Android 6.xx", "desktop Other Desktop OS", "mobile Android 6.xx")))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    public static final List<String> listSumarryDataInterest = Arrays.asList("52", "10", "0.11", "1.14", "141.00", "12335.96");

    public static final List<String> chartDataInterest = Arrays.asList("10", "0");

    public  static Map<String, Object> mapAllWidgetsBuy = Stream.of(
            new AbstractMap.SimpleEntry<>("clicks","52"),
            new AbstractMap.SimpleEntry<>("spent", "1.15"),
            new AbstractMap.SimpleEntry<>("conversionCount", "3"),
            new AbstractMap.SimpleEntry<>("conversionCost", "0.91"),
            new AbstractMap.SimpleEntry<>("conversion", "12.75"),
            new AbstractMap.SimpleEntry<>("roi", "24706.32"),
            new AbstractMap.SimpleEntry<>("revenue", "141.0"),
            new AbstractMap.SimpleEntry<>("epc", "543.93"),
            new AbstractMap.SimpleEntry<>("filter1", Arrays.asList("1001", "1001")),
            new AbstractMap.SimpleEntry<>("filter2", Arrays.asList("mobile Android 6.xx", "desktop Other Desktop OS")))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    public static final List<String> listSumarryDatatBuy = Arrays.asList("52", "3", "0.38", "1.14", "141.00", "12335.96");

    public static final List<String> chartDataBuy = Arrays.asList("3", "0");

    public  static Map<String, Object> mapAllWidgetsDecision = Stream.of(
            new AbstractMap.SimpleEntry<>("clicks","52"),
            new AbstractMap.SimpleEntry<>("spent", "1.14"),
            new AbstractMap.SimpleEntry<>("conversionCount", "5"),
            new AbstractMap.SimpleEntry<>("conversionCost", "0.32"),
            new AbstractMap.SimpleEntry<>("conversion", "77.19"),
            new AbstractMap.SimpleEntry<>("roi", "154228.93"),
            new AbstractMap.SimpleEntry<>("revenue", "141.0"),
            new AbstractMap.SimpleEntry<>("epc", "2520.76"),
            new AbstractMap.SimpleEntry<>("filter1", Arrays.asList("0", "2", "2", "15", "15")),
            new AbstractMap.SimpleEntry<>("filter2", Arrays.asList("mobile Android 6.xx", "desktop Other Desktop OS", "mobile Android 6.xx", "desktop Other Desktop OS", "mobile Android 6.xx")))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


    public static final List<String> listSumarryDatatDecision = Arrays.asList("52", "5", "0.23", "1.14", "141.00", "12335.96");

    public static final List<String> chartDataDecision = Arrays.asList("5", "0");

}
