package pages.dash.publisher.locators;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BlockedContentLocators {

    public static final SelenideElement BLOCK_POPUP_RADIO   = $(By.name("block_content"));
    public static final SelenideElement BLOCK_POPUP_SAVE    = $("#buttonSave");
    public static final SelenideElement TEASER_TITLE        = $(".heading");
    public static final SelenideElement WIDGET_SELECT = $("#cusel-scroll-widget");
    public static final SelenideElement APPLY_SELECT_BUTTON = $(".block.filters a.apply");
}
