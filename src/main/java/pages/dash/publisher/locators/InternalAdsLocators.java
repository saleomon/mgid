package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class InternalAdsLocators {
    // Filter
    public static final SelenideElement PROMO_SWITCHER_OFF = $(".isPromo .nice-check2__off");
    public static final SelenideElement PROMO_SWITCHER_ON = $(".nice-check2 input:checked + .nice-check2__wrap");
    public static final SelenideElement PROMO_SWITCHER = $(".nice-check2__wrap");

    //Mass action buttons
    public static final SelenideElement FOR_NEWS_MASS_ACTIONS_SELECT_ALL_CHECKBOX = $(".tableFloatingHeaderOriginal .selectAll");
    public static final SelenideElement MASS_ACTION_BLOCK_ICON = $("[data-action-type=block]");
    public static final SelenideElement MASS_ACTION_UNBLOCK_ICON = $("[data-action-type=unblock]");
    public static final SelenideElement MASS_ACTION_DELETE_ICON = $("[data-action-type=delete]");
    public static final SelenideElement POPUP_CONFIRM_OK_BUTTON = $("[id='confirm-ok']");

    //Table with news
    public static final ElementsCollection TEASERS_ROWS = $$("table.table tr.teaser td.td-preview");
    public static final ElementsCollection NEWS_STATUS = $$(".teaser-status-text");
}
