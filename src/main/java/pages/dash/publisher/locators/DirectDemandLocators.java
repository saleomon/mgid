package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class DirectDemandLocators {

    public static final SelenideElement ADD_CREATIVE_BUTTON = $(".direct-demand-new-custom-ad");
    public static final SelenideElement EDIT_ADD_ICON = $(".icon-edit");
    public static final SelenideElement DELETE_ADD_ICON = $(".icon-delete");
    public static final SelenideElement BLOCK_ADD_ICON = $(".block-action");
    public static final SelenideElement NAME_INPUT = $("input[name='name']");
    public static final SelenideElement TITLE_INPUT = $("input[name='title']");
    public static final SelenideElement URL_INPUT = $("input[name='url']");
    public static final SelenideElement GUARANTEED_CLICKS_INPUT = $("input[name='guaranteedClicks']");
    public static final SelenideElement IMAGE_LOCATOR = $("#image");
    public static final SelenideElement IMAGE_MANUAL_BUTTON = $("#manual-control");
    public static final SelenideElement IMAGE_CONFIRM_BUTTON = $("#manual-control-confirm");
    public static final SelenideElement IMAGE_CANCEL_BUTTON = $("#manual-control-cancel");
    public static final SelenideElement IMAGE_EDIT_BUTTON = $("#manual-control-edit");
    public static final SelenideElement IMAGE_DEFAULT_BUTTON = $("#manual-control-default");
    public static final SelenideElement IMAGE_FOCUS = $("#crosshair");
    public static final SelenideElement WHEN_START_INPUT = $(".custom-ad-field-calendar-start");
    public static final SelenideElement WHEN_END_INPUT = $(".custom-ad-field-calendar-end");
    public static final SelenideElement NEXT_START_MONTH_BUTTON = $("[class*='popup start'] [class='right']");
    public static final SelenideElement NEXT_END_MONTH_BUTTON = $("[class*='popup end'] [class='right']");
    public static final ElementsCollection AMOUNT_OF_DAY_FIRST = $$("[id*='custom-ad-c1d_'][class='day']");
    public static final ElementsCollection AMOUNT_OF_DAY_SECOND = $$("[id*='custom-ad-c2d_'][class='day']");
    public static final SelenideElement CHOOSE_ALL_PLACEMENT = $(".custom-ad-checkbox-all");
    public static final SelenideElement SUBMIT_BUTTON = $(".custom-ad-button-submit");
    public static final String ADD_ID_LOCATOR = ".teaser[data-teaser-id]";
    public static final SelenideElement TITLE_FROM_LIST_LABEL = $(".tit");
    public static final SelenideElement NAME_FROM_LIST_LABEL = $(".teaser-name-text");
    public static final SelenideElement GUARANTEED_CLICKS_FROM_LIST_LABEL = $(".grey-text");
    public static final String STATUS_ADD_LABEL = ".teaser-status-text";
    public static final SelenideElement URL_ADD_LIST_LABEL = $(".teaser-link a");
    public static final SelenideElement IMAGE_IN_LIST_LABEL = $(".td-preview img");
    public static final SelenideElement CUSTOM_ADD_ERROR = $(".custom-ad-error");
    public static final SelenideElement SEARCH_ADD_INPUT = $("#search");
    public static final SelenideElement STATUS_FILTER_SELECT = $("#cusel-scroll-status");
    public static final SelenideElement FILTER_BUTTON = $("[class='button green apply']");
    public static final String          TABLE_HEADERS = ".table th";
    public static final SelenideElement CALENDAR_LABEL = $(".calendar");

}
