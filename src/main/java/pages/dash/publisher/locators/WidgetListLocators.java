package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WidgetListLocators {
    public static final String EDIT_RULES_BUTTON = "[data-id='%s'] a[class*='icon-create-widget']";
    public static final SelenideElement ADD_AB_TEST_BUTTON = $("[id='addCloneSubwidgetButton']");
    public static final String ADD_AB_TEST_FROM_SUBWIDGET_BUTTON = "tr[data-child-id='%s'] #addChildSubWidget";
    public static final SelenideElement CLONE_ROW_LOCATOR = $("[data-parent-id][data-child-id]");
    public static final String NAME_OF_CLONED_WIDGET_LOCATOR = "//*[contains(., '%s')]";
    public static final SelenideElement CLONED_WIDGET_ID = $("[data-parent-id][data-child-id]");
    public static final SelenideElement WIDGET_CLONE_NAME = $x("//td[contains(., 'Subwidgets [50%]')]");
    public static final SelenideElement WIDGET_CLONE_MARKER = $x("//td[contains(., 'As a parent')]");
    public static final String SHOW_CHILD_WIDGET_ICON = "[class*='show-child-widgets'][data-parent-id='%s']";
    public static final SelenideElement ADD_NEW_SUBWIDGET_BUTTON = $("[id*=addSubwidgetButton]");
    public static final SelenideElement RULE_TITLE_INPUT = $("[name*=addSubwidgetName]");
    public static final SelenideElement RULE_COUNTRY_INPUT = $x(".//td[*[@name='addSubwidgetGeo']]//input");
    public static final SelenideElement RULE_COUNTRY_SELECT = $x(".//td[*[@name='addSubwidgetGeo']]//*[@class='chosen-results']");
    public static final SelenideElement RULE_TRAFFIC_TYPE_INPUT = $x(".//td[*[@name='addSubwidgetTrafficTypes']]//input");
    public static final SelenideElement SAVE_NEW_RULE = $("[id=addSubwidgetRuleButton]");
    public static final String CLONE_WIDGET_ID_LABEL = ".//tr[*[text()='%s']]";
    public static final String AB_TEST_ID_FROM_SUBWIDGET_LABEL = "tr[data-parent-id='%s']:last-child";
    public static final String SUBWIDGET_lABEL = "tr[data-child-id='%s'] td:nth-child(%s)";
    public static final String PARENT_WIDGETS_TR = ".parent-widget-row";
    public static final String CHILD_WIDGETS_TR = ".child-widget-row";

    public static final String GET_THE_CODE_ICON = "tr[data-id='%s'] [class*=icon-get-code]";
    public static final SelenideElement GET_CODE_POPUP = $("div.popup.popup_get-code");
    public static final SelenideElement WIDGET_CODE_FOR_CLIENT = $x(".//div[2]/div[@class='copy-code']");
    public static final SelenideElement COPY_CODE_ICON = $(".btnCopy");
    public static final SelenideElement WIDGET_INSTANT_ARTICLES_CODE = $x(".//*[contains(text(), 'Instant Articles')]");
    public static final SelenideElement WIDGET_IS_DELETED_FILTER = $("#cusel-scroll-isDeleted");
    public static final SelenideElement WIDGET_CONTRACT_TYPE_FILTER = $("#cusel-scroll-contractsType");
    public static final SelenideElement APPLY_BUTTON = $("#dateFilterSubmit");
    public static final SelenideElement APPLY_FILTER_BUTTON = $("#filterSubmit");
    public static final ElementsCollection WIDGETS_PARENT_LIST = $$(".parent-widget-row");
    public static final ElementsCollection WIDGETS_CHILD_LIST = $$(".child-widget-row");
}
