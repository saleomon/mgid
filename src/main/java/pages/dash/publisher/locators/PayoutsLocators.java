package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class PayoutsLocators {
    //Payouts page
    public static final SelenideElement ADD_PURSE_BUTTON = $("div.plus .button");
    public static final SelenideElement EDIT_PURSE_BUTTON = $("div.edit .button");
    public static final SelenideElement PAYMENT_METHODS_FIELD = $("#cuselFrame-auto_purses");
    public static final SelenideElement PAYMENT_METHODS_SELECT = $("#cusel-scroll-auto_purses");
    public static final SelenideElement SELECTED_PURSE = $x("//*[@id='cuselFrame-auto_purses']/div[@class='cuselText']");
    public static final ElementsCollection STATUS_FIELDS = $$(".status");
    public static final SelenideElement APPLY_DATE_PERIOD_BUTTON = $(".button.green.apply");

    //Add purse popup
    public static final SelenideElement ADD_PURSE_POPUP = $("#addPursesClient");
    public static final SelenideElement CANCEL_BUTTON = $("#confirm-cancel");
    public static final SelenideElement PURSE_TYPE_SELECT = $("#cuselFrame-typePurse");
    public static final SelenideElement PURSE_TYPE_ID_HIDDEN_FIELD = $("#typePurse");
    public static final SelenideElement BANK_TRANSFER_TYPE_SELECT = $("#cuselFrame-bankTransfers");
    public static final SelenideElement BANK_TRANSFER_ID_HIDDEN_FIELD = $("#bankTransfers");
    public static final SelenideElement ACH_BANK_NAME_INPUT = $("#wirePurseBlock [name=bank]");
    public static final SelenideElement BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT = $("#wirePurseBlock [name=swift-code]");
    public static final SelenideElement BENEFICIARYS_ACCOUNT_NAME_INPUT = $("#wirePurseBlock [name=account-name]");
    public static final SelenideElement BENEFICIARYS_ADDRESS_INPUT = $("#wirePurseBlock [name=address]");
    public static final SelenideElement ACCOUNT_NUMBER_IBAN_INPUT = $("#wirePurseBlock [name=account-number]");
    public static final SelenideElement WEBMONEY_WMZ_PURSE_INPUT = $("#wmUsdPurse");
    public static final SelenideElement PAYPAL_PURSE_INPUT = $("#paypalPurse");
    public static final SelenideElement PAXUM_PURSE_INPUT = $("#paxumPurse");
    public static final SelenideElement PAYONEER_PURSE_INPUT = $("#payoneerMyPurse");
    public static final SelenideElement CAPITALIST_PURSE_INPUT = $("#capitalistPurse");
    public static final SelenideElement HOLD_RESUME_PAYMENTS_BUTTON = $("#holdOrResume");
    public static final SelenideElement MAIL_POPUP = $x(".//div[@class = 'popup']");

    //Paymaster24
    public static final SelenideElement PAYMASTER24_PURSE_NUMBER_INPUT = $("#paymaster24Purse");
    public static final SelenideElement PAYMASTER24_NAME_INPUT = $("#paymaster24Name");
    public static final SelenideElement PAYMASTER24_SURNAME_INPUT = $("#paymaster24Surname");
    public static final SelenideElement PAYMASTER24_DATE_OF_BIRTH_INPUT = $("#paymaster24Birthdate");
    public static final SelenideElement PAYMASTER24_COUNTRY_INPUT = $("#paymaster24Country");

    //International bank-to-bank transfer
    public static final SelenideElement INTERNATIONAL_BANK_NAME_INPUT = $("#internationalBTBTransfersBlock #bank");
    public static final SelenideElement INTERNATIONAL_BANK_ADDRESS_INPUT = $("#internationalBTBTransfersBlock #bank-address");
    public static final SelenideElement INTERNATIONAL_BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT = $("#internationalBTBTransfersBlock #swift-code");
    public static final SelenideElement INTERNATIONAL_BENEFICIARYS_ACCOUNT_NAME_INPUT = $("#internationalBTBTransfersBlock #account-name");
    public static final SelenideElement INTERNATIONAL_BENEFICIARYS_ADDRESS_INPUT = $("#internationalBTBTransfersBlock #address");
    public static final SelenideElement INTERNATIONAL_ACCOUNT_NUMBER_IBAN_INPUT = $("#internationalBTBTransfersBlock #account-number");
    public static final SelenideElement COUNTRY_SELECT_NEW = $("#country");
    public static final ElementsCollection CURRENCY_RADIO = $$(".radioCurrency");
    public static final SelenideElement COUNTRY_OF_BIRTH_SELECT = $("#country_of_birth");
    public static final SelenideElement DATE_OF_BIRTH_INPUT = $("#date_of_birth");
    public static final SelenideElement NATIONAL_IDENTITY_NUMBER_INPUT = $("#national_identity_number");
    public static final SelenideElement TAX_ID_INPUT = $("#tax_id");
    public static final SelenideElement IFSC_CODE_INPUT = $("#ifsc_code");
    public static final SelenideElement CNAPS_NUMBER_INPUT = $("#cnaps_number");
    public static final SelenideElement CLABE_NUMBER_INPUT = $("#clabe_number");
    public static final SelenideElement BANK_BRANCH_CODE_INPUT = $("#bank_branch_code");
    public static final SelenideElement CNPJ_CPF_CODE_INPUT = $("#cnpj_cpf_code");
    public static final SelenideElement DNI_RUC_TAX_ID_INPUT = $("#dni_ruc_tax_id");

    public static final SelenideElement POP_UP = $("div.popup.popup__confirmation");
    public static final SelenideElement ALERT_POPUP = $("div#alert-popup");
    public static final SelenideElement POP_UP_CLOSE = $(".popup_close");
    public static final SelenideElement CONFIRM_BUTTON = $("#confirm-ok");
    public static final SelenideElement NOTIFICATION = $("#mgid-notifications .helptip div.text");
}
