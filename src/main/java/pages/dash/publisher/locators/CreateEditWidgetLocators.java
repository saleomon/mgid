package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class CreateEditWidgetLocators {
    public static final SelenideElement WIDGET_TYPE_SELECT = $("#cusel-scroll-type");
    public static final SelenideElement TITLE_SELECT = $("#cusel-scroll-widgetTitle");
    public static final SelenideElement SUB_TYPE_SELECT = $("#cusel-scroll-subType");
    public static final SelenideElement WIDGET_TITLE_INPUT = $("#clientsTitle");
    public static final SelenideElement WEBSITE_NAME = $("#cuselFrame-clientSiteId .cuselText");
    public static final SelenideElement WEBSITE_SELECT = $("#cusel-scroll-clientSiteId");
    public static final SelenideElement OTHER_WIDGET_TITLE_INPUT = $("#customWidgetTitle");
    public static final SelenideElement MOBILE_COLUMNS_SELECT = $("#cusel-scroll-colsMobile");
    public static final SelenideElement COLUMNS_INPUT = $("#cols-visible");
    public static final SelenideElement ROWS_INPUT = $("#rows-visible");
    public static final SelenideElement PASSAGE_IMPRESSIONS_INPUT = $("#passageFrequency-visible");
    public static final SelenideElement ROWS_HIDDEN = $x(".//div[input[@id='rows']]/div[contains(@style, 'block')]");
    public static final SelenideElement COLS_HIDDEN = $x(".//div[input[@id='cols']]/div[contains(@style, 'block')]");
    public static final String TEASERS_SIZE = ".mgline";
    public static final String CURRENT_TEASER_IN_CONSTRUCTOR = "div.mgbox > div.mgline:nth-of-type(%s)";
    public static final String FIRST_TEASER_IN_CONSTRUCTOR = "div.mgbox > div.mgline:nth-of-type(2), div.mgbox div.mgline:first-child";
    public static final String FIRST_IMAGE_IN_CONSTRUCTOR = "div.mgbox > div.mgline:nth-of-type(2) div.mcimg, div.mgbox div.mgline:first-child div.mcimg";
    public static final SelenideElement THEME_INPUT = $("#widget-theme");
    public static final SelenideElement BANNER_COLOR_BUTTON_INPUT = $("#colorButton");
    public static final SelenideElement BANNER_BLUR_INPUT = $("#customGradient");
    public static final SelenideElement BANNER_BLUR_SELECT = $("#cusel-scroll-blur");
    public static final SelenideElement BANNER_COLOR_SWITCH = $x(".//div[input[@id='showWidgetButton']]/div[@class='switch']/div");
    public static final SelenideElement BANNER_BUTTON_EFFECT_SWITCH = $x(".//div[input[@id='showEffectButton']]/div[@class='switch']/div");
    public static final SelenideElement THEME_HIDDEN = $x(".//span[input[@id='widget-theme']]/div[contains(@style, 'block')]");
    public static final SelenideElement GENERAL_TAB = $("div.change .t1");
    public static final SelenideElement DETAILED_TAB = $("#detailStep");
    public static final SelenideElement CONFIRM_BUTTON = $("#confirm-ok");
    public static final SelenideElement POPUP_TITLE_INPUT = $("#popupText");
    public static final SelenideElement POPUP_CAPPING_INPUT = $("#popupFrequency-visible");
    public static final String POPUP_TITLE_LABEL_IN_STAND = ".title-custom-pos>p";
    public static final SelenideElement CATEGORIES_BLOCK = $("#categories");
    public static final SelenideElement CATEGORIES_TUMBLER = $("#use_cat > div");
    public static final String CATEGORIES_INPUT = "#categoryTreeId .fancytree-node:not(.fancytree-has-children) .fancytree-title";

    public static final String CONTENT_TYPES_VISIBLE_LIST = "[id*=adType_]:not(.disabled) div";
    public static final String CONTENT_TYPES_ALL_LIST = "[id*=adType_] div";
    public static final SelenideElement INFINITE_SCROLL_TUMBLER = $x(".//div[input[@id='isInfiniteScroll']]/div[@class='switch']/div");
    public static final SelenideElement STOP_WORDS_CONTAINER = $("[id='stopWords_tagsinput']");
    public static final SelenideElement STOP_WORDS_FORM_INPUT = $("[id='stopWords_tag']");
    public static final SelenideElement STOP_WORD_ERROR = $("#stopWords_tag.not_valid");
    public static final String          STOP_WORD_OPTIONS = "#stopWords_tagsinput>span>span";
    public static final ElementsCollection STOP_WORD_DELETE_BUTTONS = $$(".tag>a");

    // Constructor Locators
    public static final String THEME_STYLE = "[id*=Composite] .mgline:hover .mctitle a";
    public static final String DOMAIN_HOVER_STYLE = "[id*=Composite] .mgline:hover .mcdomain a";
    public static final String THEME_IM_CAROUSEL_STYLE = "[id*=Composite] .mgline:hover";
    public static final String MGBOX_STYLE = "[id*=Composite] .mgbox";
    public static final String MGL_STYLE = "[id*=Composite] .mgl";
    public static final String MGLINE_STYLE = "[id*=Composite] .mgline";
    public static final String MGLINE_HOVER_STYLE = "div.mgline:hover:not(.vrline) div.mcimg";
    public static final String TEXT_ELEMENTS_STYLE = "[id*=Composite] .text-elements";
    public static final String MCIMG_STYLE = "[id*=Composite] img.mcimg";
    public static final String MCIMG_DIV_STYLE = "[id*=Composite] div.mcimg";
    public static final String HOVER_IMAGE_STYLE = "[id*=Composite] .mgbox img.mcimg:hover";
    public static final String IMAGE_INNER_SHADOW_STYLE = "[id*=Composite] div.mgbox .image-container";
    public static final String MCTITLE_A_STYLE = "[id*=Composite] .mctitle [href]";
    public static final String MCTITLE_STYLE = "[id*=Composite] .mctitle";
    public static final String MGHEAD_STYLE = "[id*=Composite] .mghead";
    public static final String MCDESC_A_STYLE = "[id*=Composite] .mcdesc a";
    public static final String MCDOMAIN_STYLE = "[id*=Composite] .mgtobottom .mcdomain";
    public static final String MCDOMAIN_A_STYLE = "[id*=Composite] .mcdomain a, [id*=Composite] .mgtobottom .mcdomain a";
    public static final String MCDOMAIN_TOP_A_STYLE = "[id*=Composite] .mcdomain-top>a";
    public static final String MCDOMAIN_SMART_STYLE = "[id*=Composite] .mcdomain a";
    public static final String MCDOMAIN_NOW_STYLE = "[id*=Composite] .mcdomain_now";
    public static final String MCDOMAIN_ICON_STYLE = "[id*=Composite] .mcdomain_icon";
    public static final String MCPOSITION_STYLE = "[id*=Composite] div.mcimg";
    public static final String MGBUTTOM_MEDIA_STYLE = "[id*=Composite] .mgbottom_media";
    public static final String MGBUTTON_STYLE = ".mg-button";
    public static final String MCMORE_STYLE = ".mcmore";
    public static final String MCMORE_LINK_STYLE = ".mcmore-link";
    public static final String MGBUTTON_SVG_STYLE = ".mg-button>svg";
    public static final String MCIMG_INNER_STYLE = ".mcimg-inner";
    public static final String IMAGE_WITH_TEXT_STYLE = "[id*=Composite] .image-with-text";
    public static final String MGCONTAINER_STYLE = "div.mgcontainer";
    public static final String BRAND_MGLBTN_STYLE = "[id*=Composite] .mglbtn > a";
    public static final String IDEAL_MGLBTN_STYLE = ".image-with-text > .mglbtn > a";
    public static final String LABEL_IMAGE_LOCATOR = "img.mcimgad";
    public static final SelenideElement DISABLED_EDIT_WIDGET_LOCATOR = $("[class='disabled-widg']");

    //In-ARTICLE
    public static final SelenideElement AUTOPLACEMENT_SELECT = $("#cusel-scroll-autoplacement");
    public static final SelenideElement AUTOPLACEMENT_TOOLTIP = $x(".//div[div[input[@id='autoplacement']]]/div[contains(@class, 'pict')]");
    public static final SelenideElement QTIP_CONTENT = $(".qtip-content");

    // MOBILE WIDGET
    public static final SelenideElement TOASTER_TUMBLER = $x(".//div[input[@id='isToaster']]/div[@class='switch']/div");
    public static final SelenideElement DRAG_DOWN_TUMBLER = $x(".//div[input[@id='isDragDown']]/div[@class='switch']/div");
    public static final SelenideElement INTERSTITIAL_TUMBLER = $x(".//div[input[@id='isInterstitial']]/div[@class='switch']/div");
    public static final SelenideElement FREQUENCY_CAPPING_IMPRESSIONS_INPUT = $("#frequencyCappingShow-visible");
    public static final SelenideElement FREQUENCY_CAPPING_MINUTES_INPUT = $("#frequencyCappingTime-visible");
    public static final SelenideElement TOASTER_INACTIVITY_TIME_INPUT = $("#toasterInactivityTime-visible");
    public static final SelenideElement SHOW_AFTER_INTERACTION_INPUT = $("#interstitialAfter-visible");

    //DETAILED SETTINGS
    //HEADLINE
    public static final SelenideElement HEADLINE_FONT_SIZE_INPUT = $("#text-size-visible");
    public static final SelenideElement HEADLINE_FONT_COLOR_INPUT = $("#text-color");

    //DOMAIN
    public static final SelenideElement DOMAIN_SETTINGS_BLOCK = $("#domain-settings");
    public static final SelenideElement DOMAIN_SHOW_TUMBLER = $x(".//div[input[@id='domain']]/div[@class='switch']/div");
    public static final SelenideElement DOMAIN_FONT_SIZE_INPUT = $("#domain-text-size-visible");
    public static final SelenideElement DOMAIN_FONT_COLOR_INPUT = $("#domain-text-color");

    //CAROUSEL
    public static final SelenideElement AUTO_SCROLL_TUMBLER = $("#scrollEffect-switch>div");

    //OTHER
    public static final SelenideElement WIDGET_SAVE_BUTTON = $("#buttonSave");
    public static final SelenideElement POPUP_CLOSE_BUTTON = $(".popup_close");
    public static final SelenideElement NOTIFICATION = $("#mgid-notifications .notification .text");
    public static final SelenideElement POPUP = $("div.popup");
    public static final SelenideElement ADD_ANOTHER_WIDGET_BUTTON = $("#addWidgetButton");
    public static final SelenideElement STAND_SECTION_ID = $("#section");
    public static final String LOGO = "[class*='widgets_logo']>a, [class^='mg_addad']>a";
    public static final SelenideElement MOBILE_BUTTON = $("[for=device_mobile]");
    public static final SelenideElement DESKTOP_BUTTON = $("[for=device_desktop]");

    //IN-SITE NOTIFICATION
    public static final String IN_SITE_NOTIFICATION_CLOSE_ICON = ".mg-close-action";
    public static final SelenideElement IN_SITE_NOTIFICATION_POSITION_SELECT = $("#cusel-scroll-notificationPosition");
    public static final SelenideElement IN_SITE_NOTIFICATION_FREQUENCY_SELECT = $("#cusel-scroll-frequencyOfDisplay");
    public static final SelenideElement IN_SITE_NOTIFICATION_RERUN_ADS_SWITCH = $x(".//div[input[@id='rerunAds']]/div[@class='switch']/div");
    public static final SelenideElement IN_SITE_NOTIFICATION_LABEL_SWITCH = $x(".//div[input[@id='is_show_ad_marker']]/div[@class='switch']/div");
    public static final String LIKE_VALUE = ".mgbtn_media-like .mgmedia__metrics-value";
    public static final By LIKE_POST_BLOCK = By.cssSelector("div[class='mgbottom_media']");

    //PASSAGE
    public static final String PASSAGE_CLOSE_ICON = ".mgclose-btn";

    //INT_EXCHANGE
    public static final SelenideElement INT_EXCHANGE_TUMBLER = $x(".//div[input[@id='is_int_exchange']]/div[@class='switch']/div");
    public static final SelenideElement POSITION_BLOCK = $("#link-positions-block");

    public static final SelenideElement POSITION_BLOCK_DIRECT_DEMAND = $("#link-positions-block-direct_publisher_demand");

    //MONETIZATION
    public static final SelenideElement MONETIZATION_TUMBLER = $x(".//div[input[@id='is_wages']]/div[@class='switch']/div");

    //DIRECT PUBLISHER DEMAND
    public static final SelenideElement DIRECT_PUBLISHER_DEMAND_TUMBLER = $x(".//div[input[@id='is_direct_publisher_demand']]/div[@class='switch']/div");


    //Stand
    public static final String COUNT_LINKS_IN_MGLINE = "div.mgbox > div.mgline:nth-of-type(3) div.mcimg>a";
    public static final SelenideElement STAND_FOOTER = $("#footer");
    public static final SelenideElement STAND_HEADER = $("#header");
    public static final SelenideElement STAND_NAV = $("#nav");
    public static final SelenideElement STAND_NAV_2 = $("#nav_2");
    public static final SelenideElement STAND_P_1 = $("#p1");
    public static final SelenideElement STAND_FRAME_AMP = $(".i-amphtml-fill-content");
    public static final SelenideElement STAND_PAGINATOR_NEXT_BUTTON = $("span[data-page='next']");
    public static final SelenideElement STAND_BACK_BUTTON_CLOSE_BANNER_ICON = $(".mg-additional-popup-close-btn");
    public static final String STAND_MGID_LOGO = "[class^='mg_addad'] img";
    public static final String STAND_IDEALMEDIA_LOGO = "[class^='mg_addad']>a";
    public static final String MGLINE_TEASER_INDEX_NUMBER = ".//div[contains(@class, 'mgline')][%s]";
    public static final String STAND_BANNER_LOGO = ".ad_new_logo img[alt]";
    public static final String STAND_BANNER_SHOW_NAME_LABEL = "div.mgl>a";
    public static final String STAND_BANNER_WIDGET_TITLE_LABEL = "div.mgl2";
    public static final String DEFAULT_JS_DIV = "#div_default_js";
    public static final String DEFAULT_JS_DIV_CHILD = "#div_default_js_child";
    public static final String TEASER_DISCLAIMER_DIV = "//div[@class='disclaimer']";
    public static final String TEASER_CUSTOM_BANNER_FRAME = "#mgBanner_%s_%s_%s";
    public static final SelenideElement TEASER_CUSTOM_BANNER_DIV = $("div.pbs__player");
    //mobile
    public static final String MOBILE_WIDGET_CONTAINER = "#AdWidgetContainer";
    public static final String MOBILE_CLOSE_ICON = "#adwidget-close-action";
    public static final String MOBILE_CONTINUE_BUTTON = "#adwidget-continue-cation";
}
