package pages.dash.publisher.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class PublisherLocators {

    // revenue block
    public static final SelenideElement TOTAL_REVENUE_LABEL = $x(".//div[@class='info' and p[text()='Total revenue']]/span");

    //general
    public static final SelenideElement CALENDAR_LABEL = $(".calendar");
    public static final SelenideElement GRAPH_SELECT = $("#cusel-scroll-graph_selector");
    public static final String          TABLE_HEADERS = ".tableFloatingHeaderOriginal th";
    public static final SelenideElement PAGINATOR_COUNT_LABEL = $(".list>a.item");

    // custom reports
    public static final SelenideElement CUSTOM_REPORTS_OPTIONS_BUTTON = $("[class*='custom-report-options-button']");
    public static final SelenideElement CUSTOM_REPORT_OPTIONS_FORM = $("[class='custom-report-options']");
    public static final SelenideElement CUSTOM_REPORT_APPLY_BUTTON = $("[class*='custom-report-apply']");
    public static final SelenideElement CUSTOM_REPORT_METRICS_FILTERS_SELECT_CONDITIONS = $(".metrics-filters-select-conditions");
    public static final SelenideElement CUSTOM_REPORT_METRICS_FILTERS_SELECT = $(".metrics-filters-select");
    public static ElementsCollection    CUSTOM_REPORT_METRICS_FILTER_OPTIONS = $$(".metrics-filters-select option");
    public static final SelenideElement CUSTOM_REPORT_METRICS_FILTER_INPUT = $(".metrics-filters-input-values");
    public static final SelenideElement CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT = $(".dimensions-filters-select");
    public static final SelenideElement CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT_VALUES = $(".dimensions-filters-select-values");
    public static final SelenideElement CUSTOM_REPORT_DIMENSIONS_FILTERS_INPUT = $("[class*='dimensions-filters-input-values']");
    public static final SelenideElement CUSTOM_REPORT_FILTER_BUTTON = $("[class*='custom-report-filters-button']");
    public static final SelenideElement CUSTOM_REPORT_FILTER_FORM = $(".custom-report-filters");
    public static final SelenideElement CUSTOM_REPORT_PAGINATOR_SELECT_ID = $("#cusel-scroll-cuSel-2");
    public static final SelenideElement CUSTOM_REPORT_INFO_MESSAGE = $(".start-message span");
    public static final SelenideElement CUSTOM_REPORT_REPORT_DATA = $(".report-data[style*='none']");
    public static final String          CUSTOM_REPORT_DATA_COLUMN_IN_TABLE = "[class*='custom-report'] tr:not(.totalRow)>td:not(.space):nth-child(1)";
    public static final SelenideElement CUSTOM_REPORT_CREATE_BUTTON = $("[class*='custom-report-saved-button']");
    public static final SelenideElement CUSTOM_REPORT_NAME_FIELD = $(".custom-report-name-field");
    public static final SelenideElement CUSTOM_REPORT_SAVE_BUTTON = $("[class*='custom-report-saved-form-save']");
    public static final SelenideElement CUSTOM_REPORT_TOTAL_DATA = $(".totalRow");
}
