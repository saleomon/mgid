package pages.dash.publisher.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AdsModerationLocators {

    public static final SelenideElement STATUS_FILTER_SELECT = $("#cusel-scroll-status");
    public static final SelenideElement CATEGORY_FILTER_SELECT = $("#cusel-scroll-category");
    public static final SelenideElement FILTER_BUTTON = $("[class='button green apply']");
    public static final String STATUS_LABEL = ".teaser-status-text";
    public static final String TEASER_ID_LABEL = "tr.teaser";
    public static final String CATEGORY_LABEL = ".category-field";
    public static final SelenideElement TUMBLER_LOADER = $(".switch-loader");
    public static final SelenideElement SEARCH_INPUT = $("#search");

    //ads comment
    public static final SelenideElement AD_COMMENT_POPUP_ICON = $("[class*='comment-btn']");
    public static final SelenideElement AD_COMMENT_FIELD = $("[id='comment-field']");
    public static final SelenideElement AD_COMMENT_ADD_BUTTON = $("[class*='comment-popup-button-submit']");
    public static final SelenideElement AD_COMMENT_CLOSE_BUTTON = $("[class*='comment-popup-button-close']");
    public static final SelenideElement AD_COMMENT_TEXT = $("[class='comment-popup-text']");
}
