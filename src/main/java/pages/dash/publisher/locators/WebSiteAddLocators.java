package pages.dash.publisher.locators;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class WebSiteAddLocators {

    public static final SelenideElement SAVE_WEB_SITE_BUTTON = $(".button.green.save");

    public static final SelenideElement DOMAIN_INPUT = $("#popup_input");

    public static final SelenideElement COMMENT_INPUT = $("#comment");

    public static final SelenideElement CATEGORY_SELECT = $("#category_select_dropdown");

    public static final SelenideElement LANGUAGE_SELECT = $("#cusel-scroll-siteLang");

    public static final SelenideElement LANGUAGE_LABEL = $(By.name("siteLang"));

    public static final SelenideElement UTM_SETTINGS_CHECKBOX = $("#useUtmSettings");

    public static final SelenideElement UTM_SOURCE_INPUT = $("#utmSource");

    public static final SelenideElement UTM_MEDIUM_INPUT = $("#utmMedium");

    public static final SelenideElement UTM_CAMPAIGN_INPUT = $("#utmCampaign");
}
