package pages.dash.publisher.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class WebsiteListLocators {

    //Self register popup MGID
    public static final SelenideElement SETUP_PROFILE_POPUP = $("#self-registered-form");
    public static final SelenideElement SELF_REGISTER_POPUP = $(".self-registered-popup");
    public static final SelenideElement SELF_REGISTER_POPUP_INDIVIDUAL_BUTTON = $(".pre-form-individual-btn");
    public static final SelenideElement SELF_REGISTER_POPUP_LEGAL_ENTITY_BUTTON = $(".pre-form-legal-entity-btn");
    public static final SelenideElement SELF_PROFILE_POPUP_BACK_BUTTON = $(".button_lines_back");

    public static final SelenideElement COUNTRY_SELECT = $("#cuselFrame-billingCountry");
    public static final SelenideElement SAVE_AND_FINISH_BUTTON = $("#submit");

    //Self register popup IDEALMEDIA
    public static final SelenideElement SETUP_PROFILE_POPUP_IDEALMEDIA = $(".popup");
    public static final SelenideElement COUNTRY_SELECT_IDEALMEDIA = $("#billing_country");
    public static final SelenideElement POLICY_CHECKBOX_LABEL = $x("//label[@for='accept']");
    public static final SelenideElement CONFIRM_BUTTON = $("#confirm-ok");

    public static final SelenideElement USERS_LOGIN_FIELD = $(".user p");

    public static final SelenideElement ADD_WEBSITE_BUTTON = $(".add-site");
    public static final SelenideElement POPUP_LOCATOR = $("[class='popup']");
    public static final SelenideElement POPUP_CLOSE =  $(".popup_close");
    public static final String          MODERATION_BUTTON_CLASS = "[class*='send_moderation icon-to-moderate']";

    /**
     * Ads.txt popup
     */
    public static final SelenideElement ADS_TXT_BUTTON = $("#adsTxtContent");
    public static final SelenideElement SITES_ADS_TXT_POPUP = $("#sites-ads-txt");
    public static final SelenideElement SITES_ADS_TXT_POPUP_CLOSE_BUTTON = $("#sites-ads-txt .popup_close");
    public static final SelenideElement ADS_TXT_CONTENT_UPDATE_POPUP = $("#adsTxtContentUpdate");
    public static final SelenideElement ADS_TXT_CONTENT_UPDATE_POPUP_CLOSE_BUTTON = $("#adsTxtContentUpdate .popup_close");
    public static final String ADS_TXT_POPUP_DOMAIN_CONTENT_FIELD = ".//div[@id='copy-%s']";
    public static final String ADS_TXT_POPUP_LINE_FIELD = ".//div[@id='copy-%s']/span[contains(text(),'%s')]";

    public static final SelenideElement DIRECT_DEMAND_BUTTON = $("a[href='/publisher/direct-demand']");
}
