package pages.dash.publisher.locators.news;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class EditNewsLocator {
    public static final SelenideElement URL_INPUT = $(".popup #url");
    public static final SelenideElement TITLE_INPUT = $(".popup #title");
    public static final SelenideElement DESCRIPTION_INPUT = $(".popup #advert_text");
    public static final SelenideElement CROP = $(".jcrop-tracker");
    public static final SelenideElement CATEGORY_SELECT = $("#category_select_dropdown");
    public static final SelenideElement SAVE_SETTINGS_BUTTON = $(".popup .button.save");
}
