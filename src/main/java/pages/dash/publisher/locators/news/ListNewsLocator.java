package pages.dash.publisher.locators.news;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ListNewsLocator {
    public static final String NEWS_ITEM = "tr[data-teaser-id='%s'] td.td-preview";

    public static final SelenideElement IMPORT_NEWS_BUTTON = $("a.import");
    public static final SelenideElement IMPORT_NEWS_LOAD_BUTTON = $(".popup #news-import-file-name");
    public static final SelenideElement IMPORT_NEWS_SAVE_FORM = $(".popup #news-import-file-save");
    public static final SelenideElement IMPORT_NEWS_LOAD_INDICATOR = $(".popup .progress-indicator-img");
    public static final SelenideElement NOTIFICATION_TEXT = $(".notification .text");
    public static final SelenideElement NEWS_ID_LOCATOR = $x(".//*[@class='teaser' and @data-teaser-id]");
    public static final ElementsCollection NEWS_TITLE_FIELDS = $$("div.tit");
    public static final ElementsCollection NEWS_TEXT_FIELDS = $$("div.text");
}
