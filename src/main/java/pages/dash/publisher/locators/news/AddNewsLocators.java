package pages.dash.publisher.locators.news;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AddNewsLocators {
    public static final SelenideElement LINK_INPUT = $("#url");
    public static final SelenideElement TITLE_INPUT = $("#title");
    public static final SelenideElement DESCRIPTION_INPUT = $("#advert_text");
    public static final SelenideElement IMAGE_LOCATOR = $("#image");
    public static final SelenideElement SAVE_BUTTON = $(".save");
    public static final SelenideElement POPUP_CLOSE_BUTTON = $(".popup_close");
}