package pages.dash.publisher.variables;

public class PublisherWebSiteVariables {
    public static String adsTxtPopupDefaultRowMgid = "mgid.com, 2021, DIRECT, d4c29acad76ce94f";

    public static String adsTxtPopupHeaderText = "Dear Partner, due to the requirements of Coalition for Better Ads, please update your ads.txt file with these lines. If you have any questions, contact your personal account manager.";

    public static String adsTxtMissingLines = "mgid.com, 2021, DIRECT, d4c29acad76ce94f";
    public static String adsTxtSitesContent = "mgid.com, 2021, DIRECT, d4c29acad76ce94f";
    public static String adsTxtNetworksContent = "google.com, pub-2603664881560000, DIRECT, f08c47fec0942fa0";
}
