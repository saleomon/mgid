package pages.dash.publisher.variables;

public class CreateEditWidgetVariables {

    public static final String otherWidgetTitleValue = "other Title";
    public static final String autoplacementOffValue = "By including this option, the widget placement will be automatically chosen on your web page";
    public static final String autoplacementOffValueRu = "Включив этот параметр, размещение виджета на вашей веб-странице будет выбрано автоматически";
    public static final String autoplacementOffValueUa = "Увімкнувши цей параметр, розміщення віджета на вашій вебсторінці буде вибрано автоматично";
    public static final String autoplacementOnValue = "The page must include an article tag and the content height must be at least 300px";
    public static final String autoplacementOnValueRu = "Необходимо чтобы страница имела тег article и высота контента была не менее 300px";
    public static final String autoplacementOnValueUa = "Необхідно, щоб сторінка мала тег article і висота контенту була не менше 300px";
    public static final String autoplacementAttentionBasedValue = "This option helps you to improve widget CTR by auto placing a widget in the best possible place, based on currently visible content by the user and user’s common attention patterns";
    public static final String autoplacementAttentionBasedValueRu = "Эта опция помогает улучшить CTR за счет автоматического определения положения виджета в статье на основании пользовательского внимания";
    public static final String autoplacementAttentionBasedValueUa = "Ця опція допомогає покращити CTR завдяки автоматичному відображенню положення реклами у статті згідно з аналізом уваги користувача";
    public static final String[] styles = {"bold", "italic", "underline"};

    /**
     * Массив HEX-цветов
     */
    public static final String[] colorMass = {"#FFFAFA", "#F8F8FF", "#F5F5F5", "#DCDCDC", "#FFFAF0", "#Fdf5E6", "#FAF0E6", "#FAEBD7", "#FFEFD5", "#FFEBCD", "#FFE4C4", "#FFDAB9", "#FFDEAD",
            "#912CEE", "#7D26CD", "#551A8B", "#AB82FF", "#9F79EE", "#5D478B", "#FFE1FF", "#EED2EE", "#CDB5CD", "#8B7B8B", "#1C1C1C", "#363636", "#4F4F4F", "#696969", "#828282",
            "#9C9C9C", "#B5B5B5", "#CFCFCF", "#E8E8E8", "#A9A9A9", "#8B008B", "#90EE90"};

    public static final String widgetDataMgid = "widgets_settings_mgid";
    public static final String widgetDataAdskeeper = "widgets_settings_adskeeper";
    public static final String widgetDataIdealmediaIo = "widgets_settings_idealmedia_io";
    public static final String fffColor = "#fff|#fefefe|#fdfdfd|#fafafa";
}
