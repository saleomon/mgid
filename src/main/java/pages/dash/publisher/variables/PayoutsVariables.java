package pages.dash.publisher.variables;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PayoutsVariables {
    public static final String holdPaymentsButtonText = "Hold payments";
    public static final String resumePaymentsButtonText = "Resume payments";
    public static final String subjectLetterMgid = "Payment details change in your Mgid Personal Account";
    public static final String subjectLetterAdskeeper = "Payment details change in your Adskeeper Personal Account";
    public static final String addPaymentEmailPopupText = "The request to add new payment details has been completed. " +
            "We sent a confirmation link to your email. Your wallet will be updated in the system after confirmation. " +
            "The link is valid for 1 hour.";
    public static final String addPaymentEmailPopupTextUkrainian = "Запит на додавання нових реквізитів виконано. " +
            "Посилання для підтвердження надіслано на ваш email. Після підтвердження гаманець буде доступний у системі. " +
            "Посилання дійсне протягом однієї години";
    public static final String subjectLetterIdealmedioIo = "Зміна банківських реквізитів в Особистому Кабінеті Idealmedia.io";
    public static final String emailPaymentPopupSuccessText = "Bank details have been successfully changed";
    public static final String holdPaymentsPopupText = "Are you sure you want to pause payments? This will cancel" +
            " your pending and approved payment requests and forbid the creation of a new payment request on the 1st" +
            " of the next month. Be sure to release payments before the end of the month in order to get paid by" +
            " the end of the next month.";
    public static final String resumePaymentsPopupText = "Are you sure you want to resume payments? This will allow " +
            "the creation of a new payment request on the 1st of the next month. Be sure to release payments before " +
            "the end of the month in order to get paid by the end of the next month.";

    public static final String identifyPaymentsPopupTextForIndividualClient = "In order to continue working, you need to confirm your identity. Are you ready to be verified?\n" +
            "To confirm the verification, the iDenfy service is used\n" +
            "OK\n" +
            "CANCEL";

    public static final String identifyPaymentsPopupTextForLegalEntity = "You are registered as a Legal Entity. You need to sign a contract. To sign the contract, contact a personal manager.\n" +
            "- For Direct Website sokwages, sokwages@ex.ua\n" +
            "CLOSE";

    public static final Map<String, String> customCountriesMap = Stream.of(new Object[][]{
            {"94", "DOMINICAN REPUBLIC"},
            {"21", "TURKEY"},
            {"52", "INDIA"},
            {"153", "NIGERIA"},
            {"137", "MOROCCO"},
            {"1", "UKRAINE"},
            {"26", "CHINA"},
            {"61", "PAKISTAN"},
            {"46", "MEXICO"},
            {"45", "BRAZIL"},
            {"198", "ZIMBABWE"},
            {"159", "PERU"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    public static final String changeFavoriteWalletBlocked = "YOU CANNOT EDIT PAYMENT REQUEST AFTER THE REQUEST " +
            "HAS BEEN APPROVED. TO CANCEL THIS PAYMENT, YOU COULD SET PAYMENTS ON HOLD OR CONTACT OUR SUPPORT TEAM" +
            " AND EXPLAIN THE ISSUE.";

    public static final String cnpjCode = "76.732.182/0001-57";
    public static final String cpfCode = "688.335.182-56";

}
