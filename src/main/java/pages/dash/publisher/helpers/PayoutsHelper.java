package pages.dash.publisher.helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.PayoutsLocators.*;
import static pages.dash.publisher.variables.PayoutsVariables.customCountriesMap;
import static core.helpers.BaseHelper.*;

public class PayoutsHelper {
    private final Logger log;
    private final HelpersInit helpersInit;

    public PayoutsHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Opens add purse popup
     */
    public void openAddPursePopup() {
        log.info("Click 'ADD NEW PAYMENT METHOD' button");
        clickInvisibleElementJs(ADD_PURSE_BUTTON.shouldBe(visible));
        CANCEL_BUTTON.shouldBe(visible, ofSeconds(6));
        log.info("AddPursePopup is opened");
    }

    /**
     * choose type purse from select 'Select type purses'
     */
    public void selectPurseType(String purseType) {
        switch (purseType) {
            case "btb-transfer", "175" -> helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(PURSE_TYPE_SELECT, purseType);
            case "PayPal", "Paxum", "Webmoney", "Paymaster24", "Payoneer", "Capitalist" -> helpersInit.getBaseHelper().selectCurrentTextInSelectListJS(PURSE_TYPE_SELECT, purseType);
        }
    }

    /**
     * choose bank transfers type
     */
    public void selectBankTransfers(String typeTransferName) {
        BANK_TRANSFER_TYPE_SELECT.shouldBe(exist);
        if (BANK_TRANSFER_TYPE_SELECT.find(By.xpath(".//span[contains(text(), '" + typeTransferName + "')]")).exists()) {
            helpersInit.getBaseHelper().selectCurrentTextInSelectListJS(BANK_TRANSFER_TYPE_SELECT, typeTransferName);
            log.info("selectBankTransfers: " + typeTransferName);
            return;
        }
        log.info("this bank transfer already added: " + typeTransferName);
    }

    /**
     * set 'Bank name'
     */
    public String setBankName() {
        String value = "ACH_B2B_transfer_" + BaseHelper.getRandomWord(3);
        sendKey(ACH_BANK_NAME_INPUT, value);
        return value;
    }

    /**
     * set 'Bank wire/ACH routing number or Bank swift code'
     */
    public String setBankWireAchSwiftCode() {
        String value = "ACH_B2B_transfer_swift_code_" + BaseHelper.getRandomWord(3);
        sendKey(BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT, value);
        return value;
    }

    /**
     * set 'Beneficiary's account name'
     */
    public String setBeneficiarysAccountName() {
        String value = "ACH_B2B_transfer_account_name_" + BaseHelper.getRandomWord(3);
        sendKey(BENEFICIARYS_ACCOUNT_NAME_INPUT, value);
        return value;
    }

    /**
     * set 'Beneficiary's address'
     */
    public String setBeneficiarysAddress() {
        String value = "ACH_B2B_transfer_address_" + BaseHelper.getRandomWord(3);
        sendKey(BENEFICIARYS_ADDRESS_INPUT, value);
        return value;
    }

    /**
     * set 'Account number and IBAN (if bank provides it)'
     */
    public String setAccountNumberIban() {
        String value = "ACH_B2B_transfer_account_" + BaseHelper.getRandomWord(3);
        sendKey(ACCOUNT_NUMBER_IBAN_INPUT, value);
        return value;
    }

    /**
     * check type purse in select payment method
     */
    public boolean checkPursesTypeInSelect(String purse) {
        log.info("Selecting added purse");
        helpersInit.getBaseHelper().refreshCurrentPage();
        helpersInit.getBaseHelper().selectCurrentTextInSelectListJS(PAYMENT_METHODS_SELECT, purse);
        waitForAjax();
        log.info("checkPursesTypeInSelect:" + SELECTED_PURSE.shouldBe(visible).scrollTo().getText() + " == " + purse);
        return helpersInit.getBaseHelper().getTextAndWriteLog(SELECTED_PURSE.shouldBe(visible).getText().toLowerCase().contains(purse.toLowerCase()) ||
                SELECTED_PURSE.shouldBe(visible).innerText().toLowerCase().contains(purse.toLowerCase()));
    }

    /**
     * open edit purses popUp
     */
    public void openEditPursePopup() {
        log.info("Click 'EDIT PAYMENT METHOD' button");
        clickInvisibleElementJs(EDIT_PURSE_BUTTON.shouldBe(visible));
        log.info("openEditPursePopUp");
        CANCEL_BUTTON.shouldBe(visible, ofSeconds(10));
    }

    /**
     * check 'Bank name'
     */
    public boolean checkBankName(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(name, ACH_BANK_NAME_INPUT.val());
    }

    /**
     * check 'Bank wire/ACH routing number or Bank swift code'
     */
    public boolean checkBankWireAchSwiftCode(String swiftCode) {
        return helpersInit.getBaseHelper().checkDatasetEquals(swiftCode, BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT.val());
    }

    /**
     * check 'Beneficiary's account name'
     */
    public boolean checkAccountNameWire(String accountName) {
        return helpersInit.getBaseHelper().checkDatasetEquals(accountName, BENEFICIARYS_ACCOUNT_NAME_INPUT.val());
    }

    /**
     * check 'Beneficiary's address'
     */
    public boolean checkTransferAddressWire(String transferAddress) {
        return helpersInit.getBaseHelper().checkDatasetEquals(transferAddress, BENEFICIARYS_ADDRESS_INPUT.val());
    }

    /**
     * check 'Account number and IBAN (if bank provides it)'
     */
    public boolean checkTransferAccountWire(String transferAccounts) {
        return helpersInit.getBaseHelper().checkDatasetEquals(transferAccounts, ACCOUNT_NUMBER_IBAN_INPUT.val());
    }

    /**
     * check 'Bank name' for International bank-to-bank transfers
     */
    public boolean checkInternationalBankName(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(name, INTERNATIONAL_BANK_NAME_INPUT.val());
    }

    /**
     * check 'Bank address' for International bank-to-bank transfers
     */
    public boolean checkInternationalBankAddress(String address) {
        return helpersInit.getBaseHelper().checkDatasetEquals(address, INTERNATIONAL_BANK_ADDRESS_INPUT.val());
    }

    /**
     * check 'Bank wire/ACH routing number or Bank swift code' for International bank-to-bank transfers
     */
    public boolean checkInternationalBankWireAchSwiftCode(String swiftCode) {
        return helpersInit.getBaseHelper().checkDatasetEquals(swiftCode, INTERNATIONAL_BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT.val());
    }

    /**
     * check 'Beneficiary's account name' for International bank-to-bank transfers
     */
    public boolean checkInternationalBeneficiarysName(String accountName) {
        return helpersInit.getBaseHelper().checkDatasetEquals(accountName, INTERNATIONAL_BENEFICIARYS_ACCOUNT_NAME_INPUT.val());
    }

    /**
     * check 'Beneficiary's address' for International bank-to-bank transfers
     */
    public boolean checkInternationalBeneficiarysAddress(String transferAddress) {
        return helpersInit.getBaseHelper().checkDatasetEquals(transferAddress, INTERNATIONAL_BENEFICIARYS_ADDRESS_INPUT.val());
    }

    /**
     * check 'Account number and IBAN (if bank provides it)' for International bank-to-bank transfers
     */
    public boolean checkInternationalAccountNumberIban(String transferAccounts) {
        return helpersInit.getBaseHelper().checkDatasetEquals(transferAccounts, INTERNATIONAL_ACCOUNT_NUMBER_IBAN_INPUT.val());
    }

    /**
     * check country for International bank-to-bank transfers
     */
    public boolean checkInternationalTransferCountry(String[] transferCountry) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                helpersInit.getBaseHelper().checkDatasetEquals(transferCountry[0], COUNTRY_SELECT_NEW.getSelectedValue()) &&
                        helpersInit.getBaseHelper().checkDatasetEquals(transferCountry[1], COUNTRY_SELECT_NEW.getSelectedText()));
    }

    /**
     * check currency for International bank-to-bank transfers
     */
    public boolean checkInternationalTransferCurrency(String[] value) {
        return helpersInit.getBaseHelper().checkDatasetEquals(value[1], CURRENCY_RADIO.get(Integer.parseInt(value[0])).closest("label").text());
    }

    /**
     * Set purse number or account
     */
    public String setPurseNumber(String purseType, String purseNumberValue) {
        switch (purseType) {
            case "PayPal" -> {
                purseNumberValue = "SOK_AUTOTEST" + randomNumberFromRange(1000, 9999) + "@TEST.COM";
                sendKey(PAYPAL_PURSE_INPUT, purseNumberValue);
            }
            case "Payoneer" -> {
                purseNumberValue = "SOK_AUTOTEST" + randomNumberFromRange(1000, 9999) + "@TEST.COM";
                sendKey(PAYONEER_PURSE_INPUT, purseNumberValue);
            }
            case "Capitalist" -> {
                purseNumberValue = "U" + randomNumberFromRange(10000000, 99999999);
                sendKey(CAPITALIST_PURSE_INPUT, purseNumberValue);
            }
            case "Webmoney" -> sendKey(WEBMONEY_WMZ_PURSE_INPUT, purseNumberValue);
            case "Paxum" -> {
                purseNumberValue = "SOK_AUTOTEST" + randomNumberFromRange(1000, 9999) + "@TEST.COM";
                sendKey(PAXUM_PURSE_INPUT, purseNumberValue);
            }
            case "Paymaster24" -> sendKey(PAYMASTER24_PURSE_NUMBER_INPUT, purseNumberValue);
        }
        return purseNumberValue;
    }

    /**
     * choose random wallet in auto purse selectList
     */
    public String selectAutoPurseWallet() {
        PAYMENT_METHODS_SELECT.scrollTo().findAll("span").asFixedIterable().forEach(e ->
                e.shouldNotBe(attribute("class", "disabled")));
        return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(PAYMENT_METHODS_SELECT);
    }

    /**
     * Check 'Hold/Resume payments' button
     */
    public boolean checkHoldResumePaymentsButton() {
        EDIT_PURSE_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(HOLD_RESUME_PAYMENTS_BUTTON.isDisplayed());
    }

    /**
     * Check 'Resume payments' button
     */
    public boolean checkResumePaymentsButton(String buttonTextValue) {
        return checkHoldResumePaymentsButton()
                && HOLD_RESUME_PAYMENTS_BUTTON.shouldBe(
                attribute("class", "back button payouts-blue"), ofSeconds(10))
                .attr("class").equals("back button payouts-blue")
                && HOLD_RESUME_PAYMENTS_BUTTON.text().equalsIgnoreCase(buttonTextValue);
    }

    /**
     * Check 'Hold payments' button
     */
    public boolean checkHoldPaymentsButton(String buttonTextValue) {
        return checkHoldResumePaymentsButton()
                && HOLD_RESUME_PAYMENTS_BUTTON.shouldBe(
                attribute("class", "back button payouts-red"), ofSeconds(10))
                .attr("class").equals("back button payouts-red")
                && HOLD_RESUME_PAYMENTS_BUTTON.text().equalsIgnoreCase(buttonTextValue);
    }

    /**
     * Click 'Hold/Resume payments' button
     */
    public void clickHoldResumePaymentsButton() {
        log.info("Click Hold/Resume payments button");
        clickInvisibleElementJs(HOLD_RESUME_PAYMENTS_BUTTON.shouldBe(visible));
        POP_UP_CLOSE.shouldBe(visible, ofSeconds(6));
    }

    /**
     * Check text in popup about on mail confirmation and close it
     */
    public boolean checkAndCloseMailPopup(String popupText) {
        MAIL_POPUP.shouldBe(visible, ofSeconds(20));
        if (helpersInit.getBaseHelper().checkDatasetEquals(popupText, MAIL_POPUP.shouldBe(visible).text())) {
            POP_UP_CLOSE.click();
            return helpersInit.getBaseHelper().getTextAndWriteLog(true);
        } else return helpersInit.getBaseHelper().getTextAndWriteLog(false);
    }

    /**
     * Check text in confirmation popup
     */
    public boolean checkConfirmationPopupText(String holdPaymentsPopupText) {
        CONFIRM_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().checkDatasetContains(holdPaymentsPopupText, POP_UP.text());
    }

    /**
     * Get payments status
     */
    public List<String> getPaymentsStatus() {
        helpersInit.getBaseHelper().refreshCurrentPage();
        APPLY_DATE_PERIOD_BUTTON.scrollTo();
        return STATUS_FIELDS.texts();
    }

    /**
     * Apply date period
     */
    public void clickApplyDateButton() {
        clickInvisibleElementJs(APPLY_DATE_PERIOD_BUTTON.shouldBe(visible));
        ADD_PURSE_BUTTON.shouldNotBe(visible);
    }

    /**
     * Get payout types list from select
     */
    public List<String> getPursesTypeList() {
        PURSE_TYPE_SELECT.shouldBe(visible).click();
        return PURSE_TYPE_SELECT.findAll("span").texts();
    }

    /**
     * Get bank transfer types list from select
     */
    public List<String> getBankTransfersList() {
        BANK_TRANSFER_TYPE_SELECT.shouldBe(visible).click();
        return BANK_TRANSFER_TYPE_SELECT.findAll("span").texts();
    }

    /**
     * Set 'Bank name' for International bank-to-bank transfers
     */
    public String setInternationalBankName() {
        String value = "International_B2B_transfer_" + BaseHelper.getRandomWord(3);
        sendKey(INTERNATIONAL_BANK_NAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set 'Bank name' for International bank-to-bank transfers
     */
    public String setInternationalBankAddress() {
        String value = "International B2B address " + BaseHelper.getRandomWord(5);
        sendKey(INTERNATIONAL_BANK_ADDRESS_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set 'Bank wire/ACH routing number or Bank swift code' for International bank-to-bank transfers
     */
    public String setInternationalBankWireAchSwiftCode() {
        String value = helpersInit.getBaseHelper().randomNumbersString(6) + BaseHelper.getRandomWord(3);
        sendKey(INTERNATIONAL_BANK_WIRE_ACH_ROUTING_NUMBER_SWIFT_CODE_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set 'Beneficiary's account name' for International bank-to-bank transfers
     */
    public String setInternationalBeneficiarysAccountName() {
        String value = "International B2B transfer account name " + BaseHelper.getRandomWord(3);
        sendKey(INTERNATIONAL_BENEFICIARYS_ACCOUNT_NAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set 'Beneficiary's address' for International bank-to-bank transfers
     */
    public String setInternationalBeneficiarysAddress() {
        String value = "International B2B transfer address " + BaseHelper.getRandomWord(3);
        sendKey(INTERNATIONAL_BENEFICIARYS_ADDRESS_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set 'Account number and IBAN (if bank provides it)' for International bank-to-bank transfers
     */
    public String setInternationalAccountNumberIban() {
        String value = BaseHelper.getRandomWord(2).toUpperCase() + helpersInit.getBaseHelper().randomNumbersString(30);
        sendKey(INTERNATIONAL_ACCOUNT_NUMBER_IBAN_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set random currency for International bank-to-bank transfers
     */
    public String[] setInternationalTransferCurrency() {
        int randomIndex = randomNumbersInt(CURRENCY_RADIO.size());
        CURRENCY_RADIO.get(randomIndex).shouldBe(visible).setSelected(true);
        String currency = CURRENCY_RADIO.get(randomIndex).closest("label").text();
        helpersInit.getBaseHelper().getTextAndWriteLog(randomIndex + " - " + currency);
        return new String[]{String.valueOf(randomIndex), currency};
    }

    /**
     * Select random country for International bank-to-bank transfers
     * Case 1: select from full list except custom countries
     * Case 2: select from custom countries list
     */
    public String[] setInternationalTransferCountry(boolean customCountry, String[] transferCountryValue) {
        Map<String, String> countriesMap = new HashMap<>();
        List<String> resultList;
        for (SelenideElement x :
                COUNTRY_SELECT_NEW.findAll("option")) {
            countriesMap.put(x.getAttribute("value"), x.text());
        }

        if (!customCountry) {
            resultList = countriesMap.keySet().stream().filter(e -> !customCountriesMap.containsKey(e)).collect(Collectors.toList());
        } else {
            resultList = new ArrayList<>(customCountriesMap.keySet());
            // Delete selected variant from countries list
            if (transferCountryValue != null) {
                resultList.remove(transferCountryValue[0]);
                log.info(String.format("The selected country has been removed from the list of available variants: %s - %s",
                        transferCountryValue[0], transferCountryValue[1]));
            }
        }
        String selectedCountryValue = resultList.get(randomNumbersInt(resultList.size()));
        COUNTRY_SELECT_NEW.selectOptionByValue(selectedCountryValue);
        helpersInit.getBaseHelper().getTextAndWriteLog(selectedCountryValue + " - " + countriesMap.get(selectedCountryValue));
        return new String[]{selectedCountryValue, countriesMap.get(selectedCountryValue).toLowerCase()};
    }

    /**
     * Set CLABE number
     */
    public String setClabeNumber() {
        String value = helpersInit.getBaseHelper().randomNumbersString(6) + BaseHelper.getRandomWord(6);
        sendKey(CLABE_NUMBER_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set Agency (bank branch code)
     */
    public String setBankBranchCode() {
        String value = helpersInit.getBaseHelper().randomNumbersString(3) + BaseHelper.getRandomWord(2);
        sendKey(BANK_BRANCH_CODE_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set CNPJ/CPF code
     */
    public String setCnpjCpfCode(String[] codeValue) {
        int codeIndex = randomNumbersInt(codeValue.length);
        sendKey(CNPJ_CPF_CODE_INPUT, codeValue[codeIndex]);
        return helpersInit.getBaseHelper().getTextAndWriteLog(codeValue[codeIndex]);
    }

    /**
     * Set DNI \ RUC \ Tax ID
     */
    public String setDniRucTaxId() {
        String value = helpersInit.getBaseHelper().randomNumbersString(6) + BaseHelper.getRandomWord(5);
        sendKey(DNI_RUC_TAX_ID_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Select random country of birth for International bank-to-bank transfers
     */
    public String[] setCountryOfBirth() {
        String selectedCountryValue = helpersInit.getBaseHelper().selectRandomValue(COUNTRY_OF_BIRTH_SELECT);
        String[] countryOfBirthValText = new String[]{selectedCountryValue, COUNTRY_OF_BIRTH_SELECT.find(
                By.cssSelector("[value='" + selectedCountryValue + "']")).text()};
        helpersInit.getBaseHelper().getTextAndWriteLog(countryOfBirthValText[0] + " - " + countryOfBirthValText[1]);
        return countryOfBirthValText;
    }

    /**
     * Set date of birth for International bank-to-bank transfers
     */
    public String setDateOfBirth() {
        String value = helpersInit.getBaseHelper().dateFormat(helpersInit.getBaseHelper().randomDate(), "yyyy-MM-dd");
        sendKey(DATE_OF_BIRTH_INPUT.shouldBe(visible), value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set National Identity Number International bank-to-bank transfers
     */
    public String setNationalIdentityNumber() {
        String value = helpersInit.getBaseHelper().randomNumbersString(12);
        sendKey(NATIONAL_IDENTITY_NUMBER_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set IFSC code for International bank-to-bank transfers
     */
    public String setIfscCode() {
        String value = BaseHelper.getRandomWord(4) + "0" + helpersInit.getBaseHelper().randomNumbersString(6);
        sendKey(IFSC_CODE_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set Tax ID for International bank-to-bank transfers
     */
    public String setTaxId() {
        String value = helpersInit.getBaseHelper().randomNumbersString(6) + BaseHelper.getRandomWord(6);
        sendKey(TAX_ID_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set CNAPS number for International bank-to-bank transfers
     */
    public String setCnapsNumber() {
        String value = helpersInit.getBaseHelper().randomNumbersString(6) + BaseHelper.getRandomWord(6);
        sendKey(CNAPS_NUMBER_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Check date of birth
     */
    public boolean checkDateOfBirth(String dateValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(dateValue, DATE_OF_BIRTH_INPUT.val());
    }

    /**
     * Check Tax ID
     */
    public boolean checkTaxId(String taxIdValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(taxIdValue, TAX_ID_INPUT.val());
    }

    /**
     * Check IFSC code
     */
    public boolean checkIfscCode(String ifscCodeValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(ifscCodeValue, IFSC_CODE_INPUT.val());
    }

    /**
     * Check National Identity Number
     */
    public boolean checkNationalIdentityNumber(String nationalIdentityNumberValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(nationalIdentityNumberValue, NATIONAL_IDENTITY_NUMBER_INPUT.val());
    }

    /**
     * Check CNAPS number
     */
    public boolean checkCnapsNumber(String cnapsNumberValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(cnapsNumberValue, CNAPS_NUMBER_INPUT.val());
    }

    /**
     * Check Clabe number
     */
    public boolean checkClabeNumber(String clabeNumberValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(clabeNumberValue, CLABE_NUMBER_INPUT.val());
    }

    /**
     * Check Agency (bank branch code)
     */
    public boolean checkBankBranchCode(String bankBranchCodeValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(bankBranchCodeValue, BANK_BRANCH_CODE_INPUT.val());
    }

    /**
     * Check CNPJ/CPF code
     */
    public boolean checkCnpjCpfCode(String cnpjCpfCodeValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(cnpjCpfCodeValue, CNPJ_CPF_CODE_INPUT.val());
    }

    /**
     * Check DNI \ RUC \ Tax ID
     */
    public boolean checkDniRucTaxId(String dniRucTaxIdValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(dniRucTaxIdValue, DNI_RUC_TAX_ID_INPUT.val());
    }

    /**
     * Check country of birth
     */
    public boolean checkCountryOfBirth(String[] countryOfBirthValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                helpersInit.getBaseHelper().checkDatasetEquals(countryOfBirthValue[0], COUNTRY_OF_BIRTH_SELECT.getSelectedValue()) &&
                        helpersInit.getBaseHelper().checkDatasetEquals(countryOfBirthValue[1], COUNTRY_OF_BIRTH_SELECT.getSelectedText()));
    }

    /**
     * Check disabled purses in Payment select
     */
    public boolean checkPursesSelectDisabled() {
        PAYMENT_METHODS_FIELD.shouldBe(visible);
        return PAYMENT_METHODS_SELECT.findAll("span[class*=disabled]").size() > 0;
    }

    /**
     * Check bank transfer purse Id
     */
    public boolean checkBankTransferId(String purseId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BANK_TRANSFER_ID_HIDDEN_FIELD.getValue().equals(purseId));
    }

    /**
     * Get purse Id
     */
    public String getPurseId() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PURSE_TYPE_ID_HIDDEN_FIELD.getValue());
    }

    /**
     * Get PayPal purse value
     */
    public String getPayPalPurseValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYPAL_PURSE_INPUT.getValue());
    }

    /**
     * Get Payoneer purse value
     */
    public String getPayoneerlPurseValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYONEER_PURSE_INPUT.getValue());
    }

    /**
     * Set name for client
     */
    public String setClientName() {
        String value = helpersInit.getGenerateDataHelper().generateName();
        clearAndSetValue(PAYMASTER24_NAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set surname for client
     */
    public String setClientSurname() {
        String value = helpersInit.getGenerateDataHelper().generateSurname();
        clearAndSetValue(PAYMASTER24_SURNAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set birth date for client
     */
    public String setClientBirthDate() {
        Date birthDate = helpersInit.getGenerateDataHelper().generateBirthDate();
        clearAndSetValue(PAYMASTER24_DATE_OF_BIRTH_INPUT,
                helpersInit.getBaseHelper().dateFormat(birthDate, "MM.dd.yyyy"));
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                helpersInit.getBaseHelper().dateFormat(birthDate, "yyyy-MM-dd"));
    }

    /**
     * Set country for Paymaster24
     */
    public Map<String, String> setCountryPaymaster24() {
        ElementsCollection countries = PAYMASTER24_COUNTRY_INPUT.$$("option");
        Map<String, String> country = new HashMap<>();
        int randomIndex = randomNumbersInt(countries.size());
        country.put(countries.get(randomIndex).getValue(), countries.get(randomIndex).text());
        PAYMASTER24_COUNTRY_INPUT.selectOptionByValue(country.keySet().stream().findFirst().get());
        return country;
    }

    /**
     * Check Paymaster24 county values
     */
    public boolean checkPaymaster24Country(Map country) {
        return helpersInit.getBaseHelper().checkDatasetEquals(country.keySet().stream().findFirst().get().toString(),
                PAYMASTER24_COUNTRY_INPUT.getSelectedValue()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(country.values().stream().findFirst().get().toString(),
                        PAYMASTER24_COUNTRY_INPUT.getSelectedOption().text());
    }

    /**
     * Check purse type in select list
     */
    public boolean checkPursesTypeIsAvailable(String purseType) {
        PURSE_TYPE_SELECT.shouldBe(visible).click();
        return PURSE_TYPE_SELECT.findAll("span").texts().contains(purseType.toUpperCase());
    }

    /**
     * Get Capitalist purse value
     */
    public String getCapitalistPurseValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAPITALIST_PURSE_INPUT.getValue());
    }
}
