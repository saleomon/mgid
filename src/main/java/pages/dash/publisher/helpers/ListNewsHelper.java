package pages.dash.publisher.helpers;

import com.codeborne.selenide.Condition;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.time.Duration;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.dash.publisher.locators.news.ListNewsLocator.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;

public class ListNewsHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public ListNewsHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Открытие интерфейса редактирования новости (попап)
     */
    public void openNewsEditInterface(String newsId) {
            log.info("открываем форму NewsEditInterface");
            $(String.format(NEWS_ITEM, newsId)).shouldBe(Condition.visible).doubleClick();
            checkErrors();
        }

    /**
     * Import news
     */
    public void createNewsByImport(String nameFileForImport) {
        log.info("открываем поп-ап для импорта новостей и передаём в него файл и сохраняем");
        IMPORT_NEWS_BUTTON.shouldBe(visible).click();
        IMPORT_NEWS_LOAD_BUTTON.shouldBe(visible).val(LINK_TO_RESOURCES_FILES + nameFileForImport);
        IMPORT_NEWS_SAVE_FORM.shouldBe(visible).click();

        // ожидаем окончания загрузки файла
        IMPORT_NEWS_LOAD_INDICATOR.shouldBe(hidden, Duration.ofSeconds(16));
        checkErrors();
        log.info("получаем ответ о импорте новости - " + NOTIFICATION_TEXT.text());
    }

    /**
     * Check news title
     */
    public boolean checkNewsTitle(String expectedNewsTitle) {
        return expectedNewsTitle == null | helpersInit.getBaseHelper().checkDatasetEquals(NEWS_TITLE_FIELDS.last().shouldBe(visible).getText(), expectedNewsTitle);
    }

    /**
     * Check news description
     */
    public boolean checkNewsDescription(String expectedNewsDescription) {
        return expectedNewsDescription == null | helpersInit.getBaseHelper().checkDatasetEquals(expectedNewsDescription, NEWS_TEXT_FIELDS.last().shouldBe(visible).getText());
    }

}
