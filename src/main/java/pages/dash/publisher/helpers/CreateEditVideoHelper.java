package pages.dash.publisher.helpers;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;

public class CreateEditVideoHelper {

    public enum VideoFormat {
        INSTREAM("instream"),
        OUTSTREAM("outstream-over");

        private final String videoFormatType;

        VideoFormat(String videoFormatType) {
            this.videoFormatType = videoFormatType;
        }
        public String getTypeValue() {
            return videoFormatType;
        }
    }

    private final HelpersInit helpersInit;
    private final Logger log;

    public final SelenideElement VIDEO_DESKTOP_CHECKBOX = $("#part_video_content_desktop_is_enabled");
    public final SelenideElement VIDEO_MOBILE_CHECKBOX = $("#part_video_content_mobile_is_enabled");
    public final SelenideElement WIDGET_VIDEO_SWITCHER = $("#part_video_content_wrapper .switch");
    public final SelenideElement VIDEO_FORMAT_SELECT = $("#cuselFrame-part_video_content_selector");
    public final SelenideElement NATIVE_BACKFILL_TUMBLER = $x(".//div[input[@id='isNativeBackfill']]/div[@class='switch']/div");
    public final SelenideElement POSITION_BLOCK_VIDEO = $("#link-positions-block-video");
    public final ElementsCollection VIDEO_GROUPS = $$("#video_groups_wrapper_vrw .video-groups-check");

    public CreateEditVideoHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void switchVideoTumbler(boolean state) { switchTumblerInDash(state, WIDGET_VIDEO_SWITCHER.hover()); }

    public boolean checkSwitchToasterTumbler(boolean isSwitchOn) { return WIDGET_VIDEO_SWITCHER.text().equalsIgnoreCase("on") == isSwitchOn; }

    public boolean videoTumblerIsDisplayed() {
         return WIDGET_VIDEO_SWITCHER.isDisplayed();
    }

    public void enableVideoContentCheckboxes(boolean enableDesktop, boolean enableMobile){
        markUnMarkCheckbox(enableDesktop, VIDEO_DESKTOP_CHECKBOX);
        markUnMarkCheckbox(enableMobile, VIDEO_MOBILE_CHECKBOX);
    }

    public boolean checkVideoContentCheckboxes(boolean enableDesktop, boolean enableMobile){
        return checkIsCheckboxSelected(enableDesktop, VIDEO_DESKTOP_CHECKBOX) &&
            checkIsCheckboxSelected(enableMobile, VIDEO_MOBILE_CHECKBOX);
    }

    public void chooseVideoFormat(VideoFormat format) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(VIDEO_FORMAT_SELECT, format.videoFormatType);
    }

    public boolean checkVideoFormat(VideoFormat format) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(VIDEO_FORMAT_SELECT, format.videoFormatType);
    }

    public void switchNativeBackfillTumbler(boolean isSwitchOn) { switchTumblerInDash(isSwitchOn, NATIVE_BACKFILL_TUMBLER); }

    public boolean checkNativeBackfillTumbler(boolean isSwitchOn) { return NATIVE_BACKFILL_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn; }

    public ArrayList<String> enableVideoGroups() {
        List<Integer> list = helpersInit.getBaseHelper().getRandomCountValue(VIDEO_GROUPS.size(), 5);
        ArrayList<String> videoGroups = list.stream().map(i -> VIDEO_GROUPS.get(i).val()).collect(Collectors.toCollection(ArrayList::new));
        list.forEach(k -> VIDEO_GROUPS.get(k).parent().click());
        return videoGroups;
    }

    public boolean checkVideoGroups(ArrayList<String> list) {
        return list.stream().allMatch(i -> $("#video_groups-" + i).is(checked));
    }

    public int choosePlacementByVideo() {
        ElementsCollection items = POSITION_BLOCK_VIDEO.shouldBe(visible).$$(".item:not(.selected):not(.bannerDefaultPosition):not([class$='has-tooltip'])");

        log.info("clear old items");
        for(SelenideElement s : items){
            if(Objects.requireNonNull(s.attr("class")).contains("selected")) s.click();
        }

        log.info("get(data-position-id) random items");
        String vr_item = items.get(randomNumbersInt(items.size())).attr("data-position-id");
        log.info("vr_item: " + vr_item);
        assert vr_item != null;
        items.find(attribute("data-position-id", vr_item)).click(ClickOptions.usingJavaScript());
        return helpersInit.getBaseHelper().parseInt(vr_item);
    }

    public boolean checkPlacementByVideo(List random_items) {
        ElementsCollection items = POSITION_BLOCK_VIDEO.shouldBe(visible).$$(".item");

        return random_items.stream()
                .allMatch(i -> (Objects.requireNonNull(items.find(attribute("data-position-id", String.valueOf(i))).attr("class")).contains("selected") &&
                        helpersInit.getBaseHelper().checkDatasetEquals(convertRgbaToHex(Objects.requireNonNull(items.find(attribute("data-position-id", String.valueOf(i))).getCssValue("background-color"))),"#cc0f00")));
    }

}
