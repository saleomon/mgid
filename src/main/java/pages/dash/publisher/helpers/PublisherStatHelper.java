package pages.dash.publisher.helpers;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.dash.publisher.locators.PublisherLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class PublisherStatHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public PublisherStatHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }


    /**
     * открываем форму с настройками опций(метрик) custom report
     */
    public void openOptionsForm() {
        CUSTOM_REPORTS_OPTIONS_BUTTON.shouldBe(visible).click();
        CUSTOM_REPORT_OPTIONS_FORM.shouldBe(visible);
    }

    /**
     * открываем форму с настройками фильтров custom report
     */
    public void openFiltersForm() {
        CUSTOM_REPORT_FILTER_BUTTON.shouldBe(visible).click(ClickOptions.usingJavaScript());
        CUSTOM_REPORT_FILTER_FORM.shouldBe(visible).scrollTo();
    }

    /**
     * метод очищает все выбранные чекбоксы и проставлет чекбоксы из аргумента - metric
     */
    public void selectMetrics(String name, String... metric) {
        $$("[name='" + name + "[]']").asDynamicIterable().forEach(
                value -> {
                    value.shouldBe(visible);
                    boolean flag = Arrays.stream(metric).collect(Collectors.toSet()).contains(value.getValue());
                    if ((flag && !value.isSelected()) || (!flag && value.isSelected())) {
                        value.shouldBe(visible).click();
                    }
                }
        );
    }

    /**
     * Проверяем что в форме Filters отображаются все выбранные параметры из формы Options переданные в аргументе value
     */
    public void checkAllOptionsInFilterForm(String[] type, String... value) {
        openFiltersForm();

        log.info("Проверяем что в форме 'Filters' в селекте 'Метрики' подтягиваются параметры из value");
        CUSTOM_REPORT_METRICS_FILTER_OPTIONS.shouldBe(CollectionCondition.anyMatch("checkAllOptionsInFilterForm", val -> Arrays.stream(value).collect(Collectors.toSet()).contains(val.getAttribute("value"))));
        boolean flag = true;

        log.info("Проверяем что в форме 'Filters' в селекте 'Показатели' подтягиваются параметры из type");
        if (flag) {
            Arrays.stream(type)
                    .anyMatch(i -> i.equals("date") ?
                            !CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT.exists()
                            : CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT.find(By.cssSelector("[value=" + i + "]")).exists());
        }
    }

    /**
     * js-команда для дальнейшего получения данных из графика
     */
    public void initGraph(){
        executeJavaScript("charts.plot = function (placeholder, data, addition) { window.seleniumData = data } ");
    }

    /**
     * Если в интерфейсе присутствует пагинатор страниц(1,2 NEXT), то выбираем в пагинаторе отображение по 100 параметров
     */
    public void loadDataIfPaginatorDisplayed() {
        if (PAGINATOR_COUNT_LABEL.isDisplayed()) {
            helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CUSTOM_REPORT_PAGINATOR_SELECT_ID, "100");
            log.info("loadDataIfPaginatorDisplayed");
            waitForAjax();
            initGraph();
        }
    }

    /**
     * проверяем что в таблице отображаются все переданные сортировки метрик
     */
    public boolean checkShowAllMetrics(String... headers) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(Arrays.stream(headers).anyMatch(i -> $(".tableFloatingHeaderOriginal [sortby=" + i + "]").exists()));
    }

    /**
     * получаем js запрос для получения из графика данных по переданной метрике
     */
    public ArrayList getJsQuery(String param){
        sleep(500);
        return executeJavaScript(
                "var t = window.seleniumData." + param + ".data.length; var mas = []; " +
                "for (var i = 0; i < t; ++i){mas[i] = window.seleniumData." + param + ".data[i][1];} return mas;");
    }

    /**
     * 1,2,3 параметры должны передаватся в следующем порядке по формуле - element_1 / element_2 * element_3
     */
    public boolean checkCustomTotalParam(SelenideElement element_1, SelenideElement element_2, SelenideElement element_3, int coefficient) {
        double result;
            // рассчитываем по формуле result и сравниваем его с val_3 из таблицы
            double d = parseDouble(element_1) / parseDouble(element_2) * coefficient;

            if(d != Double.POSITIVE_INFINITY) {
                result = formatDouble(d);
                return comparisonData(result, parseDouble(element_3), 0.1);
            }
            return false;
    }

    /**
     * 1,2,3 параметры должны передаватся в следующем порядке по формуле - element_1 / element_2 * element_3
     */
    public boolean checkCustomTotalParam(SelenideElement element_1, String element_2, SelenideElement element_3, int coefficient) {
        double result;

        // рассчитываем по формуле result и сравниваем его с val_3 из таблицы
        result = formatDouble(parseDouble(element_1) / parseDouble(element_2) * coefficient);

        return comparisonData(round(result, 1), round(parseDouble(element_3), 1), 0.2);
    }

    /**
     * Вспомагающий метод для проверки правильного вычисления данных в таблице под графиком
     * Метод принимает 3 параметра
     * 1,2,3 параметры должны передаватся в следующем порядке по формуле - value_1 / value_2 * value_3
     */
    public boolean checkCustomParamInTableInEveryRow(ElementsCollection valList_1, ElementsCollection valList_2, ElementsCollection valList_3, int coefficient) {
        int count = 0;
        double val_1 = 0, val_2 = 0, val_3 = 0, result;
        try {
            List<Double> list_1 = new ArrayList<>();
            List<Double> list_2 = new ArrayList<>();
            List<Double> list_3 = new ArrayList<>();
            for(SelenideElement s : valList_1){ list_1.add(parseDouble(s)); }
            for(SelenideElement s : valList_2){ list_2.add(parseDouble(s)); }
            for(SelenideElement s : valList_3){ list_3.add(parseDouble(s)); }

            //получаем параметры за каждый день
            for (int i = 0; i < valList_3.size(); i++) {
                val_1 = list_1.get(i);
                val_2 = list_2.get(i);
                val_3 = list_3.get(i);

                if (val_1 == 0 | val_2 == 0 | val_3 == 0) {
                    count++;
                } else {

                    // рассчитываем по формуле result и сравниваем его с val_3 из таблицы
                    result = formatDouble(val_1 / val_2 * coefficient);

                    if (comparisonData(result, val_3, 0.1)) {
                        count++;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Catch " + e);
            log.error("val_1:" + val_1 + ", val_2:" + val_2 + ", val_3:" + val_3);
        }
        return count == valList_3.size();
    }

    /**
     * Вспомагающий метод для проверки правильного вычисления данных в таблице под графиком
     * Метод принимает 3 параметра
     * 1,2,3 параметры должны передаватся в следующем порядке по формуле - value_1 / value_2 * value_3
     */
    public boolean checkCustomParamInTableInEveryRow(ElementsCollection valList_1, ArrayList<String> valList_2, ElementsCollection valList_3, int coefficient) {
        int count = 0;
        double val_1, val_2, val_3;

        //получаем параметры за каждый день
        for (int i = 0; i < valList_3.size(); i++) {
            val_1 = parseDouble(valList_1.get(i));
            val_2 = parseDouble(valList_2.get(i));
            val_3 = parseDouble(valList_3.get(i));

            if (mathActionsOnTableAndGraphHelper(val_1, val_2, val_3, coefficient)) {
                count++;
            }
        }
        return count == valList_3.size();
    }

    /**
     * Вспомагающий метод для проверки правильного вычисления данных в графиком
     * Метод принимает 3 параметра - которые должны передаватся в следующем порядке по формуле - value_1 / value_2 * value_3
     */
    public boolean checkCustomParamInGraphForAllData(ArrayList<Double> value_1, ArrayList<Long> value_2, ArrayList<Double> value_3, int coefficient) {
        int count = 0;
        double val_1, val_2, val_3;

        //получаем параметры за каждый день
        for (int i = 0; i < value_3.size(); i++) {
            val_1 = parseDouble(String.valueOf(value_1.get(i)));
            val_2 = parseDouble(String.valueOf(value_2.get(i)));
            val_3 = parseDouble(String.valueOf(value_3.get(i)));

            if (mathActionsOnTableAndGraphHelper(val_1, val_2, val_3, coefficient)) {
                count++;
            }
        }
        return count == value_3.size();
    }

    /**
     * проверяем отображание и сумму Целочисленных(Показы, Клики, реальные показы) елементов из таблицы, графика, db и тотала
     */
    public boolean checkSumGraphSumTableTotalTable(ArrayList<Long> mass, ElementsCollection metric, SelenideElement webElement, String totalFromDb) {
        int sumData = getSumInt(mass);

        return helpersInit.getBaseHelper().checkDataset(sumData, helpersInit.getBaseHelper().parseInt(webElement)) &
                helpersInit.getBaseHelper().checkDataset(sumData, helpersInit.getBaseHelper().parseInt(totalFromDb)) &
                helpersInit.getBaseHelper().checkDataset(sumData, getSumWagesTableInt(metric));
    }

    public boolean checkSumGraphSumTable(ArrayList<Long> mass, ElementsCollection metric, SelenideElement webElement, String totalFromDb) {
        int sumData = getSumInt(mass);

        return helpersInit.getBaseHelper().checkDataset(sumData, helpersInit.getBaseHelper().getNumberValueFromCell(webElement)) &
                helpersInit.getBaseHelper().checkDataset(sumData, helpersInit.getBaseHelper().parseInt(totalFromDb)) &
                helpersInit.getBaseHelper().checkDataset(sumData, getSumValuesTableInt(metric));
    }

    /**
     * проверяем отображание и сумму Целочисленных(Показы, Клики, реальные показы) елементов из таблицы, db и тотала
     */
    public boolean checkSumGraphSumTableTotalTable(ElementsCollection metric, SelenideElement webElement, String totalFromDb) {

        return helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().parseInt(webElement), helpersInit.getBaseHelper().parseInt(totalFromDb)) &
                helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().parseInt(webElement), getSumWagesTableInt(metric));
    }

    /**
     * проверяем отображание и сумму Значений с плавающей точкой(Заработок, Cpc, Cpm) елементов из таблицы, графика, db и тотала
     */
    public boolean checkSumGraphSumTableTotalTableWithDot(ArrayList<Double> mass, ElementsCollection metric, SelenideElement webElement, String totalFromDb) {
        double sumData = getSumDouble(mass);

        return comparisonData(sumData, parseDouble(webElement), 0.1) &
                comparisonData(sumData, formatDouble(parseDouble(totalFromDb)), 0.1) &
                comparisonData(sumData, getSumWagesTableDouble(metric), 0.1);
    }

    /**
     * Получаем сумму массива елементов (Long)
     */
    private Integer getSumInt(ArrayList<Long> mass) {
        return mass.stream().mapToInt(Long::intValue).sum();
    }

    /**
     * Получаем сумму массива елементов Double
     */
    private double getSumDouble(ArrayList<Double> mass) {
        double temp = 0.00;
        for (int i = 0; i < mass.size(); i++) {
            temp += Double.parseDouble(String.valueOf(mass.get(i)));
        }
        return formatDouble(temp);
    }

    /**
     * получаем сумму массива елементов из таблицы id которой передаётся в аргументе
     */
    private Integer getSumWagesTableInt(ElementsCollection elements) {
        int result = 0;
        if(elements.get(0).exists()){
            for(SelenideElement s : elements){
                result+=helpersInit.getBaseHelper().parseInt(s.innerText().replace("%", "").replace("\n", ""));
            }
        }
        return result;
    }

    private Integer getSumValuesTableInt(ElementsCollection elements) {
        int result = 0;
        if(elements.get(0).exists()){
            for(SelenideElement s : elements){
                result+=helpersInit.getBaseHelper().parseInt(s.innerText().replaceAll("\\(*[0-9]*[0-9]?\\.*[0-9]*[0-9]%\\)", "").replace(" ", ""));
            }
        }
        return result;
    }

    /**
     * получаем сумму массива елементов из таблицы id которой передаётся в аргументе
     */
    private Double getSumWagesTableDouble(ElementsCollection elements) {
        double result = 0;
        for(SelenideElement s : elements){
            result+=parseDouble(s.text().replace("$", ""));
        }
        return result;
    }

    /**
     * Выбираем рандомное значение по конкретному показателю из поп-апа 'ФИЛЬТРЫ' и фильтруем по нему данные с последующей проверкой
     */
    public boolean checkWorkFilterByDimensionWithSelect(int... customValue) {
        String getCurrentValueMetric;
        sleep(500);

        log.info("открываем поп-ап 'Фильтры' если он ещё не открыт");
        if (Objects.equals(CUSTOM_REPORT_FILTER_FORM.attr("style"), "display: none;")) openFiltersForm();

        log.info("выбираем рандомное значение параметра и фильтруем");
        CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT_VALUES.shouldBe(visible);

        if (customValue.length > 0)
            getCurrentValueMetric = helpersInit.getBaseHelper().selectCustomValueReturnText(CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT_VALUES, helpersInit.getBaseHelper().getTextAndWriteLog(String.valueOf(customValue[0])));

        else
            getCurrentValueMetric = helpersInit.getBaseHelper().selectRandomText(CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT_VALUES);

        clickApplyFilter();

        log.info("проверяем что отфильтрованные значения равняются выбранному(currentValueMetric)");
        $$(CUSTOM_REPORT_DATA_COLUMN_IN_TABLE).shouldBe(CollectionCondition.anyMatch("checkWorkFilterByDimensionWithSelect", i -> i.getText().equals(getCurrentValueMetric)));
        return true;
    }

    /**
     * Выбираем значение по конкретному показателю из поп-апа 'ФИЛЬТРЫ' и фильтруем по нему данные с последующей проверкой
     * устанавливаем рандомное значение в input (isSetValue - traffic_source, subId)
     */
    public boolean checkWorkFilterByDimensionWithInput() {
        String getCurrentValueMetric;
        waitForAjax();
        waitForAjaxLoader();
        log.info("открываем поп-ап 'Фильтры' если он ещё не открыт");
        if (Objects.equals(CUSTOM_REPORT_FILTER_FORM.attr("style"), "display: none;")) openFiltersForm();

        log.info("выбираем рандомное значение параметра и фильтруем");
        getCurrentValueMetric = getAndSetRandomDimension();
        CUSTOM_REPORT_DIMENSIONS_FILTERS_INPUT.sendKeys(getCurrentValueMetric);
        clickApplyFilter();

        log.info("проверяем что отфильтрованные значения равняются выбранному(currentValueMetric)");
        $$(CUSTOM_REPORT_DATA_COLUMN_IN_TABLE).shouldBe(CollectionCondition.anyMatch("checkWorkFilterByDimensionWithInput", i -> i.getText().equals(getCurrentValueMetric)));
        return true;
    }

    /**
     * проверяем что поле для фильтра по SubId не принимает невалтидные значения
     */
    public String getMessageForNotValidSubId() {
        log.info("CUSTOM_REPORT_FILTER_FORM.attr(\"style\"): " + CUSTOM_REPORT_FILTER_FORM.attr("style"));
        sleep(2000);
        //открываем поп-ап 'Фильтры' если он ещё не открыт
        if (helpersInit.getBaseHelper().checkDatasetEquals(CUSTOM_REPORT_FILTER_FORM.attr("style"), "display: none;")) openFiltersForm();

        // передаём невалидное значение
        CUSTOM_REPORT_DIMENSIONS_FILTERS_INPUT.shouldBe(visible).sendKeys("ая-АЯ,az-AZ,1-0.@");

        clickApplyFilter();
        checkErrors();

        // проверяем что данные не отфильтровались и выборка пустая
        return CUSTOM_REPORT_INFO_MESSAGE.text();
    }

    /**
     * Получаем рандомное значение 'Показателя' по которому будем фильтровать данные (пример 'SUBID' = '43533')
     */
    private String getAndSetRandomDimension() {
        CUSTOM_REPORT_DIMENSIONS_FILTERS_SELECT.shouldBe(visible, Duration.ofSeconds(5));
        log.info("getAndSetRandomDimension -> shouldBe visible");
        List<String> values = $$(CUSTOM_REPORT_DATA_COLUMN_IN_TABLE).texts();
        return values.get(randomNumbersInt(values.size()));
    }

    /**
     * нажимаем на кнопку 'Применить'
     */
    private void clickApplyFilter() {
        CUSTOM_REPORT_APPLY_BUTTON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Проверяем работу в поп-апе "Фильтры" работу сортировки метрик по сортировке ("equal", "greater_or_equal", "less_or_equal")
     * Проверяем метрики с целочисленными данными ("shows", "real_shows", "clicks")
     */
    public boolean checkMinOrMaxValueSelect(String... values) {
        String chosenSortOption;
        String value;
        String nameMetric;
        String[] massOptions = {"equal", "greater_or_equal", "less_or_equal"};

        //выбираем рандомный параметр (clicks, shows, real_shows)
        nameMetric = helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().getRandomFromArray(values));

        //выбираем одно из значений кололнки таблицы по одному из выбранных значений
        value = helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getSortedHelper().getTableColumnData(nameMetric).texts().stream().findAny().get());

        //открываем поп-ап 'Фильтры' если он ещё не открыт
        if (Objects.equals(CUSTOM_REPORT_FILTER_FORM.attr("style"), "display: none;")) openFiltersForm();

        //выбираем показатель по которому будем фильтровать метрику
        helpersInit.getBaseHelper().selectCustomValue(CUSTOM_REPORT_METRICS_FILTERS_SELECT, nameMetric);

        //выбираем рандомную опцию математического действия
        chosenSortOption = helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().getRandomFromArray(massOptions));
        helpersInit.getBaseHelper().selectCustomValue(CUSTOM_REPORT_METRICS_FILTERS_SELECT_CONDITIONS, chosenSortOption);

        //передаём в input значение (valueMetric) и фильтруем
        CUSTOM_REPORT_METRICS_FILTER_INPUT.sendKeys(value);
        clickApplyFilter();

        // получаем отсортированные данные из nameMetric колонки и проверяем на выбранную опцию из chosenSortOption
        return value.contains(".") ?
                checkDoubleMass(nameMetric, chosenSortOption, parseDouble(value)) :
                checkIntMass(nameMetric, chosenSortOption, helpersInit.getBaseHelper().parseInt(value));

    }

    private boolean checkIntMass(String nameMetric, String chosenSortOption, int value) {
        if (!CUSTOM_REPORT_REPORT_DATA.exists()) {
            //получаем все данные из кололнки таблицы по одному из выбранных значений
            List<Integer> mass_new = helpersInit.getSortedHelper().getTableColumnData(nameMetric).texts().stream()
                    .map(i -> helpersInit.getBaseHelper().parseInt(i))
                    .collect(Collectors.toList());

            switch (chosenSortOption) {
                case "equal":
                    return mass_new.stream().allMatch(i -> i == value);
                case "greater_or_equal":
                    return mass_new.stream().allMatch(i -> i >= value);
                case "less_or_equal":
                    return mass_new.stream().allMatch(i -> i <= value);
            }

        }
        return false;
    }

    private boolean checkDoubleMass(String nameMetric, String chosenSortOption, double value) {
        if (!CUSTOM_REPORT_REPORT_DATA.exists()) {
            //получаем все данные из кололнки таблицы по одному из выбранных значений
            List<Double> mass_new = helpersInit.getSortedHelper().getTableColumnData(nameMetric).texts().stream()
                    .map(i -> parseDouble(i))
                    .collect(Collectors.toList());

            switch (chosenSortOption) {
                case "equal":
                    return mass_new.stream().allMatch(i -> i == value);
                case "greater_or_equal":
                    return mass_new.stream().allMatch(i -> i >= value);
                case "less_or_equal":
                    return mass_new.stream().allMatch(i -> i <= value);
            }

        }
        return false;
    }

    public String createReport() {
        String reportName = "test_auto_report" + BaseHelper.getRandomWord(2);
        CUSTOM_REPORT_CREATE_BUTTON.shouldBe(visible).click();
        CUSTOM_REPORT_NAME_FIELD.shouldBe(visible).sendKeys(reportName);
        CUSTOM_REPORT_SAVE_BUTTON.shouldBe(visible).click(ClickOptions.usingJavaScript());
        checkErrors();
        helpersInit.getBaseHelper().closePopup();
        return helpersInit.getBaseHelper().getTextAndWriteLog(reportName);
    }

    public boolean deleteReport(String reportName) {
        SelenideElement deleteIcon = $x(".//tr[td[a[@class='custom-report-redirect' and contains(text(), '" + reportName + "')]]]//a[contains(@class, 'client-custom-report-remove')]");
        deleteIcon.shouldBe(visible).click(ClickOptions.usingJavaScript());
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
        helpersInit.getBaseHelper().closePopup();
        checkErrors();
        return !deleteIcon.exists();
    }

    public boolean checkCustomReportInListInterfaceAndGoHim(String reportName) {
        SelenideElement report = $x(".//a[@class='custom-report-redirect' and contains(text(), '" + reportName + "')]");
        if (report.isDisplayed()) {
            report.click();
            CUSTOM_REPORT_TOTAL_DATA.shouldBe(visible);
            return true;
        }
        return false;
    }

    public ElementsCollection getTableColumnData(String columnName) {
        return $$x(".//table//tr[not(@class='totalRow')]//td[count(//thead[1]//th[div[normalize-space(text())='" + columnName + "']]/preceding-sibling::th)+1][not(contains(@class, 'space'))]");
    }

    public SelenideElement getTotalByText(String columnName) {
        return $x(".//table//tr[@class='totalRow']/td[count(//thead[1]//th[div[normalize-space(text())='" + columnName + "']]/preceding-sibling::th)+1][not(contains(@class, 'space'))]");
    }

    public SelenideElement getTotalBySort(String columnName, int... iterator) {
        int index = iterator.length > 0 ? iterator[0] : 1;
        return $x(".//table//tr[@class='totalRow']/td[count(//thead[1]//th[div//a[@sortby='" + columnName + "']]/preceding-sibling::th)+" + index + "][not(contains(@class, 'space'))]");
    }

    private boolean mathActionsOnTableAndGraphHelper(double val_1, double val_2, double val_3, int coefficient) {
        double result;
        if (val_2 == 0) {
            return true;
        } else {
            // рассчитываем по формуле result и сравниваем его с val_3 из таблицы
            result = formatDouble(val_1 / val_2 * coefficient);

            return comparisonData(round(result, 1), round(val_3, 1), 0.1);
        }
    }
}
