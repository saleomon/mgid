package pages.dash.publisher.helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;

import java.util.Objects;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.CreateEditWidgetLocators.CONFIRM_BUTTON;
import static pages.dash.publisher.locators.CreateEditWidgetLocators.POPUP_CLOSE_BUTTON;
import static pages.dash.publisher.locators.WidgetListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class WidgetListHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public WidgetListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public Integer addAbTestWidget(int widgetId) {
        int timer = 0;
        clickInvisibleElementJs($(String.format(EDIT_RULES_BUTTON, widgetId)).shouldBe(visible, ofSeconds(8)).scrollIntoView("{display: \"inline-block\"}"));
        ADD_AB_TEST_BUTTON.shouldBe(visible, ofSeconds(8)).click();

        do {
            log.info("wait POPUP_CLOSE_BUTTON - " + timer);
            if (POPUP_CLOSE_BUTTON.isDisplayed() && CLONED_WIDGET_ID.isDisplayed()) {
                continue;
            }

            if (helpersInit.getMessageHelper().isEmergencyModePresentDash()) {

                sleep(5000);

                log.info("Click save widget button -> " + timer);
                $(String.format(EDIT_RULES_BUTTON, widgetId)).shouldBe(visible).click();
                ADD_AB_TEST_BUTTON.shouldBe(visible, ofSeconds(8)).click();
                waitForAjax();
                checkErrors();
            }

            if (timer > 0) sleep(1000);

            timer++;
        }
        while (!POPUP_CLOSE_BUTTON.isDisplayed() && timer < 60);

        log.info("Clone was added for widget");
        return Integer.valueOf(Objects.requireNonNull(CLONED_WIDGET_ID.should(visible, ofSeconds(30)).attr("data-child-id")));
    }

    /**
     * check that clone was created
     */
    public boolean checkCreatedClone(int widgetId, Integer cloneId,  String nameOfClone) {
        clickInvisibleElementJs($(String.format(EDIT_RULES_BUTTON, widgetId)).scrollIntoView("{display: \"inline-block\"}"));
        CLONE_ROW_LOCATOR.shouldBe(visible, ofSeconds(20));
        $("[data-parent-id='" + widgetId + "'][data-child-id='" + cloneId + "']").shouldBe(visible);
        $x(String.format(NAME_OF_CLONED_WIDGET_LOCATOR, nameOfClone)).shouldBe(visible);
        POPUP_CLOSE_BUTTON.click();
        log.info("Clone present with right name");
        return true;
    }

    /**
     * is present clone name
     */
    public boolean isPresentCloneName() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_CLONE_NAME.exists());
    }

    /**
     * check clone widget attributes at popup
     */
    public boolean checkCloneParentMarker() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_CLONE_MARKER.exists());
    }

    /**
     * open parent tree with clones
     */
    public void openParentTree(int widgetId) {
        $(String.format(SHOW_CHILD_WIDGET_ICON, widgetId)).click(usingJavaScript());
    }

    /**
     * check clone widget attributes
     */
    public boolean isCloneWidgetIconDisplayed(int widgetId) {
        return $(String.format(SHOW_CHILD_WIDGET_ICON, widgetId)).isDisplayed();
    }

    /**
     * delete clone
     */
    public void deleteClonedWidget(String cloneId) {
        SelenideElement deleteIcon = $("[data-id='" + cloneId + "'] [class*=archiveWidget]");
        deleteIcon.shouldBe(visible, ofSeconds(15));
        helpersInit.getBaseHelper().waitDisplayedElement(deleteIcon);
        deleteIcon.scrollIntoView(true).click(usingJavaScript());
        CONFIRM_BUTTON.shouldBe(visible, ofSeconds(8)).click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check deleted clone
     */
    public boolean checkDeletedClone(int widgetId, String cloneId) {
        if(!$("[data-id='" + cloneId + "'] [class='status-blocked']").shouldBe(visible, ofSeconds(10)).exists()){
            refresh();
            openParentTree(widgetId);
        }
        return $("[data-id='" + cloneId + "'] [class='status-blocked']").exists();
    }

    /**
     * click 'edit rules' button
     */
    public void clickEditRules(String widgetId) {
        $("[data-id='" + widgetId + "'] [class*=create-subwidget]").shouldBe(visible).scrollTo().click(usingJavaScript());
        waitForAjaxVisible();
        checkErrors();
    }

    public void clickAbTestFromSubWidget(String subWidgetId) {
        $(String.format(ADD_AB_TEST_FROM_SUBWIDGET_BUTTON, subWidgetId)).shouldBe(visible).click();
        checkErrors();
    }

    /**
     * click 'add new Subwidget' button
     */
    public void clickAddNewSubwidget(){
        ADD_NEW_SUBWIDGET_BUTTON.shouldBe(visible, ofSeconds(12)).click();
        checkErrors();
    }

    public int getSubwidgetId(String titleRule) {
        return helpersInit.getBaseHelper().parseInt($x(String.format(CLONE_WIDGET_ID_LABEL, titleRule)).shouldBe(visible, ofSeconds(12)).attr("data-child-id"));
    }

    public int getAbTestIdFromSubwidget(String subWidgetId) {
        return helpersInit.getBaseHelper().parseInt($(String.format(AB_TEST_ID_FROM_SUBWIDGET_LABEL, subWidgetId)).attr("data-child-id"));
    }

    /**
     * fill rule title
     */
    public String fillRuleTitle(String val) {
        RULE_TITLE_INPUT.val(val);
        return val;
    }

    /**
     * fill rule country
     */
    public String fillRuleCountry() {
        RULE_COUNTRY_INPUT.click();
        return selectRandomValSelectListDashJs(RULE_COUNTRY_SELECT);
    }

    /**
     * fill rule traffic type
     */
    public String fillRuleTrafficTypes(String val) {
        RULE_TRAFFIC_TYPE_INPUT.click();
        $x("//*[text()='" + val + "' and @class='active-result']").click();
        return val;
    }

    /**
     * save rule settings
     */
    public void saveRule() {
        SAVE_NEW_RULE.shouldBe(visible, ofSeconds(10)).click();
        checkErrors();
        waitForAjaxVisible();
    }

    /**
     * check rule settings
     */
    public boolean checkSettingsRule(int subwidgetId, String titleRule, String countryRule, String trafficRule) {
        log.info("Check saved settings - checkSettingsSubwidget");
        return helpersInit.getBaseHelper().checkDatasetEquals($(String.format(SUBWIDGET_lABEL, subwidgetId, 1)).text(), titleRule) &
                helpersInit.getBaseHelper().checkDatasetEquals($(String.format(SUBWIDGET_lABEL, subwidgetId, 2)).text(), countryRule) &
                helpersInit.getBaseHelper().checkDatasetEquals($(String.format(SUBWIDGET_lABEL, subwidgetId, 3)).text(), trafficRule);
    }

    public boolean checkSettingsAbTestFromSubwidget(int subwidgetId, String countryRule, String trafficRule) {
        log.info("checkSettingsAbTestForSubwidget");
        return helpersInit.getBaseHelper().checkDatasetEquals($(String.format(SUBWIDGET_lABEL, subwidgetId, 2)).text(), countryRule) &
                helpersInit.getBaseHelper().checkDatasetEquals($(String.format(SUBWIDGET_lABEL, subwidgetId, 3)).text(), trafficRule);
    }

    /**
     * Open popup with widgets code
     */
    public void openWidgetsPopupWithCode(int widgetId) {
        clickInvisibleElementJs($(String.format(GET_THE_CODE_ICON, widgetId)).hover());
        COPY_CODE_ICON.shouldBe(visible);
        checkErrors();
        log.info("Open popup with code in widgets list");
    }

    /**
     * Check AMP code for widget
     */
    public boolean checkAMPCodeInWidgetsList(int widgetId, String siteName, int siteId, String subnetName) {
        GET_CODE_POPUP.shouldBe(visible, ofSeconds(10));
        log.info("Popup with widgets code is displayed - " + GET_CODE_POPUP.shouldBe(visible).isDisplayed());
        log.info("Check AMP widgets code presence");
        return helpersInit.getBaseHelper().checkDatasetContains(GET_CODE_POPUP.text(),
                "<amp-embed width=\"600\" height=\"600\" layout=\"responsive\" type=\"" + subnetName.toLowerCase() + "\" data-publisher=\"" + siteName.toLowerCase()
                        + "\" data-widget=\"" + widgetId + "\" data-container=\"M" + siteId + "ScriptRootC" + widgetId + "\" data-block-on-consent=\"_till_responded\" > </amp-embed>");
    }

    public boolean checkShortCodeWithClicktracking(int widgetId, String siteName, int siteId, String macros){
        GET_CODE_POPUP.shouldBe(visible, ofSeconds(10));
        return helpersInit.getBaseHelper().checkDatasetContains(GET_CODE_POPUP.text(),
                "<!-- Composite Start -->\n" +
                        "<div id=\"M" + siteId + "ScriptRootC" + widgetId + "\">" +
                        "</div>\n" +
                        "    <script>var MGClickTracking = window.MGClickTracking || \"" + macros + "\";</script>\n" +
                        "<script src=\"https://jsc.mgid.com/t/e/" + siteName + "." + widgetId + ".js\" async></script>\n" +
                        "<!-- Composite End -->");
    }

    public boolean checkShortCode(int widgetId, String siteName, int siteId){
        GET_CODE_POPUP.shouldBe(visible, ofSeconds(10));
        return helpersInit.getBaseHelper().checkDatasetContains(GET_CODE_POPUP.text(),
                "<!-- Composite Start -->\n" +
                        "<div id=\"M" + siteId + "ScriptRootC" + widgetId + "\">" +
                        "</div>\n" +
                        "<script src=\"https://jsc.mgid.com/t/e/" + siteName + "." + widgetId + ".js\" async></script>\n" +
                        "<!-- Composite End -->");
    }

    /**
     * select random value in selectList for Dashboard
     */
    public String selectRandomValSelectListDashJs(SelenideElement element) {
        ElementsCollection list = element.shouldBe(exist).findAll(By.tagName("li"));
        String value = list.get(randomNumbersInt(list.size())).attr("data-option-array-index");
        String name = element.find(By.cssSelector("[data-option-array-index='" + value + "']")).text();
        element.find(By.cssSelector("[data-option-array-index='" + value + "']")).hover().click();
        return name;
    }

    public void chooseWidgetStatusFilter(String status){
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(WIDGET_IS_DELETED_FILTER, status);
        APPLY_BUTTON.click();
        checkErrors();
        waitForAjax();
    }
}
