package pages.dash.publisher.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import static com.codeborne.selenide.Condition.visible;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.news.AddNewsLocators.IMAGE_LOCATOR;
import static pages.dash.publisher.locators.news.EditNewsLocator.*;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CreateEditNewsHelper {

    private HelpersInit helpersInit;
    private Logger log;

    private boolean isGenerateNewValues = false;
    private boolean useDescription = false;

    public CreateEditNewsHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public CreateEditNewsHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    /**
     * Заполнение поля Название
     */
    public String editTitle(String title) {
        String newsTitle;
        if (isGenerateNewValues) {
            newsTitle = "edit title " + BaseHelper.getRandomWord(5);
            helpersInit.getBaseHelper().setAttributeValueJS(TITLE_INPUT.shouldBe(visible), newsTitle);
        } else if (title != null) {
            newsTitle = title;
            helpersInit.getBaseHelper().setAttributeValueJS(TITLE_INPUT.shouldBe(visible), newsTitle);
        } else {
            return null;
        }
        log.info("Edit title was filled - " + newsTitle);
        return newsTitle;
    }

    /**
     * Заполнение поля Описание
     */
    public String editDescription(String description) {
        String newsDescription = null;
        if (useDescription) {
            if (isGenerateNewValues) {
                newsDescription = "edit description " + BaseHelper.getRandomWord(2) + "\uD83D\uDE00\u2600";
                helpersInit.getBaseHelper().setAttributeValueJS(DESCRIPTION_INPUT, newsDescription);
            } else if (description != null) {
                newsDescription = description;
                helpersInit.getBaseHelper().setAttributeValueJS(DESCRIPTION_INPUT, newsDescription);
            } else {
                return null;
            }
            log.info("Edit description was filled - " + newsDescription);
        }
        return newsDescription;
    }

    /**
     * Выбор категории новости
     */
    public String editCategory(String categoryId) {
        if (isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
        } else if (categoryId != null) {
            helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
            return categoryId;
        } else {
            return null;
        }
    }

    /**
     * Редактирование изображения
     */
    public String editImageByLocal(String imageName) {
        if (imageName != null && !isGenerateNewValues) {
            String image = LINK_TO_RESOURCES_IMAGES + imageName;
            IMAGE_LOCATOR.sendKeys(image);
            waitForAjax();
            CROP.shouldBe(visible, ofSeconds(12));
            return helpersInit.getBaseHelper().getTextAndWriteLog(image);
        }
        return null;
    }

    /**
     * Заполнение поля Ссылка
     */
    public String editUrl(String url) {
        String newsUrl;
        if (isGenerateNewValues) {
            newsUrl = "test-site-with-video.com/" + BaseHelper.getRandomWord(7);
            clearAndSetValue(URL_INPUT, newsUrl);
            return newsUrl;
        } else if (url != null) {
            newsUrl = url;
            clearAndSetValue(URL_INPUT, url);
        } else {
            return null;
        }
        return newsUrl;
    }

    /**
     * Сохранение новости в интерфейсе редактирования
     */
    public void saveSettings() {
        SAVE_SETTINGS_BUTTON.click();
        log.info("saveSettings");
        checkErrors();
        waitForAjax();
    }

    /**
     * load local image
     */
    public String loadImageByLocal(String imageName) {
        String image;
        if (imageName != null && imageName.equals("")) {
            return imageName;
        } else {
            image = imageName != null ? LINK_TO_RESOURCES_IMAGES + imageName : LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg";
            IMAGE_LOCATOR.sendKeys(image);
            waitForAjax();
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(image);
    }
}
