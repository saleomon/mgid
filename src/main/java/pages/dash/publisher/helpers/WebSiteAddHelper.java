package pages.dash.publisher.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.publisher.locators.WebSiteAddLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.markUnMarkCheckbox;
import static core.helpers.ErrorsHelper.checkErrors;

public class WebSiteAddHelper {

    private final HelpersInit helpersInit;

    private boolean isGenerateNewValues = false;
    private boolean isSetSomeFields = false;

    public WebSiteAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public WebSiteAddHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    public WebSiteAddHelper setSetSomeFields(boolean isSetSomeFields) {
        this.isSetSomeFields = isSetSomeFields;
        return this;
    }

    /**
     * set 'Domain' input
     */
    public String setDomain(String domain) {
        if (isSetSomeFields && domain == null) return null;
        String domainVal = domain == null ? "testdomain" + BaseHelper.getRandomWord(2) + "com.ua" :
                domain;
        clearAndSetValue(DOMAIN_INPUT.shouldBe(visible), domainVal);
        return domainVal;
    }

    public String setComment(String comment) {
        if (isSetSomeFields && comment == null) return null;
        String val = (comment == null || isGenerateNewValues) ?
                "comment" + BaseHelper.getRandomWord(1) :
                comment;
        clearAndSetValue(COMMENT_INPUT.shouldBe(visible), val);

        return val;
    }

    public boolean checkComment(String comment) {
        return helpersInit.getBaseHelper().checkDatasetEquals(COMMENT_INPUT.text(), comment);
    }

    public String chooseLanguageSite(String language) {
        if (isSetSomeFields && language == null) return null;
        if (language == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(LANGUAGE_SELECT);
        } else return helpersInit.getBaseHelper().selectCurrentValInSelectListJSWithoutWFA(LANGUAGE_SELECT, language);
    }

    public boolean checkLanguage(String language) {
        return helpersInit.getBaseHelper().checkDatasetEquals(LANGUAGE_LABEL.val(), language);
    }

    public String chooseSiteCategory(String category) {
        if (isSetSomeFields && category == null) return null;
        if (category == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT);
        } else return helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, category);
    }

    public void saveWebSiteSettings() {
        SAVE_WEB_SITE_BUTTON.shouldBe(visible).click();
        checkErrors();
    }

    public void switchOnUtmTagging(boolean state) {
        markUnMarkCheckbox(state, UTM_SETTINGS_CHECKBOX);
    }

    public String fillUtmSource(String utmSource) {
        if (isSetSomeFields && utmSource == null) return null;
        String val = (utmSource == null || isGenerateNewValues) ?
                "utmSource" + BaseHelper.getRandomWord(2) :
                utmSource;
        clearAndSetValue(UTM_SOURCE_INPUT, val);
        return val;
    }

    public String fillUtmMedium(String utmMedium) {
        if (isSetSomeFields && utmMedium == null) return null;
        String val = (utmMedium == null || isGenerateNewValues) ?
                "utmMedium" + BaseHelper.getRandomWord(2) :
                utmMedium;
        clearAndSetValue(UTM_MEDIUM_INPUT, val);
        return val;
    }

    public String fillUtmCampaign(String utmCampaign) {
        if (isSetSomeFields && utmCampaign == null) return null;
        String val = (utmCampaign == null || isGenerateNewValues) ?
                "utmCampaign" + BaseHelper.getRandomWord(2) :
                utmCampaign;
        clearAndSetValue(UTM_CAMPAIGN_INPUT, val);
        return val;
    }

    public String getUtmSource() {
        return UTM_SOURCE_INPUT.val();
    }

    public String getUtmMedium() {
        return UTM_MEDIUM_INPUT.val();
    }

    public String getUtmCampaign() {
        return UTM_CAMPAIGN_INPUT.val();
    }


}
