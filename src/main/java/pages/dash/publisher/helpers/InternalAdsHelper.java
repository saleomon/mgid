package pages.dash.publisher.helpers;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.publisher.locators.InternalAdsLocators.FOR_NEWS_MASS_ACTIONS_SELECT_ALL_CHECKBOX;

public class InternalAdsHelper {

    public InternalAdsHelper() {
    }

    /**
     * Select all news in list
     */
    public void selectAllNews() {
        FOR_NEWS_MASS_ACTIONS_SELECT_ALL_CHECKBOX.shouldBe(visible).click();
    }
}
