package pages.dash.publisher.helpers;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.WebsiteListLocators.*;
import static pages.dash.publisher.variables.PublisherWebSiteVariables.adsTxtPopupDefaultRowMgid;
import static pages.dash.publisher.variables.PublisherWebSiteVariables.adsTxtPopupHeaderText;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class WebSiteListHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public WebSiteListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void sendSiteToModeration(Integer site) {
        //sleep(2000);
        clickInvisibleElementJs($("[data-site-id='" + site + "'] [class*='send_moderation icon-to-moderate']").shouldBe(visible));
        waitForAjaxVisible();
    }

    public boolean checkPopupText(String text) {
        return helpersInit.getBaseHelper().checkDatasetContains(text, POPUP_LOCATOR.shouldBe(visible, ofSeconds(8)).text());
    }

    public boolean checkVisibilitySendModerationIcon(Integer site) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(getDataSiteId(site).$(MODERATION_BUTTON_CLASS).exists());
    }

    /**
     * get element with @data-id
     */
    public SelenideElement getDataSiteId(Integer siteId) {
        return $("[data-site-id='" + siteId.toString() + "']");
    }

    public void waitAndClosePopUp() {
        POPUP_CLOSE.shouldBe(visible, ofSeconds(7)).click();
    }

    /**
     * получаем id сайта по известному ранее домену
     */
    public String getWebSiteId(String domain) {
        //if(POPUP_CLOSE.isDisplayed()) waitAndClosePopUp();
        if (ADD_WEBSITE_BUTTON.isDisplayed()) {
            return $x(".//tr[td[contains(text(), '" + domain.toLowerCase() + "')]]").shouldBe(visible, ofSeconds(8)).attr("data-site-id");
        }
        return null;
    }

    /**
     * Click Ads.txt button
     */
    public void clickAdsTxtButton() {
        ADS_TXT_BUTTON.shouldBe(visible).scrollTo().click();
        checkErrors();
        log.info("ADS.TXT button clicked");
    }

    /**
     * Check custom Ads.txt popup and content
     */
    public boolean checkCustomAdsTxtPopupAndContent(String adsPopupValue, String siteDomain) {
        SelenideElement adsTxtPopup = adsPopupValue.equals("new") ? SITES_ADS_TXT_POPUP : ADS_TXT_CONTENT_UPDATE_POPUP;
        return helpersInit.getBaseHelper().checkDataset(true, adsTxtPopup.shouldBe(visible).isDisplayed())
                && helpersInit.getBaseHelper().checkDatasetContains(adsTxtPopupHeaderText, adsTxtPopup.shouldBe(visible).text())
                && helpersInit.getBaseHelper().checkDatasetContains(adsTxtPopupDefaultRowMgid, adsTxtPopup.shouldBe(visible).text())
                && helpersInit.getBaseHelper().checkDatasetContains(siteDomain, adsTxtPopup.shouldHave(text(siteDomain)).text());
    }

    /**
     * Check Ads.txt popup closing
     */
    public boolean checkAdsTxtPopupClosing(String adsPopupValue) {
        SelenideElement adsTxtPopupCloseButton = adsPopupValue.equals("new") ? SITES_ADS_TXT_POPUP_CLOSE_BUTTON : ADS_TXT_CONTENT_UPDATE_POPUP_CLOSE_BUTTON;
        SelenideElement adsTxtPopup = adsPopupValue.equals("new") ? SITES_ADS_TXT_POPUP : ADS_TXT_CONTENT_UPDATE_POPUP;
        for (int i = 0; adsTxtPopupCloseButton.isDisplayed() && i < 3; i++) {
            helpersInit.getBaseHelper().getTextAndWriteLog("iteration # " + (i + 1));
            adsTxtPopupCloseButton.shouldBe(visible).click();
            sleep(500);
        }
        adsTxtPopup.shouldBe(hidden);
        return helpersInit.getBaseHelper().checkDataset(true, !adsTxtPopup.isDisplayed());
    }

    /**
     * Check default Ads.txt popup and content
     */
    public boolean checkDefaultAdsTxtPopupAndContent(String adsPopupValue, String siteDomain) {
        SelenideElement adsTxtPopup = adsPopupValue.equals("new") ? SITES_ADS_TXT_POPUP : ADS_TXT_CONTENT_UPDATE_POPUP.shouldBe(visible);
        return helpersInit.getBaseHelper().checkDataset(true, adsTxtPopup.isDisplayed())
                && helpersInit.getBaseHelper().checkDatasetContains(adsTxtPopupHeaderText, adsTxtPopup.should(appear).text())
                && helpersInit.getBaseHelper().checkDatasetContains(adsTxtPopupDefaultRowMgid, adsTxtPopup.should(appear).text())
                && !helpersInit.getBaseHelper().checkDatasetContains(siteDomain, adsTxtPopup.should(appear).text());
    }

    /**
     * Choose client country
     */
    public String setClientsCountry() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValSelectListDashJs(COUNTRY_SELECT));
    }

    /**
     * Choose client country
     */
    public String setClientsCountryIdealmedia() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(COUNTRY_SELECT_IDEALMEDIA));
    }

    /**
     * Save client data
     */
    public void saveAndFinish() {
        SAVE_AND_FINISH_BUTTON.click();
        checkErrors();
    }

    /**
     * Check clients login into dashboard on pages
     */
    public boolean checkLoginOnPage(String loginValue) {
        return USERS_LOGIN_FIELD.shouldBe(visible).text().contains(loginValue);
    }

    /**
     * Accept 'Terms and Conditions' and 'Privacy policy' checkbox
     */
    public void acceptPolicyCheckbox() {
        markUnMarkCheckbox(true, POLICY_CHECKBOX_LABEL);
    }

    /**
     * Submit popup
     */
    public void submitPopup() {
        CONFIRM_BUTTON.click();
        checkErrors();
    }

    public boolean directDemandIsDisplayed(){
        ADD_WEBSITE_BUTTON.shouldBe(visible);
        return DIRECT_DEMAND_BUTTON.isDisplayed();
    }
}
