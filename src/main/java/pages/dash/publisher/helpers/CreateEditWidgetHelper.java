package pages.dash.publisher.helpers;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import libs.hikari.tableQueries.general.Translations;
import libs.hikari.tableQueries.partners.TickersImageFormat;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import core.helpers.BaseHelper;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;

import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.shadowCss;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.CreateEditWidgetLocators.*;
import static pages.dash.publisher.variables.CreateEditWidgetVariables.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CreateEditWidgetHelper {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final TickersImageFormat tickersImageFormat;
    private final Translations translations;

    private boolean isClosePopup = true;
    private boolean customAutoplacement = false;
    private boolean isShadowDom = false;

    public CreateEditWidgetHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        tickersImageFormat = new TickersImageFormat();
        translations = new Translations();
    }

    public void setCustomAutoplacement(boolean customAutoplacement) {
        this.customAutoplacement = customAutoplacement;
    }

    public void setClosePopup(boolean closePopup) {
        isClosePopup = closePopup;
    }

    public CreateEditWidgetHelper setShadowDom(boolean shadowDom) {
        isShadowDom = shadowDom;
        return this;
    }

    /**
     * выбираем тип виджета "TYPE"
     */
    public void chooseTypeWidget(WidgetTypes.Types widgetType) {
        WEBSITE_NAME.shouldBe(visible).hover();
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(WIDGET_TYPE_SELECT, widgetType.getTypeValue());
        waitForAjaxLoader();
        sleep(1000);
    }

    /**
     * проверяем тип виджета "TYPE"
     */
    public boolean checkTypeWidget(WidgetTypes.Types widgetType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(WIDGET_TYPE_SELECT, widgetType.getTypeValue()));
    }

    /**
     * задаём имя виджета "NAME"
     */
    public String chooseWidgetName() {
        String title = "test_auto_widget_" + BaseHelper.getRandomWord(3);
        WIDGET_TITLE_INPUT.val(title);
        return title;
    }

    /**
     * получаем имя виджета "NAME"
     */
    public String getWidgetName() {
        return WIDGET_TITLE_INPUT.val();
    }

    /**
     * проверяем имя виджета "NAME"
     */
    public boolean checkWidgetName(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(WIDGET_TITLE_INPUT.val(), name);
    }

    /**
     * получаем имя сайта на который будет создан виджет "WEBSITE"
     */
    public String getWebSiteName() {
        return WEBSITE_NAME.text();
    }

    /**
     * проверяем имя сайта на который создан виджет "WEBSITE"
     */
    public boolean checkWebSiteName(String site) {
        return helpersInit.getBaseHelper().checkDatasetEquals(WEBSITE_NAME.text(), site);
    }

    /**
     * выбираем заголовок виджета "WIDGET TITLE"
     * если выбирам значение [WIDGET_TITLE_OTHER], то устанавливаем заголовок - "otherWidgetTitle"
     */
    public String chooseWidgetTitle() {
        String widgetTitle = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(TITLE_SELECT);

        if (widgetTitle.equals("[WIDGET_TITLE_OTHER]")) {
            OTHER_WIDGET_TITLE_INPUT.shouldBe(visible).val(otherWidgetTitleValue);
        }
        log.info("chooseWidgetTitle: " + widgetTitle);

        return widgetTitle;
    }

    /**
     * получаем активный елемент из селекта WIDGET TITLE
     */
    public String getWidgetTitle() {
        return helpersInit.getBaseHelper().getSelectedValue(TITLE_SELECT);
    }

    /**
     * проверяем что в селекте WIDGET TITLE выбрано верное значенеи
     * если в WIDGET TITLE выбрано [WIDGET_TITLE_OTHER] - проверяем отображение поля OTHER_WIDGET_TITLE_INPUT и его значение
     */
    public boolean checkWidgetTitle(String widgetTitle) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(TITLE_SELECT, widgetTitle) &&
                (widgetTitle.equals("[WIDGET_TITLE_OTHER]") ?
                        helpersInit.getBaseHelper().checkDatasetEquals(OTHER_WIDGET_TITLE_INPUT.val(), otherWidgetTitleValue) :
                        !OTHER_WIDGET_TITLE_INPUT.isDisplayed());
    }

    /**
     * проверяем что в конструкторе отображается верный WIDGET TITLE
     */
    public boolean checkWidgetTitleInConstructor(String widgetTitle) {
        if(!widgetTitle.equals("[WIDGET_TITLE_OTHER]")){
            widgetTitle = translations.getTranslateForWidgetTitleInEn(widgetTitle);
        }

        SelenideElement mghead = getElementIfHasShadowDom(MGHEAD_STYLE).shouldBe(visible);
        return widgetTitle.equals("[WIDGET_TITLE_OTHER]") ?
                helpersInit.getBaseHelper().checkDatasetEquals(mghead.scrollTo().text(), otherWidgetTitleValue) :
                helpersInit.getBaseHelper().checkDatasetEquals(mghead.scrollTo().text(), widgetTitle);
    }

    /**
     * choose random subType
     * choose any subtype except 'banner-300x250_4'
     */
    public WidgetTypes.SubTypes chooseSubTypeForBanner() {
        ElementsCollection list = SUB_TYPE_SELECT.shouldBe(exist).findAll(By.cssSelector("span:not([style*='none']):not(.cuselActive):not([val='banner-300x250_4']):not([val='banner-970x250'])"));
        String subType = list.get(randomNumbersInt(list.size())).attr("val");
        executeJavaScript("arguments[0].click()", SUB_TYPE_SELECT.find(By.cssSelector("[val='" + subType + "']")));
        log.info("getSubType: " + subType);
        return WidgetTypes.SubTypes.getType(subType);
    }

    /**
     * check color BUTTON for banner - input and constructor
     */
    public boolean checkColorButtonBanner(boolean showBannerButton, String color) {
        SelenideElement mgbutton = getElementIfHasShadowDom(MGBUTTON_STYLE);
        if (showBannerButton) {
            return BANNER_COLOR_SWITCH.text().equalsIgnoreCase("on") &&
                    checkColorInInput(BANNER_COLOR_BUTTON_INPUT, color) &&
                    helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("display"), "block") &&
                    checkColorInConstructor(MGBUTTON_STYLE, color, "background-color");
        } else {
            return BANNER_COLOR_SWITCH.text().equalsIgnoreCase("off") &&
                    helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("display"), "none");
        }
    }

    /**
     * check 'BUTTON EFFECT' for banner - input and constructor
     */
    public boolean checkBannerButtonEffect(boolean showBannerButton, boolean bannerButtonEffect) {
        SelenideElement mgbutton = getElementIfHasShadowDom(MGBUTTON_STYLE);

        if (!showBannerButton) {
            log.info("BANNER_COLOR_SWITCH: false -> !BANNER_BUTTON_EFFECT_SWITCH.isDisplayed()");
            return helpersInit.getBaseHelper().getTextAndWriteLog(!BANNER_BUTTON_EFFECT_SWITCH.isDisplayed());
        }
        if (bannerButtonEffect) {
            return BANNER_BUTTON_EFFECT_SWITCH.text().equalsIgnoreCase("on") &&
                    helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("animation"), "3.7s ease 0s infinite normal none running pulse");
        } else {
            return BANNER_BUTTON_EFFECT_SWITCH.text().equalsIgnoreCase("off") &&
                    helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("animation"), "none 0s ease 0s 1 normal none running");
        }
    }

    /**
     * check color BUTTON for banner - constructor(stand)
     */
    public boolean checkColorButtonBannerInConstructor(boolean showBannerButton, String color) {
        SelenideElement mgbutton = getElementIfHasShadowDom(MGBUTTON_STYLE);
        if (showBannerButton) {
            return helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("display"), "block") &&
                    checkColorInConstructor(MGBUTTON_STYLE, color, "background-color");
        } else {
            return helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("display"), "none");
        }
    }

    /**
     * check 'BUTTON EFFECT' for banner - constructor
     */
    public boolean checkBannerButtonEffectInConstructor(boolean showBannerButton, boolean bannerButtonEffect) {
        SelenideElement mgbutton = getElementIfHasShadowDom(MGBUTTON_STYLE);
        if (showBannerButton && bannerButtonEffect) {
            return helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("animation"), "3.7s ease 0s infinite normal none running pulse");
        } else {
            return helpersInit.getBaseHelper().checkDatasetEquals(mgbutton.getCssValue("animation"), "none 0s ease 0s 1 normal none running");
        }
    }

    /**
     * check banner blur in constructor
     */
    public boolean checkBlurBannerInConstructor(String typeBlur, String colorBlur) {
        if (typeBlur.equals("custom")) {
            return checkColorInConstructor(TEXT_ELEMENTS_STYLE, colorBlur, "background");
        } else {
            return checkColorInConstructor(TEXT_ELEMENTS_STYLE, "", "background");
        }
    }

    /**
     * check banner blur
     */
    public boolean checkBlurBanner(String typeBlurBanner, String colorBlurBanner) {
        if (helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(BANNER_BLUR_SELECT, typeBlurBanner)) {
            if (typeBlurBanner.equals("custom")) {
                return checkColorInInput(BANNER_BLUR_INPUT, colorBlurBanner) &
                        checkColorInConstructor(TEXT_ELEMENTS_STYLE, colorBlurBanner, "background");
            } else {
                return checkColorInConstructor(TEXT_ELEMENTS_STYLE, "", "background");
            }
        }
        return false;
    }

    /**
     * проверяем subType для BANNER типа виджета
     * проверяем что для каждого типа должно отображатся конкретное число тизеров в конструкторе
     */
    public boolean checkSubTypeForBanner(WidgetTypes.SubTypes subType, SubnetType subnetId) {
        int expectedCountTeasers = 0;
        int actualCountTeasers = $$(TEASERS_SIZE).size();
        switch (subType) {
            case BANNER_120x600 -> expectedCountTeasers = 4;
            case BANNER_728x90_1,
                    BANNER_160x600,
                    BANNER_970x90 -> expectedCountTeasers = 3;
            case BANNER_728x90_2,
                    BANNER_300x250_1,
                    BANNER_240x400 -> expectedCountTeasers = 2;
            case BANNER_300x250_2,
                    BANNER_300x250_3,
                    BANNER_320x100,
                    BANNER_300x250_4,
                    BANNER_320x50,
                    BANNER_970x250,
                    BANNER_470x325 -> expectedCountTeasers = 1;
            case BANNER_300x600 -> expectedCountTeasers = (subnetId == SubnetType.SCENARIO_IDEALMEDIA_IO) ? 2 : 3;
        }
        log.info("checkSubTypeForBanner: " + expectedCountTeasers + " == " + actualCountTeasers);
        return expectedCountTeasers == actualCountTeasers;
    }

    /**
     * передаем subType парсим и проверяем что размеры mgBox - равны переданным параметрам
     */
    public boolean checkMgBoxSize(WidgetTypes.SubTypes subType) {
        if (subType == WidgetTypes.SubTypes.BANNER_470x325 ||
                subType == WidgetTypes.SubTypes.BANNER_970x250) return true;

        SelenideElement elementForGetStyle = getElementIfHasShadowDom(MGBOX_STYLE);

        String size = subType.getTypeValue();
        size = size.contains("_") ? size.substring(size.indexOf("-") + 1, size.lastIndexOf("_")) :
                size.substring(size.indexOf("-") + 1);
        String[] temp = size.split("x");
        String width = temp[0];
        String height = temp[1];

        return helpersInit.getBaseHelper().checkDatasetContains(elementForGetStyle.getCssValue("width"), width) &
                (subType == WidgetTypes.SubTypes.BANNER_320x50 ?
                        comparisonData(parseDouble(elementForGetStyle.getCssValue("height").replaceAll("[a-z]", "")), parseDouble(height), 10) :
                        helpersInit.getBaseHelper().checkDatasetContains(elementForGetStyle.getCssValue("height"), height));
    }

    /**
     * выбираем "SUB TYPE" виджета
     */
    public WidgetTypes.SubTypes chooseSubType(WidgetTypes.SubTypes subType) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(SUB_TYPE_SELECT, subType.getTypeValue());
        return subType;
    }

    /**
     * проверяем "SUB TYPE" виджета
     */
    public boolean checkSubType(WidgetTypes.SubTypes subType) {
        if (subType == WidgetTypes.SubTypes.NONE) {
            return !SUB_TYPE_SELECT.isDisplayed();
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(SUB_TYPE_SELECT, subType.getTypeValue()));
    }

    /**
     * устанавливаем количество колонок виджета, поле "COLUMNS"
     */
    public Integer chooseColumns(int... columns) {
        String columnsCount = columns.length > 0 ?
                String.valueOf(columns[0]) :
                randomNumberFromRange(1, 3);
        sendKey(COLUMNS_INPUT, columnsCount + Keys.ENTER);
        return helpersInit.getBaseHelper().parseInt(columnsCount);
    }

    /**
     * устанавливаем количество колонок виджета, поле "COLUMNS"
     */
    public Integer chooseColumnsForCarousel() {
        String columnsCount = helpersInit.getBaseHelper().getTextAndWriteLog(randomNumberFromRange(7, 10));
        sendKey(COLUMNS_INPUT, columnsCount + Keys.ENTER);
        return helpersInit.getBaseHelper().parseInt(columnsCount);
    }

    /**
     * получаем количество колонок виджета, поле "COLUMNS"
     */
    public Integer getColumns() {
        return helpersInit.getBaseHelper().parseInt(COLUMNS_INPUT.val());
    }

    /**
     * получаем количество строк виджета, поле "ROWS"
     */
    public Integer getRows() {
        return helpersInit.getBaseHelper().parseInt(ROWS_INPUT.val());
    }

    /**
     * устанавливаем количество строк виджета, поле "ROWS"
     */
    public Integer chooseRows(int... rows) {
        String rowsCount = rows.length > 0 ?
                String.valueOf(rows[0]) :
                randomNumberFromRange(1, 3);
        sendKey(ROWS_INPUT, rowsCount + Keys.ENTER);
        return helpersInit.getBaseHelper().parseInt(rowsCount);
    }

    /**
     * устанавливаем поле "IMPRESSION EVERY"
     */
    public Integer chooseImpressionsPassage(int... impr) {
        int[] mass = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        int impressions = impr.length > 0 ?
                impr[0] :
                getRandomFromArray(mass);
        sendKey(PASSAGE_IMPRESSIONS_INPUT, String.valueOf(impressions) + Keys.ENTER);
        return impressions;
    }

    /**
     * проверяем что в конструкторе виджета отображается верное количество колонок(COLUMNS) тизеров
     * а так же в input подтягивается верное количество колонок(COLUMNS)
     */
    public boolean checkColumns(Integer columns) {
        return helpersInit.getBaseHelper().checkDataset(columns, $$(TEASERS_SIZE).size()) &
                helpersInit.getBaseHelper().checkDataset(columns, helpersInit.getBaseHelper().parseInt(COLUMNS_INPUT.val()));
    }

    public boolean checkColumnsInStand(Integer columns) {
        return helpersInit.getBaseHelper().checkDataset(columns, getElementsIfHasShadowDom(TEASERS_SIZE).size());
    }

    /**
     * проверяем что в конструкторе виджета отображается верное количество строк(ROWS) тизеров
     * а так же в input подтягивается верное количество строк(ROWS)
     */
    public boolean checkRows(Integer rows) {
        return rows == $$(TEASERS_SIZE).size() &&
                rows.equals(helpersInit.getBaseHelper().parseInt(ROWS_INPUT.val()));
    }

    /**
     * проверяем что в конструкторе виджета отображается верное количество строк(ROWS) и колонок(COLUMNS) - их сумма тизеров
     * а так же в input подтягивается верное количество строк(ROWS) и колонок(COLUMNS)
     */
    public boolean checkRowsAndColumns(Integer rows, Integer columns) {
        int countTeasers = rows * columns;
        int teasersSize = $$(TEASERS_SIZE).size();
        log.info("checkRowsAndColumns: rows * columns: " + countTeasers + " == TEASERS_SIZE in constructor: " + teasersSize);
        return countTeasers == teasersSize &&
                columns.equals(helpersInit.getBaseHelper().parseInt(COLUMNS_INPUT.val())) &&
                rows.equals(helpersInit.getBaseHelper().parseInt(ROWS_INPUT.val()));
    }

    /**
     * выбор количество отображаемых колонок для мобильных устройств "COLUMNS (MOBILE)"
     */
    public String chooseColumnsMobile() {
        String columns = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(MOBILE_COLUMNS_SELECT);
        log.info("chooseColumnsMobile: " + columns);
        return columns;
    }

    /**
     * выбор количество отображаемых колонок для мобильных устройств "COLUMNS (MOBILE)"
     */
    public String getColumnsMobile() {
        return helpersInit.getBaseHelper().getSelectedValue(MOBILE_COLUMNS_SELECT);
    }

    /**
     * проверяем выбор количество отображаемых колонок для мобильных устройств "COLUMNS (MOBILE)"
     */
    public boolean checkColumnsMobile(String columns) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(MOBILE_COLUMNS_SELECT, columns);
    }

    /**
     * включаем/выключаем тумблер "RERUN ADS"
     */
    public void switchRerunAdsTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, IN_SITE_NOTIFICATION_RERUN_ADS_SWITCH);
    }

    /**
     * проверяем состояние тумблера "RERUN ADS"
     */
    public boolean checkRerunAdsTumbler(boolean isSwitchOn) {
        return IN_SITE_NOTIFICATION_RERUN_ADS_SWITCH.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    public void switchLabelTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, IN_SITE_NOTIFICATION_LABEL_SWITCH);
    }

    public boolean checkLabelTumbler(boolean isSwitchOn) {
        return IN_SITE_NOTIFICATION_LABEL_SWITCH.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    public boolean checkLabelTumblerInConstructor(boolean isSwitchOn) {
        SelenideElement label = getElementIfHasShadowDom(LABEL_IMAGE_LOCATOR);
        return isSwitchOn ?
                label.getCssValue("display").matches("flex|block") :
                (!label.exists() || helpersInit.getBaseHelper().checkDatasetEquals(label.getCssValue("display"), "none"));
    }

    /**
     * выбор темы виджета "THEME"
     */
    public String chooseTheme() {
        return chooseAndSetColor(THEME_INPUT);
    }

    /**
     * выбор цвета "BUTTON" для банера
     */
    public String chooseColorButtonBanner() {
        return chooseAndSetColor(BANNER_COLOR_BUTTON_INPUT);
    }

    /**
     * выбор цвета "COLOR BLUR" для банера
     */
    public String chooseColorBlurBanner() {
        return chooseAndSetColor(BANNER_BLUR_INPUT);
    }

    /**
     * choose random type 'Blur' banner
     */
    public String chooseTypeBannerBlur() {
        return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(BANNER_BLUR_SELECT);
    }

    /**
     * включаем/выключаем тумблер "BANNER BUTTON"
     */
    public void switchBannerButtonTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, BANNER_COLOR_SWITCH);
    }

    /**
     * включаем/выключаем тумблер "BUTTON EFFECT"
     */
    public void switchBannerButtonEffectTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, BANNER_BUTTON_EFFECT_SWITCH);
    }

    /**
     * получаем активную тему виджета "THEME"
     */
    public String getTheme() {
        return THEME_INPUT.val();
    }

    /**
     * проверяем что выбранная тема отображается в input и в конструкторе при наведении на тизер
     * subType - another type
     */
    public boolean checkTheme(String theme) {
        if (getElementIfHasShadowDom(MGL_STYLE).isDisplayed()) {
            return checkColorInConstructor(MGL_STYLE, theme, "background");
        }
        getElementIfHasShadowDom(FIRST_TEASER_IN_CONSTRUCTOR).hover();
        sleep(500);
        return checkColorInInput(THEME_INPUT, theme) &&
                checkColorInConstructor(THEME_STYLE, theme);
    }

    /**
     * проверяем что выбранная тема отображается в input и в конструкторе при наведении на тизер
     * subType - another type
     */
    public boolean checkThemeInMobileExit(String theme) {
        return checkColorInInput(THEME_INPUT, theme) &
                checkColorInConstructor(MGLINE_STYLE, theme, "border-color");
    }

    public boolean checkThemeForMobileExitInConstructor(String theme) {
        return checkColorInConstructor(MGLINE_STYLE, theme, "border-color");
    }

    /**
     * check theme in constructor
     */
    public boolean checkThemeInConstructor(WidgetTypes.SubTypes subType, String theme) {
        SelenideElement mgHead = getElementIfHasShadowDom(MGHEAD_STYLE);
        try {
            getElementIfHasShadowDom(FIRST_TEASER_IN_CONSTRUCTOR).hover();
            sleep(1500);
            if (subType == WidgetTypes.SubTypes.BANNER_300x250_3)
                return checkColorInConstructor(MGL_STYLE, theme, "background");
            return checkColorInConstructor(THEME_STYLE, theme);
        } finally {
            if (mgHead.isDisplayed()) mgHead.hover();
        }
    }

    /**
     * выбор AUTOPLACEMENT
     * только для IN-ARTICLE
     */
    public String chooseAutoplacement(String autoplacement) {
        String placement = autoplacement;
        if(customAutoplacement) {
            helpersInit.getBaseHelper().selectCurrentValInSelectListJS(AUTOPLACEMENT_SELECT, placement);
        }
        else {
            placement = helpersInit.getBaseHelper().selectRandomValSelectListDashJs(AUTOPLACEMENT_SELECT);
        }
        log.info("chooseAutoplacement: " + placement);
        return placement;
    }

    public boolean checkPopUpTitleInStand(String popUpTitle) {
        return helpersInit.getBaseHelper().checkDatasetEquals(getElementIfHasShadowDom(POPUP_TITLE_LABEL_IN_STAND).text(), popUpTitle);
    }

    /**
     * Проверяем:
     * 1. проверяем что в селекте "AUTOPLACEMENT" отображается верное значение
     * 2. проверяем что в toolTip отображается верный тайтл
     */
    public boolean checkAutoplacement(String autoplacement) {
        boolean result = false;
        try {
            sleep(500);
            // наводим курсор на тултип для получения атрибута
            String path = helpersInit.getBaseHelper().getTextAndWriteLog(AUTOPLACEMENT_TOOLTIP.
                    hover()
                    .attr("aria-describedby"));

            String title = helpersInit.getBaseHelper().getTextAndWriteLog($("#" + path + "-content").shouldBe(visible).text());

            switch (autoplacement) {
                case "off" -> result = title.matches(autoplacementOffValue + "|" + autoplacementOffValueRu + "|" + autoplacementOffValueUa);
                case "middle", "top", "bottom" -> result = title.matches(autoplacementOnValue + "|" + autoplacementOnValueRu + "|" + autoplacementOnValueUa);
                case "attention-based" -> result = title.matches(autoplacementAttentionBasedValue + "|" + autoplacementAttentionBasedValueRu + "|" + autoplacementAttentionBasedValueUa);
            }

            return result &&
                    helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(AUTOPLACEMENT_SELECT, autoplacement));
        } finally {
            // скрываем подсказку - убираем курсор с тултипа
            DESKTOP_BUTTON.hover();
            QTIP_CONTENT.shouldNotBe(visible);
        }
    }

    public boolean checkPassageImpressions(Integer rows) {
        return rows.equals(helpersInit.getBaseHelper().parseInt(PASSAGE_IMPRESSIONS_INPUT.val()));
    }

    /**
     * проверяем состояние тумблера "INFINITE SCROLL"
     */
    public boolean checkInfiniteScroll(boolean isSwitchOn) {
        return INFINITE_SCROLL_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    /**
     * включаем/выключаем тумблер "INFINITE SCROLL"
     */
    public void switchInfiniteScrollTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, INFINITE_SCROLL_TUMBLER);
    }

    ////////////////////////// MOBILE BLOCK - START /////////////////////////

    /**
     * включаем/выключаем тумблер "TOASTER"
     */
    public void switchToasterTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, TOASTER_TUMBLER);
    }

    /**
     * включаем/выключаем тумблер "DRAG DOWN"
     */
    public void switchDragDownTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, DRAG_DOWN_TUMBLER);
    }

    /**
     * включаем/выключаем тумблер "INTERSTITIAL"
     */
    public void switchInterstitialTumbler(boolean isSwitchOn) {
        switchTumblerInDash(isSwitchOn, INTERSTITIAL_TUMBLER);
    }

    /**
     * устанавливаем значение в поле "FREQUENCY CAPPING (IMPRESSIONS)"
     */
    public String setFrequencyCappingImpressions(String frequencyCappingImpressions) {
        String cap = frequencyCappingImpressions == null ?
                randomNumberFromRange(3, 20) :
                frequencyCappingImpressions;
        sendKey(FREQUENCY_CAPPING_IMPRESSIONS_INPUT, cap + Keys.ENTER);
        return cap;
    }

    /**
     * устанавливаем значение в поле "FREQUENCY CAPPING (MINUTES)"
     */
    public String setFrequencyCappingMinutes(String frequencyCappingMinutes) {
        String cap = frequencyCappingMinutes == null ?
                randomNumberFromRange(3, 20) :
                frequencyCappingMinutes;
        sendKey(FREQUENCY_CAPPING_MINUTES_INPUT, cap + Keys.ENTER);
        return cap;
    }

    /**
     * устанавливаем значение в поле SHOW AFTER N-TH INTERACTION
     */
    public String setShowAfterInteraction(String showAfterInteraction) {
        String cap = showAfterInteraction == null ?
                randomNumberFromRange(3, 20) :
                showAfterInteraction;
        sendKey(SHOW_AFTER_INTERACTION_INPUT, cap + Keys.ENTER);
        return cap;
    }

    public String setToasterInactivityTime(String inactivityTime) {
        String cap = inactivityTime == null ?
                randomNumberFromRange(1, 120) :
                inactivityTime;
        sendKey(TOASTER_INACTIVITY_TIME_INPUT, cap + Keys.ENTER);
        return cap;
    }

    /**
     * проверяем состояние тумблера "TOASTER"
     */
    public boolean checkSwitchToasterTumbler(boolean isSwitchOn) {
        return TOASTER_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    /**
     * проверяем состояние тумблера "DRAG DOWN"
     */
    public boolean checkSwitchDragDownTumbler(boolean isSwitchOn) {
        return DRAG_DOWN_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    /**
     * проверяем состояние тумблера "INTERSTITIAL"
     */
    public boolean checkSwitchInterstitialTumbler(boolean isSwitchOn) {
        return INTERSTITIAL_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    /**
     * проверяем значение (value) поля "FREQUENCY CAPPING (IMPRESSIONS)"
     */
    public boolean checkFrequencyCappingImpressions(boolean isSwitchOn, String value) {
        return !isSwitchOn || helpersInit.getBaseHelper().checkDatasetEquals(FREQUENCY_CAPPING_IMPRESSIONS_INPUT.val(), value);
    }

    /**
     * проверяем значение (value) поля "FREQUENCY CAPPING (MINUTES)"
     */
    public boolean checkFrequencyCappingMinutes(boolean isSwitchOn, String value) {
        return !isSwitchOn || helpersInit.getBaseHelper().checkDatasetEquals(FREQUENCY_CAPPING_MINUTES_INPUT.val(), value);
    }

    public boolean checkInactivityTime(boolean isSwitchOn, String value) {
        return !isSwitchOn || helpersInit.getBaseHelper().checkDatasetEquals(TOASTER_INACTIVITY_TIME_INPUT.val(), value);
    }

    /**
     * проверяем значение (value) поля "SHOW AFTER N-TH INTERACTION"
     */
    public boolean checkShowAfterInteraction(boolean isSwitchOn, String value) {
        return !isSwitchOn || helpersInit.getBaseHelper().checkDatasetEquals(SHOW_AFTER_INTERACTION_INPUT.val(), value);
    }

    ////////////////////////// MOBILE BLOCK - FINISH /////////////////////////


    ////////////////////////// DETAILED SETTINGS - START //////////////////////
    //////////////////////////////////// WIDGET - START /////////////////////////////////

    /**
     * проверяем поле "BACKGROUND COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkBackgroundColor(String color) {
        return checkColorInConstructor(MGBOX_STYLE, color, "background-color");
    }

    /**
     * проверяем поле "BACKGROUND COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkBackgroundColorInConstructor(String color) {
        return checkColorInConstructor(MGBOX_STYLE, color, "background-color");
    }

    /**
     * проверяем поле "WIDGET BORDER"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkWidgetBorder(String border) {
        return checkSizeInConstructor(MGBOX_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "WIDGET BORDER"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkWidgetBorderInConstructor(String border) {
        return checkSizeInConstructor(MGBOX_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "BACKGROUND COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkBorderColor(String color) {
        return checkColorInConstructor(MGBOX_STYLE, color, "border-top-color");
    }

    /**
     * проверяем поле "BACKGROUND COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkBorderColorInConstructor(String color) {
        return checkColorInConstructor(MGBOX_STYLE, color, "border-top-color");
    }


    /**
     * проверяем поле "TEASER BORDER WIDTH"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkTeaserBorderWidth(String border) {
        return checkSizeInConstructor(MGLINE_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "TEASER BORDER WIDTH"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkTeaserBorderWidthInConstructor(String border) {
        return checkSizeInConstructor(MGLINE_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "BACKGROUND COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkTeaserBorderColor(String color) {
        return checkColorInConstructor(MGLINE_STYLE, color, "border-top-color");
    }

    //////////////////////////////////// WIDGET - FINISH /////////////////////////////////


    //////////////////////////////////// IMAGE - START /////////////////////////////////

    /**
     * проверяем выбранный формат
     * в конструкторе информера
     */
    public boolean checkImageFormat(String format) {
        log.info("checkImageFormat START");
        List withAndHeight = tickersImageFormat.getImageWidthAndHeight(helpersInit.getBaseHelper().parseInt(format));
        String width = withAndHeight.get(0).toString();
        String height = withAndHeight.get(1).toString();

        return helpersInit.getBaseHelper().checkDatasetContains(getElementIfHasShadowDom(MCIMG_STYLE).getCssValue("max-width"), width) &
                helpersInit.getBaseHelper().checkDatasetContains(getElementIfHasShadowDom(MCIMG_STYLE).getCssValue("max-height"), height);
    }

    /**
     * проверяем поле "IMAGE BORDER"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImageBorder(String border) {
        return checkSizeInConstructor(MCIMG_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "IMAGE BORDER"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImageBorderInConstructor(String border, String... imageWithText) {
        String styleElement = imageWithText.length > 0 ? IMAGE_WITH_TEXT_STYLE : MCIMG_STYLE;
        return checkSizeInConstructor(styleElement, border, "border-top-width");
    }

    /**
     * получаем поле "IMAGE BORDER"
     */
    public boolean checkImageBorderForHeadline(String border) {
        return checkSizeInConstructor(IMAGE_WITH_TEXT_STYLE, border, "border-top-width");
    }

    /**
     * проверяем поле "IMAGE BORDER COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImageBorderColor(String color) {
        return checkColorInConstructor(MCIMG_STYLE, color, "border-top-color");
    }

    /**
     * проверяем поле "IMAGE BORDER COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImageBorderColorInConstructor(String color, String... imageWithText) {
        String styleElement = imageWithText.length > 0 ? IMAGE_WITH_TEXT_STYLE : MCIMG_STYLE;
        return checkColorInConstructor(styleElement, color, "border-top-color");
    }

    /**
     * получаем поле "IMAGE BORDER COLOR"
     */
    public boolean checkImageBorderColorForHeadline(String color) {
        return checkColorInConstructor(IMAGE_WITH_TEXT_STYLE, color, "border-top-color");
    }

    /**
     * проверяем поле "IMAGE PADDING"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImagePadding(String border) {
        return checkSizeInConstructor(MCIMG_DIV_STYLE, border, "padding-top");
    }

    /**
     * проверяем поле "IMAGE PADDING"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkImagePaddingInConstructor(String border) {
        return checkSizeInConstructor(MCIMG_DIV_STYLE, border, "padding-top");
    }

    /**
     * проверяем что при включённом "IMAGE ZOOM ON HOVER" - при наведении на картинку она увеличивается(изменяет стиль)
     */
    public boolean checkImageZoom(boolean isSwitchOn) {
        SelenideElement mghead = getElementIfHasShadowDom(MGHEAD_STYLE);
        SelenideElement firstImage = getElementIfHasShadowDom(FIRST_IMAGE_IN_CONSTRUCTOR);
        SelenideElement hoverImage = getElementIfHasShadowDom(HOVER_IMAGE_STYLE);
        try {
            firstImage.hover();
            if (isSwitchOn) {
                return helpersInit.getBaseHelper().checkDatasetEquals(hoverImage.getCssValue("overflow-x"),"hidden");
            } else {
                return (!hoverImage.exists() ||
                        helpersInit.getBaseHelper().checkDatasetEquals(hoverImage.getCssValue("overflow-x"),"clip"));
            }
        } finally {
            mghead.hover();
        }
    }

    public boolean checkImageZoomInConstructor(boolean isSwitchOn) {
        SelenideElement mghead = getElementIfHasShadowDom(MGHEAD_STYLE);
        SelenideElement firstImage = getElementIfHasShadowDom(FIRST_IMAGE_IN_CONSTRUCTOR);
        SelenideElement hoverImage = getElementIfHasShadowDom(HOVER_IMAGE_STYLE);
        try {
            firstImage.hover();
            if (isSwitchOn) {
                return hoverImage.getCssValue("overflow-x").equals("hidden");
            } else {
                return !getElementIfHasShadowDom(HOVER_IMAGE_STYLE).exists() ||
                        getElementIfHasShadowDom(HOVER_IMAGE_STYLE).getCssValue("overflow-x").equals("clip");
            }
        } finally {
            mghead.hover();
        }
    }

    /**
     * проверяем что при включённом "IMAGE INNER SHADOW" - у всех изображений появляется дополнительный стиль для их затемнения
     */
    public boolean checkImageInnerShadow(boolean isSwitchOn) {
        SelenideElement imageInnerShadowStyle = getElementIfHasShadowDom(IMAGE_INNER_SHADOW_STYLE);
        if (isSwitchOn) {
            return helpersInit.getBaseHelper().checkDatasetEquals(helpersInit.getBaseHelper().getCssValueBeforeJs(imageInnerShadowStyle, "display"),"block");
        } else {
            return helpersInit.getBaseHelper().checkDatasetEquals(helpersInit.getBaseHelper().getCssValueBeforeJs(imageInnerShadowStyle, "display"),"inline");
        }
    }

    /**
     * включаем/выключаем Auto Scroll
     */
    public void switchAutoScroll(boolean isAutoScroll) {
        helpersInit.getBaseHelper().getTextAndWriteLog(isAutoScroll);
        switchTumblerInDash(isAutoScroll, AUTO_SCROLL_TUMBLER);
    }

    public boolean checkAutoScroll(boolean isSwitchOn) {
        return AUTO_SCROLL_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn;
    }

    /**
     * проверяем что при включённом "IMAGE INNER SHADOW" - у всех изображений появляется дополнительный стиль для их затемнения
     */
    public boolean checkImageInnerShadowInConstructor(boolean isSwitchOn) {
        SelenideElement imageInnerShadowStyle = getElementIfHasShadowDom(IMAGE_INNER_SHADOW_STYLE);
        return isSwitchOn ? helpersInit.getBaseHelper().getCssValueBeforeJs(imageInnerShadowStyle, "display").equals("block") :
                helpersInit.getBaseHelper().getCssValueBeforeJs(imageInnerShadowStyle, "display").equals("inline");
    }

    //////////////////////////////////// IMAGE - FINISH /////////////////////////////////


    //////////////////////////////////// HEADLINE - START /////////////////////////////////

    /**
     * проверяем "HEADLINE TEXT FONT" (тип шрифта)
     */
    public boolean checkHeadlineTextFontInConstructor(String textFont){
        return checkTextFont(MCTITLE_A_STYLE, textFont);
    }

    /**
     * выбираем "HEADLINE FONT SIZE" (размер шрифта)
     */
    public String chooseHeadlineFontSize() {
        return chooseAndSetSize(HEADLINE_FONT_SIZE_INPUT);
    }

    /**
     * получаем "HEADLINE FONT SIZE" (размер шрифта)
     */
    public String getHeadlineFontSize() {
        return HEADLINE_FONT_SIZE_INPUT.val();
    }

    /**
     * проверяем "HEADLINE FONT SIZE" (размер шрифта)
     */
    public boolean checkHeadlineFontSize(String fontSize) {
        return checkSizeInInput(HEADLINE_FONT_SIZE_INPUT, fontSize) &&
                checkSizeInConstructor(MCTITLE_A_STYLE, fontSize);
    }

    /**
     * проверяем "HEADLINE FONT SIZE" (размер шрифта)
     */
    public boolean checkHeadlineFontSizeInConstructor(String fontSize) {
        return checkSizeInConstructor(MCTITLE_A_STYLE, fontSize);
    }

    /**
     * заполняем поле "HEADLINE FONT COLOR"
     */
    public String chooseHeadlineFontColor() {
        return chooseAndSetColor(HEADLINE_FONT_COLOR_INPUT);
    }

    /**
     * получаем поле "HEADLINE FONT COLOR"
     */
    public String getHeadlineFontColor() {
        return HEADLINE_FONT_COLOR_INPUT.val();
    }

    /**
     * проверяем поле "HEADLINE FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkHeadlineFontColor(String color) {
        return checkColorInInput(HEADLINE_FONT_COLOR_INPUT, color) &&
                checkColorInConstructor(MCTITLE_A_STYLE, color);
    }

    /**
     * проверяем поле "HEADLINE FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkHeadlineFontColorInConstructor(String color) {
        return checkColorInConstructor(MCTITLE_A_STYLE, color);
    }

    /**
     * проверяенм выбраную ранее позицию текста "HEADLINE"
     * typeText - text, desc-text, domain-text
     */
    public boolean checkHeadlinePosition(String position) {
        return checkTextPosition(MCTITLE_STYLE, position);
    }

    /**
     * проверяенм выбраную ранее позицию текста "HEADLINE"
     */
    public boolean checkHeadlinePositionInConstructor(String position) {
        return checkTextPositionInConstructor(MCTITLE_STYLE, position);
    }

    /**
     * проверяем проставление для блока "HEADLINE" стилей текста (bold, italic, underline)
     * метод принимает состояние стилей - вкл/выкл (все)
     * typeText - text, desc-text, domain-text
     */
    public boolean checkHeadlineStyleText(ArrayList<String> list) {
        return checkStyleTextInConstructor(MCTITLE_A_STYLE, list);
    }

    public boolean checkHeadlineStyleTextInConstructor(ArrayList<String> list) {
        return checkStyleTextInConstructor(MCTITLE_A_STYLE, list);
    }

    /**
     * проверяем "HEADLINE TEXT TRANSFORM" (тип шрифта) в конструкторе
     */
    public boolean checkHeadlineTextTransformInConstructor(String format) {
        return helpersInit.getBaseHelper().checkDatasetContains(
                getElementIfHasShadowDom(MCTITLE_STYLE).getCssValue("text-transform"),
                format
        );
    }

    //////////////////////////////////// HEADLINE - FINISH /////////////////////////////////


    //////////////////////////////////// WIDGET TITLE - START /////////////////////////////////

    /**
     * проверяем "WIDGET TITLE TEXT TRANSFORM" (тип шрифта) в конструкторе
     */
    public boolean checkWidgetTitleTextTransformInConstructor(String format) {
        return getElementIfHasShadowDom(MGHEAD_STYLE).getCssValue("text-transform").contains(format);
    }

    /**
     * проверяем "WIDGET TITLE TEXT FONT" (формат шрифта)
     * в селекте и в конструкторе
     */
    public boolean checkWidgetTitleTextFont(String font) {
        return checkTextFont(MGHEAD_STYLE, font);
    }

    /**
     * проверяем "WIDGET TITLE TEXT FONT" (формат шрифта)
     * в селекте и в конструкторе
     */
    public boolean checkWidgetTitleTextFontInConstructor(String font){
        return checkTextFont(MGHEAD_STYLE, font);
    }

    /**
     * проверяем "WIDGET TITLE FONT SIZE" (размер шрифта)
     */
    public boolean checkWidgetTitleFontSize(String fontSize) {
        return checkSizeInConstructor(MGHEAD_STYLE, fontSize);
    }

    /**
     * проверяем "WIDGET TITLE FONT SIZE" (размер шрифта)
     */
    public boolean checkWidgetTitleFontSizeInConstructor(String fontSize) {
        return checkSizeInConstructor(MGHEAD_STYLE, fontSize);
    }

    /**
     * проверяем поле "WIDGET TITLE FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkWidgetTitleFontColor(String color) {
        return checkColorInConstructor(MGHEAD_STYLE, color);
    }

    /**
     * проверяем поле "WIDGET TITLE FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkWidgetTitleFontColorInConstructor(String color) {
        return checkColorInConstructor(MGHEAD_STYLE, color);
    }

    //////////////////////////////////// WIDGET TITLE - FINISH /////////////////////////////////


    //////////////////////////////////// DOMAIN - START ////////////////////////////////

    /**
     * проверяем "DOMAIN TEXT FONT" (тип шрифта)
     */
    public boolean checkDomainTextFont(String textFont) {
        return checkTextFont(MCDOMAIN_A_STYLE, textFont);
    }

    /**
     * проверяем "DOMAIN TEXT FONT" (тип шрифта)
     */
    public boolean checkDomainTextFontInConstructor(String textFont) {
        return checkTextFont(MCDOMAIN_A_STYLE, textFont);
    }

    /**
     * выбираем "DOMAIN FONT SIZE" (размер шрифта)
     */
    public String chooseDomainFontSize() {
        return chooseAndSetSize(DOMAIN_FONT_SIZE_INPUT);
    }

    /**
     * получаем "DOMAIN FONT SIZE" (размер шрифта)
     */
    public String getDomainFontSize() {
        return DOMAIN_FONT_SIZE_INPUT.val();
    }

    /**
     * проверяем "DOMAIN FONT SIZE" (размер шрифта)
     */
    public boolean checkDomainFontSize(String fontSize) {
        return checkSizeInInput(DOMAIN_FONT_SIZE_INPUT, fontSize) &&
                checkSizeInConstructor(MCDOMAIN_A_STYLE, fontSize);
    }

    /**
     * проверяем "DOMAIN FONT SIZE" (размер шрифта)
     */
    public boolean checkDomainFontSizeInConstructor(String fontSize) {
        return checkSizeInConstructor(MCDOMAIN_A_STYLE, fontSize);
    }

    /**
     * заполняем поле "DOMAIN FONT COLOR"
     */
    public String chooseDomainFontColor() {
        return chooseAndSetColor(DOMAIN_FONT_COLOR_INPUT);
    }

    /**
     * получаем поле "DOMAIN FONT COLOR"
     */
    public String getDomainFontColor() {
        return DOMAIN_FONT_COLOR_INPUT.val();
    }

    /**
     * проверяем поле "DOMAIN FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkDomainFontColor(String color) {
        return checkColorInInput(DOMAIN_FONT_COLOR_INPUT, color) &&
                checkColorInConstructor(MCDOMAIN_A_STYLE, color);
    }

    /**
     * проверяем поле "DOMAIN FONT COLOR"
     * проверяем переданный стиль в конструкторе
     */
    public boolean checkDomainFontColorInConstructor(String color) {
        return checkColorInConstructor(MCDOMAIN_A_STYLE, color);
    }


    /**
     * проверяенм выбраную ранее позицию текста "DOMAIN"
     * typeText - text, desc-text, domain-text
     */
    public boolean checkDomainPosition(String position) {
        return checkTextPosition(MCDOMAIN_A_STYLE, position);
    }

    /**
     * проверяенм выбраную ранее позицию текста "DOMAIN"
     */
    public boolean checkDomainPositionInConstructor(String position, String... dopStyle) {
        return checkTextPositionInConstructor(
                dopStyle.length > 0 ?
                        MCDOMAIN_A_STYLE :
                        MCDOMAIN_STYLE,
                position);
    }

    /**
     * проверяем проставление для блока "DOMAIN" стилей текста (bold, italic, underline)
     * метод принимает состояние стилей - вкл/выкл (все)
     * typeText - text, desc-text, domain-text
     */
    public boolean checkDomainStyleText(ArrayList<String> list) {
        return checkStyleTextInConstructor(MCDOMAIN_A_STYLE, list);
    }

    /**
     * проверяем проставление для блока "DOMAIN" стилей текста (bold, italic, underline)
     * метод принимает состояние стилей - вкл/выкл (все)
     */
    public boolean checkDomainStyleTextInConstructor(ArrayList<String> list) {
        return checkStyleTextInConstructor(MCDOMAIN_A_STYLE, list);
    }

    //////////////////////////////////// DOMAIN - FINISH ////////////////////////////////


    //////////////////////////////////// POSITION - START ////////////////////////////////

    /**
     * проверяем "POSITION" - widget position (in-site notification)
     */
    public boolean checkPositionInSiteNotification(String position) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(IN_SITE_NOTIFICATION_POSITION_SELECT, position);
    }

    public boolean checkFrequencyInSiteNotification(boolean rerunAds, String frequency) {
        return !rerunAds || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForDashJs(IN_SITE_NOTIFICATION_FREQUENCY_SELECT, frequency);
    }

    /**
     * choose "POSITION" - widget position (in-site notification)
     */
    public String choosePositionInSiteNotification() {
        return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(IN_SITE_NOTIFICATION_POSITION_SELECT);
    }

    public String chooseFrequencyInSiteNotification() {
        return helpersInit.getBaseHelper().selectRandomValSelectListDashJs(IN_SITE_NOTIFICATION_FREQUENCY_SELECT);
    }

    /**
     * проверяем "POSITION" -расположение тизеров в конструкторе виджета
     */
    public boolean checkPositionTeasers(String position) {
        String floatPosition = positionTeaserHelp();
        return position.equals("bottom") ?
                floatPosition.equals("none") :
                position.equals(floatPosition);
    }

    /**
     * проверяем "POSITION" -расположение тизеров в конструкторе виджета
     */
    public boolean checkPositionTeasersInConstructor(String position) {
        String floatPosition = positionTeaserHelp();
        return position.equals("bottom") ?
                floatPosition.equals("none") :
                position.equals(floatPosition);
    }

    /**
     * "POSITION" helper
     */
    private String positionTeaserHelp() {
        String floatPosition = getElementIfHasShadowDom(MCPOSITION_STYLE).getCssValue("float");
        switch (floatPosition) {
            case "right" -> floatPosition = "left";
            case "left" -> floatPosition = "right";
            default -> {
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(floatPosition);
    }

    //////////////////////////////////// POSITION - FINISH ////////////////////////////////

    //////////////////////////////////// CAROUSEL - START ////////////////////////////////

    /**
     * проверяем "CAROUSEL margin-right"
     */
    public boolean checkTeaserMargin(String margin, String margin_side) {
        return checkSizeInConstructor(MGLINE_STYLE, margin, margin_side);
    }

    /**
     * проверяем "CAROUSEL margin-right" in constructor
     */
    public boolean checkTeaserMarginInConstructor(String margin, String margin_side) {
        return checkSizeInConstructor(MGLINE_STYLE, margin, margin_side);
    }

    /**
     * проверяем "CAROUSEL border-radius"
     */
    public boolean checkTeaserBorderRadius(String borderRadius) {
        return checkSizeInConstructor(MGLINE_STYLE, borderRadius, "border-radius");
    }

    /**
     * проверяем "CAROUSEL border-radius" in constructor
     */
    public boolean checkTeaserBorderRadiusInConstructor(String borderRadius) {
        return checkSizeInConstructor(MGLINE_STYLE, borderRadius, "border-radius");
    }

    /**
     * проверяем "CAROUSEL teaser width"
     */
    public boolean checkTeaserWidth(String width, String margin_side) {
        return checkTeaserWidthInConstructor(width, margin_side);
    }

    /**
     * проверяем "CAROUSEL teaser width" in constructor
     */
    public boolean checkTeaserWidthInConstructor(String width, String margin_side) {
        String widthInConstructor = margin_side.contains("bottom") ?
                String.valueOf((helpersInit.getBaseHelper().parseInt(width.replace("px", "")) + 95)) :
                width;
        return checkSizeInConstructor(MGLINE_STYLE, widthInConstructor, "width");
    }

    /**
     * проверяем "CAROUSEL teaser width"
     */
    public boolean checkTeaserHeight(String height) {
        return checkSizeInConstructor(MGLINE_STYLE, height, "height");
    }

    /**
     * проверяем "CAROUSEL teaser width" in constructor
     */
    public boolean checkTeaserHeightInConstructor(String height) {
        return checkSizeInConstructor(MGLINE_STYLE, height, "height");
    }

    /**
     * check headline size in constructor on formula
     * var fontSize1 = self.autoSizeTitle - Math.round(title.clientHeight / self.autoSizeTitle);
     */
    public boolean checkHeadlineSizeCarouselInStand(String size) {
        log.info("checkHeadlineSizeCarousel -> start");
        int autoSizeTitle = helpersInit.getBaseHelper().parseInt(size.replaceAll("[a-z]", ""));
        int clientHeight = helpersInit.getBaseHelper().parseInt(getClientHeight().replaceAll("[a-z]", ""));
        int fontSize = autoSizeTitle - Math.round(clientHeight / autoSizeTitle);

        //input
        int titleFromConstructor = helpersInit.getBaseHelper().parseInt(getElementIfHasShadowDom(MCTITLE_A_STYLE).getCssValue("font-size").replaceAll("[a-z]", ""));
        log.info("checkHeadlineSizeCarousel:" + fontSize + "==" + titleFromConstructor);
        return helpersInit.getBaseHelper().comparisonData(titleFromConstructor, fontSize, 3);
    }

    public String getClientHeight(){
        return executeJavaScript(
                "// get styles for MCTITLE_A_STYLE\" +\n" +
                        "var title = $('" + MCTITLE_A_STYLE + "');\n" +
                        "var fontSize = title.css('font-size');\n" +
                        "var lineHeight = title.css('line-height');\n" +

                        "// remove styles for MCTITLE_A_STYLE\" +\n" +
                        "title.css({'font-size': ''});\n" +
                        "title.css({'line-height': ''});\n" +

                        "// get title.clientHeight\" +\n" +
                        "var clientHeight = $('" + MCTITLE_STYLE + "').css('height');\n" +

                        "// return old style values for MCTITLE_A_STYLE\"\n" +
                        "title.css('font-size', fontSize);\n" +
                        "title.css('line-height', lineHeight);\n" +
                        "return clientHeight;\n");
    }

    /**
     * включаем/выключаем TEASER FIXED WIDTH
     */
    public void switchDomainShow(boolean isTeaserFixedWidth) {
        switchTumblerInDash(isTeaserFixedWidth, DOMAIN_SHOW_TUMBLER);
    }

    public boolean checkDomainShow(boolean isSwitchOn){
        return DOMAIN_SHOW_TUMBLER.text().equalsIgnoreCase("on") == isSwitchOn &
                DOMAIN_SETTINGS_BLOCK.isDisplayed() == isSwitchOn &
                ((getElementIfHasShadowDom(MCDOMAIN_STYLE).isDisplayed() == isSwitchOn) ||
                        (getElementIfHasShadowDom(MCDOMAIN_TOP_A_STYLE).isDisplayed() == isSwitchOn));
    }

    /**
     * проверяем что при включённом "TEASER CARD SHADOW" - у всех изображений появляется дополнительный стиль для их затемнения
     */
    public boolean checkTeaserCardShadow(boolean isSwitchOn) {
        try {
            getElementIfHasShadowDom(FIRST_IMAGE_IN_CONSTRUCTOR).hover();
            SelenideElement themeInCarousel = getElementIfHasShadowDom(THEME_IM_CAROUSEL_STYLE);
            if (isSwitchOn) {
                return themeInCarousel.exists() &
                        helpersInit.getBaseHelper().checkDatasetEquals(
                                convertRgbaToHex(
                                    themeInCarousel.getCssValue("box-shadow")),
                                "#000");
            } else {
                return themeInCarousel.exists() &
                        helpersInit.getBaseHelper().checkDatasetEquals(themeInCarousel.getCssValue("box-shadow"),"none");
            }
        } finally {
            getElementIfHasShadowDom(LOGO).hover();
        }
    }

    /**
     * проверяем что при включённом "TEASER CARD SHADOW" - у всех изображений появляется дополнительный стиль для их затемнения
     */
    public boolean checkTeaserCardShadowInConstructor(boolean isSwitchOn) {
        try {
            getElementIfHasShadowDom(FIRST_IMAGE_IN_CONSTRUCTOR).hover();
            SelenideElement themeInCarousel = getElementIfHasShadowDom(THEME_IM_CAROUSEL_STYLE);
            if (isSwitchOn) {
                return themeInCarousel.exists() &&
                        helpersInit.getBaseHelper().checkDatasetEquals(
                                convertRgbaToHex(
                                    themeInCarousel.getCssValue("box-shadow")),
                                "#000"
                        );
            } else {
                return themeInCarousel.exists() &&
                        helpersInit.getBaseHelper().checkDatasetEquals(themeInCarousel.getCssValue("box-shadow"), "none");
            }
        } finally {
            getElementIfHasShadowDom(LOGO).hover();
        }
    }

    /**
     * check work tumbler - 'Teaser Fixed Width'
     * switch on - .mgline width must be more than teaserWidth(field) (67%)
     * switch off - .mgline width must be equals teaserWidth(field) (67%)
     */
    public boolean checkTeaserFixedWidth(boolean isTeaserFixedWidth, String teaserWidth) {
        try {
            MOBILE_BUTTON.click();
            double mobileWidth = parseDouble(getElementIfHasShadowDom(MGLINE_STYLE).getCssValue("width").replaceAll("[a-z]", ""));
            int desktopWidth = helpersInit.getBaseHelper().parseInt(teaserWidth.replaceAll("[a-z]", ""));
            log.info("checkTeaserFixedWidth: mobileWidth=" + mobileWidth + ", desktopWidth=" + desktopWidth);
            if (isTeaserFixedWidth) {
                return mobileWidth > desktopWidth;
            } else {
                return mobileWidth == desktopWidth;
            }
        } finally {
            DESKTOP_BUTTON.click();
        }
    }

    /**
     * Пользователь совершил scroll вниз до области видимости виджета
     * 1.1) Система определила что виджет попал в зону отображения на 100%
     * 1.2) Система выждала 3000ms
     * 1.3) Система запустила эффект autoSlide на 2 тизера влево (1000ms на эффект)
     * 1.4) Система выждала 1000ms
     * 1.5) Система совершила возврат в изначальное положение (1000ms на обратный эффект)
     */
    public boolean checkAutoScrollInStand(boolean isAutoScroll) {
        double val_0, val_1, val_2, val_3;

        // Система определила что виджет попал в зону отображения на 100%
        val_0 = getTransform();


        // Система выждала 3000ms
        sleep(3000);
        //Система запустила эффект autoSlide на 2 тизера влево (1000ms на эффект)
        val_1 = getTransform();


        // Система выждала 1000ms + (1000ms на эффект)
        sleep(2000);
        val_2 = getTransform();


        // Система совершила возврат в изначальное положение (1000ms на обратный эффект)
        sleep(1000);
        val_3 = getTransform();

        return helpersInit.getBaseHelper().getTextAndWriteLog(
                isAutoScroll
                        ?
                        val_0 == 0 && ((val_1 < val_0) | (val_2 < val_0)) & val_3 == 0
                        :
                        val_0 == val_1 && val_1 == val_2 & val_2 == val_3 & val_3 == 0
        );
    }

    private double getTransform() {
        String val = helpersInit.getBaseHelper().getTextAndWriteLog(getElementIfHasShadowDom(MGCONTAINER_STYLE).getCssValue("transform"));
        if (val.equals("none")) {
            return 0;
        }
        val = val.split(",")[4];
        return parseDouble(val);
    }

    //////////////////////////////////// CAROUSEL - FINISH ////////////////////////////////

    /////////////////////////////////// OTHER - START ////////////////////////////////////////////

    /**
     * сохраняем виджет "SAVE"
     */
    public void clickButtonCreateWidget() {
        int timer = 0;

        log.info("Click save widget button");
        WIDGET_SAVE_BUTTON.shouldBe(visible).scrollTo().click(ClickOptions.usingJavaScript());
        isConfirmDisplayed();
        waitForAjaxLoader();
        checkErrors();

        //wait popUp or notification(read-only mode) -> re-save widget

        do {
            log.info("POPUP_CLOSE_BUTTON - " + timer);
            if (POPUP_CLOSE_BUTTON.isDisplayed()) {
                break;
            }

            if (helpersInit.getMessageHelper().getMessageDash().equals("")
                    || helpersInit.getMessageHelper().isEmergencyModePresentDash()) {

                sleep(3000);

                WIDGET_SAVE_BUTTON.shouldBe(visible).scrollTo().click(ClickOptions.usingJavaScript());
                waitForAjaxLoader();
                checkErrors();
            }

            else if (timer > 0) {
                sleep(1000);
                WIDGET_SAVE_BUTTON.shouldBe(visible).scrollTo().click(ClickOptions.usingJavaScript());
            }

            timer++;
        }
        while (!POPUP_CLOSE_BUTTON.isDisplayed() && timer < 10);

        // close pop up
        if (isClosePopup) {
            POPUP_CLOSE_BUTTON.click(ClickOptions.usingJavaScript());
            POPUP_CLOSE_BUTTON.shouldBe(hidden, ofSeconds(8));
        }
    }

    /**
     * сохраняем виджет и закрываем Pop-Up с кодом виджета
     */
    public boolean clickCreateAndClosePopUp() {
        clickButtonCreateWidget();
        return !isClosePopup || POPUP_CLOSE_BUTTON.is(hidden);
    }

    /**
     * получаем id созданого виджета
     */
    public int getWidgetIdFromUrl() {
        String url = url();
        log.info(url);
        if (url.contains("/code")) {
            return helpersInit.getBaseHelper().parseInt(url.substring(url.indexOf("id/") + 3, url.indexOf("/code")));
        } else {
            return helpersInit.getBaseHelper().parseInt(url.substring(url.indexOf("id/") + 3));
        }
    }

    public boolean createButtonIsEnabled() {
        return helpersInit.getBaseHelper().checkDatasetContains(WIDGET_SAVE_BUTTON.attr("style"), "opacity: 0.2");
    }

    /**
     * удаление виджета
     */
    public boolean deleteWidget(int widgetId) {
        SelenideElement deleteIcon = $("[data-id='" + widgetId + "'] [class*=archiveWidget]");
        deleteIcon.shouldBe(visible, ofSeconds(15));
        helpersInit.getBaseHelper().waitDisplayedElement(deleteIcon);
        deleteIcon.scrollIntoView(true).click(ClickOptions.usingJavaScript());
        CONFIRM_BUTTON.shouldBe(visible, ofSeconds(8)).click();
        waitForAjax();
        return helpersInit.getBaseHelper().waitDisappearingElement(deleteIcon);
    }

    /**
     * переключаемся на GENERAL tab
     */
    public void switchToGeneralTab() {
        if (GENERAL_TAB.isDisplayed()) GENERAL_TAB.shouldBe(visible).click();
        log.info("switchToGeneralTab");
    }

    public boolean isShowDetailedTab(){ return helpersInit.getBaseHelper().getTextAndWriteLog(DETAILED_TAB.isDisplayed()); }

    /**
     * получаем value стиля "font-family" и форматируем его
     */
    public String getTextFontInnerTextOnStyle(String element) {
        return getElementIfHasShadowDom(element).getCssValue("font-family").replaceAll("\"", "").replaceAll(", ", ",");
    }

    /**
     * вспомогательный метод для выбора цвета в инпуте
     */
    public String chooseAndSetColor(SelenideElement element) {
        String color = randomColor();
        sendKey(element, color + Keys.ENTER);
        return color;
    }

    /**
     * вспомогательный метод для выбора размера шрифта в инпуте
     */
    public String chooseAndSetSize(SelenideElement element) {
        String size = randomNumberFromRange(7, 14);
        sendKey(element, size + Keys.ENTER);
        return size;
    }

    /**
     * вспомогательный метод для проверки цвета в инпуте
     */
    public boolean checkColorInInput(SelenideElement input, String color) {
        return getLogMethod(color, input.val());
    }

    /**
     * вспомогательный метод для проверки цвета в конструкторе
     */
    public boolean checkColorInConstructor(String element, String color, String... styles) {
        String style = styles.length > 0 ? styles[0] : "color";
        String hexColor = getElementIfHasShadowDom(element).getCssValue(style);
        if (hexColor.contains("rgb("))
            hexColor = hexColor.substring(hexColor.indexOf("rgb("), hexColor.lastIndexOf(")") + 1);
        hexColor = convertRgbaToHex(hexColor);
        return getLogMethod(color, color.equals("") ? "" : hexColor);
    }

    /**
     * вспомогательный метод для проверки размера шрифта в инпуте
     */
    public boolean checkSizeInInput(SelenideElement input, String border) {
        if (border.equals("")) border = "0px";
        return getLogMethod(border, input.val());
    }

    /**
     * вспомогательный метод для проверки размера шрифта в конструкторе
     */
    public boolean checkSizeInConstructor(String element, String border, String... styles) {
        if (border.equals("")) border = "0px";
        String style = styles.length > 0 ? styles[0] : "font-size";
        return helpersInit.getBaseHelper().checkDatasetContains(border, getElementIfHasShadowDom(element).getCssValue(style));
    }

    /**
     * вспомогательный метод для проверки Text Font
     */
    public boolean checkTextFont(String element, String textFont) {
        return helpersInit.getBaseHelper().checkDatasetEquals(getTextFontInnerTextOnStyle(element), textFont);
    }

    /**
     * проверяенм выбраную ранее позицию текста "HEADLINE"
     * typeText - text, desc-text, domain-text
     */
    public boolean checkTextPosition(String element, String position) {
        return helpersInit.getBaseHelper().checkDatasetEquals(
                getElementIfHasShadowDom(element).getCssValue("text-align"),
                position
        );
    }

    /**
     * проверяенм выбраную ранее позицию текста "HEADLINE"
     */
    public boolean checkTextPositionInConstructor(String element, String position) {
        return helpersInit.getBaseHelper().checkDatasetEquals(
                getElementIfHasShadowDom(element).getCssValue("text-align"),
                position
        );
    }

    /**
     * проверяем проставление для блока "HEADLINE" стилей текста (bold, italic, underline)
     * метод принимает состояние стилей - вкл/выкл (все)
     * typeText - text, desc-text, domain-text
     */
    public boolean checkStyleTextInConstructor(String element, ArrayList<String> list) {
        int count = 0;
        SelenideElement el = getElementIfHasShadowDom(element);
        String bold = el.getCssValue("font-weight");
        String italic = el.getCssValue("font-style");
        String underline = el.getCssValue("text-decoration");

        for (String style : styles) {
            if (list.contains(style)) {
                switch (style) {
                    case "bold":
                        if (helpersInit.getBaseHelper().checkDatasetEquals(bold, "700")) count++;
                        break;
                    case "italic":
                        if (helpersInit.getBaseHelper().checkDatasetEquals(italic, "italic")) count++;
                        break;
                    case "underline":
                        if (helpersInit.getBaseHelper().checkDatasetContains(underline, "underline")) count++;
                        break;
                }
            } else {
                switch (style) {
                    case "bold":
                        if (helpersInit.getBaseHelper().checkDatasetEquals(bold, "400")) count++;
                        break;
                    case "italic":
                        if (helpersInit.getBaseHelper().checkDatasetEquals(italic, "normal")) count++;
                        break;
                    case "underline":
                        if (helpersInit.getBaseHelper().checkDatasetContains(underline, "none")) count++;
                        break;
                }
            }
        }
        helpersInit.getBaseHelper().getTextAndWriteLog(count + "==" + styles.length);
        return count == styles.length;
    }

    /**
     * устанавливаем title для popUp виджета 'POPUP TITLE'
     */
    public String setPopUpTitle() {
        String popTitle = "popUpTitle_" + BaseHelper.getRandomWord(4);
        POPUP_TITLE_INPUT.val(popTitle);
        return popTitle;
    }

    /**
     * проверяем title для popUp виджета 'POPUP TITLE'
     */
    public boolean checkPopUpTitle(String popUpTitle) {
        return helpersInit.getBaseHelper().checkDatasetEquals(POPUP_TITLE_INPUT.val(), popUpTitle);
    }

    /**
     * устанавливаем capping для popUp виджета 'FREQUENCY CAPPING (MINUTES)'
     */
    public String setPopUpCapping() {
        String popTitle = randomNumberFromRange(5, 20);
        sendKey(POPUP_CAPPING_INPUT, popTitle + Keys.ENTER);
        return popTitle;
    }

    /**
     * проверяем capping для popUp виджета 'FREQUENCY CAPPING (MINUTES)'
     */
    public boolean checkPopUpCapping(String popUpCapping) {
        return helpersInit.getBaseHelper().checkDatasetEquals(POPUP_CAPPING_INPUT.val(), popUpCapping);
    }

    /**
     * записываем в мапку все типы и их состояние
     */
    public Map<String, Boolean> setContentTypesInMap() {
        String type;
        Map<String, Boolean> map = new HashMap<>();
        log.info("записываем в мапку все типы и их состояние");
        ElementsCollection list2 = $$(CONTENT_TYPES_ALL_LIST);
        for (SelenideElement s : list2) {
            type = s.parent().attr("id");
            assert type != null;
            map.put(type.substring(type.indexOf("_") + 1), Objects.requireNonNull(s.attr("class")).equalsIgnoreCase("on"));
        }
        return map;
    }

    /**
     * проверяем состояние тумблеров 'CONTENT TYPES'
     */
    public boolean checkContentTypesState(Map<String, Boolean> map) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(map.entrySet().stream()
                .allMatch(m -> Objects.requireNonNull($("#adType_" + m.getKey() + " div")
                        .attr("class"))
                        .equalsIgnoreCase("on") == m.getValue()));
    }

    public boolean isDisplayedAdType(String adType) {
        return $("#adType_" + adType + " div").exists();
    }

    /**
     * вкл/выкл в деше все тумблеры в "CONTENT TYPES "
     */
    public void switchOnOffAllContentTypes(String isSwitch) {
        for(SelenideElement s : $$(CONTENT_TYPES_VISIBLE_LIST)){
            if(Objects.requireNonNull(s.attr("class")).equalsIgnoreCase(isSwitch)) s.click();
        }
    }

    /**
     * Берёт рандомный цвет из массива colorMass, возвращает String цвет
     */
    public String randomColor() {
        return colorMass[new Random().nextInt(colorMass.length)].toLowerCase();
    }

    /**
     * get custom data widget in file etalon
     */
    public JSONObject getJsonFromFile(String filePath, WidgetTypes.Types type, WidgetTypes.SubTypes subType) {
        try {
            JSONObject jsonObjectBase, jsonObjectBase2;

            jsonObjectBase = (JSONObject) new JSONParser().parse(new FileReader(filePath));

            if (jsonObjectBase.get(type.getTypeValue()) != null) {
                jsonObjectBase2 = (JSONObject) jsonObjectBase.get(type.getTypeValue());
                if (jsonObjectBase2.get(subType.getTypeValue()) != null) {
                    return (JSONObject) jsonObjectBase2.get(subType.getTypeValue());
                }
                return jsonObjectBase2;
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    public boolean getLogMethod(String exp, String act) {
        if (exp.equals(act) || exp.contains(act) || act.contains(exp)) {
            return true;
        }
        log.error(Thread.currentThread().getStackTrace()[3].getMethodName() + ", " + Thread.currentThread().getStackTrace()[2].getMethodName() + ": " + exp + " == " + act);
        return false;
    }

    /**
     * switch on/off 'INTERNAL EXCHANGE' tumbler
     */
    public void switchIntExchangeTumbler(boolean state) { switchTumblerInDash(state, INT_EXCHANGE_TUMBLER.should(visible, ofSeconds(8)).hover()); }

    public void switchMonetizationTumbler(boolean state) { switchTumblerInDash(state, MONETIZATION_TUMBLER); }

    public void switchDirectPublisherDemandTumbler(boolean state) { switchTumblerInDash(state, DIRECT_PUBLISHER_DEMAND_TUMBLER.hover()); }

    /**
     * check 'INTERNAL EXCHANGE' tumbler
     */
    public boolean checkIntExchangeTumbler(boolean state) {
        return INT_EXCHANGE_TUMBLER.shouldBe(visible).text().equalsIgnoreCase("on") == state;
    }

    /**
     * choose all placement for int_exchange
     */
    public void chooseAllPlacementWidgetByIntExchange() {
        POSITION_BLOCK.shouldBe(visible).$$(".item:not([class$='has-tooltip']):not(.vr):not(.bannerDefaultPosition)").asDynamicIterable().forEach(i -> i.shouldBe(visible).click(ClickOptions.usingJavaScript()));
    }

    public List chooseRandomPlacement(SelenideElement positionBlock, int countPlacement) {
        positionBlock.shouldBe(visible).$$(".item:not([class$='has-tooltip']):not(.vr):not(.bannerDefaultPosition)").shouldHave(CollectionCondition.sizeGreaterThan(0));
        ElementsCollection items = positionBlock.shouldBe(visible).$$(".item:not([class$='has-tooltip']):not(.vr):not(.bannerDefaultPosition)");

        log.info("clear old items");
        for(SelenideElement s : items){
            if(Objects.requireNonNull(s.attr("class")).contains("selected")) s.click();
        }

        log.info("get(data-position-id) random items");
        List random_items = new ArrayList();
        int temp;
        while(random_items.size() != countPlacement) {
            do {
                temp = randomNumbersInt(items.size());
            }
            while (Objects.requireNonNull(items.get(temp).attr("class")).contains("selected") || random_items.contains(items.get(temp).attr("data-position-id")));
            random_items.add(items.get(temp).attr("data-position-id"));
        }

        log.info("click random items");
        random_items.forEach(i -> log.info("random_items: " + i));
        random_items.forEach(i ->
                items.find(attribute("data-position-id", i.toString())).click(usingJavaScript()));
        return random_items;
    }

    public boolean checkPlacement(SelenideElement positionBlock, List random_items, String color) {
        ElementsCollection items = positionBlock.shouldBe(visible).$$(".item");

        return random_items.stream()
                .allMatch(i -> (Objects.requireNonNull(items.find(attribute("data-position-id", String.valueOf(i))).attr("class")).contains("selected") &&
                        helpersInit.getBaseHelper().checkDatasetEquals(convertRgbaToHex(Objects.requireNonNull(items.find(attribute("data-position-id", String.valueOf(i))).getCssValue("background-color"))),color)));
    }

    /**
     * переключаемся на DETAILED tab
     */
    public void switchToDetailedTab() {
        DETAILED_TAB.shouldBe(visible).click();
    }

    /**
     * get element is has shadow DOM element
     */
    public SelenideElement getElementIfHasShadowDom(String element){
        return isShadowDom ?
                $(shadowCss(element, "[id*='ScriptRootC']")) :
                $(element);
    }

    public ElementsCollection getElementsIfHasShadowDom(String element){
        return isShadowDom ?
                $$(shadowCss(element, "[id*='ScriptRootC']")) :
                $$(element);
    }

    /////////////////////////////////// OTHER - FINISH ////////////////////////////////////////////


    ////////////////////////////////// CATEGORIES-BLOCK - START ///////////////////////////////////

    public void turnOnCategoryTumbler(boolean state){  switchTumblerInDash(state, CATEGORIES_TUMBLER.shouldBe(visible)); }

    public String getCategoryTumblerStatus(){
        return CATEGORIES_TUMBLER.attr("class"); }

    /**
     * устанавливаем 5 рандомных категорий в CATEGORIES-BLOCK (для обменных виджетов IM)
     */
    public ArrayList<String> setCategories() {
        if (CATEGORIES_BLOCK.isDisplayed()) {
            helpersInit.getMultiFilterHelper().expandMultiFilter("categoryTreeId");

            ElementsCollection categories = $$(CATEGORIES_INPUT);
            clearAllCategories();
            clearAllCategories();

            //get 5 random category
            ArrayList<Integer> list = helpersInit.getBaseHelper().getRandomCountValue(categories.size(), 5);

            //add categories to map
            ArrayList<String> categories_name = list.stream().map(i -> categories.get(i).innerText()).collect(Collectors.toCollection(ArrayList::new));

            //turn on categories
            categories_name.forEach(i -> helpersInit.getMultiFilterHelper().clickValueInMultiFilter(i));

            return categories_name;
        }
        return null;
    }

    public void clearAllCategories(){
        ElementsCollection selectedCategories = $$("#categoryTreeId .fancytree-node:not(.fancytree-has-children).fancytree-selected");

        //clear all category checkbox
        for (SelenideElement v : selectedCategories) {
                v.scrollTo().hover().click();
        }
    }

    /**
     * проверяем выбранные категории в CATEGORIES-BLOCK (для обменных виджетов IM)
     */
    public boolean checkCategories(ArrayList<String> list) {
        //checkCategories
        if (list != null)
            return list.stream().allMatch(i -> $x(".//span[span[text()=\"" + i + "\"]]").is(attributeMatching("class", ".+fancytree-selected.+")));
        return true;
    }

    ////////////////////////////////// CATEGORIES-BLOCK - FINISH ///////////////////////////////////


    ////////////////////////////////// BRANDFORMANCE - START ///////////////////////////////////////////

    /**
     * get css value for MGLBTN impact
     */
    public String getBrandMglBtnStyle(String cssStyle) {
        getElementIfHasShadowDom(MCTITLE_A_STYLE).scrollTo();
        return getElementIfHasShadowDom(BRAND_MGLBTN_STYLE).shouldBe(visible).getCssValue(cssStyle);
    }

    public String getIdealMglBtnStyle(String cssStyle){
        return getElementIfHasShadowDom(IDEAL_MGLBTN_STYLE).shouldBe(visible).getCssValue(cssStyle);
    }

    public String getBrandMglBtnText(){ return getElementIfHasShadowDom(BRAND_MGLBTN_STYLE).scrollTo().text().replaceAll("[^a-zA-Zа-яА-Я ]", ""); }


    public String getIdealMglBtnText(){ return getElementIfHasShadowDom(IDEAL_MGLBTN_STYLE).text().replaceAll("[^a-zA-Zа-яА-Я ]", ""); }

    public boolean checkMctitleLink(String startLinkValue){
        $$(MCTITLE_A_STYLE).shouldBe(CollectionCondition.allMatch("dfg", i -> i.getAttribute("href").startsWith(startLinkValue)));
        return true;
    }

    /**
     * get css value for title
     */
    public String getTitleStyle(String cssStyle) {
        return getElementIfHasShadowDom(MCTITLE_A_STYLE).getCssValue(cssStyle);
    }

    public String getTitleText() {
        return getElementIfHasShadowDom(MCTITLE_A_STYLE).innerText();
    }

    public String getMcimgStyle(String cssStyle) {
        return getElementIfHasShadowDom(MCIMG_STYLE).getCssValue(cssStyle);
    }

    /**
     * get css value for description
     */
    public String getDescriptionStyle(String cssStyle) {
        return getElementIfHasShadowDom(MCDESC_A_STYLE).getCssValue(cssStyle);
    }

    public String getDescriptionText() {
        return getElementIfHasShadowDom(MCDESC_A_STYLE).innerText();
    }


    /**
     * get css value for mgbox
     */
    public String getMgBoxStyle(String cssStyle) {
        return getElementIfHasShadowDom(MGBOX_STYLE).getCssValue(cssStyle);
    }

    public String getMgHeadStyle(String cssStyle) {
        return getElementIfHasShadowDom(MGHEAD_STYLE).getCssValue(cssStyle);
    }

    /**
     * get css value for domain
     */
    public String getDomainStyle(String cssStyle) {
        return getElementIfHasShadowDom(MCDOMAIN_A_STYLE).getCssValue(cssStyle);
    }


    ////////////////////////////////// BRANDFORMANCE - FINISH ///////////////////////////////////////////


    ////////////////////////////////// IN-SITE-NOTIFICATION - START ////////////////////////////////////

    /**
     * click 'close' icon widget
     */
    public void clickCloseIconInSiteNotification() {
        getElementIfHasShadowDom(IN_SITE_NOTIFICATION_CLOSE_ICON).hover().click();
    }

    ////////////////////////////////// IN-SITE-NOTIFICATION - FINISH ////////////////////////////////////

    ///////////////PASSAGE - START /////////////////

    public void clickCloseIconPassage() {
        getElementIfHasShadowDom(PASSAGE_CLOSE_ICON).hover().click();
    }

    ///////////////PASSAGE - FINISH /////////////////

    ////////////////// AMP - START /////////////

    /**
     * Check AMP widgets code presence in Dashboard
     */
    public boolean checkAMPCodeForWidgetsInDashboard(int widgetId, String siteName, int siteId, String subnetName) {
        POPUP.shouldBe(visible);
        log.info("Popup with widgets code is displayed - " + POPUP.isDisplayed());
        log.info("Check AMP widgets code presence");
        return helpersInit.getBaseHelper().checkDatasetContains(POPUP.text(),
                "<amp-embed width=\"600\" height=\"600\" layout=\"responsive\" type=\"" + subnetName.toLowerCase() + "\" data-publisher=\"" + siteName.toLowerCase()
                        + "\" data-widget=\"" + widgetId + "\" data-container=\"M" + siteId + "ScriptRootC" + widgetId + "\" data-block-on-consent=\"_till_responded\" > </amp-embed>");
    }

    ////////////////// AMP - FINISH /////////////

    public boolean checkDisableEditClass() {
        return DISABLED_EDIT_WIDGET_LOCATOR.shouldBe(visible, ofSeconds(8)).exists();
    }

    public List getStopWords(){
        return $$(STOP_WORD_OPTIONS).texts().stream().map(String::trim).collect(Collectors.toList());
    }

    public void setStopWords(ArrayList<String> listOfStopWords) {
        STOP_WORDS_CONTAINER.shouldBe(visible);
        listOfStopWords.forEach(elem -> sendKey(STOP_WORDS_FORM_INPUT, elem + Keys.ENTER));
    }

    public boolean isDisplayedStopWordsError(){ return STOP_WORD_ERROR.isDisplayed(); }

    public void deleteStopWords(int... count) {
        int t = 0;
        int countWords = count.length > 0 ? count[0] : STOP_WORD_DELETE_BUTTONS.size();
        while (t < countWords){
            STOP_WORD_DELETE_BUTTONS.get(0).click();
            t++;
        }
    }

    /**
     * Click "Save" button for widget
     */
    public void clickSaveWidgetButton() {
        log.info("Click save widget button");
        clickInvisibleElementJs(WIDGET_SAVE_BUTTON.shouldBe(visible).scrollTo());
        waitForAjaxVisible();
        checkErrors();
    }

    public void clickTeaserOnStand(int teaserIndexNumber){
        SelenideElement teaser = getElementIfHasShadowDom(String.format(CURRENT_TEASER_IN_CONSTRUCTOR, teaserIndexNumber + 1)).shouldBe(visible, ofSeconds(10));
        int t = 0, count = 0;
        while(t == 0 && count < 15) {
            t = helpersInit.getBaseHelper().parseInt(teaser
                    .scrollTo()
                    .hover()
                    .attr("data-observe-time"));
            count++;
            sleep(100);
        }
        if(t!=0) {
            teaser.$("img.mcimg").click();
        }
        else {
            log.error("data-observe-time = " + t);
        }
    }

    public boolean isShowDoubleClickForm(int teaserIndexNumber){
        return getElementIfHasShadowDom(String.format(CURRENT_TEASER_IN_CONSTRUCTOR, teaserIndexNumber + 1)).$("div[class*='CDLayout']").isDisplayed();
    }

    public boolean isShowDefaultJsBlock(){ sleep(1000); return $(DEFAULT_JS_DIV).isDisplayed(); }

    public int getCountDefaultJsBlock(){ return $$(DEFAULT_JS_DIV).size(); }

    public boolean isShowDefaultJsBlockChild(){ return $(DEFAULT_JS_DIV_CHILD).isDisplayed(); }

    public int getCountDefaultJsBlockChild(){ return $$(DEFAULT_JS_DIV_CHILD).size(); }

    public int getCountLinksInMcimg(){
        return getElementsIfHasShadowDom(COUNT_LINKS_IN_MGLINE).size(); }
}
