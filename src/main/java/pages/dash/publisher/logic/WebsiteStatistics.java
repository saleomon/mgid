package pages.dash.publisher.logic;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import libs.clickhouse.queries.PublisherStatistics;
import libs.clickhouse.queries.WidgetStatisticsSumm;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.PublisherStatHelper;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;

import java.util.ArrayList;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static pages.dash.publisher.locators.PublisherLocators.*;

public class WebsiteStatistics {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final PublisherStatHelper publisherStatHelper;
    private ArrayList<String> startEndDate;
    PublisherStatistics publisherStatistics;
    WidgetStatisticsSumm widgetStatisticsSumm;

    //fill list in Graph
    ArrayList<Double> massWages, massCPC, massPageErpm, massRcpm, massErpm, massPageRpm;
    ArrayList<Long> massClicks, massPageViewsWithAds, massPageView, massPageImpressions, massIntExchange,
            formRequests, formImpressions, formSubscribers,
            pushesSent, pushesDelivered, pushesClicked;

    //total get in clickHouse
    String totalDbWages, totalDbPageView, totalDbPageImpressions, totalDbIntExchange,
            totalDbGoodsClicks, totalDbInternalClicks, totalDbClicks,
            totalDbPushRequests, totalDbPushImpressions, totalDbPushSubscribers,
            totalDbPushesSent, totalDbPushesDelivered, totalDbPushesClicked;

    //custom reports
    ElementsCollection wagesTableElements, pageErpmTableElements, clicksTableElements, pageViewsTableElements, pageRpmTableElements, soldClicksTableElements,
            eRpmTableElements, cpcTableElements, rCpmTableElements, visibilityRateTableElements, ctrTableElements, viewsWithAdsTableElements,
            intExchangeTableElements, pushRequestsTableElements, pushImpressionsTableElements, pushSubscribersTableElements,
            pushesSentTableElements, pushesDeliveredTableElements, pushesClickedTableElements;

    String customReportName;

    //stat By Day
    SelenideElement PAGES_VIEWS_TOTAL_BY_DAY_LABEL, PAGES_RPM_TOTAL_BY_DAY_LABEL, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, E_RPM_TOTAL_BY_DAY_LABEL,
            SOLD_CLICKS_TOTAL_BY_DAY_LABEL, CLICKS_TOTAL_BY_DAY_LABEL, WAGES_TOTAL_BY_DAY_LABEL, INT_CLICKS_TOTAL_BY_DAY_LABEL,
            CPC_TOTAL_BY_DAY_LABEL, PAGE_ERPM_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, VISIBILITY_RATE_TOTAL_BY_DAY_LABEL;

    SelenideElement VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, PAGES_VIEWS_TABLE_TOTAL_LABEL, E_CPM_TABLE_TOTAL_LABEL, CLICKS_TABLE_TOTAL_LABEL,
            WAGES_TABLE_TOTAL_LABEL, R_CPM_TABLE_TOTAL_LABEL, CPC_TABLE_TOTAL_LABEL, CTR_TABLE_TOTAL_LABEL, VISIBILITY_RATE_TABLE_TOTAL_LABEL;

    SelenideElement FORM_REQUESTS_LABEL, FORM_IMPRESSIONS_LABEL, FORM_USERS_SUBSCRIBED_LABEL,
            PUSHES_SENT_LABEL, PUSHES_DELIVERED_LABEL, PUSHES_CLICKED_LABEL;

    public WebsiteStatistics(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        publisherStatHelper = new PublisherStatHelper(log, helpersInit);
        publisherStatistics = new PublisherStatistics();
        widgetStatisticsSumm = new WidgetStatisticsSumm();
    }

    public String getCustomReportName() {
        return customReportName;
    }

    //custom report
    public void getDataWagesFromGraph() {
        massWages = publisherStatHelper.getJsQuery("wages");
    }

    public void getDataClicksFromGraph() {
        massClicks = publisherStatHelper.getJsQuery("clicks");
    }

    public void getDataCpcFromGraph() {
        massCPC = publisherStatHelper.getJsQuery("cpc");
    }

    public void getDataPageViewsFromGraph() {
        massPageView = publisherStatHelper.getJsQuery("pageViews");
    }

    public void getDataPageViewsWithAdsFromGraph() { massPageViewsWithAds = publisherStatHelper.getJsQuery("pageViewsWithAds"); }

    public void getDataPageErpmFromGraph() {
        massPageErpm = publisherStatHelper.getJsQuery("pageERpm");
    }

    public void getDataEcpmFromGraph() {
        massPageErpm = publisherStatHelper.getJsQuery("eCpm");
    }

    public void getDataRcpmFromGraph() {
        massRcpm = publisherStatHelper.getJsQuery("rCpm");
    }

    public void getDataErpmFromGraph() {
        massErpm = publisherStatHelper.getJsQuery("pageERpm");
    }

    public void getDataPageRpmFromGraph() {
        massPageRpm = publisherStatHelper.getJsQuery("pageRpm");
    }

    public void getDataPageImpressionsFromGraph() { massPageImpressions = publisherStatHelper.getJsQuery("pageImpressions"); }

    public void getDataIntExchangeFromGraph() {
        massIntExchange = publisherStatHelper.getJsQuery("intExchange");
    }

    public void getDataPushRequestsFromGraph() {
        formRequests = publisherStatHelper.getJsQuery("pushFR");
    }

    public void getDataPushImpressionsFromGraph() {
        formImpressions = publisherStatHelper.getJsQuery("pushFI");
    }

    public void getDataPushSubscribeFromGraph() {
        formSubscribers = publisherStatHelper.getJsQuery("pushSubscribe");
    }

    public void getDataPushesSentFromGraph() {
        pushesSent = publisherStatHelper.getJsQuery("pushSend");
    }

    public void getDataPushesDeliveredFromGraph() {
        pushesDelivered = publisherStatHelper.getJsQuery("pushDelivered");
    }

    public void getDataPushesClickedFromGraph() {
        pushesClicked = publisherStatHelper.getJsQuery("pushClick");
    }

    /**
     * get total data for custom db fields from clickHouse
     * dataType - site = ?/composite = ?/""
     */
    public void getTotalDataFromClickHouse(String dataType) {
        publisherStatistics.getSumForCustomFieldsFromClickHouse(dataType, startEndDate,
                "wages_publisher_currency",
                "page_views",
                "page_impressions",
                "int_exchange_clicks_accepted",
                "goods_clicks_accepted",
                "int_exchange_clicks_accepted"
        );

        totalDbWages = publisherStatistics.getString(1);
        totalDbPageView = publisherStatistics.getString(2);
        totalDbPageImpressions = publisherStatistics.getString(3);
        totalDbIntExchange = publisherStatistics.getString(4);
        totalDbGoodsClicks = publisherStatistics.getString(5);
        totalDbInternalClicks = publisherStatistics.getString(6);

        totalDbClicks = String.valueOf(helpersInit.getBaseHelper().parseInt(totalDbGoodsClicks) + helpersInit.getBaseHelper().parseInt(totalDbInternalClicks));
    }

    public void getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(int websiteId) {
        widgetStatisticsSumm.getSumForCustomWebsiteFieldsFromClickHouse(websiteId, startEndDate);

        totalDbPageView = widgetStatisticsSumm.getString(1);
        totalDbPageImpressions = widgetStatisticsSumm.getString(2);
        totalDbWages = widgetStatisticsSumm.getString(3);
        totalDbClicks = widgetStatisticsSumm.getString(4);
    }

    public void getTotalDataFromAudienceHubEventsClickhouse(int websiteId) {
        publisherStatistics.getSumByPushSubscribersStatisticsFromClickHouse(websiteId, startEndDate);

        totalDbPushRequests = publisherStatistics.getString(1);
        totalDbPushImpressions = publisherStatistics.getString(2);
        totalDbPushSubscribers = publisherStatistics.getString(3);
    }

    public void getTotalDataFromPushStatisticsEventsClickhouse(String websiteDomain) {
        publisherStatistics.getSumByPushStatisticsEventsFromClickHouse(websiteDomain, startEndDate);

        totalDbPushesSent = publisherStatistics.getString(1);
        totalDbPushesDelivered = publisherStatistics.getString(2);
        totalDbPushesClicked = publisherStatistics.getString(3);
    }

    //////////////////////////////////////////// CUSTOM REPORT - START //////////////////////////////////////////////////////////////

    /**
     * Проставлем для отображения статистики CUSTOM REPORTS - метрики и показатели
     */
    public void customReport_choseRandomOptions(CalendarPeriods period, String[] type, String... metric) {
        startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);

        CUSTOM_REPORTS_OPTIONS_BUTTON.shouldBe(visible);
        initGraph();

        log.info("Открываем настройки параметров репортов и выбираем все dimensions из аргумента type");
        publisherStatHelper.openOptionsForm();
        publisherStatHelper.selectMetrics("dimensions", type);

        log.info("включаем все metrics из аргумента metric");
        publisherStatHelper.selectMetrics("metrics", metric);

        log.info("Проверяем метрики и показатели в поп-апе 'Фильтры'");
        publisherStatHelper.checkAllOptionsInFilterForm(type, metric);

        log.info("Выбираем нужный период в календаре и фильтруем данные");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);

        log.info("Если в интерфейсе присутствует пагинатор страниц(1,2 NEXT), то выбираем в пагинаторе отображение по 100 параметров");
        publisherStatHelper.loadDataIfPaginatorDisplayed();

        CUSTOM_REPORT_INFO_MESSAGE.shouldBe(exist);
    }

    public void getDataColumnByHeader() {
        wagesTableElements = helpersInit.getSortedHelper().getTableColumnData("wages");
        pageErpmTableElements = helpersInit.getSortedHelper().getTableColumnData("e_cpm");
        clicksTableElements = helpersInit.getSortedHelper().getTableColumnData("clicks");
        cpcTableElements = helpersInit.getSortedHelper().getTableColumnData("cpc");
        rCpmTableElements = helpersInit.getSortedHelper().getTableColumnData("r_cpm");
        visibilityRateTableElements = helpersInit.getSortedHelper().getTableColumnData("visibility_rate");
        ctrTableElements = helpersInit.getSortedHelper().getTableColumnData("ctr");
        viewsWithAdsTableElements = helpersInit.getSortedHelper().getTableColumnData("page_impressions");
        pageViewsTableElements = helpersInit.getSortedHelper().getTableColumnData("page_views");
    }

    public void getPushSubscribersDataColumnByHeader() {
        pushRequestsTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Form Requests");
        pushImpressionsTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Form Impressions");
        pushSubscribersTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Users Subscribed");
    }

    public void getPushNotificationsDataColumnByHeader() {
        pushesSentTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Pushes Sent");
        pushesDeliveredTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Pushes Delivered");
        pushesClickedTableElements = helpersInit.getSortedHelper().getTableColumnDataByText("Pushes Clicked");
    }

    public void getTotalElementForStatBy() {
        PAGES_VIEWS_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("page_views");
        VIEWS_WITH_ADS_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("page_impressions");
        CLICKS_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("clicks");
        WAGES_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("wages");
        CPC_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("cpc");
        CTR_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("ctr");
        E_CPM_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("e_cpm");
        R_CPM_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("r_cpm");
        VISIBILITY_RATE_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("visibility_rate");
    }

    public boolean customReport_checkShowAllSortedByHeaders(String... metric) {
        return publisherStatHelper.checkShowAllMetrics(metric);
    }

    public boolean customReport_checkWorkFilterByDimensionWithSelect(int... customValue) {
        return publisherStatHelper.checkWorkFilterByDimensionWithSelect(customValue);
    }

    public boolean customReport_checkWorkFilterByDimensionWithInput() {
        return publisherStatHelper.checkWorkFilterByDimensionWithInput();
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму pageViewsWithAds из графика с суммой Views with ads из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForViewsWithAds() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(massPageViewsWithAds, viewsWithAdsTableElements, VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, totalDbPageImpressions);
    }

    /**
     * Проверяем расчёт eCPM в таблице CUSTOM REPORTS под графиком - сравниваем построчно за каждый день
     * eCPM = wages/viewsWithAds * 1000
     */
    public boolean customReport_checkEcpmInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, viewsWithAdsTableElements, pageErpmTableElements, 1000);
    }

    /**
     * Проверяем расчёт CPC в таблице CUSTOM REPORTS под графиком - сравниваем построчно за каждый день
     * cpc = wages/clicks * 100
     */
    public boolean customReport_checkCpcInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, clicksTableElements, cpcTableElements, 100);
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/viewsWithAds * 100
     */
    public boolean customReport_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, viewsWithAdsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = views_with_ads/page_views * 100
     */
    public boolean customReport_checkVisibilityRateInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(viewsWithAdsTableElements, pageViewsTableElements, visibilityRateTableElements, 100);
    }

    /**
     * Проверяем расчёт eCPM в графике
     * eCPM = wages/viewsWithAds * 1000
     */
    public boolean customReport_checkEcpmInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageViewsWithAds, massPageErpm, 1000);
    }

    /**
     * Проверяем расчёт CPC в графике
     * cpc = wages/clicks * 100
     */
    public boolean customReport_checkCpcInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massClicks, massCPC, 100);
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForWages() {
        return publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TABLE_TOTAL_LABEL, totalDbWages);
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму Клики из графика с суммой Клики из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TABLE_TOTAL_LABEL, totalDbClicks));
    }

    /**
     * Проверяем работу в поп-апе "Фильтры" работу сортировки метрик по сортировке ("equal", "greater_or_equal", "less_or_equal")
     * Проверяем метрики с целочисленными данными ("ad_requests", "impressions", "clicks")
     * Проверяем метрики для данных с плавающей точкой ("visibility_rate", "wages", "cpc", "e_cpm")
     */
    public boolean checkMinOrMaxValueSelect(String... value) {
        return publisherStatHelper.checkMinOrMaxValueSelect(value);
    }

    public void createCustomReport() {
        customReportName = publisherStatHelper.createReport();
    }

    public boolean deleteCustomReport(String reportName) {
        return publisherStatHelper.deleteReport(reportName);
    }

    public boolean checkCustomReportInListInterfaceAndGoHim(String reportName) {
        return publisherStatHelper.checkCustomReportInListInterfaceAndGoHim(reportName);
    }

    //////////////////////////////////////////// CUSTOM REPORT - FINISH //////////////////////////////////////////////////////////////


    //////////////////////////////////////////// BY Country/Source/SubId/Device - START //////////////////////////////////////////////////////////////

    public void selectCalendarPeriodJs(CalendarPeriods period) {
        startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);
        CALENDAR_LABEL.shouldBe(visible);
        log.info("создаём js переменную на стороне клиента из которой мы будем получать параметры из графика(js-action будет отрабатывать после каждого респонса(период в календаре))");
        initGraph();
        log.info("Choose period in calendar and filter data");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);
    }

    /**
     * Проверка Клики в интерфейсе By Country/Device/Etc
     * сравниваем сумму Клики из графика с суммой Клики из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TABLE_TOTAL_LABEL, totalDbClicks));
    }

    /**
     * Проверка Views with ads в интерфейсе By Country/Device/Etc
     * сравниваем сумму pageViewsWithAds из графика с суммой Views with ads из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForViewsWithAds() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(massPageViewsWithAds, viewsWithAdsTableElements, VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, totalDbPageImpressions);
    }

    /**
     * Проверка Wages в интерфейсе By Country/Device/Etc
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForWages() {
        return publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TABLE_TOTAL_LABEL, totalDbWages);
    }

    /**
     * Проверка PageView в интерфейсе By Country/Device/Etc
     * сравниваем сумму PageView из графика с суммой PageView из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForPageImpressions() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(massPageView, pageViewsTableElements, PAGES_VIEWS_TABLE_TOTAL_LABEL, totalDbPageView);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushRequests() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(formRequests, pushRequestsTableElements, FORM_REQUESTS_LABEL, totalDbPushRequests);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushImpressions() {
        return publisherStatHelper.checkSumGraphSumTable(formImpressions, pushImpressionsTableElements, FORM_IMPRESSIONS_LABEL, totalDbPushImpressions);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushSubscribers() {
        return publisherStatHelper.checkSumGraphSumTable(formSubscribers, pushSubscribersTableElements, FORM_USERS_SUBSCRIBED_LABEL, totalDbPushSubscribers);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushesSent() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(pushesSent, pushesSentTableElements, PUSHES_SENT_LABEL, totalDbPushesSent);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushesDelivered() {
        return publisherStatHelper.checkSumGraphSumTable(pushesDelivered, pushesDeliveredTableElements, PUSHES_DELIVERED_LABEL, totalDbPushesDelivered);
    }

    public boolean statBy_checkSumGraphSumTableTableForPushesClicked() {
        return publisherStatHelper.checkSumGraphSumTable(pushesClicked, pushesClickedTableElements, PUSHES_CLICKED_LABEL, totalDbPushesClicked);
    }

    /**
     * Проверяем расчёт CPC в графике
     * cpc = wages/clicks * 100
     */
    public boolean statBy_checkCpcInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massClicks, massCPC, 100);
    }

    /**
     * Проверяем расчёт rCPM в графике
     * r_cpm = wages/pageView * 1000
     */
    public boolean statBy_checkRcpmInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageView, massRcpm, 1000);
    }

    /**
     * Проверяем расчёт eCpm в графике
     * eCpm = wages/viewsWithAds * 1000
     */
    public boolean statBy_checkEcpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageViewsWithAds, massPageErpm, 1000));
    }

    /**
     * Проверяем расчёт eCPM в таблице под графиком - сравниваем построчно за каждый день
     * eCPM = wages/viewsWithAds * 1000
     */
    public boolean statBy_checkEcpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, viewsWithAdsTableElements, pageErpmTableElements, 1000));
    }

    /**
     * Проверяем total eCPM
     * eCPM = wages/impressions * 1000
     */
    public boolean statBy_checkTotalEcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, E_CPM_TABLE_TOTAL_LABEL, 1000));
    }

    /**
     * Проверяем расчёт r_CPM в таблице под графиком - сравниваем построчно за каждый день
     * r_cpm = wages/pageViews * 1000
     */
    public boolean statBy_checkRcpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, pageViewsTableElements, rCpmTableElements, 1000));
    }

    /**
     * Проверяем расчёт CPC в таблице под графиком - сравниваем построчно за каждый день
     * cpc = wages/clicks * 100
     */
    public boolean statBy_checkCpcInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, clicksTableElements, cpcTableElements, 100));
    }

    /**
     * Проверяем total CPC
     * cpc = wages/sold clicks(stats.publishers_statistics.goods_clicks_accepted) * 100
     */
    public boolean statBy_checkTotalCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, CLICKS_TABLE_TOTAL_LABEL, CPC_TABLE_TOTAL_LABEL, 100));
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = viewsWithAds/pageView * 100
     */
    public boolean statBy_checkVisibilityRateInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(viewsWithAdsTableElements, pageViewsTableElements, visibilityRateTableElements, 100));
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/viewsWithAds * 100
     */
    public boolean statBy_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, viewsWithAdsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем total VisibilityRate
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean statBy_checkTotalVisibilityRate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, PAGES_VIEWS_TABLE_TOTAL_LABEL, VISIBILITY_RATE_TABLE_TOTAL_LABEL, 100));
    }

    /**
     * Проверяем total CPC
     * r_cpm = wages/adRequest * 1000
     */
    public boolean statBy_checkTotalRcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, PAGES_VIEWS_TABLE_TOTAL_LABEL, R_CPM_TABLE_TOTAL_LABEL, 1000));
    }

    /**
     * Проверяем total CTR
     * ctr = clicks/impressions * 100
     */
    public boolean statBy_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TABLE_TOTAL_LABEL, VIEWS_WITH_ADS_TABLE_TOTAL_LABEL, CTR_TABLE_TOTAL_LABEL, 100));
    }

    ///////////////////////////////////////////// BY Country/Source/SubId/Device - FINISH ////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////// BY DAY - START ////////////////////////////////////////////////////////////////

    /**
     * get custom data column by header(column name)
     */
    public void getDataColumnByHeaderForStatByDay() {
        wagesTableElements = publisherStatHelper.getTableColumnData("Ad Revenue, €");
        pageViewsTableElements = publisherStatHelper.getTableColumnData("Page Views");
        pageErpmTableElements = publisherStatHelper.getTableColumnData("Ad vRPM, €");
        viewsWithAdsTableElements = publisherStatHelper.getTableColumnData("Views with");
        clicksTableElements = publisherStatHelper.getTableColumnData("Clicks");
        cpcTableElements = publisherStatHelper.getTableColumnData("Ad CPC, ¢");
        visibilityRateTableElements = publisherStatHelper.getTableColumnData("Visibility");
        ctrTableElements = publisherStatHelper.getTableColumnData("Ad CTR, %");
        soldClicksTableElements = publisherStatHelper.getTableColumnData("Ad Clicks");
    }

    /**
     * get total element from table (first row (th))
     */
    public void getTotalElementForStatByDay() {
        PAGES_VIEWS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Page Views");
        VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Views with");
        CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Clicks");
        SOLD_CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad Clicks");
        WAGES_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad Revenue, €");
        CPC_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad CPC, ¢");
        CTR_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad CTR, %");
        PAGE_ERPM_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad vRPM, €");
        VISIBILITY_RATE_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Visibility");
    }

    public void getTotalElementForStatByPushSubscribers() {
        FORM_REQUESTS_LABEL = publisherStatHelper.getTotalByText("Form Requests");
        FORM_IMPRESSIONS_LABEL = publisherStatHelper.getTotalByText("Form Impressions");
        FORM_USERS_SUBSCRIBED_LABEL = publisherStatHelper.getTotalByText("Users Subscribed");
    }

    public void getTotalElementForStatByPushNotifications() {
        PUSHES_SENT_LABEL = publisherStatHelper.getTotalByText("Pushes Sent");
        PUSHES_DELIVERED_LABEL = publisherStatHelper.getTotalByText("Pushes Sent");
        PUSHES_CLICKED_LABEL = publisherStatHelper.getTotalByText("Pushes Clicked");
    }

    /**
     * Проверка Клики в интерфейсе By Day
     * сравниваем сумму Клики из графика с суммой Клики из таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TOTAL_BY_DAY_LABEL, totalDbClicks));
    }

    /**
     * Проверка PageView в интерфейсе By Day
     * сравниваем сумму PageView из графика с суммой PageView из таблицы и с полем тотал таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForPageImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massPageView, pageViewsTableElements, PAGES_VIEWS_TOTAL_BY_DAY_LABEL, totalDbPageView));
    }

    /**
     * Проверка Wages в интерфейсе By Day
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForWages() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TOTAL_BY_DAY_LABEL, totalDbWages));
    }

    /**
     * Проверка PageViewsWithAds в интерфейсе By Day
     * сравниваем сумму pageViewsWithAds из графика с суммой Views with ads из таблицы и с полем тотал таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForViewsWithAds() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massPageViewsWithAds, viewsWithAdsTableElements, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, totalDbPageImpressions));
    }

    /**
     * Проверяем расчёт CPC в таблице By Day под графиком - сравниваем построчно за каждый день
     * cpc = wages/sold clicks * 100
     */
    public boolean statByDay_checkCpcInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, soldClicksTableElements, cpcTableElements, 100);
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/viewsWithAds * 100
     */
    public boolean statByDay_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, viewsWithAdsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = viewsWithAds/pageView * 100
     */
    public boolean statByDay_checkVisibilityRateInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(viewsWithAdsTableElements, pageViewsTableElements, visibilityRateTableElements, 100));
    }

    /**
     * Проверяем total CPC
     * cpc = wages/sold clicks * 100
     */
    public boolean statByDay_checkTotalCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, SOLD_CLICKS_TOTAL_BY_DAY_LABEL, CPC_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем расчёт PageErpm в графике
     * PageErpm = wages/viewsWithAds * 1000
     */
    public boolean statByDay_checkPageErpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageViewsWithAds, massPageErpm, 1000));
    }

    /**
     * Проверяем расчёт PageErpm в таблице под графиком - сравниваем построчно за каждый день
     * PageErpm = wages/viewsWithAds * 1000
     */
    public boolean statByDay_checkPageErpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, viewsWithAdsTableElements, pageErpmTableElements, 1000));
    }

    /**
     * Проверяем total PageErpm
     * PageErpm = wages/viewsWithAds * 1000
     */
    public boolean statByDay_checkTotalPageErpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, PAGE_ERPM_TOTAL_BY_DAY_LABEL, 1000));
    }

    /**
     * Проверяем total CTR
     * ctr = clicks/viewsWithAds * 100
     */
    public boolean statByDay_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TOTAL_BY_DAY_LABEL, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем total VisibilityRate
     * VisibilityRate = viewsWithAds/pageView * 100
     */
    public boolean statByDay_checkTotalVisibilityRate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, PAGES_VIEWS_TOTAL_BY_DAY_LABEL, VISIBILITY_RATE_TOTAL_BY_DAY_LABEL, 100));
    }

    ///////////////////////////////////////////// BY DAY - FINISH ////////////////////////////////////////////////////////////////

    //////////////////////////////////////////// WEBSITE LIST - START ////////////////////////////////////////////////////////////

    /**
     * get total element from table (first row (th))
     */
    public void getTotalElementForStatInWebsiteList() {
        PAGES_VIEWS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("pageViews", 0);
        PAGES_RPM_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("pageRpm", 0);
        VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("pageImpressions", 0);
        INT_CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("intClicks", 0);
        WAGES_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("wages", 0);
        E_RPM_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("pageERpm", 0);
    }

    public void getDataColumnByHeaderForWebsiteList() {
        wagesTableElements = helpersInit.getSortedHelper().getTableColumnData("wages");
        viewsWithAdsTableElements = helpersInit.getSortedHelper().getTableColumnData("pageImpressions");
        pageViewsTableElements = helpersInit.getSortedHelper().getTableColumnData("pageViews");
        pageRpmTableElements = helpersInit.getSortedHelper().getTableColumnData("pageRpm");
        intExchangeTableElements = helpersInit.getSortedHelper().getTableColumnData("intClicks");
        eRpmTableElements = helpersInit.getSortedHelper().getTableColumnData("pageERpm");
    }

    /**
     * Проверка PageViewsWithAds в интерфейсе publisher
     * сравниваем сумму pageImpressions из графика с суммой Views with ads из таблицы и с полем тотал таблицы
     */
    public boolean websiteList_checkSumGraphSumTableTotalTableForViewsWithAds() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massPageImpressions, viewsWithAdsTableElements, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, totalDbPageImpressions));
    }

    /**
     * Проверка PageView в интерфейсе publisher
     * сравниваем сумму PageView из графика с суммой PageView из таблицы и с полем тотал таблицы
     */
    public boolean websiteList_checkSumGraphSumTableTotalTableForPageImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massPageView, pageViewsTableElements, PAGES_VIEWS_TOTAL_BY_DAY_LABEL, totalDbPageView));
    }

    /**
     * Проверка Wages в интерфейсе publisher
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean websiteList_checkSumGraphSumTableTotalTableForWages() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TOTAL_BY_DAY_LABEL, totalDbWages));
    }

    /**
     * Проверка IntExchange в интерфейсе publisher
     * сравниваем сумму IntExchange из графика с суммой IntExchange из таблицы и с полем тотал таблицы
     */
    public boolean websiteList_checkSumGraphSumTableTotalTableForIntExchange() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massIntExchange, intExchangeTableElements, INT_CLICKS_TOTAL_BY_DAY_LABEL, totalDbIntExchange));
    }

    /**
     * Проверяем расчёт eRPM в графике
     * eRPM = wages/pageImpressions * 1000
     */
    public boolean websiteList_checkErpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageImpressions, massErpm, 1000));
    }

    /**
     * Проверяем расчёт PageRPM в графике
     * pageRpm = wages/pageViews * 1000
     */
    public boolean websiteList_checkPageRpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massPageView, massPageRpm, 1000));
    }

    /**
     * Проверяем расчёт eRPM в таблице
     * eRPM = wages/pageImpressions * 1000
     */
    public boolean websiteList_checkErpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, viewsWithAdsTableElements, eRpmTableElements, 1000));
    }

    /**
     * Проверяем расчёт pageRPM в таблице
     * pageRPM = wages/pageViews * 1000
     */
    public boolean websiteList_checkPageRpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, pageViewsTableElements, pageRpmTableElements, 1000));
    }

    /**
     * Проверяем total eRPM
     * eRPM = total wages/total pageImpressions * 1000
     */
    public boolean websiteList_checkTotalErpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, VIEWS_WITH_ADS_TOTAL_BY_DAY_LABEL, E_RPM_TOTAL_BY_DAY_LABEL, 1000));
    }

    /**
     * Проверяем total pageRpm
     * pageRpm = total wages/total pageViews * 1000
     */
    public boolean websiteList_checkTotalPageRpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, PAGES_VIEWS_TOTAL_BY_DAY_LABEL, PAGES_RPM_TOTAL_BY_DAY_LABEL, 1000));
    }

    //////////////////////////////////////////// WEBSITE LIST - FINISH ////////////////////////////////////////////////////////////

    /**
     * js-команда для дальнейшего получения данных из графика
     */
    private void initGraph(){
        publisherStatHelper.initGraph();
    }

    /**
     * метод переключает в графике отображаемые параметры
     */
    public void switchParamInGraph(String param) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(GRAPH_SELECT, param);
    }

    /**
     * метод проверяет количество переданных колонок таблицы под графиком
     */
    public boolean checkSizeHeadersInBaseTable(int size) {
        return helpersInit.getBaseHelper().checkDataset($$(TABLE_HEADERS).size(), size);
    }

    public void loadDataIfPaginatorDisplayed(){ publisherStatHelper.loadDataIfPaginatorDisplayed(); }

}
