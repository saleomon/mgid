package pages.dash.publisher.logic;

import core.base.HelpersInit;

import static com.codeborne.selenide.Selenide.$x;
import static pages.dash.publisher.locators.BlockedContentLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class BlockedContent {

    private final HelpersInit helpersInit;

    public BlockedContent(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * @param type widget/site
     */
    public void chooseBlockerType(String type){ BLOCK_POPUP_RADIO.hover().selectRadio(type); }

    public String getTeaserTitle(){ return TEASER_TITLE.innerText(); }

    public void blockedPopupSave(){
        BLOCK_POPUP_SAVE.click();
        waitForAjaxVisible();
        checkErrors();
    }

    public String getTeaserBlockedType(String teaserTitle){
        return $x(".//*[contains(text(), '" + teaserTitle + "')]/ancestor::tr[@class='teaser']//*[@class='blocked_for_heading']").text();
    }

    public void chooseWidgetInFilter(int widgetId){
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(WIDGET_SELECT, String.valueOf(widgetId));
        APPLY_SELECT_BUTTON.click();
        waitForAjax();
    }
}
