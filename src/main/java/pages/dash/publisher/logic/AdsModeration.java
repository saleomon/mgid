package pages.dash.publisher.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;

import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static pages.dash.publisher.locators.AdsModerationLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class AdsModeration {

    private final HelpersInit helpersInit;

    public AdsModeration(HelpersInit helpersInit, Logger log) {
        this.helpersInit = helpersInit;
    }

    public ElementsCollection getAdsOnPage(){ return $$(TEASER_ID_LABEL); }

    public String getStatusAds(int adsId){ return $("tr[id^='row-" + adsId + "']").$(STATUS_LABEL).text().trim(); }

    public void checkStatusAds(String status){
        $$(STATUS_LABEL).shouldBe(CollectionCondition.sizeGreaterThan(0))
                .shouldHave(CollectionCondition.allMatch("checkStatusAds", i -> i.getAttribute("class").contains("status-" + status)));
    }

    public void checkCategoryAds(String categoryName){
        $$(CATEGORY_LABEL).shouldBe(CollectionCondition.sizeGreaterThan(0))
                .shouldHave(CollectionCondition.allMatch("checkCategoryAds", i -> i.getText().equals(categoryName)));
    }
    public void selectStatusFilter(String status) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(STATUS_FILTER_SELECT, status);
    }

    public void selectCategoryFilter(String categoryId) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(CATEGORY_FILTER_SELECT, categoryId);
    }

    public void fillValueInSearchFilter(String value){
        SEARCH_INPUT.sendKeys(value + Keys.ENTER);
        waitForAjax();
        checkErrors();
    }

    public void clickFilterButton(){
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void switchAdsState(boolean state, int adsId){
        String expectedStatus = state ? "off" : "on";

        $("label[for=status_" + expectedStatus + "_" + adsId + "]").shouldBe(visible).scrollTo().click();
        $("input[id=status_" + expectedStatus + "_" + adsId + "]").shouldNotHave(attribute("disabled"));
        TUMBLER_LOADER.shouldBe(hidden);
    }

    public void openAdsCommentPopup(){
        AD_COMMENT_POPUP_ICON.click();
        waitForAjaxLoader();
        AD_COMMENT_ADD_BUTTON.shouldBe(visible);
    }

    public AdsModeration setAdsComment(String value){
        AD_COMMENT_FIELD.sendKeys(value);
        return this;
    }

    public void saveAdsComment(){
        AD_COMMENT_ADD_BUTTON.click();
        waitForAjaxLoader();
    }

    public void clickCancelButton(){
        AD_COMMENT_CLOSE_BUTTON.click();
        waitForAjaxLoader();
    }

    public boolean isActiveAdCommentIcon(){
        return Objects.requireNonNull(AD_COMMENT_POPUP_ICON.attr("class")).contains("active");
    }

    public String getAdCommentValue(){
        return AD_COMMENT_TEXT.text();
    }

    public AdsModeration editAdsComment(String value) {
        AD_COMMENT_ADD_BUTTON.click();
        clearAndSetValue(AD_COMMENT_FIELD, value);
        return this;
    }
}
