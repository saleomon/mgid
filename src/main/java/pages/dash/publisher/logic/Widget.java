package pages.dash.publisher.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.CreateEditVideoHelper;
import pages.dash.publisher.helpers.CreateEditVideoHelper.VideoFormat;
import pages.dash.publisher.helpers.CreateEditWidgetHelper;
import pages.dash.publisher.helpers.WidgetListHelper;
import testData.project.Subnets.SubnetType;
import testData.project.publishers.WidgetTypes;
import testData.project.publishers.WidgetTypes.SubTypes;
import testData.project.publishers.WidgetTypes.Types;

import java.util.*;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.shadowCss;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.CreateEditWidgetLocators.*;
import static pages.dash.publisher.locators.WidgetListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.OthersData.LINK_TO_RESOURCES_FILES;
import static testData.project.publishers.ServicerData.videoLibVersion;

public class Widget {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final CreateEditWidgetHelper widgetHelper;
    private final CreateEditVideoHelper videoHelper;
    private final WidgetListHelper widgetListHelper;

    public enum MobileWidgetType {
        TOASTER("toaster"),
        DRAGDOWN("dragdown"),
        INTERSTITIAL("interstitial");

        private final String mobileType;

        MobileWidgetType(String mobileType) {
            this.mobileType = mobileType;
        }
        public String getTypeValue() {
            return mobileType;
        }
    }

    public Widget(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        widgetHelper = new CreateEditWidgetHelper(log, helpersInit);
        widgetListHelper = new WidgetListHelper(log, helpersInit);
        videoHelper = new CreateEditVideoHelper(log, helpersInit);
    }

    private Types widgetType;
    private SubTypes subType;
    private SubnetType subnetId = SubnetType.SCENARIO_MGID;
    private int widgetId = 0;
    private boolean isShadowDom = false;
    private boolean isAmp = false;
    private boolean showToaster = true;
    private boolean showDragDown = true;
    private boolean showInterstitial = false;
    private Integer columns, rows, impressionEveryPassage;
    private boolean zoomOnHover, innerShadow, teaserCardShadow, teaserFixedWidth, isDomainShow,
            isInfiniteScroll, showBannerButton, bannerButtonEffect, rerunAds, labelTumbler, autoScroll, nativeBackfill;
    private String widgetName, webSiteName, widgetTitle, columnsMobile, widgetTheme, colorButtonBanner, typeBlurBanner, colorBlurBanner, autoplacement;
    private String backgroundColor, widgetBorder, borderColor, teaserBorderWidth, teaserBorderColor;
    private String imageFormat, imageBorder, imageBorderColor, imagePadding;
    private String headlineTextTransform, headlineTextFont, headlineFontSize, headlineFontColor, headlinePosition;
    private String widgetTitleTextTransform, widgetTitleTextFont, widgetTitleFontSize, widgetTitleFontColor;
    private String domainTextFont, domainFontSize, domainFontColor, domainPosition;
    private String position, frequencyInSiteNotification;
    private String titleRule, countryRule, trafficTypeRule;
    private String frequencyCappingImpressions, frequencyCappingMinutes, toasterInactivityTime, showAfterInteraction;
    private String popUpTitle, popUpCapping;
    private String teaserMargin, teaserBorderRadius, teaserWidth, teaserHeight;
    private JSONObject dataWidget;
    private String pathToDataWidgetFile;
    private Integer cloneId;
    private List intExchangePlacementItems;
    private List directDemandPlacementItems;
    private int videoPlacementItems;
    private String linkOnBlockTeaser;
    private String blockedTeaserTitle;

    private ArrayList<String> headlineStyleText = new ArrayList<>();
    private ArrayList<String> domainStyleText = new ArrayList<>();
    private ArrayList<String> categoriesList = new ArrayList<>();
    private ArrayList<String> videoGroupsList = new ArrayList<>();
    private Map<String, Boolean> contentTypes = new HashMap<>();

    ////////////////// Getter/Setter ////////////////////////////////////
    public int getWidgetId() {
        return widgetId;
    }

    public Integer getCloneId() {
        return cloneId;
    }

    public void setCloneId(Integer cloneId) {
        this.cloneId = cloneId;
    }

    public String getLinkOnBlockTeaser() {
        return linkOnBlockTeaser;
    }

    public String getBlockedTeaserTitle() {
        return blockedTeaserTitle;
    }

    public Map<String, Boolean> getContentTypes() {
        return contentTypes;
    }

    public Widget setContentTypes(Map<String, Boolean> contentTypes) {
        this.contentTypes = contentTypes;
        return this;
    }

    public Widget setColumns(Integer columns) {
        this.columns = columns;
        return this;
    }

    public Widget setRows(Integer rows) {
        this.rows = rows;
        return this;
    }

    public void setIntExchangePlacementItems(List intExchangePlacementItems) {
        this.intExchangePlacementItems = intExchangePlacementItems;
    }

    public Widget setVideoPlacementItems(int videoPlacementItems) {
        this.videoPlacementItems = videoPlacementItems;
        return this;
    }

    public Widget setNativeBackfill(boolean nativeBackfill) {
        this.nativeBackfill = nativeBackfill;
        return this;
    }

    public ArrayList<String> getCategoriesList() {
        return categoriesList;
    }

    public boolean isRerunAds() {
        return rerunAds;
    }

    public Widget setRerunAds(boolean reRun){
        rerunAds = reRun;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public Widget setIsCloseCodePopup(boolean isClose) {
        this.widgetHelper.setClosePopup(isClose);
        return this;
    }

    public Widget setAutoplacement(String autoplacement) {
        this.autoplacement = autoplacement;
        widgetHelper.setCustomAutoplacement(true);
        return this;
    }

    public Widget setSubnetId(SubnetType subnetId) {
        this.subnetId = subnetId;
        return this;
    }

    public Widget setInfiniteScroll(boolean infiniteScroll) {
        isInfiniteScroll = infiniteScroll;
        return this;
    }

    public Widget setShadowDom(boolean isShadow){
        this.widgetHelper.setShadowDom(isShadow);
        this.isShadowDom = isShadow;
        return this;
    }

    public Widget setFrequencyCappingImpressions(String frequencyCappingImpressions) {
        this.frequencyCappingImpressions = frequencyCappingImpressions;
        return this;
    }

    public Widget setFrequencyCappingMinutes(String frequencyCappingMinutes) {
        this.frequencyCappingMinutes = frequencyCappingMinutes;
        return this;
    }

    public Widget setToasterInactivityTime(String toasterInactivityTime) {
        this.toasterInactivityTime = toasterInactivityTime;
        return this;
    }

    public Widget setShowAfterInteraction(String showAfterInteraction) {
        this.showAfterInteraction = showAfterInteraction;
        return this;
    }

    public Widget setImpressionEveryPassage(Integer impressionEveryPassage) {
        this.impressionEveryPassage = impressionEveryPassage;
        return this;
    }

    ////////////////// Methods ////////////////////////////////////////////


    public Widget switchToAmpFrame(){
        if(STAND_P_1.exists()) STAND_P_1.shouldBe(visible).scrollTo();

        switchTo().frame(STAND_FRAME_AMP);
        isAmp = true;
        return this;
    }

    public Widget scrollToTopWidgetSide(){
        executeJavaScript("document.querySelector(\"[class*='mgbox'] [class*='mgline']:nth-child(1)\").scrollIntoView()");
        return this;
    }

    /**
     * get path to widgets data file
     */
    public void getPathToWidgetsJsonData(String fileName) {
        pathToDataWidgetFile = LINK_TO_RESOURCES_FILES + fileName + ".txt";
    }

    /**
     * get custom param
     */
    public String getCustomWidgetParam(String param) {
        return (String) dataWidget.get(param);
    }

    /**
     * get custom params
     */
    public ArrayList<String> getCustomWidgetParams(String param) {
        if (dataWidget.get(param) != null) {
            return new ArrayList<>(Collections.unmodifiableList((ArrayList<String>) dataWidget.get(param)));
        }
        return null;
    }

    /**
     * get all params for current widget
     */
    public void getDataWidgets(Types widgetType, SubTypes subType) {
        dataWidget = widgetHelper.getJsonFromFile(pathToDataWidgetFile, widgetType, subType);

        if (dataWidget != null) {
            columns = helpersInit.getBaseHelper().parseInt(getCustomWidgetParam("columns"));
            rows = helpersInit.getBaseHelper().parseInt(getCustomWidgetParam("rows"));
            autoplacement = getCustomWidgetParam("autoplacement");
            columnsMobile = getCustomWidgetParam("columnsMobile");
            widgetTheme = getCustomWidgetParam("widgetTheme");
            widgetTitle = getCustomWidgetParam("widgetTitle");

            backgroundColor = getCustomWidgetParam("backgroundColor");
            widgetBorder = getCustomWidgetParam("widgetBorder");
            borderColor = getCustomWidgetParam("borderColor");
            teaserBorderWidth = getCustomWidgetParam("teaserBorderWidth");
            teaserBorderColor = getCustomWidgetParam("teaserBorderColor");

            imageFormat = getCustomWidgetParam("imageFormat");
            imageBorder = getCustomWidgetParam("imageBorder");
            imageBorderColor = getCustomWidgetParam("imageBorderColor");
            imagePadding = getCustomWidgetParam("imagePadding");
            zoomOnHover = Boolean.parseBoolean(getCustomWidgetParam("zoomOnHover"));
            innerShadow = Boolean.parseBoolean(getCustomWidgetParam("innerShadow"));
            headlineTextTransform = getCustomWidgetParam("headlineTextTransform");
            headlineTextFont = getCustomWidgetParam("headlineTextFont");
            headlineFontSize = getCustomWidgetParam("headlineFontSize");
            headlineFontColor = getCustomWidgetParam("headlineFontColor");
            headlinePosition = getCustomWidgetParam("headlinePosition");
            headlineStyleText = getCustomWidgetParams("headlineStyleText");
            widgetTitleTextTransform = getCustomWidgetParam("widgetTitleTextTransform");
            widgetTitleTextFont = getCustomWidgetParam("widgetTitleTextFont");
            widgetTitleFontSize = getCustomWidgetParam("widgetTitleFontSize");
            widgetTitleFontColor = getCustomWidgetParam("widgetTitleFontColor");
            isDomainShow = Boolean.parseBoolean(getCustomWidgetParam("isDomainShow"));
            domainTextFont = getCustomWidgetParam("domainTextFont");
            domainFontSize = getCustomWidgetParam("domainFontSize");
            domainFontColor = getCustomWidgetParam("domainFontColor");
            domainPosition = getCustomWidgetParam("domainPosition");
            domainStyleText = getCustomWidgetParams("domainStyleText");
            position = getCustomWidgetParam("position");
            showToaster = Boolean.parseBoolean(getCustomWidgetParam("showToaster"));
            showDragDown = Boolean.parseBoolean(getCustomWidgetParam("showDragDown"));
            showInterstitial = Boolean.parseBoolean(getCustomWidgetParam("showInterstitial"));
            frequencyCappingImpressions = getCustomWidgetParam("frequencyCappingImpressions");
            frequencyCappingMinutes = getCustomWidgetParam("frequencyCappingMinutes");
            toasterInactivityTime = getCustomWidgetParam("toasterInactivityTime");
            popUpTitle = getCustomWidgetParam("popUpTitle");
            popUpCapping = getCustomWidgetParam("popUpCapping");
            teaserMargin = getCustomWidgetParam("teaserMargin");
            teaserBorderRadius = getCustomWidgetParam("teaserBorderRadius");
            teaserWidth = getCustomWidgetParam("teaserWidth");
            teaserHeight = getCustomWidgetParam("teaserHeight");
            teaserCardShadow = Boolean.parseBoolean(getCustomWidgetParam("teaserCardShadow"));
            teaserFixedWidth = Boolean.parseBoolean(getCustomWidgetParam("teaserFixedWidth"));
            isInfiniteScroll = Boolean.parseBoolean(getCustomWidgetParam("isInfiniteScroll"));

            showBannerButton = Boolean.parseBoolean(getCustomWidgetParam("buttonBanner"));
            colorButtonBanner = getCustomWidgetParam("colorButtonBanner");
            bannerButtonEffect = Boolean.parseBoolean(getCustomWidgetParam("bannerButtonEffect"));
            typeBlurBanner = getCustomWidgetParam("blurTypeBanner");

            rerunAds = Boolean.parseBoolean(getCustomWidgetParam("rerunAds"));
            frequencyInSiteNotification = getCustomWidgetParam("frequencyInSiteNotification");
            autoScroll = Boolean.parseBoolean(getCustomWidgetParam("autoScroll"));
            labelTumbler = Boolean.parseBoolean(getCustomWidgetParam("labelTumbler"));
            impressionEveryPassage = helpersInit.getBaseHelper().parseInt(getCustomWidgetParam("impressionEveryPassage"));
        }
    }

    /**
     * Создаём виджет с указанным типом
     * Получаем id виджета
     */
    public void getDataAndSetTypeWidgets(Types widgetType, SubTypes subType) {
        this.widgetType = widgetType;
        waitWidgetConstructorReady();
        widgetHelper.chooseTypeWidget(widgetType);
        this.subType = !subType.getTypeValue().equals("none") ? widgetHelper.chooseSubType(subType) : SubTypes.NONE;
        getDataWidgets(widgetType, subType);
        widgetName = widgetHelper.getWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        setCategoriesBlock();
    }

    /**
     * set type and subType if they don't set
     */
    public Widget setTypeAndSubTypeWidgetIfTheyDontSet(Types widgetType, SubTypes subType) {
        this.widgetType = widgetType;
        waitWidgetConstructorReady();
        widgetHelper.chooseTypeWidget(widgetType);
        this.subType = subType != SubTypes.NONE ? widgetHelper.chooseSubType(subType) : SubTypes.NONE;
        getDataWidgets(widgetType, subType);
        setCategoriesBlock();

        return this;
    }

    /**
     * Сохраняем виджет и получаем его ID
     * Получаем id виджета
     */
    public void saveWidgetAndGetId() {
        waitWidgetConstructorReady();
        widgetHelper.clickCreateAndClosePopUp();
        widgetId = widgetHelper.getWidgetIdFromUrl();
        log.info("widgetId: " + widgetId);
    }

    /**
     * Создаём виджет
     * Получаем id виджета
     */
    public void createWidget(Types widgetType, WidgetTypes.SubTypes subType) {
        this.widgetType = widgetType;
        waitWidgetConstructorReady();
        widgetHelper.chooseTypeWidget(widgetType);
        this.subType = subType != SubTypes.NONE ? widgetHelper.chooseSubType(subType) : SubTypes.NONE;
        setCategoriesBlock();
        switch (widgetType) {
            case UNDER_ARTICLE -> {
                switch (subType) {
                    case UNDER_ARTICLE_CARDS, UNDER_ARTICLE_LIGHT_CARDS, UNDER_ARTICLE_RECTANGULAR,
                            UNDER_ARTICLE_SQUARE, UNDER_ARTICLE_SQUARE_BLACK -> {
                        getGeneralUnderArticle();
                        getDetailedUnderArticle();
                    }
                    case UNDER_ARTICLE_LIGHT, UNDER_ARTICLE_DARK -> {
                        getGeneralUnderArticle();
                        getDetailedUnderArticleAdskeeper();
                    }
                }
            }
            case IN_ARTICLE -> {
                switch (subType) {
                    case IN_ARTICLE_MAIN, IN_ARTICLE_CAROUSEL -> {
                        getGeneralInArticle();
                        getDetailedUnderArticle();
                    }
                    case IN_ARTICLE_CAROUSEL_NEW -> {
                        getGeneralInArticleCarouselNew();
                        getDetailedUnderArticleAdskeeper();
                    }
                    case IN_ARTICLE_IMPACT -> {
                    }
                }
            }
            case HEADER -> {
                switch (subType) {
                    case HEADER_RECTANGULAR,
                            HEADER_SQUARE -> {
                        getGeneralHeader();
                        getDetailedUnderArticle();
                    }
                    case HEADER_LIGHT,
                            HEADER_DARK -> {
                        getGeneralHeader();
                        getDetailedUnderArticleAdskeeper();
                    }
                }
            }
            case SIDEBAR -> {
                switch (subType) {
                    case SIDEBAR_CARDS, SIDEBAR_RECTANGULAR_2, SIDEBAR_RECTANGULAR, SIDEBAR_SQUARE_SMALL -> {
                        getGeneralUnderArticle();
                        getDetailedUnderArticle();
                    }
                    case SIDEBAR_LIGHT, SIDEBAR_DARK -> {
                        getGeneralUnderArticle();
                        getDetailedUnderArticleAdskeeper();
                    }
                }
            }
            case HEADLINE -> getGeneralUnderArticle();
            case BANNER ->  fillBanner();
            case EXIT_POP_UP -> fillPopUpWidget();
            case MOBILE_EXIT -> fillMobileExitWidget();
            case MOBILE -> fillMobileWidget("create");
        }
        widgetHelper.clickCreateAndClosePopUp();
        widgetId = widgetHelper.getWidgetIdFromUrl();
        log.info("widgetId: " + widgetId);
    }

    /**
     * Редактируем виджет
     * Получаем id виджета
     */
    public void editWidget() {
        widgetHelper.switchToGeneralTab();
        setCategoriesBlock();
        switch (widgetType) {
            case SMART, FEED:
                switch (subType) {
                    case SMART_PLUS, SMART_MAIN, SMART_BLUR, NONE -> fillSmart();
                }
                break;
            case UNDER_ARTICLE:
                switch (subType) {
                    case UNDER_ARTICLE_CARDS, UNDER_ARTICLE_LIGHT_CARDS, UNDER_ARTICLE_RECTANGULAR, UNDER_ARTICLE_SQUARE, UNDER_ARTICLE_SQUARE_BLACK -> {
                        fillGeneralUnderArticle();
                        fillDetailedUnderArticle();
                    }
                    case UNDER_ARTICLE_LIGHT, UNDER_ARTICLE_DARK -> {
                        fillGeneralUnderArticle();
                        fillDetailedUnderArticleAdskeeper();
                    }
                    case UNDER_ARTICLE_MAIN, UNDER_ARTICLE_PAIR_CARDS -> {
                        fillGeneralUnderArticleMain();
                        fillDetailedUnderArticle();
                    }
                    case UNDER_ARTICLE_CARD_MEDIA -> fillGeneralInArticleCardMedia();
                }
                break;
            case TEXT_ON_IMAGE:
                fillGeneralTextOnImage();
                fillDetailedCarousel();
                break;
            case CAROUSEL:
                switch (subType) {
                    case CAROUSEL_MAIN -> {
                        fillGeneralCarousel("edit");
                        fillDetailedCarousel();
                    }
                    case CAROUSEL_VERTICAL -> {
                        fillGeneralCarouselVertical();
                        fillDetailedCarouselVertical();
                    }
                }
                break;
            case IN_ARTICLE:
                switch (subType) {
                    case IN_ARTICLE_MAIN -> {
                        fillGeneralInArticle();
                        fillDetailedUnderArticle();
                    }
                    case IN_ARTICLE_CAROUSEL -> {
                        fillGeneralInArticleCarousel("edit");
                        fillDetailedInArticleCarousel();
                    }
                    case IN_ARTICLE_IMPACT, IN_ARTICLE_DOUBLE_PICTURE -> fillGeneralInArticleImpact();
                    case IN_ARTICLE_CARD_MEDIA -> fillGeneralInArticleCardMedia();
                }
                break;
            case HEADER:
                switch (subType) {
                    case HEADER_RECTANGULAR, HEADER_SQUARE -> {
                        fillGeneralHeader();
                        fillDetailedUnderArticle();
                    }
                    case HEADER_LIGHT, HEADER_DARK -> {
                        fillGeneralHeader();
                        fillDetailedUnderArticleAdskeeper();
                    }
                }
                break;
            case SIDEBAR:
                switch (subType) {
                    case SIDEBAR_CARDS, SIDEBAR_RECTANGULAR_2, SIDEBAR_RECTANGULAR, SIDEBAR_SQUARE_SMALL -> {
                        fillGeneralUnderArticle();
                        fillDetailedUnderArticle();
                    }
                    case SIDEBAR_LIGHT, SIDEBAR_DARK -> {
                        fillGeneralUnderArticle();
                        fillDetailedUnderArticleAdskeeper();
                    }
                    case SIDEBAR_IMPACT, SIDEBAR_FRAME -> fillGeneralSidebarImpact();
                    case SIDEBAR_BLUR -> {
                        fillGeneralSidebarImpact();
                        fillDetailedSidebarBlur();
                    }
                }
                break;
            case HEADLINE:
                fillGeneralUnderArticle();
                fillDetailedHeadlineInPicture();
                break;
            case BANNER:
                if (SubTypes.BANNER_300x250_4 == subType) {
                    fillBannerWithButton();
                } else {
                    fillBanner();
                }
                break;
            case IN_SITE_NOTIFICATION:
                fillInSiteNotification();
                break;
            case PASSAGE:
                fillPassageWidget();
                break;
            case EXIT_POP_UP:
                fillPopUpWidget();
                break;
            case MOBILE_EXIT:
                fillMobileExitWidget();
                break;
            case MOBILE:
                fillMobileWidget("edit");
                break;

        }
        widgetHelper.clickCreateAndClosePopUp();
        log.info("editWidget - finished");
    }

    /**
     * Проверяем основные поля виджета
     * Получаем id виджета
     */
    public boolean checkWidget(SubnetType... subnetId) {
        boolean switch_result = false;
        widgetHelper.switchToGeneralTab();
        if (checkCategoriesBlock(categoriesList)) {
            log.info("checkWidget - start");
            switch (widgetType) {
                case SMART, FEED:
                    switch (subType) {
                        case SMART_PLUS, SMART_MAIN, SMART_BLUR, NONE -> switch_result = checkSmart();
                    }
                    break;
                case UNDER_ARTICLE:
                    switch_result = switch (subType) {
                        case UNDER_ARTICLE_CARDS, UNDER_ARTICLE_LIGHT_CARDS, UNDER_ARTICLE_RECTANGULAR, UNDER_ARTICLE_SQUARE, UNDER_ARTICLE_SQUARE_BLACK
                                -> checkGeneralUnderArticle() &
                                checkDetailedUnderArticle();
                        case UNDER_ARTICLE_LIGHT, UNDER_ARTICLE_DARK -> checkGeneralUnderArticle() &
                                checkDetailedUnderArticleAdskeeper();
                        case UNDER_ARTICLE_MAIN, UNDER_ARTICLE_PAIR_CARDS -> checkGeneralUnderArticleMain() &
                                checkDetailedUnderArticle();
                        case UNDER_ARTICLE_CARD_MEDIA -> checkGeneralInArticleCardMedia();
                        default -> false;
                    };
                    break;
                case TEXT_ON_IMAGE:
                    switch_result = checkGeneralUnderArticle() &
                            checkDetailedTextOnImage();
                    break;
                case CAROUSEL:
                    switch_result = switch (subType) {
                        case CAROUSEL_MAIN -> checkGeneralCarousel() &
                                checkDetailedCarousel();
                        case CAROUSEL_VERTICAL-> checkGeneralCarouselVertical() &
                                checkDetailedCarouselVertical();
                        default -> false;
                    };
                    break;
                case IN_ARTICLE:
                    switch_result = switch (subType) {
                        case IN_ARTICLE_MAIN -> checkGeneralInArticle() &
                                checkDetailedInArticle();
                        case IN_ARTICLE_CAROUSEL -> checkGeneralInArticleCarousel() &
                                checkDetailedInArticleCarousel();
                        case IN_ARTICLE_IMPACT, IN_ARTICLE_DOUBLE_PICTURE -> checkGeneralInArticleImpact();
                        case IN_ARTICLE_CARD_MEDIA -> checkGeneralInArticleCardMedia();
                        default -> false;
                    };
                    break;
                case HEADER:
                    switch_result = switch (subType) {
                        case HEADER_RECTANGULAR, HEADER_SQUARE -> checkGeneralHeader() &
                                checkDetailedUnderArticle();
                        case HEADER_LIGHT, HEADER_DARK -> checkGeneralHeader() &
                                checkDetailedUnderArticleAdskeeper();
                        default -> false;
                    };
                    break;
                case SIDEBAR:
                    switch_result = switch (subType) {
                        case SIDEBAR_CARDS, SIDEBAR_RECTANGULAR_2, SIDEBAR_SQUARE_SMALL -> checkGeneralUnderArticle() &
                                checkDetailedUnderArticle();
                        case SIDEBAR_RECTANGULAR -> checkGeneralSideBarRectangular() &
                                checkDetailedUnderArticle();
                        case SIDEBAR_LIGHT, SIDEBAR_DARK -> checkGeneralUnderArticle() &
                                checkDetailedUnderArticleAdskeeper();
                        case SIDEBAR_IMPACT, SIDEBAR_FRAME -> checkGeneralSidebarImpact();
                        case SIDEBAR_BLUR -> checkGeneralSidebarImpact() &
                                checkDetailedSidebarBlur();
                        default -> false;
                    };
                    break;
                case HEADLINE:
                    switch_result = checkGeneralUnderArticle() &
                            checkDetailedHeaderInPicture();
                    break;
                case BANNER:
                    if (SubTypes.BANNER_300x250_4 == subType) {
                        switch_result = checkBannerWithButton(subnetId[0]);
                    } else {
                        switch_result = checkBanner(subnetId[0]);
                    }
                    break;
                case IN_SITE_NOTIFICATION:
                    switch_result = checkInSiteNotification();
                    break;
                case PASSAGE:
                    switch_result = checkPassage();
                    break;
                case EXIT_POP_UP:
                    switch_result = checkPopUpWidget();
                    break;
                case MOBILE_EXIT:
                    switch_result = checkMobileExitWidget();
                    break;
                case MOBILE:
                    switch_result = checkMobileWidget();
                    break;
            }
        }
        log.info("checkWidget switch_result: " + switch_result);
        return switch_result;
    }

    ///////////////////////////////////////// GENERAL BLOCK - START //////////////////////////////////////

    /**
     * устанавливаем 5 рандомных категорий в CATEGORIES-BLOCK (для обменных виджетов IM)
     */
    public void setCategoriesBlock() {
        categoriesList = widgetHelper.setCategories();
    }

    /**
     * проверяем выбранные категории в CATEGORIES-BLOCK (для обменных виджетов IM)
     */
    public boolean checkCategoriesBlock(ArrayList<String> list) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(widgetHelper.checkCategories(list));
    }

    public String getCategoryTumblerStatus(){
        waitWidgetConstructorReady();
        return widgetHelper.getCategoryTumblerStatus();
    }

    public void turnOnCategoryTumbler(boolean state){ widgetHelper.turnOnCategoryTumbler(state); }

    /**
     * получаем настройки GENERAL Settings для "Under Article Widget"
     */
    public void getGeneralUnderArticle() {
        widgetName = widgetHelper.getWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.getColumns();
        rows = widgetHelper.getRows();
        columnsMobile = widgetHelper.getColumnsMobile();
        widgetTheme = widgetHelper.getTheme();
        widgetTitle = widgetHelper.getWidgetTitle();
    }

    /**
     * получаем настройки GENERAL Settings для "header-widget"
     */
    public void getGeneralHeader() {
        widgetName = widgetHelper.getWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.getColumns();
        widgetTheme = widgetHelper.getTheme();
        widgetTitle = widgetHelper.getWidgetTitle();
    }

    /**
     * получаем настройки GENERAL Settings для "in-article"
     */
    public void getGeneralInArticle() {
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
        widgetName = widgetHelper.getWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTheme = widgetHelper.getTheme();
        widgetTitle = widgetHelper.getWidgetTitle();
    }

    /**
     * получаем настройки GENERAL Settings для "in-article-carousel-new"
     */
    public void getGeneralInArticleCarouselNew() {
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
        widgetName = widgetHelper.getWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTheme = widgetHelper.getTheme();
    }

    /**
     * заполняем GENERAL Settings для "Under Article Widget"
     */
    public void fillGeneralUnderArticle() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        rows = widgetHelper.chooseRows();
        columnsMobile = widgetHelper.chooseColumnsMobile();
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "Under Article Widget MAIN"
     */
    public void fillGeneralUnderArticleMain() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        rows = widgetHelper.chooseRows();
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    public void fillGeneralSidebarImpact() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        rows = widgetHelper.chooseRows();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "Text-on-image"
     */
    public void fillGeneralTextOnImage() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        rows = widgetHelper.chooseRows();
        columnsMobile = widgetHelper.chooseColumnsMobile();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "Carousel"
     */
    public void fillGeneralCarousel(String action) {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        widgetTitle = widgetHelper.chooseWidgetTitle();
        switchAutoScroll(action);
    }

    /**
     * заполняем GENERAL Settings для "Carousel-vertical"
     */
    public void fillGeneralCarouselVertical() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
    }

    /**
     * заполняем GENERAL Settings для "header-widget"
     */
    public void fillGeneralHeader() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "banner"
     */
    public void fillBanner() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        subType = widgetHelper.chooseSubTypeForBanner();
        if(subType != SubTypes.BANNER_970x250 & subType != SubTypes.BANNER_470x325) {
            widgetTheme = widgetHelper.chooseTheme();
        }
    }

    /**
     * заполняем GENERAL Settings для "banner" with BUTTON
     */
    public void fillBannerWithButton() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();

        // color button
        showBannerButton = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        log.info("showBannerButton: " + showBannerButton);
        widgetHelper.switchBannerButtonTumbler(showBannerButton);
        if (showBannerButton) colorButtonBanner = widgetHelper.chooseColorButtonBanner();

        // button effect
        bannerButtonEffect = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        log.info("bannerButtonEffect: " + bannerButtonEffect);
        if (showBannerButton) widgetHelper.switchBannerButtonEffectTumbler(bannerButtonEffect);

        // blur
        typeBlurBanner = widgetHelper.chooseTypeBannerBlur();
        if (typeBlurBanner.equals("custom")) colorBlurBanner = widgetHelper.chooseColorBlurBanner();
    }

    /**
     * заполняем GENERAL Settings для 'in-site-notification'
     */
    public void fillInSiteNotification() {
        rerunAds = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        labelTumbler = false;
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        rows = widgetHelper.chooseRows();
        if(rerunAds) frequencyInSiteNotification = widgetHelper.chooseFrequencyInSiteNotification();
        position = widgetHelper.choosePositionInSiteNotification();
        widgetHelper.switchRerunAdsTumbler(rerunAds);

        if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) widgetHelper.switchLabelTumbler(labelTumbler);
    }

    /**
     * заполняем GENERAL Settings для "banner"
     */
    public void fillSmart() {
        isInfiniteScroll = false;
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTitle = widgetHelper.chooseWidgetTitle();
        widgetHelper.switchInfiniteScrollTumbler(isInfiniteScroll);
    }

    /**
     * заполняем GENERAL Settings для "mobile-widget"
     */
    public void fillMobileWidget(String action) {
        fillMobileDetailSettings(action);
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем дополнительные настройки GENERAL tab для mobile-widget
     *
     * @param action - create/edit
     */
    public void fillMobileDetailSettings(String action) {
        if(action.equals("edit")) {
            showToaster = true;
            showDragDown = false;
            showInterstitial = true;
            frequencyCappingImpressions = null;
            frequencyCappingMinutes = null;
            toasterInactivityTime = null;
            showAfterInteraction = null;
        }
        widgetHelper.switchToasterTumbler(showToaster);
        widgetHelper.switchDragDownTumbler(showDragDown);
        widgetHelper.switchInterstitialTumbler(showInterstitial);
        if (showToaster) {
            frequencyCappingImpressions = widgetHelper.setFrequencyCappingImpressions(frequencyCappingImpressions);
            frequencyCappingMinutes = widgetHelper.setFrequencyCappingMinutes(frequencyCappingMinutes);
            if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
                toasterInactivityTime = widgetHelper.setToasterInactivityTime(toasterInactivityTime);
            }
        }
        if (showInterstitial) {
            showAfterInteraction = widgetHelper.setShowAfterInteraction(showAfterInteraction);
        }
    }

    /**
     * заполняем GENERAL Settings для "exit-pop-up"
     */
    public void fillPopUpWidget() {
        popUpTitle = widgetHelper.setPopUpTitle();
        popUpCapping = widgetHelper.setPopUpCapping();
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTheme = widgetHelper.chooseTheme();
    }

    /**
     * заполняем GENERAL Settings для "passage"
     */
    public void fillPassageWidget() {
        impressionEveryPassage = widgetHelper.chooseImpressionsPassage();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "exit-pop-up"
     */
    public void fillMobileExitWidget() {
        popUpTitle = widgetHelper.setPopUpTitle();
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTheme = widgetHelper.chooseTheme();
    }

    /**
     * заполняем GENERAL Settings для "in-article"
     */
    public void fillGeneralInArticle() {
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "in-article-carousel-super"
     */
    public void fillGeneralInArticleCarousel(String action) {
        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
            switchAutoScroll(action);
        }

        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * заполняем GENERAL Settings для "in-article-impact"
     */
    public void fillGeneralInArticleImpact() {
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
    }

    /**
     * заполняем GENERAL Settings для "in-article-card-media"
     */
    public void fillGeneralInArticleCardMedia() {
        widgetName = widgetHelper.chooseWidgetName();
        webSiteName = widgetHelper.getWebSiteName();
        columns = widgetHelper.chooseColumns();
        rows = widgetHelper.chooseRows();
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    /**
     * Проверяем GENERAL TAB - Under Article Widget
     */
    public boolean checkGeneralUnderArticle() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRowsAndColumns(rows, columns) &
                widgetHelper.checkColumnsMobile(columnsMobile) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - Under Article Widget MAIN
     */
    public boolean checkGeneralUnderArticleMain() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRowsAndColumns(rows, columns) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - Under Article Widget
     */
    public boolean checkGeneralSidebarImpact() {
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRows(rows) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - Carousel
     */
    public boolean checkGeneralCarousel() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkColumns(columns) &
                widgetHelper.checkWidgetTitle(widgetTitle) &
                widgetHelper.checkAutoScroll(autoScroll);
    }

    /**
     * Проверяем GENERAL TAB - Carousel-vertical
     */
    public boolean checkGeneralCarouselVertical() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkColumns(columns);
    }

    /**
     * Проверяем GENERAL TAB - sidebar-widget-rectangular
     */
    public boolean checkGeneralSideBarRectangular() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRowsAndColumns(rows, columns) &
                widgetHelper.checkColumnsMobile(columnsMobile) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - header-widget
     */
    public boolean checkGeneralHeader() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkColumns(columns) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - in-article
     */
    public boolean checkGeneralInArticle() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkAutoplacement(autoplacement) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - in-article-carousel
     */
    public boolean checkGeneralInArticleCarousel() {
        boolean result = true;
        widgetHelper.switchToGeneralTab();

        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            result = widgetHelper.checkAutoplacement(autoplacement) &
                    widgetHelper.checkAutoScroll(autoScroll);
        }

        return result &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkColumns(columns) &
                widgetHelper.checkWidgetTitle(widgetTitle);
    }

    /**
     * Проверяем GENERAL TAB - in-article-impact
     */
    public boolean checkGeneralInArticleImpact() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkAutoplacement(autoplacement) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                !widgetHelper.isShowDetailedTab();
    }

    /**
     * Проверяем GENERAL TAB - in-article-card-media
     */
    public boolean checkGeneralInArticleCardMedia() {
        widgetHelper.switchToGeneralTab();
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkSubType(subType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRowsAndColumns(rows, columns) &
                widgetHelper.checkWidgetTitle(widgetTitle) &
                checkImageZoomOnHover(zoomOnHover) &
                !widgetHelper.isShowDetailedTab();
    }

    /**
     * Проверяем GENERAL TAB - banner
     */
    public boolean checkBanner(SubnetType subnetId) {
        return !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkSubTypeForBanner(subType, subnetId) &
                widgetHelper.checkMgBoxSize(subType);
    }

    /**
     * Проверяем GENERAL TAB - banner with button
     */
    public boolean checkBannerWithButton(SubnetType subnetId){
        return !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkSubTypeForBanner(subType, subnetId) &
                widgetHelper.checkMgBoxSize(subType) &
                widgetHelper.checkColorButtonBanner(showBannerButton, colorButtonBanner) &
                widgetHelper.checkBannerButtonEffect(showBannerButton, bannerButtonEffect) &
                widgetHelper.checkBlurBanner(typeBlurBanner, colorBlurBanner);
    }

    /**
     * Проверяем GENERAL TAB - in-site-notification
     */
    public boolean checkInSiteNotification() {
        return !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkRows(rows) &
                widgetHelper.checkRerunAdsTumbler(rerunAds) &
                widgetHelper.checkFrequencyInSiteNotification(rerunAds, frequencyInSiteNotification) &
                widgetHelper.checkPositionInSiteNotification(position) &
                ( !subnetId.equals(SubnetType.SCENARIO_ADSKEEPER) ||
                        (widgetHelper.checkLabelTumbler(labelTumbler) &
                        widgetHelper.checkLabelTumblerInConstructor(labelTumbler))
                );
    }

    /**
     * Проверяем GENERAL TAB - Passage
     */
    public boolean checkPassage() {
        return !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkWidgetTitle(widgetTitle) &
                widgetHelper.checkPassageImpressions(impressionEveryPassage);
    }

    /**
     * Проверяем GENERAL TAB - Smart
     */
    public boolean checkSmart() {
        return !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkWidgetTitle(widgetTitle) &
                widgetHelper.checkInfiniteScroll(isInfiniteScroll);
    }

    /**
     * Проверяем GENERAL TAB - mobile-widget
     */
    public boolean checkMobileWidget() {
        return widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                widgetHelper.checkWidgetTitle(widgetTitle) &
                !widgetHelper.isShowDetailedTab() &
                checkMobileDetailSettings();
    }

    /**
     * Проверяем детальные настройки GENERAL TAB - mobile-widget
     */
    public boolean checkMobileDetailSettings() {
        return widgetHelper.checkSwitchToasterTumbler(showToaster) &
                widgetHelper.checkFrequencyCappingImpressions(showToaster, frequencyCappingImpressions) &
                widgetHelper.checkFrequencyCappingMinutes(showToaster, frequencyCappingMinutes) &
                (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER) || widgetHelper.checkInactivityTime(showToaster, toasterInactivityTime)) &
                widgetHelper.checkSwitchDragDownTumbler(showDragDown) &
                widgetHelper.checkSwitchInterstitialTumbler(showInterstitial) &
                widgetHelper.checkShowAfterInteraction(showInterstitial, showAfterInteraction);
    }

    /**
     * Проверяем GENERAL TAB - exit-pop-up
     */
    public boolean checkPopUpWidget() {
        return widgetHelper.checkPopUpTitle(popUpTitle) &
                widgetHelper.checkPopUpCapping(popUpCapping) &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkTheme(widgetTheme);
    }

    /**
     * Проверяем GENERAL TAB - mobile-exit
     */
    public boolean checkMobileExitWidget() {
        return widgetHelper.checkPopUpTitle(popUpTitle) &
                widgetHelper.checkTypeWidget(widgetType) &
                widgetHelper.checkWidgetName(widgetName) &
                widgetHelper.checkWebSiteName(webSiteName) &
                !widgetHelper.isShowDetailedTab() &
                widgetHelper.checkThemeInMobileExit(widgetTheme);
    }

    public Widget chooseColumns(int... columns){
        widgetHelper.chooseColumns(columns);
        return this;
    }

    public Widget chooseRows(int... rows){
        widgetHelper.chooseRows(rows);
        return this;
    }

    ///////////////////////////////////////// GENERAL BLOCK - FINISH //////////////////////////////////////


    //////////////////////////////////////// DETAILED BLOCK - START ///////////////////////////////////////

    /**
     * получаем настройки DETAILED Settings для "Under Article Widget"
     */
    public void getDetailedUnderArticle() {
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.getHeadlineFontSize();
        headlineFontColor = widgetHelper.getHeadlineFontColor();

        domainFontSize = widgetHelper.getDomainFontSize();
        domainFontColor = widgetHelper.getDomainFontColor();
    }

    /**
     * получаем настройки DETAILED Settings для "Under Article Widget", subnet - Adskeeper
     */
    public void getDetailedUnderArticleAdskeeper() {
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.getHeadlineFontSize();
        headlineFontColor = widgetHelper.getHeadlineFontColor();
    }

    /**
     * заполняем DETAILED Settings для "Under Article Widget"
     * ПРИМЕЧАНИЕ - switchImageZoomOnHover() и switchImageInnerShadow() - не могут быть включены одновременно - тогда switchImageZoomOnHover() не будет работать
     */
    public void fillDetailedUnderArticle() {
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        headlineFontColor = widgetHelper.chooseHeadlineFontColor();

        //domain
        if(!subnetId.equals(SubnetType.SCENARIO_IDEALMEDIA_IO)){
            isDomainShow = !isDomainShow;
            switchDomainShow();
            if(isDomainShow){
                domainFontSize = widgetHelper.chooseDomainFontSize();
                domainFontColor = widgetHelper.chooseDomainFontColor();
            }
        }
        else {
            domainFontSize = widgetHelper.chooseDomainFontSize();
            domainFontColor = widgetHelper.chooseDomainFontColor();
        }
    }

    public void fillDetailedHeadlineInPicture() {
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();

        //domain
        isDomainShow = !isDomainShow;
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
            domainFontColor = widgetHelper.chooseDomainFontColor();
        }
    }

    public void fillDetailedSidebarBlur() {
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();

        //domain
        isDomainShow = !isDomainShow;
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
        }
    }

    /**
     * заполняем DETAILED Settings для "Carousel"
     */
    public void fillDetailedCarousel() {
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        domainFontSize = widgetHelper.chooseDomainFontSize();
    }

    /**
     * заполняем DETAILED Settings для "Carousel-vertical"
     */
    public void fillDetailedCarouselVertical() {
        widgetHelper.switchToDetailedTab();
        domainFontSize = widgetHelper.chooseDomainFontSize();
    }

    /**
     * заполняем DETAILED Settings для "Under Article Widget", subnet - Adskeeper
     */
    public void fillDetailedUnderArticleAdskeeper() {
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        headlineFontColor = widgetHelper.chooseHeadlineFontColor();
    }

    /**
     * заполняем DETAILED Settings для "in-article-carousel"
     */
    public void fillDetailedInArticleCarousel() {
        widgetHelper.switchToDetailedTab();

        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            fillDomainSettingsForCarousel();
        }

        fillHeadlineSettingsForInArticleCarousel();
    }

    /**
     * Проверяем DETAILED TAB - Under Article Widget
     */
    public boolean checkDetailedUnderArticle() {
        widgetHelper.switchToDetailedTab();
        return checkWidgetSettings() &
                checkImageSettings() &
                checkImageZoomOnHover(zoomOnHover) &
                checkImageInnerShadow(innerShadow) &
                checkHeadlineSettings() &
                checkWidgetTitleSettings() &
                checkDomainShow() &
                (!isDomainShow || checkDomainSettings()) &
                checkPositionSettings();
    }

    public boolean checkDetailedSidebarBlur() {
        widgetHelper.switchToDetailedTab();
        return widgetHelper.checkHeadlineFontSize(headlineFontSize) &
                checkDomainShow() &
                (!isDomainShow || widgetHelper.checkDomainFontSize(domainFontSize));
    }

    /**
     * Проверяем DETAILED TAB - Under Article Widget
     */
    public boolean checkDetailedTextOnImage() {
        widgetHelper.switchToDetailedTab();
        return checkWidgetSettings() &
                checkImageSettingsForHeadline() &
                checkImageInnerShadow(innerShadow) &
                checkHeadlineSettingsForTextOnImage() &
                checkWidgetTitleSettings() &
                checkDomainSettingsForTextOnImage();
    }

    /**
     * Проверяем DETAILED TAB - Carousel
     */
    public boolean checkDetailedCarousel() {
        widgetHelper.switchToDetailedTab();
        return checkHeadlineSettingsForCarousel() &
                checkTeaserCardShadow(teaserCardShadow) &
                checkFixedWidthImageForMobile(teaserFixedWidth, teaserWidth) &
                checkCarouselSettings("margin-right") &
                checkWidgetTitleSettings() &
                checkDomainSettingsForCarousel();
    }

    /**
     * Проверяем DETAILED TAB - Carousel-vertical
     */
    public boolean checkDetailedCarouselVertical() {
        widgetHelper.switchToDetailedTab();
        return checkCarouselSettings("margin-bottom") &
                checkDomainSettingsForCarousel();
    }

    /**
     * Проверяем DETAILED TAB - headline-in-picture
     */
    public boolean checkDetailedHeaderInPicture() {
        widgetHelper.switchToDetailedTab();
        return checkWidgetSettings() &
                checkImageSettingsForHeadline() &
                checkImageInnerShadow(innerShadow) &
                checkWidgetTitleSettings() &
                widgetHelper.checkHeadlineFontSize(headlineFontSize) &
                checkDomainShow() &
                (!isDomainShow || checkDomainSettings());
    }

    /**
     * Проверяем DETAILED TAB - Under Article Widget, subnet - Adskeeper
     */
    public boolean checkDetailedUnderArticleAdskeeper() {
        widgetHelper.switchToDetailedTab();
        return checkWidgetSettings() &
                checkImageSettings() &
                checkHeadlineSettings() &
                checkWidgetTitleSettings() &
                checkPositionSettings();
    }

    /**
     * Проверяем DETAILED TAB - in-article
     */
    public boolean checkDetailedInArticle() {
        widgetHelper.switchToDetailedTab();
        return checkWidgetSettings() &
                checkImageSettings() &
                checkImageZoomOnHover(zoomOnHover) &
                checkImageInnerShadow(innerShadow) &
                checkHeadlineSettings() &
                checkWidgetTitleSettings() &
                checkDomainShow() &
                (!isDomainShow || checkDomainSettings());
    }

    /**
     * Проверяем DETAILED TAB - in-article-carousel-super
     */
    public boolean checkDetailedInArticleCarousel() {
        boolean result = true;
        widgetHelper.switchToDetailedTab();

        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            result = checkDomainShow() & (!isDomainShow || checkDomainSettingsForCarousel());
        }

        return result &
                checkHeadlineSettingsForInArticleCarousel() &
                checkTeaserCardShadow(teaserCardShadow) &
                checkFixedWidthImageForMobile(teaserFixedWidth, teaserWidth) &
                checkCarouselSettings("margin-right") &
                checkWidgetTitleSettings();
    }

    //////////////////////////////////////// DETAILED BLOCK - FINISH ///////////////////////////////////////

    /**
     * удаляем виджет из дешборда (список виджетов)
     */
    public boolean deleteWidget() {
        return widgetHelper.deleteWidget(widgetId);
    }

    /**
     * Проверяем "WIDGET" settings
     */
    public boolean checkWidgetSettings() {
        return widgetHelper.checkBackgroundColor(backgroundColor) &&
                widgetHelper.checkWidgetBorder(widgetBorder) &&
                widgetHelper.checkBorderColor(borderColor) &&
                widgetHelper.checkTeaserBorderWidth(teaserBorderWidth) &&
                widgetHelper.checkTeaserBorderColor(teaserBorderColor);
    }

    /**
     * Проверяем "WIDGET" settings
     */
    public boolean checkWidgetSettingsInConstructor() {
        log.info("checkWidgetSettingsInConstructor");
        return widgetHelper.checkBackgroundColorInConstructor(backgroundColor) &
                widgetHelper.checkWidgetBorderInConstructor(widgetBorder) &
                widgetHelper.checkBorderColorInConstructor(borderColor) &
                widgetHelper.checkTeaserBorderWidthInConstructor(teaserBorderWidth)/* &
                widgetHelper.checkTeaserBorderColorInConstructor(teaserBorderColor)*/;
    }

    /**
     * Проверяем "IMAGE" settings
     */
    public boolean checkImageSettings() {
        return widgetHelper.checkImageFormat(imageFormat) &
                widgetHelper.checkImageBorder(imageBorder) &
                widgetHelper.checkImageBorderColor(imageBorderColor) &
                widgetHelper.checkImagePadding(imagePadding);
    }

    /**
     * Проверяем "IMAGE" settings в конструкторе
     */
    public boolean checkImageSettingsInConstructor() {
        return widgetHelper.checkImageFormat(imageFormat) &
                widgetHelper.checkImageBorderInConstructor(imageBorder, "yes") &
                        isShowBlockTeaserIcon();
    }

    public boolean checkImageSettingsInConstructorForUnderArticle() {
        return widgetHelper.checkImageFormat(imageFormat) &
                widgetHelper.checkImageBorderInConstructor(imageBorder) &
                widgetHelper.checkImageBorderColorInConstructor(imageBorderColor) &
                widgetHelper.checkImagePaddingInConstructor(imagePadding);
    }

    /**
     * Проверяем "IMAGE" settings headline-in-picture
     */
    public boolean checkImageSettingsForHeadline() {
        return widgetHelper.checkImageFormat(imageFormat) &
                widgetHelper.checkImageBorderForHeadline(imageBorder) &
                widgetHelper.checkImageBorderColorForHeadline(imageBorderColor);
    }

    public void switchAutoScroll(String action) {
        autoScroll = !action.equals("create");
        widgetHelper.switchAutoScroll(autoScroll);
    }

    /**
     * проверяем "IMAGE ZOOM ON HOVER"
     */
    public boolean checkImageZoomOnHover(boolean isZoom) {
        return widgetHelper.checkImageZoom(isZoom);
    }

    /**
     * проверяем "IMAGE INNER SHADOW"
     */
    public boolean checkImageInnerShadow(boolean isInnerShadow) {
        return widgetHelper.checkImageInnerShadow(isInnerShadow);
    }

    /**
     * проверяем "IMAGE INNER SHADOW"
     */
    public boolean checkImageInnerShadowInConstructor(boolean isInnerShadow) {
        return widgetHelper.checkImageInnerShadowInConstructor(isInnerShadow);
    }

    /**
     * проверяем "IMAGE ZOOM ON HOVER"
     */
    public boolean checkImageZoomOnHoverInConstructor(boolean isZoom) {
        return widgetHelper.checkImageZoomInConstructor(isZoom);
    }

    public void fillHeadlineSettingsForInArticleCarousel() {
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
    }

    /**
     * Проверяем "HEADLINE" settings в инпутах и в конструкторе
     */
    public boolean checkHeadlineSettings() {
        return widgetHelper.checkHeadlineFontSize(headlineFontSize) &&
                widgetHelper.checkHeadlineFontColor(headlineFontColor) &&
                widgetHelper.checkHeadlinePosition(headlinePosition) &&
                widgetHelper.checkHeadlineStyleText(headlineStyleText);
    }

    /**
     * Проверяем "HEADLINE" settings в инпутах и в конструкторе
     */
    public boolean checkHeadlineSettingsForTextOnImage() {
        return widgetHelper.checkHeadlineFontSize(headlineFontSize);
    }

    /**
     * Проверяем "HEADLINE" settings в инпутах и в конструкторе - Carousel widget
     */
    public boolean checkHeadlineSettingsForCarousel() {
        return widgetHelper.checkHeadlineSizeCarouselInStand(headlineFontSize) &
                widgetHelper.checkHeadlinePosition(headlinePosition) &
                widgetHelper.checkHeadlineStyleText(headlineStyleText);
    }

    /**
     * Проверяем "IN-ARTICLE" settings в инпутах и в конструкторе - Carousel widget
     */
    public boolean checkHeadlineSettingsForInArticleCarousel() {
        return widgetHelper.checkHeadlineTextTransformInConstructor(headlineTextTransform) &
                widgetHelper.checkHeadlineSizeCarouselInStand(headlineFontSize) &
                widgetHelper.checkHeadlinePosition(headlinePosition) &
                widgetHelper.checkHeadlineStyleText(headlineStyleText);
    }

    /**
     * Проверяем настройки "HEADLINE" settings в конструкторе
     */
    public boolean checkHeadlineSettingsInConstructor() {
        return widgetHelper.checkHeadlineTextFontInConstructor(headlineTextFont) &
                widgetHelper.checkHeadlineFontSizeInConstructor(headlineFontSize) &
                widgetHelper.checkHeadlineFontColorInConstructor(headlineFontColor) &
                //widgetHelper.checkHeadlinePositionInConstructor(headlinePosition) &
                widgetHelper.checkHeadlineStyleTextInConstructor(headlineStyleText);
    }

    /**
     * Проверяем настройки "HEADLINE" settings в конструкторе
     */
    public boolean checkHeadlineSettingsInConstructorWithoutTextFont() {
        return widgetHelper.checkHeadlineFontSizeInConstructor(headlineFontSize) &
                widgetHelper.checkHeadlineFontColorInConstructor(headlineFontColor) &
                widgetHelper.checkHeadlinePositionInConstructor(headlinePosition) &
                widgetHelper.checkHeadlineStyleTextInConstructor(headlineStyleText);
    }

    /**
     * Проверяем настройки "HEADLINE" settings в конструкторе for Text-On-Image
     */
    public boolean checkHeadlineSettingsTextOnImageInConstructor() {
        return widgetHelper.checkHeadlineTextFontInConstructor(headlineTextFont) &&
                widgetHelper.checkHeadlineFontSizeInConstructor(headlineFontSize) &&
                widgetHelper.checkHeadlinePositionInConstructor(headlinePosition) &&
                widgetHelper.checkHeadlineStyleTextInConstructor(headlineStyleText);
    }

    /**
     * Проверяем настройки "HEADLINE" settings в конструкторе for Text-On-Image
     */
    public boolean checkHeadlineSettingsCarouselInConstructor() {
        return widgetHelper.checkHeadlineTextFontInConstructor(headlineTextFont) &
                widgetHelper.checkHeadlinePositionInConstructor(headlinePosition) &
                widgetHelper.checkHeadlineStyleTextInConstructor(headlineStyleText);
    }

    /**
     * проверяем "WIDGET TITLE" settings
     */
    public boolean checkWidgetTitleSettings() {
        return widgetHelper.checkWidgetTitleTextFont(widgetTitleTextFont) &&
                widgetHelper.checkWidgetTitleFontSize(widgetTitleFontSize) &&
                widgetHelper.checkWidgetTitleFontColor(widgetTitleFontColor);
    }

    /**
     * проверяем "WIDGET TITLE" settings в конструкторе
     */
    public boolean checkWidgetTitleSettingsInConstructor() {
        return widgetHelper.checkWidgetTitleTextTransformInConstructor(widgetTitleTextTransform) &&
                widgetHelper.checkWidgetTitleTextFontInConstructor(widgetTitleTextFont) &&
                widgetHelper.checkWidgetTitleFontSizeInConstructor(widgetTitleFontSize) &&
                widgetHelper.checkWidgetTitleFontColorInConstructor(widgetTitleFontColor);
    }

    /**
     * Заполняем "DOMAIN" settings
     */
    public void fillDomainSettingsForCarousel() {
        domainFontSize = widgetHelper.chooseDomainFontSize();
    }

    /**
     * Проверяем "DOMAIN" settings
     */
    public boolean checkDomainSettings() {
        return widgetHelper.checkDomainTextFont(domainTextFont) &
                widgetHelper.checkDomainFontSize(domainFontSize) &
                widgetHelper.checkDomainFontColor(domainFontColor) &
                widgetHelper.checkDomainPosition(domainPosition) &
                widgetHelper.checkDomainStyleText(domainStyleText);
    }

    /**
     * Проверяем "DOMAIN" settings for 'carousel-super'
     */
    public boolean checkDomainSettingsForCarousel() {
        return widgetHelper.checkDomainTextFont(domainTextFont) &
                widgetHelper.checkDomainFontSize(domainFontSize) &
                widgetHelper.checkDomainPosition(domainPosition) &
                widgetHelper.checkDomainStyleText(domainStyleText);
    }

    /**
     * Проверяем "DOMAIN" settings
     */
    public boolean checkDomainSettingsForTextOnImage() {
        return widgetHelper.checkDomainTextFont(domainTextFont) &
                widgetHelper.checkDomainFontSize(domainFontSize) &
                widgetHelper.checkDomainPosition(domainPosition) &
                widgetHelper.checkDomainStyleText(domainStyleText);
    }

    /**
     * Проверяем "DOMAIN" settings
     */
    public boolean checkDomainSettingsInConstructor(String... dopStyle) {
        return widgetHelper.checkDomainTextFontInConstructor(domainTextFont) &
                widgetHelper.checkDomainFontSizeInConstructor(domainFontSize) &
                widgetHelper.checkDomainFontColorInConstructor(domainFontColor) &
                widgetHelper.checkDomainPositionInConstructor(domainPosition, dopStyle) &
                widgetHelper.checkDomainStyleTextInConstructor(domainStyleText);
    }

    /**
     * Проверяем "DOMAIN" settings for Text-On-Image
     */
    public boolean checkDomainSettingsTextOnImageInConstructor(String... dopStyle) {
        return widgetHelper.checkDomainTextFontInConstructor(domainTextFont) &
                widgetHelper.checkDomainFontSizeInConstructor(domainFontSize) &
                widgetHelper.checkDomainPositionInConstructor(domainPosition, dopStyle) &
                widgetHelper.checkDomainStyleTextInConstructor(domainStyleText);
    }

    public void switchDomainShow() {
        widgetHelper.switchDomainShow(isDomainShow);
    }

    public boolean checkDomainShow() {
        return widgetHelper.checkDomainShow(isDomainShow);
    }

    /**
     * проверяем "TEASER CARD SHADOW"
     */
    public boolean checkTeaserCardShadow(boolean isTeaserCardShadow) {
        log.info("checkTeaserCardShadow -> start");
        return widgetHelper.checkTeaserCardShadow(isTeaserCardShadow);
    }

    /**
     * проверяем "TEASER CARD SHADOW" in constructor
     */
    public boolean checkTeaserCardShadowInConstructor(boolean isTeaserCardShadow) {
        return widgetHelper.checkTeaserCardShadowInConstructor(isTeaserCardShadow);
    }

    /**
     * проверяем "TEASER CARD SHADOW"
     */
    public boolean checkFixedWidthImageForMobile(boolean isTeaserFixedWidth, String teaserWidth) {
        return widgetHelper.checkTeaserFixedWidth(isTeaserFixedWidth, teaserWidth);
    }

    /**
     * Проверяем additional "Carousel" settings
     */
    public boolean checkCarouselSettings(String margin_side) {
        return widgetHelper.checkTeaserMargin(teaserMargin, margin_side) &
                widgetHelper.checkTeaserBorderRadius(teaserBorderRadius) &
                widgetHelper.checkTeaserWidth(teaserWidth, margin_side) &
                widgetHelper.checkTeaserHeight(teaserHeight);
    }

    /**
     * Проверяем additional "Carousel" settings in constructor
     */
    public boolean checkCarouselSettingsInConstructor() {
        return widgetHelper.checkTeaserMarginInConstructor(teaserMargin, "margin-right") &
                widgetHelper.checkTeaserBorderRadiusInConstructor(teaserBorderRadius) &
                widgetHelper.checkTeaserWidthInConstructor(teaserWidth, "margin-right") &
                widgetHelper.checkTeaserHeightInConstructor(teaserHeight);
    }

    /**
     * Проверяем additional "Carousel" settings in constructor
     */
    public boolean checkCarouselVerticalSettingsInConstructor() {
        return widgetHelper.checkTeaserMarginInConstructor(teaserMargin, "margin-bottom") &
                widgetHelper.checkTeaserBorderRadiusInConstructor(teaserBorderRadius) &
                widgetHelper.checkTeaserWidthInConstructor(teaserWidth, "margin-bottom") &
                widgetHelper.checkTeaserHeightInConstructor(teaserHeight);
    }

    /**
     * Проверяем "POSITION"
     */
    public boolean checkPositionSettings() {
        return widgetHelper.checkPositionTeasers(position);
    }

    /**
     * Проверяем "POSITION"
     */
    public boolean checkPositionSettingsInConstructor() {
        return widgetHelper.checkPositionTeasersInConstructor(position);
    }

    /**
     * метод создания простого виджета "under-article-widget"
     */
    public int createSimpleWidget() {
        waitWidgetConstructorReady();
        widgetHelper.chooseTypeWidget(WidgetTypes.Types.UNDER_ARTICLE);
        widgetHelper.clickCreateAndClosePopUp();
        widgetId = widgetHelper.getWidgetIdFromUrl();
        return widgetId;
    }

    /**
     * переходим на конкретный виджет и пересохраняем виджет + изменяем WidgetTitle
     */
    public boolean reSaveWithEditWidget() {
        waitWidgetConstructorReady();
        widgetHelper.chooseWidgetTitle();
        return widgetHelper.clickCreateAndClosePopUp();
    }

    public boolean createButtonIsEnabled() {
        return widgetHelper.createButtonIsEnabled();
    }

    /**
     * Включает или отключает в ДБ один из adTypes
     */
    public void switchContentTypes() {
        log.info("вкл/выкл один рандомный НЕ ЗАДИЗЕЙБЛЕННЫЙ тумблер 'CONTENT TYPES'");
        WIDGET_SAVE_BUTTON.shouldBe(visible, ofSeconds(8));
        ElementsCollection listTypes = $$(CONTENT_TYPES_VISIBLE_LIST);
        listTypes.get(randomNumbersInt(listTypes.size())).click();

        log.info("записываем в мапку все типы и их состояние");
        contentTypes = widgetHelper.setContentTypesInMap();

        log.info("сохраняем виджет (может появится поп-ап)");
        widgetHelper.clickButtonCreateWidget();

        widgetId = widgetHelper.getWidgetIdFromUrl();
        log.info("switchContentTypes - finish");
    }

    /**
     * переходим в ДБ и проверяем состояние тумблеров "CONTENT TYPES"
     */
    public boolean checkStateContentTypesInDash() {
        WIDGET_TITLE_INPUT.shouldBe(visible).scrollTo();
        if (!contentTypes.get("pg") & !contentTypes.get("pg13")) {
            log.info("contentTypes don't have pg or pg13 -> " + $$("[id*=adType_] div").size());
            return helpersInit.getBaseHelper().checkDataset($$("[id*=adType_] div").size(), 0);
        }

        log.info("checkStateContentTypesInDash contentTypes.get(pg)");
        waitWidgetConstructorReady();
        return widgetHelper.checkContentTypesState(contentTypes);
    }

    public void switchOnOffAllContentTypes(boolean isSwitch) {
        waitWidgetConstructorReady();
        String valTumblers = isSwitch ? "on" : "off";
        widgetHelper.switchOnOffAllContentTypes(valTumblers);
    }

    public void chooseRandomContentTypes(int countTumblersNeedSwitch) {
        ElementsCollection listTumblers = $$(CONTENT_TYPES_VISIBLE_LIST);
        ArrayList<Integer> mass = helpersInit.getBaseHelper().getRandomCountValue(listTumblers.size(), countTumblersNeedSwitch);
        mass.forEach(i -> listTumblers.get(i).click());
        setContentTypesInMap();
    }

    public void setContentTypesInMap() {
        waitWidgetConstructorReady();
        contentTypes = widgetHelper.setContentTypesInMap();
    }

    public boolean checkContentTypesState() {
        waitWidgetConstructorReady();
        return widgetHelper.checkContentTypesState(contentTypes);
    }

    public boolean isDisplayedAdType(String adType) {
        return widgetHelper.isDisplayedAdType(adType);
    }

    /**
     * проверяем работу кнопки "ADD ANOTHER WIDGET"
     */
    public boolean checkButtonAddAnotherWidget() {
        createSimpleWidget();
        sleep(1000);
        ADD_ANOTHER_WIDGET_BUTTON.shouldBe(visible).click();
        checkErrors();
        waitWidgetConstructorReady();
        return !ADD_ANOTHER_WIDGET_BUTTON.isDisplayed() &&
                helpersInit.getBaseHelper().checkStateOfDisplayed(WIDGET_TYPE_SELECT.parent().parent(), false);
    }

    public boolean isCloneWidgetIconDisplayed(int widgetId){ return widgetListHelper.isCloneWidgetIconDisplayed(widgetId); }

    /**
     * пересохраняем виджет и получаем изменённые стили и параметры
     */
    public Widget changeDataAndSaveWidget(Types type, SubTypes subType) {
        if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)){
            switch (type) {
                case UNDER_ARTICLE, HEADER, SIDEBAR -> {
                    switch (subType) {
                        case UNDER_ARTICLE_LIGHT, UNDER_ARTICLE_DARK, HEADER_LIGHT, HEADER_DARK, SIDEBAR_LIGHT, SIDEBAR_DARK -> reSaveUnderArticleWidgetAndGetDataAdskeeper();
                        case UNDER_ARTICLE_CARD_MEDIA -> chooseWidgetTitle();
                    }
                }
                case MOBILE_EXIT, EXIT_POP_UP -> reSavePopUpWidgetAndGetData();
                case IN_SITE_NOTIFICATION -> reSaveInSiteNotification();
                case IN_ARTICLE -> chooseWidgetTitle();
                case FEED -> reSaveSmart();
            }
        }
        
        else {
            switch (type) {
                case UNDER_ARTICLE, SIDEBAR, HEADER -> {
                    switch (subType) {
                        case UNDER_ARTICLE_CARDS, UNDER_ARTICLE_PAIR_CARDS, UNDER_ARTICLE_LIGHT_CARDS,
                                UNDER_ARTICLE_RECTANGULAR, UNDER_ARTICLE_SQUARE, UNDER_ARTICLE_SQUARE_BLACK, UNDER_ARTICLE_MAIN,
                                HEADER_RECTANGULAR, HEADER_SQUARE,
                                SIDEBAR_CARDS, SIDEBAR_RECTANGULAR, SIDEBAR_RECTANGULAR_2, SIDEBAR_SQUARE_SMALL -> reSaveUnderArticleWidgetAndGetData();
                        case SIDEBAR_IMPACT, SIDEBAR_FRAME -> reSaveSidebarImpact();
                        case SIDEBAR_BLUR -> reSaveSidebarBlur();
                    }
                }
                case IN_ARTICLE -> {
                    switch (subType) {
                        case IN_ARTICLE_MAIN -> reSaveInArticleMainWidgetAndGetData();
                        case IN_ARTICLE_CAROUSEL -> reSaveCarouselWidgetAndGetData();
                        case IN_ARTICLE_IMPACT, IN_ARTICLE_DOUBLE_PICTURE -> reSaveInArticleBrandformanceWidgetAndGetData();
                    }
                }
                case IN_SITE_NOTIFICATION -> reSaveInSiteNotification();
                case CAROUSEL -> {
                    switch (subType) {
                        case CAROUSEL_MAIN -> reSaveIoCarouselWidgetAndGetData();
                        case CAROUSEL_VERTICAL -> reSaveCarouselVerticalWidgetAndGetData();
                    }
                }
                case HEADLINE -> reSaveHeadlineInPictureWidgetAndGetData();
                case TEXT_ON_IMAGE -> reSaveTextOnImageWidgetAndGetData();
                case BANNER -> {
                    if (subType == SubTypes.BANNER_300x250_4) {
                        reSaveBannerWithButtonWidgetAndGetData();
                    } else {
                        reSaveBannerWidgetAndGetData();
                    }
                }
                case SMART -> reSaveSmart();
                case PASSAGE -> reSavePassage();
            }
        }

        widgetHelper.clickCreateAndClosePopUp();
        return this;
    }

    /**
     * проверяем на стенде с виджетом
     * корректное отображение виджета и приминение стилей и параметров
     */
    public boolean checkWidgetInStand(Types type, SubTypes subType) {
        boolean result = false;
        checkErrors();
        if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)){
             switch (type) {
                case UNDER_ARTICLE, HEADER, SIDEBAR -> {
                    switch (subType) {
                        case UNDER_ARTICLE_LIGHT, UNDER_ARTICLE_DARK, HEADER_LIGHT, HEADER_DARK, SIDEBAR_LIGHT, SIDEBAR_DARK -> result = checkUnderArticleWidgetInStandAdskeeper();
                        case UNDER_ARTICLE_CARD_MEDIA -> result = widgetHelper.checkWidgetTitleInConstructor(widgetTitle) & isShowBlockTeaserIcon();
                    }
                }
                case MOBILE_EXIT -> result = checkMobileExitWidgetInStand();
                case EXIT_POP_UP -> result = checkPopUpWidgetInStand();
                case IN_SITE_NOTIFICATION -> result = widgetHelper.checkLabelTumblerInConstructor(labelTumbler) & isShowBlockTeaserIcon();
                case IN_ARTICLE -> {
                    if (subType == SubTypes.IN_ARTICLE_CAROUSEL) {
                        result = checkCarouselInStand();
                    } else {
                        result = widgetHelper.checkWidgetTitleInConstructor(widgetTitle) & isShowBlockTeaserIcon();
                    }
                }
                 case FEED -> result = checkSmartInStand();
            }
        }

        else {
            switch (type) {
                case UNDER_ARTICLE, SIDEBAR, HEADER -> {
                    switch (subType) {
                        case UNDER_ARTICLE_CARDS, UNDER_ARTICLE_LIGHT_CARDS,
                                UNDER_ARTICLE_RECTANGULAR, UNDER_ARTICLE_SQUARE, UNDER_ARTICLE_SQUARE_BLACK,
                                HEADER_RECTANGULAR, HEADER_SQUARE,
                                SIDEBAR_CARDS, SIDEBAR_RECTANGULAR, SIDEBAR_RECTANGULAR_2, SIDEBAR_SQUARE_SMALL -> result = checkUnderArticleWidgetInStand();
                        case UNDER_ARTICLE_MAIN, UNDER_ARTICLE_PAIR_CARDS -> result = checkUnderArticleMainWidgetInStand();
                        case SIDEBAR_IMPACT, SIDEBAR_FRAME -> result = checkSidebarImpactWidgetInStand();
                        case SIDEBAR_BLUR -> result = checkSidebarBlurWidgetInStand();
                    }
                }
                case IN_ARTICLE -> {
                    switch (subType) {
                        case IN_ARTICLE_MAIN -> result = checkInArticleMainWidgetInStand();
                        case IN_ARTICLE_CAROUSEL -> result =  checkCarouselInStand();
                    }
                }
                case CAROUSEL -> {
                    switch (subType) {
                        case CAROUSEL_MAIN -> result = checkIoCarouselInStand();
                        case CAROUSEL_VERTICAL -> result = checkCarouselVerticalInStand();
                    }
                }
                case IN_SITE_NOTIFICATION -> result = isShowBlockTeaserIcon();
                case HEADLINE -> result = checkHeadLineWidgetInStand();
                case TEXT_ON_IMAGE -> result = checkTextOnImageWidgetInStand();
                case BANNER -> {
                    if (subType == SubTypes.BANNER_300x250_4) {
                        result = checkBannerWithButtonWidgetInStand();
                    } else {
                        result = checkBannerWidgetInStand();
                    }
                }
                case SMART -> result = checkSmartInStand();
            }
        }
        
        return result;
    }

    /**
     * for pop-up widget
     */
    public void mouseOutEvent(){
        executeJavaScript("" +
                "var event = document.createEvent('MouseEvent'); " +
                "event.initEvent('mouseout', true, true); " +
                "document.dispatchEvent(event);"
        );
    }

    public void activatePageOnStand() {
        STAND_NAV.click();
    }

    public void clickNav2OnStand() {
        STAND_NAV_2.click();
    }

    /**
     * изменяем параметры виджета для Under Article Widget
     */
    public void reSaveUnderArticleWidgetAndGetData() {
        //general
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();

        //detailed
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        headlineFontColor = widgetHelper.chooseHeadlineFontColor();

        //domain
        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
            domainFontColor = widgetHelper.chooseDomainFontColor();
        }
    }

    public void reSaveInArticleMainWidgetAndGetData() {
        //general
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);

        //detailed
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        headlineFontColor = widgetHelper.chooseHeadlineFontColor();

        //domain
        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
            domainFontColor = widgetHelper.chooseDomainFontColor();
        }
    }

    /**
     * изменяем параметры виджета для Under Article Widget Adskeeper
     */
    public void reSaveUnderArticleWidgetAndGetDataAdskeeper() {
        //general
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();

        //detailed
        widgetHelper.switchToDetailedTab();
        //headline
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        headlineFontColor = widgetHelper.chooseHeadlineFontColor();
    }

    /**
     * изменяем параметры виджета для Under Article Widget
     */
    public void reSaveCarouselWidgetAndGetData() {
        //general
        columns = widgetHelper.chooseColumnsForCarousel();
        widgetTitle = widgetHelper.chooseWidgetTitle();

        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
            autoScroll = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
            widgetHelper.switchAutoScroll(autoScroll);
        }

        //detailed
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();

        if(!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
            log.info("isDomainShow: " + isDomainShow);
            switchDomainShow();
            if(isDomainShow){
                fillDomainSettingsForCarousel();
            }
        }
    }

    /**
     * изменяем параметры виджета для Under Article Widget
     */
    public void reSaveIoCarouselWidgetAndGetData() {
        //general
        columns = widgetHelper.chooseColumnsForCarousel();
        widgetTitle = widgetHelper.chooseWidgetTitle();


        autoScroll = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        widgetHelper.switchAutoScroll(autoScroll);


        //detailed
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();

        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        log.info("isDomainShow: " + isDomainShow);
        switchDomainShow();
        if(isDomainShow){
            fillDomainSettingsForCarousel();
        }

    }

    /**
     * изменяем параметры виджета для carousel-vertical
     */
    public void reSaveCarouselVerticalWidgetAndGetData() {
        //general
        columns = widgetHelper.chooseColumnsForCarousel();

        //detailed
        widgetHelper.switchToDetailedTab();
        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
        }
    }

    /**
     * изменяем параметры виджета для In-Article subType:brandformance
     */
    public Widget reSaveInArticleBrandformanceWidgetAndGetData() {
        //general
        autoplacement = widgetHelper.chooseAutoplacement(autoplacement);
        return this;
    }

    /**
     * изменяем параметры виджета для Headline In Picture
     */
    public void reSaveHeadlineInPictureWidgetAndGetData() {
        //general
        widgetTheme = widgetHelper.chooseTheme();
        widgetTitle = widgetHelper.chooseWidgetTitle();

        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();

        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
            domainFontColor = widgetHelper.chooseDomainFontColor();
        }
    }

    /**
     * изменяем параметры виджета для Text-on-image
     */
    public void reSaveTextOnImageWidgetAndGetData() {
        //general
        widgetTitle = widgetHelper.chooseWidgetTitle();

        //detailed
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        isDomainShow = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        switchDomainShow();
        if(isDomainShow){
            domainFontSize = widgetHelper.chooseDomainFontSize();
        }
    }

    /**
     * изменяем параметры виджета для Banner widget
     */
    public void reSaveBannerWidgetAndGetData() {
        //general
        if(subType != SubTypes.BANNER_470x325) subType = widgetHelper.chooseSubTypeForBanner();

        if (subType != SubTypes.BANNER_470x325 & subType != SubTypes.BANNER_970x250) widgetTheme = widgetHelper.chooseTheme();
    }

    /**
     * изменяем параметры виджета для Banner widget with BUTTON
     */
    public void reSaveBannerWithButtonWidgetAndGetData() {
        // color button
        showBannerButton = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        log.info("showBannerButton: " + showBannerButton);
        widgetHelper.switchBannerButtonTumbler(showBannerButton);
        if (showBannerButton) colorButtonBanner = widgetHelper.chooseColorButtonBanner();

        // button effect
        bannerButtonEffect = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        log.info("bannerButtonEffect: " + bannerButtonEffect);
        if (showBannerButton) widgetHelper.switchBannerButtonEffectTumbler(bannerButtonEffect);

        // blur
        typeBlurBanner = widgetHelper.chooseTypeBannerBlur();
        if (typeBlurBanner.equals("custom")) colorBlurBanner = widgetHelper.chooseColorBlurBanner();
    }

    /**
     * изменяем параметры виджета для PopUp widget
     */
    public void reSavePopUpWidgetAndGetData() {
        //general
        popUpTitle = widgetHelper.setPopUpTitle();
        widgetTheme = widgetHelper.chooseTheme();
    }

    /**
     * изменяем параметры виджета для PopUp widget
     */
    public void reSaveInSiteNotification() {
        //general
        rows = widgetHelper.chooseRows(1);
        position = widgetHelper.choosePositionInSiteNotification();

        if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
            labelTumbler = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
            log.info("labelTumbler: " + labelTumbler);
            widgetHelper.switchLabelTumbler(labelTumbler);
        }

        switchRerunAdsTumbler(rerunAds);
    }

    public Widget switchRerunAdsTumbler(boolean rerunAds){
        widgetHelper.switchRerunAdsTumbler(rerunAds);
        return this;
    }

    public void chooseWidgetTitle() {
        widgetTitle = widgetHelper.chooseWidgetTitle();
    }

    public void reSaveMobileWidget(MobileWidgetType... mobileType) {
        //general
        widgetTitle = widgetHelper.chooseWidgetTitle();

        //switch off tumblers
        chooseMobileWidgetType(mobileType);
    }

    public Widget chooseMobileWidgetType(MobileWidgetType... mobileType) {
        //switch off tumblers
        widgetHelper.switchToasterTumbler(false);
        widgetHelper.switchDragDownTumbler(false);
        widgetHelper.switchInterstitialTumbler(false);

        for (MobileWidgetType m : mobileType) {
            switch (m) {
                case TOASTER -> {
                    widgetHelper.switchToasterTumbler(true);
                    widgetHelper.setFrequencyCappingImpressions(frequencyCappingImpressions);
                    widgetHelper.setFrequencyCappingMinutes(frequencyCappingMinutes);
                    if(subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {
                        widgetHelper.setToasterInactivityTime(toasterInactivityTime);
                    }
                }
                case DRAGDOWN -> widgetHelper.switchDragDownTumbler(true);
                case INTERSTITIAL -> {
                    widgetHelper.switchInterstitialTumbler(true);
                    widgetHelper.setShowAfterInteraction(showAfterInteraction);
                }
            }
        }
        return this;
    }

    public Widget reSaveSmart(){
        widgetTitle = widgetHelper.chooseWidgetTitle();
        widgetHelper.switchInfiniteScrollTumbler(isInfiniteScroll);
        return this;
    }

    public Widget switchInfiniteScrollTumbler(boolean state){
        isInfiniteScroll = state;
        widgetHelper.switchInfiniteScrollTumbler(isInfiniteScroll);
        return this;
    }

    public void reSavePassage(){
        widgetTitle = widgetHelper.chooseWidgetTitle();
        widgetHelper.chooseImpressionsPassage(impressionEveryPassage);
    }

    public void reSaveSidebarImpact() {
        //general
        widgetTitle = widgetHelper.chooseWidgetTitle();
        rows        = widgetHelper.chooseRows();
    }

    public void reSaveSidebarBlur() {
        //general
        widgetTitle = widgetHelper.chooseWidgetTitle();
        rows        = widgetHelper.chooseRows();

        //detailed
        widgetHelper.switchToDetailedTab();
        headlineFontSize = widgetHelper.chooseHeadlineFontSize();
        domainFontSize = widgetHelper.chooseDomainFontSize();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Headline In Picture
     */
    public boolean checkHeadLineWidgetInStand() {
        return widgetHelper.checkThemeInConstructor(subType, widgetTheme) &&
                widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &&
                checkWidgetSettingsInConstructor() &&
                checkImageSettingsInConstructor() &&
                checkImageInnerShadowInConstructor(innerShadow) &&
                checkWidgetTitleSettingsInConstructor() &
                        widgetHelper.checkHeadlineFontSizeInConstructor(headlineFontSize) &
                        (!isDomainShow ||
                                checkDomainSettingsInConstructor());
    }

    public boolean checkSidebarImpactWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                isShowBlockTeaserIcon();
    }

    public boolean checkSidebarBlurWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                widgetHelper.checkHeadlineFontSizeInConstructor(headlineFontSize) &
                widgetHelper.checkDomainFontSizeInConstructor(domainFontSize) &
                isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Text-On-Image
     */
    public boolean checkTextOnImageWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetSettingsInConstructor() &
                checkHeadlineSettingsTextOnImageInConstructor() &
                checkWidgetTitleSettingsInConstructor() &
                (!isDomainShow || checkDomainSettingsTextOnImageInConstructor()) &
                checkImageSettingsInConstructor() &
                checkImageInnerShadowInConstructor(innerShadow);
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Banner Widget
     */
    public boolean checkBannerWidgetInStand() {
        if (subType == SubTypes.BANNER_470x325) return true;

        return (subType == SubTypes.BANNER_970x250 || widgetHelper.checkThemeInConstructor(subType, widgetTheme)) &
                widgetHelper.checkMgBoxSize(subType) &
                isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Banner Widget with BUTTON
     */
    public boolean checkBannerWithButtonWidgetInStand() {
        return widgetHelper.checkColorButtonBannerInConstructor(showBannerButton, colorButtonBanner) &&
                widgetHelper.checkBannerButtonEffectInConstructor(showBannerButton, bannerButtonEffect) &
                widgetHelper.checkBlurBannerInConstructor(typeBlurBanner, colorBlurBanner) &&
                widgetHelper.checkMgBoxSize(subType) &
                        isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Pop Up Widget
     */
    public boolean checkPopUpWidgetInStand() {
        STAND_SECTION_ID.shouldBe(visible);
        return widgetHelper.checkThemeInConstructor(subType, widgetTheme) & isShowBlockTeaserIcon();

    }

    public boolean checkSmartInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle);
    }

    public boolean checkWidgetTitleInConstructor() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle);
    }

    public boolean checkMobileExitWidgetInStand() {
        return widgetHelper.checkPopUpTitleInStand(popUpTitle) &
                widgetHelper.checkThemeForMobileExitInConstructor(widgetTheme);
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Under Article
     */
    public boolean checkUnderArticleWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetSettingsInConstructor() &
                checkImageSettingsInConstructorForUnderArticle() &
                checkImageInnerShadowInConstructor(innerShadow) &
                checkImageZoomOnHoverInConstructor(zoomOnHover) &
                checkHeadlineSettingsInConstructor() &
                checkWidgetTitleSettingsInConstructor() &
                (!isDomainShow || checkDomainSettingsInConstructor()) &
                checkPositionSettingsInConstructor() &
                isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Under Article
     */
    public boolean checkUnderArticleMainWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetSettingsInConstructor() &
                checkImageSettingsInConstructorForUnderArticle() &
                checkImageInnerShadowInConstructor(innerShadow) &
                checkImageZoomOnHoverInConstructor(zoomOnHover) &
                checkHeadlineSettingsInConstructor() &
                checkWidgetTitleSettingsInConstructor() &
                (!isDomainShow || checkDomainSettingsInConstructor("dopStyle")) &
                checkPositionSettingsInConstructor() &
                isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Under Article
     */
    public boolean checkUnderArticleForViewInCab() {
        return checkWidgetSettingsInConstructor() &
                checkImageSettingsInConstructorForUnderArticle() &
                checkImageInnerShadowInConstructor(innerShadow) &
                checkImageZoomOnHoverInConstructor(zoomOnHover) &
                checkHeadlineSettingsInConstructor() &
                checkWidgetTitleSettingsInConstructor() &
                (!isDomainShow || checkDomainSettingsInConstructor()) &
                checkPositionSettingsInConstructor();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Under Article Adskeeper
     */
    public boolean checkUnderArticleWidgetInStandAdskeeper() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetSettingsInConstructor() &
                checkImageSettingsInConstructorForUnderArticle() &
                checkHeadlineSettingsInConstructor() &
                checkWidgetTitleSettingsInConstructor() &
                checkPositionSettingsInConstructor() &
                isShowBlockTeaserIcon();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для In-Article
     */
    public boolean checkInArticleMainWidgetInStand() {
        return widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetSettingsInConstructor() &
                checkImageSettingsInConstructorForUnderArticle() &
                checkImageInnerShadowInConstructor(innerShadow) &
                checkImageZoomOnHoverInConstructor(zoomOnHover) &
                checkHeadlineSettingsInConstructorWithoutTextFont() &
                checkWidgetTitleSettingsInConstructor() &
                isShowBlockTeaserIcon() &
                checkAutoplacementOnStand();
    }

    public boolean checkAutoplacementOnStand(){

        if(autoplacement.equals("amp")){

            switchTo().defaultContent();
            boolean state = $("#off_ipsum+amp-embed>iframe").exists();
            switchToAmpFrame();
            return state;
        }

        else {

            if ($("#" + autoplacement + "_ipsum+div, #" + autoplacement + "_ipsum>div, #" + autoplacement + "_ipsum+amp-embed>iframe").exists()) {
                String id = helpersInit.getBaseHelper().getTextAndWriteLog($("#" + autoplacement + "_ipsum+div, #" + autoplacement + "_ipsum>div").attr("id"));

                return helpersInit.getBaseHelper().getTextAndWriteLog(id.contains("ScriptRootC"));
            }
        }
        return false;
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Carousel-super
     */
    public boolean checkCarouselInStand() {
        boolean result = true;

        if (!subnetId.equals(SubnetType.SCENARIO_ADSKEEPER)) {

            if(!isAmp) {
                $("#" + autoplacement + "_ipsum+div, #" + autoplacement + "_ipsum>div")
                        .shouldBe(visible)
                        .scrollIntoView(true);
            }

            result = widgetHelper.checkAutoScrollInStand(autoScroll) &
                    checkAutoplacementOnStand() &
                    (!isDomainShow || checkDomainSettingsTextOnImageInConstructor("dopStyle"));
        }

        return result &
                widgetHelper.checkColumnsInStand(columns) &
                widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetTitleSettingsInConstructor() &
                checkHeadlineSettingsCarouselInConstructor() &
                checkTeaserCardShadowInConstructor(teaserCardShadow) &
                checkCarouselSettingsInConstructor();
    }

    public boolean checkIoCarouselInStand() {
        boolean result;

        if(!isAmp) STAND_P_1.shouldBe(visible).scrollTo();

        result = widgetHelper.checkAutoScrollInStand(autoScroll) &
                (!isDomainShow || checkDomainSettingsTextOnImageInConstructor("dopStyle"));


        return result &
                widgetHelper.checkColumnsInStand(columns) &
                widgetHelper.checkWidgetTitleInConstructor(widgetTitle) &
                checkWidgetTitleSettingsInConstructor() &
                checkHeadlineSettingsCarouselInConstructor() &
                checkTeaserCardShadowInConstructor(teaserCardShadow) &
                checkCarouselSettingsInConstructor();
    }

    /**
     * проверяем на стенде применение сохранённых стилей и параметров для Carousel-super-vertical
     */
    public boolean checkCarouselVerticalInStand() {
        return widgetHelper.checkColumnsInStand(columns) &
                checkDomainSettingsTextOnImageInConstructor("dopStyle") &
                checkCarouselVerticalSettingsInConstructor();
    }

    /**
     * get css value for MGLBTN impact
     */
    public String getBrandMglBtnStyle(String style){ return widgetHelper.getBrandMglBtnStyle(style); }

    public String getIdealMglBtnStyle(String style){ return widgetHelper.getIdealMglBtnStyle(style); }

    public String getBrandMglBtnText() {
        return widgetHelper.getBrandMglBtnText();
    }

    public String getIdealMglBtnText(){ return widgetHelper.getIdealMglBtnText(); }

    public boolean checkMctitleLink(String startLinkValue){ return widgetHelper.checkMctitleLink(startLinkValue); }

    /**
     * get css value for title
     */
    public String getTitleStyle(String style) {
        return widgetHelper.getTitleStyle(style);
    }

    public String getMgHeadStyle(String style) {
        return widgetHelper.getMgHeadStyle(style);
    }

    public String getTextElementsStyle(String cssStyle){
        return widgetHelper.getElementIfHasShadowDom(TEXT_ELEMENTS_STYLE).getCssValue(cssStyle);
    }

    public String getTitleText(){ return widgetHelper.getTitleText(); }

    public String getMcimgStyle(String cssStyle) {
        return widgetHelper.getMcimgStyle(cssStyle);
    }

    public String getDescriptionStyle(String style) {
        return widgetHelper.getDescriptionStyle(style);
    }

    public String getDescriptionText(){ return widgetHelper.getDescriptionText(); }

    /**
     * get custom MgBox style
     */
    public String getMgBoxStyle(String style) {
        return widgetHelper.getMgBoxStyle(style);
    }

    /**
     * get css value for domain
     */
    public String getDomainStyle(String style) {
        return widgetHelper.getDomainStyle(style);
    }

    /**
     * Save widget settings
     */
    public Widget saveWidgetSettings() {
        widgetHelper.clickButtonCreateWidget();
        return this;
    }

    /**
     * click work icon close
     */
    public void clickCloseIconInSiteNotification() {
        widgetHelper.clickCloseIconInSiteNotification();
    }
    public void clickCloseIconPassage(){ widgetHelper.clickCloseIconPassage(); }

    public boolean checkWorkLikeIcon(){
        SelenideElement likeIcon = widgetHelper.getElementIfHasShadowDom(LIKE_VALUE);
        int likeSize = helpersInit.getBaseHelper().parseInt(likeIcon.text());
        likeIcon.click();
        return helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().parseInt(likeIcon.text()), (likeSize+1));
    }

    /**
     * check disable edit class
     */
    public boolean checkDisableEditClass() {
        return widgetHelper.checkDisableEditClass();
    }

    public void chooseAllPlacementWidgetByIntExchange() {
        widgetHelper.chooseAllPlacementWidgetByIntExchange();
    }

    public void switchIntExchangeTumbler(boolean state) {
        widgetHelper.switchIntExchangeTumbler(state);
        waitWidgetConstructorReady();
    }

    public void switchDirectPublisherDemandTumbler(boolean state) {
        widgetHelper.switchDirectPublisherDemandTumbler(state);
        waitWidgetConstructorReady();
    }

    public void switchMonetizationTumbler(boolean state) { widgetHelper.switchMonetizationTumbler(state); }

    public void chooseRandomPlacementByIntExchange(int countPlacement) {
        waitWidgetConstructorReady();
        intExchangePlacementItems = widgetHelper.chooseRandomPlacement(POSITION_BLOCK, countPlacement);
    }

    public void chooseRandomPlacementByDemand(int countPlacement) {
        waitWidgetConstructorReady();
        directDemandPlacementItems = widgetHelper.chooseRandomPlacement(POSITION_BLOCK_DIRECT_DEMAND, countPlacement);
    }

    public boolean checkPlacementByIntExchange() {
        return widgetHelper.checkPlacement(POSITION_BLOCK, intExchangePlacementItems, "#24377b");
    }

    public boolean checkPlacementByDemand() {
        return widgetHelper.checkPlacement(POSITION_BLOCK_DIRECT_DEMAND, directDemandPlacementItems, "#FFB46F");
    }

    public ArrayList<String> getDisabledPlacement(){
        ArrayList<String> list = new ArrayList<>();
        waitWidgetConstructorReady();
        for(SelenideElement s : POSITION_BLOCK.shouldBe(visible).$$("div[data-position-id][class$='has-tooltip'][style*='background-color: rgb(228, 228, 228);']")){
            list.add(s.attr("data-position-id"));
        }
        return list;
    }

    public ArrayList<String> getDisabledDemandPlacement(){
        ArrayList<String> list = new ArrayList<>();
        waitWidgetConstructorReady();
        for(SelenideElement s : POSITION_BLOCK_DIRECT_DEMAND.shouldBe(visible).$$("div[data-position-id][class$='has-tooltip'][style*='background-color: rgb(228, 228, 228);']")){
            list.add(s.attr("data-position-id"));
        }
        return list;
    }

    /**
     * check state 'int exchange' tumbler for mobile/banner type
     */
    public boolean checkStateIntExchangeTumbler(boolean state) {
        waitWidgetConstructorReady();
        return widgetHelper.checkIntExchangeTumbler(state);
    }

    public boolean isPositionBlockDisplayed(){ sleep(1000); return POSITION_BLOCK.isDisplayed(); }

    public List getStopWords(){
        waitWidgetConstructorReady();
        return widgetHelper.getStopWords();
    }

    public void setStopWords(ArrayList<String> listOfStopWords) {
        widgetHelper.setStopWords(listOfStopWords);
    }

    public boolean isDisplayedStopWordsError(){ return widgetHelper.isDisplayedStopWordsError(); }

    public void deleteStopWords(int... countWords){
        waitWidgetConstructorReady();
        widgetHelper.deleteStopWords(countWords);
    }

    /**
     * Just click Save button
     */
    public void clickSaveWidgetButton() {
        widgetHelper.clickSaveWidgetButton();
    }

    /**
     * add clone widget
     */
    public void addAbTestWidget(int widgetId) {
        cloneId = widgetListHelper.addAbTestWidget(widgetId);
    }

    /**
     * check clone widget attributes at popup
     */
    public boolean checkCloneAttributesAtPopup() {
        return widgetListHelper.isPresentCloneName() &&
                widgetListHelper.checkCloneParentMarker();
    }

    /**
     * check clone widget attributes
     */
    public boolean checkCloneAttributesAtWidgetList(int widgetId) {
        helpersInit.getBaseHelper().closePopup();
        widgetListHelper.openParentTree(widgetId);
        return widgetListHelper.isCloneWidgetIconDisplayed(widgetId);
    }

    /**
     * delete clone
     */
    public void deleteClonedWidget() {
        widgetListHelper.deleteClonedWidget(String.valueOf(cloneId));
    }

    /**
     * check deleted clone
     */
    public boolean checkDeletedClone(int widgetId) {
        return widgetListHelper.checkDeletedClone(widgetId, String.valueOf(cloneId));
    }

    /**
     * check that clone was created
     */
    public boolean checkCreatedClone(int widgetId, String nameOfClone) {
        return widgetListHelper.checkCreatedClone(widgetId, cloneId, nameOfClone);
    }

    /**
     * Проверяем работу custom_styles - Блокируются/Не блокируются часть полей
     */
    public boolean checkManuallyStyles(String errorMessage, WidgetTypes.Types widgetType, boolean stateElement) {
        waitWidgetConstructorReady();
        boolean flag = switch (widgetType) {
            case UNDER_ARTICLE -> helpersInit.getBaseHelper().checkStateOfDisplayed(SUB_TYPE_SELECT.parent().parent(), stateElement) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(MOBILE_COLUMNS_SELECT.parent().parent(), stateElement) &
                    helpersInit.getBaseHelper().checkStateOfExists(ROWS_HIDDEN, stateElement) &
                    helpersInit.getBaseHelper().checkStateOfExists(COLS_HIDDEN, stateElement) &
                    helpersInit.getBaseHelper().checkStateOfExists(THEME_HIDDEN, stateElement) &
                    (stateElement != DETAILED_TAB.isDisplayed()) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(WIDGET_TYPE_SELECT.parent().parent(), stateElement);
            case EXIT_POP_UP -> helpersInit.getBaseHelper().checkStateOfExists(THEME_HIDDEN, stateElement) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(POPUP_CAPPING_INPUT, false) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(POPUP_TITLE_INPUT, false);
            case MOBILE -> helpersInit.getBaseHelper().checkStateOfDisplayed(TOASTER_TUMBLER, false) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(DRAG_DOWN_TUMBLER, false) &
                    helpersInit.getBaseHelper().checkStateOfDisplayed(INTERSTITIAL_TUMBLER, false);
            default -> false;
        };

        return flag &&
                helpersInit.getBaseHelper().checkStateOfDisplayed(WIDGET_TITLE_INPUT, false) &
                helpersInit.getBaseHelper().checkStateOfDisplayed(WEBSITE_SELECT.parent().parent(), true) &
                (stateElement ?
                        helpersInit.getMessageHelper().checkMessageDash(errorMessage) :
                        (!NOTIFICATION.exists() ||
                                !NOTIFICATION.text().equalsIgnoreCase(errorMessage)));
    }

    public void fillDataSubWidget(String widgetId){
        widgetListHelper.clickEditRules(widgetId);
        widgetListHelper.clickAddNewSubwidget();
        log.info("clickAddNewSubwidget");

        // fill rules
        titleRule = widgetListHelper.fillRuleTitle("ruleTitle" + helpersInit.getBaseHelper().randomNumbersString(3));
        countryRule = widgetListHelper.fillRuleCountry();
        trafficTypeRule = widgetListHelper.fillRuleTrafficTypes("Direct");
        widgetListHelper.saveRule();
    }

    /**
     * create rule widget
     */
    public void createSubwidget(String widgetId) {
        int counter = 0;
        do {
            fillDataSubWidget(widgetId);
            counter++;

            if(counter > 1) {
                helpersInit.getMessageHelper().getMessageDash();
                sleep(5000);
                refresh();
            }
        }
        while (counter < 5 && helpersInit.getMessageHelper().isEmergencyModePresentDash());

        cloneId = widgetListHelper.getSubwidgetId(titleRule);

    }

    /**
     * create AB test from Subwidget
     */
    public void createAbTestFromSubwidget(String widgetId, String subWidgetId) {
        int counter = 0, countChild;
        String parentLocator = "tr[data-parent-id='" + subWidgetId + "']";
        do {
            widgetListHelper.clickEditRules(widgetId);
            $("tr[data-parent-id='" + widgetId + "']").shouldBe(visible, ofSeconds(8));
            countChild = helpersInit.getBaseHelper().getTextAndWriteLog($$(parentLocator).size());
            widgetListHelper.clickAbTestFromSubWidget(subWidgetId);
            waitForAjaxVisible();

            counter++;
        }
        while (counter < 5 && helpersInit.getMessageHelper().isEmergencyModePresentDash());

        log.info("break loop");

        $$(parentLocator).shouldHave(CollectionCondition.size( countChild + 1), ofSeconds(20));
        log.info("wait AbTestFromSubwidget");
        cloneId = widgetListHelper.getAbTestIdFromSubwidget(subWidgetId);

    }

    /**
     * check rule settings
     */
    public boolean checkCreatedRule(int subWidgetId) {
        return widgetListHelper.checkSettingsRule(subWidgetId, titleRule, countryRule, trafficTypeRule);
    }

    public boolean checkSettingsAbTestFromSubwidget(int abTestForSubwidgetId, String countryRule, String trafficRule) {
        return widgetListHelper.checkSettingsAbTestFromSubwidget(abTestForSubwidgetId, countryRule, trafficRule);
    }

    /**
     * Check AMP widgets code presence in Dashboard
     */
    public boolean checkAMPCodeAfterCreatingWidget(String subnetName, String webSiteName, int siteId) {
        return widgetHelper.checkAMPCodeForWidgetsInDashboard(widgetId, webSiteName, siteId, subnetName);
    }

    /**
     * Check AMP code for widget
     */
    public boolean checkAMPCodeInWidgetsList(int widgetId, int siteId, String siteName, String subnetName) {
        widgetListHelper.openWidgetsPopupWithCode(widgetId);
        return widgetListHelper.checkAMPCodeInWidgetsList(widgetId, siteName, siteId, subnetName);
    }

    /**
     * Check AMP widgets code and instruction for managers in additional places in Cab
     */
    public boolean checkAMPCodeInAdditionalPlaces(String subnetName, int widgetID, String siteName, int siteId) {
        log.info("Check AMP widgets code presence");
        return helpersInit.getBaseHelper().checkDatasetEquals(
                "<amp-embed width=\"600\" height=\"600\" layout=\"responsive\" type=\"" + subnetName.toLowerCase() + "\" data-publisher=\"" + siteName
                        + "\" data-widget=\"" + widgetID + "\" data-container=\"M" + siteId + "ScriptRootC" + widgetID + "\" data-block-on-consent=\"_till_responded\" > </amp-embed>",
                WIDGET_CODE_FOR_CLIENT.shouldBe(visible).text());
    }

    public boolean checkShortCodeWithClicktracking(int widgetId, String siteName, int siteId, String macros){
        widgetListHelper.openWidgetsPopupWithCode(widgetId);
        return widgetListHelper.checkShortCodeWithClicktracking(widgetId, siteName, siteId, macros);
    }

    public boolean checkShortCode(int widgetId, String siteName, int siteId){
        widgetListHelper.openWidgetsPopupWithCode(widgetId);
        return widgetListHelper.checkShortCode(widgetId, siteName, siteId);
    }

    public boolean isShowAmpCode(){ return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_CODE_FOR_CLIENT.isDisplayed()); }

    public boolean isShowInstantArticlesCode() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_INSTANT_ARTICLES_CODE.isDisplayed());
    }

    public void clickTeaserOnStand(int teaserIndexNumber){ widgetHelper.clickTeaserOnStand(teaserIndexNumber); }

    public void clickMgbox(){widgetHelper.getElementIfHasShadowDom(MGBOX_STYLE).click();}

    public void scrollToMgbox(){widgetHelper.getElementIfHasShadowDom(MGBOX_STYLE).scrollTo();}

    public String getVideoPosition(int widgetId){
        return executeJavaScript(
                "return _mgLib" + videoLibVersion.replace(".", "_") + ".widgets[0].amby.get().video_" + widgetId + "_0.player.position;").toString();
    }

    public boolean isShowDoubleClickForm(int teaserIndexNumber){ return widgetHelper.isShowDoubleClickForm(teaserIndexNumber); }

    public String getDoublePicture_mgButton_Svg(String cssStyle){
        return widgetHelper.getElementIfHasShadowDom(MGBUTTON_SVG_STYLE).getCssValue(cssStyle); }

    public String getDoublePicture_mgButton(String cssStyle) {
        return widgetHelper.getElementIfHasShadowDom(MGBUTTON_STYLE).getCssValue(cssStyle);
    }

    public String getDoublePicture_mcimgInner(String cssStyle) {
        return widgetHelper.getElementIfHasShadowDom(MCIMG_INNER_STYLE).getCssValue(cssStyle);
    }

    public String getBannerLogoImg(){ return widgetHelper.getElementIfHasShadowDom(STAND_BANNER_LOGO).attr("src"); }

    public void clickNextPageInPaginatorOnStand(){ STAND_PAGINATOR_NEXT_BUTTON.click(); }

    public boolean isShowBannerCampaignName(){ return widgetHelper.getElementIfHasShadowDom(STAND_BANNER_SHOW_NAME_LABEL).isDisplayed(); }

    public String getBannerWidgetTitle(){ return widgetHelper.getElementIfHasShadowDom(STAND_BANNER_WIDGET_TITLE_LABEL).text(); }

    public void hoverToHeaderStand(){
        STAND_HEADER.hover();
        checkErrors();
    }

    public void hoverToFooterStand(){
        log.info("hoverToFooterStand");
        STAND_FOOTER.hover();
        log.info("hoverToFooterStand -> hover");
        checkErrors();
        sleep(1000);
    }

    public void hoverToTopStand(){
        $(".text").shouldBe(exist).hover();
        checkErrors();
        sleep(1000);
    }

    public void clickBackButtonBannerIcon(){
        STAND_BACK_BUTTON_CLOSE_BANNER_ICON.shouldBe(visible).click();
    }

    /**
     * @param indexTeaser - default index = 1
     */
    public boolean isShowBlockTeaserOnStand(int... indexTeaser){
        SelenideElement teaser = $x(".//*[contains(@class, 'mgbox')]//*[contains(@class, 'mgline')][" + (indexTeaser.length > 0 ? indexTeaser[0] : 1) + "]");
        teaser.shouldBe(visible).hover();
        SelenideElement blockIcon = teaser.$(".close-informer");
        linkOnBlockTeaser = Objects.requireNonNull(blockIcon.attr("href")).replace("https://", "http://local-");
        blockedTeaserTitle = teaser.$(".mctitle>a").innerText();
        if(blockedTeaserTitle.contains(" ...")) blockedTeaserTitle = blockedTeaserTitle.substring(0, blockedTeaserTitle.length()-4);
        return true;
    }

    public boolean isShowBlockTeaserIcon(){
        SelenideElement firstTeaser = widgetHelper.getElementIfHasShadowDom(FIRST_TEASER_IN_CONSTRUCTOR);

        firstTeaser.scrollIntoView(true).hover();
        return helpersInit.getBaseHelper().getTextAndWriteLog(firstTeaser.$(".close-informer").isDisplayed());
    }

    public boolean isShowMgBox(){ return widgetHelper.getElementIfHasShadowDom(MGBOX_STYLE).exists(); }

    public String getDomainTopStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCDOMAIN_TOP_A_STYLE).getCssValue(style); }

    public String getDomainSmartStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCDOMAIN_SMART_STYLE).getCssValue(style); }

    public String getDomainNowStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCDOMAIN_NOW_STYLE).getCssValue(style); }

    public String getDomainIconStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCDOMAIN_ICON_STYLE).getCssValue(style); }

    public String getButtomMediaStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MGBUTTOM_MEDIA_STYLE).getCssValue(style); }

    public String getMcMoreStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCMORE_STYLE).getCssValue(style); }

    public String getMcMoreLinkStyle(String style){
        return widgetHelper.getElementIfHasShadowDom(MCMORE_LINK_STYLE).getCssValue(style); }

    public String getLogoMgidLinkInStand(){
        return widgetHelper.getElementIfHasShadowDom(STAND_MGID_LOGO).attr("src"); }

    public String getLogoIdealmediaIoLinkInStand(){
        return widgetHelper.getElementIfHasShadowDom(STAND_IDEALMEDIA_LOGO).attr("href");
    }

    public boolean checkBlurEffectOnMouseHover(){
        SelenideElement mglineHover = widgetHelper.getElementIfHasShadowDom(MGLINE_HOVER_STYLE);
        if(!mglineHover.exists()) widgetHelper.getElementIfHasShadowDom(MGLINE_STYLE).hover();

        return mglineHover.getCssValue("opacity").equals("0.5");
    }

    public boolean checkSidebarFrameHover(){
        SelenideElement mgline = widgetHelper.getElementIfHasShadowDom(MGLINE_STYLE);
        SelenideElement theme = widgetHelper.getElementIfHasShadowDom(THEME_STYLE);
        SelenideElement domainHover = widgetHelper.getElementIfHasShadowDom(DOMAIN_HOVER_STYLE);
        if(!domainHover.exists()) mgline.hover();
        log.info("checkSidebarFrameHover, theme getColor: " + convertRgbaToHex(theme.getCssValue("color")));
        log.info("checkSidebarFrameHover, domainHover getColor: " + convertRgbaToHex(domainHover.getCssValue("color")));
        return helpersInit.getBaseHelper().getTextAndWriteLog(convertRgbaToHex(theme.getCssValue("color")).matches("#fff|#fefefe|#fafafa|#fdfdfd")) &
                helpersInit.getBaseHelper().checkDatasetContains(theme.getCssValue("text-decoration"), "none") &
                helpersInit.getBaseHelper().getTextAndWriteLog(convertRgbaToHex(domainHover.getCssValue("color")).matches("#fff|#fefefe|#fafafa|#fdfdfd")) &
                helpersInit.getBaseHelper().checkDatasetContains(domainHover.getCssValue("text-decoration"), "none");
    }

    /**
     * Choose widget type
     */
    public void chooseWidgetType(WidgetTypes.Types widgetType) {
        waitWidgetConstructorReady();
        widgetHelper.chooseTypeWidget(widgetType);
    }

    public boolean videoTumblerIsDisplayed() {
        WIDGET_SAVE_BUTTON.shouldBe(visible);
        return videoHelper.videoTumblerIsDisplayed();
    }

    public Integer createVideoWidget(VideoFormat formatValue, boolean enableDesktop, boolean enableMobile) {
        switchVideoTumbler(true);
        enableVideoContentCheckboxes(enableDesktop, enableMobile);
        chooseVideoFormat(formatValue);
        if(!subnetId.equals(SubnetType.SCENARIO_IDEALMEDIA_IO)) switchNativeBackfillTumbler(nativeBackfill);

        if(formatValue != VideoFormat.OUTSTREAM){
            choosePlacementByVideo();
            videoGroupsList = videoHelper.enableVideoGroups();
        }
        widgetHelper.clickCreateAndClosePopUp();
        widgetId = widgetHelper.getWidgetIdFromUrl();
        return widgetId;
    }

    public void editVideoWidget(VideoFormat formatValue, boolean enableDesktop, boolean enableMobile) {
        videoGroupsList.clear();
        enableVideoContentCheckboxes(enableDesktop, enableMobile);
        chooseVideoFormat(formatValue);
        if(!subnetId.equals(SubnetType.SCENARIO_IDEALMEDIA_IO)) switchNativeBackfillTumbler(nativeBackfill);

        if(formatValue != VideoFormat.OUTSTREAM){
            choosePlacementByVideo();
            videoGroupsList = videoHelper.enableVideoGroups();
        }
        widgetHelper.clickCreateAndClosePopUp();
    }

    public boolean checkVideoWidget(VideoFormat formatValue, boolean enableDesktop, boolean enableMobile){
        return videoHelper.checkSwitchToasterTumbler(true) &
                videoHelper.checkVideoContentCheckboxes(enableDesktop, enableMobile) &
                videoHelper.checkVideoFormat(formatValue) &
                //todo https://jira.mgid.com/browse/VID-4480 (ticket from Vasilyev)(subnetId.equals(SubnetType.SCENARIO_IDEALMEDIA_IO) || videoHelper.checkNativeBackfillTumbler(nativeBackfill)) &
                ((formatValue == VideoFormat.OUTSTREAM) || (checkPlacementByVideo() & videoHelper.checkVideoGroups(videoGroupsList)));
    }

    public boolean checkPlacementByVideo() {return videoHelper.checkPlacementByVideo(List.of(videoPlacementItems));}

    public void switchVideoTumbler(boolean state){ videoHelper.switchVideoTumbler(state); }

    public void chooseVideoFormat(VideoFormat formatValue) { videoHelper.chooseVideoFormat(formatValue); }

    public void enableVideoContentCheckboxes(boolean enableDesktop, boolean enableMobile){
        videoHelper.enableVideoContentCheckboxes(enableDesktop, enableMobile);
    }

    public void choosePlacementByVideo() {
        videoPlacementItems = videoHelper.choosePlacementByVideo();
    }

    public void switchNativeBackfillTumbler(boolean nativeBackfill){ videoHelper.switchNativeBackfillTumbler(nativeBackfill); }

    public boolean checkShowAllChildStatusWidget(int parentId, Map<String, String> data){
        widgetListHelper.openParentTree(parentId);
        $$(CHILD_WIDGETS_TR).shouldBe(CollectionCondition.sizeGreaterThan(1), ofSeconds(8));
        return $(".child-widget-row .info-column [data-id='" + data.keySet().toArray()[0] + "']").attr("class")
                .equals(data.values().toArray()[0]) &&
                $(".child-widget-row .info-column [data-id='" + data.keySet().toArray()[1] + "']").attr("class")
                        .equals(data.values().toArray()[1]);
    }

    /**
     * @param status: all / 1(ll but deleted)
     */
    public boolean checkWidgetStatusFilter(String status, LinkedHashMap<String, String> data){
        int k = 0;

        widgetListHelper.chooseWidgetStatusFilter(status);
        ElementsCollection list = $$(PARENT_WIDGETS_TR);

        for(Map.Entry<String, String> entry : data.entrySet()){
            SelenideElement new_el = list.get(k).$(".info-column [data-id]");
            if(!Objects.requireNonNull(new_el.attr("data-id")).equals(entry.getKey()) &
                    !Objects.requireNonNull(new_el.attr("class")).equals(entry.getValue())){
                return false;
            }
            k++;
        }

        return true;
    }

    public boolean isShowDefaultJsBlock(){ return widgetHelper.isShowDefaultJsBlock(); }

    public int getCountDefaultJsBlock(){ return widgetHelper.getCountDefaultJsBlock(); }

    public boolean isShowDefaultJsBlockChild(){ return widgetHelper.isShowDefaultJsBlockChild(); }

    public int getCountDefaultJsBlockChild(){ return widgetHelper.getCountDefaultJsBlockChild(); }

    public boolean checkCountTeasers(int teasersCount){
        widgetHelper.getElementsIfHasShadowDom(TEASERS_SIZE).shouldHave(CollectionCondition.size(teasersCount));
        return true;
    }

    public boolean checkSmartImageStyle(int row, String width, String height, int... child){
        ElementsCollection elements = child.length > 0 ?
                $$(".row" + row + " .mgline:nth-child(" + child[0] + ") img[class=mcimg]") :
                $$(".row" + row + " div.mgline[class] img.mcimg");

        for(SelenideElement s : elements){
            if(!helpersInit.getBaseHelper().checkDatasetContains(s.getCssValue("width"), width) &&
                    !helpersInit.getBaseHelper().checkDatasetContains(s.getCssValue("height"), height)){
                return false;
            }
        }

        return true;

    }

    public boolean checkTextElementStyle(int row, String style, String value){
        for(SelenideElement s : $$(".row" + row + " div.mgline[class] .text-elements")){
            if(!helpersInit.getBaseHelper().checkDatasetEquals(s.getCssValue(style), value+"px")){
                return false;
            }
        }
        return true;
    }

    /**
     * Get disclaimer text
     */
    public String getDisclaimerText(String teaserNumber) {
        return $x(String.format(MGLINE_TEASER_INDEX_NUMBER, teaserNumber) + TEASER_DISCLAIMER_DIV).scrollTo().text();
    }

    public String getTitleText(int teaserIndexNumber){
        return widgetHelper.getElementIfHasShadowDom(String.format(CURRENT_TEASER_IN_CONSTRUCTOR, teaserIndexNumber + 1)).$(".mctitle [href]").innerText();
    }

    public Widget switchToBannerFrame(int widgetId, int pageNumber, int placeNumber) {
        SelenideElement bannerIframe = $(String.format(TEASER_CUSTOM_BANNER_FRAME, widgetId, pageNumber, placeNumber));
        bannerIframe.shouldBe(visible);
        switchTo().frame(bannerIframe);
        return this;
    }

    public boolean isDisplayedCustomBanner() {
        return TEASER_CUSTOM_BANNER_DIV.shouldBe(visible).isDisplayed();
    }

    public void waitShowToasterWidget(){
        STAND_FOOTER.shouldBe(visible).scrollIntoView(true);
        STAND_HEADER.scrollIntoView(true);
        SelenideElement button = isShadowDom ?
                $(shadowCss(MOBILE_CLOSE_ICON, "#section + div")) :
                $(MOBILE_CLOSE_ICON);
        button.shouldBe(visible, ofSeconds(6));
    }

    public void scrollActionForToasterWidget(){
        STAND_FOOTER.shouldBe(visible).scrollIntoView(true);
        sleep(7000);
        STAND_HEADER.scrollIntoView(true);
    }
    
    public void waitShowDragdownWidget(){
        STAND_FOOTER.shouldBe(visible).scrollIntoView(true);
        STAND_HEADER.shouldBe(visible).scrollIntoView(false);
    }

    public boolean isShowMobileCloseIcon(){ return widgetHelper.getElementIfHasShadowDom(MOBILE_CLOSE_ICON).isDisplayed(); }

    public void clickMobileCloseIcon(){  widgetHelper.getElementIfHasShadowDom(MOBILE_CLOSE_ICON).click(); }

    public boolean isShowMobileContainer(){
        sleep(1000);
        return widgetHelper.getElementIfHasShadowDom(MOBILE_WIDGET_CONTAINER + " .mgbox").isDisplayed();
    }

    public void waitMobileContainer(){
        widgetHelper.getElementIfHasShadowDom(MOBILE_WIDGET_CONTAINER + " .mgbox").shouldBe(visible);
        sleep(1000);
    }

    public String getAdMobileContainerAttribute(String style){ return widgetHelper.getElementIfHasShadowDom(MOBILE_WIDGET_CONTAINER).attr(style); }

    public void clickMobileContinueButton(){ widgetHelper.getElementIfHasShadowDom(MOBILE_CONTINUE_BUTTON).click(); }

    public boolean isShowMobileContinueButton(){ return widgetHelper.getElementIfHasShadowDom(MOBILE_CONTINUE_BUTTON).isDisplayed(); }

    public String getMobileContinueButtonText(){ return widgetHelper.getElementIfHasShadowDom(MOBILE_CONTINUE_BUTTON).text(); }

    public String getMobileContinueButtonStyle(String style){ return widgetHelper.getElementIfHasShadowDom(MOBILE_CONTINUE_BUTTON).getCssValue(style); }

    public void clickCustomLinkInterstitial(int numberLink){ $("#id_" + numberLink).click(); }

    public void chooseWidgetContractFilter(String type) {
        APPLY_FILTER_BUTTON.scrollTo().shouldBe(visible);
        sleep(1000);
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(WIDGET_CONTRACT_TYPE_FILTER, type);
        APPLY_FILTER_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    /**
     * Click on show child icon
     */
    public void showWidgetChild(int widgetId) {
        $("a[data-parent-id='" + widgetId + "']").shouldBe(visible).scrollTo();
        $("a[data-parent-id='" + widgetId + "']").hover().click();
        int counter = 0;
        while (!$(".child-widget-row[data-id='" + widgetId + "']>td>div>span").isDisplayed()) {
            $("a[data-parent-id='" + widgetId + "']").hover().click();
            sleep(1000);
            counter++;
            if (counter > 2) {
                break;
            }
        }
    }

    /**
     * Check that contracts from filtered widgets are matched with contract filter type
     *
     */
    public boolean isContractTypeFromFilteredWidgetEqualsToSelectedType(int widgetId, String contractType, String widgetType) {
        String widgetContractType = $("." + widgetType.toLowerCase() + "-widget-row[data-id='" + widgetId + "']>td>div>b").shouldBe(visible).text();
        String[] mass = contractType.split(" ");
        if (mass.length > 1) {
            return widgetContractType.contains(mass[0]) &
                    widgetContractType.contains(mass[1]);
        }
        return widgetContractType.equalsIgnoreCase(contractType);
    }

    public int getNumberOfDisplayedParentWidgets() {
        return WIDGETS_PARENT_LIST.size();
    }

    public int getNumberOfDisplayedChildWidgets() {
        return WIDGETS_CHILD_LIST.size();
    }

    public boolean isAdOrLandingTypesSelectedForWidget(List adOrLandingTypeList){
        int counter = 0;
        for(Object element : adOrLandingTypeList){
            if(element != null)
                counter++;
        }
        return counter > 0;
    }

    public int getCountLinksInMcimg(){ return widgetHelper.getCountLinksInMcimg(); }

    public void waitWidgetConstructorReady(){
        sleep(500);
        executeJavaScript(
                "function sleep(ms) { " +
                        "return new Promise(resolve => setTimeout(resolve, ms));" +
                        "}" +
                        "async function wait(){ " +
                        "for(var i = 0; i < 100; i++){" +
                        "if(typeof window.widgetRebuild !== 'function'){" +
                        "await sleep(100);" +
                        "}" +
                        "else break;" +
                        "}" +
                        "}");
        sleep(500);
    }
}
