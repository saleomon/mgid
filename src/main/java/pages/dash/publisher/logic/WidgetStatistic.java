package pages.dash.publisher.logic;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import libs.clickhouse.queries.PublisherStatistics;
import libs.clickhouse.queries.WidgetStatisticsSumm;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.PublisherStatHelper;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;

import java.util.ArrayList;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.PublisherLocators.*;

public class WidgetStatistic {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final PublisherStatHelper publisherStatHelper;
    private ArrayList<String> startEndDate;
    PublisherStatistics publisherStatistics;
    WidgetStatisticsSumm widgetStatisticsSumm;

    //fill list in Graph
    ArrayList<Double> massWages, massCPC, massRcpm, massEcpm;
    ArrayList<Long> massClicks, massAdRequests, massImpressions;

    //total get in clickHouse
    String totalDbWages, totalDbClicks, totalDbGoodsClicks, totalDbInternalClicks, totalDbAdRequests, totalDbImpressions;

    //custom reports
    ElementsCollection wagesTableElements, impressionsTableElements, eCpmTableElements, adRequestTableElements, clicksTableElements, soldClicksElements,
            cpcTableElements, rCpmTableElements, visibilityRateTableElements, ctrTableElements;
    String customReportName;

    //statistics by Day
    SelenideElement AD_REQUEST_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, CLICKS_TOTAL_BY_DAY_LABEL, WAGES_TOTAL_BY_DAY_LABEL,
            E_CPM_TOTAL_BY_DAY_LABEL, CPC_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, VISIBILITY_RATE_TOTAL_BY_DAY_LABEL,
            SOLD_CLICKS_TOTAL_BY_DAY_LABEL;

    SelenideElement PAGES_VIEW_TABLE_TOTAL_LABEL, IMPRESSIONS_TABLE_TOTAL_LABEL, CLICKS_TABLE_TOTAL_LABEL,
            WAGES_TABLE_TOTAL_LABEL, E_CPM_TABLE_TOTAL_WIDGETS_LABEL, CPC_TABLE_TOTAL_LABEL, CTR_TABLE_TOTAL_LABEL, R_CPM_TABLE_TOTAL_WIDGETS_LABEL, VISIBILITY_RATE_TABLE_TOTAL_LABEL;


    public WidgetStatistic(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        publisherStatHelper = new PublisherStatHelper(log, helpersInit);
        publisherStatistics = new PublisherStatistics();
        widgetStatisticsSumm = new WidgetStatisticsSumm();
    }

    //custom report
    public void getDataWagesFromGraph() {
        massWages = publisherStatHelper.getJsQuery("wages");
    }

    public void getDataClicksFromGraph() {
        massClicks = publisherStatHelper.getJsQuery("clicks");
    }

    public void getDataAdRequestsFromGraph() {
        massAdRequests = publisherStatHelper.getJsQuery("adRequests");
    }

    public void getDataImpressionsFromGraph() {
        massImpressions = publisherStatHelper.getJsQuery("impressions");
    }

    public void getDataCpcFromGraph() {
        massCPC = publisherStatHelper.getJsQuery("cpc");
    }

    public void getDataRcpmFromGraph() {
        massRcpm = publisherStatHelper.getJsQuery("rCpm");
    }

    public void getDataEcpmInGraph() {
        massEcpm = publisherStatHelper.getJsQuery("eCpm");
    }

    public String getCustomReportName() {
        return customReportName;
    }

    /**
     * get total data for custom db fields from clickHouse
     * dataType - site = ?/composite = ?/""
     */
    public void getTotalDataFromPublisherStatisticsClickHouse(int tickers_composite_id) {
        publisherStatistics.getSumForCustomFieldsFromClickHouse("composite = " + tickers_composite_id, startEndDate,
                "wages_publisher_currency",
                "goods_clicks_accepted",
                "int_exchange_clicks_accepted",
                "ad_requests",
                "impressions"
        );

        totalDbWages = publisherStatistics.getString(1);
        totalDbGoodsClicks = publisherStatistics.getString(2);
        totalDbInternalClicks = publisherStatistics.getString(3);
        totalDbAdRequests = publisherStatistics.getString(4);
        totalDbImpressions = publisherStatistics.getString(5);

        totalDbClicks = String.valueOf(helpersInit.getBaseHelper().parseInt(totalDbGoodsClicks) + helpersInit.getBaseHelper().parseInt(totalDbInternalClicks));
    }

    public void getTotalDataFromWidgetStatisticsSummAndIexchangeAndClickHouse(int widgetId) {
        widgetStatisticsSumm.getSumForCustomWidgetFieldsFromClickHouse(widgetId, startEndDate);

        totalDbAdRequests = widgetStatisticsSumm.getString(1);
        totalDbImpressions = widgetStatisticsSumm.getString(2);
        totalDbWages = widgetStatisticsSumm.getString(3);
        totalDbClicks = widgetStatisticsSumm.getString(4);
    }

    ///////////////////////////////////////////// CUSTOM REPORT - START ////////////////////////////////////////////////////////////////

    /**
     * Проставлем для отображения статистики CUSTOM REPORTS - метрики и показатели
     */
    public void customReport_choseRandomOptions(CalendarPeriods period, String[] type, String... metric) {
        startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);

        CUSTOM_REPORTS_OPTIONS_BUTTON.shouldBe(visible, ofSeconds(8));
        initGraph();

        log.info("Открываем настройки параметров репортов и выбираем все dimensions из аргумента type");
        publisherStatHelper.openOptionsForm();
        publisherStatHelper.selectMetrics("dimensions", type);

        log.info("включаем все metrics из аргумента metric");
        publisherStatHelper.selectMetrics("metrics", metric);

        log.info("Проверяем метрики и показатели в поп-апе 'Фильтры'");
        publisherStatHelper.checkAllOptionsInFilterForm(type, metric);

        log.info("Выбираем нужный период в календаре и фильтруем данные");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);

        log.info("Если в интерфейсе присутствует пагинатор страниц(1,2 NEXT), то выбираем в пагинаторе отображение по 100 параметров");
        loadDataIfPaginatorDisplayed();

        CUSTOM_REPORT_INFO_MESSAGE.shouldBe(exist);
    }

    /**
     * get custom data column by header(@sortby)
     */
    public void getDataColumnByHeader() {
        wagesTableElements = helpersInit.getSortedHelper().getTableColumnData("wages");
        impressionsTableElements = helpersInit.getSortedHelper().getTableColumnData("impressions");
        eCpmTableElements = helpersInit.getSortedHelper().getTableColumnData("e_cpm");
        adRequestTableElements = helpersInit.getSortedHelper().getTableColumnData("ad_requests");
        clicksTableElements = helpersInit.getSortedHelper().getTableColumnData("clicks");
        cpcTableElements = helpersInit.getSortedHelper().getTableColumnData("cpc");
        rCpmTableElements = helpersInit.getSortedHelper().getTableColumnData("r_cpm");
        visibilityRateTableElements = helpersInit.getSortedHelper().getTableColumnData("visibility_rate");
        ctrTableElements = helpersInit.getSortedHelper().getTableColumnData("ctr");
    }

    public void getTotalElementForStatBy() {
        PAGES_VIEW_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("ad_requests");
        IMPRESSIONS_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("impressions");
        CLICKS_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("clicks");
        WAGES_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("wages");
        CPC_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("cpc");
        CTR_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("ctr");
        E_CPM_TABLE_TOTAL_WIDGETS_LABEL = publisherStatHelper.getTotalBySort("e_cpm");
        R_CPM_TABLE_TOTAL_WIDGETS_LABEL = publisherStatHelper.getTotalBySort("r_cpm");
        VISIBILITY_RATE_TABLE_TOTAL_LABEL = publisherStatHelper.getTotalBySort("visibility_rate");
    }

    public boolean customReport_checkWorkFilterByDimensionWithSelect(int... customValue) {
        return publisherStatHelper.checkWorkFilterByDimensionWithSelect(customValue);
    }

    public boolean customReport_checkWorkFilterByDimensionWithInput() {
        return publisherStatHelper.checkWorkFilterByDimensionWithInput();
    }

    /**
     * Проверяем работу в поп-апе "Фильтры" работу сортировки метрик по сортировке ("equal", "greater_or_equal", "less_or_equal")
     * Проверяем метрики с целочисленными данными ("ad_requests", "impressions", "clicks")
     * Проверяем метрики для данных с плавающей точкой ("visibility_rate", "wages", "cpc", "e_cpm")
     */
    public boolean checkMinOrMaxValueSelect(String... value) {
        return publisherStatHelper.checkMinOrMaxValueSelect(value);
    }

    /**
     * проверяем что поле для фильтра по SubId не принимает невалтидные значения
     */
    public String customReport_getMessageForNotValidSubId() {
        return publisherStatHelper.getMessageForNotValidSubId();
    }

    public boolean customReport_checkShowAllSortedByHeaders(String... metric) {
        return publisherStatHelper.checkShowAllMetrics(metric);
    }

    /**
     * Проверяем расчёт CPC в графике
     * cpc = wages/clicks * 100
     */
    public boolean customReport_checkCpcInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massClicks, massCPC, 100);
    }

    /**
     * Проверяем расчёт CPM в графике
     * r_cpm = wages/adRequests * 1000
     */
    public boolean customReport_checkRcpmInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massAdRequests, massRcpm, 1000);
    }

    /**
     * Проверяем расчёт vCPM в таблице CUSTOM REPORTS под графиком - сравниваем построчно за каждый день
     * eCPM = wages/impressions * 1000
     */
    public boolean customReport_checkEcpmInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, impressionsTableElements, eCpmTableElements, 1000);
    }

    /**
     * Проверяем расчёт CPM в таблице под графиком - сравниваем построчно за каждый день
     * r_cpm = wages/adRequest * 1000
     */
    public boolean customReport_checkRcpmInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, adRequestTableElements, rCpmTableElements, 1000);
    }

    /**
     * Проверяем расчёт CPC в таблице CUSTOM REPORTS под графиком - сравниваем построчно за каждый день
     * cpc = wages/clicks * 100
     */
    public boolean customReport_checkCpcInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, clicksTableElements, cpcTableElements, 100);
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean customReport_checkVisibilityRateInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(impressionsTableElements, adRequestTableElements, visibilityRateTableElements, 100);
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/impressions * 100
     */
    public boolean customReport_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, impressionsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму Клики из графика с суммой Клики из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TABLE_TOTAL_LABEL, totalDbClicks));
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму ad_requests из графика с суммой Pageviews из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForAdRequest() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massAdRequests, adRequestTableElements, PAGES_VIEW_TABLE_TOTAL_LABEL, totalDbAdRequests));
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForWages() {
        return publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TABLE_TOTAL_LABEL, totalDbWages);
    }

    /**
     * Проверка Клики в интерфейсе CUSTOM REPORTS
     * сравниваем сумму impressions из графика с суммой Impressions из таблицы и с полем тотал таблицы
     */
    public boolean customReport_checkSumGraphSumTableTotalTableForImpressions() {
        return publisherStatHelper.checkSumGraphSumTableTotalTable(massImpressions, impressionsTableElements, IMPRESSIONS_TABLE_TOTAL_LABEL, totalDbImpressions);
    }

    public void createCustomReport() {
        customReportName = publisherStatHelper.createReport();
    }

    public boolean deleteCustomReport(String reportName) {
        return publisherStatHelper.deleteReport(reportName);
    }

    public boolean checkCustomReportInListInterfaceAndGoHim(String reportName) {
        return publisherStatHelper.checkCustomReportInListInterfaceAndGoHim(reportName);
    }

    ///////////////////////////////////////////// CUSTOM REPORT - FINISH ////////////////////////////////////////////////////////////////


    //////////////////////////////////////////// BY Day/Country/Source/SubId/Device - START //////////////////////////////////////////////////////////////

    public void selectCalendarPeriodJs(CalendarPeriods period) {
        startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);
        CALENDAR_LABEL.shouldBe(visible);
        log.info("создаём js переменную на стороне клиента из которой мы будем получать параметры из графика(js-action будет отрабатывать после каждого респонса(период в календаре))");
        initGraph();
        log.info("Choose period in calendar and filter data");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);
    }

    /**
     * Проверка Клики в интерфейсе By Country/Device/Etc
     * сравниваем сумму Клики из графика с суммой Клики из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TABLE_TOTAL_LABEL, totalDbClicks));
    }

    /**
     * Проверка AdRequests в интерфейсе By Country/Device/Etc
     * сравниваем сумму ad_requests из графика с суммой Pageviews из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForAdRequest() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massAdRequests, adRequestTableElements, PAGES_VIEW_TABLE_TOTAL_LABEL, totalDbAdRequests));
    }

    /**
     * Проверка Wages в интерфейсе By Country/Device/Etc
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForWages() {
        return publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TABLE_TOTAL_LABEL, totalDbWages);
    }

    /**
     * Проверка Impressions в интерфейсе By Country/Device/Etc
     * сравниваем сумму impressions из графика с суммой Impressions из таблицы и с полем тотал таблицы
     */
    public boolean statBy_checkSumGraphSumTableTotalTableForImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massImpressions, impressionsTableElements, IMPRESSIONS_TABLE_TOTAL_LABEL, totalDbImpressions));
    }

    /**
     * Проверяем расчёт CPC в графике
     * cpc = wages/clicks * 100
     */
    public boolean statBy_checkCpcInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massClicks, massCPC, 100);
    }

    /**
     * Проверяем total CPC
     * cpc = wages/sold clicks(stats.publishers_statistics.goods_clicks_accepted) * 100
     */
    public boolean statBy_checkTotalCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, CLICKS_TABLE_TOTAL_LABEL, CPC_TABLE_TOTAL_LABEL, 100));
    }

    /**
     * Проверяем расчёт rCPM в графике
     * r_cpm = wages/adRequests * 1000
     */
    public boolean statBy_checkRcpmInGraph() {
        return publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massAdRequests, massRcpm, 1000);
    }

    /**
     * Проверяем расчёт eCpm в графике
     * eCpm = wages/Impressions * 1000
     */
    public boolean statBy_checkEcpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massImpressions, massEcpm, 1000));
    }

    /**
     * Проверяем расчёт vCPM в таблице под графиком - сравниваем построчно за каждый день
     * eCPM = wages/impressions * 1000
     */
    public boolean statBy_checkEcpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, impressionsTableElements, eCpmTableElements, 1000));
    }

    /**
     * Проверяем total eCPM
     * eCPM = wages/impressions * 1000
     */
    public boolean statBy_checkTotalEcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, IMPRESSIONS_TABLE_TOTAL_LABEL, E_CPM_TABLE_TOTAL_WIDGETS_LABEL, 1000));
    }

    /**
     * Проверяем расчёт CPM в таблице под графиком - сравниваем построчно за каждый день
     * r_cpm = wages/adRequest * 1000
     */
    public boolean statBy_checkRcpmInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, adRequestTableElements, rCpmTableElements, 1000);
    }

    /**
     * Проверяем total CPC
     * r_cpm = wages/adRequest * 1000
     */
    public boolean statBy_checkTotalRcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TABLE_TOTAL_LABEL, PAGES_VIEW_TABLE_TOTAL_LABEL, R_CPM_TABLE_TOTAL_WIDGETS_LABEL, 1000));
    }

    /**
     * Проверяем расчёт CPC в таблице под графиком - сравниваем построчно за каждый день
     * cpc = wages/clicks * 100
     */
    public boolean statBy_checkCpcInTable() {
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, clicksTableElements, cpcTableElements, 100);
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean statBy_checkVisibilityRateInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(impressionsTableElements, adRequestTableElements, visibilityRateTableElements, 100));
    }

    /**
     * Проверяем total VisibilityRate
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean statBy_checkTotalVisibilityRate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(IMPRESSIONS_TABLE_TOTAL_LABEL, PAGES_VIEW_TABLE_TOTAL_LABEL, VISIBILITY_RATE_TABLE_TOTAL_LABEL, 100));
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/impressions * 100
     */
    public boolean statBy_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, impressionsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем total CTR
     * ctr = clicks/impressions * 100
     */
    public boolean statBy_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TABLE_TOTAL_LABEL, IMPRESSIONS_TABLE_TOTAL_LABEL, CTR_TABLE_TOTAL_LABEL, 100));
    }


    ///////////////////////////////////////////// BY Country/Source/SubId/Device - FINISH ////////////////////////////////////////////////////////////////


    ///////////////////////////////////////////// BY DAY - START ////////////////////////////////////////////////////////////////

    /**
     * get custom data column by header(column name)
     */
    public void getDataColumnByHeaderForStatByDay() {
        wagesTableElements = publisherStatHelper.getTableColumnData("Ad Revenue, €");
        impressionsTableElements = publisherStatHelper.getTableColumnData("Views with");
        eCpmTableElements = publisherStatHelper.getTableColumnData("Ad vCPM, €");
        adRequestTableElements = publisherStatHelper.getTableColumnData("Ad Requests");
        clicksTableElements = publisherStatHelper.getTableColumnData("Clicks");
        cpcTableElements = publisherStatHelper.getTableColumnData("Ad CPC, ¢");
        visibilityRateTableElements = publisherStatHelper.getTableColumnData("Visibility");
        ctrTableElements = publisherStatHelper.getTableColumnData("Ad CTR, %");
        soldClicksElements = publisherStatHelper.getTableColumnData("Ad Clicks");
    }

    /**
     * get total element from table (first row (th))
     */
    public void getTotalElementForStatByDay() {
        AD_REQUEST_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad Requests");
        IMPRESSIONS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Views with");
        CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Clicks");
        WAGES_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad Revenue, €");
        SOLD_CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad Clicks");
        CPC_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad CPC, ¢");
        CTR_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad CTR, %");
        E_CPM_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Ad vCPM, €");
        VISIBILITY_RATE_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Visibility");
    }

    /**
     * Проверка Клики в интерфейсе By Day
     * сравниваем сумму Клики из графика с суммой Клики из таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TOTAL_BY_DAY_LABEL, totalDbClicks));
    }

    /**
     * Проверка AdRequests в интерфейсе By Day
     * сравниваем сумму ad_requests из графика с суммой Pageviews из таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForAdRequest() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massAdRequests, adRequestTableElements, AD_REQUEST_TOTAL_BY_DAY_LABEL, totalDbAdRequests));
    }

    /**
     * Проверка Wages в интерфейсе By Day
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForWages() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TOTAL_BY_DAY_LABEL, totalDbWages));
    }

    /**
     * Проверка Impressions в интерфейсе By Day
     * сравниваем сумму impressions из графика с суммой Impressions из таблицы
     */
    public boolean statByDay_checkSumGraphSumTableTotalTableForImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massImpressions, impressionsTableElements, IMPRESSIONS_TOTAL_BY_DAY_LABEL, totalDbImpressions));
    }

    /**
     * Проверяем расчёт CPC в таблице By Day под графиком - сравниваем построчно за каждый день
     * cpc = wages/sold clicks * 100
     */
    public boolean statByDay_checkCpcInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, soldClicksElements, cpcTableElements, 100));
    }

    /**
     * Проверяем total CPC
     * cpc = wages/sold clicks * 100
     */
    public boolean statByDay_checkTotalCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, SOLD_CLICKS_TOTAL_BY_DAY_LABEL, CPC_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем расчёт eCpm в графике
     * eCpm = wages/Impressions * 1000
     */
    public boolean statByDay_checkEcpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massImpressions, massEcpm, 1000));
    }

    /**
     * Проверяем расчёт vCPM в таблице под графиком - сравниваем построчно за каждый день
     * eCPM = wages/impressions * 1000
     */
    public boolean statByDay_checkEcpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, impressionsTableElements, eCpmTableElements, 1000));
    }

    /**
     * Проверяем total eCpm
     * eCPM = wages/impressions * 1000
     */
    public boolean statByDay_checkTotalEcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, E_CPM_TOTAL_BY_DAY_LABEL, 1000));
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/impressions * 100
     */
    public boolean statByDay_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, impressionsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем total CTR
     * ctr = clicks/impressions * 100
     */
    public boolean statByDay_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean statByDay_checkVisibilityRateInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(impressionsTableElements, adRequestTableElements, visibilityRateTableElements, 100));
    }

    /**
     * Проверяем total VisibilityRate
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean statByDay_checkTotalVisibilityRate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(IMPRESSIONS_TOTAL_BY_DAY_LABEL, AD_REQUEST_TOTAL_BY_DAY_LABEL, VISIBILITY_RATE_TOTAL_BY_DAY_LABEL, 100));
    }

    ///////////////////////////////////////////// BY DAY - FINISH ////////////////////////////////////////////////////////////////


    //////////////////////////////////////////// WIDGETS LIST - START ////////////////////////////////////////////////////////////

    /**
     * get custom data column by header(@sortby)
     */
    public void getDataColumnByHeaderInWidgetsList() {
        wagesTableElements = helpersInit.getSortedHelper().getTableColumnData("wages");
        impressionsTableElements = helpersInit.getSortedHelper().getTableColumnData("impressions");
        eCpmTableElements = helpersInit.getSortedHelper().getTableColumnData("eCpm");
        adRequestTableElements = helpersInit.getSortedHelper().getTableColumnData("adRequests");
        clicksTableElements = helpersInit.getSortedHelper().getTableColumnData("clicks");
        cpcTableElements = helpersInit.getSortedHelper().getTableColumnData("cpc");
        rCpmTableElements = helpersInit.getSortedHelper().getTableColumnData("r_cpm");
        visibilityRateTableElements = helpersInit.getSortedHelper().getTableColumnData("impressionsPercentage");
        ctrTableElements = helpersInit.getSortedHelper().getTableColumnData("ctr");
    }

    /**
     * get total element from table (first row (th))
     */
    public void getTotalElementForStatInWidgetsList() {
        AD_REQUEST_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("adRequests");
        IMPRESSIONS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("impressions");
        CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("clicks");
        WAGES_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("wages");
        E_CPM_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("eCpm");
        CPC_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("cpc");
        CTR_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("ctr");
        VISIBILITY_RATE_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalBySort("impressionsPercentage");
    }

    /**
     * Проверка Wages в интерфейсе publisher/widgets/site/
     * сравниваем сумму wages из графика с суммой Revenue из таблицы и с полем тотал таблицы
     */
    public boolean widgetList_checkSumGraphSumTableTotalTableForWages() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTableWithDot(massWages, wagesTableElements, WAGES_TOTAL_BY_DAY_LABEL, totalDbWages));
    }

    /**
     * Проверка Клики в интерфейсе publisher/widgets/site/
     * сравниваем сумму Клики из графика с суммой Клики из таблицы
     */
    public boolean widgetList_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massClicks, clicksTableElements, CLICKS_TOTAL_BY_DAY_LABEL, totalDbClicks));
    }

    /**
     * Проверка AdRequests в интерфейсе publisher/widgets/site/
     * сравниваем сумму ad_requests из графика с суммой Pageviews из таблицы
     */
    public boolean widgetList_checkSumGraphSumTableTotalTableForAdRequest() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massAdRequests, adRequestTableElements, AD_REQUEST_TOTAL_BY_DAY_LABEL, totalDbAdRequests));
    }

    /**
     * Проверка Impressions в интерфейсе publisher/widgets/site/
     * сравниваем сумму impressions из графика с суммой Impressions из таблицы
     */
    public boolean widgetList_checkSumGraphSumTableTotalTableForImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(massImpressions, impressionsTableElements, IMPRESSIONS_TOTAL_BY_DAY_LABEL, totalDbImpressions));
    }

    /**
     * Проверяем расчёт CPC в таблице под графиком - сравниваем построчно за каждый день
     * cpc = wages/sold clicks(stats.publishers_statistics.goods_clicks_accepted) * 100
     */
    public boolean widgetList_checkCpcInTable() {
        ArrayList<String> soldClicks = publisherStatistics.getSumGoodsClickForAllWidgetsBySite(startEndDate, wagesTableElements.size());
        return publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, soldClicks, cpcTableElements, 100);
    }

    /**
     * Проверяем расчёт CTR в таблице под графиком - сравниваем построчно за каждый день
     * ctr = clicks/impressions * 100
     */
    public boolean widgetList_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, impressionsTableElements, ctrTableElements, 100));
    }

    /**
     * Проверяем расчёт VisibilityRate в таблице под графиком - сравниваем построчно за каждый день
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean widgetList_checkVisibilityRateInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(impressionsTableElements, adRequestTableElements, visibilityRateTableElements, 100));
    }

    /**
     * Проверяем расчёт vCPM в таблице под графиком - сравниваем построчно за каждый день
     * eCPM = wages/impressions * 1000
     */
    public boolean widgetList_checkEcpmInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(wagesTableElements, impressionsTableElements, eCpmTableElements, 1000));
    }

    /**
     * Проверяем расчёт eCpm в графике
     * eCpm = wages/Impressions * 1000
     */
    public boolean widgetList_checkEcpmInGraph() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInGraphForAllData(massWages, massImpressions, massEcpm, 1000));
    }

    /**
     * Проверяем total eCPM
     * eCPM = wages/impressions * 1000
     */
    public boolean widgetList_checkTotalEcpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, E_CPM_TOTAL_BY_DAY_LABEL, 1000));
    }

    /**
     * Проверяем total CPC
     * cpc = wages/sold clicks(stats.publishers_statistics.goods_clicks_accepted) * 100
     */
    public boolean widgetList_checkTotalCpc() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(WAGES_TOTAL_BY_DAY_LABEL, totalDbGoodsClicks, CPC_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем total CTR
     * ctr = clicks/impressions * 100
     */
    public boolean widgetList_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, 100));
    }

    /**
     * Проверяем total VisibilityRate
     * VisibilityRate = impressions/ad_requests * 100
     */
    public boolean widgetList_checkTotalVisibilityRate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(IMPRESSIONS_TOTAL_BY_DAY_LABEL, AD_REQUEST_TOTAL_BY_DAY_LABEL, VISIBILITY_RATE_TOTAL_BY_DAY_LABEL, 100));
    }

    //////////////////////////////////////////// WIDGETS LIST - FINISH ////////////////////////////////////////////////////////////

    /**
     * js-команда для дальнейшего получения данных из графика
     */
    private void initGraph(){
        publisherStatHelper.initGraph();
    }

    /**
     * метод переключает в графике отображаемые параметры
     */
    public void switchParamInGraph(String param) {
        helpersInit.getBaseHelper().selectCurrentValInSelectListJS(GRAPH_SELECT, param);
    }

    /**
     * метод проверяет количество переданных колонок таблицы под графиком
     */
    public boolean checkSizeHeadersInBaseTable(int size) {
        return helpersInit.getBaseHelper().checkDataset($$(TABLE_HEADERS).size(), size);
    }

    public void loadDataIfPaginatorDisplayed(){ publisherStatHelper.loadDataIfPaginatorDisplayed(); }
}
