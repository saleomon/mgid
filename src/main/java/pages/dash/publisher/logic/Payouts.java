package pages.dash.publisher.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import org.openqa.selenium.By;
import pages.dash.publisher.helpers.PayoutsHelper;

import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;
import static pages.dash.publisher.locators.PayoutsLocators.*;
import static pages.dash.publisher.variables.PayoutsVariables.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.dash.userProfile.locators.UserProfileLocators.INFORMATION_TEXT_FIELD;

public class Payouts {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final PayoutsHelper payoutsHelper;
    private String bankName;
    private String bankWireAchSwiftCode;
    private String beneficiarysAccountName;
    private String beneficiarysAddress;
    private String accountNumberIban;
    private String bankAddress;
    private String[] transferCurrency;
    private String[] transferCountry;
    private String dateOfBirth;
    private String taxId;
    private String[] countryOfBirth;
    private String ifscCode;
    private String nationalIdentityNumber;
    private String cnapsNumber;
    private String clabeNumber;
    private String bankBranchCode;
    private String cnpjCpfCode;
    private String dniRucTaxId;
    private String paymaster24ClientName;
    private String paymaster24ClientSurname;
    private Map paymaster24Country;
    private String paymaster24purseNumber;
    private String purseValue;

    public Payouts(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        payoutsHelper = new PayoutsHelper(log, helpersInit);
    }

    /**
     * add/edit/ check wire purse
     */
    public void addAchTransferPurse(String purseType, String purseName, String subnet) {
        payoutsHelper.openAddPursePopup();
        fillAllFieldsForBankTransferPayment(purseType, purseName, subnet);
        payoutsHelper.checkAndCloseMailPopup(addPaymentEmailPopupText);
    }

    /**
     * Add International bank-to-bank transfers purse
     */
    public void addInternationalBankToBankTransfers(String purseType, String purseName, String subnet) {
        payoutsHelper.openAddPursePopup();
        fillAllFieldsForInternationalBankToBankTransfers(purseType, purseName, subnet);
        payoutsHelper.checkAndCloseMailPopup(addPaymentEmailPopupText);
    }

    /**
     * Add International bank-to-bank transfers purse for custom countries
     */
    public void addInternationalBankToBankTransfersForCustomCountries(String purseType, String purseName, String subnet) {
        payoutsHelper.openAddPursePopup();
        fillAllFieldsForInternationalBankToBankTransfersForCustomCountries(purseType, purseName, subnet);
        payoutsHelper.checkAndCloseMailPopup(addPaymentEmailPopupText);
    }

    /**
     * Check B2B ACH payout type
     */
    public boolean checkPayOutType(String purseType, String purseName, String subnet) {
        return payoutsHelper.checkAndCloseMailPopup(emailPaymentPopupSuccessText) &&
                payoutsHelper.checkPursesTypeInSelect(purseName) &&
                checkAchTransferPurse(purseType, purseName, subnet);
    }

    /**
     * check all base params for wire purse
     */
    public boolean checkAchTransferPurse(String purseType, String purseName, String subnet) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        if (subnet.equals("mgid")) payoutsHelper.selectBankTransfers(purseName);
        return payoutsHelper.checkBankName(bankName) &&
                payoutsHelper.checkBankWireAchSwiftCode(bankWireAchSwiftCode) &&
                payoutsHelper.checkAccountNameWire(beneficiarysAccountName) &&
                payoutsHelper.checkTransferAddressWire(beneficiarysAddress) &&
                payoutsHelper.checkTransferAccountWire(accountNumberIban);
    }

    /**
     * Check International B2B payout type
     */
    public boolean checkInternationalPayOutType(String purseId, String purseType, String purseName, String subnet) {
        return payoutsHelper.checkAndCloseMailPopup(emailPaymentPopupSuccessText) &&
                payoutsHelper.checkPursesTypeInSelect(purseName) &&
                checkInternationalTransferPurse(purseId, purseType, purseName, subnet);
    }

    /**
     * Check International B2B payout type for custom countries
     */
    public boolean checkInternationalPayOutTypeForCustomCountries(String purseType, String purseName, String subnet) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                payoutsHelper.checkAndCloseMailPopup(emailPaymentPopupSuccessText) &&
                        payoutsHelper.checkPursesTypeInSelect(purseName) &&
                        checkInternationalTransferPurseForCustomCountries(purseType, purseName, subnet));
    }

    /**
     * check base params for International bank-to-bank transfers
     */
    public boolean checkInternationalTransferPurse(String purseId, String purseType, String purseName, String subnet) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        if (subnet.equals("mgid")) payoutsHelper.selectBankTransfers(purseName);
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                payoutsHelper.checkBankTransferId(purseId) &&
                        payoutsHelper.checkInternationalBankName(bankName) &&
                        payoutsHelper.checkInternationalBankAddress(bankAddress) &&
                        payoutsHelper.checkInternationalBankWireAchSwiftCode(bankWireAchSwiftCode) &&
                        payoutsHelper.checkInternationalBeneficiarysName(beneficiarysAccountName) &&
                        payoutsHelper.checkInternationalBeneficiarysAddress(beneficiarysAddress) &&
                        payoutsHelper.checkInternationalAccountNumberIban(accountNumberIban) &&
                        payoutsHelper.checkInternationalTransferCountry(transferCountry) &&
                        payoutsHelper.checkInternationalTransferCurrency(transferCurrency));
    }

    /**
     * check all params for International bank-to-bank transfers with custom countries
     */
    public boolean checkInternationalTransferPurseForCustomCountries(String purseType, String purseName, String subnet) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        if (subnet.equals("mgid")) payoutsHelper.selectBankTransfers(purseName);
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                payoutsHelper.checkInternationalBankName(bankName) &&
                        payoutsHelper.checkInternationalBankAddress(bankAddress) &&
                        payoutsHelper.checkInternationalBankWireAchSwiftCode(bankWireAchSwiftCode) &&
                        payoutsHelper.checkInternationalBeneficiarysName(beneficiarysAccountName) &&
                        payoutsHelper.checkInternationalBeneficiarysAddress(beneficiarysAddress) &&
                        payoutsHelper.checkInternationalAccountNumberIban(accountNumberIban) &&
                        payoutsHelper.checkInternationalTransferCountry(transferCountry) &&
                        payoutsHelper.checkInternationalTransferCurrency(transferCurrency) &&
                        checkInternationalTransferAdditionalFields());
    }

    /**
     * Check additional fields for International bank-to-bank transfers
     */
    private boolean checkInternationalTransferAdditionalFields() {
        return switch (transferCountry[1]) {
            case "dominican republic" -> payoutsHelper.checkDateOfBirth(dateOfBirth) && payoutsHelper.checkTaxId(taxId);
            case "turkey", "nigeria", "morocco", "pakistan", "zimbabwe" -> payoutsHelper.checkDateOfBirth(dateOfBirth)
                    && payoutsHelper.checkCountryOfBirth(countryOfBirth);
            case "india" -> payoutsHelper.checkIfscCode(ifscCode);
            case "ukraine" -> payoutsHelper.checkDateOfBirth(dateOfBirth) && payoutsHelper.checkNationalIdentityNumber(nationalIdentityNumber);
            case "china" -> payoutsHelper.checkCnapsNumber(cnapsNumber);
            case "mexico" -> payoutsHelper.checkDateOfBirth(dateOfBirth)
                    && payoutsHelper.checkCountryOfBirth(countryOfBirth)
                    && payoutsHelper.checkClabeNumber(clabeNumber);
            case "brazil" -> payoutsHelper.checkBankBranchCode(bankBranchCode)
                    && payoutsHelper.checkCnpjCpfCode(cnpjCpfCode);
            case "peru" -> payoutsHelper.checkCountryOfBirth(countryOfBirth)
                    && payoutsHelper.checkDateOfBirth(dateOfBirth)
                    && payoutsHelper.checkDniRucTaxId(dniRucTaxId);
            default -> false;
        };
    }

    /**
     * edit wire purse
     */
    public void editAchTransferPurse(String purseType, String purseId, String subnet) {
        payoutsHelper.openEditPursePopup();
        fillAllFieldsForBankTransferPayment(purseType, purseId, subnet);
    }

    /**
     * edit International bank-to-bank transfers
     */
    public void editInternationalTransferPurse(String purseType, String purseId, String subnet) {
        payoutsHelper.openEditPursePopup();
        fillAllFieldsForInternationalBankToBankTransfers(purseType, purseId, subnet);
    }

    /**
     * edit International bank-to-bank transfers for custom countries
     */
    public void editInternationalTransferPurseForCustomCountries(String purseType, String purseId, String subnet) {
        payoutsHelper.openEditPursePopup();
        fillAllFieldsForInternationalBankToBankTransfersForCustomCountries(purseType, purseId, subnet);
    }

    /**
     * Fill base fields for Bank-to-Bank (B2B) transfers
     */
    private void fillAllFieldsForBankTransferPayment(String purseType, String purseName, String subnet) {
        log.info("Fill all base params");
        payoutsHelper.selectPurseType(purseType);
        if (subnet.equals("mgid")) payoutsHelper.selectBankTransfers(purseName);
        bankName = payoutsHelper.setBankName();
        bankWireAchSwiftCode = payoutsHelper.setBankWireAchSwiftCode();
        beneficiarysAccountName = payoutsHelper.setBeneficiarysAccountName();
        beneficiarysAddress = payoutsHelper.setBeneficiarysAddress();
        accountNumberIban = payoutsHelper.setAccountNumberIban();
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
    }

    /**
     * Fill all fields for Bank-to-Bank (B2B) transfers
     */
    private void fillAllFieldsForInternationalBankToBankTransfers(String purseType, String purseName, String subnet) {
        fillAllBaseParam(purseType, purseName, subnet);
        transferCountry = payoutsHelper.setInternationalTransferCountry(false, transferCountry);
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
    }

    /**
     * Fill all fields for Bank-to-Bank (B2B) transfers with additional fields
     */
    private void fillAllFieldsForInternationalBankToBankTransfersForCustomCountries(String purseType, String purseName, String subnet) {
        fillAllBaseParam(purseType, purseName, subnet);
        transferCountry = payoutsHelper.setInternationalTransferCountry(true, transferCountry);
        setInternationalTransferAdditionalFields(transferCountry);
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
    }

    /**
     * Fill all params for International bank-to-bank transfers
     */
    private void fillAllBaseParam(String purseType, String purseName, String subnet) {
        log.info("Fill all base params");
        payoutsHelper.selectPurseType(purseType);
        if (subnet.equals("mgid")) payoutsHelper.selectBankTransfers(purseName);
        bankName = payoutsHelper.setInternationalBankName();
        bankAddress = payoutsHelper.setInternationalBankAddress();
        bankWireAchSwiftCode = payoutsHelper.setInternationalBankWireAchSwiftCode();
        beneficiarysAccountName = payoutsHelper.setInternationalBeneficiarysAccountName();
        beneficiarysAddress = payoutsHelper.setInternationalBeneficiarysAddress();
        accountNumberIban = payoutsHelper.setInternationalAccountNumberIban();
        transferCurrency = payoutsHelper.setInternationalTransferCurrency();
    }

    /**
     * Fill additional fields for International bank-to-bank transfers
     */
    public void setInternationalTransferAdditionalFields(String[] transferCountry) {
        log.info("Fill additional fields.");
        switch (transferCountry[1]) {
            case "dominican republic" -> {
                dateOfBirth = payoutsHelper.setDateOfBirth();
                taxId = payoutsHelper.setTaxId();
            }
            case "turkey", "nigeria", "morocco", "pakistan", "zimbabwe" -> {
                dateOfBirth = payoutsHelper.setDateOfBirth();
                countryOfBirth = payoutsHelper.setCountryOfBirth();
            }
            case "india" -> ifscCode = payoutsHelper.setIfscCode();
            case "ukraine" -> {
                dateOfBirth = payoutsHelper.setDateOfBirth();
                nationalIdentityNumber = payoutsHelper.setNationalIdentityNumber();
            }
            case "china" -> cnapsNumber = payoutsHelper.setCnapsNumber();
            case "mexico" -> {
                clabeNumber = payoutsHelper.setClabeNumber();
                dateOfBirth = payoutsHelper.setDateOfBirth();
                countryOfBirth = payoutsHelper.setCountryOfBirth();
            }
            case "brazil" -> {
                bankBranchCode = payoutsHelper.setBankBranchCode();
                cnpjCpfCode = payoutsHelper.setCnpjCpfCode(new String[]{cnpjCode, cpfCode});
            }
            case "peru" -> {
                countryOfBirth = payoutsHelper.setCountryOfBirth();
                dateOfBirth = payoutsHelper.setDateOfBirth();
                dniRucTaxId = payoutsHelper.setDniRucTaxId();
            }
        }
    }

    /**
     * Add not B2B transfer purse
     */
    public void addPurse(String purseType, String clientPurse) {
        payoutsHelper.openAddPursePopup();
        payoutsHelper.selectPurseType(purseType);
        payoutsHelper.setPurseNumber(purseType, clientPurse);
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
        waitForAjax();
        checkErrors();
    }

    /**
     * Check not B2B payout type
     */
    public boolean checkPayoutType(String purseType) {
        return payoutsHelper.checkPursesTypeInSelect(purseType);
    }

    /**
     * Edit not B2B payout type
     */
    public void editPurse(String purseType, String clientPurse) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        purseValue = payoutsHelper.setPurseNumber(purseType, clientPurse);
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
    }

    /**
     * Change favorite wallet
     */
    public String changeFavoriteWallet(String currentPayoutScheme, String purseId) {
        log.info("Old purse Id: " + purseId);
        if (currentPayoutScheme.equals("standart") || $x(".//span[@val='" + purseId + "']")
                .shouldBe(exist, ofSeconds(12)).scrollTo().attr("selected") != null) {
            purseId = payoutsHelper.selectAutoPurseWallet();
        } else {
            log.info("currentScheme - " + currentPayoutScheme);
            log.info($x(".//span[@val='" + purseId + "']").getAttribute("selected"));
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(purseId);
    }

    /**
     * Check 'Hold/Resume payments' button
     */
    public boolean checkHoldResumePaymentsButtonInClient(boolean isDisplayed) {
        return helpersInit.getBaseHelper().checkDataset(isDisplayed, payoutsHelper.checkHoldResumePaymentsButton());
    }

    /**
     * Check text in Hold payments confirmation popup
     */
    public boolean checkHoldPaymentsConfirmationPopupText() {
        return payoutsHelper.checkConfirmationPopupText(holdPaymentsPopupText);
    }

    /**
     * Check text in Resume payments confirmation popup
     */
    public boolean checkResumePaymentsConfirmationPopupText() {
        return payoutsHelper.checkConfirmationPopupText(resumePaymentsPopupText);
    }

    /**
     * Check 'Resume payments' button
     */
    public boolean checkResumePaymentsButton() {
        return payoutsHelper.checkResumePaymentsButton(resumePaymentsButtonText);
    }

    /**
     * Check 'Hold payments' button
     */
    public boolean checkHoldPaymentsButton() {
        return payoutsHelper.checkHoldPaymentsButton(holdPaymentsButtonText);
    }

    /**
     * Click 'Hold/Resume payments' button
     */
    public void clickHoldResumePaymentsButton() {
        payoutsHelper.clickHoldResumePaymentsButton();
    }

    /**
     * Check payments status
     */
    public boolean checkPaymentsStatus(String statusValue) {
        List<String> paymentStatuses = payoutsHelper.getPaymentsStatus();
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                !paymentStatuses.isEmpty() && paymentStatuses.stream().allMatch(i -> i.equals(statusValue))
        );
    }

    /**
     * Check purses set contain payout type
     */
    public boolean checkPursesTypes(String payoutType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(payoutsHelper.getPursesTypeList().contains(payoutType.toUpperCase()));
    }

    /**
     * Check purses set contain payout type
     */
    public boolean checkPursesTypes(List<String> payoutType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(payoutsHelper.getPursesTypeList().equals(payoutType));
    }

    /**
     * Check bank transfers type
     */
    public boolean checkBankTransfers(List<String> transfersTypes) {
        payoutsHelper.selectPurseType("btb-transfer");
        return helpersInit.getBaseHelper().getTextAndWriteLog(payoutsHelper.getBankTransfersList().equals(transfersTypes));
    }

    /**
     * Open add purses popup
     */
    public void openAddPursePopup() {
        payoutsHelper.openAddPursePopup();
    }

    /**
     * Check disabled purses in Payment select
     */
    public boolean checkChangeFavoritePurseDisabled() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(payoutsHelper.checkPursesSelectDisabled());
    }

    /**
     * Change favorite wallet
     */
    public void selectRandomFavoriteWallet() {
        log.info("Try to change favorite wallet");
        helpersInit.getBaseHelper().refreshCurrentPage();
        payoutsHelper.selectAutoPurseWallet();
        waitForAjax();
    }

    /**
     * Get PayPal purse Id
     */
    public String getPurseId(String purseType) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        return payoutsHelper.getPurseId();
    }

    /**
     * Check PayPal purse value
     */
    public boolean checkPayPalPurseValue() {
        payoutsHelper.openEditPursePopup();
        return helpersInit.getBaseHelper().checkDatasetEquals(purseValue, payoutsHelper.getPayPalPurseValue());
    }

    /**
     * Open Edit purse popup
     */
    public void openEditPursePopup() {
        payoutsHelper.openEditPursePopup();
    }

    /**
     * Check Payoneer purse value
     */
    public boolean checkPayoneerPurseValue() {
        return helpersInit.getBaseHelper().checkDatasetEquals(purseValue, payoutsHelper.getPayoneerlPurseValue());
    }

    /**
     * Add Paymaster24 purse
     */
    public void addPaymaster24Purse(String purseType, String clientPurse) {
        payoutsHelper.openAddPursePopup();
        payoutsHelper.selectPurseType(purseType);
        fillAllFieldsForPaymaster24Payment(purseType, clientPurse);
        payoutsHelper.checkAndCloseMailPopup(addPaymentEmailPopupTextUkrainian);
    }

    /**
     * Add Paymaster24 purse for Ukraine
     */
    public void addPaymaster24PurseUkraine(String purseType, String clientPurse) {
        payoutsHelper.openAddPursePopup();
        payoutsHelper.selectPurseType(purseType);
        fillAllFieldsForPaymaster24Payment(purseType, clientPurse);
    }

    /**
     * Fill all fields for Paymaster24
     */
    private void fillAllFieldsForPaymaster24Payment(String purseType, String clientPurse) {
        paymaster24purseNumber = payoutsHelper.setPurseNumber(purseType, clientPurse);
        paymaster24ClientName = payoutsHelper.setClientName();
        paymaster24ClientSurname = payoutsHelper.setClientSurname();
        dateOfBirth = payoutsHelper.setClientBirthDate();
        paymaster24Country = payoutsHelper.setCountryPaymaster24();
        helpersInit.getBaseHelper().isConfirmDisplayedWithoutWFA();
        waitForAjax();
        checkErrors();
    }

    /**
     * Check all base params for Paymaster24 purse
     */
    public boolean checkPaymaster24Purse(String purseType) {
        payoutsHelper.openEditPursePopup();
        payoutsHelper.selectPurseType(purseType);
        return helpersInit.getBaseHelper().checkDatasetEquals(paymaster24purseNumber, PAYMASTER24_PURSE_NUMBER_INPUT.val()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(paymaster24ClientName, PAYMASTER24_NAME_INPUT.val()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(paymaster24ClientSurname, PAYMASTER24_SURNAME_INPUT.val()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(dateOfBirth, PAYMASTER24_DATE_OF_BIRTH_INPUT.val()) &&
                payoutsHelper.checkPaymaster24Country(paymaster24Country);
    }

    /**
     * Edit Paymaster24 purse
     */
    public void editPaymaster24(String purseType, String clientPurse) {
        fillAllFieldsForPaymaster24Payment(purseType, clientPurse);
    }

    /**
     * Check purse type is available
     */
    public boolean checkPurseTypeIsAvailable(String purseType) {
        return payoutsHelper.checkPursesTypeIsAvailable(purseType);
    }

    /**
     * Check Add New Payment Method button is disabled
     */
    public boolean checkAddNewPaymentMethodButtonIsDisabled() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                ADD_PURSE_BUTTON.shouldBe(visible).parent().has(cssClass("disabled")));
    }

    /**
     * Check Capitalist purse value
     */
    public boolean checkCapitalistPurseValue() {
        return helpersInit.getBaseHelper().checkDatasetEquals(purseValue, payoutsHelper.getCapitalistPurseValue());
    }

    public String getConfirmationPopupText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(POP_UP.text());
    }

    public String getAddPursePopupText() {
        CONFIRM_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_PURSE_POPUP.find(By.xpath("p")).text());
    }

    public String getAlertPopupText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ALERT_POPUP.text());
    }

    public String getNotificationText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NOTIFICATION.shouldBe(visible).text());
    }

    public void confirmIdenfyPopup() {
        CONFIRM_BUTTON.shouldBe(visible).click();
    }

    public String getVerificationTabText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(INFORMATION_TEXT_FIELD.text());
    }
}
