package pages.dash.publisher.logic;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.base.HelpersInit;
import core.helpers.statCalendar.CalendarForStatistics;
import libs.clickhouse.queries.DirectPublisherDemand;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import pages.dash.publisher.helpers.PublisherStatHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static libs.hikari.tableQueries.partners.DirectPublisherDemandAds.selectAddByTitle;
import static pages.dash.publisher.locators.DirectDemandLocators.*;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class DirectDemand {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final PublisherStatHelper publisherStatHelper;
    DirectPublisherDemand directPublisherDemand;
    private String name;
    private String title;
    private String url;
    private String picture;
    private String whenStart;
    private String whenEnd;
    private String guaranteedClicks;
    private Boolean focalPointState = false;
    private int focalPointLeft;
    private boolean isNeedToEditImage = true;
    private ArrayList<String> startEndDate;
    ElementsCollection impressionsTableElements, clicksTableElements, ctrTableElements;
    SelenideElement IMPRESSIONS_TOTAL_BY_DAY_LABEL, CLICKS_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL;
    //total get in clickHouse
    String totalDbClicks, totalDbImpressions;

    public DirectDemand(HelpersInit helpersInit, Logger log) {
        this.helpersInit = helpersInit;
        this.log = log;
        publisherStatHelper = new PublisherStatHelper(log, helpersInit);
        directPublisherDemand = new DirectPublisherDemand();
    }

    public String getTitle() {
        return title;
    }

    public DirectDemand setUrl(String url) {
        this.url = url;
        return this;
    }

    public DirectDemand setPicture(String picture) {
        this.picture = picture;
        return this;
    }

    public DirectDemand useManualFocalPoint(boolean usePoint) {
        focalPointState = usePoint;
        return this;
    }

    public DirectDemand setFocalPoint(int focaLeft) {
        this.focalPointLeft = focaLeft;
        return this;
    }

    public DirectDemand setNeedToEditImage(boolean needToEditImage) {
        isNeedToEditImage = needToEditImage;
        return this;
    }

    public int createNewAdd(){
        fillAddBaseSettings();

        return getAddIdByTitle(title);
    }

    public boolean editAdd(){
        fillAddBaseSettings();

        return !SUBMIT_BUTTON.isDisplayed();
    }

    private void fillAddBaseSettings(){
        name = fillName(name);
        title = fillTitle(title);
        url = fillUrl(url);
        fillImageByLocal(picture);
        indicateFocalPoint();
        guaranteedClicks = fillGuaranteedClicks(guaranteedClicks);
        selectAllPlacement();
        whenStart = fillWhenStart();
        whenEnd = fillWhenEnd();
        clickSubmit();
    }

    public boolean checkNewAddInList(){
        return helpersInit.getBaseHelper().checkDatasetEquals(getStatus(), "Active") &
                helpersInit.getBaseHelper().checkDatasetEquals(getNameInList(), name) &
                helpersInit.getBaseHelper().checkDatasetEquals(getTitleInList(), title) &
                helpersInit.getBaseHelper().checkDatasetEquals(getUrlInList(), url) &
                helpersInit.getBaseHelper().checkDatasetEquals(getWhenStartInList(), whenStart) &
                helpersInit.getBaseHelper().checkDatasetEquals(getWhenEndInList(), whenEnd) &
                helpersInit.getBaseHelper().checkDatasetEquals(getGuaranteedClicksInList(), guaranteedClicks);
    }

    public boolean checkAddInEdit(){
        return helpersInit.getBaseHelper().checkDatasetEquals(getNameInEdit(), name) &
                helpersInit.getBaseHelper().checkDatasetEquals(getTitleInEdit(), title) &
                helpersInit.getBaseHelper().checkDatasetEquals(getUrlInEdit(), url) &
                helpersInit.getBaseHelper().checkDatasetEquals(getWhenStartInEdit(), whenStart) &
                helpersInit.getBaseHelper().checkDatasetEquals(getWhenEndInEdit(), whenEnd) &
                helpersInit.getBaseHelper().checkDatasetEquals(getGuaranteedClicksInEdit(), guaranteedClicks);
    }

    public void clickAddNewCreativeButton(){
        ADD_CREATIVE_BUTTON.click();
        TITLE_INPUT.shouldBe(visible);
        checkErrors();
    }

    public void clickEditAddIcon(){
        EDIT_ADD_ICON.click();
        waitForAjaxPopup();
        TITLE_INPUT.shouldBe(visible);
        checkErrors();
    }

    public void clickDeleteAddIcon(){
        DELETE_ADD_ICON.shouldBe(visible).click(usingJavaScript());
        checkErrors();
        sleep(500);
        isConfirmDisplayed();
        sleep(500);
        waitForAjax();
    }

    public void clickBlockUnBlockAddIcon(){
        BLOCK_ADD_ICON.click();
        waitForAjax();
        checkErrors();
    }

    private String fillTitle(String title) {
        String addTitle = Objects.requireNonNullElseGet(title, () -> "title demand " + getRandomWord(3));
        TITLE_INPUT.shouldBe(visible).val(addTitle);
        return addTitle;
    }

    private String fillName(String title) {
        String addTitle = Objects.requireNonNullElseGet(title, () -> "name demand" + randomNumberFromRange(1, 100));
        NAME_INPUT.shouldBe(visible).val(addTitle);
        return addTitle;
    }

    private String fillUrl(String url) {
        String addUrl = Objects.requireNonNullElseGet(url, () -> "https://demandurl" + getRandomWord(3) + ".com.ua");
        URL_INPUT.val(addUrl);
        return addUrl;
    }

    public void fillImageByLocal(String imageName) {
        if (isNeedToEditImage) {
            String image = imageName != null ? imageName : "Stonehenge.jpg";
            IMAGE_LOCATOR.sendKeys(LINK_TO_RESOURCES_IMAGES + image);
            waitForAjaxLoader();
        }
    }

    private String fillWhenStart(){
        WHEN_START_INPUT.shouldBe(visible).click();
        NEXT_START_MONTH_BUTTON.shouldBe(visible).click();
        int t = randomNumbersInt(AMOUNT_OF_DAY_FIRST.size());
        log.info("fillWhenStart: " + t);
        AMOUNT_OF_DAY_FIRST.get(t).shouldBe(visible).click();
        return WHEN_START_INPUT.text();
    }

    private String fillWhenEnd(){
        WHEN_END_INPUT.click();
        NEXT_END_MONTH_BUTTON.shouldBe(visible).click();
        int t = randomNumbersInt(AMOUNT_OF_DAY_SECOND.size());
        log.info("fillWhenEnd: " + t);
        AMOUNT_OF_DAY_SECOND.get(t).shouldBe(visible).click();
        return WHEN_END_INPUT.text();
    }

    private String fillGuaranteedClicks(String guaranteedClicks){
        String addGuaranteedClicks = Objects.requireNonNullElse(guaranteedClicks, "57");
        GUARANTEED_CLICKS_INPUT.val(addGuaranteedClicks);
        return addGuaranteedClicks;
    }

    public void indicateFocalPoint() {
        if(focalPointState) {
            setFocalPoint(true);
        }
    }

    public void setFocalPoint(boolean isSaveData) {
        IMAGE_MANUAL_BUTTON.click();
        IMAGE_FOCUS.shouldBe(visible);
        sleep(500);
        executeJavaScript("document.getElementById('crosshair').style.left = '" + focalPointLeft + "px'; document.getElementById('crosshair').style.top = '-" + focalPointLeft + "px'");
        sleep(500);
        waitForAjax();
        if (isSaveData) IMAGE_CONFIRM_BUTTON.click();
        waitForAjax();
    }

    public boolean isDisplayedFocalPointIcon() {
        return IMAGE_FOCUS.isDisplayed();
    }

    public void cancelManualFocalPoint() {
        IMAGE_CANCEL_BUTTON.click();
        waitForAjax();
        checkErrors();
        IMAGE_CANCEL_BUTTON.shouldBe(hidden);
    }

    public void resetDataFocalPointToDefault() {
        IMAGE_DEFAULT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void editDataFocalPoint() {
        IMAGE_EDIT_BUTTON.click();
        IMAGE_EDIT_BUTTON.shouldBe(hidden);
        IMAGE_CANCEL_BUTTON.shouldBe(visible);
        waitForAjax();
        checkErrors();
    }

    private void selectAllPlacement(){
        CHOOSE_ALL_PLACEMENT.click();
    }

    private void clickSubmit(){
        SUBMIT_BUTTON.click(usingJavaScript());
        waitForAjaxPopup();
        checkErrors();
    }

    public int getAddIdByTitle(String title) {
        if (!SUBMIT_BUTTON.isDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(selectAddByTitle(title));
        }
        return 0;
    }

    public boolean isDisplayedAnyAdd(){ return $(ADD_ID_LOCATOR).exists(); }

    private String getDataColumn(String columnName){
        return  $x(".//tr[@class='teaser']/td[count(//thead//*[normalize-space(text()) = '" + columnName + "']/preceding-sibling::th)+1]").text();
    }

    private String getNameInList(){ return NAME_FROM_LIST_LABEL.text().trim(); }
    private String getTitleInList(){ return TITLE_FROM_LIST_LABEL.text().trim(); }

    private String getGuaranteedClicksInList(){
        return GUARANTEED_CLICKS_FROM_LIST_LABEL.text();
    }

    private String getWhenStartInList(){
        return getDataColumn("Start date");
    }

    private String getWhenEndInList(){ return getDataColumn("End date"); }

    public String getStatus(){ return $(STATUS_ADD_LABEL).text(); }

    public List<String> getStatuses(){ return $$(STATUS_ADD_LABEL).texts(); }

    private String getUrlInList(){ return URL_ADD_LIST_LABEL.text(); }

    private String getNameInEdit(){ return NAME_INPUT.val(); }

    private String getTitleInEdit(){ return TITLE_INPUT.val(); }

    private String getUrlInEdit(){ return URL_INPUT.val(); }

    private String getWhenStartInEdit(){ return WHEN_START_INPUT.text(); }

    private String getWhenEndInEdit(){ return WHEN_END_INPUT.text(); }

    private String getGuaranteedClicksInEdit(){ return GUARANTEED_CLICKS_INPUT.val(); }

    public String getBlockUnblockAddStatus(){ return BLOCK_ADD_ICON.attr("oldtitle"); }

    public String getCustomAddError(){ return CUSTOM_ADD_ERROR.text(); }

    public void fillValueInSearchFilter(String value){
        SEARCH_ADD_INPUT.sendKeys(value + Keys.ENTER);
        waitForAjax();
        checkErrors();
    }

    public ElementsCollection getAddsOnPage(){ return $$(ADD_ID_LOCATOR); }

    public void selectStatusFilter(String status) {
        helpersInit.getBaseHelper().selectCurrentTextInSelectListJS(STATUS_FILTER_SELECT, status);
    }

    public void clickFilterButton(){
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    ////////////// STAT ///////////////////
    public void selectCalendarPeriodJs(CalendarForStatistics.CalendarPeriods period) {
        startEndDate = helpersInit.getCalendarPeriodHelper().getCalendarPeriodDays(period);
        CALENDAR_LABEL.shouldBe(visible);
        log.info("Choose period in calendar and filter data");
        helpersInit.getBaseHelper().selectCalendarPeriodJs(period, startEndDate);
    }

    /**
     * get custom data column by header(column name)
     */
    public void getDataColumnByHeaderForStatByDay() {
        impressionsTableElements = publisherStatHelper.getTableColumnData("Imps");
        clicksTableElements = publisherStatHelper.getTableColumnData("Clicks");
        ctrTableElements = publisherStatHelper.getTableColumnData("CTR, %");
    }

    /**
     * get total element from table (first row (th))
     */
    public void getTotalElementForStatByDay() {
        IMPRESSIONS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Imps");
        CLICKS_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("Clicks");
        CTR_TOTAL_BY_DAY_LABEL = publisherStatHelper.getTotalByText("CTR, %");
    }

    /**
     * get total data for custom db fields from clickHouse
     * dataType - site = ?/composite = ?/""
     */
    public void getTotalDataFromPublisherStatisticsClickHouse(int... tickers_composite_id) {
        String ids = Arrays.stream(tickers_composite_id).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        directPublisherDemand.getSumForCustomFieldsFromClickHouse("composite IN(" + ids + ") ", startEndDate,
                "impressions",
                "clicks"
        );
        totalDbImpressions = directPublisherDemand.getFieldsValueByIndex(1);
        totalDbClicks = directPublisherDemand.getFieldsValueByIndex(2);
    }

    public boolean checkSizeHeadersInBaseTable(int size) {
        return helpersInit.getBaseHelper().checkDataset($$(TABLE_HEADERS).size(), size);
    }

    public boolean statByDay_checkSumGraphSumTableTotalTableForImpressions() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(impressionsTableElements, IMPRESSIONS_TOTAL_BY_DAY_LABEL, totalDbImpressions));
    }

    public boolean statByDay_checkSumGraphSumTableTotalTableForClicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkSumGraphSumTableTotalTable(clicksTableElements, CLICKS_TOTAL_BY_DAY_LABEL, totalDbClicks));
    }

    public boolean statByDay_checkCtrInTable() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomParamInTableInEveryRow(clicksTableElements, impressionsTableElements, ctrTableElements, 100));
    }

    /**
     * ctr = clicks/impressions * 100
     */
    public boolean statByDay_checkTotalCtr() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherStatHelper.checkCustomTotalParam(CLICKS_TOTAL_BY_DAY_LABEL, IMPRESSIONS_TOTAL_BY_DAY_LABEL, CTR_TOTAL_BY_DAY_LABEL, 100));
    }
}
