package pages.dash.publisher.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.InternalAdsHelper;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Condition.visible;
import static pages.dash.publisher.locators.InternalAdsLocators.*;

public class InternalAds {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final InternalAdsHelper internalAdsHelper;

    public InternalAds(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        this.internalAdsHelper = new InternalAdsHelper();
    }

    /**
     * Check that promo switcher is present
     */
    public boolean checkPromoFilterSwitcherIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PROMO_SWITCHER.isDisplayed());
    }

    /**
     * Enable promo filter switcher
     */
    public void enablePromoFilter() {
        PROMO_SWITCHER_OFF.shouldBe(visible).click();
        log.info("Promo-filter enabled.");
    }

    /**
     * Check promo filter switcher state is Off
     */
    public boolean checkPromoFilterSwitcherIsOff() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PROMO_SWITCHER_OFF.isDisplayed() && !PROMO_SWITCHER_ON.isDisplayed());
    }

    /**
     * Check promo filter switcher state is On
     */
    public boolean checkPromoFilterSwitcherIsOn() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PROMO_SWITCHER_ON.shouldBe(visible).isDisplayed() && !PROMO_SWITCHER_OFF.isDisplayed());
    }

    /**
     * Get news count
     */
    public int getNewsCount() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TEASERS_ROWS.size());
    }

    /**
     * Block news by mass action
     */
    public void blockNewsByMassAction() {
        internalAdsHelper.selectAllNews();
        MASS_ACTION_BLOCK_ICON.shouldBe(visible).click();
        helpersInit.getBaseHelper().refreshCurrentPage();
    }

    /**
     * Unlock news by mass action
     */
    public void unblockNewsByMassAction() {
        internalAdsHelper.selectAllNews();
        MASS_ACTION_UNBLOCK_ICON.shouldBe(visible).click();
        helpersInit.getBaseHelper().refreshCurrentPage();
    }

    /**
     * Delete news by mass action
     */
    public void deleteNewsByMassAction() {
        internalAdsHelper.selectAllNews();
        MASS_ACTION_DELETE_ICON.shouldBe(visible).click();
        POPUP_CONFIRM_OK_BUTTON.shouldBe(appear).click();
        helpersInit.getBaseHelper().refreshCurrentPage();
    }

    /**
     * Check news statuses
     */
    public boolean checkNewsStatuses(String newsStatus) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NEWS_STATUS.size() > 0 &&
                NEWS_STATUS.texts().stream().allMatch(i -> i.equals(newsStatus)));
    }
}
