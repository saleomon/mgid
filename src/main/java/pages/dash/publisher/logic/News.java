package pages.dash.publisher.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.CreateEditNewsHelper;
import pages.dash.publisher.helpers.ListNewsHelper;

import static com.codeborne.selenide.Condition.visible;
import static pages.dash.advertiser.variables.TeaserVariables.*;
import static pages.dash.publisher.locators.news.AddNewsLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;

public class News {

    CreateEditNewsHelper createEditNewsHelper;
    ListNewsHelper listNewsHelper;

    private String title;
    private String domain;
    private String categoryId;
    private String description;
    private String image;

    public News(Logger log, HelpersInit helpersInit) {
        createEditNewsHelper = new CreateEditNewsHelper(log, helpersInit);
        listNewsHelper = new ListNewsHelper(log, helpersInit);
    }

    public News isGenerateNewValues(boolean generateNewValues) {
        createEditNewsHelper.setGenerateNewValues(generateNewValues);
        return this;
    }

    /**
     * Open edit news form
     */
    public void openNewsEditInterface(String newsId) {
        listNewsHelper.openNewsEditInterface(newsId);
    }

    /**
     * Edit news
     */
    public void editNews() {
        title = createEditNewsHelper.editTitle(title);
        domain = createEditNewsHelper.editUrl(domain);
        categoryId = createEditNewsHelper.editCategory(categoryId);
        image = createEditNewsHelper.editImageByLocal(image);
        description = createEditNewsHelper.editDescription(description);

        createEditNewsHelper.saveSettings();
    }

    /**
     * Import news
     */
    public void importNews() {
        listNewsHelper.createNewsByImport(fileNameForImportTeaser);
    }

    /**
     * Check imported news
     */
    public boolean checkImportedNews() {
        return listNewsHelper.checkNewsTitle(autocreativeNewsTitle) &
                listNewsHelper.checkNewsDescription(autocreativeNewsDescription);
    }

    public void createNews(String stringValue) {
        clearAndSetValue(LINK_INPUT, "tmp.dt07.net");
        clearAndSetValue(TITLE_INPUT, stringValue);
        clearAndSetValue(DESCRIPTION_INPUT, stringValue);
        createEditNewsHelper.loadImageByLocal(null);
        SAVE_BUTTON.click();
        POPUP_CLOSE_BUTTON.shouldBe(visible).click();
    }

    public boolean checkNewsWithNbsp(String value) {
        return listNewsHelper.checkNewsTitle(value) &&
                listNewsHelper.checkNewsDescription(value);
    }

    /**
     * Re-save news with no-break space in title and description
     */
    public void reSaveNewsWithNbsp() {
        createEditNewsHelper.saveSettings();
    }
}
