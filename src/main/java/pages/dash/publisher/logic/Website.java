package pages.dash.publisher.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.dash.publisher.helpers.WebSiteAddHelper;
import pages.dash.publisher.helpers.WebSiteListHelper;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.dash.publisher.locators.WebsiteListLocators.*;

public class Website {

    private final HelpersInit helpersInit;
    private final WebSiteAddHelper webSiteAddHelper;
    private final WebSiteListHelper websiteListHelper;

    private String domain = null;
    private String category = null;
    private String language = null;
    private String comment = null;
    private boolean utmTagging = false;
    private String utmSource = null;
    private String utmMedium = null;
    private String utmCampaign = null;
    private String siteId;
    private String billingCountry;


    public enum WebSiteStatus {
        ACTIVE("active"),
        REJECT("rejected"),
        ONMODERATION("onmoderation");

        private final String status;

        WebSiteStatus(String statusVal) {
            this.status = statusVal;
        }
        public String getTypeValue() {
            return status;
        }
    }


    public Website(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        websiteListHelper = new WebSiteListHelper(log, helpersInit);
        webSiteAddHelper = new WebSiteAddHelper(log, helpersInit);
    }

    public String getSiteId() {
        return siteId;
    }

    public String getDomain() {
        return domain;
    }

    public Website setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public Website setCategory(String category) {
        this.category = category;
        return this;
    }

    public Website setLanguage(String language) {
        this.language = language;
        return this;
    }

    public Website setUtmTagging(boolean utmTagging) {
        this.utmTagging = utmTagging;
        return this;
    }

    public Website setUtmSource(String utmSource) {
        this.utmSource = utmSource;
        return this;
    }

    public Website setUtmMedium(String utmMedium) {
        this.utmMedium = utmMedium;
        return this;
    }

    public Website setUtmCampaign(String utmCampaign) {
        this.utmCampaign = utmCampaign;
        return this;
    }

    public Website setSomeFields(boolean isSet) {
        webSiteAddHelper.setSetSomeFields(isSet);
        return this;
    }

    /**
     * Check Ads.txt on Publishers page
     */
    public boolean checkCustomAdsTxtPopupAndText(String adsPopupValue, String siteDomain) {
        if (adsPopupValue.equals("by button")) websiteListHelper.clickAdsTxtButton();
        return websiteListHelper.checkCustomAdsTxtPopupAndContent(adsPopupValue, siteDomain)
                && websiteListHelper.checkAdsTxtPopupClosing(adsPopupValue);
    }

    /**
     * Check Ads.txt on Publishers page
     */
    public boolean checkDefaultAdsTxtPopupAndText(String adsPopupValue, String siteDomain) {
        if (adsPopupValue.equals("by button")) websiteListHelper.clickAdsTxtButton();
        return websiteListHelper.checkDefaultAdsTxtPopupAndContent(adsPopupValue, siteDomain)
                && websiteListHelper.checkAdsTxtPopupClosing(adsPopupValue);
    }

    public void sendSiteToModeration(Integer site) {
        websiteListHelper.sendSiteToModeration(site);
    }

    public boolean checkPopupText(String popUpText) {
        return websiteListHelper.checkPopupText(popUpText);
    }

    public void closePopup() {
        websiteListHelper.waitAndClosePopUp();
    }

    public boolean checkVisibilitySendModerationIcon(Integer site) {
        return websiteListHelper.checkVisibilitySendModerationIcon(site);
    }

    public void addWebSite() {
        fillBaseWebSiteSettings();

        if (utmTagging) {
            webSiteAddHelper.switchOnUtmTagging(true);
            fillUtmTagging();
        }

        saveWebSite();
    }

    public void addWebSiteIdealmedia() {
        domain = webSiteAddHelper.setDomain(domain);
        category = webSiteAddHelper.chooseSiteCategory(category);
        comment = webSiteAddHelper.setComment(comment);
        if (utmTagging) {
            webSiteAddHelper.switchOnUtmTagging(true);
            fillUtmTagging();
        }
        saveWebSite();
    }

    public void editWebSite() {
        comment = webSiteAddHelper.setComment(comment);

        if (utmTagging) {
            webSiteAddHelper.switchOnUtmTagging(true);
            fillUtmTagging();
        }

        webSiteAddHelper.saveWebSiteSettings();
        websiteListHelper.waitAndClosePopUp();
    }

    public String getStatusWebSite(String websiteId) {
        return websiteListHelper.getDataSiteId(Integer.valueOf(websiteId)).attr("class");
    }

    public boolean checkSiteInEditInterface() {
        return webSiteAddHelper.checkComment(comment) &
                webSiteAddHelper.checkLanguage(language) &
                (!utmTagging || checkUtmTagging());
    }

    public boolean checkUtmTagging() {
        return
                helpersInit.getBaseHelper().checkDatasetEquals(webSiteAddHelper.getUtmSource(), utmSource) &
                        helpersInit.getBaseHelper().checkDatasetEquals(webSiteAddHelper.getUtmMedium(), utmMedium) &
                        helpersInit.getBaseHelper().checkDatasetEquals(webSiteAddHelper.getUtmCampaign(), utmCampaign);
    }

    public void fillBaseWebSiteSettings() {
        domain = webSiteAddHelper.setDomain(domain);
        category = webSiteAddHelper.chooseSiteCategory(category);
        language = webSiteAddHelper.chooseLanguageSite(language);
        comment = webSiteAddHelper.setComment(comment);
    }

    public void saveWebSite() {
        webSiteAddHelper.saveWebSiteSettings();
        siteId = websiteListHelper.getWebSiteId(domain);
    }

    public void switchOnUtmTagging() {
        webSiteAddHelper.switchOnUtmTagging(utmTagging);
    }

    public void fillUtmTagging() {
        utmSource = webSiteAddHelper.fillUtmSource(utmSource);
        utmMedium = webSiteAddHelper.fillUtmMedium(utmMedium);
        utmCampaign = webSiteAddHelper.fillUtmCampaign(utmCampaign);
    }

    /**
     * Check if 'Self registered popup appears'
     */
    public boolean checkSelfRegisterPopup() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SELF_REGISTER_POPUP.shouldBe(appear).isDisplayed());
    }

    /**
     * Check if 'Self registered popup appears'
     */
    public boolean checkSelfRegisterPopupIdealmedia() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SETUP_PROFILE_POPUP_IDEALMEDIA.shouldBe(appear).isDisplayed());
    }

    public void setupPersonalDataPopup() {
        SELF_REGISTER_POPUP_INDIVIDUAL_BUTTON.click();
        checkErrors();
    }

    /**
     * Set settings for self registered Publisher
     */
    public void setupProfileInPopup() {
        setupPersonalDataPopup();
        billingCountry = websiteListHelper.setClientsCountry();
        websiteListHelper.saveAndFinish();
    }

    /**
     * Set settings for self registered Publisher
     */
    public void setupProfileInPopupIdealmedia() {
        billingCountry = websiteListHelper.setClientsCountryIdealmedia();
        websiteListHelper.acceptPolicyCheckbox();
        websiteListHelper.submitPopup();
    }

    /**
     * Check clients login into dashboard on pages
     */
    public boolean checkLoginOnPage(String loginValue) {
        return websiteListHelper.checkLoginOnPage(loginValue);
    }

    /**
     * Open Ads.txt popup
     */
    public void openAdsTxtPopup() {
        websiteListHelper.clickAdsTxtButton();
    }

    /**
     * Get Ads.txt content for domain
     */
    public String getAdsTxtContent(String domainValue) {
        return $x(String.format(ADS_TXT_POPUP_DOMAIN_CONTENT_FIELD, domainValue)).shouldBe(visible).text();
    }

    /**
     * Check Ads.txt lines color Green
     */
    public boolean checkAdsTxtLinesColorGreen(String domainValue, String adsTxtRowValue) {
       return $x(String.format(ADS_TXT_POPUP_LINE_FIELD, domainValue, adsTxtRowValue)).has(attribute("style", "color: green;"));
    }

    public boolean directDemandIsDisplayed(){ return websiteListHelper.directDemandIsDisplayed(); }
}
