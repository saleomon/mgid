package pages.cab.advertisingAgencies.helper;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.advertisingAgencies.locators.AdvertisingAgenciesLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class AdvertisingAgenciesHelper {

    public AdvertisingAgenciesHelper() {
    }

    public void registrationHistoryIconClick() {
        REGISTRATION_HISTORY_ICON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public boolean isShowLinkRegistrationHistoryDialog() {
        return REGISTRATION_HISTORY_DIALOG.isDisplayed();
    }
}
