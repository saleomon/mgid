package pages.cab.advertisingAgencies.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class AdvertisingAgenciesLocators {
    public static final SelenideElement REGISTRATION_HISTORY_ICON = $(".link-registration-history");
    public static final SelenideElement REGISTRATION_HISTORY_DIALOG = $("#link-registration-history-dialog");
    public static final SelenideElement SUBNET_SELECT = $("#subnet");
    public static final SelenideElement MSM_SELECT = $("#mcm_aa");
    public static final SelenideElement FILTER_SUBMIT_BUTTON = $("#btnsubmit");
    public static final ElementsCollection AGENCY_FIELD_IDS = $$(".advertising_rows");
    public static final SelenideElement MSM_AA_ICON = $("img[src*='mcmAaFlag.png']");
    public static final ElementsCollection MSM_AA_ICONS = $$("img[src*='mcmAaFlag.png']");
    public static final SelenideElement ADD_BONUS_CONDITIONS_ICON = $x("//button[not(contains(@class,'hidden'))]/img[contains(@alt, 'Add')]");
    public static final SelenideElement EDIT_BONUS_CONDITIONS_ICON = $x("//button[not(contains(@class,'hidden'))]/img[contains(@alt, 'Edit')]");
    public static final SelenideElement BONUS_CONDITIONS_HISTORY_ICON = $("a [src*='personal_loan.png']");
    public static final SelenideElement AA_BONUS_CONDITION_ICON = $(".individual_bonus_condition");

    //"Add individual bonuses conditions" popup
    public static final SelenideElement AA_LOGIN = $("#raLogin");
    public static final SelenideElement AA_CURRENCY = $("#raCurrency");
    public static final SelenideElement SPENT_FIRST_INPUT = $("#spent_0");
    public static final SelenideElement BONUS_FIRST_INPUT = $("#percent_0");
    public static final SelenideElement SPENT_SECOND_INPUT = $("#spent_1");
    public static final SelenideElement BONUS_SECOND_INPUT = $("#percent_1");
    public static final SelenideElement SPENT_THIRD_INPUT = $("#spent_2");
    public static final SelenideElement BONUS_THIRD_INPUT = $("#percent_2");
    public static final SelenideElement ADD_RATE_BUTTON = $(":is(.add-rate-btn):not(.add-rate-btn[style='display: none;'])");
    public static final SelenideElement REMOVE_RATE_BUTTON = $(":is(.remove-rate-btn):not(.remove-rate-btn[style='display: none;'])");
    public static final SelenideElement BONUS_ACCOUNT_RADIOBUTTON = $("#paymentMethodBonus");
    public static final SelenideElement EXTERNAL_ACCOUNT_RADIOBUTTON = $("#paymentMethodExternal");
    public static final SelenideElement COMMENT_TEXT_AREA = $(".cab-dialog-textarea");
    public static final SelenideElement SUBMIT_BUTTON = $x("//div[5]//button[1]/span");
    public static final SelenideElement CANCEL_BUTTON = $x("//div[5]//button[2]/span");
    public static final SelenideElement DISABLED_BUTTON = $x("//div[5]//button[3]/span");
    public static final SelenideElement SUBMIT_DISABLE_FORM_BUTTON = $x("//div[7]//button[1]/span");
    public static final SelenideElement WARNING_MESSAGE = $(".cab-dialog-warning-message");
    public static final SelenideElement DISABLED_HINT_BUTTON = $(".disable-btn-hint");
    public static final SelenideElement ERROR_MESSAGE = $(".cab-dialog-error");

}
