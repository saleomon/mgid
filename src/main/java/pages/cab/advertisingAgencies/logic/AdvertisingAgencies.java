package pages.cab.advertisingAgencies.logic;

import com.codeborne.selenide.Condition;
import core.base.HelpersInit;
import pages.cab.advertisingAgencies.helper.AdvertisingAgenciesHelper;

import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.advertisingAgencies.locators.AdvertisingAgenciesLocators.*;

public class AdvertisingAgencies {

    private final HelpersInit helpersInit;
    private final AdvertisingAgenciesHelper agenciesHelper;

    public AdvertisingAgencies(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        agenciesHelper = new AdvertisingAgenciesHelper();
    }

    /**
     * check show link registration history pop-up
     */
    public boolean checkLinkRegistrationHistory() {
        agenciesHelper.registrationHistoryIconClick();
        return agenciesHelper.isShowLinkRegistrationHistoryDialog();
    }

    public void blockUnblockAccessToCab(String action) {
        String currentState = action.equals("block") ? "unblock" : "block";
        $("[src*= 'AA_" + currentState + "ed.png']").click();
        waitForAjax();
    }

    public boolean checkDisplayingIconForAccess(String action) {
        return $("[src*= 'AA_" + action + "ed.png']").isDisplayed();
    }

    /**
     * choose 'Subnet' filter
     */
    public AdvertisingAgencies chooseSubnetFilter(String subnet) {
        helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(SUBNET_SELECT, subnet));
        return this;
    }

    /**
     * choose 'MCM' filter
     */
    public AdvertisingAgencies chooseMcmFilter(String mcm) {
        helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(MSM_SELECT, mcm));
        return this;
    }

    /**
     * click 'Filter' button
     */
    public void clickFilterButton() {
        FILTER_SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * get amount of advertiser agencies at list
     */
    public int getAmountDisplayedAgencies() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AGENCY_FIELD_IDS.size());
    }

    /**
     * check displayed MCM Agency icon
     */
    public boolean checkMcmAgencyIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MSM_AA_ICON.isDisplayed());
    }

    /**
     * get amount of MCM Agency icons at list
     */
    public int getAmountMcmAgencyIcons() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MSM_AA_ICONS.size());
    }

    /**
     * check displayed "Add individual bonus conditions" icon
     */
    public boolean checkAddBonusIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_BONUS_CONDITIONS_ICON.isDisplayed());
    }

    /**
     * check displayed "Edit individual bonus conditions" icon
     */
    public boolean checkEditBonusIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(EDIT_BONUS_CONDITIONS_ICON.isDisplayed());
    }

    /**
     * check displayed "Individual bonus conditions history" icon
     */
    public boolean checkHistoryBonusIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BONUS_CONDITIONS_HISTORY_ICON.isDisplayed());
    }

    /**
     * check displayed "AA has an individual bonus conditions" icon
     */
    public boolean checkAgencyIndividualBonusIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AA_BONUS_CONDITION_ICON.isDisplayed());
    }

    /**
     * open "Add individual bonus conditions" popup
     */
    public void openAddBonusConditionPopup() {
        if (ADD_BONUS_CONDITIONS_ICON.isDisplayed()) {
            ADD_BONUS_CONDITIONS_ICON.click();
        }
        else if (EDIT_BONUS_CONDITIONS_ICON.isDisplayed())
            EDIT_BONUS_CONDITIONS_ICON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check display "Submit" button for individual conditions popup
     */
    public boolean isDisplayedSubmitButton() {
        return SUBMIT_BUTTON.isDisplayed();
    }

    /**
     * check displayed "Cancel" button for individual conditions popup
     */
    public boolean isDisplayedCancelButton() {
        return CANCEL_BUTTON.isDisplayed();
    }

    /**
     * check displayed "Disable" button for individual conditions popup
     */
    public boolean isDisplayedDisableButton() {
        return DISABLED_BUTTON.isDisplayed();
    }

    /**
     * get warning message for individual conditions popup
     */
    public String getWarningMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WARNING_MESSAGE.text());
    }

    /**
     * get tooltip text for individual conditions popup
     */
    public String getDisableTooltipText() {
        DISABLED_HINT_BUTTON.hover();
        return helpersInit.getBaseHelper().getTextAndWriteLog(DISABLED_HINT_BUTTON.getAttribute("title"));
    }

    public String getUserAALogin() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AA_LOGIN.getText());
    }

    public String getUserAACurrency() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AA_CURRENCY.getText());
    }

    public String getSpentFirstGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SPENT_FIRST_INPUT.getValue());
    }

    public String getBonusFirstGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BONUS_FIRST_INPUT.getValue());
    }

    public String getSpentSecondGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SPENT_SECOND_INPUT.getValue());
    }

    public String getBonusSecondGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BONUS_SECOND_INPUT.getValue());
    }

    public String getSpentThirdGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SPENT_THIRD_INPUT.getValue());
    }

    public String getBonusThirdGrade() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BONUS_THIRD_INPUT.getValue());
    }

    public String getCommentText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMMENT_TEXT_AREA.getText());
    }

    public boolean checkPaymentBonusAccountState(boolean state) {
        return checkIsElementSelected(state, BONUS_ACCOUNT_RADIOBUTTON);
    }

    public boolean checkPaymentExternalAccountState(boolean state) {
        return checkIsElementSelected(state, EXTERNAL_ACCOUNT_RADIOBUTTON);
    }

    public AdvertisingAgencies addRate() {
        ADD_RATE_BUTTON.click();
        checkErrors();
        return this;
    }

    public AdvertisingAgencies removeRate() {
        REMOVE_RATE_BUTTON.click();
        checkErrors();
        return this;
    }

    public AdvertisingAgencies setSpentFirstGrade(String spent_0) {
        clearAndSetValue(SPENT_FIRST_INPUT, spent_0);
        helpersInit.getBaseHelper().getTextAndWriteLog(spent_0);
        return this;
    }

    public AdvertisingAgencies setBonusFirstGrade(String percent_0) {
        clearAndSetValue(BONUS_FIRST_INPUT, percent_0);
        helpersInit.getBaseHelper().getTextAndWriteLog(percent_0);
        return this;
    }

    public AdvertisingAgencies setSpentSecondGrade(String spent_1) {
        clearAndSetValue(SPENT_SECOND_INPUT, spent_1);
        helpersInit.getBaseHelper().getTextAndWriteLog(spent_1);
        return this;
    }

    public AdvertisingAgencies setBonusSecondGrade(String percent_1) {
        clearAndSetValue(BONUS_SECOND_INPUT, percent_1);
        helpersInit.getBaseHelper().getTextAndWriteLog(percent_1);
        return this;
    }

    public AdvertisingAgencies setSpentThirdGrade(String spent_2) {
        clearAndSetValue(SPENT_THIRD_INPUT, spent_2);
        helpersInit.getBaseHelper().getTextAndWriteLog(spent_2);
        return this;
    }

    public AdvertisingAgencies setBonusThirdGrade(String percent_2) {
        clearAndSetValue(BONUS_THIRD_INPUT, percent_2);
        helpersInit.getBaseHelper().getTextAndWriteLog(percent_2);
        return this;
    }

    public AdvertisingAgencies setPaymentBonusAccount() {
        BONUS_ACCOUNT_RADIOBUTTON.selectRadio("bonus");
        return this;
    }

    public AdvertisingAgencies setPaymentExternalAccount() {
        EXTERNAL_ACCOUNT_RADIOBUTTON.selectRadio("external");
        return this;
    }

    public AdvertisingAgencies setComment(String comment) {
        clearAndSetValue(COMMENT_TEXT_AREA, comment);
        helpersInit.getBaseHelper().getTextAndWriteLog(comment);
        return this;
    }

    public void clickSubmitButton() {
        SUBMIT_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public void clickDisableFormSubmitButton() {
        SUBMIT_DISABLE_FORM_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public void clickCancelButton() {
        CANCEL_BUTTON.click();
        checkErrors();
    }

    public AdvertisingAgencies clickDisableButton() {
        DISABLED_BUTTON.click();
        checkErrors();
        return this;
    }

    public String getErrorMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ERROR_MESSAGE.text());
    }

    public boolean spentSecondGradeIsDisplayed() {
        return SPENT_SECOND_INPUT.isDisplayed();
    }

    public boolean spentThirdGradeIsDisplayed() {
        return SPENT_THIRD_INPUT.isDisplayed();
    }

}
