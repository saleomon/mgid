package pages.cab.administration.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class UsersLocators {

    public static final SelenideElement NEW_USER_LOGIN_INPUT = $("#login");
    public static final SelenideElement CHECK_LOGIN_BUTTON = $("#checkButton");
    public static final SelenideElement VALIDATION_MESSAGE = $("#valid-message");
    public static final SelenideElement CREATE_USER_ACCOUNT_BUTTON = $("#submitNewLogin");
    public static final SelenideElement SAVE_BUTTON = $x(".//input[@value='Save']");
    public static final SelenideElement USER_STATUS_SELECT = $("#status");
    public static final SelenideElement USER_EMAIL_INPUT = $("#email");
    public static final SelenideElement USER_FULL_NAME_INPUT = $("#fullname");
    public static final SelenideElement ADVERTISING_AGENCY_SUBNET_SELECT = $("#advertising_agency_subnet");
    public static final SelenideElement ACCESS_TO_ADMIN_PANEL = $("#is_blocked");
    public static final SelenideElement ADVERTISING_AGENCY_MANAGER_SELECT = $("#accompanying_manager");
    public static final SelenideElement ADVERTISING_AGENCY_TYPE_SELECT = $("#ra_type");
    public static final SelenideElement ADVERTISING_AGENCY_CURRENCY_SELECT = $("#ra_currency");
    public static final SelenideElement BONUS_SALES_GEO_SELECT = $("[id='geo_team']");
    public static final SelenideElement SEARCH_FIELD_BONUS_SALES_GEO = $("[class*='search-input']");
    public static final String GEO_TEAM_SELECTED_VALUES = "[class*='fancytree-selected']:not([class*='expanded']) [class*=title]";
    public static final ElementsCollection GEO_TEAM_SEARCH_RESULT = $$("[class*='fancytree-match'] [class*=title]");
    public static final SelenideElement MCM_AA_CHECKBOX = $("#mcm_aa");
    public static final SelenideElement MCM_AA_REBATE_PERCENT_INPUT = $("#mcm_rebate_percent");
    public static final SelenideElement MCM_AA_ADVERTISER_INPUT = $("#mcm_advertiser");
}
