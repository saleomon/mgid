package pages.cab.administration.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class BlacklistDfpLocators {

    public static final SelenideElement DOMAIN_INPUT = $("#domainDfpInput");
    public static final SelenideElement ADD_BUTTON = $("#addDomainButton");
    public static final SelenideElement CONFIRM_DELETE_OK = $x(".//div[div[@id='js-confirm-dialog']]//*[text()='Ok']");
}
