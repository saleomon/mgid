package pages.cab.administration.logic;

import com.codeborne.selenide.CollectionCondition;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;
import static pages.cab.administration.locators.UsersLocators.*;

public class Users {
    private final HelpersInit helpersInit;
    private final Logger log;

    private String advertisingAgency;
    private String advertisingAgencySubnet;
    private String accessToAdminPanel;

    private final static int LEGAL_ENTITIES_ID_MGID = 14;
    private final static int LEGAL_ENTITIES_ID_ADSKEEPER = 9;


    public Users(HelpersInit helpersInit, Logger log){
        this.log = log;
        this.helpersInit = helpersInit;
    }

    public Users setAdvertiserAgencyManager(String managerName){
        advertisingAgency = helpersInit.getBaseHelper().selectCustomValueReturnText(ADVERTISING_AGENCY_MANAGER_SELECT, managerName);
        return this;
    }

    public Users setAdvertiserAgencySubnet(){
        advertisingAgencySubnet = helpersInit.getBaseHelper().selectRandomValue(ADVERTISING_AGENCY_SUBNET_SELECT);
        return this;
    }

    public Users setAccessToAdminPanel(boolean isEnable){
        accessToAdminPanel = isEnable ? "0" : "1";
        ACCESS_TO_ADMIN_PANEL.selectOptionByValue(accessToAdminPanel);
        return this;
    }

    public Users disableRole(int role){
        markUnMarkCheckbox(false, $("[id='role_" + role + "']"));
        return this;
    }

    public Users enableRole(int role){
        markUnMarkCheckbox(true, $("[id='role_" + role + "']"));
        return this;
    }

    public Users useMcmAgency(boolean state){
        markUnMarkCheckbox(state, MCM_AA_CHECKBOX);
        return this;
    }

    public Users setMcmRebatePercent(String rebatePercent) {
        clearAndSetValue(MCM_AA_REBATE_PERCENT_INPUT, rebatePercent);
        helpersInit.getBaseHelper().getTextAndWriteLog(rebatePercent);
        return this;
    }

    public Users setMcmAdvertiserName(String advertiserName) {
        clearAndSetValue(MCM_AA_ADVERTISER_INPUT, advertiserName);
        helpersInit.getBaseHelper().getTextAndWriteLog(advertiserName);
        return this;
    }

    public String getMcmRebatePercent() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MCM_AA_REBATE_PERCENT_INPUT.getValue());
    }

    public String getMcmAdvertiserName() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MCM_AA_ADVERTISER_INPUT.getValue());
    }

    public boolean checkStateMcmAgencyCheckbox(boolean state){
        return checkIsCheckboxSelected(state, MCM_AA_CHECKBOX);
    }

    public boolean checkMcmAgencyCheckboxDisableState(boolean state){
        return checkIsElementDisabled(state, MCM_AA_CHECKBOX);
    }

    public boolean checkMcmAdvertiserNameInputDisableState(boolean state){
        return checkIsElementDisabled(state, MCM_AA_ADVERTISER_INPUT);
    }

    public boolean checkIsEnableAccessToAdminPanel(boolean isEnable){
        accessToAdminPanel = isEnable ? "0" : "1";
        return helpersInit.getBaseHelper().checkDatasetEquals(ACCESS_TO_ADMIN_PANEL.getSelectedValue(), accessToAdminPanel);
    }

    public boolean checkDisablingSelectAaSubnet(){
        return ADVERTISING_AGENCY_SUBNET_SELECT.is(disabled);
    }

    public int getSubnetEntityId(){
        return switch (advertisingAgencySubnet) {
            case "Mgid" -> LEGAL_ENTITIES_ID_MGID;
            case "Adskeeper" -> LEGAL_ENTITIES_ID_ADSKEEPER;
            default -> LEGAL_ENTITIES_ID_MGID;
        };
    }

    public void saveUserSettings(){
        SAVE_BUTTON.scrollTo().shouldBe(visible).click();
        waitForAjax();
    }

    public void chooseBonusGeoTeam(String val){
        BONUS_SALES_GEO_SELECT.selectOptionByValue(val);
    }

    public void searchFieldOfBonusGeoTeam(String val){
        SEARCH_FIELD_BONUS_SALES_GEO.sendKeys(val);
    }

    public boolean checkSelectedGeoTeamOptions(String...countries){
        List<String> selectedValuesInterface = new ArrayList<>();
        SAVE_BUTTON.scrollTo();
        $$(GEO_TEAM_SELECTED_VALUES).shouldBe(CollectionCondition.sizeGreaterThan(3));

        for(int i = 0; i < $$(GEO_TEAM_SELECTED_VALUES).size(); i++) {
            $$(GEO_TEAM_SELECTED_VALUES).get(i).scrollTo();
            sleep(500);
            selectedValuesInterface.add($$(GEO_TEAM_SELECTED_VALUES).get(i).text());
        }
//        for (SelenideElement elem: $$(GEO_TEAM_SELECTED_VALUES)) {
//            elem.scrollTo();
//            if(elem.text().length() < 1) {
//                selectedValuesInterface.add(elem.text());
//            }
//            selectedValuesInterface.add(elem.text());
//        }
        List<String> selectedValuesSaved = Arrays.asList(countries);
        Collections.sort(selectedValuesSaved);
        Collections.sort(selectedValuesInterface);
        selectedValuesInterface.forEach(el-> log.info("selectedValuesInterface - " + el));
        selectedValuesSaved.forEach(el-> log.info("selectedValuesSaved - " + el));

        return selectedValuesInterface.equals(selectedValuesSaved);
    }

    public boolean checkMatchingSearchResultForGeoTeam(String pattern){
        List<String> searchResult = GEO_TEAM_SEARCH_RESULT.texts();
        searchResult.forEach(el-> log.info("searchResult - " + el));
        return searchResult.size() > 0 && searchResult.stream().allMatch(el->el.toLowerCase().contains(pattern));
    }

    public Users setNewUserLogin(String userLogin) {
        clearAndSetValue(NEW_USER_LOGIN_INPUT, userLogin);
        helpersInit.getBaseHelper().getTextAndWriteLog(userLogin);
        return this;
    }

    public void checkNewUserLogin() {
        CHECK_LOGIN_BUTTON.scrollTo().shouldBe(visible).click();
        waitForAjax();
    }

    public String getDisplayedMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VALIDATION_MESSAGE.text());
    }

    public void createNewUserAccount() {
        CREATE_USER_ACCOUNT_BUTTON.scrollTo().shouldBe(visible).click();
        waitForAjax();
    }

    public Users setEmail(String userEmail) {
        clearAndSetValue(USER_EMAIL_INPUT, userEmail);
        helpersInit.getBaseHelper().getTextAndWriteLog(userEmail);
        return this;
    }

    public Users setFullName(String fullName) {
        clearAndSetValue(USER_FULL_NAME_INPUT, fullName);
        helpersInit.getBaseHelper().getTextAndWriteLog(fullName);
        return this;
    }

    public Users setUserActiveStatus(boolean status) {
        String userStatus = status ? "1" : "3";
        USER_STATUS_SELECT.selectOptionByValue(userStatus);
        return this;
    }

    public Users setAdvertiserAgencyType() {
        helpersInit.getBaseHelper().selectRandomValue(ADVERTISING_AGENCY_TYPE_SELECT);
        return this;
    }

    public Users setAdvertiserAgencyCurrency(String currency) {
        helpersInit.getBaseHelper().selectCustomText(ADVERTISING_AGENCY_CURRENCY_SELECT, currency);
        return this;
    }

    public String getUserRaEmailInEdit() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(USER_EMAIL_INPUT.getValue());
    }

}
