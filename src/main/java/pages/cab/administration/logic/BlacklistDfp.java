package pages.cab.administration.logic;

import core.base.HelpersInit;
import pages.cab.administration.helper.BlacklistDfpHelper;

public class BlacklistDfp {

    private final HelpersInit helpersInit;
    private final BlacklistDfpHelper blacklistDfpHelper;

    private String domain;

    public BlacklistDfp(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        blacklistDfpHelper = new BlacklistDfpHelper(helpersInit);
    }

    public String getDomain() {
        return domain;
    }

    public BlacklistDfp setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    /**
     * add new dfp domain
     */
    public boolean addDfpDomain() {
        domain = blacklistDfpHelper.setDomain(domain);
        blacklistDfpHelper.clickAddDomain();
        return helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * delete dfp domain
     */
    public boolean deleteDfpDomain() {
        blacklistDfpHelper.deleteDomain(domain);
        return helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * get domain value from input after fill his
     */
    public String getDomainValueFromInput() {
        blacklistDfpHelper.setDomain(domain);
        return blacklistDfpHelper.getDomainValueFromInput();
    }
}
