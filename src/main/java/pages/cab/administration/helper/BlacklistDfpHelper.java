package pages.cab.administration.helper;

import core.base.HelpersInit;
import core.helpers.BaseHelper;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.administration.locators.BlacklistDfpLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class BlacklistDfpHelper {

    private final HelpersInit helpersInit;

    public BlacklistDfpHelper(HelpersInit helpersInit) {

        this.helpersInit = helpersInit;
    }

    /**
     * set 'DFP' domain
     */
    public String setDomain(String domain) {
        String dfpDomain = domain == null ?
                "testdfpdomain" + BaseHelper.getRandomWord(2) + ".com" :
                domain;

        clearAndSetValue(DOMAIN_INPUT.shouldBe(visible), dfpDomain);
        return dfpDomain;
    }

    /**
     * click button 'Add domain'
     */
    public void clickAddDomain() {
        ADD_BUTTON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * delete DFP domain
     */
    public void deleteDomain(String domain) {
        $("[class*='remove'][value='" + domain + "'] i").click();
        CONFIRM_DELETE_OK.shouldBe(visible).click();
        waitForAjax();
    }

    /**
     * get domain value from input
     */
    public String getDomainValueFromInput() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DOMAIN_INPUT.val());
    }
}
