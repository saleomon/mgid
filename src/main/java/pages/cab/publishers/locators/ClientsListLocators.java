package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ClientsListLocators {

    public static final SelenideElement BLOCK_ADS_ICON = $(".imgBlockAds");

    //Actions
    public static final SelenideElement AUTO_PAID_ICON = $("[id*='ap_img_']");
    public static final SelenideElement TRANSFER_TO_MG_WALLET_ICON = $x(".//img[contains(@src, 'assept.png')]");
    public static final SelenideElement MONEY_WIDHDARW_ICON = $x(".//img[contains(@src, 'icon-wages.png')]");
    public static final SelenideElement ADJUSTMENT_WAGES_ICON = $x(".//img[contains(@src, 'correct_unconf_wages.png')]");
    public static final SelenideElement AUTO_REPORT_ICON = $(".auto-reports-icon");

    //Finances
    public static final SelenideElement CLIENT_MG_WALLET_BALANCE = $x(".//td[contains(@id, 'mg_wallet_')]");
    public static final SelenideElement CLIENT_MG_WALLET_AMOUNT = $x(".//tr[@class='c_wages'][4]/td[4]");
    public static  final SelenideElement UNCONFIRMED_WAGES_VALUE = $x(".//tr[@class='c_wages'][6]/td[2]");
    public static final SelenideElement CLIENT_PAYOUTS_AMOUNT = $x(".//tr[@class='c_wages'][3]/td[4]");

    //'Transfer to MG-Wallet' popup
    public static final SelenideElement TRANSFER_TO_MG_WALLET_POPUP = $("#paymentDialog");
    public static final SelenideElement PAYOUT_AMOUNT_INPUT = $("#payoutAmount");
    public static final SelenideElement PAYOUT_COMMENT_INPUT = $("#commentPurses");
    public static final SelenideElement ADD_PAYMENT_BUTTON = $("#paymentButton");
    public static final SelenideElement IMMEDIATE_REQUEST_APPROVAL_CHECKBOX = $("#is_approved");
    public static final SelenideElement ADD_PAYMENT_POPUP_ERROR = $x(".//label[@class='validation-error']");

    //'Add payment' popup
    public static final SelenideElement ADD_PAYMENT_POPUP = $("#paymentDialog");
    public static final SelenideElement ADD_PAYMENT_PURSE_SELECT = $("#purse");

    //Payouts settings popup
    public static final SelenideElement STANDARD_PAYOUT_SCHEME_RADIO = $("#scheme-standart");
    public static final SelenideElement AUTO_PAID_PURSES_SELECT = $("[name='purse']");
    public static final SelenideElement AUTO_PAID_PERIOD_SELECT = $("[name='period']");
    public static final SelenideElement AUTO_PAID_SAVE_BUTTON = $("#save");
    public static ElementsCollection LIST_PERIOD_DAYS_INPUTS = $$("#fieldset-period-days input");
    public static final SelenideElement AUTO_PAID_INTERVAL_INPUT = $("#interval");
    public static final SelenideElement AUTO_PAID_PAYOUT_AMOUNT_INPUT = $("#payout-amount");
    public static final SelenideElement AUTO_PAID_DELAY_TYPE_SELECT = $("[name='delay-type']");
    public static final SelenideElement AUTO_PAID_DELAY_INPUT = $("#delay");
    public static final SelenideElement AUTO_PAID_AUTOSTANDART_SCHEME_RADIO = $("#scheme-autostandart");
    public static final SelenideElement AUTO_PAID_MANUAL_SCHEME_RADIO = $("#scheme-manual");
    public static final SelenideElement AUTO_PAID_PERIOD_DAYS_RADIO = $("#period-days");
    public static final SelenideElement AUTO_PAID_PERIOD_INTERVAL_RADIO = $("#period-interval");
    public static final SelenideElement AUTO_PAID_PAYOUT_AMOUNT_RADIO = $("#use-payout-amount");
    public static final SelenideElement AUTO_PAID_STANDART_SCHEME_RADIO = $("#scheme-standart");

    //Попап корректировки начисленного заработка
    public static  final SelenideElement CORRECTION_WAGES_POPUP = $x(".//div[@id='correctionDialog']");
    public static  final SelenideElement CORRECTION_POPUP_AMOUNT_INPUT = $("#correction");
    public static  final SelenideElement CORRECTION_POPUP_COMMENT_INPUT = $("#comment");
    public static  final SelenideElement CORRECTION_POPUP_SUBMIT_BUTTON = $("#submitCorrection");
    public static  final SelenideElement CORRECTION_POPUP_ERROR = $x("//div[@id = 'correctionMessage']");

    // others
    public static  final SelenideElement CLIENTS_CHECKBOX = $("[id*='clients'][type=checkbox]");


    //mass actions
    public static  final SelenideElement MASS_ACTIONS = $("[id='group-actions']");
    public static  final SelenideElement SELECT_ALL_OPTION = $("[id='selectAllItems']");
    public static  final SelenideElement CURATOR_SELECT = $("[id='curatorSelectForMass']");
    public static  final SelenideElement SUBMIT_MASS = $("[id='group-submit']");
    public static  final ElementsCollection MANAGER_LABELS = $$("[class='changeCurator']");

    //auto report modal
    public static  final SelenideElement AUTO_REPORT_POPUP = $("div#auto-report-modal");
    public static  final SelenideElement AUTO_REPORT_TIME_SELECT = $("select[name=time]");
    public static  final ElementsCollection AUTO_REPORT_PERIODICITY_CHECKBOXES = $$(".auto-report__periodicity-fieldset input");
    public static  final ElementsCollection AUTO_REPORT_PERIODICITY_LABELS = $$x(".//fieldset[@class='auto-report__periodicity-fieldset']//label");
    public static  final SelenideElement AUTO_REPORT_EMAIL_INPUT = $("input[type=email]");
    public static  final SelenideElement AUTO_REPORT_ADD_EMAIL_BUTTON = $(".auto-reports__add-email");
    public static  final SelenideElement AUTO_REPORT_EMAIL_SECOND_INPUT = $(".email-input-2");
    public static  final String AUTO_REPORT_COLUMNS_MULTISELECT = ".//div[@id='auto-reports-accordion']//div[contains(text(), '%s')]";
    public static  final String AUTO_REPORT_STATS_BY_SITES_CHECKBOX = ".//div[contains(text(), '%s')]/..//span";
    public static  final String AUTO_REPORT_COLUMNS_CHECKBOXES = "/../../following-sibling::div[1]//input[@type='checkbox']";
    public static  final SelenideElement AUTO_REPORT_CANCEL_BUTTON = $x("//div[@class='ui-dialog-buttonset']//span[text()='Cancel']");
    public static  final SelenideElement AUTO_REPORT_TURNOFF_BUTTON = $x("//div[@class='ui-dialog-buttonset']//span[text()='Turn off auto reports']");
    public static  final SelenideElement AUTO_REPORT_SUBMIT_BUTTON = $x("//div[@class='ui-dialog-buttonset']//span[text()='Submit']");
    public static  final SelenideElement AUTO_REPORT_CLOSE_BUTTON = $("#ui-dialog-title-auto-report-modal+a[href='#']");
}
