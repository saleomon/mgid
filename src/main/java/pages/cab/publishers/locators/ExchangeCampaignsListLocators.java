package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ExchangeCampaignsListLocators {
    public static final SelenideElement ID_FIELD = $("[id*=ork_id_]");
    public static final ElementsCollection MANAGER_LABEL = $$("[id^='oc_kurator']");

    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
}
