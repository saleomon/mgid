package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class WebsitesAddLocators {

    public static final SelenideElement DOMAIN_FIELD = $("#domainName");
    public static final SelenideElement DOMAIN_INPUT = $("#url");
    public static final SelenideElement MIRROR_INPUT = $("#mirrorDomains");
    public static final SelenideElement CATEGORY_SELECT = $("#category_select_dropdown");
    public static final SelenideElement LANGUAGE_SELECT = $("#site_lang");
    public static final SelenideElement PUSH_PROVIDERS_SELECT = $("#pushProvider");
    public static final SelenideElement DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT = $("#sourcesId");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_INPUT = $("#newDomainSource");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME = $("#lowVolume");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_SUBMIT = $x(".//div[div[@id='sourcesIdPopup']]//span[contains(text(), 'Submit')]");
    public static final SelenideElement LOW_VOLUME_SOURCE_CHECKBOX = $("#lowVolumeSource");
    public static final SelenideElement TIER_SELECT = $("#tierId");
    public static final SelenideElement SOURCE_TYPE_SELECT = $("#sourcesTypesId");
    public static final SelenideElement NATIONALITY_SELECT = $("#nationality");
    public static final SelenideElement LANGUAGE_PRIORITY_SELECT = $("#languages_in_priority");
    public static final SelenideElement DEFAULT_WIDGET_FOR_COMPOSITE_SELECT = $("#default_section");
    public static final SelenideElement COMMENT_INPUT = $("#comments");
    public static final SelenideElement USE_SUBDOMAINS_IN_NEWS_LINKS_CHECKBOX = $("#add_news_on_subdomains");
    public static final SelenideElement OPPORTUNITY_TO_PROMOTE_NEWS_CHECKBOX = $("#canPromoteNews");
    public static final SelenideElement ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX = $("#isChangeTypesToNewWidgets");
    public static final String          BUNDLE_ID_ELEMENT = "bundlesIds";
    public static final SelenideElement SAVE_CREATE_BUTTON = $("#save");
    public static final SelenideElement SAVE_EDIT_BUTTON = $("#submit");
    public static final SelenideElement COPPA_CHECKBOX = $("#coppa");


    //cooperation types block
    public static final SelenideElement PANEL_RIGHT = $("#cooperationPriorities_right");

    //category filter
    public static final SelenideElement CATEGORIES_FILTER_RADIOBUTTONS = $(By.name("useFilterCategories"));
    public static final String CATEGORY_FILTER_TITLES = ".//div[@id='categoriesBox']//li[span[span[@class='fancytree-title']] and not(ul)]//span[@class='fancytree-title']";
    public static final String CATEGORY_FILTER_TITLES_AFTER_SEARCH = ".//li[span[span[@class='fancytree-title'] and not(contains(@class, 'fancytree-hide'))] and not(ul)]//span[@class='fancytree-title']";
    public static final SelenideElement PRODUCT_PROMOTIONS_CHECKBOX =  $x(".//span[span[text()='Product promotions']]/*[@class='fancytree-checkbox']");
    public static final SelenideElement CONTENT_PROMOTIONS_CHECKBOX =  $x(".//span[span[text()='Content promotions']]/*[@class='fancytree-checkbox']");
    public static final SelenideElement CATEGORY_FILTER_SEARCH_INPUT = $("#catsSearch");
    public static final ElementsCollection CATEGORIES_FILTER_VALUES = $$(byName("useFilterCategories"));
    public static final String CATEGORY_FILTER_ELEMENTS = "li[id^='ft_product-']>span:not(.fancytree-has-children)";


}
