package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class NewsAddLocators {

    public static final SelenideElement CATEGORY_SELECT = $("#cat_dropdown_dropdown");
    public static final SelenideElement LINK_INPUT = $("#url");
    public static final SelenideElement TITLE_INPUT = $("#title");
    public static final SelenideElement DESCRIPTION_INPUT = $("#advert_text");
    public static final SelenideElement IMAGE_INPUT = $("#image_link");
    public static final SelenideElement IMAGE_INPUT_INLINE = $("[id='imageFile']");
    public static final SelenideElement SAVE_IMAGE_INLINE = $("#saveImgBtn");
    public static final SelenideElement IMAGE_INPUT_FIELD_INLINE = $("#imageLink");
    public static final SelenideElement LIFETIME_SELECT = $("#livetime");
    public static final SelenideElement AD_TYPES_SELECT = $("#ad_types");
    public static final SelenideElement POLITICAL_PREFERENCES_SELECT = $("#content_group");
    public static final SelenideElement IMAGE_LABEL = $("#image_file-label");
    public static final SelenideElement SAVE_BUTTON = $(".save");
    public static final SelenideElement CURATOR_LABEL = $(".curator");
    public static final SelenideElement IS_ORIGINAL_TITLE_CHECKBOX = $("[id='isOriginalTitle']");
    public static final SelenideElement ORIGINAL_TITLE_FILTER = $("[id='isOriginalTitle']");
    public static final ElementsCollection NEWS_INFO_BLOCK = $$("[class='info']");
    public static final ElementsCollection RESET_DATA_BUTTONS = $$("[class*=clear-btn-visible]");
    public static final SelenideElement IMAGE_POPUP_LOCATOR = $("[class='img']");
    public static final SelenideElement CLEAR_FIELD_BUTTON = $("[class*=clear-btn-visible]");

}
