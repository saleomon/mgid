package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class NewsModerationLocators {

    ///////////////////////////// MASS actions locators /////////////////////////////////////
    //general
    public static final SelenideElement MASS_ACTION_SUBMIT = $("#group-submit");
    public static final SelenideElement MASS_ACTIONS_SELECT = $("#group-actions");
    public static final SelenideElement SELECT_ALL_CHECKBOXES_LINK = $("[href*='.checkAll']");
    public static final SelenideElement UNSELECT_ALL_CHECKBOXES_LINK = $("[href*='.uncheckAll']");
    public static final String MASS_CHECKBOX = ".nk-check";
    public static final ElementsCollection ORIGINAL_TITLE_ICON = $$("img[src*='tag-16']");
    public static final ElementsCollection NOT_ORIGINAL_TITLE_ICON = $$("img[src*='literacy_level_none']");
    public static final SelenideElement ORIGINAL_TITLE_FILTER = $("[id='isOriginalTitle']");
    public static final ElementsCollection NEWS_INFO_BLOCK = $$("[class='info']");

    public static final ElementsCollection NEWS_ID_LABELS = $$("[title='To news'] [class=value]");

    //adType(adType)
    public static final SelenideElement MASS_AD_TYPE_SELECT = $("#news_ad_type");

    //category(changeCategory)
    public static final SelenideElement MASS_CATEGORY_SELECT = $("#mass_category");

    //lifetime
    public static final SelenideElement MASS_LIFETIME_SELECT = $("#massLivetime");

    //landing
    public static final SelenideElement MASS_LANDING_SELECT = $("#news_landing_type");

    //political Preferences
    public static final SelenideElement MASS_POLITICAL_PREFERENCES_SELECT = $("#content_group_quarantine");

    //delete
    public static final SelenideElement MASS_DELETE_REASON_INPUT = $("#mass_delete_reason");

    //reject
    public static final SelenideElement MASS_REJECT_REASON_INPUT = $("#reject-reason");

    /////////////////////////// In-Line ////////////////////////////////////////////////////

    //adType
    public static final String INLINE_AD_TYPE = "[id*=typeTeaser]";
    public static final SelenideElement INLINE_AD_TYPE_LINK = $(".teaser_type_settings");
    public static final SelenideElement INLINE_AD_TYPE_SELECT = $("#js-types-select");
    public static final SelenideElement INLINE_AD_TYPE_SUBMIT = $("#js-accept-type-button");

    //title
    public static final SelenideElement INLINE_TITLE_LABEL = $(".titleInline");
    public static final SelenideElement INLINE_TITLE_INPUT = $("[name=title-input]");
    public static final SelenideElement TITLE_ERROR_ICON = $(".titleInline+img[src*='error-16.png']");

    //description
    public static final SelenideElement INLINE_DESCRIPTION_LABEL = $(".descriptionInline");
    public static final SelenideElement INLINE_DESCRIPTION_INPUT = $("[name=description-input]");
    public static final SelenideElement DESCRIPTION_ERROR_ICON = $(".descriptionInline+img[src*='error-16.png']");

    //category
    public static final SelenideElement INLINE_CATEGORY_LABEL = $(".editableCategory");
    public static final SelenideElement INLINE_CATEGORY_SELECT = $(".dropdown-container");
    public static final SelenideElement INLINE_CATEGORY_SUBMIT = $x(".//div[*[contains(@id, 'edit-category-form_')]]//*[text()='Submit']");
    public static final String INLINE_CATEGORY = ".editableCategory";

    //lifetime
    public static final SelenideElement INLINE_LIFETIME_LABEL = $(".livetimeInline");
    public static final SelenideElement INLINE_LIFETIME_SELECT = $("[name=livetime-input]");
    public static final String INLINE_LIFETIME = ".livetimeInline";

    //landing
    public static final SelenideElement INLINE_LANDING_LABEL = $(".landingTypesInline");
    public static final SelenideElement INLINE_LANDING_SELECT = $("[name=landing_types-input]");
    public static final String INLINE_LANDING = ".landingTypesInline";

    //url
    public static final SelenideElement INLINE_URL_LABEL = $(".news_url");
    public static final SelenideElement INLINE_URL_INPUT = $("[name=url-input]");

    //image
    public static final SelenideElement INLINE_IMAGE_LABEL = $("img[id^='imgsq']");
    public static final SelenideElement INLINE_IMAGE_INPUT = $("#imageLink");
    public static final SelenideElement INLINE_IMAGE_CROP = $(".jcrop-tracker");
    public static final SelenideElement INLINE_IMAGE_FILE_LOCATOR = $("#imageFile");
    public static final SelenideElement INLINE_IMAGE_BUTTON = $("[id=saveImgBtn]");

    //POLITICAL PREFERENCES
    public static final String INLINE_POLITICAL_PREFERENCES = ".contentGroupInline";
    public static final String CONTENT_GROUP_ICON = "#contentGroup%s";

    // Political preferences popup
    public static final String CONTENT_GROUP_SELECT = "#contentGroup-input_%s";
    public static final SelenideElement SUBMIT_BUTTON = $(".ui-button-text");




    //////////////////////////////////// news list ////////////////////////////////////////
    public static final SelenideElement EMPTY_TABLE_CELL = $("table.base td");
    public static final SelenideElement NEWS_ID_LABEL = $(".news[data-id]");
    public static final String APPROVE_ICON = "[title=Approve] img";
    public static final String REJECT_ICON = "[title=Decline]";
    public static final String DELETE_ICON = "[id^='trash']";
    public static final SelenideElement REJECT_REASON_INPUT = $("#reasonInput");
    public static final SelenideElement REJECT_REASON_BUTTON = $x(".//div[*[@id='rejectContainer']]//*[text()='Decline']");
    public static final String DELETE_REASON_INPUT = "#descriptionBox%s";
    public static final SelenideElement DELETE_REASON_BUTTON = $("#btnApply");

    /////////////////////////////////// start-stop moderation //////////////////////////////
    public static final String START_MODERATION_ICON = ".moderation-start";
    public static final String STOP_MODERATION_ICON = ".moderation-stop";
    public static final String ACTIONS_BLOCK = "table.inner";
    public static final String MODERATION_PROCESS_STYLE = ".moderationProcess";
    public static final String MODERATION_USER_LABEL = ".in-work-by-user";
    public static final SelenideElement STOP_MODERATION_DIALOG = $(".stopModerationDialog h6");
    public static final SelenideElement STOP_MODERATION_DIALOG_CLOSE_BUTTON = $x(".//*[contains(@class, 'stopModerationDialog')]//*[text()='Yes']");

    //////////////////////////////// filters ///////////////////////////
    public static final SelenideElement FILTER_CAMPAIGN_ID_INPUT = $("#campaign_id");
    public static final SelenideElement FILTER_MODERATION_STATUS_SELECT = $("#moderationStatus");
    public static final SelenideElement FILTER_SUBMIT = $("#btnsubmit");
    public static final SelenideElement LINK_TO_RELATED_IMAGES = $("[id*='relatedImages'] a");
    public static final SelenideElement RELATED_IMAGES_FILTER = $("[id='related_images']");


}
