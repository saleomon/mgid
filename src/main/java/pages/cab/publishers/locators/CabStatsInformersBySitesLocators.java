package pages.cab.publishers.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CabStatsInformersBySitesLocators {

    public static final SelenideElement GRAPH_FIRST_ICON = $x(".//tr[td[contains(@class, 'actions')]][1]//*[@data-action='showChart' or @data-action='show-chart']");
    public static final SelenideElement CANVAS_FIRST_ELEMENT = $("#chartsDialog .chart-container:nth-child(1)>canvas");
}
