package pages.cab.publishers.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ClientsEditLocators {

    public static final SelenideElement FILTER_CURRENCY_SELECT = $("#client_currency");
    public static final SelenideElement FILTER_SUBMIT_BUTTON = $("#btnsubmit");

    public static final SelenideElement MG_WALLET_LABEL = $("[id^=mg_wallet_]");
    public static final SelenideElement FORBID_CREATE_NEW_WIDGET_CHECKBOX = $("#deny_create_widgets");
    public static final SelenideElement SAVE_SETTINGS_BUTTON = $("[id='submit']");
    public static final SelenideElement REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS = $("#revenue_proc");
    public static final SelenideElement ALLOW_CLICKS_FROM_CLIENTS_DOMAIN_CHECKBOX = $("[id=block_ads_on_external_domains]");
    public static final SelenideElement ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX = $("#isChangeTypesToNewWidgets");
    public static final SelenideElement COMPANY_MAIN_WEB_SITES_INPUT = $("#mainSite");
    public static final SelenideElement CLIENT_LEGAL_NAME_INPUT = $("#legalName");

    public static final SelenideElement DIRECT_PUBLISHER_DEMAND_CHECKBOX = $("#enabled_direct_publisher_demand_ads");
    public static final SelenideElement CAN_CHANGE_NSFW_TYPES_CHECKBOX = $("#can_change_ad_darkness");
    public static final String          AD_TYPES_NAME = "[name='adTypes[]']";
    public static final String          LANDING_TYPES_NAME = "[name='landingTypes[]']";

    //add-edit-delete purse
    public static final SelenideElement PURSES_ID_SELECT = $("#currencyData");
    public static final SelenideElement PURSE_ADD_BUTTON = $("#addPurse");
    public static final SelenideElement PURSE_ADD_INPUT = $("#purse");
    public static final SelenideElement PURSE_ADD_ERROR = $("#errorBlock");

    //Capitalist
    public static final SelenideElement CAPITALIST_WORLD_CARD_INFO_INPUT = $("[name=card]");
    public static final SelenideElement CAPITALIST_WORLD_EXP_DATE_MONTH_SELECT = $("#capitalist_world_validity_month");
    public static final SelenideElement CAPITALIST_WORLD_EXP_DATE_YEAR_SELECT = $("#capitalist_world_validity_year");
    public static final SelenideElement CAPITALIST_WORLD_NAME_INPUT = $("#capitalist_cardholder_name");
    public static final SelenideElement CAPITALIST_WORLD_SECOND_NAME_INPUT = $("#capitalist_cardholder_second_name");
    public static final SelenideElement CAPITALIST_WORLD_DATE_OF_BIRTH_INPUT = $("#capitalist_world_cardholder_DOB");
    public static final SelenideElement CAPITALIST_WORLD_ADDRESS_INPUT = $("#capitalist_cardholder_address");
    public static final SelenideElement CAPITALIST_WORLD_CITY_INPUT = $("#capitalist_cardholder_city");
    public static final SelenideElement CAPITALIST_WORLD_COUNTRY_SELECT = $("#capitalist_cardholder_country");
    public static final SelenideElement CAPITALIST_CARD_INFO_INPUT = $("#card");
    public static final SelenideElement CAPITALIST_NAME_INPUT = $("#cardholder");
    public static final SelenideElement CAPITALIST_ERROR_LABEL = $("[id*='capitalistWorld']>.error-requisites");
    public static final SelenideElement CAPITALIST_ACCOUNT_NUMBER_INPUT = $("#capitalistClient_purse");
    public static final SelenideElement CAPITALIST_ACCOUNT_NUMBER_ERROR_LABEL = $("[id*='capitalistClient_purse']+.error-requisites");

    public static final String ERROR_PURSE_VALIDATION_INPUT = "error-extended-requisites";

    public static final SelenideElement CLIENTS_PAYOUT_REGION_SELECT = $("#widget_payout_region");
    public static final SelenideElement CLIENTS_PAYOUT_REGION_INPUT = $("#payout_region");
    public static final SelenideElement CLIENTS_PAYOUT_REGION_TOOLTIP = $("#payoutRegionTip");
    public static final SelenideElement CLIENTS_PAYOUT_REGION_TOOLTIP_FIELD = $(".dijitTooltipContents");
    public static final SelenideElement PAYMENT_METHOD_NAME_FIELD = $("[class *= list_purses_table] tbody tr");

    //Paymaster24
    public static final SelenideElement PAYMASTER24_PURSE_NUMBER_INPUT = $("#paymaster24_purse");
    public static final SelenideElement PAYMASTER24_NAME_INPUT = $("#paymaster24_name");
    public static final SelenideElement PAYMASTER24_SURNAME_INPUT = $("#paymaster24_surname");
    public static final SelenideElement PAYMASTER24_DATE_OF_BIRTH_INPUT = $("#paymaster24_birthdate");
    public static final SelenideElement PAYMASTER24_COUNTRY_INPUT = $("#paymaster24_country");

    //Settings block
    public static final SelenideElement CAN_USE_PAYONEER_CHECKBOX = $("#can_use_payoneer");
}
