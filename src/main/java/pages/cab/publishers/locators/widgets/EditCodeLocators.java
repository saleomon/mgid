package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class EditCodeLocators {

    /**
     * Edit code interface
     */
    public static final SelenideElement SAVE_CODE_BUTTON = $("#save");
    public static final SelenideElement MANUALLY_STYLE_CHECKBOX = $("#customStyles");
    public static final SelenideElement TITLE_LIMIT_INPUT = $("#titleLimit");
    public static final SelenideElement DESC_LIMIT_INPUT = $("#descLimit");
    public static final SelenideElement VIEW_BUTTON = $("#preview");
    public static final SelenideElement IFRAME_WITH_VIEW = $("#result");
    public static final SelenideElement CSS_RADIO = $("#editorSelect-styles");
}
