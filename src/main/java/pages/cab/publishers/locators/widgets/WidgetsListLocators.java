package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WidgetsListLocators {
    //information
    public static final SelenideElement DOMAIN_FOR_ADVERTISERS_FIELD = $(".source-name-block .value");
    public static final SelenideElement DOMAIN_FOR_ADVERTISERS_ICON = $(".imgBlockSource");
    public static final SelenideElement LIMIT_BY_CATEGORY_FILTER_NEWS_ICON = $("[src$='category-limits.svg']");
    public static final SelenideElement CATEGORIES_FILTER_NEWS_POPUP = $("#categories_dialog");
    public static final String          CONTRACT_LABEL = "td>span[onclick='showDepositDialog(%s)']";
    public static final String          GUARANTEED_LABEL = "div>span[onclick='showDepositDialog(%s)']";
    public static final String          GUARANTEED_PRICE_LABEL = "label[for='rs-guarantee']";
    public static final ElementsCollection COMPOSITE_IDS = $$(".informers-info-block  a[href^='/cab/wages']");
    public static final SelenideElement NUMBER_OF_SEATS_ALLOCATED_LABEL = $("div[title='Number of seats allocated for each type of cooperation']");

    //Filter
    public static final SelenideElement AUTO_UNFOLD_SUB_WIDGETS_CHECKBOX = $("#auto_unfold_children");
    public static final SelenideElement COMPOSITE_WIDGET_ID_INPUT = $("#c_id");
    public static final SelenideElement FILTER_BUTTON = $("[id*='btnsubmit']");

    //push-widget settings
    public static final SelenideElement PUSH_START_HOUR_SELECT = $("#pushStartHour");
    public static final SelenideElement PUSH_END_HOUR_SELECT = $("#pushEndHour");
    public static final SelenideElement PUSH_PLACEMENT_INPUT = $("#placement");
    public static final SelenideElement PUSH_DELAY_INPUT = $("#pushDelay");
    public static final SelenideElement PUSH_IMAGE_ICON = $("#image-picker-event");
    public static final SelenideElement PUSH_IMAGE_FORM = $("#image-picker");
    public static final SelenideElement PUSH_SUBMIT = $x(".//div[div[@id='convert-to-push']]//span[contains(text(), \"Submit\")]");
    public static final SelenideElement PUSH_FORM = $x(".//div[div[@id='convert-to-push']]");

    // contracts settings
    public static final SelenideElement CONTRACTS_LABEL = $("[onclick*='showDepositDialog']");
    public static final SelenideElement CONTRACTS_FORM = $("#depositDialog");
    public static final SelenideElement CPC_RADIO = $("#cpc");
    public static final SelenideElement CPC_PRISE_INPUT = $("#cpc-price");
    public static final SelenideElement CPM_RADIO = $("#cpm");
    public static final SelenideElement CPM_PRISE_INPUT = $("#cpm-price");
    public static final SelenideElement ECPM_RADIO = $("#ecpm");
    public static final SelenideElement ECPM_PRISE_INPUT = $("#ecpm-price");
    public static final SelenideElement MONTH_RADIO = $("#month");
    public static final SelenideElement MONTH_PRISE_INPUT = $("#month-price");
    public static final SelenideElement MONTH_EXPIRE_DATE_INPUT = $("#contracts_end");
    public static final SelenideElement RS_RADIO = $("#rs");
    public static final SelenideElement RS_PERCENTAGE_INPUT = $("#rs-percent-goods");
    public static final SelenideElement GUARANTEED_RADIO = $("#guaranteed");
    public static final SelenideElement GUARANTEED_TYPE_DROPDOWN = $("#guarantee-contract-select");
    public static final SelenideElement GUARANTEED_PRICE_INPUT = $("#rs-guarantee");
    public static final SelenideElement GUARANTEED_PERCENTAGE_INPUT = $("#rs-percent-goods-guaranteed");
    public static final SelenideElement GUARANTEEDS_LABEL= $("div>span[onclick*='showDepositDialog']:first-child");
    public static final SelenideElement CONTRACT_SAVE = $("#save_label");
    public static final SelenideElement CONTRACT_TYPE_FILTER = $("#contracts_type");

    //Video part
    public static final String          VIDEO_ID_FIELD = ".//tr[@id='ticker_video%s']/td[1]";

    public static final SelenideElement PUSH_POPUP_LOADER = $("img[src*='loading.gif']");

    //icons
    public static final SelenideElement EDIT_CODE_ICON = $("a[href*='wages/informers-settings/']");
    public static final SelenideElement LINK_EDIT_PRODUCT_SUB_WIDGET = $("[href*='/informers-edit/type/goods/']");
    public static final SelenideElement LINK_EDIT_NEWS_SUB_WIDGET = $("[href*='/informers-edit/type/news/']");
    public static final SelenideElement SANCTIONS_DIALOG_ICON = $("#sanctions_dialog");
    public static final SelenideElement CATEGORY_FILTER_ICON = $("[src*='category_filter']");
    public static final String BLOCK_UNBLOCK_WIDGET_ICON = "#changestatimg%s";
    public static final String BLOCK_UNBLOCK_LOAD_ICON = "img[id*='trigger-load-dummy-%s']";
    public static final SelenideElement ALLOW_DISALLOW_DELETE_ICON = $("[id^=nodel]");
    public static final SelenideElement ALLOW_DISALLOW_DELETE_LOADING_ICON = $("[id^=nodel][src*=loading]");
    public static final SelenideElement BLOCK_ADS_ICON = $(".imgBlockAds");
    public static final String          GOOGLE_ADD_MANAGER_ICON = "img#widget_dfp_%s";
    public static final String          MANAGERS_ALERT_ICON = "#widget_slack_notifications_%s";
    public static final String          EXCHANGE_CAMPAIGN_FILTER_ICON = ".showFilters[data-widget-uid='%s']";
    public static final String          WAGES_CAMPAIGN_FILTER_ICON = ".showFiltersPubs[data-widget-uid='%s']";
    public static final SelenideElement SHOW_CHILD_WIDGETS_ICON = $(".widgets-tree-button");

    //Specify widget IDs popup
    public static final SelenideElement SPECIFY_POPUP_WIDGETS_IDS_INPUT = $("#bulk-action-ids");
    public static final SelenideElement SPECIFY_POPUP_APPLY_TO_ALL_CHILD_CHECKBOX = $("[name=bulk-action-specify-widgets-apply-to-children]");
    public static final SelenideElement SPECIFY_POPUP_SAVE_BUTTON = $x(".//div[//*[@id='bulk-action-ids']]//span[text()='Save']");

    // Exchange campaign filter (only/except) icon
    public static final String          EXCHANGE_CAMPAIGN_FILTER_LIST_IN_POPUP = "#filters_settings_dialog tr>td:nth-child(2)";

    public static final SelenideElement MASS_ACTIONS_SELECT = $("#group-actions");
    public static final SelenideElement SELECT_ALL_CHECKBOXES_LINK = $("[href*='.selectAll']");
    public static final SelenideElement SELECT_SPECIFY_WIDGETS_LINK = $("[href*='.specifyWidgets']");
    public static final SelenideElement MASS_ACTION_POPUP_SUBMIT = $x(".//div[*[@id='bulk-action-modal']]//*[text()='Submit']");
    public static final SelenideElement MASS_WIDGETS_MESSAGE = $("[id^=bulk-action] .message");
    public static final SelenideElement APPLY_TO_ALL_CHILD_CHECKBOX = $("[name=bulk-action-apply-to-children]");

    //MASS -> exchange campaign filters
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_SELECT = $("#change-campaign-filter");
    public static final SelenideElement MASS_WAGES_CAMPAIGN_FILTER_SELECT = $("#change-campaign-filter");
    public static final SelenideElement UPLOAD_PARTNERS_LIST_BUTTON = $(".partner-block__load-btn");
    public static final String          MASS_EXCHANGE_CAMPAIGN_FILTER_LIST = "#partnersList input";
    public static final String          MASS_EXCHANGE_CAMPAIGN_FILTER_LIST_LABEL = "#partnersList label";
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_SEARCH = $("#filterPartnersList #search");
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_CLEAN = $("#filterPartnersList #clean");
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_SELECT_ALL = $(".check-all-wrap input");
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_ACTIVE = $("#find1");
    public static final SelenideElement MASS_EXCHANGE_CAMPAIGN_FILTER_INACTIVE = $("#find0");

    //MASS -> stop words
    public static final SelenideElement MASS_CHANGE_STOP_WORDS_SELECT = $("#change-stop-words-types");
    public static final SelenideElement STOP_WORDS_INPUT = $(".tagsinput>div>input");

    //MASS -> publisher category
    public static final SelenideElement MASS_WIDGETS_CATEGORIES_POPUP = $("#turn-off-widgets-categories");
    public static final SelenideElement MASS_WIDGETS_CATEGORIES_NEXT = $x(".//div[*[@id='bulk-action-modal']]//*[text()='Next']");
    public static final SelenideElement MASS_CHANGE_WIDGETS_CATEGORIES_OK_ACTION_LABEL = $(".ok-action");
    public static final SelenideElement MASS_CHANGE_WIDGETS_CATEGORIES_WARN_ACTION_LABEL = $(".warn-action");
    public static final String          MASS_CHANGE_WIDGETS_CATEGORIES_WARN_ACTION_DETAILED_LABEL = ".widget-info-list ul:nth-of-type(1)>li";
    public static final String          MASS_CHANGE_WIDGETS_CATEGORIES_OK_ACTION_DETAILED_LABEL = ".widget-info-list ul:nth-of-type(2)>li";

    //MASS -> widget price
    public static final SelenideElement MASS_CONTRACT_PRICE_INPUT = $("#newPrice");
    public static final SelenideElement MASS_CONTRACT_PRICE_SUBMIT = $(".bulk-action-submit");

    public static final SelenideElement BULK_ACTION_IN_PROCESS_ICON = $("#bulk_actions_in_process");

    public static final SelenideElement MASS_CHANGE_WIDGETS_CATEGORIES_TYPES_SELECT = $("#change-widgets-categories-types");
    public static final SelenideElement MASS_CHANGE_EXCHANGE_CATEGORIES_TYPES_SELECT = $("#change-exchange-categories-types");
    public static final SelenideElement MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_SEARCH_INPUT = $("#newsCategoriesBox #catsSearch");
    public static final String          MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_TITLES_AFTER_SEARCH = ".//*[@id='newsCategoriesBox']//li[span[span[@class='fancytree-title'] and not(contains(@class, 'fancytree-hide'))] and not(ul)]//span[@class='fancytree-title']";
    public static final String          MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_TITLES = ".//div[@id='newsCategoriesBox']//li[span[span[@class='fancytree-title']] and not(ul)]//span[@class='fancytree-title']";

    public static final String          CATEGORY_FILTER_POPUP_DATA = "#categories_dialog div:not([style*='font-weight'])";

    //Managers alert
    public static final SelenideElement MANAGERS_ALERTS_POPUP = $("#slackNotificationsDialog");
    public static final SelenideElement MANAGERS_ALERTS_PERIOD_SELECT = $("#period");
    public static final SelenideElement MANAGERS_ALERTS_HOUR_FROM_INPUT = $("#hourFrom");
    public static final SelenideElement MANAGERS_ALERTS_HOUR_TO_INPUT = $("#hourTo");
    public static final SelenideElement MANAGERS_ALERTS_ADD_ADDITIONAL_ALERT_BUTTON = $(".manager-alert__add-alert-btn");

    public static final String          MANAGERS_ALERTS_TYPE_SELECT = ".manager-alert__fieldset--alert select";
    public static final String          MANAGERS_ALERTS_DELTA_INPUT = ".manager-alert__fieldset--alert .manager-alert__input--delta";
    public static final String          MANAGERS_ALERTS_DELTA_NUMBER = ".manager-alert__fieldset--alert .manager-alert__input--number";
    public static final SelenideElement MANAGERS_ALERTS_SAVE_BUTTON = $(".manager-alert-submit-btn");
    public static final SelenideElement MANAGERS_ALERTS_TURN_OFF_BUTTON = $(".manager-alert-turn-off-btn");

}
