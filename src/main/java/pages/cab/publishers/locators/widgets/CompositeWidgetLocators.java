package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CompositeWidgetLocators {

    public static final SelenideElement SAVE_BUTTON = $("#submit");

    public static final SelenideElement SHOW_DIRECT_LINK_CHECKBOX = $("#showDirectLink");
    public static final SelenideElement RTB_ENABLING_CHECKBOX = $("#rtbEnabled");
    public static final SelenideElement XML_RSS_CHECKBOX = $("#xml");
    public static final SelenideElement ELASTIC_CHECKBOX = $("#elastic");
    public static final SelenideElement AUTO_REFRESH_CHECKBOX = $("#autorefresh");
    public static final SelenideElement VIDEO_SSP_CHECKBOX = $("#videoSsp");
    public static final SelenideElement ALLOW_MULTIPLE_WIDGETS = $("#allowMultipleWidgets");
    public static final SelenideElement PLACE_RESERVATION_CHECKBOX = $("#usePlaceReservation");
    public static final SelenideElement ELASTIC_PAGE_COUNT_INPUT = $("#elasticPageCount");
    public static final SelenideElement UNLIMITED_CHECKBOX = $("#unlimited");
    public static final SelenideElement DFP_CHECKBOX = $("[id='changeIframeSize']");
    public static final SelenideElement GO_TO_THE_TRANSIT_CHECKBOX = $("input[id='clickableDelay']");
    public static final SelenideElement AD_LINK_CHECKBOX = $("#adLink");
    public static final SelenideElement WIDGET_TITLE_CHECKBOX = $("#adLinkWidgetTitle");
    public static final SelenideElement WIDGET_LOGO_CHECKBOX = $("#adLinkWidgetLogo");
    public static final SelenideElement NON_CLICKABLE_AREA_CHECKBOX = $("#enableNonClickableArea");
    public static final SelenideElement MIRROR_SELECT = $("#mirror");
    public static final SelenideElement SOURCE_TYPE_SELECT = $("#sourcesTypesId");
    public static final SelenideElement NEWS_AMOUNT_INPUT = $("#countNews");

    public static final SelenideElement CATEGORY_PLATFORM_SELECT = $("#category_platform_select_dropdown");
    public static final SelenideElement PUSH_PROVIDER_SELECT = $("#pushProvider");
    public static final SelenideElement SOURCES_ID_SELECT = $("#sourcesId");
    public static final SelenideElement SOURCES_ID_LABEL = $("#select2-sourcesId-container");
    public static final SelenideElement SOURCES_ID_RESULTS_SELECT = $("#select2-sourcesId-results");
    public static final SelenideElement SOURCES_ID_SEARCH_INPUT = $("[aria-controls=select2-sourcesId-results]");
    public static final SelenideElement LOW_VOLUME_DOMAIN_CHECKBOX = $("#lowVolumeSource");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_INPUT = $("#newDomainSource");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME = $("#lowVolume");
    public static final SelenideElement NEW_DOMAIN_TO_ADVERTISERS_SUBMIT = $x(".//div[div[@id='sourcesIdPopup']]//span[contains(text(), 'Submit')]");

    public static final SelenideElement SUBSOURCE_FILTER_CHECKBOX = $("[id='sourceFilter']");
    public static final SelenideElement SUBSOURCE_INPUT = $("[id='sourcesFilters_tag']");
    public static final String          SUBSOURCE_SAVED_ELEMENTS = "[id='sourcesFilters_tagsinput'] span span";
    public static final SelenideElement SUBSOURCE_FIRST_INPUTED_ELEMENT = $("[id='sourcesFilters_tagsinput'] [class='tag'] a");

    //AdBlock Integrations
    public static final SelenideElement ADBLOCK_INTEGRATIONS_CHECKBOX = $("#adBlockIntegration");
    public static final SelenideElement ADBLOCK_TEMPLATE_SELECT = $("select#adblockTemplate");

    // doubleClick settings
    public static final SelenideElement DESKTOP_DOUBLECLICK_SELECT = $("#randomClicksDisabledDesktop");
    public static final SelenideElement MOBILE_DOUBLECLICK_SELECT = $("#randomClicksDisabled");
    public static final SelenideElement DESKTOP_DOUBLECLICK_DELAY_INPUT = $("#desktopDoubleclickDelay");
    public static final SelenideElement MOBILE_DOUBLECLICK_DELAY_INPUT = $("#mobileDoubleclickDelay");

    // stop-words
    public static final SelenideElement     STOP_WORD_CHECKBOX = $("#wordFilter");
    public static final SelenideElement     STOP_WORDS_INPUT = $("#stopWords_tag");
    public static final SelenideElement     STOP_WORD_ERROR = $("#stopWords_tag.not_valid");
    public static final ElementsCollection  STOP_WORD_OPTIONS = $$("#stopWords_tagsinput>span>span");
    public static final ElementsCollection  STOP_WORD_DELETE_BUTTONS = $$(".tag>a");
    public static final SelenideElement     STOP_WORDS_COPY_ICON = $(".btnCopy");
    public static final String              STOP_WORDS_TEXTS = "#stopWords_tagsinput .tag>span";

    // src id
    public static final SelenideElement SRC_ID_CHECKBOX = $("#enableSource");
    public static final SelenideElement SRC_ID_INPUT = $("#sourceName");

    // default_js
    public static final SelenideElement DEFAULT_JS_CHECKBOX = $("#useDefaultJs");
    public static final SelenideElement DEFAULT_JS_INPUT = $("#defaultJs");

    // clicktracking
    public static final SelenideElement CLICKTRACKING_CHECKBOX = $("#clicktrackingEnabled");
    public static final SelenideElement CLICKTRACKING_INPUT = $("#clicktrackingMacros");

    // active delay
    public static final SelenideElement ACTIVATE_DELAY_CHECKBOX = $("#activateDelayCheck");
    public static final SelenideElement ACTIVE_DELAY_INPUT = $("input#activateDelay");

    // Auto-refresh ads by viewability
    public static final SelenideElement AUTO_REFRESH_ADS_BY_VIEWABILITY_CHECKBOX = $("#autoRefreshAdsEnabled");
    public static final SelenideElement AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_TIME_INPUT = $("#refreshAdsTime");
    public static final SelenideElement AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_BY_SELECT = $("#refreshAdsBy");

    // load additional widgets
    public static final SelenideElement LOAD_ADDITIONAL_WIDGETS_CHECKBOX = $("#additionalWidgets");
    public static final SelenideElement LOAD_ADDITIONAL_WIDGETS_ID_INPUT = $("input#additionalWidgetsId");
    public static final SelenideElement LOAD_ADDITIONAL_WIDGETS_SELECTOR_INPUT = $("input#additionalWidgetsSelector");

    // back button settings
    public static final SelenideElement BACK_BUTTON_SETTINGS_CHECKBOX = $("#backButtonOptions");
    public static final SelenideElement BACK_BUTTON_WIDGET_ID_INPUT = $("#backButtonWidgetId");
    public static final SelenideElement BACK_BUTTON_TRAFFIC_TYPE_CHECKBOX = $("#backButtonTrafficTypeCheckbox");
    public static final String          BACK_BUTTON_TRAFFIC_ID_ELEMENT = "backButtonTrafficType";
    public static final SelenideElement BACK_BUTTON_TRAFFIC_SOURCE_CHECKBOX = $("#backButtonTrafficSourceCheckbox");
    public static final SelenideElement BACK_BUTTON_TRAFFIC_SOURCE_INPUT = $("#backButtonTrafficSource");
    public static final SelenideElement BACK_BUTTON_DEVICE_CHECKBOX = $("#backButtonDeviceCheckbox");
    public static final String          BACK_BUTTON_DEVICE_ID_ELEMENT = "backButtonDevice";
    public static final SelenideElement BACK_BUTTON_BANNER_DISPLAY_CHECKBOX = $("#backButtonBannerDisplay");
    public static final SelenideElement BACK_BUTTON_BANNER_BACKGROUND_COLOR_INPUT = $("#backButtonBannerBgColor");
    public static final SelenideElement BACK_BUTTON_BANNER_TEXT_COLOR_INPUT = $("#backButtonBannerTextColor");
    public static final SelenideElement BACK_BUTTON_BANNER_TEXT_INPUT = $("#backButtonBannerText");
    public static final SelenideElement BACK_BUTTON_HEADER_DISPLAY_CHECKBOX = $("#backButtonHeaderDisplay");
    public static final SelenideElement BACK_BUTTON_HEADER_DISPLAY_INPUT = $("#backButtonHeaderSelector");
    public static final SelenideElement GPT_INTEGRATION_CHECKBOX = $("#gptOptions");
    public static final SelenideElement GPT_SLOT_INPUT = $("input#gptOptionsSlot");
    public static final SelenideElement GPT_DIV_ID_INPUT = $("input#gptOptionsDivId");
    public static final SelenideElement GPT_AD_UNIT_ID_INPUT = $("input#gptUnitId");
    public static final SelenideElement GPT_PLACEMENT_FOR_DESKTOP_INPUT = $("input#gptOptionsDesktopPosition");
    public static final SelenideElement GPT_PLACEMENT_FOR_MOBILE_INPUT = $("input#gptOptionsMobilePosition");
    public static final SelenideElement GPT_ENABLE_LAZY_LOAD_CHECKBOX = $("input#gptOptionsLazyCheckbox");
    public static final SelenideElement GPT_LAZY_LOAD_ROOT_MARGIN_INPUT = $("input#gptOptionsLazy");
    public static final SelenideElement GPT_DISPLAY_ON_THE_SUBWIDGETS_CHECKBOX = $("input#gptDisplayOnSubwidgets");
}
