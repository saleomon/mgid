package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.*;

public class GoodsWidgetLocators {

    public static final ElementsCollection CAMPAIGN_TIER_FILTER_VALUES = $$(byName("campaignTierValue"));
    public static final ElementsCollection SHOW_TEASERS_FILTER_VALUES = $$(byName("femaleContent"));
    public static final SelenideElement DESCRIPTION_REQUIRED_CHECKBOX = $("#descriptionRequired");
    public static final SelenideElement DESCRIPTION_REQUIRED_INFO = $x(".//label[contains(text(), 'Description required')]/img");
    public static final SelenideElement SHOW_DESCRIPTION_CHECKBOX = $("#showDescription");

    public static final SelenideElement INCLUDE_RTB_TEASERS_CHECKBOX = $("#rtbEnabled");
    public static final SelenideElement INCLUDE_FAKE_TEASERS_CHECKBOX = $("#includeFakeTeasers");
    public static final SelenideElement ROTATE_ONLY_TEASERS_OF_CURRENT_SUBNET_CHECKBOX = $("#onlyOwnSubnet");
    public static final SelenideElement ROTATE_IN_ADSKEEPER_SUBNET_CHECKBOX = $("#rotateSubnet2");
    public static final SelenideElement NO_CALCULATION_GHITS_CTR_CHECKBOX = $("#noCalcGhitsCtr");
    public static final SelenideElement ANTI_DUPLICATION_CHECKBOX = $("#blockDuplicate");
    public static final SelenideElement AGENCY_FEE_TO_WAGES_CHECKBOX = $("#agencyFeeToWages");

    public static final SelenideElement MGID_INVENTORY_BASED_WIDGET_SELECT = $("#ourBlock");

    public static final SelenideElement PAGES_COUNT_INPUT = $("#pagesCount");
    public static final SelenideElement TRANZIT_PAGE_PERCENTAGE_INPUT = $("#useTranzPage");
    public static final SelenideElement DUPLICATES_COUNT_INPUT = $("#duplicatesCount");
    public static final SelenideElement DEFAULT_MIN_CPC_DECREASING_FACTOR_INPUT = $("#defaultMinCpcDecreasingFactor");
    public static final SelenideElement SET_MINIMAL_CPC_INPUT = $("#viewPriceOfClick");
    public static final SelenideElement SET_MINIMAL_CPC_ICON = $x(".//td[*[@id='viewPriceOfClick']]/img");
    public static final SelenideElement MINIMAL_CPC_FORM = $("#mpcDialogForm");
    public static final String          MINIMAL_CPC_REGIONS_INPUT = ".tree-node:not(.parent-node) .node-value";
    public static final String          MINIMAL_CPC_REGIONS_EXPAND_ICON = ".tree-pointer";
    public static final SelenideElement MINIMAL_CPC_UPSALE_CHECKBOX = $("#upsale");
    public static final SelenideElement MINIMAL_CPC_FORM_SUBMIT = $("#setmpc_label");

    public static final SelenideElement SET_MINIMAL_CPM_ICON = $("#cpmIco");
    public static final SelenideElement MINIMAL_CPM_BUTTON = $("#button-row");
    public static final String          MINIMAL_CPM_COUNTRIES_LI = "#countryPropsCpm-country_popup li:not(:nth-child(-n+2)):not([id$='_next'])";
    public static final SelenideElement MINIMAL_CPM_FORM_BUTTON = $("#countryPropsCpm-OK_label");
    public static final String          MINIMAL_CPM_FORM_LABELS = "tr[id^=row]";
    public static final String          MINIMAL_CPM_FORM_INPUT = "input[type='text']";
    public static final SelenideElement MINIMAL_CPM_FORM_SELECT_ICON = $("#widget_countryPropsCpm-country .dijitArrowButtonInner");

    public static final SelenideElement CAMPAIGN_TIER_FILTER_INFO = $x(".//label[contains(text(), 'Campaign tier filter')]/img");
    public static final String          CAMPAIGN_TIER_RADIO = "[name=campaignTierValue]:not([checked])";
    public static final String          SHOW_TEASERS_RADIO = "[name=femaleContent]:not([checked])";
    public static final SelenideElement ANIMATION_SELECT = $("#animation");

    public static final SelenideElement CATEGORY_FILTER_SEARCH_INPUT = $("#catsSearch");
    public static final String CATEGORY_FILTER_TITLES = ".//div[@id='categoriesBox']//li[span[span[@class='fancytree-title']] and not(ul)]//span[@class='fancytree-title']";
    public static final String CATEGORY_FILTER_TITLES_AFTER_SEARCH = ".//%sli[span[span[@class='fancytree-title'] and not(contains(@class, 'fancytree-hide'))] and not(ul)]//span[@class='fancytree-title']";
    public static final String CATEGORY_FILTER_ELEMENTS = "li[id^='ft_product-']>span:not(.fancytree-has-children)";
    public static final SelenideElement PRODUCT_PROMOTIONS_CHECKBOX =  $x(".//span[span[text()='Product promotions']]/*[@class='fancytree-checkbox']");
    public static final SelenideElement PRODUCT_PROMOTIONS_EXPANDER_ICON =  $x(".//span[span[text()='Product promotions']]/*[@class='fancytree-expander']");
    public static final SelenideElement CONTENT_PROMOTIONS_CHECKBOX =  $x(".//span[span[text()='Content promotions']]/*[@class='fancytree-checkbox']");

    public static final SelenideElement ADS_FILTER_SELECT = $("#adsFilter");
    public static final SelenideElement ADS_FILTER_INPUT = $("#teaserIndata");
    public static final SelenideElement ADS_FILTER_SUBMIT = $("#teaserSend_label");
    public static final SelenideElement ADS_FILTER_EDIT_ICON = $("img[src*='edit.png']");
    public static final SelenideElement ADS_FILTER_LABEL = $("[id='goodsBox']");
    public static final SelenideElement ADS_FILTER_MESSAGE = $("#teaserResults-label>span");

    public static final String          PUBLISHERS_FILTER_STRING_RADIOBUTTONS = "useFilterPartners";
    public static final String          PUBLISHERS_FILTER_CHECKBOXES = ".green input[id^=p]";
    public static final SelenideElement PUBLISHERS_FILTER_RADIOBUTTONS = $(By.name("useFilterPartners"));
    public static final SelenideElement UPLOAD_PARTNERS_LIST_BUTTON = $("#loadpartnersbtn");
    public static final SelenideElement SEARCH_PARTNERS_ICON = $("#bt_search");
    public static final SelenideElement PUBLISHER_FILTER_FORM = $("#filterPartnersList");

    public static final SelenideElement CATEGORIES_FILTER_RADIOBUTTONS = $(By.name("useFilterCategories"));
    public static final ElementsCollection CATEGORIES_FILTER_VALUES = $$(byName("useFilterCategories"));





}
