package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class VideoCfgLocators {
    public static final SelenideElement VIDEO_CFG_JSON = $("textarea#videoCfg");
}
