package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WidgetCodeLocators {

    public static final SelenideElement CODE_lABEL = $(".codesize");
    public static final SelenideElement GET_WIDGET_LINK_FOR_CLIENT = $x(".//a[1]");
    public static final SelenideElement HEADER_WIDGET_CODE_LINK = $x(".//a[2]");
}
