package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class VideoManageLocators {
    public static final SelenideElement SKIP_TIME_INPUT = $("#videoSkipTime");
    public static final SelenideElement SUBMIT_BUTTON = $("#submit");
    public static final SelenideElement VIDEO_GROUPS_TABLE = $("table[class*=multi-radio-videoGroups]");
    public static final String VIDEO_GROUPS_ROWS = "tbody tr";
    public static final ElementsCollection VIDEO_GROUP_STATES_HEADERS = $$("th.header");
    public static final String VIDEO_GROUP_NOT_SET_RADIOBUTTONS = "td.element input";
    public static final SelenideElement VIDEO_CONFIG_CHECKBOX = $("#videoContentEnabled");
}
