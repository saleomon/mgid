package pages.cab.publishers.locators.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ExchangeWidgetLocators {
    public static final SelenideElement NO_CALC_NEWS_CTR = $("#noCalcNewsCtr");
    public static final SelenideElement CATEGORIES_FILTER_RADIOBUTTONS = $(By.name("useFilterCategories"));
    public static final String CATEGORY_FILTER_ELEMENTS = "li[id^='ft_news-']>span:not(.fancytree-has-children)";
    public static final ElementsCollection CATEGORIES_FILTER_VALUES = $$(byName("useFilterCategories"));
    public static final String CATEGORY_FILTER_TITLES = ".//div[@id='categoriesBox']//li[span[span[@class='fancytree-title']] and not(ul)]//span[@class='fancytree-title']";
    public static final String CATEGORY_FILTER_TITLES_AFTER_SEARCH = ".//li[span[span[@class='fancytree-title'] and not(contains(@class, 'fancytree-hide'))] and not(ul)]//span[@class='fancytree-title']";
    public static final SelenideElement CATEGORY_FILTER_SEARCH_INPUT = $("#catsSearch");
    public static final SelenideElement SAVE_BUTTON = $("#submit");
    public static final SelenideElement ORIGINAL_TITLES_ONLY_CHECKBOX = $("#isOriginalTitle");



}
