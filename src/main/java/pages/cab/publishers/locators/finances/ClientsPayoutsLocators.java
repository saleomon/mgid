package pages.cab.publishers.locators.finances;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ClientsPayoutsLocators {
    //Filter
    public static final SelenideElement REGION_OF_PAYOUT_SYSTEM_SELECT = $("#payout_region");
    public static final SelenideElement PUBLISHER_CURRENCY_SELECT = $("#client_currency");
    public static final SelenideElement SUBMIT_FILTER_BUTTON = $("#btnsubmit");

    //Requests table
    public static final SelenideElement PAYMENT_ROW = $("tr[id*=payment]");
    public static final ElementsCollection PAYMENTS_ROWS = $$("tr[id*=payment]");
    public static final SelenideElement PAYMENT_REQUEST_ID = $x(".//tr[contains(@id, 'payment')]/td[1]");
    public static final ElementsCollection PAYMENT_REQUEST_IDS = $$x(".//tr[contains(@id, 'payment')]/td[1]");
    public static final SelenideElement PAYMENT_STATUS_CELL = $x(".//td[contains(@id, 'namestat')]");
    public static final ElementsCollection PAYMENT_STATUS_CELLS = $$x(".//td[contains(@id, 'namestat')]");
    public static final SelenideElement PAYMENT_AMOUNT_VALUE = $x(".//d[contains(@id, 'amountRequested')]");
    public static final SelenideElement PAYMENT_COMMENT_MANAGER_ACCOUNTANT_CELL = $x(".//div[contains(@id, 'comment-container-internal')]");
    public static final SelenideElement PAYMENT_APPROVE_REQUEST_ICON = $x(".//img[contains(@src, 'yes.gif')]");
    public static final SelenideElement PAYMENTS_TABLE = $("#pack_form");
    public static final SelenideElement APPROVE_REQUEST_ICON = $("[id*='3changestat']");
    public static final ElementsCollection APPROVE_REQUEST_ICONS = $$("[id*='3changestat']");
    public static final SelenideElement REJECT_REQUEST_ICON = $("[id*='2changestat']");
    public static final ElementsCollection REJECT_REQUEST_ICONS = $$("[id*='2changestat']");
    public static final SelenideElement PAYMENT_REQUEST_PAID_ICON = $(".payments-currency-icon");
    public static final SelenideElement PAYMENT_CANCEL_PAYOUT_ICON = $x(".//img[contains(@src, 'user2.png')]");

    //Wallets
    public static final ElementsCollection PACK_PAYMENT_FIELD = $$("td[id*=pack_payment_]");
    public static final ElementsCollection PACK_PAYMENT_CHECKBOXES = $$("tr[id*=payment] input[type=checkbox]");

    //Payment amount
    public static final ElementsCollection AMOUNT_REQUESTED_FIELDS = $$("d[id*=amountRequested]");

    //Edit paid dialog
    public static final SelenideElement PAID_EDIT_POPUP = $x(".//div[@id='dialogPaidEdit' and not (contains(@style, 'display: none'))]");
    public static final SelenideElement PAID_EDIT_POPUP_PURSE_LABEL = $("#purse");
    public static final SelenideElement PAID_EDIT_POPUP_SUM_FIELD = $("#dialogSum");
    public static final SelenideElement PAID_EDIT_POPUP_COMMENT_FIELD = $("#dialogCommentP");
    public static final SelenideElement PAID_EDIT_DIALOG_SUBMIT_BUTTON = $("#paidButtonSubmit_label");

    //"Cancel order" popup
    public static final SelenideElement ORDER_CANCELLATION_POPUP = $("#changeStatusDialog");
    public static final SelenideElement ORDER_CANCELLATION_COMMENT_FIELD = $("#cancelComment");
    public static final SelenideElement ORDER_CANCELLATION_SUBMIT_BUTTON = $x(".//span[text()= 'Cancellation']");

    //above table elements
    public static final SelenideElement ADD_PACK_BUTTON = $("#addPackButton");
}
