package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class NewsListLocators {

    public static final SelenideElement TITLE_INLINE_LABEL = $(".titleInline");
    public static final SelenideElement DESCRIPTION_INLINE_LABEL = $(".descriptionInline");
    public static final SelenideElement NEWS_TYPE_INLINE_LABEL = $("[id*='typeTeaser']");
    public static final SelenideElement POLITICAL_PREFERENCES_ICON = $(".contentGroupInline");
    public static final SelenideElement STATUS_FIELD = $("td.status>span");
    public static final SelenideElement APPROVE_ICON = $("img[alt='Approve']");
    public static final SelenideElement REJECT_ICON = $("img[alt='Decline']");
    public static final ElementsCollection NEWS_ID_LABEL = $$("[id^='nn_id_']");

    //Actions
    public static final SelenideElement NEWS_EDIT_ICON = $("img[src*='edit.png']");
    public static final SelenideElement SEND_ON_MODERATION_ICON = $("img[title='Send on moderation']");
    public static final SelenideElement CANCEL_MODERATION_ICON = $("img[title='Cancel moderation']");
    public static final SelenideElement RESTORE_FROM_TRASH_ICON = $("img[src*='restore_from_trash.png']");
    public static final ElementsCollection ORIGINAL_TITLE_ICON = $$("img[src*='tag-16']");
    public static final String NOT_ORIGINAL_TITLE_ICON = "img[src*='literacy_level_none']";

    //Inline editing//
    //Image
    public static final SelenideElement INLINE_IMAGE_LABEL = $("img[id^='nn_photo_']");
    public static final SelenideElement INLINE_IMAGE_INPUT = $("#imageLink");

    //News type
    public static final SelenideElement INLINE_NEWS_TYPE_LABEL = $("[id^=typeTeaser]");
    public static final SelenideElement INLINE_NEWS_TYPE_LINK = $(".teaser_type_settings");
    public static final SelenideElement INLINE_NEWS_TYPE_SELECT = $("#js-types-select");
    public static final SelenideElement INLINE_NEWS_TYPE_SUBMIT = $("#js-accept-type-button");

    public static final SelenideElement TITLE_INLINE_INPUT = $("[name=title-input]");
    public static final SelenideElement DESCRIPTION_INLINE_INPUT = $("[name=description-input]");
    public static final String CONTENT_GROUP_ICON = "#contentGroup%s";

    //category
    public static final SelenideElement CATEGORY_INLINE_FIELD = $(".editableCategory");
    public static final SelenideElement CATEGORY_INLINE_SELECT = $(".dropdown-container");
    public static final SelenideElement CATEGORY_INLINE_SUBMIT = $x(".//div[*[contains(@id, 'edit-category-form_')]]//*[text()='Submit']");

    //lifetime
    public static final SelenideElement LIFETIME_INLINE_LABEL = $(".livetimeInline");
    public static final SelenideElement LIFETIME_INLINE_SELECT = $("[name=livetime-input]");

    //landing
    public static final SelenideElement LANDING_TYPES_INLINE_LABEL = $(".landingTypesInline");
    public static final SelenideElement LANDING_INLINE_TYPES_SELECT = $("[name=landing_types-input]");

    //url
    public static final SelenideElement URL_INLINE_LABEL = $(".news_url.urlInline");
    public static final SelenideElement URL_INLINE_INPUT = $("[name=url-input]");

    // Political preferences popup
    public static final String CONTENT_GROUP_SELECT = "#contentGroup-input_%s";
    public static final SelenideElement SUBMIT_BUTTON = $(".ui-button-text");

    //delete
    public static final String DELETE_ICON = "#trash_%s";
    public static final SelenideElement DELETE_REASON_INPUT = $("[id^='news_delete_form_'] [id^='descriptionBox']");
    public static final SelenideElement DELETE_REASON_BUTTON = $("#btnApply");
    public static final SelenideElement DELETE_TYPE_RADIO = $("input[name=droped]");
    public static final SelenideElement HIDE_ON_TRANZ_PAGE = $("input[name=hide_on_tranz_page]");
    public static final SelenideElement RESTORE_NEWS_ICON = $("[id*=newsRestoreAction]");

    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
    public static final SelenideElement LINK_TO_RELATED_IMAGES = $("[id*='relatedImages'] a");
    public static final SelenideElement RELATED_IMAGES_FILTER = $("[id='related_images']");

    //Table
    public static final SelenideElement EMPTY_TABLE_ROW = $("tr.empty");

    //Mass actions
    public static final SelenideElement MASS_ACTION_SELECT_LIST = $("[name=doaction]");
    public static final SelenideElement MASS_DELETE_REASON_INPUT = $("#mass_delete_reason");
    public static final SelenideElement HIDE_ON_TRANZ_PAGE_MASS_ACTION = $("[class=mass_hide_on_tranz_page] input[name=hide_on_tranz_page]");
    public static final SelenideElement MASS_ACTION_EXECUTE_BUTTON = $("[id=doactionbutton]");

}
