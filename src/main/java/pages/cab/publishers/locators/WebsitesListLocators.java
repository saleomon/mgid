package pages.cab.publishers.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class WebsitesListLocators {

    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
    public static final SelenideElement FILTER_POST_MODERATION_CHECKBOX = $("#post_moderation_enabled");
    public static final SelenideElement FILTER_SUBNET_SELECT = $("#subnet");

    //General Table
    //Information
    public static final SelenideElement ID_FIELD = $("span[id*='s_id_']");
    public static final SelenideElement NAME_FIELD = $("[id*='s_name_']");
    public static final SelenideElement MANAGER_FIELD = $("[id*='c_kurator_']");
    public static final ElementsCollection MANAGER_LABEL = $$("[id^='c_kurator_']");
    public static final String SITES_ROW = "tr[id^=site-]";

    //Icons
    public static final SelenideElement BLOCK_ADS_ICON = $(".imgBlockAds");
    public static final SelenideElement BLOCK_BY_ANTIFROD_ICON = $("[id^=blockSite]");
    public static final SelenideElement UNBLOCK_BY_ANTIFROD_ICON = $("[id^=unblockSite]");
    public static final String          STATUS_FIELD = ".status-container[siteid='%s']";
    public static final String          APPROVE_SITE_ICON = "#reject-thumb-up-%s";
    public static final String          REJECT_SITE_ICON = "#reject-thumb-down-%s";
    public static final String          VERIFY_SITE_ICON = "#reject-verify-%s";
    public static final String          DELETE_SITE_ICON = "[onclick='deleteSite(%s)']";
    public static final SelenideElement SAVE_APPROVE_FORM_POPUP= $("#siteApproveForm");
    public static final SelenideElement SAVE_APPROVE_FORM_BUTTON = $("#siteApproveForm #approvButton");
    public static final SelenideElement MIRRORS_SUBNET_SELECT = $("#siteApproveForm [name=mirror]:not([disabled])");
    public static final SelenideElement REJECTION_REASON_SELECT = $("#priorityId");
    public static final SelenideElement REJECTION_REASON_BUTTON = $("#rejectForm span[id*='Button']");
    public static final SelenideElement REJECTION_REASON_INPUT = $("#descriptionBox");
    public static final String          GOOGLE_ADD_MANAGER_ICON = "#changedfpflagimg%s";

    // Actions
    public static final String EDIT_ADS_TXT_ICON = "#editAdsTxtButton%s";
    public static final String ADD_ADS_TXT_ICON = "#generateAdsTxtButton%s";

    //Domain
    public static final SelenideElement SITE_DOMAIN = $("[class][id*='domain_'][href]");
    public static final SelenideElement CATEGORY_FIELD = $(".wrap_category");
    public static final SelenideElement LANGUAGE_FIELD = $(".wrap_language");
    public static final ElementsCollection TIER_FIELD = $$(".wrap_tier_id");
    public static final SelenideElement BUNDLE_FIELD = $x(".//*[*[@class='key' and text()='Bundles:']]/*[@class='value']");

    //Mirror
    public static final SelenideElement MIRROR_FIELD = $("span[id^='mirrors_']");

    //Information
    public static final SelenideElement DOMAIN_FOR_ADVERTISERS_FIELD = $x(".//*[*[@class='key' and text()='Domain for advertisers:']]/*[@class='value']");
    public static final SelenideElement DOMAIN_FOR_ADVERTISERS_ICON = $(".imgBlockSource");

    //Ads.txt popups
    public static final SelenideElement CREATE_ADS_TXT_POPUP = $x(".//span[text()='Create Ads.txt file?']");
    public static final SelenideElement CREATE_ADS_TXT_POPUP_CREATE_BUTTON = $x(".//span[@class='ui-button-text' and text()='Create']");
    public static final SelenideElement CREATE_ADS_TXT_POPUP_CANCEL_BUTTON = $x("//span[@class='ui-button-text' and text()='Cancel']");
    public static final SelenideElement EDIT_ADS_TXT_POPUP = $x(".//span[text()='Edit Ads.txt file']");
    public static final SelenideElement EDIT_ADS_TXT_POPUP_DELETE_BUTTON = $x(".//span[text()='Delete']");
    public static final SelenideElement CONFIRM_POPUP_DELETE_BUTTON = $x(".//div[div[@id='confirmAdsTxtDeletePopup']]//span[text()='Delete']");
    public static final SelenideElement ADD_ADS_TXT_POPUP_DOMAIN_FIELD = $("#adsTxtDomain_add");
    public static final SelenideElement ADD_ADS_TXT_POPUP_AUTONOTIFICATIONS_CHECKBOX = $("#adsTxtOnlyManual_add");
    public static final SelenideElement ADD_ADS_TXT_POPUP_VIDEO_LIST_CHECKBOX = $("#adsTxtAddToVideo_add");
    public static final SelenideElement APPROVE_VALIDATION_POPUP = $(".ui-dialog-content");
    public static final SelenideElement SEND_MAIL_ON_SCHEDULE_CHECKBOX = $("#adsTxtSendMail");
    public static final SelenideElement SHOW_DASHBOARD_POPUP_CHECKBOX = $("#adsTxtShowPopup");
    public static final String MISSING_LINES_TEXTAREA = "ads_txt_missing";
    public static final SelenideElement SAVE_BUTTON = $x("//button/span[text()='Save']");

    //TAGS CLOUD
    public static final SelenideElement TAGS_CLOUD_OFF = $("[src*='tags-0.png']");
    public static final SelenideElement TAGS_CLOUD_ON = $("[src*='tags-1.png']");
    public static final ElementsCollection TAGS_ELEMENTS_LIST = $$(".tagsBlock [data-name]:not([style*='none'])");
    public static final SelenideElement TAGS_FORM_SAVE = $("#btnTagsSave");
    public static final SelenideElement TAGS_FORM_SEARCH = $("#searchTags");
    public static final SelenideElement SAVE_TAGS_FOR_ALL_CLIENT_SITES_INPUT = $("[name=applyToClient]");

    //Pre-Moderation(ads moderation)
    public static final String ADS_MODERATION_ICON = "#ads_moderation_button_%s";
    public static final SelenideElement ADS_MODERATION_POPUP = $(".popup-ads-moderation");
    public static final SelenideElement ADS_MODERATION_FIELDSET_CONFIG = $(".publishers-ads-moderation-form fieldset:nth-child(1)>p");
    public static final SelenideElement ADS_MODERATION_FIELDSET_NEW_CONFIG = $(".publishers-ads-moderation-form fieldset:nth-child(2)>p");
    public static final String ADS_MODERATION_COUNTRIES_LABELS = ".publishers-ads-moderation-form fieldset .country-label";
    public static final String ADS_MODERATION_WIDGETS_LABELS = ".publishers-ads-moderation-form fieldset .widget-label";
    public static final SelenideElement ADS_MODERATION_AUTO_APPROVE_CHECKBOX = $("#autoApproveHours-checkbox");
    public static final SelenideElement ADS_MODERATION_AUTO_APPROVE_HOURS_INPUT = $("#autoApproveHours");
    public static final SelenideElement ADS_MODERATION_WIDGETS_SELECT = $(".widgets-select");
    public static final SelenideElement ADS_MODERATION_COUNTRIES_SELECT = $(".countries-select");
    public static final SelenideElement ADS_MODERATION_ADD_BUTTON = $(".add_button");
    public static final String ADS_MODERATION_DEL_ICON = "[onclick='sites.adsModerationRemoveItem(%s)']";
    public static final SelenideElement ADS_MODERATION_SAVE_BUTTON = $x(".//div[div[@id='publishers-ads-moderation-config']]//*[text()='Save']");
    public static final SelenideElement ADS_MODERATION_DELETE_ADS_BUTTON = $("#btnDeleteAds");
    public static final SelenideElement ADS_MODERATION_POPUP_DELETE = $("#publishers-ads-moderation-remove-ads");
    public static final SelenideElement ADS_MODERATION_POPUP_DELETE_BUTTON = $("#DeleteBtn");

    //Post-Moderation
    public static final String POST_MODERATION_ICON = "img[id=post_moderation_%s]";
    public static final String          POST_MODERATION_STRING_ICON = "img[id^=post_moderation_]";
    public static final SelenideElement POST_MODERATION_FORM_COMMENT_INPUT = $("#post_moderation_comment");
    public static final String          POST_MODERATION_FORM_CHECKBOXES = "#postModerationPopup>form input";
    public static final SelenideElement POST_MODERATION_FORM_SELECT_ALL_CHECKBOX = $("#post_moderation_item_all");
    public static final SelenideElement POST_MODERATION_FORM_SAVE_BUTTON = $x(".//div[*[@id='postModerationPopup']]//button[*[text()='Save']]");
    public static final SelenideElement POST_MODERATION_FORM_FINISH_MODERATION_BUTTON = $x(".//div[*[@id='postModerationPopup']]//button[*[text()='Finish moderation']]");
    public static final SelenideElement POST_MODERATION_CONFIRM_APPROVE_BUTTON = $x(".//div[*[@id='confirmPostModerationPopup']]//span[text()='Approve']");
    public static final SelenideElement POST_MODERATION_CONFIRM_CANCEL_BUTTON = $x(".//div[*[@id='confirmPostModerationPopup']]//span[text()='Cancel']");
    public static final SelenideElement POST_MODERATION_POSTPONE_BUTTON = $x(".//button/span[text() = 'Postpone']");

    //Postpone postmoderation popup
    public static final SelenideElement POSTPONE_POPUP_CURRENT_CLICKS_FIELD = $("#site_current_clicks");
    public static final SelenideElement POSTPONE_POPUP_NEXT_CLICKS_INPUT = $("#postmoderation_next_clicks");
    public static final SelenideElement POSTPONE_POPUP_POSTPONE_BUTTON = $x(".//div[@id='postModerationPostponePopup']/following-sibling::div//span[@class='ui-button-text' and text()='Postpone']");

    ////////////////////////// INLINE ///////////////////////////////////////////////////////
    //Tier
    public static final SelenideElement INLINE_TIER_LABEL = $(".wrap_tier_id .value");
    public static final SelenideElement INLINE_TIER_SELECT = $("#siteTiers");
    public static final SelenideElement INLINE_TIER_SUBMIT = $x(".//div[div[@id='editSiteTierPopup']]//*[text()='Submit']");
    //Category
    public static final SelenideElement INLINE_CATEGORY_LABEL = $(".wrap_category .value");
    public static final SelenideElement INLINE_CATEGORY_SELECT = $("#category_select_dropdown");
    public static final SelenideElement INLINE_SOURCE_SELECT = $("#sourcesId");
    public static final SelenideElement INLINE_NEW_DOMAIN_TO_ADVERTISERS_INPUT = $("#newDomainSource");
    public static final SelenideElement INLINE_NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME = $("#lowVolume");
    public static final SelenideElement INLINE_NEW_DOMAIN_TO_ADVERTISERS_SUBMIT = $x(".//div[div[@id='sourcesIdPopup']]//span[contains(text(), 'Submit')]");
    public static final SelenideElement INLINE_LOW_VOLUME_SOURCE_CHECKBOX = $("#lowVolumeSource");
    public static final SelenideElement INLINE_PUSH_PROVIDERS_SELECT = $("#pushProviderId");
    public static final SelenideElement INLINE_CATEGORY_SUBMIT = $x(".//div[div[@id='editSiteCategoryPopup']]//*[text()='Submit']");
}
