package pages.cab.publishers.variables;

public class CabPublisherClientVariables {

    // Array of radiobuttons "Clients payouts schemas"
    public static String[] payoutSchemeTypes = {"standart", "manual", "autostandart"};
    // Array of radiobutton "Period settings"
    public static String[] typeAutoPaidManualScheme = {"period-days", "period-interval", "use-payout-amount"};
    // Array of values select-list (days/months)
    public static String[] autoPaidDelayType = {"days", "months"};
    public static final String commentForCorrectionWages = "Auto test for correction wages";
    public static final String valueForCorrectionWages = "0.01";
    public static String defaultRevenueSharePercent = "65";
    public static String newRevenueShareProc = "75";
    public static String autoReportEmailBody = "Hello,\n" +
            "\n" +
            "Your Daily report is ready.\n" +
            "Client ID - %s\n" +
            "Period - %s\n" +
            "\n" +
            "Please have a look at the attached files.\n" +
            "\n" +
            "Warm wishes,\n" +
            "Mgid Team";
}
