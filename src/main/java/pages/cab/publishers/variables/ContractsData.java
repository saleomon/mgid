package pages.cab.publishers.variables;

public class ContractsData {
    /**
     * widget Contracts type
     */
    public enum Contracts {
        CPC("CPC"),
        CPM("CPM"),
        ECPM("eCPM"),
        MONTH("Month"),
        RS("RS"),
        GUARANTEED("Guarantee"),
        MIXED("mixed");

        private String contractType;

        Contracts(String statuses) {
            this.contractType = statuses;
        }

        public String getContractType() {
            return contractType;
        }
    }

    public enum GuaranteedContracts{
        /**
         * widget Guaranteed contract types
         */
        DAILY("Daily"),
        MONTHLY("Monthly"),
        CPM("CPM"),
        ECPM("eCPM");

        private String guaranteedContractType;

        GuaranteedContracts(String types){this.guaranteedContractType = types;}

        public String getGuaranteedContractType() {
            return guaranteedContractType;
        }
    }
}
