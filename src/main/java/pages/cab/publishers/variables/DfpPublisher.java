package pages.cab.publishers.variables;

public class DfpPublisher {

    public enum DfpValues {
        ON("on"),
        OFF("off"),
        PART("part");

        private String dfpType;

        DfpValues(String statuses) {
            this.dfpType = statuses;
        }

        static public DfpValues getType(String pType) {
            for (DfpValues type: DfpValues.values()) {
                if (type.getDfpType().equals(pType)) {
                    return type;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getDfpType() {
            return dfpType;
        }
    }
}
