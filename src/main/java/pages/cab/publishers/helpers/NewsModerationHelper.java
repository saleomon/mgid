package pages.cab.publishers.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.cab.publishers.logic.CabNewsModeration.MassActions;
import core.helpers.BaseHelper;

import java.util.Arrays;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.NewsModerationLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class NewsModerationHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public NewsModerationHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * select all teasers checkboxes
     */
    public void selectAllTeasers() {
        clickInvisibleElementJs(SELECT_ALL_CHECKBOXES_LINK);
    }

    /**
     * choose mass actions
     */
    public void chooseMassActions(MassActions action) {
        MASS_ACTIONS_SELECT.shouldBe(visible).selectOptionByValue(action.getMassActionVal());
    }

    /**
     * submit mass actions
     */
    public void submitMassActions() {
        MASS_ACTION_SUBMIT.click();

        if(!helpersInit.getBaseHelper().alertIsShow()) {
            waitForAjaxLoader();
            checkErrors();
        }
    }

    /**
     * check adType for all teasers on page
     */
    public boolean checkAdTypeForAllTeasersOnPage(String adType) {
        sleep(1000);
        NEWS_ID_LABEL.shouldBe(visible);
        $$(INLINE_AD_TYPE).shouldBe(CollectionCondition.allMatch("sdf", i -> i.getAttribute("title").equalsIgnoreCase(adType)));
        return $$(INLINE_AD_TYPE).size() > 0;
    }

    /**
     * choose random AdType inline
     */
    public String chooseAdTypeInline() {
        $(INLINE_AD_TYPE).shouldBe(visible).hover().click();
        INLINE_AD_TYPE_SELECT.$("option").shouldBe(visible);
        String adType = helpersInit.getBaseHelper().selectRandomValue(INLINE_AD_TYPE_SELECT.shouldBe(visible));
        INLINE_AD_TYPE_SUBMIT.click();
        waitForAjaxLoader();
        checkErrors();
        return helpersInit.getBaseHelper().getTextAndWriteLog(adType);
    }

    /**
     * approve news
     */
    public void approveNews(int newsId) {
        getDataNewsId(newsId).scrollTo().$(APPROVE_ICON).click();
    }

    /**
     * click unselected all checkboxes
     */
    public void clickUnselectedAllCheckboxesMass() {
        clickInvisibleElementJs(UNSELECT_ALL_CHECKBOXES_LINK);
    }

    /**
     * approve news mass
     */
    public void approveNewsMass(int newsId) {
        clickUnselectedAllCheckboxesMass();
        clickInvisibleElementJs(getDataNewsId(newsId).$(MASS_CHECKBOX));
        chooseMassActions(MassActions.ACCEPT);
        submitMassActions();
    }

    /**
     * reject news mass
     */
    public void rejectNewsMass(int newsId) {
        clickUnselectedAllCheckboxesMass();
        clickInvisibleElementJs(getDataNewsId(newsId).$(MASS_CHECKBOX));
        chooseMassActions(MassActions.REJECT);
        MASS_REJECT_REASON_INPUT.sendKeys("test reject reason");
        submitMassActions();
    }

    /**
     * delete news mass
     */
    public void deleteNewsMass(int newsId) {
        clickUnselectedAllCheckboxesMass();
        clickInvisibleElementJs(getDataNewsId(newsId).$(MASS_CHECKBOX));
        chooseMassActions(MassActions.DELETE);
        MASS_DELETE_REASON_INPUT.sendKeys("test delete reason");
        submitMassActions();
    }

    /**
     * click reject icon
     */
    public void clickRejectIconNews(int newsId) {
        getDataNewsId(newsId).$(REJECT_ICON).click();
    }

    /**
     * set reject reason
     */
    public void setRejectReasonAndSubmit() {
        REJECT_REASON_INPUT.shouldBe(visible).sendKeys("test reject");
        REJECT_REASON_BUTTON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * click delete icon
     */
    public void clickDeleteIconNews(int newsId) {
        getDataNewsId(newsId).$(DELETE_ICON).click();
    }

    /**
     * set delete reason
     */
    public void setDeleteReasonAndSubmit(int newsId) {
        $(String.format(DELETE_REASON_INPUT, newsId)).shouldBe(visible).sendKeys("test delete");
        DELETE_REASON_BUTTON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * click 'Start' moderation icon
     */
    public void clickStartModerationIcon(int id) {
        clickInvisibleElementJs(getDataNewsId(id).$(START_MODERATION_ICON).shouldBe(visible).scrollTo());
        waitForAjax();
        checkErrors();
    }

    /**
     * click 'Stop' moderation icon
     */
    public void clickStopModerationIcon(int id) {
        clickInvisibleElementJs(getDataNewsId(id).$(STOP_MODERATION_ICON));
        waitForAjax();
        checkErrors();
    }

    /**
     * choose 'moderationStart' in mass action
     */
    public void startModerationMass(int... ids) {
        Arrays.stream(ids).forEach(id -> clickInvisibleElementJs(getDataNewsId(id).$(MASS_CHECKBOX)));
        chooseMassActions(MassActions.MODERATION_START);
        submitMassActions();
    }

    /**
     * choose 'moderationStart' in mass action
     */
    public void stopModerationMass(int... ids) {
        Arrays.stream(ids).forEach(id -> clickInvisibleElementJs(getDataNewsId(id).$(MASS_CHECKBOX)));
        chooseMassActions(MassActions.MODERATION_STOP);
        submitMassActions();
    }

    /**
     * get element with @data-id
     */
    public SelenideElement getDataNewsId(int newsId) {
        return $(".news[data-id='" + newsId + "']").shouldBe(visible);
    }

    /**
     * check Stop Moderation for Other User And Show Dialog And Close Him
     */
    public String checkStopModerationOtherUserAndShowDialogAndCloseHim() {
        String text = STOP_MODERATION_DIALOG.text();
        STOP_MODERATION_DIALOG_CLOSE_BUTTON.click();
        waitForAjax();
        checkErrors();
        return text;
    }

    /**
     * check can't edit inline image
     */
    public boolean checkCantEditImageInline() {
        INLINE_IMAGE_LABEL.shouldBe(visible).scrollTo().doubleClick();
        waitForAjax();
        checkErrors();
        return helpersInit.getBaseHelper().checkDatasetEquals(INLINE_IMAGE_LABEL.attr("data-allowedit"), "0") &&
                !INLINE_IMAGE_INPUT.isDisplayed();
    }

    /**
     * editing image inline
     */
    public boolean editImageInline() {
        INLINE_IMAGE_LABEL.scrollTo().doubleClick();
        waitForAjax();

        INLINE_IMAGE_CROP.shouldBe(visible);
        INLINE_IMAGE_FILE_LOCATOR.sendKeys(LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg");
        checkErrors();
        waitForAjax();
        INLINE_IMAGE_CROP.shouldBe(visible);

        INLINE_IMAGE_BUTTON.scrollTo().click();
        checkErrors();
        waitForAjax();
        log.info("Image was uploaded - 'Stonehenge.jpg'");
        return !INLINE_IMAGE_FILE_LOCATOR.isDisplayed();
    }

    /**
     * edit inline title
     */
    public String editTitleInline() {
        INLINE_TITLE_LABEL.doubleClick();
        String newTitle = "new title " + BaseHelper.getRandomWord(2);
        clearAndSetValue(INLINE_TITLE_INPUT.shouldBe(visible), newTitle + Keys.ENTER);
        waitForAjax();
        checkErrors();
        return newTitle;
    }

    /**
     * edit inline description
     */
    public String editDescriptionInline() {
        INLINE_DESCRIPTION_LABEL.doubleClick();
        String newVal = "new description " + BaseHelper.getRandomWord(2);
        clearAndSetValue(INLINE_DESCRIPTION_INPUT.shouldBe(visible), newVal + Keys.ENTER);
        waitForAjax();
        checkErrors();
        return newVal;
    }

    /**
     * edit inline category
     */
    public String editCategoryInline() {
        INLINE_CATEGORY_LABEL.doubleClick();
        String val = helpersInit.getBaseHelper().selectValueFromParagraphListJs(INLINE_CATEGORY_SELECT);
        String categoryName = INLINE_CATEGORY_SELECT.find(By.cssSelector("[data-id='" + val + "']")).innerText();
        INLINE_CATEGORY_SUBMIT.click();
        waitForAjax();
        checkErrors();
        return categoryName;
    }

    /**
     * edit inline lifetime
     */
    public String editLifetimeInline() {
        INLINE_LIFETIME_LABEL.doubleClick();
        String val = helpersInit.getBaseHelper().selectRandomValue(INLINE_LIFETIME_SELECT.shouldBe(visible));
        INLINE_CATEGORY_LABEL.click();
        waitForAjax();
        checkErrors();
        refresh();
        return val;
    }

    /**
     * edit inline landing
     */
    public String editLandingInline() {
        INLINE_LANDING_LABEL.shouldBe(visible).doubleClick();
        String val = helpersInit.getBaseHelper().selectRandomValue(INLINE_LANDING_SELECT.shouldBe(visible));
        INLINE_CATEGORY_LABEL.click();
        waitForAjax();
        checkErrors();
        refresh();
        return val;
    }

    /**
     * help method for default mass actions with only select
     */
    public String editFieldMass(MassActions action, SelenideElement select) {
        selectAllTeasers();
        chooseMassActions(action);
        String val = helpersInit.getBaseHelper().selectRandomValue(select);
        submitMassActions();
        return val;
    }

    /**
     * edit inline url
     * <a href="http://test12.com">http://test12.com</a>?... - <a href="http://test12.com/dfdf">http://test12.com/dfdf</a>?...
     */
    public String editUrlInline() {
        String url = INLINE_URL_LABEL.text() + "/" + BaseHelper.getRandomWord(2);
        INLINE_AD_TYPE_LINK.scrollTo();
        INLINE_URL_LABEL.hover().doubleClick();
        clearAndSetValue(INLINE_URL_INPUT.shouldBe(visible), url + Keys.ENTER);
        waitForAjax();
        checkErrors();
        return url;
    }

    /**
     * set 'campaign id' filter
     */
    public void fillFilterCampaignId(int campaignId) {
        FILTER_CAMPAIGN_ID_INPUT.sendKeys(String.valueOf(campaignId));
    }

    /**
     * click 'Filter' button
     */
    public void filterSubmit() {
        FILTER_SUBMIT.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * check mass action select has only custom fields
     * .count()+1 - because MASS_ACTIONS_SELECT has [value='none']
     */
    public boolean checkMassActionFilterHaveOnlyCustomField(MassActions... actions){
        return helpersInit.getBaseHelper().checkDataset((int) (Arrays.stream(actions).filter(action -> MASS_ACTIONS_SELECT.find("option[value='" + action.getMassActionVal() + "']").shouldBe(exist).exists()).count()+1),
                MASS_ACTIONS_SELECT.findAll("option").size());
    }

    /**
     * helper check custom value for all news on page
     */
    public boolean checkCustomValueForAllNewsOnPage(String element, String value) {
        ElementsCollection list = $$(element);
        list.shouldBe(CollectionCondition.allMatch("checkCustomValueForAllNewsOnPage", i -> i.getAttribute("data-value").equals(value)));
        return list.size() > 0;
    }
}
