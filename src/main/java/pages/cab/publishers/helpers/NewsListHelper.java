package pages.cab.publishers.helpers;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.cab.publishers.logic.CabNews;
import core.helpers.BaseHelper;

import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.NewsAddLocators.CLEAR_FIELD_BUTTON;
import static pages.cab.publishers.locators.NewsListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;


public class NewsListHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public NewsListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * check 'Category'
     */
    public boolean checkCategory(String categoryId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORY_INLINE_FIELD.attr("data-value")).equals(categoryId);
    }

    /**
     * check 'Domain'
     */
    public boolean checkDomain(String domain) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(URL_INLINE_LABEL.text()).contains(domain);
    }

    /**
     * check 'Title'
     */
    public boolean checkTitle(String title) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TITLE_INLINE_LABEL.text()).equals(title);
    }

    public boolean checkOriginalTitleIcon(boolean state) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(state ? ORIGINAL_TITLE_ICON.first().isDisplayed() : $$(NOT_ORIGINAL_TITLE_ICON).first().isDisplayed());
    }

    /**
     * check 'Description'
     */
    public boolean checkDescription(String description) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DESCRIPTION_INLINE_LABEL.text()).equals(description);
    }

    /**
     * check 'life time'
     */
    public boolean checkLifeTime(String lifeTime) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LIFETIME_INLINE_LABEL.attr("data-value")).equals(lifeTime);
    }

    /**
     * check 'News type'
     */
    public boolean checkNewsType(String newsType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NEWS_TYPE_INLINE_LABEL.attr("title")).equalsIgnoreCase(newsType);
    }

    /**
     * check 'political preferences'
     */
    public boolean checkPoliticalPreferences(String politicalPreferences) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(POLITICAL_PREFERENCES_ICON.attr("data-value")).equals(politicalPreferences);
    }

    /**
     * check status news
     */
    public boolean checkStatusNews(CabNews.Statuses status) {
        if (STATUS_FIELD.text().contains(status.getStatusesValue())) {

            return switch (status) {
                case PENDING -> helpersInit.getBaseHelper().getTextAndWriteLog(APPROVE_ICON.isDisplayed() &&
                        REJECT_ICON.isDisplayed());
                case ACTIVE -> helpersInit.getBaseHelper().getTextAndWriteLog(!APPROVE_ICON.isDisplayed() &&
                        REJECT_ICON.isDisplayed());
                case DRAFT -> helpersInit.getBaseHelper().getTextAndWriteLog(!CANCEL_MODERATION_ICON.isDisplayed() &
                        SEND_ON_MODERATION_ICON.isDisplayed() & !RESTORE_FROM_TRASH_ICON.isDisplayed());
                case QUARANTINE -> helpersInit.getBaseHelper().getTextAndWriteLog(!NEWS_EDIT_ICON.isDisplayed() &
                        !SEND_ON_MODERATION_ICON.isDisplayed() & !CANCEL_MODERATION_ICON.isDisplayed());
            };

        }
        return false;
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * get news id from label
     */
    public List<String> getNewsIdFromLabel() {
        return NEWS_ID_LABEL.texts();
    }

    /**
     * click delete icon
     */
    public void clickDeleteIconNews(int newsId) {
        $(String.format(DELETE_ICON, newsId)).shouldBe(visible).scrollTo().click();
    }

    /**
     * set delete reason
     */
    public void setDeleteReasonAndSubmit() {
        DELETE_REASON_INPUT.shouldBe(visible).sendKeys("test delete");
        DELETE_REASON_BUTTON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * Check edit news icon visibility
     */
    public boolean isDisplayedNewsEditButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NEWS_EDIT_ICON.isDisplayed());
    }

    /**
     * Check can't edit inline image
     */
    public boolean checkCantEditImageInline() {
        INLINE_IMAGE_LABEL.shouldBe(visible).scrollTo().doubleClick();
        waitForAjax();
        checkErrors();
        return helpersInit.getBaseHelper().checkDatasetEquals(INLINE_IMAGE_LABEL.attr("data-allowedit"), "0") &&
                !INLINE_IMAGE_INPUT.isDisplayed();
    }

    /**
     * Helper for check can't edit field news type inline
     */
    public boolean checkCantEditNewsTypeInline() {
        INLINE_NEWS_TYPE_LABEL.shouldBe(visible).doubleClick();
        waitForAjax();
        return !INLINE_NEWS_TYPE_SELECT.isDisplayed();
    }

    /**
     * Edit category inline
     */
    public void editCategoryInline() {
        CATEGORY_INLINE_FIELD.doubleClick();
        String val = helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_INLINE_SELECT);
        String categoryName = CATEGORY_INLINE_SELECT.find(By.cssSelector("[data-id='" + val + "']")).innerText();
        CATEGORY_INLINE_SUBMIT.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Edit lifetime inline
     */
    public void editLifetimeInline() {
        LIFETIME_INLINE_LABEL.doubleClick();
        String val = helpersInit.getBaseHelper().selectRandomValue(LIFETIME_INLINE_SELECT.shouldBe(visible));
        CATEGORY_INLINE_FIELD.click();
        waitForAjax();
        checkErrors();
        refresh();
    }

    /**
     * Edit news type inline
     */
    public void chooseNewsTypeInline() {
        $(INLINE_NEWS_TYPE_LABEL).shouldBe(visible).hover().click();
        INLINE_NEWS_TYPE_SELECT.$("option").shouldBe(visible);
        String newsType = helpersInit.getBaseHelper().selectRandomValue(INLINE_NEWS_TYPE_SELECT.shouldBe(visible));
        INLINE_NEWS_TYPE_SUBMIT.click();
        waitForAjaxLoader();
        checkErrors();
        helpersInit.getBaseHelper().getTextAndWriteLog(newsType);
    }

    /**
     * Edit landing types inline
     */
    public String editLandingTypesInline() {
        LANDING_TYPES_INLINE_LABEL.shouldBe(visible).doubleClick();
        String val = helpersInit.getBaseHelper().selectRandomValue(LANDING_INLINE_TYPES_SELECT.shouldBe(visible));
        CATEGORY_INLINE_FIELD.click();
        waitForAjax();
        checkErrors();
        refresh();
        return val;
    }

    /**
     * Edit URL inline
     */
    public void editUrlInline() {
        String url = URL_INLINE_LABEL.text() + "/" + BaseHelper.getRandomWord(2);
        INLINE_NEWS_TYPE_LINK.scrollTo();
        URL_INLINE_LABEL.hover().doubleClick();
        CLEAR_FIELD_BUTTON.click();
        URL_INLINE_INPUT.sendKeys(url + Keys.ENTER);
        waitForAjax();
        checkErrors();
    }
}
