package pages.cab.publishers.helpers;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.publishers.locators.NewsAddLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class NewsAddHelper {
    private final HelpersInit helpersInit;
    private final Logger log;
    private boolean isGenerateNewValues = false;

    public NewsAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public NewsAddHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    /**
     * choose 'Category'
     */
    public String chooseCategory(String categoryId) {
        if (categoryId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
        }
        return helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
    }

    /**
     * set 'Link'(domain) input
     */
    public void setDomain(String domain) {
        if (!checkDomain(domain)) clearAndSetValue(LINK_INPUT, domain);
    }

    public void isOriginalTitle(boolean state) {
        markUnMarkCheckbox(state, IS_ORIGINAL_TITLE_CHECKBOX);
    }

    /**
     * set 'Title'
     */
    public String setTitle(String title) {
        String titleNews = (title == null || isGenerateNewValues) ?
                "Titlenews" + BaseHelper.getRandomWord(3) :
                title;
        clearAndSetValue(TITLE_INPUT, titleNews);
        return titleNews;
    }

    /**
     * set 'Description'
     */
    public String setDescription(String description) {
        String desc = (description == null || isGenerateNewValues) ?
                "descriptionnews" + BaseHelper.getRandomWord(2) :
                description;
        clearAndSetValue(DESCRIPTION_INPUT, desc);
        return desc;
    }

    /**
     * load image by imageLink
     */
    public void loadImageByUrl(String imageLink) {
        clearAndSetValue(IMAGE_INPUT, imageLink + Keys.ENTER);
        IMAGE_LABEL.click();
        waitForAjax();
        log.info("loadImage");
    }

    /**
     * choose 'life time'
     */
    public String chooseLifeTime(String lifeTime) {
        if (lifeTime == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(LIFETIME_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(LIFETIME_SELECT, lifeTime);
    }

    /**
     * choose 'news type'
     */
    public String chooseNewsType(String newsType) {
        if (newsType == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(AD_TYPES_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(AD_TYPES_SELECT, newsType);
    }

    /**
     * choose 'political preferences'
     */
    public String choosePoliticalPreferences(String politicalPreferences) {
        if (politicalPreferences == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(POLITICAL_PREFERENCES_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(POLITICAL_PREFERENCES_SELECT, politicalPreferences);
    }

    /**
     * save news
     */
    public void saveNews() {
        log.info("saveNews");
        clickInvisibleElementJs(SAVE_BUTTON);
        checkErrors();
        waitForAjax();
    }

    /**
     * check 'Category'
     */
    public boolean checkCategory(String categoryId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORY_SELECT.find(By.cssSelector("[data-id='" + categoryId + "']")).attr("class").contains("selected-cat"));
    }

    /**
     * check 'Link'
     */
    public boolean checkDomain(String domain) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LINK_INPUT.text()).contains(domain);
    }

    /**
     * check 'Title'
     */
    public boolean checkTitle(String title) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TITLE_INPUT.text()).equals(title);
    }

    public boolean checkOriginalTitle(boolean state) {
        return checkIsCheckboxSelected(state, IS_ORIGINAL_TITLE_CHECKBOX);
    }

    /**
     * check 'Advertising text'
     */
    public boolean checkDescription(String description) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DESCRIPTION_INPUT.text()).equals(description);
    }

    /**
     * News lifetime
     */
    public boolean checkLifeTime(String lifeTime) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(LIFETIME_SELECT, lifeTime));
    }

    /**
     * check 'Political preferences'
     */
    public boolean checkPoliticalPreferences(String politicalPreferences) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(POLITICAL_PREFERENCES_SELECT, politicalPreferences));
    }

    /**
     * get 'Curator' (manager)
     */
    public String getCurator() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CURATOR_LABEL.shouldBe(visible).text().trim());
    }
}
