package pages.cab.publishers.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import core.base.HelpersInit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.ClientsListLocators.*;
import static pages.cab.publishers.variables.CabPublisherClientVariables.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class ClientsListHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    public ClientsListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public ArrayList<Integer> chosenPeriods;
    private String currentInterval;
    private String payoutAmount;
    private String currentDelay;
    private String currentPeriod;

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return BLOCK_ADS_ICON.isDisplayed();
    }

    /**
     * Save payout scheme
     */
    public void savePayoutScheme() {
        AUTO_PAID_SAVE_BUTTON.click();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        checkErrors();
    }

    /**
     * Submit payout scheme form
     */
    public void submitPayoutScheme() {
        AUTO_PAID_SAVE_BUTTON.click();
        checkErrors();
    }

    /**
     * Open clients payouts settings
     */
    public void clickAutoPaidIcon() {
        clickInvisibleElementJs(AUTO_PAID_ICON);
        STANDARD_PAYOUT_SCHEME_RADIO.shouldBe(visible, ofSeconds(10));
    }

    /**
     * Check if clients payout settings is saved correctly
     */
    public boolean checkAutoPaid(String purseId, String currentPayoutScheme) {
        boolean flag = false;
        try {
            if ($("#scheme-" + currentPayoutScheme).isSelected()) {
                if (AUTO_PAID_AUTOSTANDART_SCHEME_RADIO.isSelected() && checkSelectedValue("purse", purseId)
                        && checkSelectedValue("period", currentPeriod)) {
                    flag = true;
                } else if (AUTO_PAID_MANUAL_SCHEME_RADIO.isSelected() && checkSelectedValue("purse", purseId)
                        && Objects.requireNonNull(AUTO_PAID_DELAY_INPUT.getAttribute("value")).equals(currentDelay)) {
                    if (AUTO_PAID_PERIOD_DAYS_RADIO.isSelected()) {
                        if (chosenPeriods.stream().filter(i -> LIST_PERIOD_DAYS_INPUTS.get(i).isSelected()).count() == chosenPeriods.size()) {
                            flag = true;
                        }
                    } else if (AUTO_PAID_PERIOD_INTERVAL_RADIO.isSelected()
                            && Objects.requireNonNull(AUTO_PAID_INTERVAL_INPUT.getAttribute("value")).equals(currentInterval)) {
                        flag = true;
                    } else {
                        if (AUTO_PAID_PAYOUT_AMOUNT_RADIO.isSelected()
                                && Objects.requireNonNull(AUTO_PAID_PAYOUT_AMOUNT_INPUT.getAttribute("value")).equals(payoutAmount)) {
                            flag = true;
                        }
                    }
                } else {
                    if (AUTO_PAID_STANDART_SCHEME_RADIO.isSelected()) {
                        flag = true;
                    }
                }
            }
        } catch (Exception e) {
            log.error("Catch " + e);
        }
        return flag;
    }

    /**
     * Check if select expected value in select-list
     */
    public boolean checkSelectedValue(String name, String val) {
        return executeJavaScript("return arguments[0].getAttribute('selected')", $("[name='" + name + "'] [value='" + val + "']")) != null;
    }


    /**
     * Edit clients auto-payouts
     */
    public HashMap<String, String> editAutoPaid() {
        // открытие поп-апа "Настройка схемы выплат клиенту"
        HashMap<String, String> paySettings = new HashMap<>();
        String purseId = "";
        clickAutoPaidIcon();

        // Select random payout scheme
        String currentPayoutScheme = payoutSchemeTypes[randomNumbersInt(payoutSchemeTypes.length)];
        paySettings.put("currentPayoutScheme", currentPayoutScheme);
        log.info("currentScheme - " + currentPayoutScheme);
        $("#scheme-" + currentPayoutScheme).click();

        // Set settings for chosen scheme
        if (currentPayoutScheme.equals("autostandart")) {
            purseId = helpersInit.getBaseHelper().selectRandomValue(AUTO_PAID_PURSES_SELECT);
            currentPeriod = helpersInit.getBaseHelper().selectRandomValue(AUTO_PAID_PERIOD_SELECT);
        } else if (currentPayoutScheme.equals("manual")) {
            purseId = chooseManualSchemeInCab();
        }
        paySettings.put("currentPayoutScheme", currentPayoutScheme);
        paySettings.put("purseId", purseId);

        log.info("Save payout scheme");
        savePayoutScheme();
        return paySettings;
    }

    /**
     * Set settings for manual payout scheme
     * select period, interval, sum
     */
    public String chooseManualSchemeInCab() {
        log.info("Select random manual scheme");
        String currentManualScheme = typeAutoPaidManualScheme[randomNumbersInt(typeAutoPaidManualScheme.length)];
        log.info("currentManualScheme - " + currentManualScheme);
        $("#" + currentManualScheme).shouldBe(visible).click();

        log.info("Select actions for each manual scheme");
        if (currentManualScheme.equals("period-days")) {
            LIST_PERIOD_DAYS_INPUTS
                    .filter(selected)
                    .asFixedIterable()
                    .forEach(SelenideElement::click);

            chosenPeriods = helpersInit.getBaseHelper().getRandomCountValue(LIST_PERIOD_DAYS_INPUTS.size(), 3);
            chosenPeriods.forEach(k -> LIST_PERIOD_DAYS_INPUTS.get(k).click());
        } else if (currentManualScheme.equals("period-interval")) {
            clearAndSetValue(AUTO_PAID_INTERVAL_INPUT, currentInterval = randomNumberFromRange(1, 365));
        } else {
            clearAndSetValue(AUTO_PAID_PAYOUT_AMOUNT_INPUT, payoutAmount = randomNumberFromRange(100, 2000));
        }

        log.info("Select random purse");
        String purseId = helpersInit.getBaseHelper().selectRandomValue(AUTO_PAID_PURSES_SELECT);

        log.info("Set payout date settings (chose - day/month and date)");
        String delayValue = autoPaidDelayType[randomNumbersInt(autoPaidDelayType.length)];
        helpersInit.getBaseHelper().selectCustomValue(AUTO_PAID_DELAY_TYPE_SELECT, delayValue);
        currentDelay = delayValue.equals("days") ? randomNumberFromRange(1, 365) : randomNumberFromRange(1, 12);
        clearAndSetValue(AUTO_PAID_DELAY_INPUT, currentDelay);
        return purseId;
    }

    /**
     * Get client payout amount
     */
    public double getClientMgWalletBalance() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(Double.parseDouble(CLIENT_MG_WALLET_BALANCE.shouldBe(visible, ofSeconds(10)).text().substring(1)));
    }

    /**
     * Get client payout amount
     */
    public double getClientMgWalletAmount() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(Double.parseDouble(CLIENT_MG_WALLET_AMOUNT.text().substring(1)));
    }

    /**
     * Get "Unconfirmed wages" value from client
     */
    public double getClientsUnconfirmedWages() {
        String value = UNCONFIRMED_WAGES_VALUE.text();
        if (value.contains("(")) {
            value = value.substring(0, value.indexOf("(")).trim();
        }
        log.info("Current wages value: " + value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(Double.parseDouble(value.replace("$", "").replace(",", ".")));
    }

    /**
     * Click "Transfer to MG-Wallet" icon
     */
    public void clickTransferToMgIcon() {
        TRANSFER_TO_MG_WALLET_ICON.shouldBe(visible).scrollTo();
        clickInvisibleElementJs(TRANSFER_TO_MG_WALLET_ICON);
        checkErrors();
        log.info("Transfer to MG-wallet icon clicked.");
        TRANSFER_TO_MG_WALLET_POPUP.shouldBe(visible);
    }

    /**
     * Input the value of requested sum
     */
    public void inputAmountForRequestedSum(String payOutValue) {
        PAYOUT_AMOUNT_INPUT.shouldBe(exist, visible);
        clearAndSetValue(PAYOUT_AMOUNT_INPUT, payOutValue);
        helpersInit.getBaseHelper().getTextAndWriteLog("Amount of payout inputed.");
        helpersInit.getBaseHelper().getTextAndWriteLog(payOutValue);
    }

    /**
     * Input the comment
     */
    public void inputCommentForMgTransferMoney(String comment) {
        clearAndSetValue(PAYOUT_COMMENT_INPUT, comment);
        helpersInit.getBaseHelper().getTextAndWriteLog(comment);
    }

    /**
     * Set auto-approve checkbox
     */
    public void setApproveCheckbox(boolean autoApprove) {
        IMMEDIATE_REQUEST_APPROVAL_CHECKBOX.setSelected(autoApprove);
    }

    /**
     * Отправка формы заказа выплаты
     */
    public void submitAddPaymentPopup() {
        ADD_PAYMENT_BUTTON.click();
        checkErrors();
        log.info("Correction popup submited.");
    }

    /**
     * Проверка наличия ошибки валидации и ее содержания
     */
    public String getPaymentValidationError() {
        ADD_PAYMENT_POPUP_ERROR.shouldBe(visible);
        log.info("Error is displayed - " + ADD_PAYMENT_POPUP_ERROR.isDisplayed());
        return ADD_PAYMENT_POPUP_ERROR.getText();
    }

    /**
     * Get client payout amount
     */
    public double getClientPayoutsAmount() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(Double.parseDouble(CLIENT_PAYOUTS_AMOUNT.shouldBe(visible, ofSeconds(10)).text().substring(1)));
    }

    /**
     * Клик по иконке заказа выплаты
     * Проверка появления попапа
     */
    public void clickMoneyWithdrawIcon() {
        clickInvisibleElementJs(MONEY_WIDHDARW_ICON);
        checkErrors();
        log.info("Money withdraw icon clicked.");
        ADD_PAYMENT_POPUP.shouldBe(visible);
    }

    /**
     * Select purse in dropdown menu
     */
    public void selectPurse(String id) {
        ADD_PAYMENT_PURSE_SELECT.shouldBe(visible, ofSeconds(8)).selectOptionContainingText(id);
        log.info("Purse selected - " + id);
    }

    /**
     * Ввод величины суммы выплаты в поле
     */
    public void inputAmountForWithdrawMoney(String payOutValue) {
        ADD_PAYMENT_PURSE_SELECT.shouldBe(visible);
        clearAndSetValue(PAYOUT_AMOUNT_INPUT, payOutValue);
        log.info("Amount of payout inputed.");
    }

    /**
     * Input comment in text field
     */
    public void inputCommentForWithdrawMoney(String comment) {
        PAYOUT_COMMENT_INPUT.shouldBe(visible);
        clearAndSetValue(PAYOUT_COMMENT_INPUT, comment);
        log.info("Comment for payout has been input.");
    }


    /**
     * Клик по иконке корректировки баланса
     * Проверка появления попапа
     */
    public void clickCorrectWagesIcon() {
        clickInvisibleElementJs(ADJUSTMENT_WAGES_ICON);
        checkErrors();
        log.info("ADJUSTMENT_WAGES_ICON clicked.");
        CORRECTION_WAGES_POPUP.shouldBe(visible);
    }


    /**
     * Ввод величины корректировки баланса в поле
     */
    public void inputAmount(String amountValue) {
        clearAndSetValue(CORRECTION_POPUP_AMOUNT_INPUT, amountValue);
        log.info("Amount of correction wage inputed.");
    }

    /**
     * Ввод комментария в поле
     */
    public void inputComment(String comment) {
        clearAndSetValue(CORRECTION_POPUP_COMMENT_INPUT, comment);
        log.info("Comment for correction wage inputed.");
    }


    /**
     * Submit 'Correction Popup'
     */
    public void submitCorrectionPopup() {
        CORRECTION_POPUP_SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
        CORRECTION_POPUP_SUBMIT_BUTTON.shouldBe(hidden, ofSeconds(10));
        log.info("Correction popup submitted.");
    }

    /**
     * Парсинг текущего значения нераспределенного заработка
     */
    public double getCurrentWagesValue() {
        String wagesValue = UNCONFIRMED_WAGES_VALUE.text();
        if (wagesValue.contains("(")) {
            wagesValue = wagesValue.substring(0, wagesValue.indexOf("(")).trim();
        }
        log.info("Current wages value: " + wagesValue);
        return Double.parseDouble(wagesValue.replace("$", "").replace(",", "."));
    }

    /**
     * Cliсk 'Submit' button in 'Correction popup'
     */
    public void clickButtonSubmitInCorrectionPopup() {
        CORRECTION_POPUP_SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
        log.info("Button 'Submit' clicked.");
    }

    /**
     * Проверка наличия ошибки валидации и ее содержания
     */
    public boolean checkValidationErrors(String error) {
        CORRECTION_POPUP_ERROR.shouldBe(visible);
        log.info("Error is displayed - " + CORRECTION_POPUP_ERROR.isDisplayed());
        return helpersInit.getBaseHelper().checkDatasetEquals(error, CORRECTION_POPUP_ERROR.getText());
    }

    public boolean checkStateAutoReportColumns(String statsTypeValue, String autoReportColumnsValue, boolean isAfterEdit) {
        $x(String.format(AUTO_REPORT_COLUMNS_MULTISELECT, statsTypeValue)).shouldBe(visible).click();
        ElementsCollection columnsCheckboxes = $$x(String.format(AUTO_REPORT_COLUMNS_MULTISELECT, statsTypeValue) + AUTO_REPORT_COLUMNS_CHECKBOXES);
        if (!isAfterEdit) {
            columnsCheckboxes.shouldBe(CollectionCondition.allMatch("checkStateAutoReportColumns", WebElement::isSelected));
            return true;
        } else {
            return helpersInit.getBaseHelper().getTextAndWriteLog(
                    columnsCheckboxes.filterBy(value(autoReportColumnsValue)).first().isSelected());
        }
    }

    public boolean checkAutoReportTime(String autoReportTimeValue) {
        AUTO_REPORT_SUBMIT_BUTTON.shouldBe(visible);
        return helpersInit.getBaseHelper().checkDatasetEquals(
                autoReportTimeValue, Objects.requireNonNull(AUTO_REPORT_TIME_SELECT.shouldBe(visible).getSelectedValue()));
    }

    public boolean checkStateAutoReportPeriodicity(String periodicityValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                AUTO_REPORT_PERIODICITY_LABELS.filterBy(text(periodicityValue)).first().find("input").isSelected());
    }

    public boolean checkAutoReportEmails(String[] autoReportEmailsValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(autoReportEmailsValue[0], Objects.requireNonNull(AUTO_REPORT_EMAIL_INPUT.getValue())) &&
                helpersInit.getBaseHelper().checkDatasetEquals(autoReportEmailsValue[1], Objects.requireNonNull(AUTO_REPORT_EMAIL_SECOND_INPUT.getValue()));
    }

    public String selectAutoReportTime(String... autoReportTimeValue) {
        if (autoReportTimeValue.length > 0) {
            AUTO_REPORT_TIME_SELECT.selectOptionByValue(autoReportTimeValue[0]);
            return autoReportTimeValue[0];
        } else
            return helpersInit.getBaseHelper().selectRandomValue(AUTO_REPORT_TIME_SELECT);
    }

    public String[] setAutoReportEmails(boolean editFlag) {
        String[] autoReportEmails = new String[2];
        for (int i = 0; i < autoReportEmails.length; i++) {
            autoReportEmails[i] = helpersInit.getGenerateDataHelper().generateEmail();
        }
        clearAndSetValue(AUTO_REPORT_EMAIL_INPUT, autoReportEmails[0]);
        if (!editFlag) AUTO_REPORT_ADD_EMAIL_BUTTON.click();
        clearAndSetValue(AUTO_REPORT_EMAIL_SECOND_INPUT, autoReportEmails[1]);
        return autoReportEmails;
    }

    public String selectAutoReportColumns(String statsTypeValue, boolean isEdit) {
        if (!isEdit) {
            $x(String.format(AUTO_REPORT_STATS_BY_SITES_CHECKBOX, statsTypeValue)).shouldBe(visible).click();
            return "all";
        } else {
            $x(String.format(AUTO_REPORT_COLUMNS_MULTISELECT, statsTypeValue)).shouldBe(visible).hover();
            $x(String.format(AUTO_REPORT_COLUMNS_MULTISELECT, statsTypeValue)).click();
            $x(String.format(AUTO_REPORT_STATS_BY_SITES_CHECKBOX, statsTypeValue)).shouldBe(visible).click();
            ElementsCollection columnsCheckboxes = $$x(String.format(AUTO_REPORT_COLUMNS_MULTISELECT, statsTypeValue) + AUTO_REPORT_COLUMNS_CHECKBOXES);
            SelenideElement selectedColumn = columnsCheckboxes.get(randomNumbersInt(columnsCheckboxes.size()));
            String selectedValue = selectedColumn.getValue();
            selectedColumn.sibling(0).click();
            return selectedValue;
        }
    }

    public String setAutoReportPeriodicity(String... autoReportTimeValue) {
        //unselect all checkboxes
        AUTO_REPORT_PERIODICITY_CHECKBOXES
                .filter(selected)
                .asFixedIterable()
                .forEach(i -> i.sibling(0).click());
        SelenideElement periodicityValue;
        if (autoReportTimeValue.length > 0) {
            periodicityValue = AUTO_REPORT_PERIODICITY_CHECKBOXES.filterBy(value(autoReportTimeValue[1])).first();
            periodicityValue.sibling(0).click();
        } else {
            periodicityValue = AUTO_REPORT_PERIODICITY_CHECKBOXES.get(randomNumbersInt(AUTO_REPORT_PERIODICITY_CHECKBOXES.size()));
            periodicityValue.sibling(0).click();
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(periodicityValue.closest("label").text());
    }
}
