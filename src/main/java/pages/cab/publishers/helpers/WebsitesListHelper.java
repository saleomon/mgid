package pages.cab.publishers.helpers;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.publishers.variables.DfpPublisher.DfpValues;
import core.helpers.BaseHelper;

import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.WebsitesListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class WebsitesListHelper {

    private final HelpersInit helpersInit;
    private final Logger log;

    private boolean isSetSomeFields = false;

    public WebsitesListHelper(Logger log, HelpersInit helpersInit) {

        this.helpersInit = helpersInit;
        this.log = log;
    }

    public WebsitesListHelper setSetSomeFields(boolean isSetSomeFields) {
        this.isSetSomeFields = isSetSomeFields;
        return this;
    }

    /**
     * check 'Name'
     */
    public boolean checkDomain(String domain) {
        return (isSetSomeFields && domain == null)
                || helpersInit.getBaseHelper().checkDatasetEquals(NAME_FIELD.scrollTo().text(), domain);
    }

    /**
     * check 'Manager'
     */
    public boolean checkManager(String manager) {
        return helpersInit.getBaseHelper().checkDatasetEquals(MANAGER_FIELD.text(), manager);
    }

    public boolean checkMirror(String mirror) {
        return helpersInit.getBaseHelper().checkDatasetEquals(MIRROR_FIELD.text(), mirror);
    }

    public boolean checkDomainForAdvertiser(String domainForAdvertiser) {
        return helpersInit.getBaseHelper().checkDatasetEquals(DOMAIN_FOR_ADVERTISERS_FIELD.text(), domainForAdvertiser);
    }

    public boolean isDisplayedDomainForAdvertiserIcon(boolean lowVolumeDomain) {
        return helpersInit.getBaseHelper().checkDataset(lowVolumeDomain, DOMAIN_FOR_ADVERTISERS_ICON.isDisplayed());
    }

    /**
     * get 'Website ID'
     */
    public String getSiteId() {
        if (ID_FIELD.isDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(ID_FIELD.text());
        }
        return null;
    }

    /**
     * check field 'Category'
     */
    public boolean checkCategory(String categoryId) {
        return (isSetSomeFields && categoryId == null)
                || helpersInit.getBaseHelper().checkDatasetEquals(CATEGORY_FIELD.attr("data-category-id"), categoryId);
    }

    /**
     * check field 'Site language'
     */
    public boolean checkLanguage(String languageId) {
        return (isSetSomeFields && languageId == null)
                || helpersInit.getBaseHelper().checkDatasetEquals(LANGUAGE_FIELD.attr("data-languageid"), languageId);
    }

    /**
     * check field 'Website Tier'
     */
    public boolean checkTier(String tier) {
        if(!isSetSomeFields && tier != null) {
            TIER_FIELD.shouldBe(CollectionCondition.allMatch("checkTier", i -> i.getAttribute("data-tierid").equals(tier)));
        }
        return true;
    }

    /**
     * check field 'Bundles'
     */
    public boolean checkBundles(ArrayList<String> bundles) {
        List list;
        if (isSetSomeFields && (bundles == null | bundles.size() == 0)) return true;


        if (BUNDLE_FIELD.shouldBe(visible).text().equals("-")) {
            list = new ArrayList();
        } else {
            list = Arrays.stream(BUNDLE_FIELD.shouldBe(visible).text().split(",")).map(String::trim).collect(Collectors.toList());
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(list).containsAll(bundles));
    }

    /**
     * edit inline 'Tier'
     */
    public String editTierInline(String tier) {
        if (tier == null) {
            clickInvisibleElementJs(INLINE_TIER_LABEL.shouldBe(visible).scrollTo());
            String val = helpersInit.getBaseHelper().selectRandomTextInHideSelectOptions(INLINE_TIER_SELECT);
            INLINE_TIER_SUBMIT.click();
            waitForAjax();
            checkErrors();
            if (helpersInit.getMessageHelper().isSuccessMessagesCab()) {
                return helpersInit.getBaseHelper().getTextAndWriteLog(val);
            }
            return "";
        }
        clickInvisibleElementJs(INLINE_TIER_LABEL.shouldBe(visible).scrollTo());
        helpersInit.getBaseHelper().selectCustomValInHideSelectOptions(INLINE_TIER_SELECT, tier);
        INLINE_TIER_SUBMIT.click();
        waitForAjax();
        checkErrors();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        return tier;
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * Delete ads.txt for website
     */
    public boolean deleteAdsTxt(String websiteId) {
        $(String.format(EDIT_ADS_TXT_ICON, websiteId)).shouldBe(visible).click();
        EDIT_ADS_TXT_POPUP.shouldBe(visible, ofSeconds(10));
        EDIT_ADS_TXT_POPUP_DELETE_BUTTON.shouldBe(visible).click();
        CONFIRM_POPUP_DELETE_BUTTON.shouldBe(visible).click();
        $(String.format(ADD_ADS_TXT_ICON, websiteId)).shouldBe(visible, ofSeconds(20));
        log.info("Ads.txt deleted");
        return helpersInit.getBaseHelper().checkDataset(true, $(String.format(ADD_ADS_TXT_ICON, websiteId)).isDisplayed());
    }

    /**
     * Check 'Edit ads.txt' icon
     */
    public boolean checkEditAdsTxtIcon(String websiteId) {
        CREATE_ADS_TXT_POPUP.shouldBe(hidden, ofSeconds(30));
        EDIT_ADS_TXT_POPUP.shouldBe(hidden, ofSeconds(30));
        helpersInit.getBaseHelper().waitForReadyPage();
        SITE_DOMAIN.shouldBe(visible);
        return helpersInit.getBaseHelper().checkDataset(true, $(String.format(EDIT_ADS_TXT_ICON, websiteId)).isDisplayed());
    }

    /**
     * Click on add ads.txt icon
     */
    public void clickAddAdsTxtIcon(String websiteId) {
        log.info("Click on add ads.txt icon");
        $(String.format(ADD_ADS_TXT_ICON, websiteId)).shouldBe(visible).scrollTo().click();
    }

    /**
     * Get site domain value from field
     */
    public String getSiteDomain() {
        CREATE_ADS_TXT_POPUP.shouldBe(visible, ofSeconds(10));
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_ADS_TXT_POPUP_DOMAIN_FIELD.shouldBe(visible).val());
    }

    /**
     * Click create button
     */
    public void clickCreateButton() {
        CREATE_ADS_TXT_POPUP.shouldBe(visible);
        log.info("Click create button");
        CREATE_ADS_TXT_POPUP_CREATE_BUTTON.shouldBe(visible).click();
        checkErrors();
    }

    /**
     * Close 'Create Ads.txt file?' popup
     */
    public void closeAddAdsTxtPopup() {
        CREATE_ADS_TXT_POPUP_CANCEL_BUTTON.click();
        CREATE_ADS_TXT_POPUP.shouldBe(hidden, ofSeconds(8));
        log.info("Close 'Create Ads.txt file?' popup");
    }

    /**
     * Get site domain from Cab - Websites
     */
    public String getSiteDomainFromWebsitesList() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SITE_DOMAIN.shouldBe(visible).text());
    }

    /**
     * Check default checkboxes in 'Create Ads.txt file?' popup
     */
    public boolean checkAddAdsTxtPopupDefaultCheckboxes() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(!ADD_ADS_TXT_POPUP_AUTONOTIFICATIONS_CHECKBOX.isSelected() &&
                ADD_ADS_TXT_POPUP_VIDEO_LIST_CHECKBOX.isSelected());
    }

    /**
     * Clear domain field in 'Create Ads.txt file?' popup
     */
    public void clearAddAdsTxtPopupDomainInput() {
        ADD_ADS_TXT_POPUP_DOMAIN_FIELD.shouldBe(visible).clear();
        log.info("Clear domain field");
    }

    /**
     * Check presence of add Ads.txt icon
     */
    public boolean checkAddAdsTxtIcon(int websiteId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($(String.format(ADD_ADS_TXT_ICON, websiteId)).isDisplayed());
    }

    //////////////////////////////////// TAGS CLOUD - START ///////////////////////////////////

    /**
     * открываем форму с выбором TAGS CLOUD
     */
    public void openTagsCloudForm() {
        if (TAGS_CLOUD_ON.isDisplayed()) {
            clickInvisibleElementJs(TAGS_CLOUD_ON.shouldBe(visible));
        } else {
            clickInvisibleElementJs(TAGS_CLOUD_OFF.shouldBe(visible));
        }
    }

    /**
     * сохраняем форму
     */
    public void saveTagsForm() {
        clickInvisibleElementJs(TAGS_FORM_SAVE);
        waitForAjax();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * click 'Add tags to all client sites' checkbox
     */
    public void clickSwitchOnTagsForAllSitesClient() {
        SAVE_TAGS_FOR_ALL_CLIENT_SITES_INPUT.click();
    }

    /**
     * выбираем рандомные теги для разных типов(TITLE_CHECK, IMAGE_CHECK, LINK_CHECK)
     */
    public Multimap<String, String> chooseRandomTags(String[] tagsType) {
        Multimap<String, String> tags = ArrayListMultimap.create();
        List<String> tagsCloudName;

        for (String type : tagsType) {

            //получаем список елементов тегов для конкретного типа
            ElementsCollection tagsElements = $$(".tagsBlock [data-name]:not([style*='none'])[data-type=" + type + "]");

            // получаем рандомные имена тегов
            tagsCloudName = helpersInit.getBaseHelper().getRandomCountValue(tagsElements.size(), 3).stream()
                    .map(i -> tagsElements.get(i).attr("data-name"))
                    .collect(Collectors.toList());

            //выбираем(включаем) их
            tagsCloudName.forEach(i -> tagsElements.findBy(attribute("data-name", i)).click());

            //записываем отобранные теги в коллекцию tags
            tagsCloudName.forEach(i -> tags.put(type, i));
        }
        return tags;
    }

    /**
     * проверка отображения тегов после сохранения
     */
    public boolean checkChooseTags(Multimap<String, String> tags, String[] tagsType) {
        return Arrays.stream(tagsType)
                .allMatch(k -> tags.get(k).stream()
                        .allMatch(i -> Objects.equals($$("#tagsDialog .tagsBlock [data-name]:not([style*='none'])[data-type=" + k + "]").findBy(attribute("data-name", i))
                                .attr("class"), "active")));
    }

    /**
     * метод проверки корректной работы по поиску тегов
     */
    public boolean searchTags() {
        String temp = TAGS_ELEMENTS_LIST.get(randomNumbersInt(TAGS_ELEMENTS_LIST.size())).attr("data-name");

        assert temp != null;
        if (temp.length() > 3) {
            temp = temp.substring(0, 3);
        }
        TAGS_FORM_SEARCH.sendKeys(temp);
        String finalTemp = temp;
        TAGS_ELEMENTS_LIST.shouldBe(CollectionCondition.allMatch("searchTags", i -> i.getAttribute("data-name").contains(finalTemp)));
        return true;
    }

    //////////////////////////////////// TAGS CLOUD - FINISH ///////////////////////////////////

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return BLOCK_ADS_ICON.isDisplayed();
    }

    /**
     * check work different filter
     *
     * @param pathElement - String ElementsCollection
     */
    public boolean isShowDataAfterFilter(String pathElement) {
        $$(SITES_ROW).shouldBe(CollectionCondition.sizeGreaterThan(0));
        return helpersInit.getBaseHelper().checkDataset($$(SITES_ROW).size(), $$(pathElement).size());
    }

    public void blockByAntifrod() {
        BLOCK_BY_ANTIFROD_ICON.shouldBe(visible).click(ClickOptions.usingJavaScript());
        waitForAjax();
        checkErrors();
    }

    public void unblockByAntifrod() {
        UNBLOCK_BY_ANTIFROD_ICON.shouldBe(visible).scrollTo().click();
        waitForAjax();
        checkErrors();
    }

    public String chooseCategoryInline(String categoryId) {
        clickInvisibleElementJs(INLINE_CATEGORY_LABEL.shouldBe(visible).scrollTo());
        if (categoryId == null) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(INLINE_CATEGORY_SELECT));
        }
        return helpersInit.getBaseHelper().selectValueFromParagraphListJs(INLINE_CATEGORY_SELECT, categoryId);
    }

    public void submitCategoryInline() {
        clickInvisibleElementJs(INLINE_CATEGORY_SUBMIT);
        waitForAjax();
        checkErrors();
        helpersInit.getMessageHelper().isSuccessMessagesCabWithoutClean();
    }

    public String choosePushProviderInline(String providerId) {
        if (providerId == null) {
            return helpersInit.getBaseHelper().selectRandomValue(INLINE_PUSH_PROVIDERS_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(INLINE_PUSH_PROVIDERS_SELECT, providerId);
    }

    public String chooseDomainThatWillBeDisplayedToAdvertisersInline(String sourcesId) {
        if (sourcesId == null) {
            return helpersInit.getBaseHelper().selectRandomValue(INLINE_SOURCE_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(INLINE_SOURCE_SELECT, sourcesId);
    }

    public String addNewDomainForAdvertisersInline(boolean lowVolumeDomain, String domainThatWillBeDisplayedToAdvertisersName) {
        String val = domainThatWillBeDisplayedToAdvertisersName == null ?
                "newDomainToAdvertisers" + BaseHelper.getRandomWord(2) :
                domainThatWillBeDisplayedToAdvertisersName;

        clearAndSetValue(INLINE_NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(visible), val);
        markUnMarkCheckbox(lowVolumeDomain, INLINE_NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME);
        INLINE_NEW_DOMAIN_TO_ADVERTISERS_SUBMIT.click();
        checkErrors();

        if (helpersInit.getMessageHelper().isSuccessMessagesCabWithoutClean()) {
            INLINE_NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(hidden);
            return helpersInit.getBaseHelper().getTextAndWriteLog(INLINE_SOURCE_SELECT.getSelectedValue());
        }
        return "";
    }

    public boolean getLowVolumeDomainInline() {
        return INLINE_LOW_VOLUME_SOURCE_CHECKBOX.isSelected();
    }

    /**
     * Click on edit ads.txt icon
     */
    public void clickEditAdsTxtIcon(String websiteId) {
        log.info("Click on add ads.txt icon");
        $(String.format(EDIT_ADS_TXT_ICON, websiteId)).shouldBe(visible).scrollTo().click();
    }

    public void switchDfpIcon(DfpValues dfpVal, int siteId) {
        SelenideElement dfp_icon = $(String.format(GOOGLE_ADD_MANAGER_ICON, siteId));
        String dfpStatus = dfp_icon.attr("src");
        dfpStatus = dfpStatus.substring(dfpStatus.lastIndexOf("_")+1, dfpStatus.lastIndexOf("."));

        switch (dfpVal) {
            case ON -> {

                if (dfpStatus.equals(DfpValues.OFF.getDfpType())) {
                    dfp_icon.click();
                    helpersInit.getMessageHelper().isSuccessMessagesCab();
                }

                if (dfpStatus.equals(DfpValues.PART.getDfpType())) {
                    dfp_icon.click();
                    helpersInit.getBaseHelper().checkAlertAndClose();
                    helpersInit.getMessageHelper().isSuccessMessagesCab();
                }
            }

            case OFF -> {

                if (dfpStatus.equals(DfpValues.ON.getDfpType())){
                    dfp_icon.click();
                    helpersInit.getMessageHelper().isSuccessMessagesCab();
                }

                if (dfpStatus.equals(DfpValues.PART.getDfpType())) {
                    dfp_icon.click();
                    helpersInit.getBaseHelper().checkAlertAndClose();
                    helpersInit.getMessageHelper().isSuccessMessagesCab();
                    dfp_icon.click();
                    helpersInit.getMessageHelper().isSuccessMessagesCab();
                }
            }
        }
    }

    public boolean checkDfpIcon(DfpValues dfpVal, int siteId){
        String dfpStatus = $(String.format(GOOGLE_ADD_MANAGER_ICON, siteId)).attr("src");
        dfpStatus = dfpStatus.substring(dfpStatus.lastIndexOf("_")+1, dfpStatus.lastIndexOf("."));

        return helpersInit.getBaseHelper().checkDatasetEquals(dfpVal.getDfpType(), dfpStatus);
    }

    public boolean isExistDfpIcon(int siteId){ return $(String.format(GOOGLE_ADD_MANAGER_ICON, siteId)).exists();}
}
