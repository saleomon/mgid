package pages.cab.publishers.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import static pages.cab.publishers.locators.ExchangeCampaignsListLocators.FILTER_BUTTON;
import static pages.cab.publishers.locators.ExchangeCampaignsListLocators.ID_FIELD;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class ExchangeCampaignsListHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public ExchangeCampaignsListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * get campaign id
     */
    public String getCampaignId() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ID_FIELD.text());
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }
}
