package pages.cab.publishers.helpers.finances;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.List;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.finances.ClientsPayoutsLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class ClientsPayoutsHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public ClientsPayoutsHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Get payout color
     */
    public String getPayoutColor() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(convertRgbaToHex(PAYMENT_ROW.getCssValue("background-color")));
    }

    /**
     * Get payout request ID
     */
    public String getClientsPayoutRequestId() {
        PAYMENT_REQUEST_ID.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENT_REQUEST_ID.text());
    }

    /**
     * Get payout requests ID
     */
    public List<String> getClientsPayoutRequestsIds() {
        return PAYMENT_REQUEST_IDS.texts();
    }

    /**
     * Get status value
     */
    public String getPayoutRequestStatus() {
        PAYMENT_STATUS_CELL.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENT_STATUS_CELL.attr("status"));
    }

    /**
     * Get payout amount
     */
    public String getPaymentAmount() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENT_AMOUNT_VALUE.text());
    }

    /**
     * Get payout comment
     */
    public String getPayoutRequestCell() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENT_COMMENT_MANAGER_ACCOUNTANT_CELL.text());
    }

    /**
     * Approve payment and check errors
     */
    public boolean clickPayoutApproveIcon() {
        PAYMENT_APPROVE_REQUEST_ICON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
        log.info("Payment request approved.");
        PAYMENT_APPROVE_REQUEST_ICON.shouldBe(hidden, ofSeconds(10));
        return !PAYMENT_APPROVE_REQUEST_ICON.isDisplayed();
    }

    /**
     * Check approved requests and clear them
     */
    public void checkApprovedRequestsAndClearThem() {
        PAYMENTS_TABLE.shouldBe(visible);
        if (APPROVE_REQUEST_ICON.exists()) APPROVE_REQUEST_ICONS.asFixedIterable().forEach(i -> i.shouldBe(visible).click());
        checkErrors();
    }

    /**
     * Check pending requests and clear them
     */
    public void checkPendingRequestsAndClearThem() {
        PAYMENTS_TABLE.shouldBe(visible);
        if (REJECT_REQUEST_ICON.exists()) REJECT_REQUEST_ICONS.asFixedIterable().forEach(i -> i.shouldBe(visible).click());
        checkErrors();
    }

    /**
     * Click paid-icon $
     */
    public void clickPaymentRequestPaidIcon() {
        PAYMENT_REQUEST_PAID_ICON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Check payment settings
     */
    public boolean checkPaymentSettings(String purseId, String sum, String comment) {
        PAID_EDIT_POPUP.shouldBe(visible);
        return helpersInit.getBaseHelper().checkDatasetContains(purseId, PAID_EDIT_POPUP_PURSE_LABEL.text()) &&
                helpersInit.getBaseHelper().checkDataset(Double.parseDouble(sum), Double.parseDouble(PAID_EDIT_POPUP_SUM_FIELD.val())) &&
                helpersInit.getBaseHelper().checkDatasetContains(comment, PAID_EDIT_POPUP_COMMENT_FIELD.val());
    }

    /**
     * Submit paid popup
     */
    public void submitPaidPopup() {
        PAID_EDIT_DIALOG_SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
        PAID_EDIT_POPUP.shouldBe(hidden, ofSeconds(10));
    }

    /**
     * Click on cancel transaction icon
     */
    public void clickCancelTransactionIcon() {
        clickInvisibleElementJs(PAYMENT_CANCEL_PAYOUT_ICON);
        waitForAjax();
        checkErrors();
    }

    /**
     * Input comment in text-field
     */
    public void fillComment(String comment) {
        ORDER_CANCELLATION_POPUP.shouldBe(visible);
        clearAndSetValue(ORDER_CANCELLATION_COMMENT_FIELD, comment);
        helpersInit.getBaseHelper().getTextAndWriteLog(comment);
    }

    /**
     * Submit cancellation popup
     */
    public void submitCancellationPopup() {
        ORDER_CANCELLATION_SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Check that transaction has been canceled
     */
    public boolean checkTransactionCancellation(String status) {
        return helpersInit.getBaseHelper().checkDataset(false, PAYMENT_CANCEL_PAYOUT_ICON.isDisplayed()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(status, PAYMENT_STATUS_CELL.text());
    }

    /**
     * Select region of payout system
     */
    public void setFieldClientsPaymentsByRegion(String value) {
        REGION_OF_PAYOUT_SYSTEM_SELECT.shouldBe(visible).selectOptionByValue(value);
    }

    /**
     * Submit filter
     */
    public void submitFilter() {
        SUBMIT_FILTER_BUTTON.shouldBe(visible).click();
        checkErrors();
    }

    /**
     * Check "Asia" clients payments
     */
    public boolean checkClientsPaymentsPurse(String purseName) {
        PACK_PAYMENT_FIELD.shouldBe(CollectionCondition.allMatch("checkClientsPaymentsPurse", i -> i.getText().toLowerCase().contains(purseName)));
        return true;
    }

    /**
     * Get selected value for Region of payout system
     */
    public String getValueForRegionOfPayoutSystemSelect() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REGION_OF_PAYOUT_SYSTEM_SELECT.shouldBe(visible).getSelectedText());
    }

    /**
     * Get selected value for Publisher currency
     */
    public String getValueFromPublisherCurrencySelect() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUBLISHER_CURRENCY_SELECT.shouldBe(visible).getSelectedText());
    }

    /**
     * Select Publisher currency
     */
    public void setFieldClientsPaymentsByPublisherCurrency(String currencyValue) {
        PUBLISHER_CURRENCY_SELECT.shouldBe(visible).selectOptionByValue(currencyValue);
    }

    /**
     * Check Publisher currency icon in clients payments
     */
    public boolean checkClientsPaymentsCurrency(String currencySymbol) {
        for(SelenideElement s : AMOUNT_REQUESTED_FIELDS){
            if(!s.parent().getText().toLowerCase().contains(currencySymbol)){
                return false;
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENTS_ROWS.size() > 0);
    }

    /**
     * Check package payments checkboxes
     */
    public boolean checkPaymentsPackCheckboxes(boolean state) {
        if (state) {
            PACK_PAYMENT_CHECKBOXES.shouldBe(CollectionCondition.allMatch("checkPaymentsPackCheckboxes", i -> i.isSelected() & i.isEnabled()));
        }
        else {
            PACK_PAYMENT_CHECKBOXES.shouldBe(CollectionCondition.allMatch("checkPaymentsPackCheckboxes", i -> i.isSelected() & !i.isEnabled()));
        }
        return true;
    }

    /**
     * Click 'Добавить в пакет' button
     */
    public void addPaymentsToPack() {
        ADD_PACK_BUTTON.shouldBe(visible).scrollTo().click();
        waitForAjax();
        checkErrors();
    }
}
