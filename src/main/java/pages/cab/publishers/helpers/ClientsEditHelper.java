package pages.cab.publishers.helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.ClientsEditLocators.*;
import static pages.cab.publishers.variables.CabPublisherClientVariables.defaultRevenueSharePercent;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class ClientsEditHelper {

    private final Logger log;
    private final HelpersInit helpersInit;

    public ClientsEditHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * choose random value in 'Currency' filter
     */
    public String chooseFilterCurrency() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomText(FILTER_CURRENCY_SELECT));
    }

    public void switchAllowedClicksFromClientDomainsOnly(boolean state) {
        markUnMarkCheckbox(state, ALLOW_CLICKS_FROM_CLIENTS_DOMAIN_CHECKBOX);
    }

    public boolean checkAllowedClicksFromClientDomainsOnly(boolean state) {
        return checkIsCheckboxSelected(state, ALLOW_CLICKS_FROM_CLIENTS_DOMAIN_CHECKBOX);
    }

    /**
     * click 'Filter' button
     */
    public void clickFilterButton() {
        FILTER_SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * get currency type
     */
    public String getCurrencyType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MG_WALLET_LABEL
                .text()
                .replaceAll("[\\d. ]", ""));
    }

    public void switchForbidCreateNewWidget(boolean isTurnOn) {
        markUnMarkCheckbox(isTurnOn, FORBID_CREATE_NEW_WIDGET_CHECKBOX);
    }

    public void switchDirectPublisherDemand(boolean isTurnOn) {
        markUnMarkCheckbox(isTurnOn, DIRECT_PUBLISHER_DEMAND_CHECKBOX);
    }

    public boolean checkDirectPublisherDemand(boolean state) {
        return checkIsCheckboxSelected(state, DIRECT_PUBLISHER_DEMAND_CHECKBOX);
    }

    public void saveSettings() {
        SAVE_SETTINGS_BUTTON.click();
        checkErrors();
    }

    /**
     * choose payment method in 'Payment method:'
     */
    public void choosePaymentMethod(String method) {
        helpersInit.getBaseHelper().selectCustomValue(PURSES_ID_SELECT, method);
    }


    /**
     * set 'Card info' (Capitalist world card)
     */
    public void setCapitalistWorldCardInfo(String cardInfo) {
        clearAndSetValue(CAPITALIST_WORLD_CARD_INFO_INPUT, cardInfo);
    }


    /**
     * choose Expiration Date 'Month'
     */
    public void chooseCapitalistWorldExpDateMonth(String expDateMonth) {
        if (!expDateMonth.equals(""))
            helpersInit.getBaseHelper().selectCustomValue(CAPITALIST_WORLD_EXP_DATE_MONTH_SELECT, expDateMonth);
    }


    /**
     * choose Expiration Date 'Year'
     */
    public void chooseCapitalistWorldExpDateYear(String expDateYear) {
        if (!expDateYear.equals(""))
            helpersInit.getBaseHelper().selectCustomValue(CAPITALIST_WORLD_EXP_DATE_YEAR_SELECT, expDateYear);
    }


    /**
     * set 'Name' (Capitalist world card)
     */
    public void setCapitalistWorldName(String name) {
        clearAndSetValue(CAPITALIST_WORLD_NAME_INPUT, name);
    }

    /**
     * set 'Second name'
     */
    public void setCapitalistWorldSecondName(String capitalistSecondName) {
        clearAndSetValue(CAPITALIST_WORLD_SECOND_NAME_INPUT, capitalistSecondName);
    }


    /**
     * set 'date of birth (YYYY-MON-DD)'
     */
    public void setCapitalistWorldDateOfBirth(String capitalistDateOfBirth) {
        helpersInit.getBaseHelper().setAttributeValueJS(CAPITALIST_WORLD_DATE_OF_BIRTH_INPUT, capitalistDateOfBirth);
    }


    /**
     * set 'Address'
     */
    public void setCapitalistWorldAddress(String capitalistAddress) {
        clearAndSetValue(CAPITALIST_WORLD_ADDRESS_INPUT, capitalistAddress);
    }


    /**
     * set 'City'
     */
    public void setCapitalistWorldCity(String capitalistCity) {
        clearAndSetValue(CAPITALIST_WORLD_CITY_INPUT, capitalistCity);
    }


    /**
     * choose 'Country'
     */
    public void chooseCapitalistWorldCountry(String capitalistCountry) {
        if (!capitalistCountry.equals(""))
            helpersInit.getBaseHelper().selectCustomValue(CAPITALIST_WORLD_COUNTRY_SELECT, capitalistCountry);
    }

    /**
     * add purse
     */
    public void addPurseValue() {
        PURSE_ADD_BUTTON.click();
        log.info("addPurseValue");
        if (helpersInit.getBaseHelper().alertIsShow()) {
            return;
        }
        PURSE_ADD_BUTTON.shouldBe(attribute("aria-disabled", "false"), ofSeconds(10));
        log.info("Add purse button is active!");
        checkErrors();
    }

    /**
     * Add purse
     */
    public void addPurse() {
        PURSE_ADD_BUTTON.click();
        log.info("addPurseValue");
        PURSE_ADD_BUTTON.shouldHave(attribute("aria-disabled", "false"), ofSeconds(10));
        log.info("Add purse button is active!");
        checkErrors();
    }

    /**
     * get custom purse if his exist
     */
    public String getCustomPurseId(String purseName) {
        if ($x(".//td[(contains(text(), '" + purseName + "'))]/../td/img[@data-purse-id]").exists()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog($x(".//td[(contains(text(), '" + purseName + "'))]/../td/img[@data-purse-id]").attr("data-purse-id"));
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog((String) null);
    }

    /**
     * delete custom purse
     */
    public void deleteCustomPurse(String purseId) {
        $("[data-purse-id='" + purseId + "']").shouldBe(visible).click();
        $("[data-purse-id='" + purseId + "']").shouldBe(hidden, ofSeconds(10));
    }

    /**
     * set 'Card info'
     */
    public void setCapitalistCardInfo(String cardInfo) {
        clearAndSetValue(CAPITALIST_CARD_INFO_INPUT, cardInfo);
    }

    /**
     * set 'Name'
     */
    public void setCapitalistName(String name) {
        clearAndSetValue(CAPITALIST_NAME_INPUT, name);
    }

    /**
     * get count errors purses fields
     */
    public int getCountErrorValidFields() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$("." + ERROR_PURSE_VALIDATION_INPUT).size());
    }

    /**
     * check light input for custom field
     */
    public boolean isShowErrorLightForCustomField(String fieldName) {
        SelenideElement element = switch (fieldName) {
            case "Card info" -> CAPITALIST_WORLD_CARD_INFO_INPUT;
            case "Expiration Date Month" -> CAPITALIST_WORLD_EXP_DATE_MONTH_SELECT;
            case "Expiration Date Year" -> CAPITALIST_WORLD_EXP_DATE_YEAR_SELECT;
            case "Name" -> CAPITALIST_WORLD_NAME_INPUT;
            case "Second name" -> CAPITALIST_WORLD_SECOND_NAME_INPUT;
            case "date of birth" -> CAPITALIST_WORLD_DATE_OF_BIRTH_INPUT;
            case "Address" -> CAPITALIST_WORLD_ADDRESS_INPUT;
            case "City" -> CAPITALIST_WORLD_CITY_INPUT;
            case "Country" -> CAPITALIST_WORLD_COUNTRY_SELECT;
            default -> null;
        };
        assert element != null;
        return element.is(Condition.cssClass(ERROR_PURSE_VALIDATION_INPUT));
    }

    /**
     * get error message for capitalist fields
     */
    public String getCapitalistWorldErrorMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAPITALIST_ERROR_LABEL.text());
    }

    /**
     * Проверка видимости поля
     */
    public boolean checkRevenueShareFieldDisplaying() {
        log.info("Поле 'RS Income percentage for new widgets, %' отображается - " + REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS.isDisplayed());
        return helpersInit.getBaseHelper().getTextAndWriteLog(REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS.isDisplayed());
    }

    /**
     * Проверка значения Revenue Share
     */
    public boolean checkRevenueShareDefaultPercent() {
        log.info("Значение в поле 'RS Income percentage for new widgets, %' - " + REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS.scrollTo().getValue());
        return helpersInit.getBaseHelper().checkDatasetEquals(defaultRevenueSharePercent, Objects.requireNonNull(REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS.getValue()));
    }

    /**
     * Установка необходимого значения в поле 'RS Income percentage for new widgets, %'
     */
    public void fillRevenueShareField(String revenueShare) {
        clearAndSetValue(REVENUE_SHARE_PERCENT_FOR_NEW_WIDGETS, revenueShare);
    }

    public void fillCampaignMainWebsite(String mainWebSite) {
        clearAndSetValue(COMPANY_MAIN_WEB_SITES_INPUT, mainWebSite);
    }

    public boolean checkCampaignMainWebsite(String mainWebSite) {
        return helpersInit.getBaseHelper().checkDatasetEquals(mainWebSite, Objects.requireNonNull(COMPANY_MAIN_WEB_SITES_INPUT.val()));
    }

    public void fillClientLegalName(String legalName) {
        clearAndSetValue(CLIENT_LEGAL_NAME_INPUT, legalName);
    }

    public boolean checkClientLegalName(String legalName) {
        return helpersInit.getBaseHelper().checkDatasetEquals(legalName, Objects.requireNonNull(CLIENT_LEGAL_NAME_INPUT.val()));
    }

    public void turnOnAllAdTypes() {
        $$(AD_TYPES_NAME).filter(Condition.not(selected)).asFixedIterable().forEach(i -> i.shouldBe(visible).click());
    }

    public void turnOnCustomAdTypes(Map<String, Boolean> map) {
        map.entrySet().stream()
                .filter(m -> ($("[name='adTypes[]'][value='" + m.getKey() + "']").isSelected() != m.getValue()))
                .forEach(m -> $("[name='adTypes[]'][value='" + m.getKey() + "']").click());
    }

    public void turnOnAllLandingTypes() {
        $$(LANDING_TYPES_NAME).filter(Condition.not(selected)).asFixedIterable().forEach(i -> i.shouldBe(visible).click());
    }

    public void turnOnCustomLandingTypes(Map<String, Boolean> map) {
        map.entrySet().stream()
                .filter(m -> ($("[name='landingTypes[]'][value='" + m.getKey() + "']").isSelected() != m.getValue()))
                .forEach(m -> $("[name='landingTypes[]'][value='" + m.getKey() + "']").click());
    }

    public void switchAssignDefaultTeaserTypesForNewClientWidgets(boolean state) {
        markUnMarkCheckbox(state, ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX);
    }

    public void switchCanChangeAdTypesInDashboard(boolean state) {
        markUnMarkCheckbox(state, CAN_CHANGE_NSFW_TYPES_CHECKBOX);
    }

    public boolean isDisplayedAssignDefaultTeaserTypesForNewClientWidgets() {
        SAVE_SETTINGS_BUTTON.shouldBe(visible);
        return ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX.exists();
    }

    /**
     * Check if Payout Region Select is disabled
     */
    public boolean checkPayoutRegionSelectState() {
        return Objects.requireNonNull(CLIENTS_PAYOUT_REGION_SELECT.getAttribute("class")).toLowerCase().contains("disabled") &&
                CLIENTS_PAYOUT_REGION_SELECT.isEnabled();
    }

    /**
     * Get selected value
     */
    public String getPayoutRegionValue() {
        return CLIENTS_PAYOUT_REGION_INPUT.scrollTo().getValue();
    }

    /**
     * Set Client's payout region
     */
    public void setClientsPayoutRegion(String value) {
        clearAndSetValue(CLIENTS_PAYOUT_REGION_INPUT.scrollTo(), value);
    }

    /**
     * Get Payment method Id
     */
    public String getPaymentMethodId() {
        return PAYMENT_METHOD_NAME_FIELD.getAttribute("data-currency-id");
    }

    /**
     * Get tooltip text
     */
    public String getTooltipText() {
        CLIENTS_PAYOUT_REGION_TOOLTIP.hover();
        return CLIENTS_PAYOUT_REGION_TOOLTIP_FIELD.innerText();
    }

    /**
     * Set purse's number for Paymaster24
     */
    public String setPaymaster24PurseNumber(String purseNumberValue) {
        sendKey(PAYMASTER24_PURSE_NUMBER_INPUT, purseNumberValue);
        return purseNumberValue;
    }

    /**
     * Set client's name for Paymaster24
     */
    public String setPaymaster24ClientName() {
        String value = helpersInit.getGenerateDataHelper().generateName();
        clearAndSetValue(PAYMASTER24_NAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set client's surname for Paymaster24
     */
    public String setPaymaster24ClientSurname() {
        String value = helpersInit.getGenerateDataHelper().generateSurname();
        clearAndSetValue(PAYMASTER24_SURNAME_INPUT, value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * Set client's date of birth for Paymaster24
     */
    public String setPaymaster24BirthDate() {
        String dateValue = helpersInit.getBaseHelper().dateFormat(
                helpersInit.getGenerateDataHelper().generateBirthDate(), "yyyy-MM-dd");
        helpersInit.getBaseHelper().setAttributeValueJS(PAYMASTER24_DATE_OF_BIRTH_INPUT, dateValue);
        return helpersInit.getBaseHelper().getTextAndWriteLog(dateValue);
    }

    /**
     * Set client's country for Paymaster24
     */
    public Map<String, String> setPaymaster24Country() {
        ElementsCollection countries = PAYMASTER24_COUNTRY_INPUT.$$("option");
        Map<String, String> country = new HashMap<>();
        int randomIndex = randomNumbersInt(countries.size());
        country.put(countries.get(randomIndex).getValue(), countries.get(randomIndex).text());
        PAYMASTER24_COUNTRY_INPUT.selectOptionByValue(country.keySet().stream().findFirst().get());
        return country;
    }

    /**
     * Set purse value
     */
    public void setPayoneerPurseValue(String purseValue) {
        clearAndSetValue(PURSE_ADD_INPUT, purseValue);
    }

    /**
     * Set purse's number for
     */
    public void setCapitalistIdaaUsdPurseNumber(String purseNumberValue) {
        sendKey(CAPITALIST_ACCOUNT_NUMBER_INPUT, purseNumberValue);
    }

    /**
     * Get error message for Capitalist account number
     */
    public String getCapitalistIdaaUsdErrorMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAPITALIST_ACCOUNT_NUMBER_ERROR_LABEL.text());
    }
}