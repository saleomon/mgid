package pages.cab.publishers.helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.base.HelpersInit;
import core.helpers.BaseHelper;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.publishers.locators.WebsitesAddLocators.*;

public class WebsitesAddHelper {

    private final HelpersInit helpersInit;
    private final Logger log;
    private boolean isGenerateNewValues = false;
    private boolean isSetSomeFields = false;

    public WebsitesAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public WebsitesAddHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    public WebsitesAddHelper setSetSomeFields(boolean isSetSomeFields) {
        this.isSetSomeFields = isSetSomeFields;
        return this;
    }

    /**
     * check 'Domain' (name)
     */
    public boolean checkDomain(String domain) {
        return (isSetSomeFields && domain == null)
                || helpersInit.getBaseHelper().checkDatasetEquals(DOMAIN_FIELD.val(), domain);
    }

    public boolean checkMirror(String mirror) {
        return (isSetSomeFields && mirror.equals(""))
                || helpersInit.getBaseHelper().checkDatasetEquals(MIRROR_INPUT.text(), mirror);
    }

    /**
     * check block 'Select cooperation types and priorities'
     */
    public boolean checkCooperationType(List<String> cooperationType) {
        return cooperationType.stream().allMatch(i -> PANEL_RIGHT.find(By.id(i)).isDisplayed());
    }

    /**
     * check 'Site category'
     */
    public boolean checkCategory(String categoryId) {
        return (isSetSomeFields && categoryId == null)
                || helpersInit.getBaseHelper().checkDatasetContains(CATEGORY_SELECT.find(By.cssSelector("[data-id='" + categoryId + "']")).attr("class"), "selected-cat");
    }

    /**
     * check 'Website Tier' in edit inter
     */
    public boolean checkTier(String tier) {
        return (isSetSomeFields && tier == null)
                || helpersInit.getBaseHelper().checkDatasetEquals(TIER_SELECT.getSelectedValue(), tier);
    }

    /**
     * check 'Site language'
     */
    public boolean checkLanguage(String languageId) {
        return (isSetSomeFields && languageId == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(LANGUAGE_SELECT, languageId);
    }

    /**
     * set 'Domain' input
     */
    public String setDomain(String domain) {
        if (isSetSomeFields && domain == null) return null;
        String domainVal = domain == null ? "domain" + BaseHelper.getRandomWord(4) + ".com.ua" :
                domain;
        clearAndSetValue(DOMAIN_INPUT.shouldBe(visible), domainVal);
        return domainVal;
    }

    public String setMirror(String mirror) {
        if (isSetSomeFields && mirror.equals("")) return mirror;
        String mirrorVal = mirror.equals("") ? "test" + BaseHelper.getRandomWord(2) + ".domain.com" :
                mirror;
        clearAndSetValue(MIRROR_INPUT.shouldBe(visible), mirrorVal);
        return mirrorVal;
    }

    /**
     * choose 'Category'
     */
    public String chooseCategory(String categoryId) {
        if (isSetSomeFields && categoryId == null) return null;
        if (categoryId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
        }
        return helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
    }

    public String choosePushProvider(String providerId) {
        if (isSetSomeFields && providerId == null) return null;
        if (providerId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(PUSH_PROVIDERS_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(PUSH_PROVIDERS_SELECT, providerId);
    }

    public boolean checkPushProvider(String providerId) {
        return (isSetSomeFields && providerId == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(PUSH_PROVIDERS_SELECT, providerId);
    }

    public String chooseDomainThatWillBeDisplayedToAdvertisers(String sourcesId) {
        if (isSetSomeFields && sourcesId == null) return null;
        if (sourcesId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT, sourcesId);
    }

    public String addNewDomainForAdvertisers(boolean lowVolumeDomain, String domainThatWillBeDisplayedToAdvertisersName) {
        String val = domainThatWillBeDisplayedToAdvertisersName == null ?
                "newDomainToAdvertisers" + BaseHelper.getRandomWord(2) :
                domainThatWillBeDisplayedToAdvertisersName;

        clearAndSetValue(NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(visible), val);
        markUnMarkCheckbox(lowVolumeDomain, NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME);
        NEW_DOMAIN_TO_ADVERTISERS_SUBMIT.click();
        checkErrors();

        if (helpersInit.getMessageHelper().isSuccessMessagesCabWithoutClean()) {
            NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(hidden);
            return helpersInit.getBaseHelper().getTextAndWriteLog(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT.getSelectedValue());
        }
        return "";
    }

    public boolean checkDomainThatWillBeDisplayedToAdvertisers(String sourcesId) {
        return (isSetSomeFields && sourcesId == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT, sourcesId);
    }

    public boolean getLowVolumeDomain() {
        return LOW_VOLUME_SOURCE_CHECKBOX.isSelected();
    }

    public boolean checkLowVolumeDomain(boolean lowVolumeDomain) {
        return checkIsCheckboxSelected(lowVolumeDomain, LOW_VOLUME_SOURCE_CHECKBOX);
    }

    /**
     * default = choose 5 random bundle
     */
    public ArrayList<String> chooseBundles(List<String> cooperationType, ArrayList<String> bundles){
        if((isSetSomeFields && (bundles == null | bundles.size() == 0)) || !cooperationType.contains("wages") ) return bundles;

        return bundles.isEmpty() ? helpersInit.getBaseHelper().selectRandomValuesForMultiSelect(BUNDLE_ID_ELEMENT, 5) :
                helpersInit.getBaseHelper().selectCustomValuesForMultiSelect(BUNDLE_ID_ELEMENT, bundles);
    }

    public boolean checkBundles(ArrayList<String> bundles) {
        if (isSetSomeFields && (bundles == null | bundles.size() == 0)) return true;
        return helpersInit.getBaseHelper().checkValuesForMultiSelect(BUNDLE_ID_ELEMENT, bundles);
    }

    /**
     * set cooperation type
     */
    public void setCooperationTypes(List<String> cooperationType) {
        cooperationType.forEach(i -> $("#" + i).dragAndDropTo(PANEL_RIGHT));
    }

    /**
     * choose 'Source type'
     */
    public String chooseSourceType(String sourceType) {
        if (isSetSomeFields && sourceType == null) return null;
        if (sourceType == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(SOURCE_TYPE_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(SOURCE_TYPE_SELECT, sourceType);
    }

    /**
     * choose 'Nationality'
     */
    public String chooseNationality(String nationality) {
        if (isSetSomeFields && nationality == null) return null;
        if (nationality == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(NATIONALITY_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(NATIONALITY_SELECT, nationality);
    }

    /**
     * Site language
     */
    public String chooseLanguage(String languageId) {
        if (isSetSomeFields && languageId == null) return null;
        if (languageId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(LANGUAGE_SELECT));
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomValue(LANGUAGE_SELECT, languageId));
    }

    /**
     * Site tier
     */
    public String chooseTier(List<String> cooperationType, String tier) {
        if (isSetSomeFields && tier == null) return null;
        if ((tier == null || isGenerateNewValues)) {
            if (cooperationType.contains("wages")) {
                return helpersInit.getBaseHelper().selectRandomValue(TIER_SELECT);
            } else return tier;
        }
        return helpersInit.getBaseHelper().selectCustomValue(TIER_SELECT, tier);
    }

    /**
     * choose 'Language Priority'
     */
    public String chooseLanguagePriority(String languagePriority) {
        if (isSetSomeFields && languagePriority == null) return null;
        if (languagePriority == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(LANGUAGE_PRIORITY_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(LANGUAGE_PRIORITY_SELECT, languagePriority);
    }

    /**
     * set 'Comments'
     */
    public String setComments(String comments) {
        if (isSetSomeFields && comments == null) return null;
        String com = (comments == null || isGenerateNewValues) ?
                "testCom" + BaseHelper.getRandomWord(3) :
                comments;
        clearAndSetValue(COMMENT_INPUT, com);
        return com;
    }

    /**
     * choose 'Default widget for composite'
     */
    public String chooseDefaultWidgetForComposite(String defaultWidgetForComposite) {
        if (isSetSomeFields && defaultWidgetForComposite == null) return null;
        if (defaultWidgetForComposite == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().selectRandomValue(DEFAULT_WIDGET_FOR_COMPOSITE_SELECT);
        }
        return helpersInit.getBaseHelper().selectCustomValue(DEFAULT_WIDGET_FOR_COMPOSITE_SELECT, defaultWidgetForComposite);
    }

    /**
     * switch 'Use subdomains in news links'
     */
    public void switchUseSubdomainsInNewsLinks(boolean useSubdomainsInNewsLinks) {
        markUnMarkCheckbox(useSubdomainsInNewsLinks, USE_SUBDOMAINS_IN_NEWS_LINKS_CHECKBOX);
    }

    /**
     * switch 'COPPA checkbox'
     */
    public void switchUseCoppaCheckbox(boolean useCoppaCheckbox) {
        checkIsCheckboxSelected(useCoppaCheckbox, COPPA_CHECKBOX);
        clickInvisibleElementJs(COPPA_CHECKBOX);
    }

    /**
     * switch 'Opportunity to promote news'
     */
    public void switchOpportunityToPromoteNews(boolean opportunityToPromoteNews) {
        markUnMarkCheckbox(opportunityToPromoteNews, OPPORTUNITY_TO_PROMOTE_NEWS_CHECKBOX.shouldBe(visible).scrollTo());
    }

    /**
     * switch 'Assign default teaser types for new client widgets'
     */
    public void switchAssignDefaultTeaserTypesForNewClientWidgets(boolean assignDefaultTeaserTypesForNewClientWidgets) {
        markUnMarkCheckbox(assignDefaultTeaserTypesForNewClientWidgets, ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX);
    }

    /**
     * check 'Source type'
     */
    public boolean checkSourceType(String sourceType) {
        return (isSetSomeFields && sourceType == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(SOURCE_TYPE_SELECT, sourceType);
    }

    /**
     * check 'Nationality'
     */
    public boolean checkNationality(String nationality) {
        return (isSetSomeFields && nationality == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(NATIONALITY_SELECT, nationality);
    }

    /**
     * check 'Comments'
     */
    public boolean checkComments(String comments) {
        return (isSetSomeFields && comments == null)
                || helpersInit.getBaseHelper().getTextAndWriteLog(COMMENT_INPUT.val()).equals(comments);
    }

    /**
     * check 'Language Priority'
     */
    public boolean checkLanguagePriority(String languagePriority) {
        return (isSetSomeFields && languagePriority == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(LANGUAGE_PRIORITY_SELECT, languagePriority);
    }

    /**
     * check select 'Default widget for composite'
     */
    public boolean checkDefaultWidgetForComposite(String defaultWidgetForComposite) {
        return (isSetSomeFields && defaultWidgetForComposite == null)
                || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(DEFAULT_WIDGET_FOR_COMPOSITE_SELECT, defaultWidgetForComposite);
    }

    /**
     * check state 'Use subdomains in news links' checkbox
     */
    public boolean checkUseSubdomainsInNewsLinks(boolean useSubdomainsInNewsLinks) {
        return checkIsCheckboxSelected(useSubdomainsInNewsLinks, USE_SUBDOMAINS_IN_NEWS_LINKS_CHECKBOX);
    }

    /**
     * check state 'Opportunity to promote news' checkbox
     */
    public boolean checkOpportunityToPromoteNews(boolean opportunityToPromoteNews) {
        return checkIsCheckboxSelected(opportunityToPromoteNews, OPPORTUNITY_TO_PROMOTE_NEWS_CHECKBOX);
    }

    /**
     * check state 'Assign default teaser types for new client widgets' checkbox
     */
    public boolean checkAssignDefaultTeaserTypesForNewClientWidgets(boolean assignDefaultTeaserTypesForNewClientWidgets) {
        return checkIsCheckboxSelected(assignDefaultTeaserTypesForNewClientWidgets, ASSIGN_DEFAULT_TEASER_TYPES_FOR_NEW_CLIENT_WIDGETS_CHECKBOX);
    }

    public void submitWebsite() {
        clickInvisibleElementJs(SAVE_CREATE_BUTTON);
    }

    public void saveEditWebsite() {
        clickInvisibleElementJs(SAVE_EDIT_BUTTON);
        waitForAjax();
    }

    /**
     * @param type 0 - No; 1 - Only; 2 - Except
     */
    public void chooseCategoriesFilterType(String type) {
        CATEGORIES_FILTER_RADIOBUTTONS.hover().selectRadio(type);
    }

    public boolean isDisplayedCategoriesFilter() {
        SAVE_EDIT_BUTTON.shouldBe(visible);
        return CATEGORIES_FILTER_RADIOBUTTONS.isDisplayed();
    }

    public boolean checkSearchCategoryFilter() {
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        ElementsCollection categoryList = $$x(CATEGORY_FILTER_TITLES);
        String category = helpersInit.getBaseHelper().getTextAndWriteLog(categoryList.get(randomNumbersInt(categoryList.size())).text());

        setCategoryToMultiFilter(category);
        return $$x(CATEGORY_FILTER_TITLES_AFTER_SEARCH)
                .texts().stream().allMatch(i -> i.toLowerCase().contains(category.toLowerCase()) || i.equalsIgnoreCase(category));
    }

    public void setCategoryToMultiFilter(String category) {
        CATEGORY_FILTER_SEARCH_INPUT.scrollTo().sendKeys(category);
    }

    public void clearAllSelectedCategoryInMultiFilter() {
        helpersInit.getMultiFilterHelper().clearAllSelectedCategoryInFilter(PRODUCT_PROMOTIONS_CHECKBOX);
        helpersInit.getMultiFilterHelper().clearAllSelectedCategoryInFilter(CONTENT_PROMOTIONS_CHECKBOX);
    }

    public boolean checkCategoriesFilterTypeValue(String radioButtonValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORIES_FILTER_VALUES.findBy(value(radioButtonValue)).shouldBe(visible).isSelected());
    }

    public boolean categoryFilterBlockIsDisplayed() {
        return CATEGORY_FILTER_SEARCH_INPUT.isDisplayed();
    }

    public boolean checkAdTypesAndLandingTypesForCategory(JSONArray obj) {
        String categoryId;
        ArrayList<String> adTypes;
        ArrayList<String> landingTypes;

        int count = 0;

        for (Object j : obj) {
            // get category, ad_types, landing_types from json
            categoryId = ((JSONObject) j).get("category").toString();
            adTypes = new Gson().fromJson(((JSONObject) j).get("adType").toString(), new TypeToken<List<String>>() {
            }.getType());
            landingTypes = new Gson().fromJson(((JSONObject) j).get("landingTypes").toString(), new TypeToken<List<String>>() {
            }.getType());

            log.info("check chosen category");
            if (helpersInit.getBaseHelper().checkDatasetContains($("[id^='ft_product-" + categoryId + "']>span").attr("class"), "fancytree-selected")) {
                //click category icon (лейка) and checkbox
                $("#ft_product-" + categoryId + " .fancytree-icon").scrollTo().click();
                $("#ft_product-" + categoryId + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

                List<String> adTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_product-" + categoryId + " .ad_types [data-type][checked='checked']")){
                    adTypes_list.add(s.attr("data-type"));
                }

                List<String> landingTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_product-" + categoryId + " .landing_types [data-type][checked='checked']")){
                    landingTypes_list.add(s.attr("data-type"));
                }

                log.info("check chosen adTypes and landingTypes");
                if (new HashSet<>(adTypes_list).containsAll(adTypes) &
                        new HashSet<>(landingTypes_list).containsAll(landingTypes)) {
                    count++;
                }
                else {
                    log.info("adTypes: " + adTypes);
                }
                CATEGORY_FILTER_SEARCH_INPUT.click();
            }
            else {
                log.info("categoryId: " + categoryId);
            }
        }
        return obj.size() > 0 &&
                obj.size() == count;
    }

    public JSONArray chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(int countValue){
        JSONArray main = new JSONArray();
        ArrayList<String> adTypes = new ArrayList<>();
        ArrayList<String> landingTypes = new ArrayList<>();

        log.info("отримуємо колекцію усіх елементів дерева");
        ElementsCollection list = $$(CATEGORY_FILTER_ELEMENTS);
        ArrayList<Integer> listCategoryId = helpersInit.getBaseHelper().getRandomCountValue(list.size() - 1, countValue);

        log.info("вибираємо 4 рандомні категорії та записуємо їх у масив");
        for (Integer integer : listCategoryId) {
            // get category id
            String category = list.get(integer).parent().attr("id");
            category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);

            //click category icon (лейка) and checkbox
            $("#ft_product-" + category + " .fancytree-checkbox").scrollTo().click();
            $("#ft_product-" + category + " .fancytree-icon").click();
            $("#ft_product-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

            //get random ad_types
            ElementsCollection adTypesList = $$("#ft_product-" + category + " .ad_types [data-type]");
            ArrayList<Integer> adTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(adTypesList.size() - 1, 3);

            // clear all checked adTypes
            for(SelenideElement s : $$("#ft_product-" + category + " .ad_types [data-type]:checked"))
            {
                s.shouldBe(attribute("selected"));
                s.click(usingJavaScript());
                s.shouldNotBe(attribute("selected"));
            }

            for (int at : adTypesChosen) {
                // click new adTypes
                adTypesList.get(at).click(usingJavaScript());
                adTypes.add(adTypesList.get(at).attr("data-type"));
            }

            //get random landing_types
            ElementsCollection landingTypesList = $$("#ft_product-" + category + " .landing_types [data-type]");
            ArrayList<Integer> landingTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(landingTypesList.size() - 1, 4);

            // clear all checked landingTypes
            for(SelenideElement s : $$("#ft_product-" + category + " .landing_types [data-type]:checked"))
            {
                s.shouldBe(attribute("selected"));
                s.click(usingJavaScript());
                s.shouldNotBe(attribute("selected"));
            }

            for (int at : landingTypesChosen) {
                // click new landingTypes
                landingTypesList.get(at).click(usingJavaScript());
                landingTypes.add(landingTypesList.get(at).attr("data-type"));
            }

            //write data to json
            JSONObject temp = new JSONObject();
            temp.put("category", category);
            temp.put("adType", new Gson().toJson(adTypes));
            temp.put("landingTypes", new Gson().toJson(landingTypes));

            main.add(temp);

            adTypes.clear();
            landingTypes.clear();
        }
        return main;
    }

}
