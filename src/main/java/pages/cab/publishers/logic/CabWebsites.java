package pages.cab.publishers.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import libs.hikari.tableQueries.partners.CategoryPlatform;
import libs.hikari.tableQueries.partners.Sources;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.openqa.selenium.UnhandledAlertException;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.WebsitesAddHelper;
import pages.cab.publishers.helpers.WebsitesListHelper;
import pages.cab.publishers.variables.DfpPublisher;
import core.helpers.BaseHelper;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.WebsitesAddLocators.DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT;
import static pages.cab.publishers.locators.WebsitesAddLocators.PUSH_PROVIDERS_SELECT;
import static pages.cab.publishers.locators.WebsitesListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;


public class CabWebsites {

    private final HelpersInit helpersInit;
    private final Logger log;
    WebsitesAddHelper websitesAddHelper;
    WebsitesListHelper websitesListHelper;
    CategoryPlatform categoryPlatform;
    Sources sources;

    public enum SiteStatus {
        APPROVE("Permitted"),
        REJECT("Rejected"),
        ONMODERATION("Pending"),
        DELETE("Dropped"),
        VERIFY("On verification");

        private final String status;

        SiteStatus(String statusVal) {
            this.status = statusVal;
        }

        public String getTypeValue() {
            return status;
        }
    }

    public CabWebsites(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        websitesAddHelper = new WebsitesAddHelper(log, helpersInit);
        websitesListHelper = new WebsitesListHelper(log, helpersInit);
        categoryPlatform = new CategoryPlatform();
        sources = new Sources();
    }

    private String websiteId;
    private String domain;
    private String mirror = "";
    private SiteStatus status;
    private String categoryId;
    private String pushProviderId = null;
    private String domainThatWillBeDisplayedToAdvertisersId = null;
    private String domainThatWillBeDisplayedToAdvertisersName = null;
    private String tier;
    private String sourceType;
    private String nationality;
    private String languageId;
    private String languagePriority;
    private String comments;
    private String defaultWidgetForComposite;
    private List<String> cooperationType = new LinkedList<>(Collections.singletonList("wages"));
    private ArrayList<String> bundles = new ArrayList<>();
    boolean useSubdomainsInNewsLinks = false;
    boolean opportunityToPromoteNews = true;
    boolean assignDefaultTeaserTypesForNewClientWidgets = false;
    boolean isPushCategory = false;
    boolean right_canManageSiteSource = true;
    boolean lowVolumeDomain = false;
    boolean setSomeFields = false;
    boolean useCoppaCheckbox = false;
    String mirrorsSubnet;
    String rejectReason;
    Map<String, String> adsModeration = new HashMap<>();
    private String textAlertRejectSiteAndDisableWidgets;

    // tags
    private String[] tagsType; // title|lp|url
    private Multimap<String, String> tags = ArrayListMultimap.create(); // mass tagsType and tag for them

    // Post-moderation
    private final List<String> postModerationCustomCheckboxes = new ArrayList<>();


    public String getDomain() {
        return domain;
    }

    public CabWebsites setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public CabWebsites setMirror(String mirror) {
        this.mirror = mirror;
        return this;
    }

    public CabWebsites setCategoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public CabWebsites setPushProviderId(String pushProviderId) {
        this.pushProviderId = pushProviderId;
        return this;
    }

    public void setAdsModeration(Map<String, String> adsModeration) {
        this.adsModeration.clear();
        this.adsModeration = adsModeration;
    }

    public String getLanguageId() {
        return languageId;
    }

    public CabWebsites setLanguageId(String languageId) {
        this.languageId = languageId;
        return this;
    }

    public String getWebsiteId() {
        return websiteId;
    }

    public CabWebsites setWebsiteId(String websiteId) {
        this.websiteId = websiteId;
        return this;
    }

    public CabWebsites setStatus(SiteStatus status) {
        this.status = status;
        return this;
    }

    public CabWebsites setTier(String tier) {
        this.tier = tier;
        return this;
    }

    public CabWebsites isGenerateNewValues(boolean generateNewValues) {
        websitesAddHelper.setGenerateNewValues(generateNewValues);
        return this;
    }

    public CabWebsites isSetSomeFields(boolean setSomeFields) {
        websitesAddHelper.setSetSomeFields(setSomeFields);
        websitesListHelper.setSetSomeFields(setSomeFields);
        this.setSomeFields = setSomeFields;
        return this;
    }

    public CabWebsites setCooperationType(String... cooperationType) {
        this.cooperationType.clear();
        this.cooperationType = Arrays.stream(cooperationType).collect(Collectors.toList());
        return this;
    }

    public CabWebsites setUseSubdomainsInNewsLinks(boolean useSubdomainsInNewsLinks) {
        this.useSubdomainsInNewsLinks = useSubdomainsInNewsLinks;
        return this;
    }

    public CabWebsites setCoppaCheckbox(boolean useCoppaCheckbox) {
        this.useCoppaCheckbox = useCoppaCheckbox;
        return this;
    }

    public CabWebsites setOpportunityToPromoteNews(boolean opportunityToPromoteNews) {
        this.opportunityToPromoteNews = opportunityToPromoteNews;
        return this;
    }

    public CabWebsites setTagsType(String... tagsType) {
        this.tagsType = tagsType;
        return this;
    }

    public List<String> getPostModerationCustomCheckboxes() {
        return postModerationCustomCheckboxes;
    }

    public CabWebsites setBundles(ArrayList<String> bundles) {
        this.bundles = bundles;
        return this;
    }

    public String getMirrorsSubnet() {
        return mirrorsSubnet;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public String getDomainThatWillBeDisplayedToAdvertisersId() {
        return domainThatWillBeDisplayedToAdvertisersId;
    }

    public CabWebsites setDomainThatWillBeDisplayedToAdvertisersId(String domainThatWillBeDisplayedToAdvertisersId) {
        this.domainThatWillBeDisplayedToAdvertisersId = domainThatWillBeDisplayedToAdvertisersId;
        return this;
    }

    public CabWebsites setDomainThatWillBeDisplayedToAdvertisersName(String domainThatWillBeDisplayedToAdvertisersName) {
        this.domainThatWillBeDisplayedToAdvertisersName = domainThatWillBeDisplayedToAdvertisersName;
        return this;
    }

    public CabWebsites setRight_canManageSiteSource(boolean right_canManageSiteSource) {
        this.right_canManageSiteSource = right_canManageSiteSource;
        return this;
    }

    public String getPushProviderId() {
        return pushProviderId;
    }

    public String getTextAlertRejectSiteAndDisableWidgets() {
        return textAlertRejectSiteAndDisableWidgets;
    }

    /**
     * create webSite
     */
    public String createWebsite() {
        domain = websitesAddHelper.setDomain(domain);
        fillBaseFieldsWebSite();

        websitesAddHelper.submitWebsite();
        websiteId = readWebSiteId();
        return websiteId;
    }

    /**
     * edit webSite
     */
    public void editWebsite() {
        lowVolumeDomain = !lowVolumeDomain;
        bundles.clear();
        fillBaseFieldsWebSite();
        websitesAddHelper.switchOpportunityToPromoteNews(opportunityToPromoteNews);
        websitesAddHelper.switchAssignDefaultTeaserTypesForNewClientWidgets(assignDefaultTeaserTypesForNewClientWidgets);
        mirror = websitesAddHelper.setMirror(mirror);

        websitesAddHelper.saveEditWebsite();
    }

    /**
     * fill base fields for webSite
     * add/edit method
     */
    private void fillBaseFieldsWebSite() {
        fillCategoryPushProviderOrDomainAdvertisersBlock();

        sourceType = websitesAddHelper.chooseSourceType(sourceType);
        nationality = websitesAddHelper.chooseNationality(nationality);
        languageId = websitesAddHelper.chooseLanguage(languageId);
        languagePriority = websitesAddHelper.chooseLanguagePriority(languagePriority);
        comments = websitesAddHelper.setComments(comments);
        defaultWidgetForComposite = websitesAddHelper.chooseDefaultWidgetForComposite(defaultWidgetForComposite);
        tier = websitesAddHelper.chooseTier(cooperationType, tier);
        bundles = websitesAddHelper.chooseBundles(cooperationType, bundles);

        websitesAddHelper.setCooperationTypes(cooperationType);
        websitesAddHelper.switchUseSubdomainsInNewsLinks(useSubdomainsInNewsLinks);
        websitesAddHelper.switchUseCoppaCheckbox(useCoppaCheckbox);
    }

    /**
     * check base settings in list interface
     */
    public boolean checkWebsiteInListInterface() {
        String manager = "sokwages";
        return websitesListHelper.checkDomain(domain) &
                websitesListHelper.checkCategory(categoryId) &
                websitesListHelper.checkLanguage(languageId) &
                websitesListHelper.checkTier(tier) &
                websitesListHelper.checkBundles(bundles) &
                websitesListHelper.checkManager(manager) &
                websitesListHelper.checkMirror(mirror) &
                checkStatus(status, helpersInit.getBaseHelper().parseInt(websiteId)) &
                (!right_canManageSiteSource ||
                        isPushCategory ||
                        (setSomeFields && domainThatWillBeDisplayedToAdvertisersId == null) ||
                        (websitesListHelper.checkDomainForAdvertiser(sources.getSourcesName(domainThatWillBeDisplayedToAdvertisersId)) &
                                websitesListHelper.isDisplayedDomainForAdvertiserIcon(lowVolumeDomain)
                        )
                );
    }

    /**
     * check base settings in edit interface
     */
    public boolean checkWebSiteInEditInterface() {
        return websitesAddHelper.checkDomain(domain) &
                websitesAddHelper.checkCategory(categoryId) &
                websitesAddHelper.checkBundles(bundles) &
                websitesAddHelper.checkSourceType(sourceType) &
                websitesAddHelper.checkNationality(nationality) &
                websitesAddHelper.checkLanguage(languageId) &
                websitesAddHelper.checkTier(tier) &
                websitesAddHelper.checkLanguagePriority(languagePriority) &
                websitesAddHelper.checkComments(comments) &
                websitesAddHelper.checkMirror(mirror) &
                websitesAddHelper.checkDefaultWidgetForComposite(defaultWidgetForComposite) &
                websitesAddHelper.checkUseSubdomainsInNewsLinks(useSubdomainsInNewsLinks) &
                websitesAddHelper.checkOpportunityToPromoteNews(opportunityToPromoteNews) &
                websitesAddHelper.checkAssignDefaultTeaserTypesForNewClientWidgets(assignDefaultTeaserTypesForNewClientWidgets) &
                websitesAddHelper.checkCooperationType(cooperationType) &

                (!right_canManageSiteSource ||
                        (isPushCategory ?
                                websitesAddHelper.checkPushProvider(pushProviderId) :
                                (websitesAddHelper.checkDomainThatWillBeDisplayedToAdvertisers(domainThatWillBeDisplayedToAdvertisersId) &
                                        websitesAddHelper.checkLowVolumeDomain(lowVolumeDomain)))
                );
    }

    public void fillCategoryPushProviderOrDomainAdvertisersBlock() {
        categoryId = websitesAddHelper.chooseCategory(categoryId);

        if (right_canManageSiteSource && cooperationType.contains("wages")) {

            // Если категория сайта пушовая (category_platform.platform_type = "push"), тогда под "Category" выводим push providers,
            // Если категория сайта не пушовая (category_platform.platform_type != "push"), тогда вместо "push providers" выводим "Domain, that will be displayed to advertisers"
            isPushCategory = categoryPlatform.isPlatformTypePush(categoryId);
            if (isPushCategory) {
                pushProviderId = websitesAddHelper.choosePushProvider(pushProviderId);
            } else {
                domainThatWillBeDisplayedToAdvertisersId = websitesAddHelper.chooseDomainThatWillBeDisplayedToAdvertisers(domainThatWillBeDisplayedToAdvertisersId);

                if (domainThatWillBeDisplayedToAdvertisersId != null && domainThatWillBeDisplayedToAdvertisersId.equals("-1")) {
                    lowVolumeDomain = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
                    domainThatWillBeDisplayedToAdvertisersId = websitesAddHelper.addNewDomainForAdvertisers(lowVolumeDomain, domainThatWillBeDisplayedToAdvertisersName);
                } else {
                    lowVolumeDomain = websitesAddHelper.getLowVolumeDomain();
                }
            }

        }
    }

    public CabWebsites chooseTier() {
        tier = websitesAddHelper.chooseTier(cooperationType, tier);
        return this;
    }

    public CabWebsites chooseCategory() {
        categoryId = websitesAddHelper.chooseCategory(categoryId);
        return this;
    }

    public boolean checkBundles() {
        return websitesAddHelper.checkBundles(bundles);
    }

    public boolean checkBundlesInListInterface() {
        return websitesListHelper.checkBundles(bundles);
    }

    /**
     * get website id
     */
    public String readWebSiteId() {
        return websitesListHelper.getSiteId();
    }

    /**
     * check that any option is selected
     */
    public boolean checkTierInEditInterface() {
        return websitesAddHelper.checkTier(tier);
    }

    public boolean checkTierInListInterface() {
        return websitesListHelper.checkTier(tier);
    }

    /**
     * multi filter 'Website tier'
     */
    public void chooseTierFilterValue(List listTier) {
        // get random tier row
        int currentTier = randomNumbersInt(listTier.size());
        // convert tier list to Object[]
//        Objectt[] obj = (Object[]) listTier.get(currentTier);

        // get tier name and id
        String tierName = helpersInit.getBaseHelper().getTextAndWriteLog(currentTier % 2 == 0 ? listTier.get(currentTier + 1).toString() : listTier.get(currentTier).toString());
        tier = helpersInit.getBaseHelper().getTextAndWriteLog(currentTier % 2 == 0 ? listTier.get(currentTier).toString() : listTier.get(currentTier - 1).toString());

        // expand tier filter tree and choose current value by name
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("websiteTierTree", tierName);
        websitesListHelper.submitFilter();
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getBaseHelper().getTextAndWriteLog(curator);
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        websitesListHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }

    /**
     * Delete ads.txt
     */
    public boolean deleteAdsTxt(String webSiteId) {
        if (checkEditAdsTxtIcon(webSiteId)) {
            return websitesListHelper.deleteAdsTxt(webSiteId);
        } else {
            log.info("Ads.txt is not active");
            return false;
        }
    }

    /**
     * Check Ads.txt edit button
     */
    public boolean checkEditAdsTxtIcon(String websiteId) {
        return websitesListHelper.checkEditAdsTxtIcon(websiteId);
    }

    /**
     * Add ads.txt
     */
    public void addAdsTxt(String websiteId) {
        log.info("Try to set ads.txt for website");
        websitesListHelper.clickAddAdsTxtIcon(websiteId);
        domain = websitesListHelper.getSiteDomain();
        websitesListHelper.clickCreateButton();
    }

    /**
     * Close 'Create Ads.txt file?' popup
     */
    public void closeAddAdsTxtPopup() {
        websitesListHelper.closeAddAdsTxtPopup();
    }

    /**
     * Get site domain from Cab - Websites
     */
    public void getSiteDomainFromWebsitesList() {
        domain = websitesListHelper.getSiteDomainFromWebsitesList();
    }

    /**
     * Check default values in 'Create Ads.txt file?' popup
     */
    public boolean checkAddAdsTxtPopupDefaultValues(String webSiteId) {
        websitesListHelper.clickAddAdsTxtIcon(webSiteId);
        return websitesListHelper.getSiteDomain().equals(domain)
                && websitesListHelper.checkAddAdsTxtPopupDefaultCheckboxes();
    }

    /**
     * Submit 'Create Ads.txt file?' popup without domain
     */
    public void submitAdsTxtPopupWithoutDomain() {
        websitesListHelper.clearAddAdsTxtPopupDomainInput();
        websitesListHelper.clickCreateButton();
    }

    /**
     * edit inline 'Tier'
     */
    public void editTierInline() {
        tier = websitesListHelper.editTierInline(tier);
    }

    /**
     * Check presence of add Ads.txt icon
     */
    public boolean checkAddAdsTxtIcon(int websiteId) {
        return websitesListHelper.checkAddAdsTxtIcon(websiteId);
    }

    public void openTagsCloudForm() {
        websitesListHelper.openTagsCloudForm();
    }

    public void saveTagsForm() {
        websitesListHelper.saveTagsForm();
    }

    public void clickSwitchOnTagsForAllSitesClient() {
        websitesListHelper.clickSwitchOnTagsForAllSitesClient();
    }

    public void chooseRandomTags() {
        tags = websitesListHelper.chooseRandomTags(tagsType);
    }

    public boolean checkChooseTags() {
        return websitesListHelper.checkChooseTags(tags, tagsType);
    }

    public boolean checkSearchTags() {
        return websitesListHelper.searchTags();
    }

    public void submitFilter() {
        websitesListHelper.submitFilter();
    }

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return websitesListHelper.isDisplayedAllowedClicksFromClientDomainsOnly();
    }

    public void clickAdsModerationIcon(int id) {
        $(String.format(ADS_MODERATION_ICON, id)).shouldBe(visible).click();
        checkErrors();
        ADS_MODERATION_POPUP.shouldBe(visible);
    }

    public void clickAdsModerationDeleteAds() {
        ADS_MODERATION_DELETE_ADS_BUTTON.shouldBe(visible).click();
        checkErrors();
        ADS_MODERATION_POPUP_DELETE.shouldBe(visible);
    }

    public boolean isDisplayedAdsModerationDeleteAds(){
        return ADS_MODERATION_DELETE_ADS_BUTTON.isDisplayed();
    }

    public void submitAdsModerationDeleteAds(){
        ADS_MODERATION_POPUP_DELETE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public String getAdsModerationDeletePopupText(){
        return ADS_MODERATION_POPUP_DELETE.text();
    }

    private String adsModerationSelectCountry(){ return helpersInit.getBaseHelper().selectRandomText(ADS_MODERATION_COUNTRIES_SELECT); }

    private String adsModerationSelectWidget(){ return helpersInit.getBaseHelper().selectRandomText(ADS_MODERATION_WIDGETS_SELECT); }

    private void adsModerationClickAddButton(){ ADS_MODERATION_ADD_BUTTON.click(); }

    private void adsModerationClickSaveButton(){ ADS_MODERATION_SAVE_BUTTON.click(); }

    private void adsModerationSwitchAutoApprove(boolean state){ markUnMarkCheckbox(state, ADS_MODERATION_AUTO_APPROVE_CHECKBOX); }

    private boolean adsModerationCheckAutoApprove(boolean state){ return checkIsCheckboxSelected(state, ADS_MODERATION_AUTO_APPROVE_CHECKBOX); }

    private void adsModerationFillAutoApproveHours(String hours){ ADS_MODERATION_AUTO_APPROVE_HOURS_INPUT.shouldBe(visible).val(hours);}

    private boolean adsModerationCheckAutoApproveHours(String hours){ return ADS_MODERATION_AUTO_APPROVE_HOURS_INPUT.shouldBe(visible).val().equals(hours); }

    public void adsModerationAddNewConfiguration(boolean autoApprove, String autoApproveHours, int countConfigs){
        for(int i = 0; i < countConfigs; i++) {
            adsModeration.put("Country: " +adsModerationSelectCountry(),
                    "Widget: " + adsModerationSelectWidget());
            adsModerationClickAddButton();
        }
        adsModerationSwitchAutoApprove(autoApprove);
        if(autoApprove) adsModerationFillAutoApproveHours(autoApproveHours);
        adsModerationClickSaveButton();
    }

    public boolean adsModerationCheckNewConfiguration(boolean autoApprove, String autoApproveHours){
        return adsModerationCheckAutoApprove(autoApprove) &
                (!autoApprove || adsModerationCheckAutoApproveHours(autoApproveHours)) &
                adsModerationCheckCountriesWidget();
    }

    public void adsModerationEditConfiguration(boolean autoApprove, String autoApproveHours, int... countriesIdForDelete){
        adsModerationSwitchAutoApprove(autoApprove);
        if(autoApprove) adsModerationFillAutoApproveHours(autoApproveHours);
        for (int j : countriesIdForDelete) {
            $(String.format(ADS_MODERATION_DEL_ICON, j)).click();
        }
        adsModerationClickSaveButton();
    }

    private boolean adsModerationCheckCountriesWidget(){
        List<String> countriesList = $$(ADS_MODERATION_COUNTRIES_LABELS).texts();
        List<String> widgetsList = $$(ADS_MODERATION_WIDGETS_LABELS).texts();
        List<String> keySet = new ArrayList<>(adsModeration.keySet());
        List<String> valSet = new ArrayList<>(adsModeration.values());
        Collections.sort(countriesList);
        Collections.sort(widgetsList);
        Collections.sort(keySet);
        Collections.sort(valSet);

        return countriesList.equals(keySet) &&
                widgetsList.equals(valSet);
    }

    public String getAdsModerationFieldsetConfig(){ return ADS_MODERATION_FIELDSET_CONFIG.text(); }

    public String getAdsModerationFieldsetNewConfig(){ return ADS_MODERATION_FIELDSET_NEW_CONFIG.text(); }

    public void clickPostModerationIcon(int id) {
        $(String.format(POST_MODERATION_ICON, id)).shouldBe(visible).click();
        checkErrors();
        POST_MODERATION_FORM_SELECT_ALL_CHECKBOX.shouldBe(visible);
    }

    public boolean isExistPostModerationIcon(int id) {
        SITE_DOMAIN.shouldBe(visible);
        return $(String.format(POST_MODERATION_ICON, id)).exists();
    }

    public void chooseAllPostModerationOptions() {
        POST_MODERATION_FORM_SELECT_ALL_CHECKBOX.click();
        postModerationCustomCheckboxes.clear();
        for(SelenideElement s : $$(POST_MODERATION_FORM_CHECKBOXES)){
            postModerationCustomCheckboxes.add(s.name());
        }
    }

    public void clickPostModerationFilter(boolean state) {
        markUnMarkCheckbox(state, FILTER_POST_MODERATION_CHECKBOX);
    }

    public boolean checkPostModerationFilter() {
        return websitesListHelper.isShowDataAfterFilter(POST_MODERATION_STRING_ICON);
    }

    public void clickSavePostModerationForm() {
        POST_MODERATION_FORM_SAVE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void clickFinishPostModerationForm() {
        POST_MODERATION_FORM_FINISH_MODERATION_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void fillPostModerationComment() {
        clearAndSetValue(POST_MODERATION_FORM_COMMENT_INPUT, "comment" + BaseHelper.getRandomWord(2));
    }

    public void chooseSubnetFilter(String... subnet) {
        if (subnet == null) {
            helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(FILTER_SUBNET_SELECT));
            return;
        }
        helpersInit.getBaseHelper().selectCustomValue(FILTER_SUBNET_SELECT, subnet[0]);
    }

    public void chooseRandomPostModerationOptions() {
        ElementsCollection list = $$(POST_MODERATION_FORM_CHECKBOXES);
        ArrayList<Integer> randomElements = helpersInit.getBaseHelper().getRandomCountValue(list.size(), 3);

        for (int r : randomElements) {
            list.get(r).click();
            postModerationCustomCheckboxes.add(list.get(r).attr("name"));
        }

    }

    public boolean checkPostModerationOptions() {
        $$(POST_MODERATION_FORM_CHECKBOXES).shouldBe(
                CollectionCondition.allMatch("s1", i ->
                        (postModerationCustomCheckboxes.contains(i.getAttribute("name")) & i.isSelected()) |
                                (!postModerationCustomCheckboxes.contains(i.getAttribute("name")) & !i.isSelected())
                )
        );
        return true;
    }

    public boolean isEnabledFinishModerationButton() {
        return POST_MODERATION_FORM_FINISH_MODERATION_BUTTON.isEnabled();
    }

    public void clickConfirmCancelPostModeration() {
        POST_MODERATION_CONFIRM_CANCEL_BUTTON.shouldBe(visible).click();
        checkErrors();
    }

    public void clickConfirmApprovePostModeration() {
        POST_MODERATION_CONFIRM_APPROVE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void blockByAntifrod() {
        websitesListHelper.blockByAntifrod();
    }

    public void waitUnblockByAntifrodIcon(){
        UNBLOCK_BY_ANTIFROD_ICON.shouldBe(visible);
    }

    /**
     * Switch 'Opportunity to promote news' checkbox
     */
    public void switchOpportunityToPromoteNews(boolean state) {
        websitesAddHelper.switchOpportunityToPromoteNews(state);
        websitesAddHelper.saveEditWebsite();
    }


    public boolean approveWebSite(int siteId) {
        SelenideElement approveIcon = $(String.format(APPROVE_SITE_ICON, siteId));

        approveIcon.shouldBe(visible).scrollTo().click();
        sleep(1000);
        if (SAVE_APPROVE_FORM_BUTTON.isDisplayed() && MIRRORS_SUBNET_SELECT.isDisplayed()) {
            mirrorsSubnet = helpersInit.getBaseHelper().selectRandomValue(MIRRORS_SUBNET_SELECT);
            SAVE_APPROVE_FORM_BUTTON.shouldBe(visible).click();

            if (SAVE_APPROVE_FORM_BUTTON.is(hidden)) {
                approveIcon.shouldBe(hidden);
                log.info("approve webSite true");
            }
            waitForAjaxLoader();
        }

        return helpersInit.getBaseHelper().getTextAndWriteLog(!approveIcon.isDisplayed());
    }

    public boolean rejectWebSite(int siteId) {
        $(String.format(STATUS_FIELD, siteId)).shouldBe(visible);
        SelenideElement rejectIcon = $(String.format(REJECT_SITE_ICON, siteId));

        rejectIcon.shouldBe(visible).hover().click();
        sleep(1000);
        if (REJECTION_REASON_BUTTON.isDisplayed() && REJECTION_REASON_SELECT.isDisplayed()) {
            rejectReason = helpersInit.getBaseHelper().selectRandomText(REJECTION_REASON_SELECT);
            if (rejectReason.equals("Other reason")) {
                REJECTION_REASON_INPUT.sendKeys("test reason");
                rejectReason = "test reason";
            }
            REJECTION_REASON_BUTTON.shouldBe(visible).click();
            textAlertRejectSiteAndDisableWidgets = helpersInit.getBaseHelper().getTextFromAlert();
            REJECTION_REASON_BUTTON.shouldBe(hidden);

            rejectIcon.shouldBe(hidden);
            log.info("approve webSite true");
        }

        return !rejectIcon.isDisplayed();
    }

    public void verifyWebSite(int siteId) {
        SelenideElement icon = $(String.format(VERIFY_SITE_ICON, siteId));
        icon.shouldBe(visible).scrollTo().click();
        waitForAjaxLoader();
    }

    public boolean deleteWebSite(int siteId) {
        SelenideElement deleteIcon = $(String.format(DELETE_SITE_ICON, siteId));

        try {
            deleteIcon.shouldBe(visible).click();
            if (helpersInit.getBaseHelper().checkAlertAndClose()) {
                helpersInit.getBaseHelper().checkAlertAndClose();
                return deleteIcon.is(hidden);
            }
        } catch (UnhandledAlertException e) {
            if (helpersInit.getBaseHelper().checkAlertAndClose()) {
                return deleteIcon.is(hidden);
            }
        }
        return false;
    }

    /**
     * check status website
     */
    public boolean checkStatus(SiteStatus status, int siteId) {
        SelenideElement approveIcon = $(String.format(APPROVE_SITE_ICON, siteId));
        SelenideElement rejectIcon = $(String.format(REJECT_SITE_ICON, siteId));
        SelenideElement statusLabel = $(String.format(STATUS_FIELD, siteId));

        if (helpersInit.getBaseHelper().checkDatasetContains(statusLabel.text(), status.getTypeValue())) {

            return switch (status) {
                case ONMODERATION, VERIFY -> approveIcon.isDisplayed() &
                        rejectIcon.isDisplayed();
                case APPROVE -> !approveIcon.isDisplayed() &
                        rejectIcon.isDisplayed();
                case REJECT -> approveIcon.isDisplayed() &
                        !rejectIcon.isDisplayed();
                case DELETE -> !approveIcon.isDisplayed() &
                        !rejectIcon.isDisplayed();
            };

        }
        return false;
    }

    public String getValidationPopUpOnApprove() {
        return APPROVE_VALIDATION_POPUP.shouldBe(visible).text();
    }

    public void editCategoryInline() {
        categoryId = websitesListHelper.chooseCategoryInline(categoryId);

        if (right_canManageSiteSource) {

            // Если категория сайта пушовая (category_platform.platform_type = "push"), тогда под "Category" выводим push providers,
            // Если категория сайта не пушовая (category_platform.platform_type != "push"), тогда вместо "push providers" выводим "Domain, that will be displayed to advertisers"
            isPushCategory = categoryPlatform.isPlatformTypePush(categoryId);
            if (isPushCategory) {
                pushProviderId = websitesListHelper.choosePushProviderInline(pushProviderId);
            } else {
                domainThatWillBeDisplayedToAdvertisersId = websitesListHelper.chooseDomainThatWillBeDisplayedToAdvertisersInline(domainThatWillBeDisplayedToAdvertisersId);

                if (domainThatWillBeDisplayedToAdvertisersId != null && domainThatWillBeDisplayedToAdvertisersId.equals("-1")) {
                    lowVolumeDomain = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
                    domainThatWillBeDisplayedToAdvertisersId = websitesListHelper.addNewDomainForAdvertisersInline(lowVolumeDomain, domainThatWillBeDisplayedToAdvertisersName);
                } else {
                    lowVolumeDomain = websitesListHelper.getLowVolumeDomainInline();
                }
            }

        }

        websitesListHelper.submitCategoryInline();
    }

    /**
     * Open edit ads.txt popup
     */
    public void openEditAdsTxtPopup(String websiteId) {
        websitesListHelper.clickEditAdsTxtIcon(websiteId);
    }

    /**
     * Save Ads.txt settings
     */
    public void saveAdsTxt() {
        SAVE_BUTTON.shouldBe(visible).click();
        checkErrors();
    }

    /**
     * Get ads.txt missing lines
     */
    public String checkMissingLines() {
        return executeJavaScript("return document.getElementsByClassName('" + MISSING_LINES_TEXTAREA + "')[0].value;");
    }

    /**
     * Check 'Send Mail on schedule'
     */
    public boolean checkSendMailOnScheduleSelected() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                SEND_MAIL_ON_SCHEDULE_CHECKBOX.shouldBe(visible).has(disabled) &&
                SEND_MAIL_ON_SCHEDULE_CHECKBOX.shouldBe(visible).isSelected());
    }

    /**
     * Check 'Show dashboard popup'
     */
    public boolean checkShowDashboardPopupSelected() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                SHOW_DASHBOARD_POPUP_CHECKBOX.shouldBe(visible).has(disabled) &&
                SHOW_DASHBOARD_POPUP_CHECKBOX.isSelected());
    }

    /**
     * Check 'Show dashboard popup' is not visible
     */
    public boolean checkShowDashboardPopupHidden() {
        SHOW_DASHBOARD_POPUP_CHECKBOX.shouldBe(hidden);
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    /**
     * Check 'Send Mail on schedule' is not visible
     */
    public boolean checkSendMailOnScheduleHidden() {
        SEND_MAIL_ON_SCHEDULE_CHECKBOX.shouldBe(hidden);
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    public void switchDfpIcon(DfpPublisher.DfpValues state, int siteId){ websitesListHelper.switchDfpIcon(state, siteId); }

    public boolean checkDfpIcon(DfpPublisher.DfpValues state, int siteId){ return websitesListHelper.checkDfpIcon(state, siteId); }

    public boolean isExistDfpIcon(int siteId){ return websitesListHelper.isExistDfpIcon(siteId); }

    /**
     * Click Postpone button
     */
    public void clickPostponeButton() {
        POST_MODERATION_POSTPONE_BUTTON.click();
        checkErrors();
    }

    /**
     * Get current sites clicks
     */
    public String getCurrentSitesCLicks() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(POSTPONE_POPUP_CURRENT_CLICKS_FIELD.text());
    }

    /**
     * Postpone site moderation
     */
    public void postponeSiteModeration() {
        clearAndSetValue(POSTPONE_POPUP_NEXT_CLICKS_INPUT, "1000");
        POSTPONE_POPUP_POSTPONE_BUTTON.click();
        checkErrors();
        POSTPONE_POPUP_POSTPONE_BUTTON.shouldBe(hidden, Duration.ofSeconds(10));
        POST_MODERATION_POSTPONE_BUTTON.shouldBe(hidden);
    }

    /**
     * Check 'Push provider' select is visible and
     */
    public boolean checkPushProvider() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUSH_PROVIDERS_SELECT.isDisplayed());
    }

    /**
     * Check 'Domain, that will be displayed to advertisers' select is visible
     */
    public boolean checkDomainSource() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT.isDisplayed());
    }

    public void chooseCategoriesFilterType(String type){
        websitesAddHelper.chooseCategoriesFilterType(type);
    }

    public boolean checkSearchCategoryFilter(){
        return websitesAddHelper.checkSearchCategoryFilter();
    }

    public void clearAllSelectedCategoryInMultiFilter(){
        websitesAddHelper.clearAllSelectedCategoryInMultiFilter();
    }

    public void saveEditWebsite(){
        websitesAddHelper.saveEditWebsite();
    }

    public boolean checkCategoriesFilterTypeValue(String radioButtonValue){
        return websitesAddHelper.checkCategoriesFilterTypeValue(radioButtonValue);
    }

    public boolean categoryFilterBlockIsDisplayed(){
        return websitesAddHelper.categoryFilterBlockIsDisplayed();
    }

    public boolean checkAdTypesAndLandingTypesForCategory(JSONArray obj){
        return websitesAddHelper.checkAdTypesAndLandingTypesForCategory(obj);
    }

    public JSONArray chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(int countValue){
        return websitesAddHelper.chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(countValue);
    }

    public boolean isDisplayedCategoriesFilter(){
        return websitesAddHelper.isDisplayedCategoriesFilter();
    }

    public CabWebsites editAdvertisersDomain() {
        helpersInit.getBaseHelper().selectRandomValue(DOMAIN_THAT_WILL_BE_DISPLAYED_TO_ADVERTISERS_SELECT);
        return this;
    }

    public boolean approveWebSiteIdealmedia(int siteId) {
        SelenideElement approveIcon = $(String.format(APPROVE_SITE_ICON, siteId));
        approveIcon.shouldBe(visible).scrollTo().click();
        SAVE_APPROVE_FORM_POPUP.shouldBe(visible);
        SAVE_APPROVE_FORM_BUTTON.click();
        waitForAjax();
        checkErrors();
        SAVE_APPROVE_FORM_POPUP.shouldNotBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(!approveIcon.isDisplayed());
    }
}
