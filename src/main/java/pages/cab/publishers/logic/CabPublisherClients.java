package pages.cab.publishers.logic;

import com.codeborne.selenide.Condition;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.ClientsEditHelper;
import pages.cab.publishers.helpers.ClientsListHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.ClientsEditLocators.*;
import static pages.cab.publishers.locators.ClientsListLocators.*;
import static pages.cab.publishers.variables.CabPublisherClientVariables.commentForCorrectionWages;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabPublisherClients {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final ClientsEditHelper clientsEditHelper;
    private final ClientsListHelper clientsListHelper;

    public CabPublisherClients(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        clientsEditHelper = new ClientsEditHelper(log, helpersInit);
        clientsListHelper = new ClientsListHelper(log, helpersInit);
    }

    //Payouts
    public String purseId;
    public String currentPayoutScheme;
    private double unconfirmedWagesValue;
    private double payoutsAmount;
    private double mgWalletBalanceValue;
    private double mgWalletAmountValue;
    private String paymaster24ClientName;
    private String paymaster24ClientSurname;
    private Map<String,String> paymaster24Country;
    private String paymaster24PurseNumber;
    private String paymaster24DateOfBirth;

    String capitalistCardInfo = "4276380079956881";
    String capitalistExpDateMonth = "04";
    String capitalistExpDateYear = "2023";
    String capitalistName = "John";
    String capitalistSecondName = "Smith";
    String capitalistDateOfBirth = "1955-07-07";
    String capitalistAddress = "A. Lermontove 5";
    String capitalistCity = "Paris";
    String capitalistCountry = "PF";

    private String autoReportTime;
    private String[] autoReportEmails = new String[2];
    private String autoReportPeriodicity;
    private String autoReportColumns;

    public String getPurseId() {
        return purseId;
    }

    public String getCurrentPayoutScheme() {
        return currentPayoutScheme;
    }

    public CabPublisherClients setCapitalistCardInfo(String capitalistCardInfo) {
        this.capitalistCardInfo = capitalistCardInfo;
        return this;
    }

    public CabPublisherClients setCapitalistExpDateMonth(String capitalistExpDateMonth) {
        this.capitalistExpDateMonth = capitalistExpDateMonth;
        return this;
    }

    public CabPublisherClients setCapitalistExpDateYear(String capitalistExpDateYear) {
        this.capitalistExpDateYear = capitalistExpDateYear;
        return this;
    }

    public CabPublisherClients setCapitalistName(String capitalistName) {
        this.capitalistName = capitalistName;
        return this;
    }

    public CabPublisherClients setCapitalistSecondName(String capitalistSecondName) {
        this.capitalistSecondName = capitalistSecondName;
        return this;
    }

    public CabPublisherClients setCapitalistDateOfBirth(String capitalistDateOfBirth) {
        this.capitalistDateOfBirth = capitalistDateOfBirth;
        return this;
    }

    public CabPublisherClients setCapitalistAddress(String capitalistAddress) {
        this.capitalistAddress = capitalistAddress;
        return this;
    }

    public CabPublisherClients setCapitalistCity(String capitalistCity) {
        this.capitalistCity = capitalistCity;
        return this;
    }

    public void setCapitalistCountry(String capitalistCountry) {
        this.capitalistCountry = capitalistCountry;
    }

    /**
     * check work 'Currency' filter
     */
    public boolean checkWorkCurrencyFilter() {
        String currency = clientsEditHelper.chooseFilterCurrency();
        clientsEditHelper.clickFilterButton();
        return helpersInit.getBaseHelper().checkDatasetEquals(currency, clientsEditHelper.getCurrencyType());
    }

    /**
     * Edit clients auto-payouts
     */
    public void editAutoPaid() {
        HashMap<String, String> payoutSettings = clientsListHelper.editAutoPaid();
        purseId = payoutSettings.get("purseId");
        currentPayoutScheme = payoutSettings.get("currentPayoutScheme");
    }

    /**
     * Check auto-payout settings
     */
    public boolean checkAutoPaid(String purseId) {
        clientsListHelper.clickAutoPaidIcon();
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsListHelper.checkAutoPaid(purseId, currentPayoutScheme));
    }

    public void switchForbidCreateNewWidget(boolean isTurnOn) {
        clientsEditHelper.switchForbidCreateNewWidget(isTurnOn);
        clientsEditHelper.saveSettings();
    }

    public void switchDirectPublisherDemand(boolean state){
        clientsEditHelper.switchDirectPublisherDemand(state);
    }

    public boolean checkDirectPublisherDemand(boolean state){
        return clientsEditHelper.checkDirectPublisherDemand(state);
    }

    public void fillCampaignMainWebsite(String mainWebSite){
        clientsEditHelper.fillCampaignMainWebsite(mainWebSite);
    }

    public boolean checkCampaignMainWebsite(String mainWebSite){
        return clientsEditHelper.checkCampaignMainWebsite(mainWebSite);
    }

    public void fillClientLegalName(String legalName){
        clientsEditHelper.fillClientLegalName(legalName);
    }

    public boolean checkClientLegalName(String legalName){
        return clientsEditHelper.checkClientLegalName(legalName);
    }

    /**
     * add Capitalist World Card purse
     */
    public void addCapitalistWorldPurse(String purseFo) {
        clientsEditHelper.choosePaymentMethod(purseFo);

        clientsEditHelper.setCapitalistWorldCardInfo(capitalistCardInfo);
        clientsEditHelper.chooseCapitalistWorldExpDateMonth(capitalistExpDateMonth);
        clientsEditHelper.chooseCapitalistWorldExpDateYear(capitalistExpDateYear);
        clientsEditHelper.setCapitalistWorldName(capitalistName);
        clientsEditHelper.setCapitalistWorldSecondName(capitalistSecondName);
        clientsEditHelper.setCapitalistWorldDateOfBirth(capitalistDateOfBirth);
        clientsEditHelper.setCapitalistWorldAddress(capitalistAddress);
        clientsEditHelper.setCapitalistWorldCity(capitalistCity);
        clientsEditHelper.chooseCapitalistWorldCountry(capitalistCountry);
        clientsEditHelper.addPurseValue();
    }

    /**
     * check detail Capitalist World Card purse
     */
    public boolean checkCapitalistWorldPurseDetail(String purseName) {
        String purseData = helpersInit.getBaseHelper().getTextAndWriteLog($x(".//td[(contains(text(), '" + purseName + "'))]/../td[2]").text());
        return purseData.contains(capitalistCardInfo) &&
                purseData.contains(capitalistExpDateMonth) &&
                purseData.contains(capitalistExpDateYear) &&
                purseData.contains(capitalistName) &&
                purseData.contains(capitalistSecondName) &&
                purseData.contains(capitalistDateOfBirth) &&
                purseData.contains(capitalistAddress) &&
                purseData.contains(capitalistCity) &&
                purseData.contains(capitalistCountry);
    }

    /**
     * if exist custom purse - delete it
     */
    public boolean ifExistCustomPurseDeleteIt(String purseName) {
        String purseId = clientsEditHelper.getCustomPurseId(purseName);
        log.info("pursId - " + purseId);
        if (purseId != null) {
            clientsEditHelper.deleteCustomPurse(purseId);
            return true;
        }
        return false;
    }

    /**
     * Check that delete purse button is present
     */
    public String checkPurseDeleteButton(String purseName) {
        return clientsEditHelper.getCustomPurseId(purseName);
    }

    /**
     * add Capitalist purse
     */
    public void addCapitalistPurse(String purseFo) {
        clientsEditHelper.choosePaymentMethod(purseFo);

        clientsEditHelper.setCapitalistCardInfo(capitalistCardInfo);
        clientsEditHelper.setCapitalistName(capitalistName);

        clientsEditHelper.addPurseValue();
    }

    /**
     * check detail Capitalist purse
     */
    public boolean checkCapitalistPurseDetail(String purseName) {
        String purseData = helpersInit.getBaseHelper().getTextAndWriteLog($x(".//td[(contains(text(), '" + purseName + "'))]/../td[2]").text());
        return purseData.contains(capitalistCardInfo) &&
                purseData.contains(capitalistName);
    }

    /**
     * get count errors purses fields
     */
    public int getCountErrorValidFields() {
        return clientsEditHelper.getCountErrorValidFields();
    }

    /**
     * check light input for custom field
     */
    public boolean isShowErrorLightForCustomField(String fieldName) {
        return clientsEditHelper.isShowErrorLightForCustomField(fieldName);
    }

    /**
     * get error message for capitalist fields
     */
    public String getCapitalistWorldErrorMessage() {
        return clientsEditHelper.getCapitalistWorldErrorMessage();
    }

    /**
     * Get started value of MG-wallet balance
     */
    public void getClientMgWalletBalance() {
        mgWalletBalanceValue = clientsListHelper.getClientMgWalletBalance();
    }

    /**
     * Get started value of MG-wallet balance
     */
    public void getClientMgWalletAmount() {
        mgWalletAmountValue = clientsListHelper.getClientMgWalletAmount();
    }

    /**
     * Получение текущего значения нераспределенного заработка
     */
    public void getCurrentUnconfirmedWagesValue() {
        unconfirmedWagesValue = clientsListHelper.getClientsUnconfirmedWages();
    }

    /**
     * Filling fields with values and submitting the form
     */
    public void fillSumWithCommentAndSubmitMgTransferPopup(String payOutValue, String comment, boolean autoApprove) {
        clientsListHelper.clickTransferToMgIcon();
        clientsListHelper.inputAmountForRequestedSum(payOutValue);
        clientsListHelper.inputCommentForMgTransferMoney(comment);
        clientsListHelper.setApproveCheckbox(autoApprove);
        clientsListHelper.submitAddPaymentPopup();
        helpersInit.getBaseHelper().checkAlertAndClose();
        checkErrors();
    }

    /**
     * Checking sums: MG-Wallet balance, amount of payments and Unconfirmed wages
     */
    public boolean checkCorrectTransferToMgProcess(String transactionAmountString) {
        log.info(mgWalletBalanceValue + " + " + Double.parseDouble(transactionAmountString) + " = " + clientsListHelper.getClientMgWalletBalance());
        log.info(mgWalletAmountValue + " + " + Double.parseDouble(transactionAmountString) + " = " + clientsListHelper.getClientMgWalletAmount());
        log.info(unconfirmedWagesValue + " - (" + clientsListHelper.getClientsUnconfirmedWages() + ") = " + Double.parseDouble(transactionAmountString));
        return helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().formatDouble(mgWalletBalanceValue + Double.parseDouble(transactionAmountString)),
                clientsListHelper.getClientMgWalletBalance()) &&
                helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().formatDouble(mgWalletAmountValue + Double.parseDouble(transactionAmountString)),
                        clientsListHelper.getClientMgWalletAmount()) &&
                helpersInit.getBaseHelper().checkDataset(helpersInit.getBaseHelper().formatDouble(unconfirmedWagesValue - clientsListHelper.getClientsUnconfirmedWages()),
                        Double.parseDouble(transactionAmountString));
    }

    /**
     * Filling in the fields with values and submitting the form
     */
    public void fillDataAndSubmitMgTransferPopup(String payOutValue) {
        clientsListHelper.clickTransferToMgIcon();
        clientsListHelper.inputAmountForRequestedSum(payOutValue);
        clientsListHelper.submitAddPaymentPopup();
        helpersInit.getBaseHelper().getTextAndWriteLog("All inputs in the pop-up window “MG-transfer” have been filled.");
    }

    /**
     * Проверка сообщений валидации
     */
    public String getMoneyWithdrawNotValid() {
        return clientsListHelper.getPaymentValidationError();
    }

    /**
     * Get and save payout amount.
     */
    public void getClientPayoutsAmount() {
        payoutsAmount = clientsListHelper.getClientPayoutsAmount();
    }

    /**
     * Filling fields with values and submitting the form
     */
    public void fillAmountWithCommentAndSubmitPayOutPopup(String payOutValue, String comment, String purseId, boolean autoApprove) {
        clientsListHelper.clickMoneyWithdrawIcon();
        clientsListHelper.selectPurse(purseId);
        clientsListHelper.inputAmountForWithdrawMoney(payOutValue);
        clientsListHelper.inputCommentForWithdrawMoney(comment);
        clientsListHelper.setApproveCheckbox(autoApprove);
        clientsListHelper.submitAddPaymentPopup();
        helpersInit.getBaseHelper().checkAlertAndClose();
        checkErrors();
    }

    /**
     * Checking sums: Payouts amount and Unconfirmed wages
     */
    public boolean checkCorrectPaymentProcess(String transactionAmountString) {
        double transactionAmountDouble = Double.parseDouble(transactionAmountString), unconfirmedWages, unconfirmedWages2, sumPaid, sumPaid2;
        unconfirmedWages = unconfirmedWagesValue;
        sumPaid = payoutsAmount;
        unconfirmedWages2 = clientsListHelper.getClientsUnconfirmedWages();
        sumPaid2 = clientsListHelper.getClientPayoutsAmount();
        return helpersInit.getBaseHelper().formatDouble(sumPaid + transactionAmountDouble) == sumPaid2 &&
                helpersInit.getBaseHelper().formatDouble(unconfirmedWages - unconfirmedWages2) == transactionAmountDouble;
    }

    /**
     * Заполнение полей значениями и отправка формы
     */
    public void fillDataAndSubmitPayOutPopup(String payOutValue) {
        clientsListHelper.clickMoneyWithdrawIcon();
        clientsListHelper.inputAmountForWithdrawMoney(payOutValue);
        clientsListHelper.submitAddPaymentPopup();
    }

    /**
     * Корректировка измененного заработка
     */
    public void setWagesValue(String valueForCorrectionWages) {
        clientsListHelper.clickCorrectWagesIcon();
        clientsListHelper.inputAmount(valueForCorrectionWages);
        clientsListHelper.inputComment(commentForCorrectionWages);
        clientsListHelper.submitCorrectionPopup();
    }

    /**
     * Проверка изменненного нераспределенного заработка
     */
    public boolean checkNewWagesValue(String valueForCorrectionWages) {
        log.info("Old wages value + new value: " + unconfirmedWagesValue + " + " + valueForCorrectionWages);
        return clientsListHelper.getCurrentWagesValue() == helpersInit.getBaseHelper().formatDouble(
                unconfirmedWagesValue + Double.parseDouble(valueForCorrectionWages));
    }

    /**
     * Заполнение полей значениями и отправка формы
     */
    public void fillDataAndSubmitPopup(String amountValue, String comment) {
        clientsListHelper.clickCorrectWagesIcon();
        clientsListHelper.inputAmount(amountValue);
        clientsListHelper.inputComment(comment);
        clientsListHelper.clickButtonSubmitInCorrectionPopup();
    }

    /**
     * Проверка сообщений валидации
     */
    public boolean checkValidationErrors(String error) {
        return clientsListHelper.checkValidationErrors(error);
    }

    /**
     * Check field displaying
     */
    public boolean checkRevenueShareFieldDisplaying() {
        return clientsEditHelper.checkRevenueShareFieldDisplaying();
    }

    /**
     * Check value of Revenue Share percent
     */
    public boolean checkDefaultRevenueSharePercent() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsEditHelper.checkRevenueShareDefaultPercent());
    }

    /**
     * Edit Revenue share
     */
    public void editRevenueShareForClient(String revenueShareProc) {
        clientsEditHelper.fillRevenueShareField(revenueShareProc);
        clientsEditHelper.saveSettings();
    }

    public void saveSettings() {
        clientsEditHelper.saveSettings();
    }

    public void switchAllowedClicksFromClientDomainsOnly(boolean state) {
        clientsEditHelper.switchAllowedClicksFromClientDomainsOnly(state);
    }

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return clientsListHelper.isDisplayedAllowedClicksFromClientDomainsOnly();
    }

    public void turnOnAllAdTypes() {
        clientsEditHelper.turnOnAllAdTypes();
    }

    public void turnOnCustomAdTypes(Map<String, Boolean> map) {
        clientsEditHelper.turnOnCustomAdTypes(map);
    }

    public void turnOnAllLandingTypes() {
        clientsEditHelper.turnOnAllLandingTypes();
    }

    public void turnOnCustomLandingTypes(Map<String, Boolean> map) {
        clientsEditHelper.turnOnCustomLandingTypes(map);
    }

    public void switchAssignDefaultTeaserTypesForNewClientWidgets(boolean state) {
        clientsEditHelper.switchAssignDefaultTeaserTypesForNewClientWidgets(state);
    }

    public void switchCanChangeAdTypesInDashboard(boolean state) {
        clientsEditHelper.switchCanChangeAdTypesInDashboard(state);
    }

    public boolean isDisplayedAssignDefaultTeaserTypesForNewClientWidgets() {
        return clientsEditHelper.isDisplayedAssignDefaultTeaserTypesForNewClientWidgets();
    }

    /**
     * Check Client's payout region availability
     */
    public boolean checkPayoutRegionAvailability() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsEditHelper.checkPayoutRegionSelectState());
    }

    /**
     * Get current client's payout region value
     */
    public String getPayoutRegionValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsEditHelper.getPayoutRegionValue());
    }

    /**
     * Select Client's payout region
     */
    public void setClientsPayoutRegion(String value) {
        clientsEditHelper.setClientsPayoutRegion(value);
        clientsEditHelper.saveSettings();
        checkErrors();
    }

    /**
     * Get client payment method Id
     */
    public String getPaymentMethodId() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsEditHelper.getPaymentMethodId());
    }

    /**
     * Get tooltip text
     */
    public String getTooltipText() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsEditHelper.getTooltipText());
    }

    /**
     * Check if client has enabled option of possibility to work with Payoneer system
     */
    public boolean checkClientCanWorkWithPayoneerSystemOption() {
        return CAN_USE_PAYONEER_CHECKBOX.isSelected();
    }

    /**
     * Check if after create client has disabled ad_types checkbox
     */
    public boolean checkDisableAdTypesAfterCreateClient() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAN_CHANGE_NSFW_TYPES_CHECKBOX.isSelected());
    }

    /**
     * Mark checkbox for mass action
     */
    public void markCheckbox(boolean state) {
        markUnMarkCheckbox(state, CLIENTS_CHECKBOX);
    }

    /**
     * Select checkbox for mass action
     */
    public void selectMassAction(String... name) {
        MASS_ACTIONS.selectOptionByValue(name[0]);
    }

    /**
     * check visibility select all options
     */
    public boolean checkVisibilitySelectAllOption() {
        return SELECT_ALL_OPTION.text().contains("Do you want to select all");
    }

    /**
     * is displayed mass option
     */
    public boolean isPresentMassAction(String option) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($("[value='" + option + "']").exists());
    }

    /**
     * click select all option
     */
    public void clickSelectAll() {
        SELECT_ALL_OPTION.click();
    }

    /**
     * select curator
     */
    public void selectCuratorAndApply(String curator) {
        helpersInit.getBaseHelper().selectCustomValue(CURATOR_SELECT, curator);
        SUBMIT_MASS.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        checkErrors();
    }

    /**
     * get amount of labels
     */
    public int getAmountManagerLabel(String manager) {
        return helpersInit.getBaseHelper().getTextAndWriteLog((int) MANAGER_LABELS.texts().stream().filter(el -> el.equals(manager)).count());
    }

    /**
     * Add Paymaster24 purse
     */
    public void addPaymaster24Purse(String purseFo, String purseNumberValue) {
        clientsEditHelper.choosePaymentMethod(purseFo);

        paymaster24PurseNumber = clientsEditHelper.setPaymaster24PurseNumber(purseNumberValue);
        paymaster24ClientName = clientsEditHelper.setPaymaster24ClientName();
        paymaster24ClientSurname = clientsEditHelper.setPaymaster24ClientSurname();
        paymaster24DateOfBirth = clientsEditHelper.setPaymaster24BirthDate();
        paymaster24Country = clientsEditHelper.setPaymaster24Country();
        clientsEditHelper.addPurseValue();
    }

    /**
     * Add Payoneer purse
     */
    public void addPayoneerPurse(String purseFo, String purseValue) {
        clientsEditHelper.choosePaymentMethod(purseFo);
        clientsEditHelper.setPayoneerPurseValue(purseValue);
        clientsEditHelper.addPurse();
    }


    /**
     * Check Payoneer purse
     */
    public boolean checkPayoneerPurse(String purseName, String clientsPurse) {
        String purseData = helpersInit.getBaseHelper().getTextAndWriteLog(
                $x(".//td[(contains(text(), '" + purseName + "'))]/../td[2]").text());
        return helpersInit.getBaseHelper().checkDatasetContains(clientsPurse, purseData);
    }

    /**
     * check detail Capitalist World Card purse
     */
    public boolean checkPaymaster24Detail(String purseName) {
        String purseData = helpersInit.getBaseHelper().getTextAndWriteLog(
                $x(".//td[(contains(text(), '" + purseName + "'))]/../td[2]").text());
        return helpersInit.getBaseHelper().checkDatasetContains(paymaster24PurseNumber, purseData) &&
                helpersInit.getBaseHelper().checkDatasetContains(paymaster24ClientName, purseData) &&
                helpersInit.getBaseHelper().checkDatasetContains(paymaster24ClientSurname, purseData) &&
                helpersInit.getBaseHelper().checkDatasetContains(paymaster24DateOfBirth, purseData) &&
                helpersInit.getBaseHelper().checkDatasetContains(paymaster24Country.values().stream().findFirst().get(), purseData);
    }

    /**
     * Set request amount for client less then minimum limit
     */
    public void setRequestAmountForClient(String purseName, String amountValue) {
        clientsListHelper.clickAutoPaidIcon();
        AUTO_PAID_MANUAL_SCHEME_RADIO.shouldBe(Condition.visible).click();
        AUTO_PAID_PAYOUT_AMOUNT_RADIO.shouldBe(Condition.visible).click();
        AUTO_PAID_PURSES_SELECT.selectOptionContainingText(purseName);
        clearAndSetValue(AUTO_PAID_PAYOUT_AMOUNT_INPUT, amountValue);
    }

    /**
     * Submit Payout scheme form
     */
    public void submitPayoutScheme() {
        clientsListHelper.submitPayoutScheme();
    }

    /**
     * Get validation error text
     */
    public String getValidationError() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PURSE_ADD_ERROR.shouldBe(visible).text());
    }

    /**
     * Add Capitalist IDAA, USD Purse
     */
    public void addCapitalistIdaaUsdPurse(String purseId, String purseNumber) {
        clientsEditHelper.choosePaymentMethod(purseId);
        clientsEditHelper.setCapitalistIdaaUsdPurseNumber(purseNumber);
        clientsEditHelper.addPurseValue();
    }

    /**
     * check detail Capitalist World Card purse
     */
    public boolean checkCapitalistIdaaUsdPurseDetail(String purseNumber, String purseName) {
        String purseData = helpersInit.getBaseHelper().getTextAndWriteLog(
                $x(".//td[(contains(text(), '" + purseName + "'))]/../td[2]").text());
        return helpersInit.getBaseHelper().checkDatasetContains(purseNumber, purseData);
    }

    /**
     * Get error message for capitalist account number field
     */
    public String getCapitalistIdaaUsdErrorMessage() {
        return clientsEditHelper.getCapitalistIdaaUsdErrorMessage();
    }

    public boolean checkAutoReportIcon(String iconState) {
        return iconState.equals("disabled") ?
                helpersInit.getBaseHelper().checkDatasetContains("auto-reports-gray.svg", Objects.requireNonNull(AUTO_REPORT_ICON.shouldBe(visible).getAttribute("src"))) :
                helpersInit.getBaseHelper().checkDatasetContains("auto-reports.svg", Objects.requireNonNull(AUTO_REPORT_ICON.shouldBe(visible).getAttribute("src")));
    }

    public void openAutoReportPopup() {
        AUTO_REPORT_ICON.shouldBe(visible).click();
        AUTO_REPORT_POPUP.shouldBe(visible);
        waitForAjaxLoader();
    }

    public void createAutoReport(String statsTypeValue, String... autoReportTimeValue) {
        openAutoReportPopup();
        autoReportTime = clientsListHelper.selectAutoReportTime(autoReportTimeValue);
        autoReportPeriodicity = clientsListHelper.setAutoReportPeriodicity(autoReportTimeValue);
        autoReportEmails = clientsListHelper.setAutoReportEmails(false);
        autoReportColumns = clientsListHelper.selectAutoReportColumns(statsTypeValue, false);
        AUTO_REPORT_SUBMIT_BUTTON.click();
        AUTO_REPORT_POPUP.shouldBe(hidden);
        waitForAjax();
        checkErrors();
    }

    public void editAutoReport(String statsTypeValue) {
        openAutoReportPopup();
        autoReportTime = clientsListHelper.selectAutoReportTime();
        autoReportPeriodicity = clientsListHelper.setAutoReportPeriodicity();
        autoReportEmails = clientsListHelper.setAutoReportEmails(true);
        autoReportColumns = clientsListHelper.selectAutoReportColumns(statsTypeValue, true);
        AUTO_REPORT_SUBMIT_BUTTON.click();
        AUTO_REPORT_POPUP.shouldBe(hidden);
        waitForAjax();
        checkErrors();
    }

    public boolean checkAutoReport(String statsTypeValue, boolean isAfterEdit) {
        openAutoReportPopup();
        return clientsListHelper.checkAutoReportTime(autoReportTime) &&
                clientsListHelper.checkStateAutoReportPeriodicity(autoReportPeriodicity) &&
                clientsListHelper.checkAutoReportEmails(autoReportEmails) &&
                clientsListHelper.checkStateAutoReportColumns(statsTypeValue, autoReportColumns, isAfterEdit);
    }

    public void turnOffAutoReport() {
        openAutoReportPopup();
        AUTO_REPORT_TURNOFF_BUTTON.click();
        AUTO_REPORT_POPUP.shouldBe(hidden);
        waitForAjax();
        checkErrors();
    }

    public boolean checkCloseAutoReport() {
        AUTO_REPORT_CLOSE_BUTTON.click();
        AUTO_REPORT_POPUP.shouldBe(hidden);
        waitForAjax();
        return !AUTO_REPORT_POPUP.shouldBe(hidden).isDisplayed();
    }

    public boolean checkCancelAutoReport() {
        AUTO_REPORT_CANCEL_BUTTON.click();
        AUTO_REPORT_POPUP.shouldBe(hidden);
        waitForAjax();
        sleep(2000);
        return !AUTO_REPORT_POPUP.shouldBe(hidden).isDisplayed();
    }

    public String getAutoReportEmails() {
        return autoReportEmails[0] + "," + autoReportEmails[1];
    }
}