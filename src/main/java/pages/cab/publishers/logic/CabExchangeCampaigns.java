package pages.cab.publishers.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.ExchangeCampaignsListHelper;

import static pages.cab.publishers.locators.ExchangeCampaignsListLocators.MANAGER_LABEL;


public class CabExchangeCampaigns {

    private final HelpersInit helpersInit;
    private final ExchangeCampaignsListHelper campaignsListHelper;

    public CabExchangeCampaigns(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        campaignsListHelper = new ExchangeCampaignsListHelper(log, helpersInit);
    }

    String campaignId;

    /**
     * get campaign id
     */
    public String readCampaignId() {
        campaignId = campaignsListHelper.getCampaignId();
        return campaignId;
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        campaignsListHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }
}
