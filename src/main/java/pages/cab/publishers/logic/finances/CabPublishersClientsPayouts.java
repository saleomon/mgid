package pages.cab.publishers.logic.finances;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.finances.ClientsPayoutsHelper;

import java.math.BigDecimal;
import java.util.List;

import static pages.cab.publishers.locators.finances.ClientsPayoutsLocators.*;

public class CabPublishersClientsPayouts {
    private final HelpersInit helpersInit;
    private final ClientsPayoutsHelper clientsPayoutsHelper;

    public CabPublishersClientsPayouts(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        clientsPayoutsHelper = new ClientsPayoutsHelper(log, helpersInit);
    }

    public String requestId;

    private List<String> paymentIds;

    public List<String> getPaymentIds() {
        return paymentIds;
    }


    /**
     * Check payouts color
     */
    public boolean checkPayoutsColor(String attributeValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(attributeValue, clientsPayoutsHelper.getPayoutColor());
    }

    /**
     * Get payouts requests IDs
     */
    public void getClientsPayoutRequestsIds() {
        paymentIds = clientsPayoutsHelper.getClientsPayoutRequestsIds();
    }
    /**
     * Get payouts request ID
     */
    public void getPayoutRequestId() {
        requestId = clientsPayoutsHelper.getClientsPayoutRequestId();
    }

    /**
     * Check transaction attributes
     */
    public boolean checkClientsPayoutRequests(String statusValue, String payoutAmount, String comment) {
        return helpersInit.getBaseHelper().checkDatasetEquals(statusValue, clientsPayoutsHelper.getPayoutRequestStatus()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(payoutAmount, clientsPayoutsHelper.getPaymentAmount()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(comment, clientsPayoutsHelper.getPayoutRequestCell());
    }

    /**
     * Approve payment
     */
    public boolean approvePayoutRequest() {
        return clientsPayoutsHelper.clickPayoutApproveIcon();
    }

    /**
     * Check approved requests and clear them
     */
    public void checkApprovedRequestsAndClearThem() {
        clientsPayoutsHelper.checkApprovedRequestsAndClearThem();
    }

    /**
     * Check pending requests and clear them
     */
    public void checkPendingRequestsAndClearThem() {
        clientsPayoutsHelper.checkPendingRequestsAndClearThem();
    }

    /**
     * Check payout settings
     */
    public boolean checkPayoutRequest(String purseId, String sum, String comment) {
        clientsPayoutsHelper.clickPaymentRequestPaidIcon();
        return clientsPayoutsHelper.checkPaymentSettings(purseId, sum, comment);
    }

    /**
     * Paid request
     */
    public void paidPayoutRequest() {
        clientsPayoutsHelper.submitPaidPopup();
    }

    /**
     * Cancel transaction after test ending.
     */
    public void cancelTransaction(String comment) {
        clientsPayoutsHelper.clickCancelTransactionIcon();
        clientsPayoutsHelper.fillComment(comment);
        clientsPayoutsHelper.submitCancellationPopup();
        helpersInit.getBaseHelper().refreshCurrentPage();
    }

    /**
     * Check that transaction has been canceled
     */
    public boolean checkThatTransactionCancellation(String statusValue) {
        return clientsPayoutsHelper.checkTransactionCancellation(statusValue);
    }

    /**
     * Do filtering by Region of payout system
     */
    public void findClientsPaymentsByRegion(String value) {
        clientsPayoutsHelper.setFieldClientsPaymentsByRegion(value);
        clientsPayoutsHelper.submitFilter();
    }

    /**
     * Check clients payments
     */
    public boolean checkClientsPayments(String purseName) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsPayoutsHelper.checkClientsPaymentsPurse(purseName));
    }

    /**
     * Get selected value for Region of payout system
     */
    public String getValueForClientsPaymentsByRegion() {
        return clientsPayoutsHelper.getValueForRegionOfPayoutSystemSelect();
    }

    /**
     * Get selected value for Publisher currency
     */
    public String getValueForPublisherCurrency() {
        return clientsPayoutsHelper.getValueFromPublisherCurrencySelect();
    }

    /**
     * Do filtering by Publisher currency
     */
    public void findClientsPaymentsByPublisherCurrency(String currencyValue) {
        clientsPayoutsHelper.setFieldClientsPaymentsByPublisherCurrency(currencyValue);
        clientsPayoutsHelper.submitFilter();
    }

    /**
     * Check clients payments currency icon
     */
    public boolean checkClientsCurrency(String currencySymbol) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientsPayoutsHelper.checkClientsPaymentsCurrency(currencySymbol));
    }

    /**
     * Check states of checkboxes for in payout requests
     */
    public boolean checkPaymentsPackCheckboxes(boolean state) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                clientsPayoutsHelper.checkPaymentsPackCheckboxes(state));
    }

    /**
     * Add payout requests po package
     */
    public void addPaymentsToPack() {
        clientsPayoutsHelper.addPaymentsToPack();
    }

    /**
     * Count payments
     */
    public int countPayments() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAYMENTS_ROWS.size());
    }

    /**
     * Check all payments status
     */
    public boolean checkClientsPaymentsStatus(String statusValue) {
        return PAYMENT_STATUS_CELLS.texts().stream().allMatch(i -> i.equals(statusValue));
    }

    /**
     * Compare payments sums with expected
     */
    public boolean checkPaymentSums(List paymentSums, BigDecimal convertedSum) {
        return paymentSums.stream().allMatch(i -> helpersInit.getBaseHelper().comparisonData(new BigDecimal(i.toString()), convertedSum, BigDecimal.valueOf(0.1)));
    }
}
