package pages.cab.publishers.logic.stats;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.CabStatsInformersBySitesLocators.CANVAS_FIRST_ELEMENT;
import static pages.cab.publishers.locators.CabStatsInformersBySitesLocators.GRAPH_FIRST_ICON;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabStatsInformersBySites {

    private int siteId;

    private final SelenideElement EXPORT_ICON = $("[src*='excel.png']");

    public CabStatsInformersBySites() {
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public void clickIconGraphForGetDataSite() {
        GRAPH_FIRST_ICON.shouldBe(Condition.visible).click();
        checkErrors();
    }

    public boolean clickSomeDataInGraphAndCheckOnErrors() {
        // 1 graph
        actions().moveToElement(CANVAS_FIRST_ELEMENT, 0, 0).moveByOffset(80, -80).click().perform();
        actions().moveToElement(CANVAS_FIRST_ELEMENT, 0, 0).moveByOffset(-40, -80).click().perform();

        // 2 graph
        actions().moveToElement(CANVAS_FIRST_ELEMENT, 0, 0).moveByOffset(80, 180).click().perform();
        actions().moveToElement(CANVAS_FIRST_ELEMENT, 0, 0).moveByOffset(-40, 180).click().perform();
        return checkErrors();
    }

    public String getValueByHeader(String columnName){
        return $x(".//div[@id='sites-table']//tbody/tr[@data-row-id='" + siteId + "']/td[count(//div[@id='sites-table']//th[span/a[contains(@href, 'sort_by/" + columnName + "')]]/preceding-sibling::th)+4]").text();
    }

    public void clickDownloadStats(){
        EXPORT_ICON.click();
    }
}
