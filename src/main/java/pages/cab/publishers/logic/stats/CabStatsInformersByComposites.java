package pages.cab.publishers.logic.stats;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CabStatsInformersByComposites {

    private int widgetId;

    private final SelenideElement EXPORT_ICON = $("[src*='excel.png']");

    public CabStatsInformersByComposites() { }

    public void setWidgetId(int widgetId) {
        this.widgetId = widgetId;
    }

    public String getValueByHeader(String columnName){
        return $x(".//table[@id='composites-stats-table']//tbody/tr[td//a[contains(@href, '?c_id=" + widgetId + "')]]/td[count(//table[@id='composites-stats-table']//th[span/a[contains(@href, 'sort_by/" + columnName + "')]]/preceding-sibling::th)+4][not(.//a)]").text();
    }

    public void clickDownloadStats(){
        EXPORT_ICON.click();
    }
}
