package pages.cab.publishers.logic.stats;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CabStatsInformersByClients {

    private int clientId;

    private final SelenideElement EXPORT_ICON = $("[src*='excel.png']");

    public CabStatsInformersByClients() { }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getValueByHeader(String columnName){
        return $x(".//div[@id='clients-table']//tbody/tr[@data-row-id='" + clientId + "']/td[count(//div[@id='clients-table']//th[span/a[contains(@href, 'sort_by/" + columnName + "')]]/preceding-sibling::th)+4]").text();
    }

    public void clickDownloadStats(){
        EXPORT_ICON.click();
    }
}
