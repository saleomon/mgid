package pages.cab.publishers.logic.stats;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class CabStatsInformersByCountries {

    private String country;
    private final SelenideElement EXPORT_ICON = $("[src*='excel.png']");

    public CabStatsInformersByCountries() { }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getValueByHeader(String columnName){
        return $x(".//table[@id='countries-stats-table']//tbody/tr[td[contains(text(), '" + country + "')]]/td[count(//table[@id='countries-stats-table']//th[span/a[contains(@href, 'sort_by/" + columnName + "')]]/preceding-sibling::th)+2]").text();
    }

    public void clickDownloadStats(){
        EXPORT_ICON.click();
    }
}
