package pages.cab.publishers.logic.widgets;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.publishers.locators.widgets.WidgetCodeLocators.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabInformersCode {
    private final HelpersInit helpersInit;
    private final Logger log;

    private String[] codeArray;

    public CabInformersCode(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    private void parseCodePage() {
        codeArray = CODE_lABEL.text().split("\\n");
    }

    public boolean checkShortCode(int siteId, String domain, int compositeId) {
        parseCodePage();
        String originCode = "<!-- Composite Start --> <div id=\"M%sScriptRootC%s\"></div> <script src=\"https://jsc.mgid.com/t/e/%s.%s.js\" async></script> <!-- Composite End -->";

        String act_code = codeArray[1];

        String exp_code = String.format(originCode, siteId, compositeId, domain, compositeId);

        return helpersInit.getBaseHelper().checkDatasetEquals(exp_code, act_code);
    }

    /**
     * get title in XML code widgets
     */
    public boolean checkXmlCode(int compositeId, String tokenId) {
        checkErrors();
        return helpersInit.getBaseHelper().checkDatasetEquals(CODE_lABEL.shouldBe(visible).text(),
                "This is the link for the XML feed\n" +
                "http://api-wa.mgid.com/" + compositeId + "?content_type=xml&token=" + tokenId + "&ip={ip}&ua={user-agent}\n" +
                        "\n" +
                        "where:\n" +
                        "token= your personal token\n" +
                        "src_id=12312 is a source ID (subID)\n" +
                        "ip={ip} where {ip} should be replaced with real user IP\n" +
                        "ua = {user-agent} , it’s obligatory to use urlencoded when using this parameter (see the link below)\n" +
                        "https://www.w3schools.com/tags/ref_urlencode.asp\n" +
                        "\n" +
                        "You need to pass Accept language header\n" +
                        "https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language\n" +
                        "This would help to better target the audience you have.\n" +
                        "\n" +
                        "Token:\n" +
                        tokenId);
    }

    public String getLinkWidgetCodeForClient() {
        return GET_WIDGET_LINK_FOR_CLIENT.attr("href");
    }

    public String getLinkForHeaderWidget() {
        return HEADER_WIDGET_CODE_LINK.attr("href");
    }

    /**
     * Check AMP widgets code and instruction for managers presence in Cab
     */
    public boolean checkAMPCodeForWidgetsInCab(int widgetId, String siteName, int siteId, String subnetName) {
        log.info("Check AMP widgets code presence");
        return helpersInit.getBaseHelper().checkDatasetContains(CODE_lABEL.text(), "AMP code\n" +
                "Html parameter should be replaced on <html ⚡>\n" +
                "Please check for 2 scripts in the client's page head.\n" +
                "<script async custom-element=\"amp-ad\" src=\"https://cdn.ampproject.org/v0/amp-ad-0.1.js\"></script>\n" +
                "<script async src=\"https://cdn.ampproject.org/v0.js\"></script>\n" +
                "<amp-embed width=\"600\" height=\"600\" layout=\"responsive\" type=\"" + subnetName.toLowerCase() + "\" data-publisher=\"" + siteName.toLowerCase()
                + "\" data-widget=\"" + widgetId + "\" data-container=\"M" + siteId + "ScriptRootC" + widgetId + "\" data-block-on-consent=\"_till_responded\" > </amp-embed>");
    }

    /**
     * Check short code in Cab
     */
    public boolean checkShortCodeWithClicktracking(int widgetId, String siteName, int siteId, String macros) {
        return helpersInit.getBaseHelper().checkDatasetContains(CODE_lABEL.text(),
                "<!-- Composite Start --> <div id=\"M" + siteId + "ScriptRootC" + widgetId + "\"></div> " +
                        "<script>var MGClickTracking = window.MGClickTracking || \"" + macros + "\";</script> " +
                        "<script src=\"https://jsc.mgid.com/t/e/" + siteName + "." + widgetId + ".js\" async></script> <!-- Composite End -->");
    }

    /**
     * Check Instant Articles code in Cab
     */
    public boolean checkInstantArticlesCodeWithClicktracking(int widgetId, String siteName, int siteId, String macros) {
        return helpersInit.getBaseHelper().checkDatasetContains(CODE_lABEL.text(),
                "<!-- Composite Start --> <div id=\"M" + siteId + "ScriptRootC" + widgetId + "\"></div> " +
                        "<script>var MGClickTracking = \"" + macros + "\";</script> <script>var isFBIA" + widgetId + " = true;</script> " +
                        "<script src=\"https://jsc.mgid.com/t/e/" + siteName + "." + widgetId + ".js\" async></script> <!-- Composite End -->");
    }

    public boolean checkInstantArticlesCode(int widgetId, String siteName, int siteId) {
        return helpersInit.getBaseHelper().checkDatasetContains(CODE_lABEL.text(),
                "<!-- Composite Start --> <div id=\"M" + siteId + "ScriptRootC" + widgetId + "\"></div> <script>var isFBIA" + widgetId + " = true;</script> " +
                        "<script src=\"https://jsc.mgid.com/t/e/" + siteName + "." + widgetId + ".js\" async></script> <!-- Composite End -->");
    }

    public boolean checkInstantArticlesWidgetsInCab(int widgetId, String siteName, int siteId, String jsc) {
        log.info("Check Instant Articles widgets code presence");
        return helpersInit.getBaseHelper().checkDatasetContains(CODE_lABEL.text(),
                "<!-- Composite Start --> <div id=\"M" + siteId + "ScriptRootC" + widgetId + "\"></div> <script>var isFBIA" + widgetId + " = true;" +
                        "</script> <script src=\"https://" + jsc + "/t/e/" + siteName + "." + widgetId + ".js\" async></script> <!-- Composite End -->");
    }
}
