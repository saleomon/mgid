package pages.cab.publishers.logic.widgets;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.widgets.EditCodeLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabEditCode {

    private final Logger log;
    private final HelpersInit helpersInit;

    public CabEditCode(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * вкл/выкл чекбокс - 'Styles changed manually'
     */
    public void manuallyStyleSwitchOn(boolean isSwitch) {
        log.info("manuallyStyleSwitchOn: start");
        markUnMarkCheckbox(isSwitch, MANUALLY_STYLE_CHECKBOX);
        log.info("manuallyStyleSwitchOn: turn " + isSwitch);
    }

    /**
     * сохраняем изменения в интерфейсе 'Publishers - Widgets - Edit code'
     */
    public boolean saveEditCodeInterface() {
        clickInvisibleElementJs(SAVE_CODE_BUTTON.shouldBe(visible));
        log.info("click: SAVE_CODE_BUTTON");
        helpersInit.getBaseHelper().checkAlertAndClose();
        log.info("alert: close");
        checkErrors();
        return helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    public void fillTitleLimit(int limit){ sendKey(TITLE_LIMIT_INPUT, String.valueOf(limit)); }

    public void fillDescriptionLimit(int limit){ sendKey(DESC_LIMIT_INPUT, String.valueOf(limit)); }

    public void clickViewButton(){
        VIEW_BUTTON.click();
        checkErrors();
    }

    public void switchToViewIframe(){
        IFRAME_WITH_VIEW.shouldBe(visible, ofSeconds(14));
        switchTo().frame(IFRAME_WITH_VIEW);
        checkErrors();
    }

    public void switchToCssRadio(){
        CSS_RADIO.click();
    }

    // loop
    public void changeStyle(String attrName, String numberOfDivInAttr, String newValue){
        int count = 0;
        while(!$x(".//div[span[text()='" + attrName + "']]/../following-sibling::div[" + numberOfDivInAttr + "]//span[@class='ace_constant ace_numeric']").isDisplayed() && count < 20){
            actions().sendKeys($x(".//div[@id='styles_box']//div[@class='ace_content']"), Keys.PAGE_DOWN, Keys.CONTROL)
                    .build().perform();
            sleep(500);
            count++;
        }

        actions().sendKeys($x(".//div[span[text()='" + attrName + "']]/../following-sibling::div[" + numberOfDivInAttr + "]//span[@class='ace_constant ace_numeric']"), Keys.RIGHT, Keys.BACK_SPACE, Keys.BACK_SPACE + newValue)
                .build().perform();
    }
}
