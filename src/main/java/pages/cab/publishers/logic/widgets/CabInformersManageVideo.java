package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.checked;
import static com.codeborne.selenide.Condition.visible;
import static pages.cab.publishers.locators.widgets.VideoManageLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabInformersManageVideo {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final Map<SelenideElement, Integer> selectedVideoGroups = new HashMap<>();


    public CabInformersManageVideo(HelpersInit helpersInit, Logger log) {
        this.helpersInit = helpersInit;
        this.log = log;
    }


    /**
     * Change 'Skip time' in video widget config
     */
    public void changeSkipTime() {
        log.info("Change 'Skip time' in video widget config");
        clearAndSetValue(SKIP_TIME_INPUT, String.valueOf((Integer.parseInt(Objects.requireNonNull(SKIP_TIME_INPUT.getValue())) + 1)));
        SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * Get video groups states names
     */
    public String[] getVideoGroupStates() {
        String[] stateValues = new String[3];
        VIDEO_GROUP_STATES_HEADERS.texts().toArray(stateValues);
        return stateValues;
    }

    /**
     * Select video groups and save widget
     */
    public void selectVideoGroups(int enabledVideoGroupsItems) {
        ElementsCollection videoGroups = VIDEO_GROUPS_TABLE.$$(VIDEO_GROUPS_ROWS);
        for (int i = 0; i < enabledVideoGroupsItems; i++) {
            selectedVideoGroups.put(videoGroups.get(randomNumbersInt(videoGroups.size())),
                    randomNumbersInt(3));
        }
        selectedVideoGroups.forEach((row, checkbox) -> row.$$(VIDEO_GROUP_NOT_SET_RADIOBUTTONS).get(checkbox).setSelected(true));
        SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * Check saved video groups
     */
    public boolean checkVideoGroups() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                selectedVideoGroups.entrySet().stream().allMatch(e ->
                        e.getKey().$$(VIDEO_GROUP_NOT_SET_RADIOBUTTONS).get(e.getValue()).exists() &&
                                e.getKey().$$(VIDEO_GROUP_NOT_SET_RADIOBUTTONS).get(e.getValue()).is(checked)));
    }

    public void expandVideoConfig() {
        markUnMarkCheckbox(true, VIDEO_CONFIG_CHECKBOX.shouldBe(visible).scrollTo());
    }
}
