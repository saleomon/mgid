package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.*;
import com.google.common.collect.Table;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import core.base.HelpersInit;
import pages.cab.publishers.variables.ContractsData;
import pages.cab.publishers.variables.DfpPublisher.DfpValues;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.widgets.WidgetsListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabWidgetsList {

    private final HelpersInit helpersInit;
    private final Logger log;

    boolean setPushCustomField = false;

    // Push widget (for set custom field use setPushCustomField(true))
    private String pushPlacement, pushDelay, pushStartHour, pushEndHour, pushIcon;

    // Contracts
    private String cpcPrice, cpmPrice, eCpmPrice, monthPrice, monthExpDate, rsPercentage, guaranteedType, guaranteedPrice, guaranteedPercentage;

    //Managers alerts
    private String managersAlertComparisonPeriod, managersAlertTimePeriodFrom, managersAlertTimePeriodTo;
    private Map<String, Map<String, String>> managersAlertTypeAndDelta = new HashMap<>();

    public enum MassAction {
        CONTENT_TYPES("set-up-widgets-content-types"),
        CHANGE_STOP_WORDS("change-stop-words"),
        TURN_OFF_WIDGETS_CATEGORIES("turn-off-widgets-categories"),
        CHANGE_CONTRACT_PRICE("change-widgets-contract-price"),
        CHANGE_WIDGETS_CATEGORIES("change-widgets-categories"),
        CHANGE_WAGES_CAMPAIGN_FILTER("change-wages-campaign-filter"),
        CHANGE_EXCHANGE_CAMPAIGN_FILTER("change-exchange-campaign-filter"),
        CHANGE_EXCHANGE_WIDGETS_CATEGORIES("change-exchange-categories");

        private final String value;

        MassAction(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public enum ChangeExchangeCampaignFilter {
        ADD_TO_EXCEPT_FILTER("add-to-except-filter"),
        REMOVE_FROM_EXCEPT_FILTER("remove-from-except-filter");

        private final String value;

        ChangeExchangeCampaignFilter(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public enum ChangeWagesCampaignFilter {
        ADD_TO_EXCEPT_FILTER("add-to-except-filter"),
        REMOVE_FROM_EXCEPT_FILTER("remove-from-except-filter");

        private final String value;

        ChangeWagesCampaignFilter(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public enum ChangeWidgetsCategories {
        ADD_TO_EXCEPT_FILTER("add-to-except-filter"),
        REMOVE_TO_EXCEPT_FILTER("remove-from-except-filter"),
        ADD_TO_ONLY_FILTER("add-to-only-filter"),
        REMOVE_TO_ONLY_FILTER("remove-from-only-filter");

        private final String value;

        ChangeWidgetsCategories(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public enum ChangeStopWords {
        ADD_STOP_WORDS("add-to-except-filter"),
        REMOVE_STOP_WORDS("remove-from-except-filter");

        private final String value;

        ChangeStopWords(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public enum ChangeExchangeWidgetsCategories {
        ADD_TO_ONLY_FILTER("add-to-only-filter"),
        REMOVE_TO_ONLY_FILTER("remove-from-only-filter");

        private final String value;

        ChangeExchangeWidgetsCategories(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    public CabWidgetsList(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public CabWidgetsList setPushCustomField(boolean setPushCustomField) {
        this.setPushCustomField = setPushCustomField;
        return this;
    }

    public CabWidgetsList setPushStartHour(String pushStartHour) {
        this.pushStartHour = pushStartHour;
        return this;
    }

    public CabWidgetsList setPushEndHour(String pushEndHour) {
        this.pushEndHour = pushEndHour;
        return this;
    }

    public CabWidgetsList setPushPlacement(String pushPlacement) {
        this.pushPlacement = pushPlacement;
        return this;
    }

    public CabWidgetsList setPushDelay(String pushDelay) {
        this.pushDelay = pushDelay;
        return this;
    }

    public void setManagersAlertTypeAndDelta(Map<String, Map<String, String>> managersAlertTypeAndDelta) {
        this.managersAlertTypeAndDelta = managersAlertTypeAndDelta;
    }

    public void clearManagersAlertTypeAndDelta(){ managersAlertTypeAndDelta.clear(); }

    //////////////////////////////////////////////// PUSH - START /////////////////////////////////////////////////////////

    /**
     * заполняем поле "Specify The Order Of Sending Content"
     */
    public String setPlacement(String placement) {
        if (setPushCustomField && placement == null) return null;
        StringBuilder value = new StringBuilder();
        if (placement != null) {
            value = new StringBuilder(placement);
        } else {
            String[] params = new String[]{"t", "n"};
            int r = randomNumbersInt(40);
            for (int i = 0; i < r; i++) {
                value.append(params[randomNumbersInt(params.length)]).append("-").append(params[randomNumbersInt(params.length)]).append("-");
            }
            value.append("t");
        }
        clearAndSetValue(PUSH_PLACEMENT_INPUT.shouldBe(visible, ofSeconds(32)), value.toString());
        return helpersInit.getBaseHelper().getTextAndWriteLog(value.toString());
    }

    /**
     * check 'Specify The Order Of Sending Content'
     */
    public boolean checkPlacement(String placement) {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_PLACEMENT_INPUT.shouldBe(visible, ofSeconds(30)).val(), placement);
    }

    /**
     * check push has icon can't delete widget
     * tickers_composite.nodel =1
     */
    public boolean checkPushWidgetHasNoDeleteFlag(int widgetId) {
        helpersInit.getBaseHelper().waitDisplayedElement($("#nodel" + widgetId + "[data-value='1']"));
        return helpersInit.getBaseHelper().checkDatasetEquals($("#nodel" + widgetId).attr("data-value"), "1");
    }

    /**
     * заполняем поле Delay After Delivered Push Message (min), пределы: 0 - 1440
     */
    public String fillPushDelay(String pushDelay) {
        if (setPushCustomField && pushDelay == null) return null;

        String delay = pushDelay == null ?
                randomNumberFromRange(0, 1440) :
                pushDelay;
        clearAndSetValue(PUSH_DELAY_INPUT.shouldBe(visible, ofSeconds(20)), delay);
        return helpersInit.getBaseHelper().getTextAndWriteLog(delay);
    }

    /**
     * check 'Delay After Delivered Push Message (min)'
     */
    public boolean checkPushDelay(String pushDelay) {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_DELAY_INPUT.val(), pushDelay);
    }

    /**
     * Push Start Hour
     */
    public String selectPushStartHour(String startHour) {
        if (setPushCustomField && startHour == null) return null;
        String value = startHour == null ?
                randomNumberFromRange(0, 12) :
                startHour;
        PUSH_START_HOUR_SELECT.shouldBe(visible, ofSeconds(20)).selectOptionByValue(value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * check 'Push Start Hour'
     */
    public boolean checkPushStartHour(String pushStartHour) {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_START_HOUR_SELECT.getSelectedValue(), pushStartHour);
    }

    /**
     * Push End Hour
     */
    public String selectPushEndHour(String pushEndHour) {
        if (setPushCustomField && pushEndHour == null) return null;
        String value = pushEndHour == null ?
                randomNumberFromRange(13, 23) :
                pushEndHour;
        PUSH_END_HOUR_SELECT.selectOptionByValue(value);
        return helpersInit.getBaseHelper().getTextAndWriteLog(value);
    }

    /**
     * check 'Push End Hour'
     */
    public boolean checkPushEndHour(String pushEndHour) {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_END_HOUR_SELECT.getSelectedValue(), pushEndHour);
    }

    /**
     * выбираем иконку пуш-уведомления
     */
    public String setPushIcon(String currentIcon) {
        if (setPushCustomField && currentIcon == null) return null;

        PUSH_IMAGE_ICON.click();
        PUSH_IMAGE_FORM.shouldBe(visible);
        ElementsCollection icons = PUSH_IMAGE_FORM.$$("img");
        int icon = randomNumbersInt(icons.size());
        icons.get(icon).click();
        return helpersInit.getBaseHelper().getTextAndWriteLog(icons.get(icon).attr("src"));
    }

    /**
     * check 'push icon'
     */
    public boolean checkPushIcon(String icon) {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_IMAGE_ICON.attr("src"), icon);
    }

    /**
     * save push
     */
    public void savePushWidget() {
        int timer = 0;
        do {
            clickInvisibleElementJs(PUSH_SUBMIT);
            waitForAjax();
            checkErrors();
            PUSH_POPUP_LOADER.shouldBe(hidden, ofSeconds(15));
            sleep(1000);
            timer++;
        } while (helpersInit.getBaseHelper().isEmergencyModePresentCab() && PUSH_SUBMIT.isDisplayed() && timer < 10);
    }

    /**
     * check is push widget created
     */
    public boolean isPushWidgetCreated(int widgetId) {
        helpersInit.getBaseHelper().waitDisplayedElement($("[id=convertPushIcon" + widgetId + "][src*='push_widget_active.png']"));
        return helpersInit.getBaseHelper().getTextAndWriteLog($("[id=convertPushIcon" + widgetId + "][src*='push_widget_active.png']").isDisplayed());
    }

    /**
     * open push pop-up
     * RKO
     */
    public void openPushPopUp(int widgetId) {
        helpersInit.getBaseHelper().waitDisplayedElement(EDIT_CODE_ICON);
        clickInvisibleElementJs($("#convertPushIcon" + widgetId).shouldBe(visible, ofSeconds(10)));
        log.info("wait open popUp with push settings");
        PUSH_FORM.shouldBe(visible, ofSeconds(20));
        log.info("popUp with push settings -> Opened");
        checkErrors();
    }

    /**
     * create push widget
     */
    public boolean createEditPushWidget(int widgetId) {
        pushPlacement = setPlacement(pushPlacement);
        pushDelay = fillPushDelay(pushDelay);
        pushStartHour = selectPushStartHour(pushStartHour);
        pushEndHour = selectPushEndHour(pushEndHour);
        pushIcon = setPushIcon(pushIcon);

        savePushWidget();
        return helpersInit.getMessageHelper().isSuccessMessagesCab() &&
                isPushWidgetCreated(widgetId);
    }

    /**
     * check 'push-widget' settings
     */
    public boolean checkPushSettingsInForm(int widgetId) {
        openPushPopUp(widgetId);
        return checkPlacement(pushPlacement) &&
                checkPushDelay(pushDelay) &&
                checkPushStartHour(pushStartHour) &&
                checkPushEndHour(pushEndHour) &&
                checkPushIcon(pushIcon);
    }

    /////////////////////////////////////////////// PUSH - FINISH ///////////////////////////////////////////////////////////

    ///////////////////////// CONTRACT TYPE -> START ///////////////////////////////

    /**
     * set custom contract type
     */
    public void setContractType(ContractsData.Contracts type, ContractsData.GuaranteedContracts typeGuaranteed) {
        switch (type) {
            case CPC -> {
                clickCpcContract();
                cpcPrice = setCpcContractPrice(cpcPrice);
            }
            case CPM -> {
                clickCpmContract();
                cpmPrice = setCpmContractPrice(cpmPrice);
            }
            case ECPM -> {
                clickEcpmContract();
                eCpmPrice = setEcpmContractPrice(eCpmPrice);
            }
            case MONTH -> {
                clickMonthContract();
                monthPrice = setMonthContractPrice(monthPrice);
                monthExpDate = setMonthExpDate(monthExpDate);
            }
            case RS -> {
                clickRsContract();
                rsPercentage = setRsPercentageContract(rsPercentage);
            }
            case GUARANTEED -> {
                clickGuaranteedContract();
                guaranteedType = setGuaranteedType(typeGuaranteed.getGuaranteedContractType());
                guaranteedPrice = setGuaranteedContractPrice(guaranteedPrice);
                guaranteedPercentage = setGuaranteedPercentage(guaranteedPercentage);
            }
        }
        saveContract();
    }

    /**
     * check custom 'Contracts'
     */
    public boolean checkContractType(ContractsData.Contracts type) {
        clickWidgetsContracts();
        return switch (type) {
            case CPC -> helpersInit.getBaseHelper().checkDatasetContains(getContractValue(), cpcPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getCpcContractPrice(), cpcPrice);
            case CPM -> helpersInit.getBaseHelper().checkDatasetContains(getContractValue(), cpmPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getCpmContractPrice(), cpmPrice);
            case ECPM -> helpersInit.getBaseHelper().checkDatasetContains(getContractValue(), eCpmPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getEcpmContractPrice(), eCpmPrice);
            case MONTH -> helpersInit.getBaseHelper().checkDatasetContains(getContractValue(), monthPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getMonthContractPrice(), monthPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getMonthContractExpDate(), monthExpDate);
            case RS -> helpersInit.getBaseHelper().checkDatasetContains(getContractValue(), rsPercentage) &
                    helpersInit.getBaseHelper().checkDatasetContains(getRsContractPrice(), rsPercentage);
            case GUARANTEED -> helpersInit.getBaseHelper().checkDatasetContains(getGuaranteedContractPrice(), guaranteedPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getGuaranteedContractType(),guaranteedType) &
                    helpersInit.getBaseHelper().checkDatasetContains(getContractValue(),guaranteedPercentage) &
                    helpersInit.getBaseHelper().checkDatasetContains(getGuaranteedType(),guaranteedType) &
                    helpersInit.getBaseHelper().checkDatasetContains(getGuaranteedPrice(),guaranteedPrice) &
                    helpersInit.getBaseHelper().checkDatasetContains(getGuaranteedPercentage(),guaranteedPercentage);
            case MIXED -> false;
        };
    }

    /**
     * click and open 'Contracts edit form'
     */
    public void clickWidgetsContracts() {
        clickInvisibleElementJs(CONTRACTS_LABEL.hover().shouldBe(visible));
        CONTRACTS_FORM.shouldBe(visible);
        sleep(1000);
    }

    /**
     * click 'cpc' contracts
     */
    public void clickCpcContract() { CPC_RADIO.shouldBe(visible).click(usingJavaScript()); }

    /**
     * click 'cpm' contracts
     */
    public void clickCpmContract() {
        CPM_RADIO.shouldBe(visible).click(usingJavaScript());
    }

    /**
     * click 'ecpm' contracts
     */
    public void clickEcpmContract() {
        ECPM_RADIO.shouldBe(visible).click(usingJavaScript());
    }

    /**
     * click 'month' contracts
     */
    public void clickMonthContract() { MONTH_RADIO.shouldBe(visible).click(usingJavaScript()); }

    /**
     * click 'rs' contracts
     */
    public void clickRsContract() {
        RS_RADIO.shouldBe(visible).click(usingJavaScript());
    }

    /**
     * click 'guaranteed' contracts
     */
    public void clickGuaranteedContract() {
        GUARANTEED_RADIO.shouldBe(visible).click(usingJavaScript());
    }

    /**
     * set 'cpc' contract price
     */
    public String setCpcContractPrice(String cpcPrice) {
        String cpc = cpcPrice == null ?
                helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.00, 80.00).replace(",", ".") :
                cpcPrice;
        sendKey(CPC_PRISE_INPUT.shouldBe(visible), cpc);
        return helpersInit.getBaseHelper().getTextAndWriteLog(cpc);
    }

    /**
     * set 'cpm' contract price
     */
    public String setCpmContractPrice(String cpmPrice) {
        String cpm = cpmPrice == null ?
                helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.00, 80.00).replace(",", ".") :
                cpmPrice;
        sendKey(CPM_PRISE_INPUT.shouldBe(visible), cpm);
        return helpersInit.getBaseHelper().getTextAndWriteLog(cpm);
    }

    /**
     * get 'cpc' contract price
     */
    public String getCpcContractPrice() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CPC_PRISE_INPUT.val());
    }

    /**
     * get 'cpm' contract price
     */
    public String getCpmContractPrice() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CPM_PRISE_INPUT.val());
    }

    /**
     * get 'ecpm' contract price
     */
    public String getEcpmContractPrice() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ECPM_PRISE_INPUT.val());
    }

    /**
     * get 'month' contract price
     */
    public String getMonthContractPrice() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MONTH_PRISE_INPUT.val());
    }

    /**
     * get 'month' exp date
     */
    public String getMonthContractExpDate() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MONTH_EXPIRE_DATE_INPUT.val());
    }

    /**
     * set 'eCpm' contract price
     */
    public String setEcpmContractPrice(String eCpmPrice) {
        String eCpm = eCpmPrice == null ?
                helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.00, 80.00).replace(",", ".") :
                eCpmPrice;
        sendKey(ECPM_PRISE_INPUT.shouldBe(visible), eCpm);
        return helpersInit.getBaseHelper().getTextAndWriteLog(eCpm);
    }

    /**
     * set 'month' contract price
     */
    public String setMonthContractPrice(String monthPrice) {
        String month = monthPrice == null ?
                helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.00, 80.00).replace(",", ".") :
                monthPrice;
        sendKey(MONTH_PRISE_INPUT.shouldBe(visible), month);
        return helpersInit.getBaseHelper().getTextAndWriteLog(month);
    }

    /**
     * set 'month' expiration date
     */
    public String setMonthExpDate(String monthExpDate) {
        // get random date
        ZoneId zoneId = ZoneId.of("Pacific/Honolulu");
        LocalDate dayNow = LocalDate.now(zoneId);

        String monthDate = monthExpDate == null ?
                DateTimeFormatter.ofPattern("yyyy-MM-dd").format(dayNow.plusDays(3)) : // MM/dd/yyyy
                monthExpDate;

        clearAndSetValue(MONTH_EXPIRE_DATE_INPUT.shouldBe(visible), monthDate);

        return helpersInit.getBaseHelper().getTextAndWriteLog(monthDate);
    }

    /**
     * set 'rs' contract percentage
     */
    public String setRsPercentageContract(String rsPercentage) {
        String rs = rsPercentage == null ?
                String.valueOf(randomNumberFromRangeInt(10, 80)) :
                rsPercentage;
        sendKey(RS_PERCENTAGE_INPUT.shouldBe(visible), rs);
        return helpersInit.getBaseHelper().getTextAndWriteLog(rs);
    }

    /**
     * get 'rs' contract percentage
     */
    public String getRsContractPrice() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(RS_PERCENTAGE_INPUT.val());
    }

    /**
     * set 'guaranteed' contract price
     */
    public String setGuaranteedContractPrice(String guaranteedPrice) {
        String guaranteed = guaranteedPrice == null ?
                helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.00, 80.00).replace(",", ".") :
                guaranteedPrice;
        sendKey(GUARANTEED_PRICE_INPUT.shouldBe(visible), guaranteed);
        return helpersInit.getBaseHelper().getTextAndWriteLog(guaranteed);
    }

    /**
     * set 'guaranteed' contract percentage
     */
    public String setGuaranteedPercentage(String guaranteedPercentage){
        String guaranteed = guaranteedPercentage == null ? String.valueOf(randomNumberFromRangeInt(10,80)) :
                guaranteedPercentage;
        sendKey(GUARANTEED_PERCENTAGE_INPUT.shouldBe(visible), guaranteed);
        return helpersInit.getBaseHelper().getTextAndWriteLog(guaranteed);
    }

    /**
     * set 'guaranteed' contract type
     */
    public String setGuaranteedType(String guaranteedType){
        return helpersInit.getBaseHelper().selectCustomValueReturnText(GUARANTEED_TYPE_DROPDOWN.shouldBe(visible),guaranteedType);
    }

    /**
     * get 'guaranteed' contract price
     */
    public String getGuaranteedPrice(){
        return helpersInit.getBaseHelper().getTextAndWriteLog(GUARANTEED_PRICE_INPUT.val());
    }

    /**
     * get 'guaranteed' contract percentage
     */
    public String getGuaranteedPercentage(){
        return GUARANTEED_PERCENTAGE_INPUT.val();
    }

    /**
     * get 'guaranteed' contract type
     */
    public String getGuaranteedType(){
       return GUARANTEED_TYPE_DROPDOWN.getSelectedValue();
    }

    /**
     * get selected contract type from Contracts type filter
     */
    public String getSelectedTextFromContractsFilter(){
        return CONTRACT_TYPE_FILTER.getSelectedText();
    }

    /**
     * save contract price
     */
    public void saveContract() {
        sleep(1000);
        CONTRACT_SAVE.click(ClickOptions.usingJavaScript());
        waitForAjax();
        checkErrors();
    }

    /**
     * get contract value for different types except Guaranteed
     */
    public String getContractValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CONTRACTS_LABEL.shouldBe(visible).text().split("\n")[1].replaceAll("[^\\d.]+", ""));
    }

    /**
     * get Guaranteed contract price from label
     */
    public String getGuaranteedContractPrice() {
        return GUARANTEEDS_LABEL.shouldBe(visible).text().split(" ")[2].replaceAll("[^\\d.]+", "");
    }

    /**
     * get Guaranteed contract type from label
     */
    public String getGuaranteedContractType() {
        return GUARANTEEDS_LABEL.shouldBe(visible).text().split(" ")[1];
    }

    /**
     * get contract type from label
     */
    public String getContractType(){
        return CONTRACTS_LABEL.shouldBe(visible).text().split("\n")[0];
    }

    /**
     * get Guaranteed contract price label
     */
    public String getGuaranteedPriceLabel(){
        return $(GUARANTEED_PRICE_LABEL + "[style*='inline']").shouldBe(visible).text().split(",")[0];
    }

    public boolean checkGuaranteedPriceLabel(Map<String, String> map){
        return map.get(getGuaranteedType()).equals(getGuaranteedPriceLabel());
    }

    /**
     * select random contract type from "Contract type" filter
     */
    public String selectRandomContractTypeFromContractTypeFilter(){
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(CONTRACT_TYPE_FILTER));
    }

    public void clickFilterButton(){
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public boolean isContractTypeFromWidgetEqualsToFilteredType(String filteredType) {
        if (filteredType.contains("floorType")) {
            return helpersInit.getBaseHelper().checkDatasetEquals(getSelectedTextFromContractsFilter(), getGuaranteedContractType());
        } else
            return helpersInit.getBaseHelper().checkDatasetEquals(getSelectedTextFromContractsFilter(), getContractType());
    }

    /**
     *  get number of displayed widgets by composite id's
     */
    public int getNumberOfDisplayedWidgets(){
        sleep(500);
        waitForAjax();
        return COMPOSITE_IDS.size();
    }

    public void fillCompositeIdFilter(int id){ COMPOSITE_WIDGET_ID_INPUT.sendKeys(String.valueOf(id)); }

    public void fillAutoUnfoldSubWidgetsFilter(boolean state){
        markUnMarkCheckbox(state, AUTO_UNFOLD_SUB_WIDGETS_CHECKBOX);
    }

    ///////////////////////// CONTRACT TYPE -> FINISH ///////////////////////////////

    /**
     * get link from product sub widget
     */
    public String getLinkEditProductSubWidget() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LINK_EDIT_PRODUCT_SUB_WIDGET.attr("href"));
    }

    public String getLinkEditNewsSubWidget() {
        return LINK_EDIT_NEWS_SUB_WIDGET.attr("href");
    }

    /**
     * check price of cloned widget
     */
    public boolean checkPriceOfClone(Integer cloneId) {
        String priceOfContract = $x("//*[@onclick='showDepositDialog(" + cloneId + ")']").getText().split("\n")[1];
        return helpersInit.getBaseHelper().checkDatasetEquals(priceOfContract, "0.000 €");
    }

    /**
     * check displayed sanction text of icon
     */
    public boolean checkDisplayedSanctionSettings(String text) {
        return helpersInit.getBaseHelper().checkDatasetEquals(text, SANCTIONS_DIALOG_ICON.getText());
    }

    /**
     * check displayed sanction icon
     */
    public boolean checkVisibilityDisplayedSanctionIcon() {
        return SANCTIONS_DIALOG_ICON.exists();
    }

    public void clickCategoryFilterIcon() {
        FILTER_BUTTON.scrollTo();
        CATEGORY_FILTER_ICON.shouldBe(visible).click();
        $$(CATEGORY_FILTER_POPUP_DATA).get(0).shouldBe(visible);
    }

    public void clickBlockUnblockWidget(int widgetId) {
        $(String.format(BLOCK_UNBLOCK_WIDGET_ICON, widgetId)).scrollTo().click();
        $(String.format(BLOCK_UNBLOCK_LOAD_ICON, widgetId)).shouldBe(exist);
        $(String.format(BLOCK_UNBLOCK_LOAD_ICON, widgetId)).shouldBe(hidden, ofSeconds(20));
        checkErrors();
    }

    public void clickBlockUnblockWidgetWithoutWaiting(int widgetId) {
        $(String.format(BLOCK_UNBLOCK_WIDGET_ICON, widgetId)).scrollTo().click();
        sleep(1000);
        checkErrors();
    }

    public String getBlockUnblockWidgetColor(int widgetId) {
        String color = $(String.format(BLOCK_UNBLOCK_WIDGET_ICON, widgetId)).shouldBe(visible, ofSeconds(12)).attr("src");
        assert color != null;
        color = color.substring(color.lastIndexOf("/") + 1, color.lastIndexOf("."));
        return helpersInit.getBaseHelper().getTextAndWriteLog(color);
    }

    public String getBlockUnblockWidgetAttr(String attrName, int widgetId) {
        String attr = $(String.format(BLOCK_UNBLOCK_WIDGET_ICON, widgetId)).shouldBe(visible, ofSeconds(12)).attr(attrName);
        assert attr != null;
        return helpersInit.getBaseHelper().getTextAndWriteLog(attr);
    }

    public void clickAllowDisallowDeleteIcon() {
        ALLOW_DISALLOW_DELETE_ICON.shouldBe(visible).click();
        ALLOW_DISALLOW_DELETE_LOADING_ICON.shouldBe(visible);
        ALLOW_DISALLOW_DELETE_LOADING_ICON.shouldBe(hidden);
        checkErrors();
    }

    public String getAllowDisallowDeleteIconLink() {
        String val = ALLOW_DISALLOW_DELETE_ICON.shouldBe(visible).scrollTo().attr("src");
        assert val != null;
        val = val.substring(val.lastIndexOf("/") + 1, val.lastIndexOf("."));
        return helpersInit.getBaseHelper().getTextAndWriteLog(val);
    }

    public boolean checkAllowDisallowDeleteIconLink() {
        String newVal = getAllowDisallowDeleteIconLink().equals("key_minus") ?
                "key_plus" :
                "key_minus";
        clickAllowDisallowDeleteIcon();
        return helpersInit.getBaseHelper().checkDatasetEquals(newVal, getAllowDisallowDeleteIconLink());
    }

    /**
     * Check revenue share for widget
     */
    public boolean checkWidgetRevenueShare(int widgetID, String revenueShare) {
        helpersInit.getBaseHelper().waitDisplayedElement($x("//span[@onclick = 'showDepositDialog(" + widgetID + ")']"));
        String revenueValue = $x("//span[@onclick = 'showDepositDialog(" + widgetID + ")']").getText().substring(18, 20);
        log.info("Revenue share для виджета: " + revenueValue);
        return helpersInit.getBaseHelper().checkDatasetEquals(revenueShare, revenueValue);
    }

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return BLOCK_ADS_ICON.isDisplayed();
    }

    public void switchDfpIcon(DfpValues dfpVal, int widgetId) {
        SelenideElement dfp_icon = $(String.format(GOOGLE_ADD_MANAGER_ICON, widgetId));
        String dfpStatus = dfp_icon.attr("src");
        dfpStatus = Objects.requireNonNull(dfpStatus).substring(dfpStatus.lastIndexOf("_") + 1, dfpStatus.lastIndexOf("."));

        if (!dfpVal.getDfpType().equals(dfpStatus)) {
            dfp_icon.click();
            helpersInit.getMessageHelper().isSuccessMessagesCab();
        }
    }

    public boolean checkDfpIcon(DfpValues dfpVal, int widgetId) {
        String dfpStatus = $(String.format(GOOGLE_ADD_MANAGER_ICON, widgetId)).attr("src");
        dfpStatus = Objects.requireNonNull(dfpStatus).substring(dfpStatus.lastIndexOf("_") + 1, dfpStatus.lastIndexOf("."));

        return helpersInit.getBaseHelper().checkDatasetEquals(dfpVal.getDfpType(), dfpStatus);
    }

    public boolean isExistDfpIcon(int widgetId) {
        return $(String.format(GOOGLE_ADD_MANAGER_ICON, widgetId)).exists();
    }

    public boolean checkDomainForAdvertiser(String domainForAdvertiser) {
        return helpersInit.getBaseHelper().checkDatasetEquals(DOMAIN_FOR_ADVERTISERS_FIELD.text(), domainForAdvertiser);
    }

    public boolean isDisplayedDomainForAdvertiserIcon(boolean lowVolumeDomain) {
        return helpersInit.getBaseHelper().checkDataset(lowVolumeDomain, DOMAIN_FOR_ADVERTISERS_ICON.isDisplayed());
    }

    /**
     * Get ID of widget's video part
     */
    public String getVideoPartId(int compositeId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                $x(String.format(VIDEO_ID_FIELD, compositeId)).text().substring(10));
    }

    ///////////////// MASS actions ////////////////////////////

    /**
     * choose mass actions
     */
    public void chooseMassActions(MassAction action) {
        log.info("chooseMassActions");
        MASS_ACTIONS_SELECT.selectOptionByValue(action.getTypeValue());
        waitForAjax();
    }

    public boolean isDisabledMassAction(String text) {
        ElementsCollection list = MASS_ACTIONS_SELECT.$$("option");
        SelenideElement el = list.filter(Condition.exactText(text)).first();
        return el.is(disabled);
    }

    public List<MassAction> getAllEnabledMassActionsOptions() {
        return Arrays.stream(MassAction.values())
                .filter(opt -> MASS_ACTIONS_SELECT.$("option[value='" + opt.getTypeValue() + "']").isDisplayed())
                .collect(Collectors.toList());
    }

    public void fillSpecifyPopupWidgets(String val) {
        SPECIFY_POPUP_WIDGETS_IDS_INPUT.setValue(val);
        sleep(1000);
    }

    public void turnOffPopupApplyToAllChildCheckbox(){
       if(SPECIFY_POPUP_APPLY_TO_ALL_CHILD_CHECKBOX.shouldBe(visible).isSelected()){
           SPECIFY_POPUP_APPLY_TO_ALL_CHILD_CHECKBOX.hover().click();
       }
    }

    public boolean checkPopupApplyToAllChildCheckboxIsSelected(boolean state){
        return checkIsCheckboxSelected(state, SPECIFY_POPUP_APPLY_TO_ALL_CHILD_CHECKBOX);
    }

    public void saveSpecifyPopupWidgets() {
        SPECIFY_POPUP_SAVE_BUTTON.shouldBe(visible).click();
        sleep(1000);
    }

    public void selectAllWidgets() { SELECT_ALL_CHECKBOXES_LINK.shouldBe(visible).scrollTo().click(); }

    public void selectCustomWidgets() {
        SELECT_SPECIFY_WIDGETS_LINK.shouldBe(visible).scrollTo().click(usingJavaScript());
        SPECIFY_POPUP_WIDGETS_IDS_INPUT.shouldBe(visible);
    }

    public void massActionPopupSubmit() {
        MASS_ACTION_POPUP_SUBMIT.click();
        waitForAjax();
        checkErrors();
    }

    public boolean isDisplayedMassActions(){ return SELECT_ALL_CHECKBOXES_LINK.exists(); }

    public void turnOnCustomAdTypes(Map<String, Boolean> map) {
        map.entrySet().stream()
                .filter(m -> ($("[name='adTypes[]'][value='" + m.getKey() + "']").isSelected() != m.getValue()))
                .forEach(m -> $("[name='adTypes[]'][value='" + m.getKey() + "']").click());
    }

    public void turnOnCustomLandingTypes(Map<String, Boolean> map) {
        map.entrySet().stream()
                .filter(m -> ($("[name='landingTypes[]'][value='" + m.getKey() + "']").isSelected() != m.getValue()))
                .forEach(m -> $("[name='landingTypes[]'][value='" + m.getKey() + "']").click());
    }

    public void setAdTypes(Table<String, Boolean, String> adTypesData) {
        boolean adTypeState;
        String adTypeValue;

        ElementsCollection adTypesList = $$("[name='adTypes[]']");
        ElementsCollection adTypesLimit = $$("input.type");

        // switch custom checkboxes
        for (SelenideElement element : adTypesList) {

            adTypeState = Boolean.parseBoolean(adTypesData.row(Objects.requireNonNull(element.val())).keySet().toArray()[0].toString());
            adTypeValue = adTypesData.row(Objects.requireNonNull(element.val())).values().toArray()[0].toString();

            // switch on/off adType checkbox (adTypeState)
            if ((adTypeState & !element.shouldBe(visible).isSelected()) || (!adTypeState & element.shouldBe(visible).isSelected())) {
                element.click();
            }

            // fill adType limit (adTypeValue)
            if (!adTypeValue.equals("-") && element.isSelected()) {
                clearAndSetValue(adTypesLimit.find(Condition.id(Objects.requireNonNull(element.val()))), adTypeValue);
            }
        }
    }

    public String getPublisherCategoriesPopupText() {
        return MASS_WIDGETS_CATEGORIES_POPUP.text();
    }

    public void publisherCategoriesClickNext() {
        MASS_WIDGETS_CATEGORIES_NEXT.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public String getMassChangeCategoryWidgetOkActionLabel(){ return MASS_CHANGE_WIDGETS_CATEGORIES_OK_ACTION_LABEL.text(); }

    public boolean isDisplayedWidgetOkActionLabel(){ return MASS_CHANGE_WIDGETS_CATEGORIES_OK_ACTION_LABEL.isDisplayed(); }

    public String getMassChangeCategoryWidgetWarnActionLabel(){ return MASS_CHANGE_WIDGETS_CATEGORIES_WARN_ACTION_LABEL.text(); }

    public String getMassChangeCategoryWidgetWarnDetailedActionLabel(){
        return String.join(";", $$(MASS_CHANGE_WIDGETS_CATEGORIES_WARN_ACTION_DETAILED_LABEL).texts()).replaceAll(Character.toString(8226), "").trim();
    }

    public String getMassChangeCategoryWidgetOkDetailedActionLabel(){
        return String.join(";", $$(MASS_CHANGE_WIDGETS_CATEGORIES_OK_ACTION_DETAILED_LABEL).texts()).replaceAll(Character.toString(8226), "").trim();
    }

    public void choosePublisherCategoriesTypeInPopUp(ChangeWidgetsCategories type){
        MASS_CHANGE_WIDGETS_CATEGORIES_TYPES_SELECT.shouldBe(visible).selectOptionByValue(type.getTypeValue());
    }

    public void chooseOptionStopWordsInPopUp(ChangeStopWords type){
        MASS_CHANGE_STOP_WORDS_SELECT.shouldBe(visible).selectOptionByValue(type.getTypeValue());
    }

    public void chooseExchangeCategoriesTypeInPopUp(ChangeExchangeWidgetsCategories type){
        MASS_CHANGE_EXCHANGE_CATEGORIES_TYPES_SELECT.shouldBe(visible).selectOptionByValue(type.getTypeValue());
    }

    public void setStopWords(ArrayList<String> listOfStopWords) {
        STOP_WORDS_INPUT.shouldBe(visible);
        for (String s : listOfStopWords) {
            STOP_WORDS_INPUT.sendKeys(s + Keys.ENTER);
        }
        int counter = 0;
        STOP_WORDS_INPUT.sendKeys(Keys.TAB);
        while (STOP_WORDS_INPUT.getCssValue("color").contains("rgba(0,0,0,1)") && counter < 3) {
            STOP_WORDS_INPUT.sendKeys(Keys.TAB);
            counter++;
        }
    }

    public void clickAdTypeOrLandingTypeForExchangeCategory(String categoryName, boolean isAdType){
        // get category id
        String category = $x(".//li[*[*[text()=\"" + categoryName + "\"]]]").attr("id");
        category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);
        //click category icon (лейка) and checkbox
        $("#ft_news-" + category + " .fancytree-icon").click();
        $("#ft_news-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

        $$("#ft_news-" + category + " ." + (isAdType ? "ad":"landing") + "_types [data-type]").get(1).click(usingJavaScript());
    }

    public boolean checkSearchCategoryFilterInMass() {
        helpersInit.getMultiFilterHelper().expandMultiFilter("newsCategoriesBox");
        ElementsCollection categoryList = $$x(MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_TITLES);
        String category = helpersInit.getBaseHelper().getTextAndWriteLog(categoryList.get(randomNumbersInt(categoryList.size())).text());

        MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_SEARCH_INPUT.scrollTo().sendKeys(category);
        return $$x(MASS_CHANGE_EXCHANGE_CATEGORY_FILTER_TITLES_AFTER_SEARCH)
                .texts().stream().allMatch(i -> i.toLowerCase().contains(category.toLowerCase()) || i.equalsIgnoreCase(category));
    }

    public void chooseExchangeCampaignFilterTypeInPopUp(ChangeExchangeCampaignFilter type){
        MASS_EXCHANGE_CAMPAIGN_FILTER_SELECT.shouldBe(visible).selectOptionByValue(type.getTypeValue());
    }

    public void chooseWagesCampaignFilterTypeInPopUp(ChangeWagesCampaignFilter type){
        MASS_WAGES_CAMPAIGN_FILTER_SELECT.shouldBe(visible).selectOptionByValue(type.getTypeValue());
    }

    public void clickUploadPartnersListButton(){
        UPLOAD_PARTNERS_LIST_BUTTON.shouldBe(visible).click();
        waitForAjaxLoader();
        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).shouldBe(CollectionCondition.sizeGreaterThan(0));
    }

    public void searchExchangeCampaignInInput(String campaign){
        MASS_EXCHANGE_CAMPAIGN_FILTER_SEARCH.shouldBe(visible).sendKeys(campaign + Keys.ENTER);
        waitForAjaxLoader();
    }

    public void cleanSearchExchangeCampaignInput(){
        MASS_EXCHANGE_CAMPAIGN_FILTER_CLEAN.shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public boolean checkSearchResult(boolean isCampaignId, String campaign){
        return $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).size() == 1 &&
                (isCampaignId
                        ?
                        Objects.requireNonNull($$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).get(0).attr("id")).equals(campaign)
                        :
                        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST_LABEL).get(0).text().trim().contains(campaign));
    }

    public int getCountExchangeCampaignInput(){
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).size());
    }

    public boolean ifLeftOnlyActivateCheckboxes(int count){
        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).shouldBe(CollectionCondition.allMatch("ifLeftOnlyActivateCheckboxes", WebElement::isSelected));
        return helpersInit.getBaseHelper().checkDataset($$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).size(), count);
    }

    public boolean ifLeftOnlyInActivateCheckboxes(int count){
        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).shouldBe(CollectionCondition.noneMatch("ifLeftOnlyInActivateCheckboxes", WebElement::isSelected));
        return helpersInit.getBaseHelper().checkDataset($$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).size(), count);
    }

    public List<String> chooseRandomExchangeCampaigns(int count){
        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).shouldBe(CollectionCondition.sizeGreaterThan(0));
        ElementsCollection campaigns = $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST);
        ArrayList<Integer> campaign_numbers = helpersInit.getBaseHelper().getRandomCountValue(campaigns.size(), count);

        // turn on campaigns in filter
        campaign_numbers.forEach(i -> campaigns.get(i).click());

        // write chosen campaigns to list
        return campaign_numbers.stream().map(i -> Objects.requireNonNull(campaigns.get(i).attr("id")).substring(1)).collect(Collectors.toList());
    }

    public List<String> chooseRandomWagesCampaigns(int count){
        $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST).shouldBe(CollectionCondition.sizeGreaterThan(0));
        ElementsCollection campaigns = $$(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST);
        ArrayList<Integer> campaign_numbers = helpersInit.getBaseHelper().getRandomCountValue(campaigns.size(), count);

        // turn on campaigns in filter
        campaign_numbers.forEach(i -> campaigns.get(i).click());

        // write chosen campaigns to list
        return campaign_numbers.stream().map(i -> Objects.requireNonNull(campaigns.get(i).attr("id")).substring(1)).collect(Collectors.toList());
    }

    public void clickCurrentExchangeCampaigns(int campaignId){
        $(MASS_EXCHANGE_CAMPAIGN_FILTER_LIST + "#p" + campaignId).shouldBe(visible).click();
    }

    public void clickActivateCheckboxInExchangeCampaignsPopUp(){
        MASS_EXCHANGE_CAMPAIGN_FILTER_ACTIVE.shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public void clickInactivateCheckboxInExchangeCampaignsPopUp(){
        MASS_EXCHANGE_CAMPAIGN_FILTER_INACTIVE.shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public void clickSelectAllCheckboxInExchangeCampaignsPopUp(){
        MASS_EXCHANGE_CAMPAIGN_FILTER_SELECT_ALL.shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public void clickExchangeCampaignFilterIcon(int tickersId){
        $(String.format(EXCHANGE_CAMPAIGN_FILTER_ICON, tickersId)).shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public void clickWagesCampaignFilterIcon(int gblockesId){
        $(String.format(WAGES_CAMPAIGN_FILTER_ICON, gblockesId)).shouldBe(visible).click();
        waitForAjaxLoader();
    }

    public List getAllExchangeCampaignsFilterFromPopUp(){
        return $$(EXCHANGE_CAMPAIGN_FILTER_LIST_IN_POPUP).texts();
    }

    public String getMassActionErrorMessage(){ return MASS_WIDGETS_MESSAGE.shouldBe(visible).text(); }

    public boolean bulkActionInProcessIconIsDisplayed() {
        return BULK_ACTION_IN_PROCESS_ICON.isDisplayed();
    }

    public String getBulkActionInProcess() {
        return BULK_ACTION_IN_PROCESS_ICON.innerText();
    }

    public boolean checkDisplayedBlockUnblockButton(int widgetId) {
        return $(String.format(BLOCK_UNBLOCK_WIDGET_ICON, widgetId)).isDisplayed();
    }

    /**
     * Open Manager's alert popup
     */
    public void openManagersAlertPopup(int widgetId) {
        $(String.format(MANAGERS_ALERT_ICON, widgetId)).shouldBe(visible).click();
        checkErrors();
        MANAGERS_ALERTS_POPUP.shouldBe(visible);
        waitForAjax();
        waitForAjaxLoader();
    }

    public void selectComparisonPeriod(){
        managersAlertComparisonPeriod = helpersInit.getBaseHelper().selectRandomValue(MANAGERS_ALERTS_PERIOD_SELECT);
    }

    public boolean checkComparisonPeriod(){
         return helpersInit.getBaseHelper().checkDatasetEquals(MANAGERS_ALERTS_PERIOD_SELECT.getSelectedValue(), managersAlertComparisonPeriod);
    }

    public void selectTimePeriodFrom(){
        managersAlertTimePeriodFrom = helpersInit.getBaseHelper().selectRandomValue(MANAGERS_ALERTS_HOUR_FROM_INPUT);
    }

    public boolean checkTimePeriodFrom(){
        return helpersInit.getBaseHelper().checkDatasetEquals(MANAGERS_ALERTS_HOUR_FROM_INPUT.val(), managersAlertTimePeriodFrom);
    }

    public void selectTimePeriodTo(){
        managersAlertTimePeriodTo = helpersInit.getBaseHelper().selectRandomValue(MANAGERS_ALERTS_HOUR_TO_INPUT);
    }

    public boolean checkTimePeriodTo(){
        return helpersInit.getBaseHelper().checkDatasetEquals(MANAGERS_ALERTS_HOUR_TO_INPUT.val(), managersAlertTimePeriodTo);
    }

    public void selectRandomTypeOfAlertAndDelta(int countType, boolean withoutQuantitativeValue){
        String type, delta, deltaNumber;
        int iterator;

        // get different(NOT EQUALS) index options
        ArrayList<Integer> list = helpersInit.getBaseHelper().getRandomCountValue($$(MANAGERS_ALERTS_TYPE_SELECT).get(0).$$("option:not([selected]):not([value=''])").size(), countType);

        for (int i = 0; i < countType; i++){

            // set option by index and get his value
            iterator = list.get(i) == 0 ? 1 : list.get(i);
            $$(MANAGERS_ALERTS_TYPE_SELECT).get(i).selectOption(iterator);
            type = $$(MANAGERS_ALERTS_TYPE_SELECT).get(i).getSelectedValue();

            // choose and set random delta
            delta = randomNumberFromRange(1,1000);
            clearAndSetValue($$(MANAGERS_ALERTS_DELTA_INPUT).get(i), delta);

            // choose and set random delta number
            if(withoutQuantitativeValue) {
                deltaNumber = randomNumberFromRange(1, 1000000);
                clearAndSetValue($$(MANAGERS_ALERTS_DELTA_NUMBER).get(i), deltaNumber);
            }
            else {
                deltaNumber = "0";
            }

            managersAlertTypeAndDelta.put(type, Map.of(delta, deltaNumber));

            // add new type of alert
            if(countType > 1 && i < countType-1){
                MANAGERS_ALERTS_ADD_ADDITIONAL_ALERT_BUTTON.click();
            }
        }
    }

    public boolean checkTypeOfAlertAndDelta(){
        $$(MANAGERS_ALERTS_TYPE_SELECT).shouldHave(CollectionCondition.size(managersAlertTypeAndDelta.size()), ofSeconds(8));
        ElementsCollection typeList = $$(MANAGERS_ALERTS_TYPE_SELECT);
        ElementsCollection deltaList = $$(MANAGERS_ALERTS_DELTA_INPUT);
        ElementsCollection deltaNumberList = $$(MANAGERS_ALERTS_DELTA_NUMBER);
        log.info("typeList.size(): " + typeList.size());
        log.info("deltaList.size(): " + deltaList.size());
        log.info("deltaNumberList.size(): " + deltaNumberList.size());
        log.info("managersAlertTypeAndDelta.size(): " + managersAlertTypeAndDelta.size());
        for(int c = 0; c < typeList.size(); c++){
            if(!(managersAlertTypeAndDelta.get(typeList.get(c).getSelectedValue())
                    .get(deltaList.get(c).val()) + ".00")
                    .equals(deltaNumberList.get(c).val())){
                return false;
            }
        }
        return typeList.size() == managersAlertTypeAndDelta.size();
    }

    public void deleteCustomAlert(String value){
        $(MANAGERS_ALERTS_TYPE_SELECT + " option[value='" + value + "']:checked").parent().parent().parent().$("button").click();
        checkErrors();
    }

    public void addCustomAlert(String alertType, String delta, String... quantitativeValueDelta){
        int alertTypeSize = $$(MANAGERS_ALERTS_TYPE_SELECT).size();
        if(alertTypeSize == 1 && Objects.requireNonNull($$(MANAGERS_ALERTS_DELTA_INPUT).get(0).val()).equals("")){
            alertTypeSize = 0;
        }
        else {
            MANAGERS_ALERTS_ADD_ADDITIONAL_ALERT_BUTTON.shouldBe(visible).click();
        }

        $$(MANAGERS_ALERTS_TYPE_SELECT).get(alertTypeSize).shouldBe(visible).selectOptionByValue(alertType);
        clearAndSetValue($$(MANAGERS_ALERTS_DELTA_INPUT).get(alertTypeSize).shouldBe(visible), delta);
        if(quantitativeValueDelta.length > 0) clearAndSetValue($$(MANAGERS_ALERTS_DELTA_NUMBER).get(alertTypeSize).shouldBe(visible), quantitativeValueDelta[0]);
    }

    public ArrayList<String> getAllDataWidgetsSlackNotificationByWidgetId(int widgetId) {
        String key;
        ArrayList<String> list = new ArrayList<>();
        for(int i = 0; i < managersAlertTypeAndDelta.size(); i++){
            key = String.valueOf(managersAlertTypeAndDelta.keySet().toArray()[i]);
            list.add(widgetId + ", " +
                    key + ", " +
                    managersAlertTypeAndDelta.get(key).keySet().toArray()[0] + ", " +
                    managersAlertTypeAndDelta.get(key).values().toArray()[0] + ".00, " +
                    managersAlertTimePeriodFrom + ", " +
                    managersAlertTimePeriodTo + ", " +
                    managersAlertComparisonPeriod + ", ");
        }
        return list;
    }

    public void saveManagerAlert(){
        MANAGERS_ALERTS_SAVE_BUTTON.click();
        checkErrors();
    }

    public void turnOffManagerAlert(){
        MANAGERS_ALERTS_TURN_OFF_BUTTON.click();
        checkErrors();
    }

    public boolean isExistErrorOnManagerAlertsSelect(){
        return Objects.requireNonNull($$(MANAGERS_ALERTS_TYPE_SELECT).get(0).parent().attr("class")).contains("error");
    }

    public boolean isExistErrorOnManagerAlertsDelta(){
        return Objects.requireNonNull($$(MANAGERS_ALERTS_DELTA_INPUT).get(0).parent().attr("class")).contains("error");
    }

    public boolean isExistErrorOnManagerAlertsQuantitativeValueDelta(){
        return Objects.requireNonNull($$(MANAGERS_ALERTS_DELTA_NUMBER).get(0).parent().attr("class")).contains("error");
    }

    /**
     * Check Managers Alert state
     */
    public boolean checkManagersAlertIconState(int widgetId, String alertState) {
        return Objects.requireNonNull($(String.format(MANAGERS_ALERT_ICON, widgetId)).shouldBe(visible).attr("src")).contains(alertState);
    }

    /**
     * Check presence of Managers Alert icon
     */
    public boolean checkManagersAlertIcon(int widgetId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($(String.format(MANAGERS_ALERT_ICON, widgetId)).isDisplayed());
    }

    public void massFillContractPrice(String price){
        MASS_CONTRACT_PRICE_INPUT.shouldBe(visible).val(price);
    }

    public void massContractPriceSubmit(){
        MASS_CONTRACT_PRICE_SUBMIT.click();
        checkErrors();
    }

    public String getContractTypeLabel(int widgetId){
        return $(String.format(CONTRACT_LABEL, widgetId)).shouldBe(visible).text().split("\n")[0];
    }

    public String getContractPriceLabel(int widgetId){
        return $(String.format(CONTRACT_LABEL, widgetId)).text().split("\n")[1];
    }

    public boolean isContractTypeLabelExist(int widgetId){
        return $(String.format(CONTRACT_LABEL, widgetId)).exists();
    }

    public String getGuaranteedContract(int widgetId){
        return $(String.format(GUARANTEED_LABEL, widgetId)).shouldBe(visible).text().trim();
    }

    public void clickChildTreeWidgetsIcon(boolean status){
        boolean classIcon = Objects.requireNonNull(SHOW_CHILD_WIDGETS_ICON.attr("class")).contains("active");
        if((status & !classIcon) || (!status & classIcon)) {
            SHOW_CHILD_WIDGETS_ICON.shouldBe(visible).click();
            checkErrors();
            waitForAjax();
            if (!classIcon) {
                SHOW_CHILD_WIDGETS_ICON.shouldHave(attributeMatching("class", ".+active"));
            } else {
                SHOW_CHILD_WIDGETS_ICON.shouldNotHave(attributeMatching("class", ".+active"));
            }
        }
    }

    public void waitLoadAllChildWidgets(){
        SHOW_CHILD_WIDGETS_ICON.shouldHave(attributeMatching("class", ".+active"));
    }

    public void switchApplyToAllChildCheckbox(boolean status){
        markUnMarkCheckbox(status, APPLY_TO_ALL_CHILD_CHECKBOX);
    }

    public String getAmountOfPlacementForCustomWidget(String placesType){
        String[] seatsTypes = NUMBER_OF_SEATS_ALLOCATED_LABEL.text().split("\n");
        String val = Arrays.stream(seatsTypes).filter(i -> i.contains(placesType)).findFirst().get();
        return val.substring(val.lastIndexOf(":")+1).trim();
    }

    public void clickIconLimitByCategoryFilterNews(){
        LIMIT_BY_CATEGORY_FILTER_NEWS_ICON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public String getAttributeIconLimitByCategoryFilterNews(String attributeName){
        return LIMIT_BY_CATEGORY_FILTER_NEWS_ICON.shouldBe(visible).attr(attributeName);
    }

    public List<String> getLimitByCategoryFilterNews(){
        return Arrays.stream(CATEGORIES_FILTER_NEWS_POPUP.shouldBe(visible).text().split("\n"))
                .filter(i -> i.contains("[")).collect(Collectors.toList());
    }
}
