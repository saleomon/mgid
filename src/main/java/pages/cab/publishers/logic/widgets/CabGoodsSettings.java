package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;

import java.util.*;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.widgets.CompositeWidgetLocators.SAVE_BUTTON;
import static pages.cab.publishers.locators.widgets.GoodsWidgetLocators.*;
import static pages.cab.publishers.locators.widgets.WidgetsListLocators.CATEGORY_FILTER_POPUP_DATA;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabGoodsSettings {

    private final HelpersInit helpersInit;
    private final Logger log;

    //tier
    private String campaignTierFilterValue = "0";

    //show teasers
    private String showTeasersValue;

    // minPrices CPM
    private final Map<String, String> countryCpm = new HashMap<>();

    // minPrices CPC
    private ArrayList<Integer> minPricesCpc = new ArrayList<>();
    private final Map<String, String> regionIdMinPrices = new HashMap<>();

    // MGID Inventory Based Widget
    private String mgidInventoryBasedWidget;

    private String optionAdsFilterSelect, optionsAdsFilterContainer;

    public CabGoodsSettings(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public String getCampaignTierFilterValue() {
        return campaignTierFilterValue;
    }

    public String getShowTeasersValue() { return showTeasersValue; }

    public Map<String, String> getCountryCpm() { return countryCpm; }

    public Map<String, String> getRegionIdMinPrices() {
        return regionIdMinPrices;
    }

    public String getMgidInventoryBasedWidget() {
        return mgidInventoryBasedWidget;
    }

    public String getOptionAdsFilterSelect() { return optionAdsFilterSelect; }

    public CabGoodsSettings setOptionAdsFilterSelect(String optionsAdsFilterContainer) {
        this.optionAdsFilterSelect = optionsAdsFilterContainer;
        return this;
    }

    public CabGoodsSettings setOptionsAdsFilterContainer(String optionsAdsFilterContainer) {
        this.optionsAdsFilterContainer = optionsAdsFilterContainer;
        return this;
    }

    /**
     * Check radio button 'Campaign tier filter' marked value
     */
    public boolean checkCampaignTierFilterValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_TIER_FILTER_VALUES.findBy(value(campaignTierFilterValue)).shouldBe(visible).isSelected());
    }

    public boolean checkAnimationFilterValue(String animationValue) {
        return ANIMATION_SELECT.getSelectedValue().equals(animationValue);
    }

    public boolean checkShowTeasersFilterValue() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SHOW_TEASERS_FILTER_VALUES.findBy(value(showTeasersValue)).shouldBe(visible).isSelected());
    }

    /**
     * "Description required" checkbox is disabled
     */
    public boolean descriptionRequiredIsDisabled() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DESCRIPTION_REQUIRED_CHECKBOX.is(attribute("disabled")));
    }

    public void switchDescriptionRequired(boolean state) {
        markUnMarkCheckbox(state, DESCRIPTION_REQUIRED_CHECKBOX);
    }

    public boolean descriptionRequiredIsSelected(boolean state) {
        return checkIsCheckboxSelected(state, DESCRIPTION_REQUIRED_CHECKBOX.shouldBe(visible));
    }

    public boolean showDescriptionIsSelected(boolean state) {
        return checkIsCheckboxSelected(state, SHOW_DESCRIPTION_CHECKBOX.shouldBe(visible));
    }

    public boolean checkIncludeFakeTeasers(boolean state) {
        return checkIsCheckboxSelected(state, INCLUDE_FAKE_TEASERS_CHECKBOX);
    }

    public boolean checkIncludeRtbTeasers(boolean state) {
        return checkIsCheckboxSelected(state, INCLUDE_RTB_TEASERS_CHECKBOX);
    }

    public boolean checkRotateOnlyTeasersOfCurrentSubnet(boolean state) {
        return checkIsCheckboxSelected(state, ROTATE_ONLY_TEASERS_OF_CURRENT_SUBNET_CHECKBOX);
    }

    public boolean checkRotateInAdskeeperSubnet(boolean state) {
        return checkIsCheckboxSelected(state, ROTATE_IN_ADSKEEPER_SUBNET_CHECKBOX);
    }

    public boolean checkDoNotIncludeThisWidgetForGoodhitsCtrCalculation(boolean state) {
        return checkIsCheckboxSelected(state, NO_CALCULATION_GHITS_CTR_CHECKBOX);
    }

    public boolean checkAntiDuplication(boolean state) {
        return checkIsCheckboxSelected(state, ANTI_DUPLICATION_CHECKBOX);
    }

    public boolean checkExcludeAgencyFeeFromPublisherWages(boolean state) {
        return checkIsCheckboxSelected(state, AGENCY_FEE_TO_WAGES_CHECKBOX);
    }

    public void switchShowDescription(boolean state) {
        markUnMarkCheckbox(state, SHOW_DESCRIPTION_CHECKBOX);
    }

    public void switchIncludeFakeTeasers(boolean state) {
        markUnMarkCheckbox(state, INCLUDE_FAKE_TEASERS_CHECKBOX);
    }

    public void switchIncludeRtbTeasers(boolean state) {
        markUnMarkCheckbox(state, INCLUDE_RTB_TEASERS_CHECKBOX);
    }

    public void switchRotateOnlyTeasersOfCurrentSubnet(boolean state) {
        markUnMarkCheckbox(state, ROTATE_ONLY_TEASERS_OF_CURRENT_SUBNET_CHECKBOX);
    }

    public void switchDoNotIncludeThisWidgetForGoodhitsCtrCalculation(boolean state) {
        markUnMarkCheckbox(state, NO_CALCULATION_GHITS_CTR_CHECKBOX);
    }

    public void switchRotateInAdskeeperSubnet(boolean state) {
        markUnMarkCheckbox(state, ROTATE_IN_ADSKEEPER_SUBNET_CHECKBOX);
    }

    public void switchAntiDuplication(boolean state) {
        markUnMarkCheckbox(state, ANTI_DUPLICATION_CHECKBOX);
    }

    public void switchExcludeAgencyFeeFromPublisherWages(boolean state) {
        markUnMarkCheckbox(state, AGENCY_FEE_TO_WAGES_CHECKBOX);
    }

    public String getCampaignTierFilterTooltip() {
        return CAMPAIGN_TIER_FILTER_INFO.attr("title");
    }

    public void setPagesCountInput(String value) {
        sendKey(PAGES_COUNT_INPUT, value);
    }

    public void setDuplicatesCountInput(String value) {
        sendKey(DUPLICATES_COUNT_INPUT, value);
    }

    public String getPagesCountInput() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PAGES_COUNT_INPUT.val());
    }

    public String getDuplicatesCountInput() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DUPLICATES_COUNT_INPUT.val());
    }

    public String getDefaultMinimalCpcDecreasingFactor() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DEFAULT_MIN_CPC_DECREASING_FACTOR_INPUT.val());
    }

    public void setTranzitPagePercentage(String tranzitPagePercent) {
        clearAndSetValue(TRANZIT_PAGE_PERCENTAGE_INPUT, tranzitPagePercent);
    }

    public void setDefaultMinimalCpcDecreasingFactor(String tranzitPagePercent) {
        clearAndSetValue(DEFAULT_MIN_CPC_DECREASING_FACTOR_INPUT, tranzitPagePercent);
    }

    public void setMinimalCpc(String cpc) {
        sendKey(SET_MINIMAL_CPC_INPUT.shouldBe(visible), cpc + Keys.ENTER);
        TRANZIT_PAGE_PERCENTAGE_INPUT.click();
    }

    public void clickMinimalCpcIcon() {
        SET_MINIMAL_CPC_ICON.click();
        MINIMAL_CPC_FORM.shouldBe(visible);
    }

    public void clickMinimalCpmIcon() {
        SET_MINIMAL_CPM_ICON.click();
        MINIMAL_CPM_BUTTON.shouldBe(visible);
    }

    public String getTranzitPagePercentage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TRANZIT_PAGE_PERCENTAGE_INPUT.val());
    }

    public boolean checkMinimalCpcForAllRegions(String cpc) {
        $$(MINIMAL_CPC_REGIONS_INPUT).shouldBe(CollectionCondition.allMatch("checkMinimalCpcForAllRegions", i -> i.getAttribute("value").equals(cpc)));
        return true;
    }

    public void switchMinimalCpcUpset(boolean state) {
        markUnMarkCheckbox(state, MINIMAL_CPC_UPSALE_CHECKBOX);
    }

    public boolean checkMinimalCpcUpset(boolean state) {
        return checkIsCheckboxSelected(state, MINIMAL_CPC_UPSALE_CHECKBOX);
    }

    public void chooseMgidInventoryBasedWidget() {
        mgidInventoryBasedWidget = helpersInit.getBaseHelper().selectRandomValue(MGID_INVENTORY_BASED_WIDGET_SELECT);
    }

    public String getSelectedMgidInventoryBasedWidget() {
        return MGID_INVENTORY_BASED_WIDGET_SELECT.getSelectedValue();
    }

    public void chooseCountryAndSetThemCpm() {
        String value, countryName, country_id;
        SelenideElement element;
        ElementsCollection elementsList;
        int size = 0;


        for (int i = 0; i < 2; i++) {
            log.info("get random cpm");
            value = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.1, 20.0);

            log.info("select random country");
            // open select
            $("#countryPropsCpm-country[value='-- select country --']").shouldBe(visible);
            MINIMAL_CPM_FORM_SELECT_ICON.shouldBe(visible).click();

            //так як колекція після кожного степу зменшується, потрібно чекати поки колекція зменшиться
            log.info("$$(MINIMAL_CPM_COUNTRIES_LI): " + $$(MINIMAL_CPM_COUNTRIES_LI).size());
            if (i == 0) size = $$(MINIMAL_CPM_COUNTRIES_LI).shouldHave(CollectionCondition.sizeGreaterThan(0)).size();
            log.info("success");
            elementsList = $$(MINIMAL_CPM_COUNTRIES_LI).shouldHave(size(size - i));
            int temp = helpersInit.getBaseHelper().getTextAndWriteLog(randomNumbersInt(elementsList.size()));

            // select country
            element = elementsList.get(temp);
            countryName = element.innerText();
            element.hover().click();

            log.info("get country_id");
            country_id = $x(".//span[text()='" + countryName + "']/ancestor::tr").shouldBe(visible).attr("id");
            assert country_id != null;
            country_id = country_id.substring(country_id.indexOf("-") + 1);

            // fill country input -> CPM
            sendKey($("#row-" + country_id + " input[type='text']"), value);

            // fill map country -> cpm
            countryCpm.put(country_id, value);
        }

        // save cpm form
        MINIMAL_CPM_FORM_BUTTON.click();
        checkErrors();

    }

    public boolean checkMinimalCpm() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(countryCpm.entrySet().stream()
                .allMatch(entry ->
                        Objects.requireNonNull($$(MINIMAL_CPM_FORM_LABELS).find(attribute("id", "row-" + entry.getKey())).$(MINIMAL_CPM_FORM_INPUT)
                                .val()).equals(entry.getValue())));
    }

    public void expandMinCpcCountries() {
        $$(MINIMAL_CPC_REGIONS_EXPAND_ICON).asDynamicIterable().forEach(i ->
        {
            i.click();
            i.shouldHave(attribute("data-open", "true"));
        });
    }

    public void clickMinimalCpcSubmit() {
        MINIMAL_CPC_FORM_SUBMIT.click();
        checkErrors();
        MINIMAL_CPC_FORM.shouldBe(hidden);
    }

    public void getRandomRegionGroup() {
        minPricesCpc = helpersInit.getBaseHelper().getRandomCountValue($$(MINIMAL_CPC_REGIONS_INPUT).size(), 2);
    }

    public void setMinimalCpcForCustomGroup() {
        String group_id, value;
        SelenideElement group_element;

        for (int i : minPricesCpc) {
            // get random value
            value = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.1, 100.0);

            // get custom group element
            group_element = $$(MINIMAL_CPC_REGIONS_INPUT).get(i);

            // set value to group element
            sendKey(group_element.parent().$(".text-geo-price-input"), value);

            // get group id
            group_id = group_element.attr("name");
            assert group_id != null;
            group_id = group_id.substring(group_id.indexOf("[") + 1, group_id.indexOf("]"));

            // fill map
            regionIdMinPrices.put(group_id, value);
        }
    }

    public boolean checkMinimalCpcForCustomGroup() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(regionIdMinPrices.entrySet().stream()
                .allMatch(entry ->
                        Objects.requireNonNull($$(MINIMAL_CPC_REGIONS_INPUT).find(attribute("name", "geo-group-price[" + entry.getKey() + "]"))
                                .val()).equals(entry.getValue())));
    }

    /**
     * get 'Category filter' checkbox element
     */
    public SelenideElement getCategoryFilterCheckbox(String categoryName) {
        return $x(".//span[*[text()='" + categoryName + "']]").hover();
    }

    public void setAndSubmitAdsFilter(String teasers) {
        clearAndSetValue(ADS_FILTER_INPUT, teasers);
        ADS_FILTER_SUBMIT.click();
        waitForAjax();
    }

    public void setCategoryToMultiFilter(String category) {
        CATEGORY_FILTER_SEARCH_INPUT.scrollTo().sendKeys(category);
    }

    public boolean categoryFilterBlockIsDisplayed() {
        return CATEGORY_FILTER_SEARCH_INPUT.isDisplayed();
    }

    /**
     * изменение состояния фильтра по тизерам
     */
    public CabGoodsSettings changeStateAdsFilter(String value) {
        ADS_FILTER_SELECT.selectOptionContainingText(value);
        return this;
    }

    /**
     * открытие формы редактирования фильтра по тизерам
     */
    public void openEditAdsFilterPopup() {
        ADS_FILTER_EDIT_ICON.hover().click();
    }

    public String getAdsFilterErrorMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADS_FILTER_MESSAGE.text());
    }

    public boolean isExistAdsFilterErrorMessage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADS_FILTER_MESSAGE.exists());
    }

    /**
     * проверка значений фильтра по тизерам
     */
    public boolean checkDisplayedAdsValue(String... values) {
        return Arrays.stream(values).allMatch(el -> helpersInit.getBaseHelper().checkDatasetContains(ADS_FILTER_LABEL.getText(), el));
    }

    /**
     * проверка значений фильтра по тизерам
     */
    public boolean checkDisplayedAdsValueAtPopup(String... values) {
        openEditAdsFilterPopup();
        return Arrays.stream(values).allMatch(el -> helpersInit.getBaseHelper().checkDatasetContains(ADS_FILTER_INPUT.getText(), el));
    }

    /**
     * проверка знаения селекта
     */
    public boolean checkAdsFilterSelectedValue(String value) {
        return helpersInit.getBaseHelper().checkDatasetEquals(ADS_FILTER_SELECT.getSelectedText(), value);
    }

    /**
     * отображается ли кнопка редактирования фильтра тиеров
     */
    public boolean isDisplayedEditButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADS_FILTER_EDIT_ICON.isDisplayed());
    }

    /**
     * @param type 0 - No; 1 - Only; 2 - Except
     */
    public void choosePublishersFilterType(String type) {
        PUBLISHERS_FILTER_RADIOBUTTONS.scrollTo().hover().selectRadio(type);
    }

    /**
     * @param type 0 - No; 1 - Only; 2 - Except
     */
    public void chooseCategoriesFilterType(String type) {
        CATEGORIES_FILTER_RADIOBUTTONS.hover().selectRadio(type);
    }

    public boolean checkCategoriesFilterTypeValue(String radioButtonValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORIES_FILTER_VALUES.findBy(value(radioButtonValue)).shouldBe(visible).isSelected());
    }

    public String checkPublishersFilterType() {
        $$(By.name(PUBLISHERS_FILTER_STRING_RADIOBUTTONS)).shouldHave(size(3));
        return $$(By.name(PUBLISHERS_FILTER_STRING_RADIOBUTTONS))
                .filter(checked).first().val();
    }

    public void clickUploadPartnersList() {
        UPLOAD_PARTNERS_LIST_BUTTON.shouldBe(visible).hover().click();
        waitForAjaxLoader();
        checkErrors();
        SEARCH_PARTNERS_ICON.shouldBe(visible);
    }

    public void clearAllSelected() {
        if (!$$(PUBLISHERS_FILTER_CHECKBOXES).isEmpty()) {
            log.info("need to clear");
            $$(PUBLISHERS_FILTER_CHECKBOXES).filter(checked).asFixedIterable().forEach(SelenideElement::click);
            SEARCH_PARTNERS_ICON.click();
            waitForAjaxLoader();
            checkErrors();
        }
    }

    public void chooseCampaignTierRadio() {
        ElementsCollection list = $$(CAMPAIGN_TIER_RADIO);
        int element = new Random().nextInt(list.size());
        list.get(element).click();
        campaignTierFilterValue = helpersInit.getBaseHelper().getTextAndWriteLog(list.get(element).val());
    }

    public String chooseAnimation() {
        return helpersInit.getBaseHelper().selectRandomValue(ANIMATION_SELECT);
    }

    public void chooseShowTeasersRadio() {
        ElementsCollection list = $$(SHOW_TEASERS_RADIO);
        int element = new Random().nextInt(list.size());
        list.get(element).click();
        showTeasersValue = helpersInit.getBaseHelper().getTextAndWriteLog(list.get(element).val());
    }

    public void choosePartnerInFilter(int campaignId) {
        $("#p" + campaignId).shouldBe(visible).click();
    }

    public boolean checkPartnerInFilter(int campaignId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($("#p" + campaignId).shouldBe(visible).is(checked));
    }

    public boolean publisherFilterIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUBLISHER_FILTER_FORM.isDisplayed());
    }

    public String getDescriptionRequiredLabel() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DESCRIPTION_REQUIRED_INFO.shouldBe(visible).attr("title"));
    }

    /**
     * Check disabled "Description required" checkbox
     */
    public boolean checkDescriptionRequiredDependency() {
        if (showDescriptionIsSelected(false)) {
            return descriptionRequiredIsDisabled();
        }
        return false;
    }

    /**
     * сохраняем виджет в кабе после редактирования
     */
    public void saveWidgetSettings() {
        clickInvisibleElementJs(SAVE_BUTTON.shouldBe(visible));
        waitForAjaxLoader();
        waitForAjax();
    }

    /**
     * изменение систояния фильтра по тизерам и сохранение настроек
     */
    public void changeStateAdsFilter() {
        changeStateAdsFilter(optionAdsFilterSelect);
        setAndSubmitAdsFilter(optionsAdsFilterContainer);

        if (isExistAdsFilterErrorMessage()) return;

        saveWidgetSettings();

        helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * изменение систояния фильтра по тизерам и сохранение настроек
     */
    public void editAdsFilter() {
        openEditAdsFilterPopup();
        setAndSubmitAdsFilter(optionsAdsFilterContainer);

        if (isExistAdsFilterErrorMessage()) return;

        saveWidgetSettings();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * проверка систояния фильтра по тизерам
     */
    public boolean checkAdsFilterSetting() {
        return checkAdsFilterSelectedValue(optionAdsFilterSelect) &
                checkDisplayedAdsValue(optionsAdsFilterContainer) &
                checkDisplayedAdsValueAtPopup(optionsAdsFilterContainer);
    }

    /**
     * @param filterType 0 - No; 1 - Only; 2 - Except
     */
    public void choosePublishersFilterType(String filterType, int campaignId) {

        choosePublishersFilterType(filterType);
        if (!filterType.equals("0")) {
            clickUploadPartnersList();
            clearAllSelected();
            choosePartnerInFilter(campaignId);
        }
        saveWidgetSettings();
    }

    /**
     * @param filterType 0 - No; 1 - Only; 2 - Except
     */
    public boolean checkPublishersFilter(String filterType, int campaignId) {
        if (checkPublishersFilterType().equals(filterType)) {

            if (filterType.equals("0")) return !publisherFilterIsDisplayed();

            clickUploadPartnersList();
            return checkPartnerInFilter(campaignId);
        }
        return false;
    }

    public void clearAllSelectedCategoryInMultiFilter() {
        helpersInit.getMultiFilterHelper().clearAllSelectedCategoryInFilter(PRODUCT_PROMOTIONS_CHECKBOX);
        helpersInit.getMultiFilterHelper().clearAllSelectedCategoryInFilter(CONTENT_PROMOTIONS_CHECKBOX);
    }

    /**
     * 'Category filter'
     * check is selected custom category by Name
     */
    public boolean isSelectedCustomCheckboxInCategoryFilter(String categoryName) {
        SelenideElement category = getCategoryFilterCheckbox(categoryName);
        return helpersInit.getBaseHelper().checkDatasetContains(category.attr("class"), "fancytree-selected");
    }

    public boolean checkSearchCategoryFilter(boolean isMassAction) {
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        ElementsCollection categoryList = $$x(CATEGORY_FILTER_TITLES);
        String category = helpersInit.getBaseHelper().getTextAndWriteLog(categoryList.get(randomNumbersInt(categoryList.size())).text());

        setCategoryToMultiFilter(category);
        return $$x(String.format(CATEGORY_FILTER_TITLES_AFTER_SEARCH, isMassAction ? "*[@id='categoriesBox']//" : ""))
                .texts().stream().allMatch(i -> i.toLowerCase().contains(category.toLowerCase()) || i.equalsIgnoreCase(category));
    }

    public boolean checkCategoryFilterDataInIcon(ArrayList<String> chooseCategoryInEditInterface) {
        chooseCategoryInEditInterface.forEach(System.out::println);
        log.info("-------");
        $$(CATEGORY_FILTER_POPUP_DATA).texts().forEach(System.out::println);
        return chooseCategoryInEditInterface.size() > 0 &&
                new HashSet<>($$(CATEGORY_FILTER_POPUP_DATA).texts()).containsAll(chooseCategoryInEditInterface);
    }

    public boolean checkCategoryFilterDataInIconAfterMassAction(ArrayList<String> chooseCategoryInEditInterface) {
        return $$(CATEGORY_FILTER_POPUP_DATA).size() == chooseCategoryInEditInterface.size() &&
                new HashSet<>($$(CATEGORY_FILTER_POPUP_DATA).texts()).containsAll(chooseCategoryInEditInterface);
    }

    public JSONArray chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(int countValue){
        JSONArray main = new JSONArray();
        ArrayList<String> adTypes = new ArrayList<>();
        ArrayList<String> landingTypes = new ArrayList<>();

        log.info("отримуємо колекцію усіх елементів дерева");
        ElementsCollection list = $$(CATEGORY_FILTER_ELEMENTS);
        ArrayList<Integer> listCategoryId = helpersInit.getBaseHelper().getRandomCountValue(list.size() - 1, countValue);

        log.info("вибираємо 4 рандомні категорії та записуємо їх у масив");
        for (Integer integer : listCategoryId) {
            // get category id
            String category = list.get(integer).parent().attr("id");
            category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);

            //click category icon (лейка) and checkbox
            $("#ft_product-" + category + " .fancytree-checkbox").scrollTo().click();
            $("#ft_product-" + category + " .fancytree-icon").click();
            $("#ft_product-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

            //get random ad_types
            ElementsCollection adTypesList = $$("#ft_product-" + category + " .ad_types [data-type]");
            ArrayList<Integer> adTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(adTypesList.size() - 1, 3);

            // clear all checked adTypes
            $$("#ft_product-" + category + " .ad_types [data-type]:checked").asDynamicIterable().forEach(i ->
            {
                i.shouldBe(attribute("selected"));
                i.click(usingJavaScript());
                i.shouldNotBe(attribute("selected"));
            });

            for (int at : adTypesChosen) {
                // click new adTypes
                adTypesList.get(at).click(usingJavaScript());
                adTypes.add(adTypesList.get(at).attr("data-type"));
            }

            //get random landing_types
            ElementsCollection landingTypesList = $$("#ft_product-" + category + " .landing_types [data-type]");
            ArrayList<Integer> landingTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(landingTypesList.size() - 1, 4);

            // clear all checked landingTypes
            $$("#ft_product-" + category + " .landing_types [data-type]:checked").asDynamicIterable().forEach(i ->
            {
                i.shouldBe(attribute("selected"));
                i.click(usingJavaScript());
                i.shouldNotBe(attribute("selected"));
            });

            for (int at : landingTypesChosen) {
                // click new landingTypes
                landingTypesList.get(at).click(usingJavaScript());
                landingTypes.add(landingTypesList.get(at).attr("data-type"));
            }

            //write data to json
            JSONObject temp = new JSONObject();
            temp.put("category", category);
            temp.put("adType", new Gson().toJson(adTypes));
            temp.put("landingTypes", new Gson().toJson(landingTypes));

            main.add(temp);

            adTypes.clear();
            landingTypes.clear();
        }
        return main;
    }

    public void clickAdTypeOrLandingTypeForCategory(String categoryName, boolean isAdType){
        // get category id
        String category = $x(".//li[*[*[text()=\"" + categoryName + "\"]]]").attr("id");
        category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);
        //click category icon (лейка) and checkbox
        $("#ft_product-" + category + " .fancytree-icon").click();
        $("#ft_product-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

        $$("#ft_product-" + category + " ." + (isAdType ? "ad":"landing") + "_types [data-type]").get(1).click(usingJavaScript());
    }

    public boolean checkAdTypesAndLandingTypesForCategory(JSONArray obj) {
        String categoryId;
        ArrayList<String> adTypes;
        ArrayList<String> landingTypes;

        int count = 0;

        for (Object j : obj) {
            // get category, ad_types, landing_types from json
            categoryId = ((JSONObject) j).get("category").toString();
            adTypes = new Gson().fromJson(((JSONObject) j).get("adType").toString(), new TypeToken<List<String>>() {
            }.getType());
            landingTypes = new Gson().fromJson(((JSONObject) j).get("landingTypes").toString(), new TypeToken<List<String>>() {
            }.getType());

            log.info("check chosen category");
            if (helpersInit.getBaseHelper().checkDatasetContains($("[id^='ft_product-" + categoryId + "']>span").attr("class"), "fancytree-selected")) {
                //click category icon (лейка) and checkbox
                $("#ft_product-" + categoryId + " .fancytree-icon").scrollTo().click();
                $("#ft_product-" + categoryId + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

                List<String> adTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_product-" + categoryId + " .ad_types [data-type][checked='checked']")){
                    adTypes_list.add(s.attr("data-type"));
                }

                List<String> landingTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_product-" + categoryId + " .landing_types [data-type][checked='checked']")){
                    landingTypes_list.add(s.attr("data-type"));
                }

                log.info("check chosen adTypes and landingTypes");
                if (new HashSet<>(adTypes_list).containsAll(adTypes) &
                        new HashSet<>(landingTypes_list).containsAll(landingTypes)) {
                    count++;
                }
                else {
                    log.info("adTypes: " + adTypes);
                }
                CATEGORY_FILTER_SEARCH_INPUT.click();
            }
            else {
                log.info("categoryId: " + categoryId);
            }
        }
        return obj.size() > 0 &&
                obj.size() == count;
    }

    public void productPromotionsExpanderClick(){ PRODUCT_PROMOTIONS_EXPANDER_ICON.click(); }
}
