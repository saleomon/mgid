package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.*;
import com.google.common.collect.Table;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.checkIsCheckboxSelected;
import static pages.cab.publishers.locators.widgets.CompositeWidgetLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabCompositeSettings {

    private final HelpersInit helpersInit;
    private final Logger log;

    private String pushProvider = null;
    private String sourcesId = null;
    private String sourcesIdName = null;
    private boolean lowVolumeDomain = false;

    // Mirror
    private String mirror;

    // Category Platform
    private String categoryPlatform;

    // non clickable area
    private final String[] areaNames = {"top", "bottom", "right", "left"};
    private final ArrayList<String> areaPercents = new ArrayList<>();

    private ArrayList<String> backButtonTrafficTypes = new ArrayList<>();
    private ArrayList<String> backButtonDevices = new ArrayList<>();


    public CabCompositeSettings(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public enum AdBlockTemplates {
        NONE("0"),
        UNDER_ARTICLE("1"),
        SIDEBAR("2"),
        HEADER("3"),
        IN_ARTICLE("4");

        private final String value;

        AdBlockTemplates(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    //clicktracking
    private String clicktrackingMacros;

    //GPT options
    private boolean gptIntegrationIsEnable;
    private String gptSlot;
    private String gptDivId;
    private String gptAdUnit;
    private String gptPlacementForDesktop;
    private String gptPlacementForMobile;
    private boolean gptLazyLoadIsEnable;
    private String gptLazyLoadRootMargin;
    private boolean gptDisplayOnSubwidgetsIsEnable;

    public String getMirror() {
        return mirror;
    }

    public CabCompositeSettings setCategoryPlatform(String categoryPlatform) {
        this.categoryPlatform = categoryPlatform;
        return this;
    }

    public CabCompositeSettings setPushProvider(String pushProvider) {
        this.pushProvider = pushProvider;
        return this;
    }

    public String getPushProvider() {
        return pushProvider;
    }

    public String getSourcesId() {
        return sourcesId;
    }

    public boolean isLowVolumeDomain() {
        return lowVolumeDomain;
    }

    public CabCompositeSettings setSourcesIdName(String sourcesIdName) {
        this.sourcesIdName = sourcesIdName;
        return this;
    }

    public CabCompositeSettings setSourcesId(String sourcesId) {
        this.sourcesId = sourcesId;
        return this;
    }

    public CabCompositeSettings setClicktrackingMacros(String clicktrackingMacros) {
        this.clicktrackingMacros = clicktrackingMacros;
        return this;
    }

    public String getClicktrackingMacros() {
        return clicktrackingMacros;
    }

    /**
     * изменяет состояние чекбокса RTB
     */
    public void changeStateOfRtbOption(boolean state) {
        markUnMarkCheckbox(state, RTB_ENABLING_CHECKBOX);
    }

    public void fillDelayInMsInput(String state) {
        clearAndSetValue(ACTIVE_DELAY_INPUT, state + Keys.TAB);
    }

    public void switchDelayOfClickability(boolean state) {markUnMarkCheckbox(state, ACTIVATE_DELAY_CHECKBOX);}

    public boolean checkDelayOfClickability(boolean state) {return checkIsCheckboxSelected(state, ACTIVATE_DELAY_CHECKBOX);}

    public boolean checkStateOfRtbCheckbox() {
        return RTB_ENABLING_CHECKBOX.isSelected();
    }

    public boolean checkVisibilityRtbOption() {
        return RTB_ENABLING_CHECKBOX.exists();
    }

    public String getActiveDelayValue() { return ACTIVE_DELAY_INPUT.shouldBe(visible).val(); }

    /**
     * check state checkbox 'Show direct links on hover'
     */
    public boolean showDirectLinkIsSelected() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SHOW_DIRECT_LINK_CHECKBOX.isSelected());
    }

    public String getSelectedSourceType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SOURCE_TYPE_SELECT.getSelectedValue());
    }

    /**
     * is disabled checkbox 'XML / RSS'
     */
    public boolean isDisabledXmlCheckbox() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(XML_RSS_CHECKBOX.shouldBe(visible).is(disabled));
    }

    public void switchXmlCheckbox(boolean flag) {
        markUnMarkCheckbox(flag, XML_RSS_CHECKBOX);
    }

    public void switchAdLinkCheckbox(boolean flag) {
        markUnMarkCheckbox(flag, AD_LINK_CHECKBOX);
    }

    public boolean checkAdLinkCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, AD_LINK_CHECKBOX);
    }

    public void switchWidgetLogoCheckbox(boolean flag) {
        markUnMarkCheckbox(flag, WIDGET_LOGO_CHECKBOX);
    }

    public boolean checkWidgetLogoCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, WIDGET_LOGO_CHECKBOX);
    }

    public boolean isDisplayedWidgetLogoCheckbox() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_LOGO_CHECKBOX.isDisplayed());
    }

    public void switchWidgetTitleCheckbox(boolean flag) {
        markUnMarkCheckbox(flag, WIDGET_TITLE_CHECKBOX);
    }

    public void switchPlaceMultipleTimesOnPage(boolean state) {
        markUnMarkCheckbox(state, ALLOW_MULTIPLE_WIDGETS);
    }

    public void switchAutoRefresh(boolean state) {
        markUnMarkCheckbox(state, AUTO_REFRESH_CHECKBOX);
    }

    public void switchVideoSsp(boolean state) {
        markUnMarkCheckbox(state, VIDEO_SSP_CHECKBOX);
    }

    public boolean checkVideoSsp(boolean state) {
        return checkIsCheckboxSelected(state, VIDEO_SSP_CHECKBOX);
    }

    public boolean checkAutoRefresh(boolean state) {
        return checkIsCheckboxSelected(state, AUTO_REFRESH_CHECKBOX);
    }

    public boolean checkPlaceMultipleTimesOnPage(boolean state) {
        return checkIsCheckboxSelected(state, ALLOW_MULTIPLE_WIDGETS);
    }

    public boolean checkPlaceReservation(boolean state) {
        return checkIsCheckboxSelected(state, PLACE_RESERVATION_CHECKBOX);
    }

    public boolean checkWidgetTitleCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, WIDGET_TITLE_CHECKBOX);
    }

    public void switchPlaceReservation(boolean state) {
        markUnMarkCheckbox(state, PLACE_RESERVATION_CHECKBOX);
    }

    public void switchAdBlockIntegrations(boolean state) {
        markUnMarkCheckbox(state, ADBLOCK_INTEGRATIONS_CHECKBOX);
    }

    public void selectAdBlockTemplates(AdBlockTemplates template){ ADBLOCK_TEMPLATE_SELECT.shouldBe(visible).selectOptionByValue(template.getTypeValue()); }

    public boolean isDisabledAdBlockTemplates(){ return ADBLOCK_TEMPLATE_SELECT.is(disabled); }

    public boolean isDisplayedAdBlockTemplates(){ return ADBLOCK_TEMPLATE_SELECT.isDisplayed(); }

    public boolean checkAdBlockTemplates(AdBlockTemplates template){ return ADBLOCK_TEMPLATE_SELECT.getSelectedValue().equals(template.getTypeValue()); }

    public void switchDefaultJs(boolean state){
        markUnMarkCheckbox(state, DEFAULT_JS_CHECKBOX);
    }

    public void fillDefaultJs(String value){
        sendKey(DEFAULT_JS_INPUT, value);
    }

    public void switchAutoRefreshAdsByViewability(boolean state){markUnMarkCheckbox(state, AUTO_REFRESH_ADS_BY_VIEWABILITY_CHECKBOX);}

    public void fillAutoRefreshAdsByViewabilityTime(String value){ sendKey(AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_TIME_INPUT, value);}

    public void selectAutoRefreshAdsByViewabilityBy(String option){ AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_BY_SELECT.selectOptionByValue(option); }

    public boolean checkAutoRefreshAdsByViewability(boolean state) {return checkIsCheckboxSelected(state, AUTO_REFRESH_ADS_BY_VIEWABILITY_CHECKBOX);}

    public String getAutoRefreshAdsByViewabilityTime() {return AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_TIME_INPUT.val();}

    public String getAutoRefreshAdsByViewabilityBy(){ return AUTO_REFRESH_ADS_BY_VIEWABILITY_REFRESH_ADS_BY_SELECT.getSelectedValue(); }

    public void switchLoadAdditionalWidgets(boolean state){markUnMarkCheckbox(state, LOAD_ADDITIONAL_WIDGETS_CHECKBOX);}

    public boolean isDisplayedLoadAdditionalSettings(){
        return LOAD_ADDITIONAL_WIDGETS_CHECKBOX.isDisplayed();
    }

    public void fillLoadAdditionalWidgetsId(String value){
        sendKey(LOAD_ADDITIONAL_WIDGETS_ID_INPUT, value);
    }

    public void fillLoadAdditionalWidgetsSelector(String value){
        sendKey(LOAD_ADDITIONAL_WIDGETS_SELECTOR_INPUT, value);
    }

    public boolean checkAdBlockIntegrations(boolean state) {
        return checkIsCheckboxSelected(state, ADBLOCK_INTEGRATIONS_CHECKBOX);
    }

    public boolean isDisplayedAdBlockIntegrations(){
        return ADBLOCK_INTEGRATIONS_CHECKBOX.isDisplayed();
    }

    public boolean isDisplayedWidgetTitleCheckbox() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WIDGET_TITLE_CHECKBOX.isDisplayed());
    }

    public void switchNonClickableAreaCheckbox(boolean state) {
        markUnMarkCheckbox(state, NON_CLICKABLE_AREA_CHECKBOX);
    }

    public boolean checkNonClickableAreaCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, NON_CLICKABLE_AREA_CHECKBOX);
    }

    public boolean isDisplayedNonClickableAreaCheckbox() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NON_CLICKABLE_AREA_CHECKBOX.isDisplayed());
    }

    public void switchGoogleDfp(boolean state) {
        markUnMarkCheckbox(state, DFP_CHECKBOX);
    }

    public void switchFlexibleWidget(boolean state) {
        markUnMarkCheckbox(state, ELASTIC_CHECKBOX);
    }

    public CabCompositeSettings switchClicktracking(boolean state) {
        markUnMarkCheckbox(state, CLICKTRACKING_CHECKBOX);
        return this;
    }

    public CabCompositeSettings fillClicktrackingMacros() {
        clearAndSetValue(CLICKTRACKING_INPUT.shouldBe(visible).scrollTo(), clicktrackingMacros);
        return this;
    }

    public void switchEndless(boolean state) {
        markUnMarkCheckbox(state, UNLIMITED_CHECKBOX);
    }

    public boolean checkFlexibleWidget(boolean state) {
        return checkIsCheckboxSelected(state, ELASTIC_CHECKBOX);
    }

    public boolean checkEndless(boolean state) {
        return checkIsCheckboxSelected(state, UNLIMITED_CHECKBOX);
    }

    public boolean checkGoogleDfp(boolean state) {
        return checkIsCheckboxSelected(state, DFP_CHECKBOX);
    }

    public void switchStateBackButtonSettings(boolean state){
        markUnMarkCheckbox(state, BACK_BUTTON_SETTINGS_CHECKBOX);
    }

    public boolean checkBackButtonCheckbox(boolean state){ return checkIsCheckboxSelected(state, BACK_BUTTON_SETTINGS_CHECKBOX); }

    public void switchStateBackButtonTrafficType(boolean state, List<String> values){
        markUnMarkCheckbox(state, BACK_BUTTON_TRAFFIC_TYPE_CHECKBOX);
        backButtonTrafficTypes = helpersInit.getBaseHelper().selectCustomValuesForMultiSelect(BACK_BUTTON_TRAFFIC_ID_ELEMENT, values );
    }

    public boolean checkBackButtonTrafficType(boolean state){
        return checkIsCheckboxSelected(state, BACK_BUTTON_TRAFFIC_TYPE_CHECKBOX) &&
                (!state ||
                helpersInit.getBaseHelper().checkValuesForMultiSelect(BACK_BUTTON_TRAFFIC_ID_ELEMENT, backButtonTrafficTypes));
    }

    public void switchStateBackButtonDevice(boolean state, List<String> values){
        markUnMarkCheckbox(state, BACK_BUTTON_DEVICE_CHECKBOX);
        backButtonDevices = helpersInit.getBaseHelper().selectCustomValuesForMultiSelect(BACK_BUTTON_DEVICE_ID_ELEMENT, values);
    }

    public boolean checkBackButtonDevices(boolean state){
        return checkIsCheckboxSelected(state, BACK_BUTTON_DEVICE_CHECKBOX) &&
                (!state ||
                helpersInit.getBaseHelper().checkValuesForMultiSelect(BACK_BUTTON_DEVICE_ID_ELEMENT, backButtonDevices));
    }

    public void fillBackButtonWidgetId(String widgetId){ clearAndSetValue(BACK_BUTTON_WIDGET_ID_INPUT, widgetId); }

    public boolean checkBackButtonWidgetId(String widgetId){ return BACK_BUTTON_WIDGET_ID_INPUT.val().equals(widgetId); }

    public void switchStateBackButtonTrafficSource(boolean state, String trafficSource){
        markUnMarkCheckbox(state, BACK_BUTTON_TRAFFIC_SOURCE_CHECKBOX);
        clearAndSetValue(BACK_BUTTON_TRAFFIC_SOURCE_INPUT, trafficSource);
    }

    public boolean checkBackButtonTrafficSource(boolean state, String trafficSource){
        return checkIsCheckboxSelected(state, BACK_BUTTON_TRAFFIC_SOURCE_CHECKBOX) &&
                (!state || BACK_BUTTON_TRAFFIC_SOURCE_INPUT.val().equals(trafficSource));
    }

    public void switchStateBackButtonBanner(boolean state, String backgroundColor, String textColor, String text){
        markUnMarkCheckbox(state, BACK_BUTTON_BANNER_DISPLAY_CHECKBOX);
        clearAndSetValue(BACK_BUTTON_BANNER_BACKGROUND_COLOR_INPUT, backgroundColor);
        clearAndSetValue(BACK_BUTTON_BANNER_TEXT_COLOR_INPUT, textColor);
        clearAndSetValue(BACK_BUTTON_BANNER_TEXT_INPUT, text);
    }

    public boolean checkBackButtonBanner(boolean state, String backgroundColor, String textColor, String text){
        return checkIsCheckboxSelected(state, BACK_BUTTON_BANNER_DISPLAY_CHECKBOX) &&
                (!state ||
                        (BACK_BUTTON_BANNER_BACKGROUND_COLOR_INPUT.val().equals(backgroundColor) &
                         BACK_BUTTON_BANNER_TEXT_COLOR_INPUT.val().equals(textColor) &
                         BACK_BUTTON_BANNER_TEXT_INPUT.val().equals(text))
                );
    }

    public void switchStateBackButtonHeader(boolean state, String header){
        markUnMarkCheckbox(state, BACK_BUTTON_HEADER_DISPLAY_CHECKBOX);
        clearAndSetValue(BACK_BUTTON_HEADER_DISPLAY_INPUT, header);
    }

    public boolean checkBackButtonHeader(boolean state, String header){
        return checkIsCheckboxSelected(state, BACK_BUTTON_HEADER_DISPLAY_CHECKBOX) &&
                (!state ||
                BACK_BUTTON_HEADER_DISPLAY_INPUT.val().equals(header));
    }

    /**
     * изменяет состояние чекбокса по фильтру по площадкам
     */
    public void changeStateSubSourceCheckbox(boolean state) {
        markUnMarkCheckbox(state, SUBSOURCE_FILTER_CHECKBOX);
    }

    /**
     * добавляет источники в фильтр по источникам
     */
    public void insertSubSource(ArrayList<String> listOfSubSource) {
        listOfSubSource.forEach(elem -> SUBSOURCE_INPUT.sendKeys(elem + Keys.SPACE));
    }

    /**
     * проверка настроек фильтра по подисточникам
     */
    public boolean checkSavedSubSources(ArrayList<String> listOfSubSource) {
        return new HashSet<>($$(SUBSOURCE_SAVED_ELEMENTS).texts().stream().map(String::trim).collect(Collectors.toList())).containsAll(listOfSubSource);
    }

    /**
     * удаляет первый источник из контейнера подисточников
     */
    public void deleteFirstElement() {
        SUBSOURCE_FIRST_INPUTED_ELEMENT.scrollTo().click();
    }

    /**
     * изменяет состояние чекбокса по фильтру по площадкам
     */
    public boolean checkIsCheckboxSubSourceFilterSelected() {
        return checkIsCheckboxSelected(true, SUBSOURCE_FILTER_CHECKBOX);
    }

    /**
     * проверка на пустой контейнер фильтра подисточников
     */
    public boolean checkIsEmptySubSourceContainer() {
        return $(SUBSOURCE_SAVED_ELEMENTS).exists();
    }

    /**
     * проверка отображения фильтра по подисточников
     */
    public boolean checkDisplayingSubSourceOption() {
        return SUBSOURCE_FILTER_CHECKBOX.exists();
    }

    public boolean isDisabled_NewsAmount() {
        return NEWS_AMOUNT_INPUT.shouldBe(visible).is(disabled);
    }

    public boolean goToTheTransitIsSelected() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(GO_TO_THE_TRANSIT_CHECKBOX.isSelected());
    }

    /**
     * get selected value in 'DesktopDoubleClick'
     */
    public String getSelectedValInDesktopDoubleClick() {
        return DESKTOP_DOUBLECLICK_SELECT.getSelectedValue();
    }

    /**
     * get selected value in 'MobileDoubleClick'
     */
    public String getSelectedValInMobileDoubleClick() {
        return MOBILE_DOUBLECLICK_SELECT.getSelectedValue();
    }

    /**
     * is displayed input 'Doubleclick delay'
     */
    public boolean isDisplayedDesktopDoubleClickDelay() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DESKTOP_DOUBLECLICK_DELAY_INPUT.isDisplayed());
    }

    /**
     * is displayed input 'Doubleclick delay' mobile
     */
    public boolean isDisplayedMobileDoubleClickDelay() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MOBILE_DOUBLECLICK_DELAY_INPUT.isDisplayed());
    }

    /**
     * select current value in 'Desktop doubleclick'
     */
    public void selectDesktopDoubleClick(String value) {
        DESKTOP_DOUBLECLICK_SELECT.shouldBe(visible).selectOptionByValue(value);
    }

    /**
     * set desktop 'Doubleclick delay'
     */
    public void setDesktopDoubleClickDelay(String value) {
        clearAndSetValue(DESKTOP_DOUBLECLICK_DELAY_INPUT.shouldBe(visible), value);
    }

    /**
     * get desktop 'Doubleclick delay'
     */
    public String getDesktopDoubleClickDelay() {
        return DESKTOP_DOUBLECLICK_DELAY_INPUT.val();
    }

    /**
     * select current value in 'Mobile doubleclick'
     */
    public void selectMobileDoubleclick(String value) {
        MOBILE_DOUBLECLICK_SELECT.shouldBe(visible).selectOptionByValue(value);
    }

    /**
     * set mobile 'Doubleclick delay' desktop
     */
    public void setMobileDoubleClickDelay(String value) {
        clearAndSetValue(MOBILE_DOUBLECLICK_DELAY_INPUT.shouldBe(visible), value);
    }

    /**
     * get mobile 'Doubleclick delay' desktop
     */
    public String getMobileDoubleClickDelay() {
        return MOBILE_DOUBLECLICK_DELAY_INPUT.val();
    }

    public void fillPageCount(String val) {
        clearAndSetValue(ELASTIC_PAGE_COUNT_INPUT, val);
    }

    /**
     * switch on/off SOURCE_ID
     */
    public void switchSourceId(boolean state) {
        markUnMarkCheckbox(state, SRC_ID_CHECKBOX);
    }

    /**
     * is selected SOURCE_ID
     */
    public boolean isSourceIdChecked() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SRC_ID_CHECKBOX.isSelected());
    }

    /**
     * choose random mirror
     */
    public void chooseMirror() {
        mirror = helpersInit.getBaseHelper().selectRandomValue(MIRROR_SELECT);
    }

    public List<String> getAllMirrorsFromSubnetSelect(){
        List<String> list = new ArrayList<>();
        for(SelenideElement s : MIRROR_SELECT.$$("option")){
            list.add(s.val());
        }
        return list;
    }

    /**
     * check selected custom mirror
     */
    public boolean checkMirror(String mirrorVal) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(MIRROR_SELECT, mirrorVal);
    }

    /**
     * check disabled save button
     */
    public boolean checkDisabledSaveButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SAVE_BUTTON.shouldBe(visible).is(disabled));
    }

    /**
     * change state of stop words checkbox
     */
    public void changeStateStopWordsCheckbox(boolean state) {
        markUnMarkCheckbox(state, STOP_WORD_CHECKBOX);
    }

    public boolean checkStateStopWordsCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, STOP_WORD_CHECKBOX);
    }

    public boolean checkSavedStopWords(ArrayList<String> tags) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(STOP_WORDS_TEXTS).texts().stream().map(String::trim).collect(Collectors.toList()).equals(tags));
    }

    public void pasteStopWords() {
        STOP_WORDS_INPUT.shouldBe(visible).sendKeys(Keys.CONTROL + "V");
    }

    public List getStopWords() {
        return STOP_WORD_OPTIONS.texts().stream().map(String::trim).collect(Collectors.toList());
    }

    /**
     * delete stop word options
     */
    public void deleteStopWords(int... count) {
        int t = 0;
        int countWords = count.length > 0 ? count[0] : STOP_WORD_DELETE_BUTTONS.size();
        while (t < countWords){
            STOP_WORD_DELETE_BUTTONS.get(0).scrollTo().click();
            t++;
        }
    }

    /**
     * copy stopWords to buffer
     */
    public void clickCopyStopWords() {
        STOP_WORDS_COPY_ICON.scrollTo().hover().click();
        checkErrors();
    }

    public void chooseCategoryPlatform() {
        if (categoryPlatform == null) {
            categoryPlatform = helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_PLATFORM_SELECT);
        } else {
            helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_PLATFORM_SELECT, categoryPlatform);
        }

        if (categoryPlatform.matches("149|150")) choosePushProvider();
    }

    public CabCompositeSettings choosePushProvider() {
        pushProvider = pushProvider == null ?
                helpersInit.getBaseHelper().selectRandomValue(PUSH_PROVIDER_SELECT.shouldBe(visible)) :
                helpersInit.getBaseHelper().selectCustomValue(PUSH_PROVIDER_SELECT.shouldBe(visible), pushProvider);
        return this;
    }

    public boolean checkPushProvider() {
        return helpersInit.getBaseHelper().checkDatasetEquals(PUSH_PROVIDER_SELECT.getSelectedValue(), pushProvider);
    }

    public boolean checkSourcesId() {
        return helpersInit.getBaseHelper().checkDatasetEquals(SOURCES_ID_SELECT.getSelectedValue(), sourcesId);
    }

    public boolean checkCategoryPlatform() {
        return helpersInit.getBaseHelper().checkDatasetContains(CATEGORY_PLATFORM_SELECT.find(By.cssSelector("[data-id='" + categoryPlatform + "']")).attr("class"), "selected-cat");
    }

    /**
     * check base push settings
     */
    public boolean checkEditPushWidget() {
        if (isDisabledXmlCheckbox()) {
            saveWidgetSettings();
            checkErrors();
            return helpersInit.getMessageHelper().isSuccessMessagesCab();
        }
        return false;
    }

    /**
     * выключаем все чекбоксы adTypes
     */
    public void clearAllAdTypesCheckBoxes(Map<String, Boolean> map) {
        map.keySet().stream()
                .map(aBoolean -> $("[name='adTypes[]'][value='" + aBoolean + "']"))
                .collect(Collectors.toList())
                .stream()
                .filter(WebElement::isSelected)
                .forEach(SelenideElement::click);
    }

    /**
     * сохраняем виджет в кабе после редактирования
     */
    public CabCompositeSettings saveWidgetSettings() {
        SAVE_BUTTON.shouldBe(visible).click(ClickOptions.usingJavaScript());
        waitForAjaxLoader();
        waitForAjax();
        checkErrors();
        return this;
    }

    /**
     * set values into stop words form
     */
    public void setStopWords(ArrayList<String> listOfStopWords) {
        STOP_WORDS_INPUT.shouldBe(visible);
        for (String s : listOfStopWords) {
            STOP_WORDS_INPUT.sendKeys(s + Keys.ENTER);
        }
    }

    public boolean isDisplayedStopWordsError(){ return STOP_WORD_ERROR.isDisplayed(); }

    /**
     * check srcId and source name
     */
    public boolean checkSrcId(boolean flag, String value) {
        return checkIsCheckboxSelected(flag, SRC_ID_CHECKBOX.shouldBe(visible)) &
                (flag ? Objects.equals(SRC_ID_INPUT.val(), value) : !SRC_ID_INPUT.isDisplayed());
    }

    /**
     * switch on/off srcId and fill source name
     */
    public void switchAndSetSrcId(boolean flag, String value) {
        switchSourceId(flag);

        if (flag) clearAndSetValue(SRC_ID_INPUT.shouldBe(visible), value);

        saveWidgetSettings();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    public void switchWidgetLogoAndTitle(boolean adLink, boolean widgetLogo, boolean widgetTitle) {
        switchAdLinkCheckbox(adLink);
        if (adLink) {
            switchWidgetLogoCheckbox(widgetLogo);
            switchWidgetTitleCheckbox(widgetTitle);
        }
        saveWidgetSettings();
    }

    public boolean checkWidgetLogoAndTitleSettings(boolean adLink, boolean widgetLogo, boolean widgetTitle) {
        return checkAdLinkCheckbox(adLink) &
                (adLink ?
                        (checkWidgetLogoCheckbox(widgetLogo) &
                                checkWidgetTitleCheckbox(widgetTitle))
                        :
                        (!isDisplayedWidgetLogoCheckbox() &
                                isDisplayedWidgetTitleCheckbox()));
    }

    ///////////////////////// AD TYPE LIMIT - START /////////////////////////////////////////


    public void setAdTypes(Table<String, Boolean, String> adTypesData) {
        boolean adTypeState;
        String adTypeValue;
        //Map<String, Boolean> adTypesMap = new HashMap<>();
        ElementsCollection adTypesList = $$("[name='adTypes[]']");
        ElementsCollection adTypesLimit = $$("input.type");

        // get state all adType checkboxes
        //adTypesList.forEach((k) -> adTypesMap.put(k.val(), k.isSelected()));

        // switch custom checkboxes
        for (SelenideElement element : adTypesList) {

            adTypeState = Boolean.parseBoolean(adTypesData.row(Objects.requireNonNull(element.val())).keySet().toArray()[0].toString());
            adTypeValue = adTypesData.row(Objects.requireNonNull(element.val())).values().toArray()[0].toString();

            // switch on/off adType checkbox (adTypeState)
            if ((adTypeState & !element.shouldBe(visible).isSelected()) || (!adTypeState & element.shouldBe(visible).isSelected())) {
                element.click();
            }

            // fill adType limit (adTypeValue)
            if (!adTypeValue.equals("-") && element.isSelected()) {
                clearAndSetValue(adTypesLimit.find(Condition.id(Objects.requireNonNull(element.val()))), adTypeValue);
            }
        }

        saveWidgetSettings();
    }

    public boolean checkAdType(Table<String, Boolean, String> adTypesData) {
        boolean adTypeState;
        String adTypeValue;
        int count = 0;
        ElementsCollection adTypesList = $$("[name='adTypes[]']");
        ElementsCollection adTypesLimit = $$("input.type");

        // check state custom checkboxes and limits
        for (SelenideElement element : adTypesList) {

            adTypeState = Boolean.parseBoolean(adTypesData.row(Objects.requireNonNull(element.val())).keySet().toArray()[0].toString());
            adTypeValue = adTypesData.row(Objects.requireNonNull(element.val())).values().toArray()[0].toString();

            if ((adTypeState == element.isSelected()) &
                    (adTypeValue.equals("-") ||
                            (!element.isSelected() && !adTypesLimit.find(Condition.id(Objects.requireNonNull(element.val()))).isDisplayed()) ||
                            (element.isSelected() && adTypeValue.equals(adTypesLimit.find(Condition.id(Objects.requireNonNull(element.val()))).val())))
            ) {
                count++;
            }
        }
        return helpersInit.getBaseHelper().checkDataset(count, adTypesData.size());
    }

    /**
     * проверяем проставление adTypes landingTypes
     */
    public boolean checkAdTypesInCab(Map<String, Boolean> map) {
        return map.entrySet().stream().allMatch(m -> $("[name='adTypes[]'][value='" + m.getKey() + "']").isSelected() == m.getValue());
    }

    /**
     * проверяем проставление landingTypes
     */
    public boolean checkLandingTypesInCab(Map<String, Boolean> map) {
        return map.entrySet().stream().allMatch(m -> $("[name='landingTypes[]'][value='" + m.getKey() + "']").isSelected() == m.getValue());
    }

    /**
     * снимаем в кабе все adTypes
     * и проставляем рандомные
     * если map.size() больше чем 2 - вкл/выкл 2 чекбокса, если меньше - 1 чекбокс
     */
    public Map<String, Boolean> switchOnRandomAdTypes(Map<String, Boolean> map) {
        ArrayList<Integer> massSwitchOnElements = new ArrayList<>();
        Map<String, Boolean> returnMap = new HashMap<>();

        log.info("выключаем все чекбоксы adTypes");
        clearAllAdTypesCheckBoxes(map);

        log.info("выбираем рандомные чекбоксы для включения их дальше");
        if (map.size() > 2) {
            massSwitchOnElements = helpersInit.getBaseHelper().getRandomCountValue(map.size(), 2);
        } else {
            massSwitchOnElements.add(randomNumbersInt(map.size()));
        }

        log.info("включаем выбранные чекбоксы");
        massSwitchOnElements.forEach(i -> $("[name='adTypes[]'][value='" + map.keySet().toArray()[i] + "']").click());

        log.info("перезаписываем мапку");
        for (Map.Entry<String, Boolean> m : map.entrySet()) {
            returnMap.put(m.getKey(), $("[name='adTypes[]'][value='" + m.getKey() + "']").isSelected());
        }

        log.info("сохраняем виджет в кабе");
        saveWidgetSettings();
        return returnMap;
    }

    ///////////////////////// AD TYPE LIMIT  - FINISH /////////////////////////////////////////

    ///////////////////////// DOUBLE CLICK - START ////////////////////////////////

    /**
     * check default settings for doubleClick
     */
    public boolean checkDefaultSettingsForDoubleClick() {
        return helpersInit.getBaseHelper().checkDatasetEquals(getSelectedValInDesktopDoubleClick(), "0") &
                helpersInit.getBaseHelper().checkDatasetEquals(getSelectedValInMobileDoubleClick(), "0") &
                !isDisplayedDesktopDoubleClickDelay() &
                !isDisplayedMobileDoubleClickDelay();
    }

    /**
     * add settings for Desktop doubleClick
     */
    public void setDesktopDoubleClickSettings(String desktopSelectVal, String desktopDoubleClickDelayVal) {
        selectDesktopDoubleClick(desktopSelectVal);

        if (!desktopSelectVal.equals("0")) setDesktopDoubleClickDelay(desktopDoubleClickDelayVal);
    }

    /**
     * add settings for Mobile doubleClick
     * 0 - Mobile doubleclick NO
     * 1 - Mobile doubleclick Default
     */
    public void setMobileDoubleClickSettings(String mobileSelectVal, String mobileDoubleClickDelay) {
        selectMobileDoubleclick(mobileSelectVal);

        if (!mobileSelectVal.equals("0")) setMobileDoubleClickDelay(mobileDoubleClickDelay);
    }

    /**
     * check Desktop DoubleClick Settings
     */
    public boolean checkDesktopDoubleClickSettings(String desktopSelectVal, String desktopDoubleClickDelayVal) {
        return helpersInit.getBaseHelper().checkDatasetEquals(getSelectedValInDesktopDoubleClick(), desktopSelectVal) &
                helpersInit.getBaseHelper().checkDatasetEquals(getDesktopDoubleClickDelay(), desktopDoubleClickDelayVal) &
                isDisplayedDesktopDoubleClickDelay();
    }

    /**
     * check Mobile DoubleClick Settings
     */
    public boolean checkMobileDoubleClickSettings(String mobileSelectVal, String mobileDoubleClickDelay) {
        return helpersInit.getBaseHelper().checkDatasetEquals(getSelectedValInMobileDoubleClick(), mobileSelectVal) &
                helpersInit.getBaseHelper().checkDatasetEquals(getMobileDoubleClickDelay(), mobileDoubleClickDelay) &
                isDisplayedMobileDoubleClickDelay();
    }

    ///////////////////////// DOUBLE CLICK - FINISH /////////////////////////////////////////


    public void setAllAreasResizable() {
        int x = 0, y = 0;

        for (String name : areaNames) {
            switch (name) {
                case "top" -> {
                    x = 0;
                    y = randomNumberFromRangeInt(2, 100);
                }
                case "bottom" -> {
                    x = 0;
                    y = randomNumberFromRangeInt(-100, -2);
                }
                case "right" -> {
                    x = randomNumberFromRangeInt(-120, -2);
                    y = 0;
                }
                case "left" -> {
                    x = randomNumberFromRangeInt(2, 120);
                    y = 0;
                }
            }
            actions().dragAndDropBy($("." + name + ".ui-resizable div"), x, y).perform();
        }
    }

    public void setAllAreasResizable_valid() {
        int x = 0, y = 0;

        for (String name : areaNames) {
            switch (name) {
                case "top" -> {
                    x = 0;
                    y = 170;
                }
                case "bottom" -> {
                    x = 0;
                    y = -170;
                }
                case "right" -> {
                    x = -170;
                    y = 0;
                }
                case "left" -> {
                    x = 170;
                    y = 0;
                }
            }
            actions().dragAndDropBy($("." + name + ".ui-resizable div"), x, y).perform();
        }
    }

    public void getAllResizableValue() {
        for (String area : areaNames) {
            areaPercents.add($(".percent." + area + " span").text());
        }
    }

    public boolean checkAllAreasResizable() {
        return IntStream.range(0, areaNames.length)
                .allMatch(i -> $(".percent." + areaNames[i] + " span").text().equals(areaPercents.get(i)));
    }

    public void chooseSourceId() {
        SOURCES_ID_LABEL.shouldBe(visible).click();
        ElementsCollection list = SOURCES_ID_RESULTS_SELECT.findAll(By.cssSelector("li:not([id$='--1'])"));

        if (sourcesId == null) {
            sourcesId = list.get(randomNumbersInt(list.size())).attr("id");
            sourcesId = Objects.requireNonNull(sourcesId).substring(sourcesId.lastIndexOf("-") + 1);
        }

        SOURCES_ID_RESULTS_SELECT.find(By.cssSelector("li[id$='-" + sourcesId + "']")).click();
    }

    public void getLowVolumeDomain() {
        lowVolumeDomain = LOW_VOLUME_DOMAIN_CHECKBOX.isSelected();
    }

    public void addNewDomainForAdvertisers() {
        lowVolumeDomain = helpersInit.getBaseHelper().getRandomFromArray(new boolean[]{true, false});
        String val = sourcesIdName == null ?
                "newDomainToAdvertisers" + BaseHelper.getRandomWord(2) :
                sourcesIdName;

        clearAndSetValue(NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(visible), val);
        markUnMarkCheckbox(lowVolumeDomain, NEW_DOMAIN_TO_ADVERTISERS_LOW_VOLUME);
        NEW_DOMAIN_TO_ADVERTISERS_SUBMIT.click();
        checkErrors();

        if (helpersInit.getMessageHelper().isSuccessMessagesCabWithoutClean()) {
            NEW_DOMAIN_TO_ADVERTISERS_INPUT.shouldBe(hidden);
            sourcesId = helpersInit.getBaseHelper().getTextAndWriteLog(SOURCES_ID_SELECT.getSelectedValue());
        }
    }

    public void setSearchSourcesIdValue(String val) {
        SOURCES_ID_LABEL.shouldBe(visible).click();
        SOURCES_ID_SEARCH_INPUT.shouldBe(visible).sendKeys(val);
    }

    public List<String> getValueFromSourcesIdSearch() {
        return SOURCES_ID_RESULTS_SELECT.findAll(By.tagName("li")).texts();
    }

    public void enableGptIntegration(boolean stateValue) {
        gptIntegrationIsEnable = stateValue;
        markUnMarkCheckbox(gptIntegrationIsEnable, GPT_INTEGRATION_CHECKBOX);
    }

    public void enableGptDisplayOnSubwidgets(boolean stateValue) {
        gptDisplayOnSubwidgetsIsEnable = stateValue;
        markUnMarkCheckbox(gptDisplayOnSubwidgetsIsEnable, GPT_DISPLAY_ON_THE_SUBWIDGETS_CHECKBOX);
    }

    public void fillGptSlotValue(String... gptSlotValue) {
        gptSlot = gptSlotValue.length > 0 ? gptSlotValue[0] : "Gpt slot " + getRandomWord(6);
        clearAndSetValue(GPT_SLOT_INPUT, gptSlot);
        log.info("Gpt slot - " + gptSlot);
    }

    public void fillGptDivIdValue(String... divIdValue) {
        gptDivId = divIdValue.length > 0 ? divIdValue[0] : "Gpt DivId " + getRandomWord(6);
        clearAndSetValue(GPT_DIV_ID_INPUT, gptDivId);
        log.info("Gpt Div Id - " + gptDivId);
    }

    public void fillGptAdUnitIdValue(String... adUnitIdValue) {
        gptAdUnit = adUnitIdValue.length > 0 ? adUnitIdValue[0] : helpersInit.getBaseHelper().randomNumbersString(3);
        clearAndSetValue(GPT_AD_UNIT_ID_INPUT, gptAdUnit);
        log.info("Gpt Ad Unit - " + gptAdUnit);
    }

    public void fillPlacementForDesktopValue(String... gptPlacementForDesktopValue) {
        gptPlacementForDesktop = gptPlacementForDesktopValue.length > 0 ? gptPlacementForDesktopValue[0] : String.valueOf(randomNumberFromRangeInt(1, 10));
        clearAndSetValue(GPT_PLACEMENT_FOR_DESKTOP_INPUT, gptPlacementForDesktop);
        log.info("GPT Placement For Desktop - " + gptAdUnit);
    }

    public void fillPlacementForMobileValue(String... gptPlacementForMobileValue) {
        gptPlacementForMobile = gptPlacementForMobileValue.length > 0 ? gptPlacementForMobileValue[0] : String.valueOf(randomNumberFromRangeInt(1, 10));
        clearAndSetValue(GPT_PLACEMENT_FOR_MOBILE_INPUT, gptPlacementForMobile);
        log.info("GPT Placement For Mobile - " + gptPlacementForMobile);
    }

    public void enableGptLazyLoad(boolean stateValue, String... gptLazyLoadRootMarginValue) {
        gptLazyLoadIsEnable = stateValue;
        markUnMarkCheckbox(gptLazyLoadIsEnable, GPT_ENABLE_LAZY_LOAD_CHECKBOX);
        if (stateValue) {
            gptLazyLoadRootMargin = gptLazyLoadRootMarginValue.length > 0 ? gptLazyLoadRootMarginValue[0] : String.valueOf(randomNumberFromRangeInt(100, 200));
            clearAndSetValue(GPT_LAZY_LOAD_ROOT_MARGIN_INPUT, gptLazyLoadRootMargin);
            log.info("GPT Lazy Load Root Margin - " + gptLazyLoadRootMargin);
        }
    }

    public boolean checkGptIntegration() {
        return helpersInit.getBaseHelper().checkDataset(GPT_INTEGRATION_CHECKBOX.isSelected(), gptIntegrationIsEnable) &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_SLOT_INPUT.getValue(), gptSlot) &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_DIV_ID_INPUT.getValue(), gptDivId) &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_AD_UNIT_ID_INPUT.getValue(), gptAdUnit) &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_PLACEMENT_FOR_DESKTOP_INPUT.getValue(), gptPlacementForDesktop) &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_PLACEMENT_FOR_MOBILE_INPUT.getValue(), gptPlacementForMobile) &&
                helpersInit.getBaseHelper().checkDataset(GPT_ENABLE_LAZY_LOAD_CHECKBOX.isSelected(), gptLazyLoadIsEnable) &&
                GPT_ENABLE_LAZY_LOAD_CHECKBOX.isSelected() ?
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_LAZY_LOAD_ROOT_MARGIN_INPUT.getValue(), gptLazyLoadRootMargin) :
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_LAZY_LOAD_ROOT_MARGIN_INPUT.getValue(), "500") &&
                helpersInit.getBaseHelper().checkDataset(GPT_DISPLAY_ON_THE_SUBWIDGETS_CHECKBOX.isSelected(), gptDisplayOnSubwidgetsIsEnable);
    }

    public boolean checkAllGptSettingsIsEmpty() {
        return helpersInit.getBaseHelper().checkDatasetEquals(GPT_SLOT_INPUT.getValue(), "") &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_DIV_ID_INPUT.getValue(), "") &&
                helpersInit.getBaseHelper().checkDatasetEquals(GPT_AD_UNIT_ID_INPUT.getValue(), "");
    }

    public boolean isDisplayedGptIntegrations(){
        return GPT_INTEGRATION_CHECKBOX.isDisplayed();
    }
}
