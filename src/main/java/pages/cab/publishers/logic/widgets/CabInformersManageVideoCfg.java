package pages.cab.publishers.logic.widgets;

import org.json.simple.JSONObject;


import static core.helpers.BaseHelper.stringToJson;
import static pages.cab.publishers.locators.widgets.VideoCfgLocators.VIDEO_CFG_JSON;

public class CabInformersManageVideoCfg {

    public CabInformersManageVideoCfg() {

    }

    public JSONObject getVideoCfgJson() {
        return stringToJson(VIDEO_CFG_JSON.getOwnText());
    }
}
