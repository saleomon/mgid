package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import core.base.HelpersInit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.ClickOptions.usingJavaScript;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static pages.cab.publishers.locators.widgets.ExchangeWidgetLocators.*;
import static core.helpers.BaseHelper.*;

public class CabExchangeSettings {
    private final HelpersInit helpersInit;
    private final Logger log;

    public CabExchangeSettings(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * is selected checkbox 'Do not include this widget clicks for news CTR calculation'
     */
    public boolean noCalcNewsCtrIsSelected() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(NO_CALC_NEWS_CTR.shouldBe(visible).isSelected());
    }

    /**
     * @param type 0 - No; 1 - Yes
     */
    public void chooseCategoriesFilterType(String type) {
        CATEGORIES_FILTER_RADIOBUTTONS.hover().selectRadio(type);
    }

    /**
     *  Save widget in CAB
     */
    public void saveWidgetSettings() {
        clickInvisibleElementJs(SAVE_BUTTON.shouldBe(visible));
        waitForAjaxLoader();
        waitForAjax();
    }

    /**
     *  Check Categories filter after saving
     */
    public boolean checkCategoriesFilterTypeValue(String radioButtonValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CATEGORIES_FILTER_VALUES.findBy(value(radioButtonValue)).shouldBe(visible).isSelected());
    }

    /**
     * Input category name in the search field
     * @param category name
     */
    public void setCategoryToMultiFilter(String category) {
        CATEGORY_FILTER_SEARCH_INPUT.scrollTo().sendKeys(category);
        waitForAjax();
        sleep(1000);
    }

    public boolean checkSearchCategoryFilter() {
        helpersInit.getMultiFilterHelper().expandMultiFilter("categoriesBox");
        ElementsCollection categoryList = $$x(CATEGORY_FILTER_TITLES);
        String category = helpersInit.getBaseHelper().getTextAndWriteLog(categoryList.get(randomNumbersInt(categoryList.size())).text());
        setCategoryToMultiFilter(category);
        return $$x(CATEGORY_FILTER_TITLES_AFTER_SEARCH)
                .texts().stream().allMatch(i -> i.toLowerCase().contains(category.toLowerCase()) || i.equalsIgnoreCase(category));
    }

    public void clearAllSelectedFilters(ElementsCollection collection){
        log.info("clear all category in filter");
        for(SelenideElement element: collection){
            element.shouldBe(visible).click();
        }
        log.info("clear all category success");
    }

    /**
     * @param baseElementId
     * @return All selected checkboxes
     */
    public ElementsCollection getAllSelectedCheckboxesInMultiFilter(String baseElementId) {
        return $$("#" + baseElementId + " .fancytree-selected .fancytree-checkbox");
    }

    public void clickAdTypeOrLandingTypeForCategory(String categoryName, boolean isAdType){
        // get category id
        String category = $x(".//li[*[*[text()=\"" + categoryName + "\"]]]").attr("id");
        category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);
        //click category icon (лейка) and checkbox
        $("#ft_news-" + category + " .fancytree-icon").click();
        $("#ft_news-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

        $$("#ft_news-" + category + " ." + (isAdType ? "ad":"landing") + "_types [data-type]").get(1).click(usingJavaScript());
    }


    public JSONArray chooseCustomAdTypeAndLandingForCategoryInMultiFilterAndGetTheirData(int countValue){
        JSONArray main = new JSONArray();
        ArrayList<String> adTypes = new ArrayList<>();
        ArrayList<String> landingTypes = new ArrayList<>();

        log.info("Get collection from all elements");
        ElementsCollection list = $$(CATEGORY_FILTER_ELEMENTS);
        ArrayList<Integer> listCategoryId = helpersInit.getBaseHelper().getRandomCountValue(list.size() - 1, countValue);

        log.info("Choose random categories and add to massive");
        for (Integer integer : listCategoryId) {
            // get category id
            String category = list.get(integer).parent().attr("id");
            category = Objects.requireNonNull(category).substring(category.lastIndexOf("-") + 1);

            //click category icon (лейка) and checkbox
            $("#ft_news-" + category + " .fancytree-checkbox").scrollTo().click();
            $("#ft_news-" + category + " .fancytree-icon").click();
            $("#ft_news-" + category + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

            //get random ad_types
            ElementsCollection adTypesList = $$("#ft_news-" + category + " .ad_types [data-type]");
            ArrayList<Integer> adTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(adTypesList.size() - 1, 3);

            // clear all checked adTypes
            for(SelenideElement s : $$("#ft_news-" + category + " .ad_types [data-type]:checked"))
            {
                s.shouldBe(attribute("selected"));
                s.click(usingJavaScript());
                s.shouldNotBe(attribute("selected"));
            }

            for (int at : adTypesChosen) {
                // click new adTypes
                adTypesList.get(at).click(usingJavaScript());
                adTypes.add(adTypesList.get(at).attr("data-type"));
            }

            //get random landing_types
            ElementsCollection landingTypesList = $$("#ft_news-" + category + " .landing_types [data-type]");
            ArrayList<Integer> landingTypesChosen = helpersInit.getBaseHelper().getRandomCountValue(landingTypesList.size() - 1, 4);

            // clear all checked landingTypes
            for(SelenideElement s : $$("#ft_news-" + category + " .landing_types [data-type]:checked"))
            {
                s.shouldBe(attribute("selected"));
                s.click(usingJavaScript());
                s.shouldNotBe(attribute("selected"));
            }

            for (int at : landingTypesChosen) {
                // click new landingTypes
                landingTypesList.get(at).click(usingJavaScript());
                landingTypes.add(landingTypesList.get(at).attr("data-type"));
            }

            //write data to json
            JSONObject temp = new JSONObject();
            temp.put("category", category);
            temp.put("adType", new Gson().toJson(adTypes));
            temp.put("landingTypes", new Gson().toJson(landingTypes));

            main.add(temp);

            adTypes.clear();
            landingTypes.clear();
        }
        return main;
    }

    public boolean checkAdTypesAndLandingTypesForCategory(JSONArray obj) {
        String categoryId;
        ArrayList<String> adTypes;
        ArrayList<String> landingTypes;

        int count = 0;

        for (Object j : obj) {
            // get category, ad_types, landing_types from json
            categoryId = ((JSONObject) j).get("category").toString();
            adTypes = new Gson().fromJson(((JSONObject) j).get("adType").toString(), new TypeToken<List<String>>() {
            }.getType());
            landingTypes = new Gson().fromJson(((JSONObject) j).get("landingTypes").toString(), new TypeToken<List<String>>() {
            }.getType());

            log.info("Check chosen category");
            if (helpersInit.getBaseHelper().checkDatasetContains($("[id^='ft_news-" + categoryId + "']>span").attr("class"), "fancytree-selected")) {
                //click category icon (лейка) and checkbox
                $("#ft_news-" + categoryId + " .fancytree-icon").scrollTo().click();
                $("#ft_news-" + categoryId + " .fancytree-icon").shouldBe(attributeMatching("class", ".+active"));

                List<String> adTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_news-" + categoryId + " .ad_types [data-type][checked='checked']")){
                    adTypes_list.add(s.attr("data-type"));
                }

                List<String> landingTypes_list = new ArrayList<>();
                for(SelenideElement s : $$("#ft_news-" + categoryId + " .landing_types [data-type][checked='checked']")){
                    landingTypes_list.add(s.attr("data-type"));
                }

                log.info("Check chosen adTypes and landingTypes");
                if (new HashSet<>(adTypes_list).containsAll(adTypes) &
                        new HashSet<>(landingTypes_list).containsAll(landingTypes)) {
                    count++;
                }
                else {
                    log.info("adTypes: " + adTypes);
                }
                CATEGORY_FILTER_SEARCH_INPUT.click();
            }
            else {
                log.info("categoryId: " + categoryId);
            }
        }
        return obj.size() > 0 &&
                obj.size() == count;
    }

    public void switchOriginalTitlesOnlyCheckbox(boolean state) {
        markUnMarkCheckbox(state, ORIGINAL_TITLES_ONLY_CHECKBOX);
    }

    public boolean checkOriginalTitlesOnlyCheckbox(boolean state) {
        return checkIsCheckboxSelected(state, ORIGINAL_TITLES_ONLY_CHECKBOX);
    }
}

