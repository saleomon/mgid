package pages.cab.publishers.logic.widgets;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabInformersPreviewMomentary {

    private final SelenideElement REGION_SELECT = $("#region_chzn");
    private final SelenideElement REGION_SEARCH = $(".chzn-search>input");

    public CabInformersPreviewMomentary() {
    }

    public CabInformersPreviewMomentary regionSelectOpen(){
        REGION_SELECT.click();
        checkErrors();
        return this;
    }

    public void fillRegionSearch(String region){ REGION_SEARCH.sendKeys(region + Keys.ENTER);}
}
