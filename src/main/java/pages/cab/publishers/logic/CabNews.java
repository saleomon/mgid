package pages.cab.publishers.logic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.NewsAddHelper;
import pages.cab.publishers.helpers.NewsListHelper;
import core.helpers.BaseHelper;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static java.time.Duration.ofSeconds;
import static pages.cab.publishers.locators.NewsAddLocators.*;
import static pages.cab.publishers.locators.NewsListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CabNews {
    private final HelpersInit helpersInit;
    private final Logger log;
    NewsAddHelper newsAddHelper;
    NewsListHelper newsListHelper;

    public CabNews(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        newsAddHelper = new NewsAddHelper(log, helpersInit);
        newsListHelper = new NewsListHelper(log, helpersInit);
    }

    String newsId;
    String categoryId;
    String domain;
    boolean stateOfOriginalTitle = false;
    String title;
    String description;
    String imageLink = "https://img1.goodfon.ru/wallpaper/big/1/ca/foto-makro-kartinka-zelenye.jpg";
    String lifeTime;
    String newsType;
    String politicalPreferences;
    Statuses status;

    public CabNews isGenerateNewValues(boolean generateNewValues) {
        newsAddHelper.setGenerateNewValues(generateNewValues);
        return this;
    }

    public CabNews setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public CabNews setIsOriginalTitle(boolean state) {
        this.stateOfOriginalTitle = state;
        return this;
    }

    public CabNews setTitle(String title) {
        this.title = title;
        return this;
    }

    public CabNews setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getNewsId() {
        return newsId;
    }

    public CabNews setStatus(Statuses status) {
        this.status = status;
        return this;
    }

    public enum Statuses {
        PENDING("In quarantine"),
        ACTIVE("Active"),
        DRAFT("In Drafts"),
        QUARANTINE("In quarantine");

        private final String statusesValue;

        Statuses(String statuses) {
            this.statusesValue = statuses;
        }

        public String getStatusesValue() {
            return statusesValue;
        }
    }

    public enum MassAction {
        MASS_ACTION_DELETE_NEWS_IN_BIN("delete");

        private final String value;

        MassAction(String massActionValue) {
            this.value = massActionValue;
        }

        public String getTypeValue() {
            return value;
        }
    }

    /**
     * create news
     */
    public String createNews() {
        newsAddHelper.loadImageByUrl(imageLink);
        categoryId = newsAddHelper.chooseCategory(categoryId);
        newsAddHelper.setDomain(domain);
        newsAddHelper.isOriginalTitle(stateOfOriginalTitle);
        title = newsAddHelper.setTitle(title);
        description = newsAddHelper.setDescription(description);
        lifeTime = newsAddHelper.chooseLifeTime(lifeTime);
        newsType = newsAddHelper.chooseNewsType(newsType);
        politicalPreferences = newsAddHelper.choosePoliticalPreferences(politicalPreferences);

        newsAddHelper.saveNews();
        newsId = readNewsId();
        return newsId;
    }

    /**
     * edit news
     */
    public void editNews() {
        newsAddHelper.loadImageByUrl(imageLink);
        categoryId = newsAddHelper.chooseCategory(categoryId);
        newsAddHelper.setDomain(domain);
        newsAddHelper.isOriginalTitle(stateOfOriginalTitle);
        title = newsAddHelper.setTitle(title);
        description = newsAddHelper.setDescription(description);
        lifeTime = newsAddHelper.chooseLifeTime(lifeTime);
        politicalPreferences = newsAddHelper.choosePoliticalPreferences(politicalPreferences);

        newsAddHelper.saveNews();
    }

    /**
     * check news base settings in 'List interface'
     */
    public boolean checkNewsInListInterface() {
        return newsListHelper.checkCategory(categoryId) &&
                newsListHelper.checkDomain(domain) &&
                newsListHelper.checkTitle(title) &&
                newsListHelper.checkOriginalTitleIcon(stateOfOriginalTitle) &&
                newsListHelper.checkDescription(description) &&
                newsListHelper.checkLifeTime(lifeTime) &&
                newsListHelper.checkNewsType(newsType) &&
                newsListHelper.checkPoliticalPreferences(politicalPreferences) &&
                newsListHelper.checkStatusNews(status);
    }

    /**
     * check news base settings in 'Edit interface'
     */
    public boolean checkNewsInEditInterface() {
        return newsAddHelper.checkCategory(categoryId) &&
                newsAddHelper.checkDomain(domain) &&
                newsAddHelper.checkTitle(title) &&
                newsAddHelper.checkOriginalTitle(stateOfOriginalTitle) &&
                newsAddHelper.checkDescription(description) &&
                newsAddHelper.checkLifeTime(lifeTime) &&
                newsAddHelper.checkPoliticalPreferences(politicalPreferences);
    }

    /**
     * get 'News ID'
     */
    public String readNewsId() {
        String val;
        if ((val = helpersInit.getMessageHelper().getCustomMessageValueCab("News id")) != null) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(val.substring(val.indexOf("News id ") + 8, val.indexOf(" has")));
        }
        return null;
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public void openCuratorFilterAndSearch(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator, "hover");
        newsListHelper.submitFilter();
    }

    /**
     * get 'Curator' (manager)
     */
    public String getCurator() {
        return newsAddHelper.getCurator();
    }

    /**
     * get news id from label
     */
    public List<String> getNewsIdFromLabel() {
        return newsListHelper.getNewsIdFromLabel();
    }

    /**
     * Move news to recycle bin
     */
    public void deleteNews(int newsId) {
        newsListHelper.clickDeleteIconNews(newsId);
        newsListHelper.setDeleteReasonAndSubmit();
    }

    /**
     * Restore news from recycle bin
     */
    public void restoreNews(){
        helpersInit.getBaseHelper().refreshCurrentPage();
        RESTORE_NEWS_ICON.shouldBe(visible).click();
        waitForAjax();
        SUBMIT_BUTTON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    /**
     * click on mass action checkbox
     */
    public void clickOnMassActionCheckbox(Integer...newsIds){
        Stream.of(newsIds).forEach(elem -> $("[class='news'][value='" + elem + "']").click());
    }

    /**
     * restore news
     */
    public void restoreEachNews(Integer...newsIds){
        Stream.of(newsIds).forEach(elem -> {
            $("[id='newsRestoreAction_" + elem + "']").click();
            waitForAjax();
            SUBMIT_BUTTON.shouldBe(visible).click();
            waitForAjax();
            checkErrors();
        });
    }


    /**
     * choose mass actions
     */
    public void chooseMassActions(CabNews.MassAction action) {
        log.info("Choose mass actions");
        MASS_ACTION_SELECT_LIST.selectOptionByValue(action.getTypeValue());
        waitForAjax();
    }

    /**
     * Set reason for news deletion
     */
    public void setDeleteReason(){
        MASS_DELETE_REASON_INPUT.sendKeys("Remove news in mass action");
        log.info("Check hide on tranz page checkbox");
        HIDE_ON_TRANZ_PAGE_MASS_ACTION.shouldBe(visible).click();
    }

    /**
     * Execute button click in mass actions
     */
    public void submitMassActions(){
        log.info("Submit mass action");
        MASS_ACTION_EXECUTE_BUTTON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }


    /**
     * Delete news permanently
     */
    public void deleteNewsPermanently(int newsId) {
        newsListHelper.clickDeleteIconNews(newsId);
        DELETE_TYPE_RADIO.shouldBe(visible).selectRadio("2");
        newsListHelper.setDeleteReasonAndSubmit();
    }

    /**
     * Delete news with hiding on transit page
     */
    public void deleteNewsWithHideOnTranzPage(int newsId){
        newsListHelper.clickDeleteIconNews(newsId);
        HIDE_ON_TRANZ_PAGE.shouldBe(visible).click();
        newsListHelper.setDeleteReasonAndSubmit();
    }

    /**
     * Check absent flag in delete news pop-up
     */
    public boolean tranzPageNotDisplayedFlagInDeletePopup(int newsId){
        newsListHelper.clickDeleteIconNews(newsId);
        return HIDE_ON_TRANZ_PAGE.isDisplayed();
    }

    /**
     * Check absent flag mass action news deletion
     */
    public boolean tranzPageNotDisplayedFlagInMassAction(){
        return HIDE_ON_TRANZ_PAGE_MASS_ACTION.isDisplayed();
    }

    /**
     * Edit content group inline
     */
    public void changeContentGroupInline(String newsId) {
        $(String.format(CONTENT_GROUP_ICON, newsId)).click();
        SelenideElement contentTypeSelect = $(String.format(CONTENT_GROUP_SELECT, newsId));
        ElementsCollection options = $(String.format(CONTENT_GROUP_SELECT, newsId)).findAll("option");
        List<SelenideElement> validOptions = options.filter(Condition.not(attribute("value", Objects.requireNonNull(contentTypeSelect.getSelectedValue()))));

        contentTypeSelect.selectOptionByValue(
                validOptions.get(randomNumbersInt(validOptions.size())).val()
        );
        SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * Edit category inline
     */
    public void editCategoryInline() {
        newsListHelper.editCategoryInline();
    }

    /**
     * Edit lifetime inline
     */
    public void editLifetimeInline() {
        newsListHelper.editLifetimeInline();
    }

    /**
     * Edit news type inline
     */
    public void editNewsTypeInline() {
        newsListHelper.chooseNewsTypeInline();
    }

    /**
     * Edit url inline
     */
    public void editUrlInline() {
        newsListHelper.editUrlInline();
    }

    /**
     * Try edit description inline
     */
    public void tryEditDescriptionInline(String value) {
        DESCRIPTION_INLINE_LABEL.shouldBe(visible).doubleClick();
        log.info("description clicked");
        DESCRIPTION_INLINE_INPUT.shouldBe(visible)
                .setValue(value + " " + BaseHelper.getRandomWord(10)).pressEnter();
    }

    /**
     * Check description
     */
    public boolean checkDescription(String value) {
        return newsListHelper.checkDescription(value);
    }

    /**
     * Cancel news moderation
     */
    public void cancelNewsModeration() {
        CANCEL_MODERATION_ICON.shouldBe(visible).click();
        checkErrors();
    }

    /**
     * Send news on moderation
     */
    public void resendNewsOnModeration() {
        SEND_ON_MODERATION_ICON.click();
        checkErrors();
    }

    /**
     * Check cancel moderation icon is hidden
     */
    public boolean checkCancelModerationIconIsHidden() {
        return CANCEL_MODERATION_ICON.shouldBe(Condition.hidden).isDisplayed();
    }

    /**
     * Check send on moderation icon is hidden
     */
    public boolean checkSendOnModerationIconIsHidden() {
        return SEND_ON_MODERATION_ICON.shouldBe(Condition.hidden).isDisplayed();
    }

    /**
     * Check news status
     */
    public boolean checkNewsStatus(Statuses statusValue) {
        return newsListHelper.checkStatusNews(statusValue);
    }

    /**
     * Check news list is empty
     */
    public boolean checkNewsListIsEmpty() {
        return EMPTY_TABLE_ROW.shouldBe(visible).isDisplayed();
    }

    /**
     * Check edit news icon visibility
     */
    public boolean checkEditNewsIcon() {
        return newsListHelper.isDisplayedNewsEditButton();
    }

    /**
     * Check can't edit inline image
     */
    public boolean checkCantEditImageInline() {
        return newsListHelper.checkCantEditImageInline();
    }

    /**
     * Check can't edit inline title
     */
    public boolean checkCantEditTitleInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(TITLE_INLINE_LABEL, TITLE_INLINE_INPUT);
    }

    /**
     * Check can't edit inline description
     */
    public boolean checkCantEditDescriptionInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(DESCRIPTION_INLINE_LABEL, DESCRIPTION_INLINE_INPUT);
    }

    /**
     * Check can't edit inline category
     */
    public boolean checkCantEditCategoryInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(CATEGORY_INLINE_FIELD, CATEGORY_INLINE_SELECT);
    }

    /**
     * Check can't edit inline lifetime
     */
    public boolean checkCantEditLifetimeInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(LIFETIME_INLINE_LABEL, LIFETIME_INLINE_SELECT);
    }

    /**
     * Check can't edit inline news type
     */
    public boolean checkCantEditNewsTypeInline() {
        return newsListHelper.checkCantEditNewsTypeInline();
    }

    /**
     * Check can't edit inline landing
     */
    public boolean checkCantEditLandingInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(LANDING_TYPES_INLINE_LABEL, LANDING_INLINE_TYPES_SELECT);
    }


    /**
     * Try edit URL inline
     */
    public void tryEditUrlInline(String value) {
        URL_INLINE_LABEL.shouldBe(visible).scrollTo().doubleClick();
        log.info("Url label clicked");
        $(URL_INLINE_INPUT).shouldBe(visible)
                .setValue(value + "/" + BaseHelper.getRandomWord(5)).pressEnter();
    }

    /**
     * Just save news without changing
     */
    public void saveNews() {
        newsAddHelper.saveNews();
    }

    /**
     * check news Title and Description with '&nbsp' in 'List interface'
     */
    public boolean checkNewsWithNbspInListInterface(String stringValue) {
        return newsListHelper.checkTitle(stringValue) &&
                newsListHelper.checkDescription(stringValue);
    }

    /**
     * check news Title and Description with '&nbsp' in 'Edit interface'
     */
    public boolean checkNewsWithNbspInEditInterface(String stringValue) {
        return newsAddHelper.checkTitle(stringValue) &&
                newsAddHelper.checkDescription(stringValue);
    }

    /**
     * Edit title inline
     */
    public void editTitleInline(String value) {
        TITLE_INLINE_LABEL.shouldBe(visible).doubleClick();
        log.info("title was clicked");
        CLEAR_FIELD_BUTTON.click();
        TITLE_INLINE_INPUT.sendKeys(value + Keys.ENTER);
        TITLE_INLINE_INPUT.shouldBe(Condition.hidden);
    }

    /**
     * Edit description inline
     */
    public void editDescriptionInline(String value) {
        DESCRIPTION_INLINE_LABEL.shouldBe(visible).doubleClick();
        log.info("description was clicked");
        CLEAR_FIELD_BUTTON.click();
        DESCRIPTION_INLINE_INPUT.sendKeys(value + Keys.ENTER);
        DESCRIPTION_INLINE_INPUT.shouldBe(Condition.hidden);
    }

    public String getTitleInline() {
        return TITLE_INLINE_LABEL.getText();
    }

    public String getDescriptionInline() {
        return DESCRIPTION_INLINE_LABEL.getText();
    }

    public void changeStateOriginalTitleIcon(boolean state) {
        if(state && $$(NOT_ORIGINAL_TITLE_ICON).first().isDisplayed()) {
            $$(NOT_ORIGINAL_TITLE_ICON).first().click();
        } else if(!state && ORIGINAL_TITLE_ICON.first().isDisplayed()) {
            ORIGINAL_TITLE_ICON.first().click();
        }
        waitForAjax();
    }

    public boolean checkStateOriginalTitleIcon(boolean state) {
        return state ? ORIGINAL_TITLE_ICON.first().isDisplayed() : $$(NOT_ORIGINAL_TITLE_ICON).first().isDisplayed();
    }

    public boolean isDisplayedOriginalTitleIcon() {
        return ORIGINAL_TITLE_ICON.first().isDisplayed() || $$(NOT_ORIGINAL_TITLE_ICON).first().isDisplayed();
    }

    public boolean isDisplayedOriginalTitleCheckbox() {
        return IS_ORIGINAL_TITLE_CHECKBOX.isDisplayed();
    }

    public void filterOriginalTitle(String value) {
        ORIGINAL_TITLE_FILTER.selectOptionByValue(value);
        FILTER_BUTTON.click();
        checkErrors();
    }

    public int getAmountOfNews() {
        return NEWS_INFO_BLOCK.size();
    }

    public int getAmountOfNewsWithOriginalTitle() {
        return ORIGINAL_TITLE_ICON.size();
    }

    public int getAmountOfNewsWithoutOriginalTitle() {
        return $$(NOT_ORIGINAL_TITLE_ICON).size();
    }

    public CabNews fillDomain(String domain) {
        newsAddHelper.setDomain(domain);
        return this;
    }

    public CabNews fillTitle(String title) {
        newsAddHelper.setTitle(title);
        return this;
    }

    public CabNews fillAdvertText(String description) {
        newsAddHelper.setDescription(description);
        return this;
    }

    public void fillImageField(String imageLink) {
        IMAGE_INPUT.sendKeys(imageLink);
    }

    public String getDomainValue() {
        return LINK_INPUT.val();
    }

    public String getTitleValue() {
        return TITLE_INPUT.val();
    }

    public String getAdvertValue() {
        return DESCRIPTION_INPUT.val();
    }

    public String getImageLinkValue() {
        return IMAGE_INPUT.val();
    }

    public String getImageLinkInlineValue() {
        return IMAGE_INPUT_FIELD_INLINE.val();
    }

    public int getAmountResetButtons() {
        return RESET_DATA_BUTTONS.size();
    }

    public void resetAllFieldsByResetButtons() {
        for(int i = RESET_DATA_BUTTONS.size()-1; i >= 0; i--){
            if(i == 0) RESET_DATA_BUTTONS.get(i).scrollTo().hover().click();
            RESET_DATA_BUTTONS.get(i).shouldBe(visible).click();
            waitForAjax();
        }
    }

    public void resetImageFieldByResetButton() {
        RESET_DATA_BUTTONS.first().click();
    }

    public void openPopupEditImageInline() {
        IMAGE_POPUP_LOCATOR.doubleClick();
        waitForAjax();
    }

    public void loadImageInline(String image) {
        IMAGE_INPUT_INLINE.sendKeys( LINK_TO_RESOURCES_IMAGES +  image);
        waitForAjax();
        SAVE_IMAGE_INLINE.click();
        waitForAjax();
        SAVE_IMAGE_INLINE.shouldBe(hidden);
    }

    public void chooseCategory() {
        newsAddHelper.chooseCategory(categoryId);
    }

    public boolean checkVisibilityIconOfStatusForComparisonImages(String icon) {
        return $("img[src*='" + icon + "']").isDisplayed();
    }

    public boolean waitVisibilityIconOfStatusForComparisonImages(String icon) {
        $("img[src*='" + icon + "']").shouldBe(visible, ofSeconds(8));
        return true;
    }

    public String getlinkOfComparisonImagesIcon(String icon) {
        return LINK_TO_RELATED_IMAGES.attr("href");
    }

    public void filterByRelatedImages(String id) {
        RELATED_IMAGES_FILTER.sendKeys(id);
        FILTER_BUTTON.click();
        checkErrors();
    }
}
