package pages.cab.publishers.logic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.publishers.helpers.NewsModerationHelper;

import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.publishers.locators.NewsModerationLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.AuthUserCabData.AuthorizationUsers.PRIVILEGE_USER;

public class CabNewsModeration {

    //variables
    private final HelpersInit helpersInit;
    private final Logger log;
    NewsModerationHelper moderationHelper;
    String adType;


    //enum
    public enum ModerationFlowStatuses {
        OPEN("open"),
        IN_WORK("in_work"),
        IN_WORK_FOR_OTHER("in_work_for_other"),
        APPROVE("approve"),
        REJECT("reject");

        private final String moderationVal;

        ModerationFlowStatuses(String moderationVal) {
            this.moderationVal = moderationVal;
        }

        public String getModerationVal() {
            return moderationVal;
        }
    }

    public enum MassActions {
        MODERATION_START("moderationStart"),
        MODERATION_STOP("moderationStop"),
        ACCEPT("accept"),
        REJECT("reject"),
        DELETE("delete"),
        CATEGORY("changeCategory"),
        LIFETIME("changeLivetime"),
        TYPE("adType"),
        LANDING("changeLandingType"),
        POLITICAL_PREFERENCES("contentGroup");

        private final String massActionVal;

        MassActions(String massActionVal) {
            this.massActionVal = massActionVal;
        }

        public String getMassActionVal() {
            return massActionVal;
        }
    }


    public CabNewsModeration(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        moderationHelper = new NewsModerationHelper(log, helpersInit);
    }

    public String getAdType() {
        return adType;
    }

    /**
     * choose 'AdType' in mass action
     */
    public String editAdTypeMass() {
        adType = moderationHelper.editFieldMass(MassActions.TYPE, MASS_AD_TYPE_SELECT);
        return adType;
    }

    /**
     * check adType for all teasers on page
     */
    public boolean checkAdTypeForAllTeasersOnPage(String adType) {
        return moderationHelper.checkAdTypeForAllTeasersOnPage(adType);
    }

    /**
     * choose random AdType inline
     */
    public void editAdTypeInline() {
        adType = moderationHelper.chooseAdTypeInline();
    }

    /**
     * approve news
     */
    public void approveNews(int newsId) {
        moderationHelper.approveNews(newsId);
        waitForAjaxLoader();
    }

    /**
     * approve news mass
     */
    public void approveNewsMass(int newsId) {
        moderationHelper.approveNewsMass(newsId);
    }

    public void rejectNews(int newsId) {
        moderationHelper.clickRejectIconNews(newsId);
        moderationHelper.setRejectReasonAndSubmit();
    }

    /**
     * reject news mass
     */
    public void rejectNewsMass(int newsId) {
        moderationHelper.rejectNewsMass(newsId);
    }

    /**
     * delete news
     */
    public void deleteNews(int newsId) {
        moderationHelper.clickDeleteIconNews(newsId);
        moderationHelper.setDeleteReasonAndSubmit(newsId);
    }

    /**
     * delete news mass
     */
    public void deleteNewsMass(int newsId) {
        moderationHelper.deleteNewsMass(newsId);
    }

    ////////////////////////////////////////// start-stop moderation flow - START /////////////////////////////////////////////////////////

    /**
     * click 'Start' moderation icon
     */
    public void clickStartModerationIcon(int id) {
        waitForAjax();
        moderationHelper.clickStartModerationIcon(id);
    }

    /**
     * choose 'moderationStart' in mass action
     */
    public void startModerationMass(int... ids) {
        moderationHelper.startModerationMass(ids);
    }

    /**
     * click 'Stop' moderation icon
     */
    public void clickStopModerationIcon(int id) {
        moderationHelper.clickStopModerationIcon(id);
    }

    /**
     * choose 'moderationStop' in mass action
     */
    public void stopModerationMass(int... ids) {
        moderationHelper.stopModerationMass(ids);
    }

    /**
     * check news moderation FLOW(start-stop) statuses
     */
    public boolean checkNewsModerationStatus(ModerationFlowStatuses status, int... ids) {
        int count = 0;
        SelenideElement data_id;

        for (int id : ids) {
            data_id = moderationHelper.getDataNewsId(id);
            switch (status) {

                case OPEN:
                    if (data_id.$(START_MODERATION_ICON).isDisplayed() &&
                            !data_id.$(ACTIONS_BLOCK).isDisplayed()) count++;
                    break;

                case IN_WORK:
                    if (data_id.shouldBe(visible).$(STOP_MODERATION_ICON).isDisplayed() &&
                            data_id.shouldBe(visible).$(ACTIONS_BLOCK).isDisplayed()) count++;
                    break;

                case IN_WORK_FOR_OTHER:
                    if (!data_id.$(ACTIONS_BLOCK).isDisplayed() &
                            data_id.$(STOP_MODERATION_ICON).isDisplayed() &
                            helpersInit.getBaseHelper().checkDatasetEquals(convertRgbaToHex(data_id.$(MODERATION_PROCESS_STYLE).shouldBe(exist).getCssValue("background-color")), "#FFC7CA") &
                            data_id.$(MODERATION_USER_LABEL).isDisplayed() &&
                            helpersInit.getBaseHelper().checkDatasetEquals(data_id.$(MODERATION_USER_LABEL).text(), PRIVILEGE_USER.getLoginUser()))
                        count++;
                    break;

                case APPROVE:
                    if (!data_id.$(ACTIONS_BLOCK).isDisplayed() &&
                            !data_id.$(STOP_MODERATION_ICON).isDisplayed() &&
                            !data_id.$(START_MODERATION_ICON).isDisplayed() &&
                            data_id.$(MODERATION_USER_LABEL).isDisplayed() &&
                            helpersInit.getBaseHelper().checkDatasetEquals(data_id.$(MODERATION_USER_LABEL).text(), PRIVILEGE_USER.getLoginUser()) &&
                            helpersInit.getBaseHelper().checkDatasetEquals(convertRgbaToHex(data_id.getCssValue("background-color")), "#afa"))
                        count++;
                    break;

                case REJECT:
                    if (!data_id.$(ACTIONS_BLOCK).isDisplayed() &&
                            !data_id.$(STOP_MODERATION_ICON).isDisplayed() &&
                            !data_id.$(START_MODERATION_ICON).isDisplayed() &&
                            data_id.$(MODERATION_USER_LABEL).isDisplayed() &&
                            helpersInit.getBaseHelper().checkDatasetEquals(data_id.$(MODERATION_USER_LABEL).text(), PRIVILEGE_USER.getLoginUser()) &&
                            helpersInit.getBaseHelper().checkDatasetEquals(convertRgbaToHex(data_id.getCssValue("background-color")), "#FFBF63"))
                        count++;
                    break;

            }
        }

        return helpersInit.getBaseHelper().checkDataset(count, ids.length);
    }

    /**
     * check Stop Moderation for Other User And Show Dialog And Close Him
     */
    public String checkStopModerationDialogAndCloseHim() {
        return moderationHelper.checkStopModerationOtherUserAndShowDialogAndCloseHim();
    }

    /**
     * check filter 'Moderation status' has selected value 'in_work' (Taken to work)
     */
    public boolean moderationStatusFilterHasCustomSelectedValue(ModerationFlowStatuses status) {
        return helpersInit.getBaseHelper().checkDatasetEquals(FILTER_MODERATION_STATUS_SELECT.getSelectedValue(), status.getModerationVal());
    }

    ////////////////////////////////////////// start-stop moderation flow - FINISH /////////////////////////////////////////////////////////

    /**
     * check can't edit inline title
     */
    public boolean checkCantEditTitleInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_TITLE_LABEL, INLINE_TITLE_INPUT);
    }

    /**
     * edit inline title
     */
    public String editTitleInline() {
        return moderationHelper.editTitleInline();
    }

    public String readTitle() {
        return INLINE_TITLE_LABEL.shouldBe(visible).text();
    }

    /**
     * check can't edit inline description
     */
    public boolean checkCantEditDescriptionInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_DESCRIPTION_LABEL, INLINE_DESCRIPTION_INPUT);
    }

    /**
     * edit inline description
     */
    public String editDescriptionInline() {
        return moderationHelper.editDescriptionInline();
    }

    public String readDescription() {
        return INLINE_DESCRIPTION_LABEL.shouldBe(visible).text();
    }

    /**
     * check can't edit inline category
     */
    public boolean checkCantEditCategoryInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_CATEGORY_LABEL, INLINE_CATEGORY_SELECT);
    }

    /**
     * edit inline category
     */
    public String editCategoryInline() {
        return moderationHelper.editCategoryInline();
    }

    public String editCategoryMass() {
        return moderationHelper.editFieldMass(MassActions.CATEGORY, MASS_CATEGORY_SELECT);
    }

    public String readCategory() {
        return INLINE_CATEGORY_LABEL.shouldBe(visible).text();
    }

    /**
     * check category value for all news on page
     */
    public boolean checkCategoryValueForAllNewsOnPage(String value) {
        return moderationHelper.checkCustomValueForAllNewsOnPage(INLINE_CATEGORY, value);
    }

    /**
     * check can't edit inline lifetime
     */
    public boolean checkCantEditLifetimeInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_LIFETIME_LABEL, INLINE_LIFETIME_SELECT);
    }

    /**
     * edit inline lifetime
     */
    public String editLifetimeInline() {
        return moderationHelper.editLifetimeInline();
    }

    /**
     * edit mass lifetime
     */
    public String editLifetimeMass() {
        return moderationHelper.editFieldMass(MassActions.LIFETIME, MASS_LIFETIME_SELECT);
    }

    public String readLifetime() {
        return INLINE_LIFETIME_LABEL.shouldBe(visible).attr("data-value");
    }

    /**
     * check Lifetime value for all news on page
     */
    public boolean checkLifetimeValueForAllNewsOnPage(String value) {
        return moderationHelper.checkCustomValueForAllNewsOnPage(INLINE_LIFETIME, value);
    }

    /**
     * check can't edit inline adType
     */
    public boolean checkCantEditAdTypeInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_AD_TYPE_LINK, INLINE_AD_TYPE_SELECT);
    }

    public String readAdType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($(INLINE_AD_TYPE).shouldBe(visible).text().toLowerCase());
    }

    /**
     * check can't edit inline landing
     */
    public boolean checkCantEditLandingInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_LANDING_LABEL, INLINE_LANDING_SELECT);
    }

    /**
     * edit inline landing
     */
    public String editLandingInline() {
        return moderationHelper.editLandingInline();
    }

    /**
     * edit mass landing
     */
    public String editLandingMass() {
        return moderationHelper.editFieldMass(MassActions.LANDING, MASS_LANDING_SELECT);
    }

    public String readLanding() {
        return INLINE_LANDING_LABEL.shouldBe(visible).attr("data-value");
    }

    /**
     * check Landing value for all news on page
     */
    public boolean checkLandingValueForAllNewsOnPage(String value) {
        return moderationHelper.checkCustomValueForAllNewsOnPage(INLINE_LANDING, value);
    }

    /**
     * edit mass Political Preferences
     */
    public String editPoliticalPreferencesMass() {
        return moderationHelper.editFieldMass(MassActions.POLITICAL_PREFERENCES, MASS_POLITICAL_PREFERENCES_SELECT);
    }

    /**
     * check Political Preferences value for all news on page
     */
    public boolean checkPoliticalPreferencesValueForAllNewsOnPage(String value) {
        return moderationHelper.checkCustomValueForAllNewsOnPage(INLINE_POLITICAL_PREFERENCES, value);
    }

    /**
     * check can't edit inline url
     */
    public boolean checkCantEditUrlInline() {
        return helpersInit.getBaseHelper().checkCantEditFieldInline(INLINE_URL_LABEL, INLINE_URL_INPUT);
    }

    /**
     * edit inline url
     */
    public String editUrlInline() {
        return moderationHelper.editUrlInline();
    }

    public String readUrl() {
        return INLINE_URL_LABEL.shouldBe(visible).text();
    }

    /**
     * check can't edit inline image
     */
    public boolean checkCantEditImageInline() {
        return moderationHelper.checkCantEditImageInline();
    }

    /**
     * editing image inline
     */
    public boolean editImageInline() {
        return moderationHelper.editImageInline();
    }

    /**
     * choose campaign by filter 'campaign id'
     */
    public void chooseCampaignIdByFilter(int campaignId) {
        moderationHelper.fillFilterCampaignId(campaignId);
        moderationHelper.filterSubmit();
    }

    /**
     * check mass action select has only custom fields
     */
    public boolean checkMassActionFilterHaveOnlyCustomField(MassActions... actions) {
        return moderationHelper.checkMassActionFilterHaveOnlyCustomField(actions);
    }

    /**
     * Edit title inline
     */
    public void editTitleInline(String value) {
        INLINE_TITLE_LABEL.shouldBe(Condition.visible).doubleClick();
        log.info("title was clicked");
        INLINE_TITLE_INPUT.setValue(value).pressEnter();
        INLINE_TITLE_INPUT.shouldBe(Condition.hidden);
    }

    /**
     * Edit description inline
     */
    public void editDescriptionInline(String value) {
        INLINE_DESCRIPTION_LABEL.shouldBe(Condition.visible).doubleClick();
        log.info("description was clicked");
        INLINE_DESCRIPTION_INPUT.setValue(value).pressEnter();
        INLINE_DESCRIPTION_INPUT.shouldBe(Condition.hidden);
    }

    /**
     * Edit content group inline
     */
    public void changeContentGroupInline(String newsId) {
        $(String.format(CONTENT_GROUP_ICON, newsId)).click();
        SelenideElement contentTypeSelect = $(String.format(CONTENT_GROUP_SELECT, newsId));
        ElementsCollection options = $(String.format(CONTENT_GROUP_SELECT, newsId)).findAll("option");
        List<SelenideElement> validOptions = options.filter(not(attribute("value", contentTypeSelect.getSelectedValue())));

        contentTypeSelect.selectOptionByValue(
                validOptions.get(randomNumbersInt(validOptions.size())).val()
        );
        SUBMIT_BUTTON.click();
        checkErrors();
    }

    public boolean checkErrorIconForNewsTitle() {
        return TITLE_ERROR_ICON.isDisplayed();
    }

    public boolean checkErrorIconForNewsDescription() {
        return DESCRIPTION_ERROR_ICON.isDisplayed();
    }

    public boolean checkTitleOfErrorIconForNewsTitle(String titleValue) {
        return TITLE_ERROR_ICON.attr("title").equals(titleValue);
    }

    public boolean checkTitleOfErrorIconForNewsDescription(String titleValue) {
        return DESCRIPTION_ERROR_ICON.attr("title").equals(titleValue);
    }

    public String getTitleInline() {
        return INLINE_TITLE_LABEL.getText();
    }

    public String getDescriptionInline() {
        return INLINE_DESCRIPTION_LABEL.getText();
    }

    public String getEmptyTableText() {
        checkErrors();
        return EMPTY_TABLE_CELL.shouldBe(visible).getText();
    }

    public void changeStateOriginalTitleIcon(boolean state) {
        if(state && NOT_ORIGINAL_TITLE_ICON.first().isDisplayed()) {
            NOT_ORIGINAL_TITLE_ICON.first().click();
        } else if(!state && ORIGINAL_TITLE_ICON.first().isDisplayed()) {
            ORIGINAL_TITLE_ICON.first().click();
        }
        waitForAjax();
    }

    public boolean checkStateOriginalTitleIcon(boolean state) {
        return state ? ORIGINAL_TITLE_ICON.first().isDisplayed() : NOT_ORIGINAL_TITLE_ICON.first().isDisplayed();
    }

    public void filterOriginalTitle(String value) {
        ORIGINAL_TITLE_FILTER.selectOptionByValue(value);
        FILTER_SUBMIT.click();
        checkErrors();
    }

    public int getAmountOfNews() {
        return NEWS_INFO_BLOCK.size();
    }

    public int getAmountOfNewsWithOriginalTitle() {
        return ORIGINAL_TITLE_ICON.size();
    }

    public int getAmountOfNewsWithoutOriginalTitle() {
        return NOT_ORIGINAL_TITLE_ICON.size();
    }

    public String getlinkOfComparisonImagesIcon(String icon) {
        return LINK_TO_RELATED_IMAGES.attr("href");
    }

    public void filterByRelatedImages(String id) {
        RELATED_IMAGES_FILTER.sendKeys(id);
        FILTER_SUBMIT.click();
        checkErrors();
    }

    public List<String> getNewsIdFromLabel() {
        return NEWS_ID_LABELS.texts();
    }

    public boolean checkVisibilityIconOfStatusForComparisonImages(String icon) {
        return $("img[src*='" + icon + "']").isDisplayed();
    }
}
