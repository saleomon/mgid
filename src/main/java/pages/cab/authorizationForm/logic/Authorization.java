package pages.cab.authorizationForm.logic;

import com.codeborne.selenide.Condition;
import core.base.HelpersInit;
import testData.project.AuthUserCabData;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.codeborne.selenide.Condition.visible;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static core.helpers.HelperLocators.MGID_CAB_LOGO;
import static pages.cab.authorizationForm.locators.AuthorizationLocators.*;
import static testData.project.AuthUserCabData.userPasswordCab;

public class Authorization {

    private final HelpersInit helpersInit;

    public Authorization(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public void authorizeWithForm(AuthUserCabData.AuthorizationUsers login, String... passwordValue) {
        String pass = passwordValue.length > 0 ? passwordValue[0] : userPasswordCab;

        LOGIN_INPUT.sendKeys(login.getLoginUser());
        PASS_INPUT.sendKeys(pass);

        SIGN_IN.click();
        SUBMIT_LOADER.shouldBe(Condition.hidden, Duration.ofSeconds(8));
        waitForAjax();
        checkErrors();
    }

    public String getDisplayedMessage() {
        return NOTIFICATION_BLOCK.shouldBe(visible, Duration.ofSeconds(8)).text().trim();
    }

    public void enterEmailForRestorePasswordAndSabmitIt(String emailValue) {
        clearAndSetValue(EMAIL_INPUT.shouldBe(visible), emailValue);
        RESTORE_PASSWORD_BUTTON.click();
        checkErrors();
    }

    public String getPasswordFromEmail(String emailBodyValue) {
        Scanner scanner = new Scanner(emailBodyValue);
        List<String> rows = new ArrayList<>();
        while (scanner.hasNextLine()) {
            rows.add(scanner.nextLine());
        }
        scanner.close();
        return rows.get(8);
    }

    public boolean isShowCab() {
        return MGID_CAB_LOGO.isDisplayed();
    }

    public void authCab(AuthUserCabData.AuthorizationUsers user, String link) {
        helpersInit.getAuthorizationHelper().goLinkCab(link);
        if (!isShowCab()) {
            helpersInit.getAuthorizationHelper().setCookieForAuthorization(user);
            helpersInit.getAuthorizationHelper().goLinkCab(link);
        }
    }
}
