package pages.cab.authorizationForm.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AuthorizationLocators {
    public static final SelenideElement LOGIN_INPUT = $("[formcontrolname='login'] input[tuitextfield]");
    public static final SelenideElement PASS_INPUT = $("[formcontrolname='password'] input[tuitextfield]");
    public static final SelenideElement SIGN_IN = $("[class='sign-in-button']");
    public static final SelenideElement NOTIFICATION_BLOCK = $("mgui-alert [automation-id*='content']");
    public static final SelenideElement SUBMIT_LOADER = $("[automation-id='tui-loader__loader']");
    public static final SelenideElement EMAIL_INPUT = $("#userInfo");
    public static final SelenideElement RESTORE_PASSWORD_BUTTON = $("#sbmt");
}
