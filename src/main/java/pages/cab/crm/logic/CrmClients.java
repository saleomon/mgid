package pages.cab.crm.logic;

import core.base.HelpersInit;
import pages.cab.crm.helper.CrmAddHelper;
import pages.cab.crm.helper.CrmListHelper;

import java.util.*;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.crm.locators.CrmAddLocators.*;
import static pages.cab.crm.locators.CrmListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.AuthUserCabData.goodhitsCurator;
import static testData.project.AuthUserCabData.wagesCurator;


public class CrmClients {

    private final HelpersInit helpersInit;
    CrmAddHelper crmAddHelper;
    CrmListHelper crmListHelper;

    private String clientId;
    private String website;
    private String login;
    private String source;
    private final String status = "3";
    private String subnet = "{\"subnet\":0,\"mirror\":null}";
    private final String inviter = goodhitsCurator;
    private final String fullName = "Ivanov Mitya";
    private String email;
    private final String phone = "380934471290";
    private final String phoneForLegalEntity = "+380934471290";
    private final String skype = "ivanovskype";
    private final String telegram = "ivanovtelegram";
    private final String country = "Ukraine";

    private CrmAddHelper.PaymentTerms paymentTerms;
    private HashMap<String, String> paymentTermsOptions;
    private final String publishersCurator = wagesCurator;
    private String productsCurator = goodhitsCurator;
    private final String currency = "usd";
    private final boolean isTestClient = false;
    private final boolean isSeasonalClient = false;
    private final boolean isPublisherDirection = true;
    private final boolean isProductsDirection = true;
    private String liability;
    private String liabilityPostponedDate = "";
    private String liabilityPostponedSpent = "";

    private final List<String> cooperationType = new LinkedList<>(Collections.singletonList("wages"));
    private String legalEntityBusinessType;
    private String legalEntityCompanyName;
    private String legalEntityContactPersonName;
    private String legalEntityContactPersonEmail;

    public CrmClients(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        crmAddHelper = new CrmAddHelper(helpersInit);
        crmListHelper = new CrmListHelper(helpersInit);
    }

    public String getClientId() {
        return clientId;
    }

    public String getLogin() {
        return login;
    }

    public String getWebsite() {
        return website;
    }

    public String getSource() {
        return source;
    }

    public CrmClients setSubnet(String subnet) {
        this.subnet = subnet;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CrmClients setProductsCurator(String productsCurator) {
        this.productsCurator = productsCurator;
        return this;
    }

    public CrmClients setPaymentTerms(CrmAddHelper.PaymentTerms paymentTerms) {
        this.paymentTerms = paymentTerms;
        return this;
    }

    public CrmClients setCanSeeSubnet(boolean canSeeSubnet) {
        crmAddHelper.setCanSeeSubnet(canSeeSubnet);
        return this;
    }

    public CrmClients setBlockedClient(boolean state) {
        markUnMarkCheckbox(state, BLOCKED_CLIENT_CHECKBOX);
        return this;
    }

    public CrmClients setLegalEntityBusinessType(String legalEntityBusinessType) {
        this.legalEntityBusinessType = legalEntityBusinessType;
        return this;
    }

    public CrmClients setLegalEntityCompanyName(String legalEntityCompanyName) {
        this.legalEntityCompanyName = legalEntityCompanyName;
        return this;
    }

    public CrmClients setLegalEntityContactPersonName(String legalEntityContactPersonName) {
        this.legalEntityContactPersonName = legalEntityContactPersonName;
        return this;
    }

    public CrmClients setLegalEntityContactPersonEmail(String legalEntityContactPersonEmail) {
        this.legalEntityContactPersonEmail = legalEntityContactPersonEmail;
        return this;
    }

    /**
     * create client
     */
    public String createClient() {
        //login = crmAddHelper.setLogin(login);
        email = crmAddHelper.setEmail(email);
        crmAddHelper.setFullName(fullName);
        crmAddHelper.setPhone(phone);
        crmAddHelper.setSkype(skype);
        crmAddHelper.setTelegram(telegram);

        // selectors
        crmAddHelper.chooseSubnet(subnet);
        crmAddHelper.chooseInviter(inviter);
        String partnership = "cpa";
        crmAddHelper.choosePartnership(partnership);
        crmAddHelper.chooseCurrency(currency);
        crmAddHelper.chooseClientCountry(country);

        //default status 3
        crmAddHelper.chooseStatus(status);

        // checkboxes
        crmAddHelper.switchTestClient(isTestClient);
        crmAddHelper.switchSeasonClient(isSeasonalClient);

        // cooperation type
        crmAddHelper.switchPublisherDirection(isPublisherDirection);
        crmAddHelper.selectPublisherDirection(isPublisherDirection, publishersCurator);
        crmAddHelper.switchProductsDirection(isProductsDirection);
        crmAddHelper.selectProductsDirection(isProductsDirection, productsCurator);

        // payment terms
        paymentTerms = crmAddHelper.selectPaymentTerms(paymentTerms);
        paymentTermsOptions = crmAddHelper.setAdditionalOptionPaymentTerms(paymentTerms);

        //publisher currency
        crmAddHelper.choosePublisherCurrency(isPublisherDirection);

        crmAddHelper.setCooperationTypes(cooperationType);

        crmAddHelper.saveClient();

        clientId = crmListHelper.getClientId();
        return helpersInit.getBaseHelper().getTextAndWriteLog(clientId);
    }

    public void saveSettings() {
        crmAddHelper.saveClient();
    }

    public boolean checkBlockedIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BLOCKED_CLIENT_ICON.isDisplayed());
    }

    /**
     * create client
     */
    public void createClientRa() {
//        login = crmAddHelper.setLogin(login);
        email = crmAddHelper.setEmail(email);
        crmAddHelper.setFullName(fullName);
        crmAddHelper.setPhone(phone);
        crmAddHelper.setSkype(skype);
        crmAddHelper.setTelegram(telegram);

        // selectors
        crmAddHelper.chooseSubnet(subnet);

        //default status 3
        crmAddHelper.chooseStatus(status);

        // cooperation type
        crmAddHelper.switchProductsDirection(isProductsDirection);
        crmAddHelper.selectProductsDirection(isProductsDirection, productsCurator);

        crmAddHelper.chooseClientCountry(country);

        crmAddHelper.saveClient();
    }

    /**
     * check base client settings in list interface
     */
    public boolean checkClientInListInterface() {
        return crmListHelper.checkLogin(email) &&
                crmListHelper.checkWebsite(website) &&
                crmListHelper.checkStatus(status) &&
                crmListHelper.checkInviter(inviter) &&
                crmListHelper.checkEmail(email) &&
                crmListHelper.checkPhone(phone) &&
                crmListHelper.checkSkype(skype) &&
                crmListHelper.checkTelegram(telegram) &&
                crmListHelper.checkFullName(fullName) &&
                crmListHelper.checkPublisherCurator(publishersCurator) &&
                crmListHelper.checkProductsCurator(productsCurator) &&
                crmListHelper.checkShowGogolIcon();
    }

    /**
     * check base client settings in edit interface
     */
    public boolean checkClientInEditInterface() {
        return crmAddHelper.checkLogin(email) &
                crmAddHelper.checkStatus(status) &
                crmAddHelper.checkIsTestClient(isTestClient) &
                crmAddHelper.checkSeasonClient(isSeasonalClient) &
                crmAddHelper.checkCurrency(currency) &
                crmAddHelper.checkPublisherCurator(isPublisherDirection, publishersCurator) &
                crmAddHelper.checkProductsCurator(isProductsDirection, productsCurator) &
                crmAddHelper.checkCooperationType(cooperationType) &
                crmAddHelper.checkEmail(email) &
                crmAddHelper.checkPhone(phone) &
                crmAddHelper.checkSkype(skype) &
                crmAddHelper.checkTelegram(telegram) &
                crmAddHelper.checkFullName(fullName);
    }

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return crmListHelper.isDisplayedAllowedClicksFromClientDomainsOnly();
    }

    public boolean checkPaymentTerms(CrmAddHelper.PaymentTerms paymentTerms, Map<String, String> paymentTermsOptions) {
        return PAYMENT_TERMS_SELECT.text().equalsIgnoreCase(paymentTerms.getPaymentTermsValue()) &&
                helpersInit.getBaseHelper().checkDatasetContains(PAYMENT_TERMS_FILE_NAME.text(), paymentTermsOptions.get("file").split(".pdf")[0]) &&
                switch (paymentTerms) {
                    case POST_PAYMENT, MIXED_PAYMENT ->
                            helpersInit.getBaseHelper().checkDatasetEquals(PAYMENT_TERMS_NET_INPUT.val(), paymentTermsOptions.get("net"));
                    default -> true;
                };
    }

    public void choosePaymentTerms(CrmAddHelper.PaymentTerms paymentTerms) {
        crmAddHelper.selectPaymentTerms(paymentTerms);
    }

    public void setPaymentTermsNet(String netVal) {
        crmAddHelper.inputPaymentTermsNet(netVal);
    }

    public void setPaymentTermsFile(String fileName) {
        crmAddHelper.uploadPaymentTermsFile(fileName);
    }

    public void openPaymentTermFilesPopup() {
        crmAddHelper.openAddTermsPopup();
    }

    public boolean isDisplayedPaymentTermsIcon() {
        return PAYMENT_TERMS_ICON.isDisplayed();
    }

    public void deletePaymentTermsFile() {
        PAYMENT_TERMS_FILE_REMOVE_BUTTON.click();
        waitForAjax();
    }

    public boolean isDisplayedPaymentTermsField() {
        return PAYMENT_TERMS_POPUP_BUTTON.isDisplayed();
    }

    public void saveSettingsOfPaymentTermsPopup() {
        SAVE_PAYMENT_TERMS_BUTTON_POPUP.click();
        waitForAjax();
    }

    public void saveSettingsOfPaymentTerms() {
        crmAddHelper.saveSettingsOfPaymentTermsPopup();
    }

    public void clickPaymentTermsIcon() {
        PAYMENT_TERMS_ICON.scrollTo().click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check work 'Login' filter
     */
    public void filterClientsByLogin(String login) {
        helpersInit.getBaseHelper().setAttributeValueJS(CRM_FILTER_BY_LOGIN, login);
        crmListHelper.clickFindButton();
    }

    /**
     * check work 'Domain' filter
     */
    public void filterClientsByDomain(String domain) {
        helpersInit.getBaseHelper().setAttributeValueJS(FILTER_BY_DOMAIN, domain);
        crmListHelper.clickFindButton();
    }

    /**
     * check work 'Source' filter
     */
    public void filterClientsBySource(String source) {
        helpersInit.getBaseHelper().setAttributeValueJS(FILTER_BY_SOURCE, source);
        crmListHelper.clickFindButton();
    }

    /**
     * get client login
     */
    public String getClientLogin() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CRM_LOGIN_FIELD.text());
    }

    /**
     * get client website
     */
    public String getClientWebsite() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(WEBSITE_FIELD.text());
    }

    /**
     * get client source
     */
    public String getClientSource() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SOURCE_FIELD.text());
    }

    /**
     * get amount of clients at list
     */
    public int getAmountClients() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CLIENT_FIELD_IDS.size());
    }

    public boolean checkIdenfyIsDisplayed() {
        return IDENFY_CHECKBOX.isDisplayed();
    }

    public void setStateIdenfyVerificationForClient(boolean stateValue) {
        markUnMarkCheckbox(stateValue, IDENFY_CHECKBOX);
        crmAddHelper.saveClient();
    }

    public boolean checkIdenfyState(boolean stateValue) {
        return checkIsCheckboxSelected(stateValue, IDENFY_CHECKBOX);
    }

    public boolean checkAutoModerationCheckboxState(boolean stateValue) {
        return checkIsCheckboxSelected(stateValue, AUTO_MODERATION_SUSPECTED_FRIENDS_CHECKBOX);
    }

    public CrmClients changeStateAutoModerationCheckbox(boolean stateValue) {
        markUnMarkCheckbox(stateValue, AUTO_MODERATION_SUSPECTED_FRIENDS_CHECKBOX);
        checkErrors();
        return this;
    }

    public void enableProductDirection() {
        crmAddHelper.switchProductsDirection(isProductsDirection);
        crmAddHelper.selectProductsDirection(isProductsDirection, productsCurator);
    }

    public boolean checkIdenfyLabelIconIsDisplayed(String labelColor) {
        return $(String.format(IDENFY_LABEL_ICON, labelColor)).isDisplayed();
    }

    public String getIdenfyLabelTitle(String labelColor) {
        return $(String.format(IDENFY_LABEL_ICON, labelColor)).getAttribute("title");
    }

    public boolean amlButtonIsDisplayed() {
        return AML_BUTTON.isDisplayed();
    }

    public void clickAmlButton() {
        AML_BUTTON.shouldBe(visible).click();
        AML_POPUP.shouldBe(visible);
        checkErrors();
    }

    public void chooseAmlVariant() {
        AML_RADIO_BUTTON.shouldBe(visible).click();
    }

    public void chooseCompanyIdentification() {
        AML_COMPANY_RADIO_BUTTON.shouldBe(visible).click();
    }

    public void chooseLidIdentification() {
        AML_LID_RADIO_BUTTON.shouldBe(visible).click();
    }

    public void fillClientName(String nameValue) {
        clearAndSetValue(AML_NAME_INPUT, nameValue);
    }

    public void fillClientSurname(String surnameValue) {
        clearAndSetValue(AML_SURNAME_INPUT, surnameValue);
    }

    public void fillCompanyName(String companyName) {
        clearAndSetValue(AML_COMPANY_NAME_INPUT, companyName);
    }

    public String selectCountryValue() {
        return helpersInit.getBaseHelper().selectRandomValue(AML_COUNTRY_SELECT);
    }

    public void clickVerifyButton() {
        AML_POPUP_VERIFY_BUTTON.click();
        checkErrors();
    }

    public boolean checkAmlResponseData(String value) {
        return AML_DATA_FIELD.text().contains(value);
    }

    public void fillDocumentNumber(String numberValue) {
        clearAndSetValue(AML_DOCUMENT_NUMBER, numberValue);
    }

    public boolean checkLiabilityIsDisplayed() {
        return LIABILITY_SELECT.isDisplayed();
    }

    public CrmClients setLiabilitySettings(String... liabilityValue) {
        liability = liabilityValue.length == 0 ?
                helpersInit.getBaseHelper().selectRandomValue(LIABILITY_SELECT) :
                helpersInit.getBaseHelper().selectCustomValue(LIABILITY_SELECT, liabilityValue[0]);
        return this;
    }

    public CrmClients setLiabilityDate() {
        int offset = randomNumbersInt(30);
        liabilityPostponedDate = helpersInit.getBaseHelper().dateFormat(helpersInit.getBaseHelper().getCurrentDateWithDateOffset("Pacific/Honolulu", offset), ("yyyy-MM-dd"));
        clearAndSetValue(LIABILITY_POSTPONED_DATE_INPUT, liabilityPostponedDate);
        return this;
    }

    public CrmClients setLiabilitySpend() {
        liabilityPostponedSpent = helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(1, 999).replace(",", ".");
        clearAndSetValue(LIABILITY_POSTPONED_SPENT_INPUT, liabilityPostponedSpent);
        return this;
    }

    public boolean checkLiabilitySettings() {
        return helpersInit.getBaseHelper().checkDatasetEquals(liability, Objects.requireNonNull(LIABILITY_SELECT.getSelectedValue()));
    }

    public boolean checkLiabilityPostponedDate() {
        return helpersInit.getBaseHelper().checkDatasetEquals(liabilityPostponedDate, Objects.requireNonNull(LIABILITY_POSTPONED_DATE_INPUT.getValue()));
    }

    public boolean checkLiabilityPostponedSpent() {
        return helpersInit.getBaseHelper().checkDatasetEquals(liabilityPostponedSpent, Objects.requireNonNull(LIABILITY_POSTPONED_SPENT_INPUT.getValue()));
    }

    public CrmClients chooseLegalRelation(String legalRelationValue) {
        LEGAL_ENTITY_RADIO_BUTTON.selectRadio(legalRelationValue);
        return this;
    }

    public CrmClients inputLegalEntityCompanyName() {
        legalEntityCompanyName = helpersInit.getGenerateDataHelper().generateCompanyName();
        clearAndSetValue(LEGAL_ENTITY_COMPANY_NAME_INPUT, legalEntityCompanyName);
        return this;
    }

    public CrmClients selectLegalEntityBusinessType() {
        legalEntityBusinessType = helpersInit.getBaseHelper().selectRandomValue(LEGAL_ENTITY_BUSINESS_TYPE_SELECT);
        return this;
    }

    public CrmClients inputLegalEntityPersonName() {
        legalEntityContactPersonName = helpersInit.getGenerateDataHelper().generateSurname() + " " + helpersInit.getGenerateDataHelper().generateName();
        clearAndSetValue(LEGAL_ENTITY_CONTACT_PERSON_NAME_INPUT, legalEntityContactPersonName);
        return this;
    }

    public CrmClients inputLegalEntityPersonEmail() {
        legalEntityContactPersonEmail = helpersInit.getGenerateDataHelper().generateEmail();
        clearAndSetValue(LEGAL_ENTITY_CONTACT_PERSON_EMAIL_INPUT, legalEntityContactPersonEmail);
        return this;
    }

    public CrmClients inputLegalEntityPersonPhone() {
        clearAndSetValue(LEGAL_ENTITY_CONTACT_PERSON_PHONE_INPUT, phoneForLegalEntity);
        return this;
    }

    public boolean checkClientLegalEntityInEditInterface() {
        return crmAddHelper.checkClientLegalEntityFieldValue("Company name:", legalEntityCompanyName) &&
                crmAddHelper.checkClientLegalEntityFieldValue("Business type:", legalEntityBusinessType) &&
                crmAddHelper.checkClientLegalEntityFieldValue("Contact person name:", legalEntityContactPersonName) &&
                crmAddHelper.checkClientLegalEntityFieldValue("Contact person email:", legalEntityContactPersonEmail) &&
                crmAddHelper.checkClientLegalEntityFieldValue("Contact person phone:", phoneForLegalEntity);
    }

    public boolean checkLegalEntityIsDisplayed() {
        return LEGAL_ENTITY_RADIO_BUTTON.isDisplayed();
    }
}
