package pages.cab.crm.locators;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;


public class CrmAddLocators {

    public static final SelenideElement WEBSITE_INPUT = $("#site");
    public static final SelenideElement LOGIN_INPUT = $("#login");
    public static final SelenideElement EMAIL_INPUT = $(".contacts .email");
    public static final SelenideElement FULL_NAME_INPUT = $(".contacts .name");
    public static final SelenideElement TEST_CLIENT_CHECKBOX = $("#isTest");
    public static final SelenideElement PAYMENT_TERMS_POPUP_BUTTON = $("[id='paymentTermsFileUpload']");
    public static final SelenideElement SAVE_PAYMENT_TERMS_BUTTON = $("[aria-labelledby*='payment-terms-file-upload-popup'] button");
    public static final SelenideElement SAVE_PAYMENT_TERMS_BUTTON_POPUP = $x("//span[text()='Submit']");
    public static final SelenideElement PAYMENT_TERMS_NET_INPUT = $("[id='paymentTermsNet']");
    public static final SelenideElement PAYMENT_TERMS_FILE_NAME = $("[id='uploaded-files'] a");
    public static final SelenideElement PAYMENT_TERMS_FILE_UPLOAD = $("[id='fileUploader']");
    public static final SelenideElement PAYMENT_TERMS_FILE_REMOVE_BUTTON = $("[id*='remove']");
    public static final SelenideElement PAYMENT_TERMS_FILE_UPLOAD_BUTTON = $("[class*='payment-terms-file-upload-button']");
    public static final String          INNER_CLIENT_STRING_RADIOBUTTONS = "isInner";
    public static final SelenideElement INNER_CLIENT_RADIOBUTTONS = $(By.name("isInner"));
    public static final SelenideElement SEASON_CLIENT_CHECKBOX = $("#seasonalClient");
    public static final SelenideElement SAVE_BUTTON = $("#save");
    public static final SelenideElement PHONE_INPUT = $(".contacts .phone");
    public static final SelenideElement SKYPE_INPUT = $(".contacts .skype");
    public static final SelenideElement TELEGRAM_INPUT = $(".contacts .telegram");
    public static final SelenideElement SUBNET_SELECT = $("#subnet");
    public static final SelenideElement INVITER_SELECT = $("#inviter");
    public static final SelenideElement CURRENCY_SELECT = $("#currency");
    public static final SelenideElement CURRENCY_EDIT_SELECT = $("#clientCurrency");
    public static final SelenideElement CLIENT_COUNTRY_SELECT = $("#billingCountry");
    public static final SelenideElement PAYMENT_TERMS_SELECT = $("[id='paymentTerms']");
    public static final SelenideElement STATUS_SELECT = $("#crmStatus");
    public static final SelenideElement PUBLISHER_CURRENCY_SELECT = $("#publisherCurrency");
    public static final SelenideElement PARTNERSHIP_SELECT = $("#partnership");
    public static final SelenideElement SITE_LANGUAGE_SELECT = $("#sites-0-site_lang");
    public static final SelenideElement SITE_CATEGORY_SELECT = $("#sites-0-category_dropdown");
    public static final SelenideElement SITE_TIER_SELECT = $("select.crm_add_tier_id");
    public static final SelenideElement BLOCKED_CLIENT_CHECKBOX = $("[id='godmodeBlock']");
    public static final SelenideElement BLOCKED_CLIENT_ICON = $("[src*='lock_red.png']");

    public static final SelenideElement LOGIN_FIELD = $x(".//tr[td[span[contains(text(), 'Login:')]]]/td[2]");

    //curators block
    public static final SelenideElement PUBLISHERS_DIRECTION_CHECKBOX = $("#sunriseField_wages_checkbox");
    public static final SelenideElement PRODUCTS_DIRECTION_CHECKBOX = $("#sunriseField_goodhits_checkbox");
    public static final SelenideElement PUBLISHERS_DIRECTION_SELECT = $("#curators-wages");
    public static final SelenideElement PRODUCTS_DIRECTION_SELECT = $("#curators-goodhits");
    public static final SelenideElement PUBLISHERS_DIRECTION_EDIT_CHECKBOX = $("#section_wages");
    public static final SelenideElement PRODUCTS_DIRECTION_EDIT_CHECKBOX = $("#section_goodhits");


    //cooperation types block
    public static final SelenideElement PANEL_RIGHT = $("#cooperation_priorities_right");

    //cloudinary
    public static final SelenideElement CLOUDINARY_CHECKBOX = $("[id='useCloudinary']");

    //client legal relation
    public static final SelenideElement LEGAL_ENTITY_RADIO_BUTTON = $("#legalRelation-legal_entity");
    public static final SelenideElement LEGAL_ENTITY_COMPANY_NAME_INPUT = $("#companyName");
    public static final SelenideElement LEGAL_ENTITY_BUSINESS_TYPE_SELECT = $("#businessType");
    public static final SelenideElement LEGAL_ENTITY_CONTACT_PERSON_NAME_INPUT = $("#contactPersonName");
    public static final SelenideElement LEGAL_ENTITY_CONTACT_PERSON_EMAIL_INPUT = $("#contactPersonEmail");
    public static final SelenideElement LEGAL_ENTITY_CONTACT_PERSON_PHONE_INPUT = $("#contactPersonPhone");
    public static final String LEGAL_ENTITY_TABLE_FIELD = "//fieldset//label[text() = '%s']//parent::span//following-sibling::span/label";

}
