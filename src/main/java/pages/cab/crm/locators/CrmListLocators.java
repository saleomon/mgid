package pages.cab.crm.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CrmListLocators {

    public static final SelenideElement CLIENT_FIELD_ID = $(".client[id*='client-']");
    public static final ElementsCollection CLIENT_FIELD_IDS = $$(".client[id*='client-']");
    public static final SelenideElement CRM_LOGIN_FIELD = $("[id*='-login']");
    public static final SelenideElement WEBSITE_FIELD = $x(".//div[span[contains(text(), 'Website')]]/*[@class='value']");
    public static final SelenideElement SOURCE_FIELD = $x(".//div[span[contains(text(), 'Source')]]/*[@class='value']");
    public static final SelenideElement STATUS_SELECT = $("[id*='crm_status_']");
    public static final SelenideElement INVITER_FIELD = $(".changeInviter");
    public static final SelenideElement EMAIL_FIELD = $x(".//tr[td[contains(text(), 'E-mail:')]]//span");
    public static final SelenideElement PHONE_FIELD = $x(".//tr[td[contains(text(), 'Phone:')]]//span");
    public static final SelenideElement SKYPE_FIELD = $x(".//tr[td[contains(text(), 'Skype:')]]//span");
    public static final SelenideElement TELEGRAM_FIELD = $x(".//tr[td[contains(text(), 'Telegram:')]]//span");
    public static final SelenideElement FULL_NAME_FIELD = $x(".//th[contains(text(), 'Main contact person')]//span");
    public static final SelenideElement PUBLISHER_MANAGER_FIELD = $("[id*='curator_wages']>a");
    public static final SelenideElement PRODUCTS_MANAGER_FIELD = $("[id*='curator_goodhits']>a");
    public static final SelenideElement GOGOL_ICON = $("img[src*='cablink.png']");
    public static final SelenideElement PAYMENT_TERMS_ICON = $("img[src*='payment-terms.png']");
    public static final SelenideElement IDENFY_CHECKBOX = $("#antifraudKYCCanVerifyKyc");
    public static final SelenideElement AUTO_MODERATION_SUSPECTED_FRIENDS_CHECKBOX = $("#antifraudKYCIdenfyModeAutoDisabled");
    public static final String IDENFY_LABEL_ICON = "img[src*='%s.png']";
    public static final SelenideElement AML_BUTTON = $("input[type=button][value=AML]");
    public static final SelenideElement AML_POPUP_VERIFY_BUTTON = $("#form_aml_verification_button");
    public static final SelenideElement AML_POPUP_HISTORY_BUTTON = $("#aml_verification_history_button");
    public static final SelenideElement AML_POPUP = $(".form_aml_verification");
    public static final SelenideElement AML_RADIO_BUTTON = $("#aml");
    public static final SelenideElement AML_COMPANY_RADIO_BUTTON = $("#company");
    public static final SelenideElement AML_LID_RADIO_BUTTON = $("#lid");
    public static final SelenideElement AML_NAME_INPUT = $("#aml_name");
    public static final SelenideElement AML_SURNAME_INPUT = $("#aml_surname");
    public static final SelenideElement AML_COMPANY_NAME_INPUT = $("#company_name");
    public static final SelenideElement AML_COUNTRY_SELECT = $("#company_countries");
    public static final SelenideElement AML_DOCUMENT_TYPE_SELECT = $("#document_type");
    public static final SelenideElement AML_DOCUMENT_NUMBER = $("#document_number");
    public static final SelenideElement AML_COUNTRY_INPUT = $("#country");
    public static final SelenideElement AML_DATA_FIELD = $("#aml_data");
    public static final SelenideElement LIABILITY_SELECT = $("#liability");
    public static final SelenideElement LIABILITY_POSTPONED_DATE_INPUT = $("#liabilityPostponedDate");
    public static final SelenideElement LIABILITY_POSTPONED_SPENT_INPUT = $("#liabilityPostponedSpent");

    //filters
    public static final SelenideElement CRM_FILTER_BY_LOGIN = $("#login");
    public static final SelenideElement FILTER_BY_DOMAIN = $("#domain");
    public static final SelenideElement FILTER_BY_SOURCE = $("#clients_referer");
    public static final SelenideElement FIND_SUBMIT_BUTTON = $("input[value=Find]");

}
