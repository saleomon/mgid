package pages.cab.crm.helper;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.HashMap;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static pages.cab.crm.locators.CrmAddLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;


public class CrmAddHelper {

    private final HelpersInit helpersInit;

    private final boolean isCreateWebsite = false;
    private boolean canSeeSubnet = true;

    public enum PaymentTerms {
        PRE_PAYMENT("prepayment"),
        POST_PAYMENT("postpayment"),
        MIXED_PAYMENT("mixedpayment"),
        EMPTY_VALUE("");
        private final String paymentTermsValue;

        PaymentTerms(String paymentTerms) {
            this.paymentTermsValue = paymentTerms;
        }

        public String getPaymentTermsValue() {
            return paymentTermsValue;
        }
    }

    public CrmAddHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public void setCanSeeSubnet(boolean canSeeSubnet) {
        this.canSeeSubnet = canSeeSubnet;
    }

    /**
     * set client login
     */
    public String setLogin(String login) {
        String clientLogin = login == null ? "AddClient" + BaseHelper.getRandomWord(2) : login;
        clearAndSetValue(LOGIN_INPUT, clientLogin);
        return clientLogin;
    }

    /**
     * set 'Full Name'
     */
    public void setFullName(String fullName) {
        clearAndSetValue(FULL_NAME_INPUT, fullName);
    }

    /**
     * set 'Email'
     */
    public String setEmail(String email) {
        String clientEmail = email == null ? "testEmail" + BaseHelper.getRandomWord(2) + "@ex.ua" :
                email;
        clearAndSetValue(EMAIL_INPUT, clientEmail);
        return clientEmail;
    }

    /**
     * set phone
     */
    public void setPhone(String phone) {
        clearAndSetValue(PHONE_INPUT, phone);
    }

    /**
     * set 'Skype'
     */
    public void setSkype(String skype) {
        clearAndSetValue(SKYPE_INPUT, skype);
    }

    /**
     * set 'Telegram'
     */
    public void setTelegram(String telegram) {
        clearAndSetValue(TELEGRAM_INPUT, telegram);
    }

    /**
     * choose 'Subnet'
     */
    public void chooseSubnet(String subnetId) {
        if (canSeeSubnet) {
            helpersInit.getBaseHelper().selectCustomValue(SUBNET_SELECT, subnetId);
        }
    }


    /**
     * choose 'Inviter'
     */
    public void chooseInviter(String inviter) {
        helpersInit.getBaseHelper().selectCustomValue(INVITER_SELECT, inviter);
    }

    /**
     * choose 'Partnership'
     */
    public void choosePartnership(String partnership) {
        helpersInit.getBaseHelper().selectCustomValue(PARTNERSHIP_SELECT, partnership);
    }

    /**
     * choose 'Clients Currency'
     */
    public void chooseCurrency(String currency) {
        helpersInit.getBaseHelper().selectCustomValue(CURRENCY_SELECT, currency);
    }

    /**
     * choose 'Client country'
     */
    public void chooseClientCountry(String country) {
        helpersInit.getBaseHelper().selectCustomText(CLIENT_COUNTRY_SELECT, country);
    }

    public PaymentTerms selectPaymentTerms(PaymentTerms paymentMethod) {
        if(paymentMethod == null) return null;
        helpersInit.getBaseHelper().selectCustomText(PAYMENT_TERMS_SELECT, paymentMethod.paymentTermsValue);
        return paymentMethod;
    }

    public HashMap<String, String> setAdditionalOptionPaymentTerms(PaymentTerms paymentMethod) {
        if (paymentMethod == null) return null;
        HashMap<String, String> data = new HashMap<>();
        switch (paymentMethod) {
            case POST_PAYMENT, MIXED_PAYMENT -> data.put("net", inputPaymentTermsNet());
        }
        openAddTermsPopup();
        data.put("file", uploadPaymentTermsFile());
        saveSettingsOfPaymentTermsPopup();
        return data;
    }

    public void openAddTermsPopup() {
        PAYMENT_TERMS_POPUP_BUTTON.click();
        SAVE_PAYMENT_TERMS_BUTTON.shouldBe(Condition.visible);
    }

    public String inputPaymentTermsNet(String...val) {
        String randomNumber = val.length == 0 ? randomNumberFromRange(1, 10) : val[0];
        PAYMENT_TERMS_NET_INPUT.clear();
        PAYMENT_TERMS_NET_INPUT.sendKeys(randomNumber);
        return randomNumber;
    }

    public String uploadPaymentTermsFile(String...fileName) {
        String file = fileName.length == 0 ? "certif_pdf.pdf" : fileName[0];
        PAYMENT_TERMS_FILE_UPLOAD.sendKeys(LINK_TO_RESOURCES_FILES + file);
        PAYMENT_TERMS_FILE_REMOVE_BUTTON.shouldBe(Condition.visible);
        waitForAjax();
        return file;
    }

    public void saveSettingsOfPaymentTermsPopup() {
        SAVE_PAYMENT_TERMS_BUTTON.click();
        waitForAjax();
    }

    /**
     * switch 'Test client'
     */
    public void switchTestClient(boolean isInnerClient) {
        markUnMarkCheckbox(isInnerClient, TEST_CLIENT_CHECKBOX);
    }

    /**
     * switch 'Seasonal client'
     */
    public void switchSeasonClient(boolean isSeasonalClient) {
        markUnMarkCheckbox(isSeasonalClient, SEASON_CLIENT_CHECKBOX);
    }

    /**
     * switch - Cooperation type - 'Publisher'
     */
    public void switchPublisherDirection(boolean isPublisherDirection) {
        markUnMarkCheckbox(isPublisherDirection, PUBLISHERS_DIRECTION_CHECKBOX);
    }

    /**
     * switch - Cooperation type - 'Products'
     */
    public void switchProductsDirection(boolean isProductsDirection) {
        markUnMarkCheckbox(isProductsDirection, PRODUCTS_DIRECTION_CHECKBOX);
    }

    /**
     * choose - Cooperation type - 'Publisher'
     */
    public void selectPublisherDirection(boolean isPublisherDirection, String publishersCurator) {
        if (isPublisherDirection)
            helpersInit.getBaseHelper().selectCustomValue(PUBLISHERS_DIRECTION_SELECT, publishersCurator);
    }

    /**
     * choose - Cooperation type - 'Products'
     */
    public void selectProductsDirection(boolean isProductsDirection, String productsCurator) {
        if (isProductsDirection)
            helpersInit.getBaseHelper().selectCustomValue(PRODUCTS_DIRECTION_SELECT, productsCurator);
    }

    /**
     * click 'Save' button
     */
    public void saveClient() {
        SAVE_BUTTON.scrollTo().click();
        checkErrors();
    }

    /**
     * set cooperation type
     */
    public void setCooperationTypes(List<String> cooperationType) {
        cooperationType.forEach(i ->
                helpersInit.getBaseHelper().dragAndDrop($("#" + i), PANEL_RIGHT)
                );
    }

    /**
     * choose 'Publisher currency'
     */
    public void choosePublisherCurrency(boolean isPublisherDirection) {
        if (isPublisherDirection) {
            helpersInit.getBaseHelper().selectRandomValue(PUBLISHER_CURRENCY_SELECT);
        }
    }

    /**
     * choose 'Status'
     */
    public void chooseStatus(String status) {
        helpersInit.getBaseHelper().selectCustomValue(STATUS_SELECT, status);
    }

    /**
     * check 'Login'
     */
    public boolean checkLogin(String login) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LOGIN_FIELD.text()).equals(login);
    }

    /**
     * check 'Status'
     */
    public boolean checkStatus(String status) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(STATUS_SELECT, status);
    }

    /**
     * check 'Test client'
     */
    public boolean checkIsTestClient(boolean isTestClient) {
        return checkIsCheckboxSelected(isTestClient, TEST_CLIENT_CHECKBOX);
    }

    /**
     * check 'Seasonal client'
     */
    public boolean checkSeasonClient(boolean isSeasonalClient) {
        return checkIsCheckboxSelected(isSeasonalClient, SEASON_CLIENT_CHECKBOX);
    }

    /**
     * check select 'Dashboard currency'
     */
    public boolean checkCurrency(String currency) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(CURRENCY_EDIT_SELECT, currency);
    }

    /**
     * check publisher direction and manager
     */
    public boolean checkPublisherCurator(boolean isPublisherDirection, String publishersCurator) {
        return !isPublisherDirection || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(PUBLISHERS_DIRECTION_SELECT, publishersCurator) &&
                (!isCreateWebsite || PUBLISHERS_DIRECTION_EDIT_CHECKBOX.isSelected());
    }

    /**
     * check products direction and manager
     */
    public boolean checkProductsCurator(boolean isProductsDirection, String productsCurator) {
        return !isProductsDirection || helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(PRODUCTS_DIRECTION_SELECT, productsCurator) &&
                (!isCreateWebsite || PRODUCTS_DIRECTION_EDIT_CHECKBOX.isSelected());
    }

    /**
     * check 'E-mail'
     */
    public boolean checkEmail(String email) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(EMAIL_INPUT.val()).equals(email);
    }

    /**
     * check 'Phone'
     */
    public boolean checkPhone(String phone) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PHONE_INPUT.val()).equals(phone);
    }

    /**
     * check 'Skype'
     */
    public boolean checkSkype(String skype) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SKYPE_INPUT.val()).equals(skype);
    }

    /**
     * check 'Telegram'
     */
    public boolean checkTelegram(String telegram) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TELEGRAM_INPUT.val()).equals(telegram);
    }

    /**
     * check 'Full Name'
     */
    public boolean checkFullName(String fullName) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(FULL_NAME_INPUT.val()).equals(fullName);
    }

    /**
     * check block 'Select cooperation types and priorities'
     */
    public boolean checkCooperationType(List<String> cooperationType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(!isCreateWebsite || cooperationType.stream().allMatch(i -> PANEL_RIGHT.find(By.id(i)).isDisplayed()));
    }

    public boolean checkClientLegalEntityFieldValue(String fieldName, String expectedFieldValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(expectedFieldValue, $x(String.format(LEGAL_ENTITY_TABLE_FIELD, fieldName)).text());
    }

}
