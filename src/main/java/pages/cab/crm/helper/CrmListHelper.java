package pages.cab.crm.helper;

import core.base.HelpersInit;

import java.util.Objects;

import static pages.cab.crm.locators.CrmListLocators.*;
import static pages.cab.publishers.locators.widgets.WidgetsListLocators.BLOCK_ADS_ICON;
import static core.helpers.ErrorsHelper.checkErrors;

public class CrmListHelper {

    private final HelpersInit helpersInit;

    private boolean isCreateWebsite;

    public CrmListHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * get clientId
     */
    public String getClientId() {
        if (CLIENT_FIELD_ID.isDisplayed()) {
            String text = CLIENT_FIELD_ID.attr("id");
            return helpersInit.getBaseHelper().getTextAndWriteLog(Objects.requireNonNull(text).substring(text.indexOf("-") + 1));
        }
        return null;
    }

    /**
     * check 'Login'
     */
    public boolean checkLogin(String login) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CRM_LOGIN_FIELD.text()).equals(login);
    }

    /**
     * click 'Find' button
     */
    public void clickFindButton() {
        FIND_SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * check 'Website' field
     */
    public boolean checkWebsite(String website) {
        return !isCreateWebsite || helpersInit.getBaseHelper().getTextAndWriteLog(WEBSITE_FIELD.text()).equals(website);
    }

    /**
     * check 'Status'
     */
    public boolean checkStatus(String status) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(STATUS_SELECT, status);
    }

    /**
     * check 'Inviter'
     */
    public boolean checkInviter(String inviter) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(INVITER_FIELD.attr("data-inviter-login")).equals(inviter);
    }

    /**
     * check 'E-mail' field
     */
    public boolean checkEmail(String email) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(EMAIL_FIELD.text()).equals(email);
    }

    /**
     * check 'Phone' field
     */
    public boolean checkPhone(String phone) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PHONE_FIELD.text()).equals(phone);
    }

    /**
     * check 'Skype' field
     */
    public boolean checkSkype(String skype) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SKYPE_FIELD.text()).equals(skype);
    }

    /**
     * check 'Telegram' field
     */
    public boolean checkTelegram(String telegram) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TELEGRAM_FIELD.text()).equals(telegram);
    }

    /**
     * check 'Main contact person'
     */
    public boolean checkFullName(String fullName) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(FULL_NAME_FIELD.text()).equals(fullName);
    }

    /**
     * check 'publisher manager'
     */
    public boolean checkPublisherCurator(String publishersCurator) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PUBLISHER_MANAGER_FIELD.text()).equals(publishersCurator);
    }

    /**
     * check 'products manager'
     */
    public boolean checkProductsCurator(String productsCurator) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PRODUCTS_MANAGER_FIELD.text()).equals(productsCurator);
    }

    /**
     * check show 'Dashboard' (cabLink) icon
     */
    public boolean checkShowGogolIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(GOGOL_ICON.isDisplayed());
    }

    public boolean isDisplayedAllowedClicksFromClientDomainsOnly() {
        return BLOCK_ADS_ICON.isDisplayed();
    }
}
