package pages.cab.crm.variables;

public class CrmVariables {
    public static final String messageForClientBlockedAccount      = "Dear client,\n" +
            "Your account is blocked due to suspicious activity. Your campaigns have been paused. Please contact your account manager for details.\n" +
            "\n" +
            "Your account manager contacts:\n" +
            "EMAIL: sokwages@ex.ua\n" +
            "SKYPE: ";

    public static final String messageForClientUnBlockedAccount      = "Dear client,\n" +
            "Your account is unblocked. All campaigns are paused, please launch them manually (https://dashboard.mgid.com/advertisers).\n" +
            "We apologize for temporary inconveniences.";

    public static final String messageForCuratorBlockedClient      = "Client 1128 has been blocked due to suspicious activity. Please do not provide any changes or settings in client's account without your team leader approval.";

}
