package pages.cab.products.helpers;

import core.base.HelpersInit;

import static pages.cab.products.locators.WebsitesListLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class WebsiteListHelper {
    private final HelpersInit helpersInit;

    public WebsiteListHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * get site id
     */
    public String getSiteIdFromListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SITE_ID_LOCATOR.getText().trim());
    }

    /**
     * check displayed domain
     */
    public boolean checkDomainAtWebsiteListInterface(String domain) {
        return helpersInit.getBaseHelper().checkDatasetContains(domain, DOMAIN_FIELD.text().trim());
    }

    /**
     * filter submit
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }
}
