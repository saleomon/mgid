package pages.cab.products.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.cab.products.logic.CabTeasersModeration;
import core.helpers.BaseHelper;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TeaserModerationLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class TeaserModerationHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public TeaserModerationHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public boolean checkDisplayingTierOption(String tierOption) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(tierOption.equals("brand") ? BRAND_ICON.exists() : WHITEHAT_ICON.exists());
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(APAC_ICON.isDisplayed());
    }

    /**
     * check displaying apac icon at zion tab
     */
    public boolean checkDisplayingApacIconZionTab() {
        waitForAjax();
        return helpersInit.getBaseHelper().getTextAndWriteLog(APAC_ICON_ZION.isDisplayed());
    }

    public void clickLinkCheckButton() {
        clickInvisibleElementJs(LINK_CHECK_BUTTON.shouldBe(visible));
        checkErrors();
        waitForAjax();
    }

    public void clickLpCheckButton() {
        clickInvisibleElementJs(LP_CHECK_BUTTON.scrollTo().shouldBe(visible));
        checkErrors();
        waitForAjax();
    }

    public void clickImageCheckButton() {
        clickInvisibleElementJs(IMAGE_CHECK_BUTTON.shouldBe(visible));
        checkErrors();
        waitForAjax();
    }

    public void clickTitleCheckButton() {
        clickInvisibleElementJs(TITLE_CHECK_BUTTON.scrollTo().shouldBe(visible));
        checkErrors();
        waitForAjax();
    }

    public void closeZionForm() {
        clickInvisibleElementJs(CLOSE_ZION_POPUP);
        CLOSE_ZION_POPUP.shouldBe(hidden);
    }

    ////////////////////////////////////////////// TAGS CLOUD - START ////////////////////////////////////////////

    /**
     * метод проверки корректной работы по поиску тегов
     */
    public boolean checkWorkSearchTags(String tagType) {
        SelenideElement searchInput;
        if (tagType.matches("title|description")) searchInput = $("[id^='tags_" + tagType + "'] .searchTags");
        else searchInput = TAGS_FORM_SEARCH;

        ElementsCollection list = $$(".tagsBlock [data-name]:not([style*='none'])[data-type=" + tagType + "]");

        String temp = list.get(randomNumbersInt(list.size())).attr("data-name");
        log.info("searchTags - " + temp);
        if (temp.length() > 3) {
            temp = temp.substring(0, 3);
        }
        searchInput.sendKeys(temp);
        String finalTemp = temp;
        list.shouldBe(CollectionCondition.allMatch("checkWorkSearchTags", i -> i.getAttribute("data-name").contains(finalTemp)));
        return true;
    }

    public Multimap<String, String> chooseRandomTags(String tagType, String... massTags) {
        Multimap<String, String> tags = ArrayListMultimap.create();
        String form = massTags.length > 0 ? "#massTagsDialog" : ".tagsDialog";
        List<String> tagsCloudName;

        $$(form + " .tagsBlock [data-name]:not([style*='none'])[data-type=" + tagType + "]").get(3).shouldBe(visible);

        log.info("получаем список елементов тегов для конкретного типа");
        ElementsCollection tagsElements = $$(form + " .tagsBlock [data-name]:not([style*='none'])[data-type=" + tagType + "]");

        log.info("получаем рандомные имена тегов, tagsElements.size() - " + tagsElements.size());
        tagsCloudName = helpersInit.getBaseHelper().getRandomCountValue(tagsElements.size(), 3).stream()
                .map(i -> tagsElements.get(i).shouldBe(visible).attr("data-id"))
                .collect(Collectors.toList());
        log.info("chooseRandomTags: " + tagType + ", size: " + tagsCloudName);

        //выбираем(включаем) их
        tagsCloudName.forEach(i -> {
            clickInvisibleElementJs(tagsElements.findBy(attribute("data-id", i)));
            waitForAjax();
        });

        //записываем отобранные теги в коллекцию tags
        tagsCloudName.forEach(i -> tags.put(tagType, i));

        return tags;
    }

    /**
     * проверка отображения тегов после сохранения
     */
    public boolean checkChooseTags(Multimap<String, String> tags, String tagType, boolean isDeleteTags) {
        String tagsClass = isDeleteTags ? "" : "active";
        ElementsCollection tagsElements = $$(".tagsBlock [data-name]:not([style*='none']).tag[data-type=" + tagType + "]");

        return tagsElements.size() > 0 &
                tags.get(tagType).stream()
                        .allMatch(i ->
                                tagsElements.findBy(attribute("data-id", i))
                                        .attr("class")
                                        .contains(tagsClass)
                        );
    }

    /**
     * сохраняем форму
     */
    public void saveTagsForm() {
        TAGS_FORM_SAVE.click();
        waitForAjax();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
    }

    /**
     * появляющийся поп-ап после инлайн редактирования тайтла или lp с возможностью удалить теги
     */
    public void clickButtonDeleteTags() {
        log.info("clickButtonDeleteTags");
        TAGS_DELETE_BUTTON.shouldBe(visible).click();
        waitForAjax();
        waitForAjaxLoader();
    }

    /**
     * появляющийся поп-ап после инлайн редактирования тайтла или lp с возможностью сохранить теги
     */
    public void clickButtonSaveTags() {
        log.info("clickButtonSaveTags");
        TAGS_SAVE_BUTTON.shouldBe(visible).click();
        TAGS_SAVE_BUTTON.shouldBe(hidden, ofSeconds(6));
        waitForAjax();
        waitForAjaxLoader();
    }

    ////////////////////////////////////////////// TAGS CLOUD - FINISH ////////////////////////////////////////////

    /**
     * select all teasers checkboxes
     */
    public void selectAllTeasers() {
        clickInvisibleElementJs(SELECT_ALL_CHECKBOXES_LINK);
    }

    /**
     * choose mass actions
     */
    public void chooseMassActions(CabTeasersModeration.MassAction action) {
        MASS_ACTIONS_SELECT.selectOptionByValue(action.getMassAction());
    }

    public void clickChooseRejectReason() {
        REJECT_REASON_BUTTON_MASS.click();
        checkErrors();
    }

    /**
     * SUBMIT по МАССОВЫМ ДЕЙСТВИЯМ
     */
    public void massActionSubmit() {
        MASS_ACTION_SUBMIT.click();
        checkErrors();
    }

    /**
     * изменяем тайтл инлайн
     */
    public void editInLineTitle(String...title) {
        String curTitle = title.length > 0 ? title[0] : "Test's title_edit_" + BaseHelper.getRandomWord(2);
        FILTER_BUTTON.scrollTo();
        INLINE_TITLE_LABEL.shouldBe(visible).doubleClick();
        sendKey(INLINE_TITLE_INPUT.shouldBe(visible), curTitle + Keys.ENTER);
        INLINE_TITLE_INPUT.shouldBe(hidden);
        waitForAjax();
    }

    /**
     * изменяем тайтл инлайн in Zion Form
     */
    public void editInLineTitleInZion(String...title) {
        INLINE_TITLE_ZION_LABEL.shouldBe(visible).doubleClick();
        String curTitle = title.length > 0 ? title[0] : "Test's title_edit_zion_" + BaseHelper.getRandomWord(2);
        sendKey(INLINE_TITLE_ZION_INPUT.shouldBe(visible), curTitle + Keys.ENTER);
        INLINE_TITLE_ZION_INPUT.shouldBe(hidden);
        waitForAjax();
    }

    /**
     * Выбор рандомного лендинга в интерфейсе КАРАНТИН ТИЗЕРОВ
     */
    public void editLandingInline() {
        INLINE_LANDING_LABEL.shouldBe(visible, ofSeconds(10)).click();
        INLINE_LANDING_SELECT.findAll(By.tagName("option")).get(0).shouldBe(visible);
        String landing = helpersInit.getBaseHelper().selectRandomText(INLINE_LANDING_SELECT);
        INLINE_LANDING_SUBMIT_BUTTON.click();
        waitForAjax();
        helpersInit.getBaseHelper().getTextAndWriteLog(landing);
    }

    /**
     * Выбор рандомного лендинга в интерфейсе СПИСОК ТИЗЕРОВ через МАССОВЫЕ ДЕЙСТВИЯ
     */
    public void editLandingInMassAction() {
        selectAllTeasers();
        chooseMassActions(CabTeasersModeration.MassAction.LANDING);
        String landing = helpersInit.getBaseHelper().selectRandomValue(MASS_LANDING_SELECT.shouldBe(visible));
        massActionSubmit();
        waitForAjax();
    }

    /**
     * Approve teaser by mass action
     */
    public void approveTeaserMassAction() {
        selectAllTeasers();
        chooseMassActions(CabTeasersModeration.MassAction.APPROVE);
        massActionSubmit();
        waitForAjax();
    }

    /**
     * check is teaser ids locator visible
     */
    public boolean checkPosibilityOfEditingCallToAction() {
        return INLINE_CALL_TO_ACTION_LABEL.isDisplayed();
    }

    /**
     * check editing description inline
     */
    public String editDescriptionInline(String teaserDescription) {
        String description = teaserDescription == null ?
                "Description " + BaseHelper.getRandomWord(3) :
                teaserDescription;
        INLINE_DESCRIPTION_LABEL.scrollIntoView(true).doubleClick();
        sendKey(DESCRIPTION_INPUT, description);
        CATEGORY_LABEL.click();
        waitForAjax();
        sleep(3000);
        return description;
    }

    /**
     * check editing call to action inline
     */
    public String editCallToActionInline(String callToAction) {
        String description = callToAction == null ?
                "callToAction " + BaseHelper.getRandomWord(3) :
                callToAction;
        INLINE_CALL_TO_ACTION_LABEL.scrollIntoView(true).doubleClick();
        sendKey(CALL_TO_ACTION_INPUT, description);
        INLINE_DESCRIPTION_LABEL.click();
        waitForAjaxLoader();
        return description;
    }

    public void approveTeaser() {
        APPROVE_ICON.scrollTo().shouldBe(visible).click();
        waitForAjax();
        if (CONTINUE_WITHOUT_LINKING.isDisplayed()) CONTINUE_WITHOUT_LINKING.click();
        waitForAjax();
        checkErrors();
    }

    public void rejectTeaser(String... subnetId) {
        if (REJECT_ICON.isDisplayed()) {
            rejectIconClick();
            chooseRejectReason("7", subnetId);
            saveRejectReasonForm();
        }
    }

    public void rejectIconClick() {
        log.info("rejectIconClick");
        REJECT_ICON.shouldBe(visible).scrollTo().click();
        REJECT_FORM.shouldBe(visible);
    }

    /**
     * метод выбора причины отклонения тизера и его отклонение
     * numberOfReason - принимает порядковый номер причины 0-82 и причину remoderation
     * Subnet(0/2/3/5) - по умолчанию - 0
     */
    public void chooseRejectReason(String numberOfReason, String... subnetId) {
        String subnet = subnetId.length > 0 ? subnetId[0] : "0";

        //choose reject reason
        executeJavaScript(
                "var numb = " + numberOfReason + ";" +
                        "$('.rejection-reason[data-subnet=" + subnet + "]')[numb].click();" +
                        "$('.addCommentPlus[data-subnet=" + subnet + "]')[numb].click();" +
                        "$('.commentField[data-subnet=" + subnet + "]')[numb].append('test sok');");
        log.info("Comment for reason was filled");
    }

    /**
     * сохраняем форму "Select the reason for rejection"
     */
    public void saveRejectReasonForm() {
        REJECT_FORM_SUBMIT_BUTTON.shouldBe(visible).hover().click();
        log.info("Changes were saved");
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * Проставляем или убираем google add manager
     */
    public void setRemoveGoogleAddManager(boolean isEnabled) {
        if(isEnabled && !ICON_GOOGLE_ADD_MANAGER_ENABLED.isDisplayed()) {
            ICON_GOOGLE_ADD_MANAGER_DISABLED.scrollTo().click();
            ICON_GOOGLE_ADD_MANAGER_ENABLED.shouldBe(visible);
        } else if (!isEnabled && !ICON_GOOGLE_ADD_MANAGER_DISABLED.isDisplayed()) {
            ICON_GOOGLE_ADD_MANAGER_ENABLED.scrollTo().click();
            ICON_GOOGLE_ADD_MANAGER_DISABLED.shouldBe(visible);
        }
        waitForAjax();
        checkErrors();
        log.info("The state of google add manager was changed - " + isEnabled);
    }

    public void loadImageCloudinaryInline(String imageName) {
        String image = imageName == null ?
                LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg" :
                LINK_TO_RESOURCES_IMAGES + imageName;
        CLOUDINARY_IMAGE_FIELD.scrollTo().sendKeys(image);
        waitForAjax();

        helpersInit.getBaseHelper().getTextAndWriteLog(CLOUDINARY_IMAGE_INPUT.val());
    }
}
