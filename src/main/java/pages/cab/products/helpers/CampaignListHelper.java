package pages.cab.products.helpers;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.Map;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.CampaignsListLocators.*;
import static core.helpers.BaseHelper.markUnMarkCheckbox;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class CampaignListHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public CampaignListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * get campaign id from client list interface
     */
    public String getCampaignId() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_ID_LOCATOR.getText().trim());
    }

    /**
     * check name of campaign at campaign list interface
     */
    public boolean checkCampaignNameListInterface(String campaignName) {
        return helpersInit.getBaseHelper().checkDatasetEquals(campaignName, CAMPAIGN_NAME_TEXT.getAttribute("title"));
    }

    /**
     * clear publisher filter
     */
    public boolean clearPublisherFilter() {
        if (FILTER_WIDGET_OFF_ICON.isDisplayed()) {
            FILTER_WIDGET_OFF_ICON.click();
            FILTER_WIDGET_CLEAR_POPUP_ACCESS_BUTTON.shouldBe(visible, ofSeconds(10)).click();
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(!FILTER_WIDGET_OFF_ICON.isDisplayed());
    }

    /**
     * check state filter after set widget
     */
    public boolean isFilterWidgetShow(String filterId) {
        if ($("[id^='filter_" + filterId + "']:not([style='opacity: 0.5;'])").exists()) {
            clearPublisherFilter();
            return helpersInit.getBaseHelper().getTextAndWriteLog($("[id^='filter_" + filterId + "'][style='opacity: 0.5;']").shouldBe(visible, ofSeconds(10)).exists());
        } else {
            log.info("filterWidget_" + filterId + " - Isn't exists");
        }
        return false;
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(APAC_ICON.isDisplayed());
    }

    /**
     * check hint apac's icon
     */
    public boolean checkHintApacIcon(String[] regions) {
        int counter = 0;
        for (String region : regions) {
            switch (region) {
                case "India":
                    if (APAC_ICON.getAttribute("title").contains("India")) counter++;
                    break;
                case "Malaysia":
                    if (APAC_ICON.getAttribute("title").contains("Malaysia")) counter++;
                    break;
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(regions.length == counter);
    }

    /**
     * click icon 'Clone'
     */
    public void cloneIconClick() {
        CLONE_ICON.shouldBe(exist, visible).scrollTo().click();
        waitForAjax();
        checkErrors();
        CLONE_FORM.shouldBe(visible, ofSeconds(20));
    }

    /**
     * set name for clone campaign
     */
    public String setCloneName() {
        String name = "CloneSokAuto_" + BaseHelper.getRandomWord(7);
        CLONE_NAME_INPUT.sendKeys(name);
        return helpersInit.getBaseHelper().getTextAndWriteLog(name);
    }

    /**
     * set clone options
     */
    public void setOptionsForClone(boolean isCopyCategory, boolean isCopyLanguage, Map<String, Object> options, boolean isEnableOption) {
        // switch copy category/language checkbox
        markUnMarkCheckbox(isCopyCategory, CLONE_COPY_CATEGORY_CHECKBOX);
        markUnMarkCheckbox(isCopyLanguage, CLONE_COPY_LANGUAGE_CHECKBOX);

        for (Map.Entry<String, Object> entry : options.entrySet()) {
            switch (entry.getKey()) {
                case "showName" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_SHOW_NAME_CHECKBOX);
                case "isWhite" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_IS_WHITE_CHECKBOX);
                case "onlySameLanguageWidgets" -> markUnMarkCheckbox(isEnableOption, CLONE_USE_ONLY_SAME_LANGUAGE_WIDGETS_CHECKBOX);
                case "copySchedule" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_SCHEDULE_CHECKBOX);
                case "copyAdLivetime" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_AD_LIFETIME_CHECKBOX);
                case "limits" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_LIMITS_CHECKBOX);
                case "geoTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_GEO_TARGETING_CHECKBOX);
                case "browserTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_BROWSER_TARGETING_CHECKBOX);
                case "osTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_OS_TARGETING_CHECKBOX);
                case "languageTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_LANGUAGE_TARGETING_CHECKBOX);
                case "connectionTypeTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_CONNECTION_TYPE_TARGETING_CHECKBOX);
                case "selectiveBidding" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_SELECTIVE_BIDDING_CHECKBOX);
                case "conversionSettings" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_CONVERSION_SETTINGS_CHECKBOX);
                case "filters" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_FILTERS_CHECKBOX);
                case "utmCustom" -> markUnMarkCheckbox(isEnableOption, CLONE_COPY_UTM_AND_GA_TAG_CHECKBOX);
                case "mediaSource" -> markUnMarkCheckbox(isEnableOption, CLONE_MARKETING_OBJECTIVE_CHECKBOX);
                case "viewabilityTargeting" -> markUnMarkCheckbox(isEnableOption, CLONE_VIEWABILITY_CHECKBOX);
                default -> {
                }
            }
        }
        checkErrors();
    }

    /**
     * check campaign name inb list interface
     */
    public boolean checkCampaignNameInListInterface(String name) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_NAME_TEXT.attr("title")).equals(name);
    }

    /**
     * check show 'Schedule icon'
     */
    public boolean isShowScheduleIcon(boolean isShow) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SCHEDULE_ICON.isDisplayed()) == isShow;
    }

    /**
     * check show icon 'Filter Except'
     */
    public boolean isFilterExceptSwitchOn() {
        return !helpersInit.getBaseHelper().getTextAndWriteLog(FILTER_EXCEPT_ICON.attr("style").contains("0.5"));
    }

    /**
     * check show icon 'Filter Only'
     */
    public boolean isFilterOnlyEnabled() {
        return !helpersInit.getBaseHelper().getTextAndWriteLog(FILTER_ONLY_ICON.attr("style").contains("0.5"));
    }

    /**
     * check show icon 'Stop filters  by informers'
     */
    public boolean isFiltersStopIconDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(FILTER_STOP_ICON.isDisplayed());
    }

    /**
     * get amount of campaigns at list
     */
    public Integer getAmountOfCampaigns() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAMPAIGN_ID_LOCATORS.size());
    }

    /**
     * is visible search feed icon
     */
    public boolean checkVisibilityOfSearchFeedIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SEARCH_FEED_ICON.isDisplayed());
    }

    /**
     * Pause campaign
     */
    public void pauseCampaign() {
            PAUSE_CAMPAIGN_ICON.shouldBe(visible).scrollTo().click();
            log.info("PAUSE_CAMPAIGN_ICON (play.png) clicked.");
            REJECTED_REASON_SELECT.shouldBe(visible, ofSeconds(20));
            helpersInit.getBaseHelper().selectCustomValue(REJECTED_REASON_SELECT, "Budget constraints");
            log.info("REJECTED_REASON selected.");
            PAUSE_CAMPAIGN_BUTTON.shouldBe(visible).click();
            log.info("PAUSE_CAMPAIGN_BUTTON clicked.");
            LOADING_CAMPAIGN_ICON.shouldBe(hidden, ofSeconds(20));
            checkErrors();
            START_CAMPAIGN_ICON.shouldBe(visible, ofSeconds(10));
            helpersInit.getBaseHelper().refreshCurrentPage();
            log.info("Current page refreshed.");
            log.info("Campaign paused.");
    }

    public boolean checkContextSettingsInListInterface(String targetingValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetingValue,
                CONTEXT_TARGETING_FIELD.findElement(By.xpath("./following-sibling::span[@class='value']")).getText());
    }

    public boolean checkSentimentsSettingsInListInterface(String targetingValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetingValue,
                SENTIMENTS_TARGETING_FIELD.findElement(By.xpath("./following-sibling::span[@class='value']")).getText());
    }
}
