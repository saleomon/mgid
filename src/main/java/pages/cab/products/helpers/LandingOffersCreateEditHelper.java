package pages.cab.products.helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import core.base.HelpersInit;
import core.helpers.BaseHelper;
import org.openqa.selenium.WebElement;

import java.util.*;

import static com.codeborne.selenide.Selenide.*;
import static pages.cab.products.locators.LandingOffersCreateEditLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class LandingOffersCreateEditHelper {
    private final HelpersInit helpersInit;

    public LandingOffersCreateEditHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    private boolean isGenerateNewValues = true;
    private final int numberOfChoosenGeoElements = 5;
    private final int numberOfAttemptsForUniqueValues = 10;

    public LandingOffersCreateEditHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }


    /**
     * filling name of product
     */
    public String fillProductNameField(String name) {
        if (name == null && !isGenerateNewValues) return null;
        if (name != null) {
            clearAndSetValue(PRODUCT_NAME, name);
        } else {
            clearAndSetValue(PRODUCT_NAME, name = BaseHelper.getRandomWord(6));
        }
        waitForAjax();
        return helpersInit.getBaseHelper().getTextAndWriteLog(name);
    }

    /**
     * filling link of page
     */
    public String fillPageLinkField(String link) {
        if (link == null && !isGenerateNewValues) return null;
        if (link != null) {
            clearAndSetValue(PAGE_LINK, link);
        } else {
            clearAndSetValue(PAGE_LINK, link = BaseHelper.getRandomWord(6));
        }
        waitForAjax();
        return helpersInit.getBaseHelper().getTextAndWriteLog(link);
    }

    /**
     * choose all geo for all subnets
     */
    public void chooseAllWhiteGeoForAllSubnets() {
        WHITE_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(true, SELECT_ALL_WHITE_GEO_MGID_CHECKBOX);
        markUnMarkCheckbox(true, SELECT_ALL_WHITE_GEO_ADSKEEPER_CHECKBOX);
    }
    /**
     * choose all geo for all subnets
     */
    public void chooseAllBlackGeoForAllSubnets() {
        BLACK_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(true, SELECT_ALL_BLACK_GEO_MGID_CHECKBOX);
        markUnMarkCheckbox(true, SELECT_ALL_BLACK_GEO_ADSKEEPER_CHECKBOX);
    }
    /**
     * choose casino and gambling geo for mgid
     */
    public void chooseCasinoAndGamblingForMgid() {
        BLACK_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(true, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX);
    }
    /**
     * choose casino and gambling geo for adskeeper
     */
    public void chooseCasinoAndGamblingForAdskeeper() {
        BLACK_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(true, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX);
    }
    /**
     * choose casino and gambling geo for all subnets
     */
    public void chooseCasinoAndGamblingAllSubnets() {
        BLACK_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(true, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX);
        markUnMarkCheckbox(true, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX);
    }
    /**
     * reject casino and gambling geo for all subnets
     */
    public void rejectCasinoAndGamblingAllSubnets() {
        BLACK_LIST_GEO_REGIONS.click();
        markUnMarkCheckbox(false, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX);
        markUnMarkCheckbox(false, SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX);
    }
    public void uploadCertificate(String certificate) {
        if(certificate != null) {
            UPLOAD_CERTIFICATES_BUTTON.sendKeys(certificate);
            waitForAjax();
            checkErrors();
        }
    }

    public String fillOfferManufactured(String offer) {
        if (offer != null) {
            clearAndSetValue(OFFER_MANUFACTURER, offer);
        } else {
            clearAndSetValue(OFFER_MANUFACTURER, offer = BaseHelper.getRandomWord(6));
        }
        waitForAjax();
        return helpersInit.getBaseHelper().getTextAndWriteLog(offer);
    }

    /**
     * check all white geo for all subnets
     */
    public boolean checkWhiteListGeo() {
        ARROW_DOWN_TIER_1_ADSKEEPER.click();
        int numberOfAllWhiteGeoBothSubnets = 8;
        return helpersInit.getBaseHelper().getTextAndWriteLog(ACTIVE_WHITE_GEO_ALL.size() == numberOfAllWhiteGeoBothSubnets && ACTIVE_WHITE_GEO_PART.size() == 1);
    }


    /**
     * get chosen elements mgid
     */
    public List<String> markedRegionsMgid() {
        return MARKED_REGIONS_MGID.texts();
    }

    /**
     * chose white list of regions mgid
     */
    public String chooseWhiteListMgid(String region) {
        if (region == null && !isGenerateNewValues) return null;
        WHITE_LIST_GEO_REGIONS.click();
        waitForAjax();
        if (region == null) {
            int numberOfElement = randomNumbersInt(GEO_REGIONS_MGID.size());
            String regions = GEO_REGIONS_MGID.get(numberOfElement).text();
            GEO_REGIONS_MGID.get(numberOfElement).click();
            waitForAjax();
            return helpersInit.getBaseHelper().getTextAndWriteLog(regions);
        } else {
            if (!$("[class*='tabs_content active'] [id='subnet_0'] [name='" + region + "'] [class*='tierName active']").exists()) {
                $("[class='tabs_content active'] [id='subnet_0'] [name='" + region + "']").click();
                checkErrors();
                return region;
            } else {
                $("[id='content_enabledCountries'] [id='subnet_0'] [name='" + region + "'] [class*='arrow-down']").click();
            }

            return region;
        }
    }

    /**
     * chose white list of countries mgid
     */
    public List<String> chooseWhiteListCountriesMgid(String... list) {
        List<String> choosenElements = new ArrayList<>();
        int index;
        if (list == null && !isGenerateNewValues) return null;
        if (list == null) {
            ENABLED_COUNTRIES_WHITE_LIST_MGID.asFixedIterable().forEach(SelenideElement::click);
            for (int i = 0; i < numberOfChoosenGeoElements; i++) {
                index = randomNumbersInt(DISABLED_COUNTRIES_WHITE_LIST_MGID.size());
                for (int k = 0; k < numberOfAttemptsForUniqueValues && choosenElements.contains(DISABLED_COUNTRIES_WHITE_LIST_MGID.get(index).text()); k++) {
                    index = randomNumbersInt(DISABLED_COUNTRIES_WHITE_LIST_MGID.size());
                }
                choosenElements.add(DISABLED_COUNTRIES_WHITE_LIST_MGID.get(index).text());
                DISABLED_COUNTRIES_WHITE_LIST_MGID.get(index).click();
            }
        } else {
            choosenElements = Arrays.asList(list);
            choosenElements.forEach(elem -> {
                if (!Objects.requireNonNull($x("//*[@id='subnet_0']//*[text()='" + elem + "']").attr("class")).contains("active"))
                    $x("//*[@id='subnet_0']//*[text()='" + elem + "']").click();
            });
        }
        return choosenElements;
    }

    /**
     * make regions not active
     */
    public void inactiveSelectedRegionsBySubnet(String subnet) {
        $$("[id='subnet_" + subnet + "'] [class*='active']").asFixedIterable().forEach(SelenideElement::click);
    }

    /**
     * chose black list of countries adskeeper
     */
    public List<String> chooseBlackListCountriesAdskeeper(String... list) {
        List<String> choosenElements = new ArrayList<>();
        int index;
        if (list == null && !isGenerateNewValues) return null;
        if (list == null) {
            ENABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.asFixedIterable().forEach(SelenideElement::click);
            for (int i = 0; i < numberOfChoosenGeoElements; i++) {
                index = randomNumbersInt(DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.size());
                for (int k = 0; k < numberOfAttemptsForUniqueValues && choosenElements.contains(DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.get(index).text()); k++) {
                    index = randomNumbersInt(DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.size());
                }
                choosenElements.add(DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.get(index).text());
                DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.get(index).click();
            }
        } else {
            choosenElements = Arrays.asList(list);
            choosenElements.forEach(elem -> $x("//*[@id='subnet_0']//*[text()='" + elem + "']"));
        }
        return choosenElements;
    }

    /**
     * chose white list of regions adskeeper
     */
    public String chooseWhiteListAdskeeper(String region, String mgidRegion) {
        if (region == null && !isGenerateNewValues) return null;
        if (mgidRegion != null) GEO_REGIONS_ADSKEEPER.findBy(Condition.exactText(mgidRegion)).click();
        if (region == null) {
            WHITE_LIST_GEO_REGIONS.click();
            waitForAjax();
            int numberOfElement = randomNumbersInt(GEO_REGIONS_ADSKEEPER.size());
            String regions = GEO_REGIONS_ADSKEEPER.get(numberOfElement).text();
            GEO_REGIONS_ADSKEEPER.get(numberOfElement).click();
            if (!$("[class='tabs_content active'] [id='subnet_2'] [name='" + GEO_REGIONS_ADSKEEPER.get(numberOfElement).text() + "'] [class='tierName active-all']").isDisplayed()) {
                GEO_REGIONS_ADSKEEPER.get(numberOfElement).click();
                GEO_REGIONS_ADSKEEPER.get(numberOfElement).click();
            }
            waitForAjax();
            return helpersInit.getBaseHelper().getTextAndWriteLog(regions);
        } else {
            $("[class='tabs_content active'] [id='subnet_2'] [name='" + region + "']").click();
            waitForAjaxLoader();
            checkErrors();
            return helpersInit.getBaseHelper().getTextAndWriteLog(region);
        }
    }

    /**
     * chose black list of regions adskeeper
     */
    public String chooseBlackListAdskeeper(String regionForBlack, String regionForWhite, String mgidRegions) {
        if (regionForBlack == null && !isGenerateNewValues) return null;
        if (mgidRegions.length() > 0) GEO_REGIONS_ADSKEEPER.findBy(Condition.exactText(mgidRegions)).click();
        if (regionForBlack == null) {
            BLACK_LIST_GEO_REGIONS.click();
            waitForAjax();
            int numberOfElement = randomNumbersInt(GEO_REGIONS_ADSKEEPER.size());
            for (int i = 0; i < numberOfChoosenGeoElements && regionForWhite.equalsIgnoreCase(GEO_REGIONS_ADSKEEPER.get(numberOfElement).text()); i++) {
                numberOfElement = randomNumbersInt(GEO_REGIONS_ADSKEEPER.size());
            }
            String regions = GEO_REGIONS_ADSKEEPER.get(numberOfElement).text();
            GEO_REGIONS_ADSKEEPER.get(numberOfElement).click();
            waitForAjax();
            return helpersInit.getBaseHelper().getTextAndWriteLog(regions);
        } else {
            $("[class='tabs_content active'] [id='subnet_2'] [name='" + regionForBlack + "']").click();
            checkErrors();
        }
        return null;
    }

    /**
     * chose black list of regions mgid
     */
    public String chooseBlackListMgid(String regionForBlack, String regionForWhite) {
        if (regionForBlack == null && !isGenerateNewValues) return null;
        if (regionForBlack == null) {
            BLACK_LIST_GEO_REGIONS.click();
            waitForAjax();
            int numberOfElement = randomNumbersInt(GEO_REGIONS_MGID.size());
            for (int i = 0; i < numberOfChoosenGeoElements && regionForWhite.equalsIgnoreCase(GEO_REGIONS_ADSKEEPER.get(numberOfElement).text()); i++) {
                numberOfElement = randomNumbersInt(GEO_REGIONS_ADSKEEPER.size());
            }
            String regions = GEO_REGIONS_MGID.get(numberOfElement).text();
            GEO_REGIONS_MGID.get(numberOfElement).click();
            waitForAjax();
            return helpersInit.getBaseHelper().getTextAndWriteLog(regions);
        } else {
            BLACK_LIST_GEO_REGIONS.click();
            $("[class='tabs_content active'] [id='subnet_0'] [name='" + regionForBlack + "']").click();
            checkErrors();
            return regionForBlack;
        }
    }

    /**
     * save product settings
     */
    public void saveOffer() {
        SAVE_OFFER.click();
//        if (!helpersInit.getMessageHelper().isSuccessMessagesCab()) {
//            SAVE_OFFER.click();
//        }
        waitForAjax();
        checkErrors();
    }

    /**
     * check displayed product name
     */
    public boolean checkProductName(String name) {
        return name == null || helpersInit.getBaseHelper().getTextAndWriteLog(name.equalsIgnoreCase(PRODUCT_NAME.val()));
    }

    /**
     * check displayed page link
     */
    public boolean checkPageLink(String link) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(link.equalsIgnoreCase(PAGE_LINK.val()));
    }

    /**
     * check white list regions mgid
     */
    public boolean checkWhiteListMgid(String region) {
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(region.equalsIgnoreCase(PART_ENABLED_REGIONS_WHITE_MGID.text()));
    }

    /**
     * check white list countries mgid
     */
    public boolean checkWhiteListCountriesMgid(List<String> countries, String region) {
        WHITE_LIST_GEO_REGIONS.click();
        $("[id='content_enabledCountries'] [id='subnet_0'] [name='" + region + "'] [class='arrow-down']").click();
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(ENABLED_COUNTRIES_WHITE_LIST_MGID.texts()).containsAll(countries));
    }

    /**
     * check black list countries adskeeper
     */
    public boolean checkBlackListCountriesAdskeeper(List<String> countries, String region) {
        BLACK_LIST_GEO_REGIONS.click();
        $("[id='content_disabledCountries'] [id='subnet_2'] [name='" + region + "'] [class='arrow-down']").click();
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(ENABLED_COUNTRIES_BLACK_LIST_ADSKEEPER.texts()).containsAll(countries));
    }

    /**
     * check white list region adskeeper
     */
    public boolean checkWhiteListAdskeeper(String region) {
        WHITE_LIST_GEO_REGIONS.click();
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(ALL_ENABLED_REGIONS_WHITE_ADSKEEPER.getText().equalsIgnoreCase(region));
    }

    /**
     * check black list region mgid
     */
    public boolean checkBlackListMgid(String region) {
        BLACK_LIST_GEO_REGIONS.click();
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(ALL_ENABLED_REGIONS_BLACK_MGID.getText().equalsIgnoreCase(region));
    }

    /**
     * check black list region adskeeper
     */
    public boolean checkBlackListAdskeeper(String region) {
        BLACK_LIST_GEO_REGIONS.click();
        return region == null || helpersInit.getBaseHelper().getTextAndWriteLog(PART_ENABLED_REGIONS_BLACK_ADSKEEPER.getText().equalsIgnoreCase(region));
    }
    /**
     * check casino and gambling mgid
     */
    public boolean checkCasinoAndGamblingForMgid() {
        BLACK_LIST_GEO_REGIONS.click();
        WebElement checkbox =  SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX;

        if (checkbox.isSelected()) {
            return true;
        } else {
            checkbox.click();
            return checkbox.isSelected();
        }
    }
    /**
     * check casino and gambling adskeeper
     */
    public boolean checkCasinoAndGamblingForAdskeeper() {
        BLACK_LIST_GEO_REGIONS.click();
        WebElement checkbox =  SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX;




        if (checkbox.isSelected()) {
            return true;
        } else {
            checkbox.click();
            return checkbox.isSelected();
        }
    }
    /**
     * check casino and gambling all subnets
     */
    public boolean checkCasinoAndGamblingAllSubnetsCheckbox() {
        BLACK_LIST_GEO_REGIONS.click();
        WebElement checkbox1 = SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX;
        WebElement checkbox2 = SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX;


        boolean isChecked1 = checkbox1.isSelected();
        boolean isChecked2 = checkbox2.isSelected();


        return isChecked1 && isChecked2;
    }


    public boolean checkDisplayedCertificates(String name) {
        if(name != null) name = name.substring(name.lastIndexOf("/")+1);
        return name == null || helpersInit.getBaseHelper().getTextAndWriteLog($x(String.format(CERTIFICATE_NAME_LABEL, name)).isDisplayed());
    }

    /**
     * clear selected geo settings
     */
    public void clearGeoSettings() {
        List<SelenideElement> list = Arrays.asList(PART_ENABLED_REGIONS_WHITE_MGID, ALL_ENABLED_REGIONS_WHITE_MGID, ALL_ENABLED_REGIONS_WHITE_ADSKEEPER, PART_ENABLED_REGIONS_WHITE_ADSKEEPER,
                ALL_ENABLED_REGIONS_BLACK_MGID, PART_ENABLED_REGIONS_BLACK_MGID, PART_ENABLED_REGIONS_BLACK_ADSKEEPER, ALL_ENABLED_REGIONS_BLACK_ADSKEEPER);

        WHITE_LIST_GEO_REGIONS.click();
        list.stream().filter(SelenideElement::isDisplayed).forEach(SelenideElement::click);

        BLACK_LIST_GEO_REGIONS.click();
        list.stream().filter(SelenideElement::isDisplayed).forEach(SelenideElement::click);
    }

    /**
     * is active united states cdb
     */
    public boolean isMarkedUnitedStatesCdb(String region) {
        $("[id='content_enabledCountries'] [id='subnet_0'] [name='" + region + "'] [class='arrow-down']").click();
        return UNITED_STATES_CDB_ACTIVE.exists();
    }

    /**
     * check error message product form
     */
    public boolean checkErrorMessageProductForm(String message) {
        return helpersInit.getBaseHelper().checkDatasetContains(ERROR_MESSAGE.text(), message);
    }

}
