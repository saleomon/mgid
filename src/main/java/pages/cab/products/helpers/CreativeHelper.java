package pages.cab.products.helpers;

import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.products.locators.CreativeLocators.*;

public class CreativeHelper {
    private final HelpersInit helpersInit;
    private final Logger log;

    public CreativeHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Check requestId in 'Creative' list
     */
    public boolean checkRequestId(String requestId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REQUEST_ID_FIELD.text()).equals(requestId);
    }

    /**
     * Check priority in 'Creative' list
     */
    public boolean checkPriority(String priority) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(PRIORITY_FIELD.text()).equals(priority);
    }

    /**
     * Check amount 'Creative' list
     */
    public boolean checkAmount(String amount) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AMOUNT_FIELD.text()).contains(amount);
    }

    /**
     * Click "Assume task button"
     */
    public void assumeTask(String amountTask, String... customAdType) {
        ASSUME_TASK_BUTTON.shouldBe(visible).click();
        TEASERS_AMOUNT_INPUT.shouldBe(visible).sendKeys(amountTask);
        if (customAdType.length > 0) {
            TEASERS_TYPE_INPUT.selectOptionByValue(customAdType[0]);
        }
        CREATE_TASK_BUTTON.click();

        waitForAjax();
        checkErrors();
        log.info("Assume task clicked");
    }

    /**
     * Get link for teaser creation interface
     */
    public String getLinkToAddTeaserForm() {
        log.info("goToCreateTeaserFromTask");
        if (ADD_TEASER_ICON.isDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_TEASER_ICON.attr("href"));
        }
        return null;
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * check custom adType in taking tasks
     */
    public boolean checkAdTypeInTakingTasks(String type) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(AD_TYPES_LABEL).texts().stream().allMatch(i -> i.equalsIgnoreCase(type)));
    }
}