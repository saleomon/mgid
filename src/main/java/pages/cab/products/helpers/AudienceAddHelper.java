package pages.cab.products.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.Objects;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.visible;
import static pages.cab.products.locators.AudienceAddLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class AudienceAddHelper {
    private final Logger log;
    private final HelpersInit helpersInit;

    public AudienceAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public AudienceAddHelper setSomeFields(boolean setSomeFields) {
        isSetSomeFields = setSomeFields;
        return this;
    }

    private boolean isSetSomeFields = false;

    /**
     * Set audience name
     */
    public String setAudienceName(String audienceName) {
        String name;
        if (AUDIENCE_NAME_INPUT.is(disabled)) {
            log.info("AUDIENCE_NAME_INPUT -> is disabled");
            return null;
        }
        if (isSetSomeFields && audienceName == null) {
            return null;
        } else
            name = Objects.requireNonNullElseGet(audienceName, () -> "Default audience name - " + BaseHelper.getRandomWord(5));
        clearAndSetValue(AUDIENCE_NAME_INPUT, name);
        return helpersInit.getBaseHelper().getTextAndWriteLog(name);
    }

    /**
     * Set audience description
     */
    public String setAudienceDescription(String audienceDescription) {
        String description;
        if (isSetSomeFields && audienceDescription == null) {
            return null;
        } else
            description = Objects.requireNonNullElseGet(audienceDescription, () -> "Default audience description - " + BaseHelper.getRandomWord(5));
        clearAndSetValue(AUDIENCE_DESCRIPTION_INPUT, description);
        return helpersInit.getBaseHelper().getTextAndWriteLog(description);
    }

    /**
     * Choose target in audience interface
     */
    public String setAudienceTarget(String audienceTarget) {
        if (isSetSomeFields && audienceTarget == null) {
            return null;
        } else if (audienceTarget != null) {
            return helpersInit.getBaseHelper().selectCustomValue(AUDIENCE_TARGETS_SELECT, audienceTarget);
        } else
            return helpersInit.getBaseHelper().selectRandomValue(AUDIENCE_TARGETS_SELECT);
    }

    /**
     * Choose Period of validity in audience interface
     */
    public String setAudiencePeriodOfValidity(String audiencePeriodOfValidity) {
        if (isSetSomeFields && audiencePeriodOfValidity == null) {
            return null;
        } else if (audiencePeriodOfValidity != null) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomValue(AUDIENCE_EXPIRE_SELECT, audiencePeriodOfValidity));
        } else
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(AUDIENCE_EXPIRE_SELECT));
    }

    /**
     * Set audience condition
     */
    public String setAudienceCondition(String audienceCondition) {
        if (isSetSomeFields && audienceCondition == null) {
            return null;
        } else if (audienceCondition != null) {
            return helpersInit.getBaseHelper().selectCustomValue(AUDIENCE_CONDITION_SELECT, audienceCondition);
        } else
            return helpersInit.getBaseHelper().selectRandomValue(AUDIENCE_CONDITION_SELECT);
    }

    /**
     * Submit audience
     */
    public void submitAudience() {
        SUBMIT_AUDIENCE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Check audience name
     */
    public boolean checkAudienceName(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(name, AUDIENCE_NAME_INPUT.val());
    }

    /**
     * Check audience description
     */
    public boolean checkAudienceDescription(String name) {
        return helpersInit.getBaseHelper().checkDatasetEquals(name, AUDIENCE_DESCRIPTION_INPUT.val());
    }

    /**
     * Check target in audience interface
     */
    public boolean checkTargetInAudience(String audience) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(AUDIENCE_TARGETS_SELECT, audience);
    }

    /**
     * Check period of validity in audience interface
     */
    public boolean checkExpireInAudience(String expire) {
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(AUDIENCE_EXPIRE_SELECT, expire);
    }

    /**
     * Click 'Add condition' button
     */
    public void addNewCondition() {
        ADD_CONDITION_BUTTON.shouldBe(visible).click();
    }
}