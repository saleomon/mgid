package pages.cab.products.helpers;

import com.codeborne.selenide.Condition;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.LandingOffersListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class LandingOffersListHelper {
    private final Logger log;
    private final HelpersInit helpersInit;

    public LandingOffersListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * open form create product
     */
    public void openCreateProductForm() {
        clickInvisibleElementJs(ADD_NEW_PRODUCT_BUTTON.shouldBe(visible, ofSeconds(8)));
        waitForAjax();
        checkErrors();
    }

    /**
     * find product by name at list interface
     */
    public void findProduct(String productName) {
        clearAndSetValue(SEARCH_FIELD, productName);
        SEARCH_BUTTON.click();
        if (!EDIT_PRODUCT_BUTTON.isDisplayed()) {
            SEARCH_BUTTON.click();
        }
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * delete product
     */
    public void deleteProduct() {
        DELETE_BUTTON.shouldBe(visible).click();
        CONFIRM_DELETION_BUTTON.click();
        waitForAjax();
        checkErrors();
        DELETE_BUTTON.shouldBe(Condition.hidden);
    }

    /**
     * open product form for edit
     */
    public void openEditProductForm() {
        EDIT_PRODUCT_BUTTON.click();
        waitForAjax();
        checkErrors();
        PRODUCT_NAME.shouldBe(visible);
    }

    /**
     * check product at list interface white geo mgid
     */
    public boolean checkWhiteListGeoMgid(String region, List<String> listOfCountries) {
        List<String> listOfDisplayedCountries = Arrays.asList(WHITE_LIST_GEO_COUNTRIES_MGID.text().split(", "));
        return helpersInit.getBaseHelper().getTextAndWriteLog(WHITE_LIST_GEO_MGID.text().equalsIgnoreCase(region)) && helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(listOfCountries).containsAll(listOfDisplayedCountries));
    }

    /**
     * check product at list interface black geo mgid
     */
    public boolean checkBlackListGeoMgid(String region) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BLACK_LIST_GEO_MGID.text().equalsIgnoreCase(region));
    }

    /**
     * check product at list interface white geo adskeeper
     */
    public boolean checkWhiteListGeoAdskeeper(String region) {
        log.info("List: " + WHITE_LIST_GEO_ADSKEEPER.text());
        return helpersInit.getBaseHelper().getTextAndWriteLog(WHITE_LIST_GEO_ADSKEEPER.text().equalsIgnoreCase(region));
    }

    /**
     * check product at list interface black geo adskeeper
     */
    public boolean checkBlackListGeoAdskeeper(String region, List<String> listOfCountries) {
        List<String> listOfDisplayedCountries = Arrays.asList(BLACK_LIST_GEO_COUNTRIES_ADSKEEPER.text().split(", "));
        return helpersInit.getBaseHelper().getTextAndWriteLog(BLACK_LIST_GEO_ADSKEEPER.text().equalsIgnoreCase(region)) && helpersInit.getBaseHelper().getTextAndWriteLog(new HashSet<>(listOfCountries).containsAll(listOfDisplayedCountries));
    }

    /**
     * check visibility add product button
     */
    public boolean checkVisibilityAddProductButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_NEW_PRODUCT_BUTTON.isDisplayed());
    }

    /**
     * check visibility delete product button
     */
    public boolean checkVisibilityDeleteProductButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DELETE_BUTTON.isDisplayed());
    }

    /**
     * check visibility search results
     */
    public boolean checkVisibilitySearchResults() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SEARCH_RESULTS.isDisplayed());
    }

    /**
     * check visibility edit product button
     */
    public boolean checkVisibilityEditProductButton() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(EDIT_PRODUCT_BUTTON.isDisplayed());
    }

    /**
     * check displayed error message
     */
    public boolean checkErrorMessageField(String message) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($x("//*[text()='" + message + "']").isDisplayed());
    }

    /**
     * get link from go to teasers icon
     */
    public String getLinkOfLinkRedirection() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(GO_TO_TEASERS_ICON.getAttribute("href"));
    }

}
