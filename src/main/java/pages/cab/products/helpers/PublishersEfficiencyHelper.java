package pages.cab.products.helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.HashMap;

import static com.codeborne.selenide.Selenide.$$;
import static pages.cab.products.locators.PublishersEfficiencyLocators.DATA_UID_FIELD;

public class PublishersEfficiencyHelper {

    public PublishersEfficiencyHelper() {
    }

    /**
     * get list widgets ID and their Factor - to MAP
     */
    public HashMap<String, String> getWidgetsIdAndFactor() {
        HashMap<String, String> map = new HashMap<>();
        ElementsCollection list = $$(DATA_UID_FIELD);
        for (SelenideElement s : list) {
            map.put(s.attr("data-uid"),
                    s.find(By.className("qf_value")).text().trim());
        }
        return map;
    }
}
