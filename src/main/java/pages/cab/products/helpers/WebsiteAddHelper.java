package pages.cab.products.helpers;

import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.Objects;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.products.locators.WebsitesAddLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class WebsiteAddHelper {
    private final HelpersInit helpersInit;
    boolean isGenerateNewValues = false;

    public WebsiteAddHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * fill domain
     */
    public String fillDomain(String domain) {
        if (domain == null && !isGenerateNewValues) return null;
        if (domain != null) {
            clearAndSetValue(DOMAIN_INPUT.shouldBe(visible), domain);
            return helpersInit.getBaseHelper().getTextAndWriteLog(domain);
        } else {
            String domainRandom = "https://" + BaseHelper.getRandomWord(5) + ".com";
            clearAndSetValue(DOMAIN_INPUT.shouldBe(visible), domainRandom);
            return domainRandom;
        }
    }

    /**
     * fill comment
     */
    public String fillComment(String comment) {
        if (comment == null && !isGenerateNewValues) return null;
        if (comment != null) {
            clearAndSetValue(COMMENT_INPUT, comment);
            return helpersInit.getBaseHelper().getTextAndWriteLog(comment);
        } else {
            String commentRandom = "Hello, " + BaseHelper.getRandomWord(7);
            clearAndSetValue(COMMENT_INPUT, commentRandom);
            return commentRandom;
        }
    }

    /**
     * change state allowAddingTeasersSubdomain checkbox
     */
    public void allowAddingTeasersSubdomain(boolean state) {
        markUnMarkCheckbox(state, ALLOW_ADDING_TEASER_SUBDOMAIN_CHECKBOX);
        helpersInit.getBaseHelper().getTextAndWriteLog(state);
    }

    /**
     * change state allowAddingDomainToPartners checkbox
     */
    public void allowAddingDomainToPartners(boolean state) {
        markUnMarkCheckbox(state, ALLOW_ADDING_DOMAIN_OTHER_CLIENTS_CHECKBOX);
        helpersInit.getBaseHelper().getTextAndWriteLog(state);
    }

    /**
     * save settings
     */
    public void saveSettings() {
        SAVE_BUTTON.click();
        checkErrors();
        helpersInit.getBaseHelper().checkAlertAndClose();
        waitForAjax();
    }

    /**
     * save settings
     */
    public void submitSettings() {
        SUBMIT_BUTTON.click();
        checkErrors();
        helpersInit.getBaseHelper().checkAlertAndClose();
        waitForAjax();
    }

    /**
     * check domain
     */
    public boolean checkDomain(String damain) {
        return helpersInit.getBaseHelper().checkDatasetContains(damain, DOMAIN_FIELD.text());
    }

    /**
     * check comment
     */
    public boolean checkComment(String comment) {
        return helpersInit.getBaseHelper().checkDatasetContains(comment, Objects.requireNonNull(COMMENT_INPUT.val()));
    }

    /**
     * check AllowAddingTeasersSubdomain
     */
    public boolean checkAllowAddingTeasersSubdomain(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, ALLOW_ADDING_TEASER_SUBDOMAIN_CHECKBOX.isSelected());
    }

    /**
     * check AllowAddingDomainToPartners
     */
    public boolean checkAllowAddingDomainToPartners(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, ALLOW_ADDING_DOMAIN_OTHER_CLIENTS_CHECKBOX.isSelected());
    }
}
