package pages.cab.products.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.ArrayList;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.FilterBlockedPublishersLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class FilterBlockedPublishersHelper {

    private HelpersInit helpersInit;
    private Logger log;

    public FilterBlockedPublishersHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * load file to form
     */
    public void chooseLoadFile(String file) {
        CHOOSE_FILE_BUTTON.shouldBe(visible)
                .scrollTo()
                .sendKeys(file);
        helpersInit.getBaseHelper().getTextAndWriteLog(file);
    }

    public boolean isMarkedWidgetInFilter(String... widgetId) {
        SEARCH_SOURCE.sendKeys(widgetId[0]);
        return helpersInit.getBaseHelper().getTextAndWriteLog(GREEN_SOURCE.exists() &&
        Objects.requireNonNull($x("//div[contains(.,'" + widgetId[0] + "') and @class='row-content']/..").getAttribute("class")).contains("green"));
    }


    /**
     * choose actions in 'Listed publishers'
     */
    public void chooseActionWithLoadFile(String action) {
        ACTIONS_WITH_LOAD_FILE_SELECT.selectOptionByValue(action);
    }

    /**
     * click button 'Save'
     */
    public void saveForm() {
        SAVE_BUTTON.click();
        waitForAjax();
        checkErrors();
        log.info("saveForm");
    }

    /**
     * check load file - on success
     */
    public boolean isLoadFileSuccess() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SUCCESS_LOAD_FILE_LABEL.shouldBe(visible, ofSeconds(10)).isDisplayed());
    }

    /**
     * switch on widget in filter
     */
    public boolean switchOnWidgetInFilter(String widgetId) {
        if (getCountCheckWidgets() == 0) {
            SEARCH_WIDGET_INPUT.shouldBe(visible).sendKeys(widgetId);
            waitForAjax();
            WIDGET_LABEL.shouldBe(visible, ofSeconds(8));
            WIDGET_LABEL.shouldBe(text(widgetId));
            if (WIDGET_LABEL.text().equals(widgetId)) {
                WIDGET_LABEL.click();
                CHECK_STAY_WIDGETS_FILTER.click();
                log.info("getCountCheckWidgets must be = 1, now = " + getCountCheckWidgets());
                return getCountCheckWidgets() == 1;
            } else {
                log.error("WIDGET_LABEL isn't displayed");
            }
        }
        return false;
    }

    /**
     * get count checked widgets
     */
    public Integer getCountCheckWidgets() {
        return helpersInit.getBaseHelper().parseInt(CHECKED_WIDGET_COUNT);
    }

    /**
     * click to open checked publishers
     */
    public void openCheckedPublishers() {
        CHECKED_WIDGET_COUNT.click();
        if (!CHECKED_WIDGET_COUNT.text().equals("0")) IS_CHECKED_CLASS.shouldBe(visible);
        IS_UNCHECKED_CLASS.shouldBe(hidden);
        log.info("openCheckedPublishers");
    }

    /**
     * get data checked widgets
     */
    public ArrayList<String> getDataCheckedWidgets() {
        return new ArrayList<>($$(WIDGETS_ROW).texts());
    }
}
