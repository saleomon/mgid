package pages.cab.products.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.Arrays;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.CreativeRequestsListLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.BaseHelper.waitForAjaxLoader;
import static core.helpers.ErrorsHelper.checkErrors;

public class CreativeRequestsListHelper {
    private HelpersInit helpersInit;
    private Logger log;

    public CreativeRequestsListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Get items from 'Conditions' block
     */
    public String getTeaserRequestLabel(String value) {
        String d = Arrays.stream(TEASER_REQUEST_LABELS_BLOCK
                .text()
                .split("\n\n"))
                .filter(val -> val.contains(value))
                .findFirst()
                .get()
                .split("\n")[1];

        return helpersInit.getBaseHelper().getTextAndWriteLog(d);
    }

    /**
     * Check webSite in list interface
     */
    public boolean checkWebSiteInListInterface(String webSite) {
        return helpersInit.getBaseHelper().checkDatasetEquals(webSite, WEB_SITES_FIELD.shouldBe(visible).text());
    }

    /**
     * Check 'Link for the ads:'
     */
    public boolean checkLinkForAds(String linkForAds) {
        return helpersInit.getBaseHelper().checkDatasetContains("https://" + linkForAds, getTeaserRequestLabel("Link for the ads:"));
    }

    /**
     * Check 'Preview link:'
     */
    public boolean checkPreviewLink(String previewLink) {
        return helpersInit.getBaseHelper().checkDatasetEquals(previewLink, getTeaserRequestLabel("Preview link:"));
    }

    /**
     * Check 'Legal approve:'
     */
    public boolean checkLegalApprove(String legalApprove) {
        return helpersInit.getBaseHelper().checkDatasetEquals(legalApprove, getTeaserRequestLabel("Legal approve:"));
    }

    /**
     * Check 'Targeted audience:'
     */
    public boolean checkTargetedAudience(String targetedAudience) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetedAudience, getTeaserRequestLabel("Targeted audience:"));
    }

    /**
     * Check 'Offer review:'
     */
    public boolean checkOfferReview(String offerReview) {
        return helpersInit.getBaseHelper().checkDatasetEquals(offerReview, getTeaserRequestLabel("Offer review:"));
    }

    /**
     * Check 'Comment for teasermakers:'
     */
    public boolean checkCommentForTeasermakers(String commentForTeasermakers) {
        return helpersInit.getBaseHelper().checkDatasetEquals(commentForTeasermakers, getTeaserRequestLabel("Comment for teasermakers:"));
    }

    /**
     * Check 'Comment for moderators:'
     */
    public boolean checkCommentForModerators(String commentForModerators) {
        return helpersInit.getBaseHelper().checkDatasetEquals(commentForModerators, getTeaserRequestLabel("Comment for moderators:"));
    }

    /**
     * Check 'Amount'
     */
    public boolean checkAmount(String amount) {
        return helpersInit.getBaseHelper().checkDatasetEquals(amount, AMOUNT_LABEL.text());
    }

    /**
     * Check 'CPC'
     */
    public boolean checkPrice(String price) {
        return helpersInit.getBaseHelper().checkDatasetContains(price, PRICE_LABEL.text().trim().replace("\u00A2", ""));
    }

    /**
     * Check 'Priority'
     */
    public boolean checkPriority(String priority) {
        return helpersInit.getBaseHelper().checkDatasetEquals(priority, PRIORITY_FIELD.text());
    }

    /**
     * Check request status
     */
    public boolean checkStatusRequest(String status) {
        log.info("checkStatusRequest - " + status);
        return helpersInit.getBaseHelper().checkDataset(true, STATUS_LABEL.shouldBe(text(status), ofSeconds(16)).is(text(status)));
    }

    /**
     * Approve teaser request
     */
    public boolean approveTeaserRequest() {
        if (ACCEPT_REQUEST_ICON.isDisplayed()) {
            ACCEPT_REQUEST_ICON.click();
            LOADING_ICON.shouldBe(hidden, ofSeconds(10));
            GO_TO_TASKS_ICON.shouldBe(visible);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(checkStatusRequest("New"));
    }

    public void clickApproveIcon() {
        if (ACCEPT_REQUEST_ICON.isDisplayed()) {
            ACCEPT_REQUEST_ICON.click();
            waitForAjaxLoader();
            checkErrors();
        }
    }

    /**
     * Delete creative request
     */
    public boolean deleteTeaserRequest() {
        if (CLOSE_REQUEST_ICON.isDisplayed()) {
            CLOSE_REQUEST_ICON.click();
            REQUEST_CLOSING_REASON_INPUT.sendKeys("test reason");
            REJECT_REQUEST_BUTTON.click();
            LOADING_ICON.shouldBe(hidden, ofSeconds(10));
            CLOSE_REQUEST_ICON.shouldBe(hidden);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(checkStatusRequest("Closed"));
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * open add product form
     */
    public void openFormAddProduct() {
        ADD_PRODUCT_ICON.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * check visibility of icon add product
     */
    public boolean checkVisibilityAddProductIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ADD_PRODUCT_ICON.isDisplayed());
    }

    public boolean isDisplayedAdTypeInputs() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($(AD_TYPE_INPUT).isDisplayed());
    }

    public void setPgType(String value) {
        PG_INPUT.val(value);
    }

    public void setPg13Type(String value) {
        PG13_INPUT.val(value);
    }

    public void setRType(String value) {
        R_INPUT.val(value);
    }

    public void setNc17Type(String value) {
        NC17_INPUT.val(value);
    }

    public void setNsfwType(String value) {
        NSFW_INPUT.val(value);
    }

}