package pages.cab.products.helpers;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.List;
import java.util.stream.IntStream;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TargetAddlocators.TARGET_NAME_INPUT;
import static pages.cab.products.locators.TargetListLocators.*;

public class TargetListHelper {
    private HelpersInit helpersInit;
    private Logger log;

    public TargetListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Add new target
     */
    public void addNewTarget() {
        TARGET_ADD_BUTTON.shouldBe(visible).click();
        TARGET_NAME_INPUT.shouldBe(visible, ofSeconds(8));
        log.info("addTargetClick");
    }

    /**
     * Delete client target
     */
    public boolean deleteClientTarget(String targetId) {
        SelenideElement deleteIcon = $(String.format(TARGET_DELETE_ICON, targetId));
        deleteIcon.shouldBe(visible).click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        deleteIcon.shouldBe(hidden, ofSeconds(8));
        return helpersInit.getBaseHelper().checkDataset(true, !deleteIcon.isDisplayed());
    }

    /**
     * Delete all client targets
     */
    public void deleteAllClientTargets() {
        if (TARGET_ID_LOCATORS.size() > 0) {
            List<String> targetIdsList = TARGET_ID_LOCATORS.texts();
            IntStream.range(0, targetIdsList.size()).forEach(i -> deleteClientTarget(targetIdsList.get(i)));
        }
        log.info("There aren't any targets");
    }
}
