package pages.cab.products.helpers;

import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.products.locators.ClientEditLocators.*;
import static core.helpers.BaseHelper.checkIsCheckboxSelected;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class ClientEditHelper {
    private final HelpersInit helpersInit;

    public ClientEditHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * check client email in edit interface
     */
    public boolean checkClientEmailInEdit(String email) {
        return helpersInit.getBaseHelper().checkDatasetEquals(EMAIL_CLIENT_INPUT.shouldBe(visible).val(), email);
    }

    /**
     * check 'Client is active'
     */
    public boolean checkIsActiveClient(boolean isActiveClient) {
        return checkIsCheckboxSelected(isActiveClient, CLIENT_IS_ACTIVE_CHECKBOX);
    }

    /**
     * check 'Allowed Manual-Push'
     */
    public boolean checkIsAllowedManualPush(boolean isAllowedManualPush) {
        return checkIsCheckboxSelected(isAllowedManualPush, ALLOWED_MANUAL_PUSH_CHECKBOX);
    }

    /**
     * check 'Allow client to delete teasers'
     */
    public boolean checkAllowedClientToDeleteTeasers(boolean allowedClientToDeleteTeasers) {
        return checkIsCheckboxSelected(allowedClientToDeleteTeasers, ALLOWED_CLIENT_TO_DELETE_TEASERS_CHECKBOX);
    }

    /**
     * check 'Total expenses stats'
     */
    public boolean checkTotalExpensesStats(boolean totalExpensesStats) {
        return checkIsCheckboxSelected(totalExpensesStats, TOTAL_EXPENSES_STAT_CHECKBOX);
    }

    /**
     * check 'Total expenses stats'
     */
    public void saveClientsSettings() {
        SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }
}
