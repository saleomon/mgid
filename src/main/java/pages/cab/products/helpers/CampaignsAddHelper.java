package pages.cab.products.helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import core.base.HelpersInit;
import pages.cab.products.locators.CampaignsAddLocators;
import pages.cab.products.logic.CabCampaigns;
import core.helpers.BaseHelper;

import java.sql.Date;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.CampaignsAddLocators.*;
import static pages.cab.products.locators.CampaignsListLocators.*;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.AUDIENCE_OUT_BLOCK;
import static pages.dash.advertiser.locators.AddEditCampaignLocators.TRAFFIC_TYPE_OUT_BLOCK;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.TargetingType.Targeting.*;


public class CampaignsAddHelper {
    private final HelpersInit helpersInit;
    private final Logger log;
    private boolean isGenerateNewValues = false;
    List<String> listLimits = new ArrayList<>();
    public int targetCounter = 0;
    String[] typeOfTargetting = {"include", "exclude"};
    String priceOptimizationRotationLimitTeasers = "1", priceOptimizationCalculationLimitCampaign = "4",
            priceOptimizationQualityFactorWeight = "9", priceOptimizationRotationLimitCampaign = "2",
            priceOptimizationConversionsToRefill = "5", priceOptimizationCalculationLimitTeasers = "3",
            priceOptimizationLimit = "6.00";
    String vastCpm = "0.03", dspUrl = "http://test_sok_auto.kr.com";

    public CampaignsAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void setTargetCounter(int targetCounter) {
        this.targetCounter += targetCounter;
    }

    public void setGenerateNewValues(boolean generateNewValues) {
        isGenerateNewValues = generateNewValues;
    }

    /**
     * select campaign type
     */
    public void selectType(String campaignType) {
        if (campaignType != null) {
            TYPE_SELECT.selectOptionByValue(campaignType);
            waitForAjaxLoader();
            log.info("Campaign type is - " + campaignType);
        }
    }

    public String fillFeedsProvider(String feedProvider) {
        if (feedProvider != null) {
            sendKey(FEEDS_PROVIDER_INPUT, feedProvider);
            waitForAjaxLoader();
            log.info("Campaign type is - " + feedProvider);
            return feedProvider;
        }
        return null;
    }


    /**
     * select video-campaign subtype
     */
    public void selectSubType(String campaignSubType) {
        if (campaignSubType != null) {
            SUB_TYPE_SELECT.selectOptionByValue(campaignSubType);
            log.info("Campaign subtype is - " + campaignSubType);
        }
    }

    /**
     * select video-campaign Payment Model
     */
    public void selectPaymentModel(String campaignPaymentModel) {
        if (campaignPaymentModel != null) {
            PAYMENT_MODEL_SELECT.selectOptionByValue(campaignPaymentModel);
            log.info("Campaign Payment model is - " + campaignPaymentModel);
        }
    }

    /**
     * select Marketing objective
     */
    public void selectMarketingObjective(String marketingObjectiveValue) {
        if (marketingObjectiveValue != null) {
            MARKETING_OBJECTIVE_SELECT.selectOptionByValue(marketingObjectiveValue);
            log.info("Campaign 'Marketing objective is - " + marketingObjectiveValue);
        }
    }

    /**
     * select Marketing objective
     */
    public void selectMediaSource(String mediaSourceValue) {
        if (mediaSourceValue != null) {
            MEDIA_SOURCE_SELECT.selectOptionByValue(mediaSourceValue);
            log.info("Campaign Media Source is - " + mediaSourceValue);
        }
    }

    /**
     * select campaign tier
     */
    public String selectTier(String campaignTire) {
        if (campaignTire == null && !isGenerateNewValues) return null;
        String tire;
        if (campaignTire != null) {
            tire = campaignTire;
            TIRE_SELECT.selectOptionByValue(tire);
        } else {
            tire = helpersInit.getBaseHelper().selectRandomValue(TIRE_SELECT);
        }
        waitForAjax();
        log.info("Campaign tier is - " + tire);
        return tire;
    }

    /**
     * mark/unmark campaign as white
     */
    public void markAsWhite(boolean state) {
        markUnMarkCheckbox(state, MARK_AS_WHITE_CHECKBOX);
        log.info(MARK_AS_WHITE_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * select campaign mirror
     */
    public String selectMirror(String campaignMirror) {
        if (campaignMirror == null && !isGenerateNewValues) return null;
        String mirror;
        if (campaignMirror != null) {
            mirror = campaignMirror;
            MIRROR_SELECT.selectOptionByValue(mirror);
        } else {
            mirror = helpersInit.getBaseHelper().selectRandomValue(MIRROR_SELECT);
        }
        waitForAjax();
        log.info("Campaign mirror is - " + mirror);
        return mirror;
    }

    /**
     * fill campaign Name
     */
    public String fillName(String campaignName) {
        if (campaignName == null && !isGenerateNewValues) return null;
        String name;
        name = Objects.requireNonNullElseGet(campaignName, () -> "Campaign test " + getRandomWord(6));
        clearAndSetValue(NAME_FIELD, name);
        log.info("Campaign name is - " + name);
        return name;
    }

    /**
     * fill displayed campaign Name
     */
    public String fillNameDisplayedWidgets(String campaignName) {
        if (campaignName == null && !isGenerateNewValues) return null;
        String name;
        name = Objects.requireNonNullElseGet(campaignName, () -> "Campaign test displayed name" + getRandomWord(3));
        clearAndSetValue(DISPLAYED_NAME_FIELD, name);
        log.info("Campaign displayed name is - " + name);
        return name;
    }

    public String fillCampaignKeyword(String keyword) {
        if (keyword == null) return null;
        clearAndSetValue(KEYWORD_FIELD, keyword);
        log.info("Campaign keywordName is - " + keyword);
        return keyword;
    }

    /**
     * select campaign category
     */
    public String selectCategory(String dataId) {
        List<String> forbiddenCategory = Arrays.asList("254", "149", "148", "105");
        if (dataId == null && !isGenerateNewValues) return null;
        String category = "";
        if (dataId != null) {
            category = dataId;
            helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, dataId);
        } else {
            for (int i = 0; i < 10; i++) {
                category = helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT);
                if (!forbiddenCategory.contains(category)) {
                    break;
                }
            }
        }

        waitForAjaxLoader();
        log.info("Campaign category id is - " + category);
        return category;
    }

    /**
     * select campaign language
     */
    public String selectLanguage(String languageCamp) {
        if (languageCamp == null && !isGenerateNewValues) return null;

        String language;
        if (languageCamp != null) {
            language = languageCamp;
            LANGUAGE_SELECT.selectOptionByValue(language);
        } else {
            language = helpersInit.getBaseHelper().selectRandomValue(LANGUAGE_SELECT);
        }
        waitForAjax();
        log.info("Campaign language is - " + language);
        return language;
    }

    /**
     * Choose language for campaign without context targeting
     */
    public String chooseLanguageWithoutContext(List<String> languagesForContext) {
        List<String> languagesAll = new ArrayList<>();
        for (SelenideElement option : LANGUAGE_SELECT.findAll("option")) {
            languagesAll.add(option.getValue());
        }
        List<String> differences = languagesAll
                .stream().filter(i -> !languagesForContext.contains(i))
                .collect(Collectors.toList());
        return differences.get(randomNumbersInt(differences.size()));
    }

    /**
     * block teaser after creation
     */
    public void blockTeaserAfterCreation(boolean state) {
        markUnMarkCheckbox(state, BLOCK_TEASER_AFTER_CREATION_CHECKBOX);
        log.info(BLOCK_TEASER_AFTER_CREATION_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * block teaser after creation
     */
    public void chooseShowsOnlyForThisLanguage(boolean state) {
        markUnMarkCheckbox(state, SHOW_TEASERS_ONLY_ON_SITES_IN_THIS_LANGUAGE_CHECKBOX);
        log.info(SHOW_TEASERS_ONLY_ON_SITES_IN_THIS_LANGUAGE_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * block by manager
     */
    public void blockByManager(boolean state) {
        markUnMarkCheckbox(state, BLOCK_BY_MANAGER_CHECKBOX);
        log.info(BLOCK_BY_MANAGER_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * rotate in adskeeper subnet
     */
    public void rotateAdskeeperSubnet(boolean state) {
        markUnMarkCheckbox(state, ROTATE_IN_ADSKEEPER_CHECKBOX);
        log.info(ROTATE_IN_ADSKEEPER_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * check name of campaign at campaign list interface
     */
    public void selectLocationTarget(String typeTargeting, String... countries) {
        LOCATION_TARGET.click();
        Arrays.asList(countries).forEach(el -> clickInvisibleElementJs($x("//*[text()='" + el + "']/../*[@class='targeting-btn targeting-" + typeTargeting + "']")));
    }

    /**
     * fill campaign acf
     */
    public String fillAcf(String acf) {
        if (acf == null && !isGenerateNewValues) return null;

        String nameAcf;
        if (acf != null) {
            nameAcf = acf;
        } else {
            nameAcf = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.7, 0.8);
        }
        clearAndSetValue(ACF_FIELD, nameAcf);
        log.info("Campaign acf is - " + nameAcf);
        return nameAcf;
    }

    /**
     * fill show percent
     */
    public Map<String, String> fillShowPercent(String numberOfPercent) {
        if (numberOfPercent == null && !isGenerateNewValues) return null;
        String percent;
        percent = Objects.requireNonNullElseGet(numberOfPercent, () -> randomNumberFromRange(3, 6));
        ElementsCollection listOfSubregions;
        Map<String, String> list_id_region = new HashMap<>();
        if (PERCENTAGE_OF_DISPLAYS.scrollIntoView(true).isDisplayed()) {
            markUnMarkCheckbox(true, PERCENTAGE_OF_DISPLAYS);

            if (PERCENTAGE_CHECKBOX.isDisplayed()) {
                PERCENTAGE_CHECKBOX.scrollIntoView(false);
                markUnMarkCheckbox(true, PERCENTAGE_CHECKBOX);
                listOfSubregions = LIST_OF_SUB_REGIONS;
            } else {
                listOfSubregions = LIST_OF_COUNTRY;
            }
            PERCENTAGE_OF_DISPLAYS.scrollIntoView(true);
            for (int i = 0; i < 1; i++) {
                clearAndSetValue(listOfSubregions.get(i), percent);
                list_id_region.putIfAbsent(listOfSubregions.get(i).getAttribute("data-id"), percent);
                log.info("Data-id of percent - " + listOfSubregions.get(i).getAttribute("data-id"), percent);
                log.info("Value of percent - " + percent);
            }
        } else {
            PERCENTAGE_OF_DISPLAYS.scrollIntoView(true).sendKeys(percent);
            list_id_region.putIfAbsent("empty", percent);
            log.info("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            log.info(list_id_region.get("empty"));

        }
        log.info("fill_show_percent");
        return list_id_region;
    }

    /**
     * check show percent
     */
    public boolean checkShowPercent(Map<String, String> listIdRegion) {
        if (listIdRegion != null) {
            boolean flag = false;
            int k = 0;
            String key = listIdRegion.keySet().toString().substring(1, listIdRegion.keySet().toString().length() - 1);
            if (COUNTRY_CHECKBOX.exists()) {
                ElementsCollection list = LIST_OF_SUB_REGIONS;
                for (int i = 0; i < 1; i++) {
                    log.info($(list.get(i)).getAttribute("data-id") + " - " + key);
                    if (Objects.requireNonNull($(list.get(i)).getAttribute("data-id")).equals(key)) {
                        log.info($(list.get(i)).getAttribute("value") + " - " + listIdRegion.get(key));
                        if (Objects.requireNonNull($(list.get(i)).getAttribute("value")).equals(listIdRegion.get(key)))
                            k++;
                        if (k == listIdRegion.size()) {
                            flag = true;
                        }
                    }
                }
            } else {
                if (Objects.requireNonNull(REGION_ELEMENT.getAttribute("value")).equals(listIdRegion.get(key))) {
                    log.info(REGION_ELEMENT.getAttribute("value") + "==" + listIdRegion.get(key));
                    flag = true;
                }
            }
            log.info("check_show_percent " + flag);
            return flag;
        }
        return true;
    }


    /**
     * select native version
     */
    public String selectNativeVersion(boolean use) {
        return use ? helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(NATIVE_VERSION)) : null;
    }

    /**
     * select native version
     */
    public String chooseDsp(boolean use) {
        return use ? helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(DSP)) : null;
    }

    /**
     * select send site info
     */
    public String selectSendSiteInfo(boolean use) {
        return use ? helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(SEND_SITE_INFO_SELECT)) : null;
    }

    /**
     * select send site info
     */
    public String selectCooperationType(boolean use) {
        return use ? helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(COOPERATION_TYPE)) : null;
    }

    /**
     * mark/unmark only matched visitors
     */
    public void onlyMatchetVisitors(boolean state) {
        if (MATCHED_VISITORS.isDisplayed()) {
            markUnMarkCheckbox(state, MATCHED_VISITORS);
            log.info(MATCHED_VISITORS.getAttribute("id") + " Checkbox state - " + state);
        }
    }

    /**
     * check only matched visitors
     */
    public boolean checkOnlyMatchetVisitors(boolean state) {
        if (MATCHED_VISITORS.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, MATCHED_VISITORS.isSelected());
        return true;
    }

    /**
     * choose auction type
     */
    public String auctionTypeDsp(String auctionTypeValue) {
        if (auctionTypeValue != null) {
            String flag;
            log.info("dspAuctionType - start");
            if (auctionTypeValue.equals("video")) {
                log.info("video");
                markUnMarkCheckbox(true, VIDEO_DSP);
                clearAndSetValue(VAST_BIDFLOOR_FIELD, vastCpm);
                flag = vastCpm;
            } else {
                log.info("cpc");
                markUnMarkCheckbox(false, VIDEO_DSP);
                clearAndSetValue(BID_REQUEST_URL_US_MID_INPUT, dspUrl);
                flag = dspUrl;
            }
            return flag;
        }
        return null;
    }

    /**
     * check auction type
     */
    public boolean checkAuctionTypeDsp(String auctionTypeValue, String value) {
        int[] IndexesForCroppingStringToHundredth = {0, 4};
        if (auctionTypeValue != null) {
            boolean isVideoAuctionType = auctionTypeValue.equals("video");
            markUnMarkCheckbox(isVideoAuctionType, VIDEO_DSP);

            return isVideoAuctionType ?
                    helpersInit.getBaseHelper().checkDatasetEquals(
                            Objects.requireNonNull(VAST_BIDFLOOR_FIELD.getAttribute("value")).substring(IndexesForCroppingStringToHundredth[0], IndexesForCroppingStringToHundredth[1]),
                            value)
                    :
                    helpersInit.getBaseHelper().checkDatasetEquals(
                            Objects.requireNonNull(BID_REQUEST_URL_US_MID_INPUT.getAttribute("value")).substring(IndexesForCroppingStringToHundredth[0], IndexesForCroppingStringToHundredth[1]),
                            value);
        }
        return true;
    }

    /**
     * check selected send site info
     */
    public boolean checkSelectedSendSiteInfo(String getDspInfoAboutSub) {
        if (getDspInfoAboutSub != null) {
            if (!getDspInfoAboutSub.equals("0")) {
                log.info("Send Site info - " + SELECTED_SEND_SITE_INFO.getText());
                return helpersInit.getBaseHelper().checkDatasetEquals(SELECTED_SEND_SITE_INFO.getAttribute("value"), getDspInfoAboutSub);
            }
            log.info("Send Site info - " + getDspInfoAboutSub);
            return helpersInit.getBaseHelper().getTextAndWriteLog(true);
        }
        return true;
    }

    /**
     * fill campaign probability coef
     */
    public String fillShowProbabilityMax(String probCoef) {
        if (probCoef == null && !isGenerateNewValues) return null;
        String coef;
        if (probCoef != null) {
            coef = probCoef;
        } else {
            coef = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.8, 0.9);
        }
        clearAndSetValue(SHOW_PROBABILITY_MAX_FIELD, coef);
        log.info("Campaign probability coef is - " + coef);
        return coef;
    }

    public String fillShowProbabilityDsp(String probCoef) {
//        if (probCoef == null && !isGenerateNewValues) return null;
        String coef;
        if (probCoef != null) {
            coef = probCoef;
        } else {
            coef = helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(0.8, 0.9);
        }
        clearAndSetValue(SHOW_PROBABILITY_DSP, coef);
        log.info("Campaign probability coef is - " + coef);
        return coef;
    }

    /**
     * include report quota
     */
    public void includeReportQuota(boolean state) {
        markUnMarkCheckbox(state, INCLUDE_REPORTS_QUOTA_CHECKBOX);
        log.info(INCLUDE_REPORTS_QUOTA_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * start count bad sites quotas
     */
    public String fillCountBadQoutas(String badQuota) {
        if (badQuota == null && !isGenerateNewValues) return null;

        String quota;
        if (badQuota != null) {
            quota = badQuota;
        } else {
            quota = new Date(System.currentTimeMillis()).toLocalDate().minusDays(1).toString();
        }
        clearAndSetValue(COUNT_BAD_QUOTAS_CALENDAR, quota);
        log.info("Campaign bad quotas start - " + quota);
        helpersInit.getMessageHelper().isSuccessMessagesCab();

        return quota;
    }

    /**
     * set value
     */
    public void setValue(boolean state) {
        markUnMarkCheckbox(state, SET_VALUE_CHECKBOX);
        log.info(SET_VALUE_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * don't include quality factor
     */
    public void dontIncludeQualityFactor(boolean state) {
        markUnMarkCheckbox(state, DO_NOT_SHOW_QUALITY_FACTOR_CHECKBOX);
        log.info(DO_NOT_SHOW_QUALITY_FACTOR_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * don't send campaign to GeoEdge
     */
    public void dontSendToGeoEdge(boolean state) {
        markUnMarkCheckbox(state, DO_NOT_SEND_TO_GEO_EDGE_CHECKBOX);
        log.info(DO_NOT_SEND_TO_GEO_EDGE_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * Teaser impressions frequency for a unique user after click
     */
    public String fillTeaserImpressionFrequently(String teaserImpr) {
        if (teaserImpr == null && !isGenerateNewValues) return null;
        String impress;
        impress = Objects.requireNonNullElseGet(teaserImpr, () -> String.valueOf(randomNumbersInt(5)));
        clearAndSetValue(TEASER_IMPRESSION_FREQUENCY_FIELD, impress);
        log.info("Campaign teaser impressions frequency is - " + impress);
        return impress;
    }

    /**
     * Do not show all teasers by campaign after click on any of them
     */
    public void dontShowTeaserAfterClick(boolean state) {
        markUnMarkCheckbox(state, DO_NOT_SHOW_ALL_TEASERS_AFTER_CLICK_CHECKBOX);
        log.info(DO_NOT_SHOW_ALL_TEASERS_AFTER_CLICK_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * Do not show all teasers by campaign after conversions to Action
     */
    public String dontShowTeaserAfterConversion(boolean state, String period) {
        if (state && !isGenerateNewValues) return null;

        String periodOfDays = "";
        markUnMarkCheckbox(state, DO_NOT_SHOW_ALL_TEASERS_AFTER_ACTION_CHECKBOX);
        if (state) {
            if (period != null) {
                clearAndSetValue(TEASER_IMPRESSION_FREQUENCY_FIELD, periodOfDays = period);
            } else {
                clearAndSetValue(TEASER_IMPRESSION_FREQUENCY_FIELD, periodOfDays = String.valueOf(BaseHelper.getRandomWord(5)));
            }
        }
        log.info(DO_NOT_SHOW_ALL_TEASERS_AFTER_ACTION_CHECKBOX.getAttribute("id") + " Checkbox state - " + state + ". Period - " + periodOfDays);
        return periodOfDays;
    }

    /**
     * Limit the total impressions frequency of all campaign teasers per unique user for period
     */
    public void limitImpressionsFrequently(boolean state) {
        markUnMarkCheckbox(state, LIMIT_TOTAL_IMPRESSIONS_CHECKBOX);
        log.info(LIMIT_TOTAL_IMPRESSIONS_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }

    /**
     * Impressions limit
     */
    public String fillImpressionLimit(boolean state) {
        String impress = "";
        if (state) {
            clearAndSetValue(IMPRESSION_LIMIT_FIELD, impress = String.valueOf(randomNumbersInt(5)));
        }
        log.info("Campaign impressions limit is - " + impress);
        return impress;
    }

    /**
     * Impressions limit period
     */
    public String fillImpressionLimitPeriod(boolean state) {
        String impress = "";
        if (state) {
            clearAndSetValue(IMPRESSION_LIMIT_PERIOD_FIELD, impress = String.valueOf(randomNumbersInt(5)));
        }
        log.info("Campaign impressions limit period is - " + impress);
        return impress;
    }

    /**
     * Blocking according to schedule
     */
    public void blockingAccordingSchedule(boolean state) {
        markUnMarkCheckbox(state, BLOCK_BY_SCHEDULE_CHECKBOX);
        log.info(BLOCK_BY_SCHEDULE_CHECKBOX.getAttribute("id") + " Checkbox state - " + state);
    }


    /**
     * Ad campaign start date
     */
    public String fillAdStartDate(String startDate) {
        if (startDate == null && !isGenerateNewValues) return null;

        String date;
        if (startDate != null) {
            date = startDate;
        } else {
            date = new Date(System.currentTimeMillis() + 86555555).toString();
        }
        clearAndSetValue(AD_START_DAY_CALENDAR, date);
        log.info("Campaign ad start date - " + date);
        return date;
    }

    /**
     * Ad campaign end date
     */
    public String fillAdEndDate(String endDate) {
        if (endDate == null && !isGenerateNewValues) return null;

        String date;
        if (endDate != null) {
            date = endDate;
        } else {
            date = new Date(System.currentTimeMillis() + 86555555).toString();
        }
        clearAndSetValue(AD_END_DAY_CALENDAR, date);
        GENERAL_SETTINGS_FIELDSET.click();
        log.info("Campaign ad end date - " + date);
        return date;
    }

    public List<String> setLimits(String typeOfLimit, String dayLimit, String generalLimit, boolean unlimitedState) {
        if (unlimitedState) return null;
        if (typeOfLimit == null) {
            String[] qwe = {"budget", "clicks"};
            typeOfLimit = qwe[randomNumbersInt(1)];
            dayLimit = randomNumberFromRange(2500, 2500);
            generalLimit = randomNumberFromRange(3001, 3001);
        }
        return switch (typeOfLimit) {
            case "budget" -> budgetLimits(dayLimit, generalLimit);
            case "clicks" -> clicksLimits(dayLimit, generalLimit);
            default -> null;
        };
    }

    public List<String> setLimitsForVideoCampaign(String typeOfLimit, String dayLimit, String generalLimit, boolean unlimitedState) {
        if (unlimitedState) return null;
        SET_LIMITS_RADIOBUTTON.selectRadio("complete_limits");
        if (typeOfLimit == null) {
            String[] qwe = {"budget", "impressions", "both"};
            typeOfLimit = qwe[randomNumbersInt(1)];
            dayLimit = randomNumberFromRange(1, 2500);
            generalLimit = randomNumberFromRange(1, 3001);
        }
        switch (typeOfLimit) {
            //todo AIA need refactoring to ENUM in task https://jira.mgid.com/browse/TA-52559
            case "budget" -> budgetLimitsVideo(dayLimit, generalLimit);
            case "impressions" -> impressionsLimitsVideo(dayLimit, generalLimit);
            case "clicks" -> clicksLimitsVideo(dayLimit, generalLimit);
            case "completeViews" -> completeViewsLimitsVideo(dayLimit, generalLimit);
            case "both" -> {
                budgetLimitsVideo(dayLimit, generalLimit);
                impressionsLimitsVideo(dayLimit, generalLimit);
            }
        }
        return listLimits;
    }

    /**
     * Ad campaign limits budget
     */
    public List<String> budgetLimits(String dayLimit, String generalLimit) {
        LIMITS_BLOCK_RADIO.selectRadio("budget_limits");
        clearAndSetValue(LIMIT_PER_DAY_BUDGET_FIELD, dayLimit);
        clearAndSetValue(LIMIT_GENERAL_BUDGET_FIELD, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Campaign budget limit day - " + dayLimit + ", general - " + generalLimit);
        return listLimits;
    }

    /**
     * Video campaign limits budget
     */
    public void budgetLimitsVideo(String dayLimit, String generalLimit) {
        BUDGETS_LIMITS_CHECKBOX.setSelected(true);
        clearAndSetValue(AD_CAMPAIGN_DAYLIMIT_INPUT, dayLimit);
        clearAndSetValue(GENERAL_CAMPAIGN_LIMIT_BUDGET_INPUT, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Video campaign budget limit day - " + dayLimit + ", general - " + generalLimit);
    }

    /**
     * Video campaign limits Complete Views
     */
    public void completeViewsLimitsVideo(String dayLimit, String generalLimit) {
        COMPLETE_VIEWS_LIMITS_CHECKBOX.setSelected(true);
        clearAndSetValue(COMPLETE_VIEWS_DAYLIMIT_INPUT, dayLimit);
        clearAndSetValue(COMPLETE_VIEWS_GENERAL_LIMIT_INPUT, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Video campaign Complete Views limit day - " + dayLimit + ", general - " + generalLimit);
    }

    /**
     * Ad campaign limits clicks
     */
    public List<String> clicksLimits(String dayLimit, String generalLimit) {
        DO_NOT_SEND_TO_GEO_EDGE_CHECKBOX.scrollIntoView("{display: \"inline-block\"}");
        LIMITS_BLOCK_RADIO.selectRadio("clicks_limits");
        waitForAjaxLoader();
        clearAndSetValue(LIMIT_PER_DAY_CLICKS_FIELD, dayLimit);
        clearAndSetValue(LIMIT_GENERAL_CLICKS_FIELD, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Campaign clicks limit day - " + dayLimit + ", general - " + generalLimit);
        return listLimits;
    }

    /**
     * Video Prebid campaign limits clicks
     */
    public void clicksLimitsVideo(String dayLimit, String generalLimit) {
        CLICKS_LIMIT_CHECKBOX.setSelected(true);
        clearAndSetValue(CampaignsAddLocators.LIMIT_PER_DAY_CLICKS_FIELD, dayLimit);
        clearAndSetValue(CampaignsAddLocators.LIMIT_GENERAL_CLICKS_FIELD, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Video campaign clicks limit day - " + dayLimit + ", general - " + generalLimit);
    }

    /**
     * Video campaign limits impressions
     */
    public void impressionsLimitsVideo(String dayLimit, String generalLimit) {
        IMPRESSIONS_LIMIT_CHECKBOX.setSelected(true);
        clearAndSetValue(CampaignsAddLocators.IMPRESSIONS_DAYLIMIT_INPUT, dayLimit);
        clearAndSetValue(CampaignsAddLocators.GENERAL_CAMPAIGN_LIMIT_IMPRESSIONS_INPUT, generalLimit);
        waitForAjaxLoader();
        listLimits.add(dayLimit);
        listLimits.add(generalLimit);
        log.info("Video campaign impressions limit day - " + dayLimit + ", general - " + generalLimit);
    }

    /**
     * use ignore min price
     */
    public Map<String, String> ignoreMinPrice(boolean use) {
        if (use) {
            Map<String, String> data_prices = new HashMap<>();
            log.info("start");
            markUnMarkCheckbox(true, IGNORE_POC_LIMITS_CHECKBOX);
            markUnMarkCheckbox(false, APPLY_FOR_ALL_REGIONS_CHECKBOX);
            CONTAINER_FOR_PRICES_BLOCK.shouldBe(visible);

            // раскрываем дерево регионов
            PARETN_ELEMENTS
                    .filter(Condition.attribute("data-open", "false"))
                    .asFixedIterable()
                    .forEach(BaseHelper::clickInvisibleElementJs);

            for (int i = 0; i < 3; i++) {
                int randomElement = randomNumbersInt(LIST_REGIONS.size());
                String lim = "5";
                while (data_prices.containsKey(LIST_REGIONS.get(randomElement))) {
                    randomElement = randomNumbersInt(LIST_REGIONS.size());
                }
//                LIST_PRICES.get(randomElement).clear();
                LIST_PRICES.get(randomElement).sendKeys(lim);
                if (!LIST_REGIONS.get(randomElement).text().contains("Other")) {
                    data_prices.put(LIST_REGIONS.get(randomElement).getText(), lim);
                }
            }

            log.info("fill_ignore_limits");
            return data_prices;
        }
        return null;
    }

    /**
     * select ad lifetime
     */
    public String selectAdLifeTime(String adLifeTime) {
        if (adLifeTime == null && !isGenerateNewValues) return null;

        String time;
        if (adLifeTime != null) {
            time = adLifeTime;
        } else {
            time = helpersInit.getBaseHelper().selectRandomValue(AD_LIFETIME_SELECT);
        }
        AD_LIFETIME_SELECT.selectOptionByValue(time);
        log.info("Campaign life time is - " + time);
        return time;
    }

    /**
     * method choose targets for all section or can choose target for one section
     */
    public JsonObject selectTargeting(String... neededTargetting) {
        if (neededTargetting == null) return null;
        JsonObject jsonTargetingElements = new JsonObject();
        targetCounter = 0;
        TARGETINGS_LEGEND.scrollTo();
        USE_TARGETING_RADIO.click();
        for (String parameter : neededTargetting) {
            switch (parameter) {
                case "all" -> {
                    jsonTargetingElements.add(GEO.getTypeValue(), locationTarget());
                    targetCounter++;
                    jsonTargetingElements.add(DEVICES_OS.getTypeValue(), osTarget());
                    targetCounter++;
                    jsonTargetingElements.add(BROWSER.getTypeValue(), browserTarget());
                    targetCounter++;
                    jsonTargetingElements.add(BROWSER_LANGUAGE.getTypeValue(), browserLanguageTarget());
                    targetCounter++;
//                    jsonTargetingElements.add(MOBILE_CONNECTION.getTypeValue(), connectionTypeTarget());
//                    targetCounter++;
                    jsonTargetingElements.add(PROVIDER.getTypeValue(), providerTarget());
                    targetCounter++;
                    jsonTargetingElements.add(PUBLISHER_CATEGORIES.getTypeValue(), platformTarget());
                    targetCounter++;
                    jsonTargetingElements.add(DEMOGRAPHICS.getTypeValue(), socdemTarget());
                    targetCounter++;
                    jsonTargetingElements.add(INTERESTS.getTypeValue(), interestsTarget());
                    targetCounter++;
                    jsonTargetingElements.add(BUNDLES.getTypeValue(), bundleTarget());
                    targetCounter++;
                    jsonTargetingElements.add(AUDIENCES.getTypeValue(), audienceTarget());
                    targetCounter++;
                    jsonTargetingElements.add(TRAFFIC_TYPE.getTypeValue(), trafficTarget());
                    targetCounter++;
                    jsonTargetingElements.add(PHONE_PRICE_RANGES.getTypeValue(), phonePriceRangeTarget());
                    targetCounter++;
                }
                case "location" -> {
                    jsonTargetingElements.add(GEO.getTypeValue(), locationTarget());
                    targetCounter++;
                }
                case "os" -> {
                    jsonTargetingElements.add(DEVICES_OS.getTypeValue(), osTarget());
                    targetCounter++;
                }
//                case "connection":
//                    jsonTargetingElements.add("connectionType", connectionTypeTarget());
//                    targetCounter++;
//                    break;
                case "browser" -> {
                    jsonTargetingElements.add(BROWSER.getTypeValue(), browserTarget());
                    targetCounter++;
                }
                case "browserLanguage" -> {
                    jsonTargetingElements.add(BROWSER_LANGUAGE.getTypeValue(), browserLanguageTarget());
                    targetCounter++;
                }
                case "audience" -> {
                    jsonTargetingElements.add(AUDIENCES.getTypeValue(), audienceTarget());
                    targetCounter++;
                }
                case "provider" -> {
                    jsonTargetingElements.add(PROVIDER.getTypeValue(), providerTarget());
                    targetCounter++;
                }
                case "platform" -> {
                    jsonTargetingElements.add(PUBLISHER_CATEGORIES.getTypeValue(), platformTarget());
                    targetCounter++;
                }
                case "socdem" -> {
                    jsonTargetingElements.add(DEMOGRAPHICS.getTypeValue(), socdemTarget());
                    targetCounter++;
                }
                case "interest" -> {
                    jsonTargetingElements.add(INTERESTS.getTypeValue(), interestsTarget());
                    targetCounter++;
                }
                case "bundle" -> {
                    jsonTargetingElements.add(BUNDLES.getTypeValue(), bundleTarget());
                    targetCounter++;
                }
                case "trafficType" -> {
                    jsonTargetingElements.add(TRAFFIC_TYPE.getTypeValue(), trafficTarget());
                    targetCounter++;
                }
                case "context" -> {
                    jsonTargetingElements.add(CONTEXT.getTypeValue(), contextTarget());
                    targetCounter++;
                }
                case "sentiments" -> {
                    jsonTargetingElements.add(SENTIMENTS.getTypeValue(), sentimentsTarget());
                    targetCounter++;
                }
                case "phonePriceRange" -> {
                    jsonTargetingElements.add(PHONE_PRICE_RANGES.getTypeValue(), phonePriceRangeTarget());
                    targetCounter++;
                }
                default -> log.info("There are no matched targets");
            }
        }
        return jsonTargetingElements;
    }

    /**
     * method choose location target
     */
    public JsonObject locationTarget() {
        //location
        JsonObject dataLocation;
        String choosenType = getTypeTargeting();

        LOCATION_TARGET.click();
        dataLocation = chooseRandomTarget(choosenType, GEO.getTypeValue());

        LOCATION_OUT_BLOCK.shouldBe(visible);
        log.info("Locaton selected - " + dataLocation.get("data-id").toString());
        log.info("Type targeting selected - " + dataLocation.get("typeTarget").toString());

        return dataLocation;
    }

    /**
     * method choose os target
     */
    public JsonObject osTarget() {
        //os
        JsonObject dataOs;
        String choosenType = getTypeTargeting();

        OS_TARGET.click();
        dataOs = chooseRandomTarget(choosenType, DEVICES_OS.getTypeValue());

        OS_OUT_BLOCK.shouldBe(visible);
        log.info("Selected OS - " + dataOs.get("data-id").toString());
        log.info("Type targeting selected - " + dataOs.get("typeTarget").toString());

        return dataOs;
    }

    /**
     * method choose browser target
     */
    public JsonObject browserTarget() {
        //browser
        JsonObject dataBrowser;
        String choosenType = getTypeTargeting();

        BROWSER_TARGET.click();
        dataBrowser = chooseRandomTarget(choosenType, BROWSER.getTypeValue());

        BROWSER_OUT_BLOCK.shouldBe(visible);
        log.info("Selected browser - " + dataBrowser.get("data-id").toString());
        log.info("Type targeting selected - " + dataBrowser.get("typeTarget").toString());

        return dataBrowser;
    }

    /**
     * method choose browser language target
     */
    public JsonObject browserLanguageTarget() {
        ////browser language
        JsonObject dataBrowserLanguage;
        String choosenType = getTypeTargeting();

        BROWSER_LANGUAGE_TARGET.click();
        dataBrowserLanguage = chooseRandomTarget(choosenType, BROWSER_LANGUAGE.getTypeValue());

        LANGUAGE_OUT_BLOCK.shouldBe(visible);
        log.info("Selected browser language - " + dataBrowserLanguage.get("data-id").toString());
        log.info("Type targeting selected - " + dataBrowserLanguage.get("typeTarget").toString());

        return dataBrowserLanguage;
    }

    /**
     * method choose provider target
     */
    public JsonObject connectionTypeTarget() {
        //provider
        JsonObject dataProvider;
        String choosenType = "include";

        CONNECTION_TARGET.click();
        dataProvider = chooseRandomTarget(choosenType, MOBILE_CONNECTION.getTypeValue());

        CONNECTION_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected connection - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose provider target
     */
    public JsonObject providerTarget() {
        //provider
        JsonObject dataProvider;
        String choosenType = getTypeTargeting();

        PROVIDER_TARGET.click();
        dataProvider = chooseRandomTarget(choosenType, PROVIDER.getTypeValue());

        PROVIDER_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected provider - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose platform target
     */
    public JsonObject platformTarget() {
        //provider
        JsonObject dataProvider;
        String choosenType = getTypeTargeting();

        PUBLISHER_CATEGORIES_TAB.click();
        dataProvider = chooseRandomTarget(choosenType, PUBLISHER_CATEGORIES.getTypeValue());

        PLATFORM_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected platform - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose Demographics target
     */
    public JsonObject socdemTarget() {
        //provider
        JsonObject dataProvider;
        String chosenType = "include";

        DEMOGRAPHICS_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, DEMOGRAPHICS.getTypeValue());

        SOCDEM_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected socdem - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose Interests target
     */
    public JsonObject interestsTarget() {
        //provider
        JsonObject dataProvider;
        String choosenType = getTypeTargeting();

        INTERESTS_TAB.click();
        dataProvider = chooseRandomTarget(choosenType, INTERESTS.getTypeValue());

        INTERESTS_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected interest - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose platform target
     */
    public JsonObject bundleTarget() {
        //provider
        JsonObject dataProvider;
        String chosenType = getTypeTargeting();

        BUNDLES_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, BUNDLES.getTypeValue());

        BUNDLE_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected bundle - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose audiences
     */
    public JsonObject audienceTarget() {
        //provider
        JsonObject dataProvider = new JsonObject();

        AUDIENCE_TAB.click();
        dataProvider.add("exclude", chooseRandomTarget("exclude", AUDIENCES.getTypeValue()));
        dataProvider.add("include", chooseRandomTarget("include", AUDIENCES.getTypeValue()));
        AUDIENCE_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected audience exclude - " + dataProvider.get("exclude").getAsJsonObject().get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("exclude").getAsJsonObject().get("typeTarget").toString());
        log.info("Selected audience include - " + dataProvider.get("include").getAsJsonObject().get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("include").getAsJsonObject().get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject trafficTarget() {
        //traffic
        JsonObject dataProvider;
        String chosenType = getTypeTargeting();

        TRAFFIC_TYPE_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, TRAFFIC_TYPE.getTypeValue());

        TRAFFIC_TYPE_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected traffic - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject contextTarget() {
        //context
        JsonObject dataProvider;
        String chosenType = getTypeTargeting();

        CONTEXT_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, CONTEXT.getTypeValue());

        CONTEXT_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected context - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject sentimentsTarget() {
        //sentiments
        JsonObject dataProvider;
        String chosenType = "include";

        SENTIMENTS_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, SENTIMENTS.getTypeValue());

        SENTIMENTS_OUT_BLOCK.shouldBe(visible, ofSeconds(8));
        log.info("Selected sentiments - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    public JsonObject phonePriceRangeTarget() {
        //phonePriceRange
        JsonObject dataProvider;
        String chosenType = getTypeTargeting();

        PHONE_PRICE_RANGES_TAB.click();
        dataProvider = chooseRandomTarget(chosenType, PHONE_PRICE_RANGES.getTypeValue());

        PHONE_PRICE_RANGES_OUT_BLOCK.shouldBe(visible, Duration.ofSeconds(8));
        log.info("Selected phone price range - " + dataProvider.get("data-id").toString());
        log.info("Type targeting selected - " + dataProvider.get("typeTarget").toString());

        return dataProvider;
    }

    /**
     * method choose random target elements
     */
    public JsonObject chooseRandomTarget(String typeOfTarget, String kindOfTarget) {
        JsonObject data = new JsonObject();
        int indexOfElement;
        ElementsCollection listOptions;
        String data_id;
        String element;
        int counter = 0;
        //choose needed layer of locator for targeting elements
        if ($(String.format(TARGET_ELEMENTS_LEVEL_2, kindOfTarget)).exists()) {
            element = TARGET_ELEMENTS_LEVEL_2;
        } else {
            element = TARGET_ELEMENTS_LEVEL_0;
        }

        //this block evaluated if target has expanded elements and open its
        while ($$(String.format(NOT_EXPANDED_ELEMENTS, kindOfTarget)).size() > 0 && counter < 3) {
            counter++;
            expandAllTargetElements();
        }

        listOptions = $$(String.format(element, kindOfTarget));
        data_id = listOptions.get(indexOfElement = randomNumbersInt(listOptions.size())).attr("data-id");
        System.out.println("listOptions size - " + listOptions.size());
        if(kindOfTarget.equals("location")) System.out.println("indexOfElement - " + indexOfElement);
        if(kindOfTarget.equals("location")) System.out.println("listOptions - " + listOptions.get(indexOfElement));
        //choose random target element
        clickChosenTargeting(element, kindOfTarget, indexOfElement);
        clickTypeTargetingButton(element, kindOfTarget, typeOfTarget, indexOfElement);
        data.addProperty("data-id", data_id);
        data.addProperty("typeTarget", typeOfTarget);
        checkErrors();
        return data;
    }

    private void clickChosenTargeting(String element, String kindOfTarget, int indexOfElement) {
        clickInvisibleElementJs($$(String.format(element, kindOfTarget)).get(indexOfElement));
    }

    private void clickTypeTargetingButton(String element, String kindOfTarget, String typeOfTarget, int indexOfElement) {
        clickInvisibleElementJs($$(String.format(element, kindOfTarget)).get(indexOfElement).$("div span[class*='" + typeOfTarget + "']"));
    }

    private void expandAllTargetElements() {
        executeJavaScript("var v = $('.targeting-expand');" + "for(var i = 0; i < v.length; i++){ v[i].click(); }");
    }

    /**
     * method check selected section of targets for all section
     */
    public boolean checkSelectedTargets(JsonObject targetingElements) {
        Iterator<String> keys = targetingElements.keySet().iterator();
        String currentKey;
        String dataId, typeTargeting;
        int counter = 0;
        while (keys.hasNext()) {
            currentKey = keys.next().replaceAll("\"", "");
            switch (currentKey) {
                case "location", "browser", "os", "language", "connection", "provider", "bundle", "socdem", "interest",
                        "platform", "trafficType", "context", "sentiments", "phonePriceRange" -> {
                    typeTargeting = targetingElements.get(currentKey).getAsJsonObject().get("typeTarget").getAsString();
                    dataId = targetingElements.get(currentKey).getAsJsonObject().get("data-id").getAsString();
                }
                case "audience" -> {
                    typeTargeting = targetingElements.get(currentKey).getAsJsonObject().get("include").getAsJsonObject().get("typeTarget").getAsString();
                    dataId = targetingElements.get(currentKey).getAsJsonObject().get("exclude").getAsJsonObject().get("data-id").getAsString();
                }
                default -> throw new IllegalStateException("Unexpected value: " + currentKey);
            }
            if (helpersInit.getBaseHelper().getTextAndWriteLog(
                    $(String.format(CHOSEN_TARGET_ELEMENT, currentKey, typeTargeting, dataId)).exists() ||
                    $(String.format(CHOSEN_TARGET_ELEMENT_MULTIPLE, currentKey, dataId)).has(attribute("data-current-mode", typeTargeting)) ||
                    $(String.format(CHOSEN_TARGET_ELEMENT_WITHOUT_TYPE, currentKey, dataId)).exists())) {
                counter++;
            }
        }
        log.info(counter + " == " + targetCounter);
        return helpersInit.getBaseHelper().getTextAndWriteLog(counter == targetCounter);
    }

    public String getTypeTargeting() {
        return typeOfTargetting[randomNumbersInt(typeOfTargetting.length)];
    }


    /**
     * clear chosen targeting
     */
    public void clearTargetingSettings(boolean state) {
        while (REMOVE_SELECTED_TARGET_BUTTONS.size() > 0 && state) {
            clickInvisibleElementJs(REMOVE_SELECTED_TARGET_BUTTON);
        }
    }

    /**
     * fill utm tags
     */
    public Map<String, String> useUtmTagging(String customTag) {
        if (customTag == null && !isGenerateNewValues) return null;
        String tag = customTag != null ? customTag : "ref=mgid.com";
        Map<String, String> utms = new HashMap<>();
        if (UTM_CHECKBOX.isSelected()) {
            utms.put("medium", UTM_MEDIUM_TAG_FIELD.val());
            utms.put("source", UTM_SOURCE_TAG_FIELD.val());
            utms.put("campaign", UTM_CAMPAIGN_TAG_FIELD.val());
            utms.put("custom", tag);
            markUnMarkCheckbox(true, CUSTOM_TAG_CHECKBOX);
            clearAndSetValue(CUSTOM_TAG_FIELD, tag);
        }
        utms.keySet().forEach(x -> log.info("utm tags - " + x));
        return utms;
    }


    /**
     * fill our sensors conversion
     */
    public Map<String, String> useConversion(CabCampaigns.Conversion conversion) {
        Map<String, String> sensorsOfConversion = new HashMap<>();
        if (conversion != null) {
            switch (conversion) {
                case GOOGLE_ANALYTICS -> CONVERSIONS_STAGE_RADIO.selectRadio("Google Analytics");
                case OUR_SENSORS -> {
                    markUnMarkCheckbox(true, INTEREST_STAGE_CHECKBOX);
                    markUnMarkCheckbox(true, DESIRE_STAGE_CHECKBOX);
                    markUnMarkCheckbox(true, ACTION_STAGE_CHECKBOX);
                    sensorsOfConversion.put("interest", selectStageValue(INTEREST_STAGE_SELECT, "interest"));
                    sensorsOfConversion.put("interestCpa", fillCpaGoal("interest"));
                    sensorsOfConversion.put("decision", selectStageValue(DESIRE_STAGE_SELECT, "decision"));
                    sensorsOfConversion.put("decisionCpa", fillCpaGoal("decision"));
                    sensorsOfConversion.put("buy", selectStageValue(ACTION_STAGE_SELECT, "buy"));
                    sensorsOfConversion.put("buyCpa", fillCpaGoal("buy"));
                    sensorsOfConversion.put("interestCount", selectConversionCountingValue(INTEREST_STAGE_COUNTING_SELECT, "interest"));
                    sensorsOfConversion.put("decisionCount", selectConversionCountingValue(DESIRE_STAGE_COUNTING_SELECT, "decision"));
                    sensorsOfConversion.put("buyCount", selectConversionCountingValue(ACTION_STAGE_COUNTING_SELECT, "buy"));
                    String cpaCorrection = randomNumberFromRange(1, 5);
                    sensorsOfConversion.put("cpaCorrection", cpaCorrection);
                    clearAndSetValue(CPA_CORRECTION_FIELD, cpaCorrection);
                    log.info("Sensor of conversions was set");
                }
            }
            return sensorsOfConversion;
        }
        return null;
    }

    public String selectStageValue(SelenideElement element, String stage) {
        String s = "";
        ElementsCollection list = $$("#conversion-conversion_" + stage + "-target>option");
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i).getAttribute("disabled") == null) {
                element.selectOption(i);
                s = list.get(i).getText();
                break;
            }
        }
        return s;
    }

    public String selectConversionCountingValue(SelenideElement element, String stage) {
        List<SelenideElement> list = new ArrayList<>();
        for(SelenideElement s : $$("#conversion-conversion_" + stage + "-deduplicateConversions>option")){
            if(!Objects.requireNonNull(s.val()).equalsIgnoreCase("")) list.add(s);
        }

        int indexOfElement = randomNumbersInt(list.size());
        element.selectOptionByValue(list.get(indexOfElement).val());
        return element.$("[value='" + list.get(indexOfElement).val() + "'").text();
    }

    public String fillCpaGoal(String stage) {
        String goalCpa = randomNumberFromRange(10, 50);
        clearAndSetValue($("#conversion-conversion_" + stage + "-cpa"), goalCpa);

        return goalCpa;
    }

    /**
     * fill autooptimization option
     */
    public void useAutoOptimization(boolean useAutoOptimization) {
        if (useAutoOptimization) {
            markUnMarkCheckbox(true, USE_AUTOOPTIMIZATION_CHECKBOX);
            clearAndSetValue(TEASER_ROTATION_LIMIT, priceOptimizationRotationLimitTeasers);
            clearAndSetValue(CAMPAIGN_ROTATION_LIMIT, priceOptimizationRotationLimitCampaign);
            clearAndSetValue(TEASER_CALCULATION_LIMIT, priceOptimizationCalculationLimitTeasers);
            clearAndSetValue(CAMPAIGN_CALCULATION_LIMIT, priceOptimizationCalculationLimitCampaign);

            clearAndSetValue(CONVERSIONS_TO_REFILL, priceOptimizationConversionsToRefill);
            clearAndSetValue(OPTIMIZATION_LIMIT_BY_WIDGET, priceOptimizationLimit);
            clearAndSetValue(SELECTIVE_BIDDING_WEIGHT, priceOptimizationQualityFactorWeight);

            log.info("Auto optimization fields were filled");
        } else {
            log.info("Auto optimization fields weren't filled");
        }
    }

    /**
     * save campaign settings by click the button
     */
    public void addIconForPush(boolean isLoadIconForPush, String... imageFromProject) {
        if (isLoadIconForPush) {
            if (imageFromProject[0].contains("http")) {
//                FIELD_FOR_PUSH_ICON.clear();
                ANY_ELEMENT_FOR_CLICK.click();
                FIELD_FOR_PUSH_ICON.sendKeys(imageFromProject[0]);
            } else {
//                FIELD_FOR_PUSH_ICON.clear();
                ANY_ELEMENT_FOR_CLICK.click();
                INPUT_FOR_PUSH_ICON.sendKeys(imageFromProject[0]);
            }
            ANY_ELEMENT_FOR_CLICK.click();
            waitForAjax();
            log.info("The icon for push teaser was set");
        }
    }

    public boolean checkIconForPush(boolean isLoadIconForPush) {
        if (isLoadIconForPush) {
            if (!Objects.requireNonNull(FIELD_FOR_PUSH_ICON.getAttribute("value")).contains("amazonaws.com")) {
                log.info("The icon for push teaser wasn't set");
                return helpersInit.getBaseHelper().getTextAndWriteLog(false);
            }
            log.info("The icon for push teaser was set");
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }


    /**
     * save campaign settings by click the button
     */
    public void saveSettings() {
        SAVE_BUTTON.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        waitForAjax();
        checkErrors();
        log.info("saveCampaign");
    }

    /**
     * check type of campaign
     */
    public boolean checkType(String campaignType) {
        return campaignType == null || helpersInit.getBaseHelper().checkDatasetContains(campaignType, Objects.requireNonNull(TYPE_SELECT.getSelectedValue()).trim());
    }

    /**
     * check subtype of campaign
     */
    public boolean checkSubType(String campaignSubType) {
        return campaignSubType == null || helpersInit.getBaseHelper().checkDatasetContains(campaignSubType, Objects.requireNonNull(SUB_TYPE_SELECT.getSelectedValue()).trim());
    }

    /**
     * check payment model of video campaign
     */
    public boolean checkPaymentModel(String campaignPaymentModel) {
        return campaignPaymentModel == null || helpersInit.getBaseHelper().checkDatasetContains(campaignPaymentModel, Objects.requireNonNull(PAYMENT_MODEL_SELECT.getSelectedValue()).trim());
    }

    /**
     * check tier of campaign
     */
    public boolean checkTier(String campaignTier) {
        return campaignTier == null || helpersInit.getBaseHelper().checkDatasetContains(campaignTier, Objects.requireNonNull(TIRE_SELECT.getSelectedValue()).trim());
    }

    /**
     * check mark as white checkbox
     */
    public boolean checkMarkAsWhite(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, MARK_AS_WHITE_CHECKBOX.isSelected());
    }

    /**
     * check mirror of campaign
     */
    public boolean checkMirror(String mirror) {
        return mirror == null || helpersInit.getBaseHelper().checkDatasetContains(mirror, Objects.requireNonNull(MIRROR_SELECT.getSelectedValue()).trim());
    }

    /**
     * check name of campaign
     */
    public boolean checkName(String name) {
        return name == null || helpersInit.getBaseHelper().checkDatasetContains(name, Objects.requireNonNull(NAME_FIELD.getValue()).trim());
    }

    /**
     * check displayed name of campaign
     */
    public boolean checkDisplayedName(String displayedName) {
        return displayedName == null || helpersInit.getBaseHelper().checkDatasetContains(displayedName, Objects.requireNonNull(DISPLAYED_NAME_FIELD.getValue()).trim());
    }

    public boolean checkFeedsProvider(String feedsProvider) {
        return feedsProvider == null || helpersInit.getBaseHelper().checkDatasetContains(feedsProvider, Objects.requireNonNull(FEEDS_PROVIDER_INPUT.getValue()).trim());
    }

    public boolean checkCampaignKeyword(String keyword) {
        return keyword == null || helpersInit.getBaseHelper().checkDatasetContains(keyword, Objects.requireNonNull(KEYWORD_FIELD.getValue()).trim());
    }

    /**
     * check category of campaign
     */
    public boolean checkCategory(String category) {
        return category == null || helpersInit.getBaseHelper().checkDatasetContains(category, Objects.requireNonNull(CATEGORY_SELECTED.getAttribute("data-id")).trim());
    }

    /**
     * check language of campaign
     */
    public boolean checkLanguage(String language) {
        return language == null || helpersInit.getBaseHelper().checkDatasetContains(language, Objects.requireNonNull(LANGUAGE_SELECTED.val()));
    }

    /**
     * check block teaser after creation checkbox
     */
    public boolean checkBlockTeaserAfterCreation(boolean state) {
        if (BLOCK_TEASER_AFTER_CREATION_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, BLOCK_TEASER_AFTER_CREATION_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check block teaser after creation checkbox
     */
    public boolean checkShowsOnlyForThisLanguage(boolean state) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(state == isOnlySameLanguageWidgets());
    }

    /**
     * check block by manager checkbox
     */
    public boolean checkBlockByManager(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, BLOCK_BY_MANAGER_CHECKBOX.isSelected());
    }

    /**
     * check rotate in adskeeper subnet checkbox
     */
    public boolean checkRotateAdskeeperSubnet(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, ROTATE_IN_ADSKEEPER_CHECKBOX.isSelected());
    }

    /**
     * check show probability max of campaign
     */
    public boolean checkShowProbabilityMax(String probability) {
        return probability == null || helpersInit.getBaseHelper().checkDatasetContains(probability, Objects.requireNonNull(SHOW_PROBABILITY_MAX_FIELD.getValue()).trim());
    }

    /**
     * check include report quota checkbox
     */
    public boolean checkIncludingReportQuota(boolean state) {
        if (INCLUDE_REPORTS_QUOTA_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, INCLUDE_REPORTS_QUOTA_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check count bad quotas  of campaign
     */
    public boolean checkCountBadQuotas(String quotas) {
        return quotas == null || helpersInit.getBaseHelper().checkDatasetContains(quotas, Objects.requireNonNull(COUNT_BAD_QUOTAS_CALENDAR.getValue()).trim());
    }

    /**
     * check set value checkbox
     */
    public boolean checkSetValue(boolean state) {
        if (SET_VALUE_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, SET_VALUE_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check dont include quality factor checkbox
     */
    public boolean checkDontIncludeQualityFactor(boolean state) {
        if (DO_NOT_SHOW_QUALITY_FACTOR_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, DO_NOT_SHOW_QUALITY_FACTOR_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check dont send to GeoEdge checkbox
     */
    public boolean checkDontSendToGeoEdge(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, DO_NOT_SEND_TO_GEO_EDGE_CHECKBOX.isSelected());
    }

    /**
     * check teaser impression of campaign
     */
    public boolean checkFillTeaserImpression(String impression) {
        return impression == null || helpersInit.getBaseHelper().checkDatasetContains(impression, Objects.requireNonNull(TEASER_IMPRESSION_FREQUENCY_FIELD.getValue()).trim());
    }

    /**
     * check dont show teaseer after click of campaign
     */
    public boolean checkDontShowTeaserAfterClick(boolean state) {
        if (DO_NOT_SHOW_ALL_TEASERS_AFTER_CLICK_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, DO_NOT_SHOW_ALL_TEASERS_AFTER_CLICK_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check dont show teaseer after conversion of campaign
     */
    public boolean checkDontShowTeaserAfterConversion(boolean state) {
        if (DO_NOT_SHOW_ALL_TEASERS_AFTER_ACTION_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, DO_NOT_SHOW_ALL_TEASERS_AFTER_ACTION_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check limit impression of campaign
     */
    public boolean checkLimitImpression(boolean state) {
        if (LIMIT_TOTAL_IMPRESSIONS_CHECKBOX.isDisplayed())
            return helpersInit.getBaseHelper().checkDataset(state, LIMIT_TOTAL_IMPRESSIONS_CHECKBOX.isSelected());
        return true;
    }

    /**
     * check impression limit of campaign
     */
    public boolean checkImpressionLimit(String impression) {
        return impression == null || helpersInit.getBaseHelper().checkDatasetContains(impression, Objects.requireNonNull(IMPRESSION_LIMIT_FIELD.getValue()).trim());
    }

    /**
     * check impression limit period of campaign
     */
    public boolean checkImpressionLimitPeriod(String impression) {
        return impression == null || helpersInit.getBaseHelper().checkDatasetContains(impression, Objects.requireNonNull(IMPRESSION_LIMIT_PERIOD_FIELD.getValue()).trim());
    }

    /**
     * check block according schedule of campaign
     */
    public boolean checkBlockAccordingSchedule(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, BLOCK_BY_SCHEDULE_CHECKBOX.isSelected());
    }

    public boolean checkDisplayedCertificates(String name) {
        if (name != null) name = name.substring(name.lastIndexOf("/") + 1);
        return name == null || helpersInit.getBaseHelper().getTextAndWriteLog($x(String.format(CERTIFICATE_NAME_LABEL, name)).isDisplayed());
    }

    /**
     * check ad start day of campaign
     */
    public boolean checkAdStartDate(String date) {
        return date == null || helpersInit.getBaseHelper().checkDatasetContains(date, Objects.requireNonNull(AD_START_DAY_CALENDAR.getValue()).trim());
    }

    /**
     * check ad end day of campaign
     */
    public boolean checkAdEndDate(String date) {
        return date == null || helpersInit.getBaseHelper().checkDatasetContains(date, Objects.requireNonNull(AD_END_DAY_CALENDAR.getValue()).trim());
    }

    public boolean checkLimits(String option, List<String> list) {
        switch (option) {
            case "budget" -> {
                return list.contains(Objects.requireNonNull(LIMIT_GENERAL_BUDGET_FIELD.val()).replace(".00", "")) && list.contains(Objects.requireNonNull(LIMIT_PER_DAY_BUDGET_FIELD.val()).replace(".00", ""));
            }
            case "impressions" -> {
                return list.contains(Objects.requireNonNull(CampaignsAddLocators.GENERAL_CAMPAIGN_LIMIT_IMPRESSIONS_INPUT.val()).replace(".00", "")) && list.contains(Objects.requireNonNull(CampaignsAddLocators.IMPRESSIONS_DAYLIMIT_INPUT.val()).replace(".00", ""));
            }
            case "clicks" -> {
                return list.contains(LIMIT_GENERAL_CLICKS_FIELD.val()) && list.contains(LIMIT_PER_DAY_CLICKS_FIELD.val());
            }
            case "conversion" -> {
                return list.contains(LIMIT_GENERAL_CONVERSION_FIELD.val()) && list.contains(LIMIT_PER_DAY_CONVERSION_FIELD.val());
            }
            case "both" -> {
                return list.contains(Objects.requireNonNull(LIMIT_GENERAL_BUDGET_FIELD.val()).replace(".00", "")) && list.contains(Objects.requireNonNull(LIMIT_PER_DAY_BUDGET_FIELD.val()).replace(".00", "")) &&
                        list.contains(Objects.requireNonNull(CampaignsAddLocators.GENERAL_CAMPAIGN_LIMIT_IMPRESSIONS_INPUT.val()).replace(".00", "")) && list.contains(Objects.requireNonNull(CampaignsAddLocators.IMPRESSIONS_DAYLIMIT_INPUT.val()).replace(".00", ""));
            }
            default -> {
                return true;
            }
        }
    }

    /**
     * check type of limit of campaign
     */
    public boolean checkTypeLimit(String option) {
        return helpersInit.getBaseHelper().checkDatasetContains(option, Objects.requireNonNull(SELECTED_OPTION_LIMITS_BLOCK_RADIO.getValue()));
    }

    /**
     * check ignore min prices of campaign
     */
    public boolean checkIgnoreMinPrice(Map<String, String> regionPrices) {
        if (regionPrices != null) {
            int count = 0;
            boolean flag = false;
            String temp;
            APPLY_FOR_ALL_REGIONS_CHECKBOX.scrollTo();
            for (Map.Entry<String, String> entry : regionPrices.entrySet()) {
                temp = $x("//div[@id='geo-group-prices-limits-tree']//div[@class='node-body' and not(div[@class='node-expander']) and div[@class='node-label']/span[contains(text(), '" + entry.getKey() + "')]]//input[@class='node-value']").val();
                if (helpersInit.getBaseHelper().checkDatasetEquals(temp, entry.getValue().replace(",", "."))) {
                    count++;
                }
            }
            if (count == regionPrices.size()) {
                flag = true;
            }
            return helpersInit.getBaseHelper().getTextAndWriteLog(flag);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    /**
     * check ad life time of campaign
     */
    public boolean checkAdLifeTime(String lifetime) {
        return lifetime == null || helpersInit.getBaseHelper().checkDatasetContains(lifetime, Objects.requireNonNull(AD_LIFETIME_SELECT.getSelectedValue()).trim());
    }

    /**
     * check selected targeting
     */
    public boolean checkSelectedTargeting(JsonObject targetingElements) {
        return /*checkTypeTargetingSections(targetingElements) &&*/
                checkSelectedTargets(targetingElements);
    }

    /**
     * check UTM tags
     */
    public boolean checkUtmTags(boolean isEnabledTag, boolean isEnabledCustomTag, Map<String, String> customValue) {
        if (isEnabledTag) {
            UTM_CHECKBOX.shouldBe(selected);
            UTM_MEDIUM_TAG_FIELD.shouldHave(exactValue(customValue.get("medium")));
            UTM_SOURCE_TAG_FIELD.shouldHave(exactValue(customValue.get("source")));
            UTM_CAMPAIGN_TAG_FIELD.shouldHave(exactValue(customValue.get("campaign").replace(" ", "+")));
        }
        if (isEnabledCustomTag) {
            CUSTOM_TAG_CHECKBOX.shouldBe(selected);
            CUSTOM_TAG_FIELD.shouldHave(exactValue(customValue.get("custom")));
        }
        log.info("Utm Tags meet with required");
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    /**
     * check sensor conversion
     */
    public boolean checkOurSensorsConversions(Map<String, String> sensorsOfConversion) {
        if (sensorsOfConversion != null) {
            //check interest
            INTEREST_STAGE_CHECKBOX.shouldBe(selected);
            INTEREST_STAGE_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("interest")));
            INTEREST_STAGE_COUNTING_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("interestCount")));
            INTEREST_CONVERSION_FIELD.shouldHave(exactValue(sensorsOfConversion.get("interestCpa")));

            //check interest
            DESIRE_STAGE_CHECKBOX.shouldBe(selected);
            DESIRE_STAGE_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("decision")));
            DESIRE_STAGE_COUNTING_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("decisionCount")));
            DESIRE_CONVERSION_FIELD.shouldHave(exactValue(sensorsOfConversion.get("decisionCpa")));

            //check interest
            ACTION_STAGE_CHECKBOX.shouldBe(selected);
            ACTION_STAGE_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("buy")));
            ACTION_STAGE_COUNTING_SELECT.getSelectedOption().shouldHave(exactText(sensorsOfConversion.get("buyCount")));
            ACTION_CONVERSION_FIELD.shouldHave(exactValue(sensorsOfConversion.get("buyCpa")));

            //cpa correction
            CPA_CORRECTION_FIELD.shouldHave(exactValue(sensorsOfConversion.get("cpaCorrection")));

            log.info("Sensors Conversions meet with required");
            return helpersInit.getBaseHelper().getTextAndWriteLog(true);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(true);
    }

    /**
     * check if 'Campaign type' - is have custom value
     */
    public boolean isCampaignTypeHaveCustomValue(String value) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TYPE_SELECT.find(By.cssSelector("[value=" + value + "]")).exists());
    }

    /**
     * check show select 'campaign type'
     */
    public boolean isShowCampaignTypeSelect() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TYPE_SELECT.isDisplayed());
    }

    /**
     * is schedule company run
     */
    public boolean isScheduleSwitch() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(BLOCK_BY_SCHEDULE_CHECKBOX.isSelected());
    }

    /**
     * get campaign 'Campaign name that will be displayed in widgets'
     */
    public String getCampaignShowName() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DISPLAYED_NAME_FIELD.val());
    }

    /**
     * get status checkbox 'mark campaign as white'
     */
    public boolean isWhiteCampaign() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MARK_AS_WHITE_CHECKBOX.isSelected());
    }

    /**
     * get status checkbox 'Show teasers only on sites in this language'
     */
    public boolean isOnlySameLanguageWidgets() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SHOW_TEASERS_ONLY_ON_SITES_IN_THIS_LANGUAGE_CHECKBOX.isSelected());
    }

    /**
     * check is custom, tagging switchOn
     */
    public boolean isCustomTaggingSwitchOn() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CUSTOM_TAG_CHECKBOX.isSelected());
    }

    /**
     * check if need change clone category
     */
    public String chooseCloneCategory() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CLONE_CATEGORY_SELECT));
    }

    /**
     * check if need change clone language
     */
    public String chooseCloneLanguage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(CLONE_LANGUAGE_SELECT));
    }

    /**
     * save clone
     */
    public void saveClone() {
        CLONE_SAVE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check location id targeting
     */
    public boolean checkLocationIdTargeting(String locationId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(locationId, Objects.requireNonNull(LOCATION_TARGET_SELECTED.attr("data-id")));
    }

    /**
     * check Browser id targeting
     */
    public boolean checkBrowserIdTargeting(String browserId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(browserId, Objects.requireNonNull(BROWSER_TARGET_SELECTED.attr("data-id")));
    }

    /**
     * check os id targeting
     */
    public boolean checkOsIdTargeting(String osId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(osId, Objects.requireNonNull(OS_TARGET_SELECTED.attr("data-id")));
    }

    /**
     * check language id targeting
     */
    public boolean checkLanguageIdTargeting(String languageId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(languageId, Objects.requireNonNull(LANGUAGE_TARGET_SELECTED.attr("data-id")));
    }

    /**
     * check connection type id targeting
     */
    public boolean checkConnectionTypeIdTargeting(String connectionTypeId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(connectionTypeId, Objects.requireNonNull(CONNECTION_TYPE_SELECTED.attr("data-id")));
    }

    /**
     * check bundles id targeting
     */
    public boolean checkBundlesIdTargeting(String typeId) {
        return helpersInit.getBaseHelper().checkDatasetEquals(typeId, Objects.requireNonNull(BUNDLES_SELECTED.attr("data-id")));
    }

    /**
     * check viewability targeting
     */
    public boolean checkViewabilityTargeting(String viewabilityId) {
        return VIEWABILITY_SELECTED.isDisplayed() &&
                helpersInit.getBaseHelper().checkDatasetEquals(viewabilityId, Objects.requireNonNull(VIEWABILITY_SELECTED.attr("data-id")));
    }

    /**
     * check Action Conversion Stage
     */
    public boolean checkAuctionStageConversion(String auctionStageVal) {
        return helpersInit.getBaseHelper().checkDatasetEquals(auctionStageVal, Objects.requireNonNull(ACTION_STAGE_SELECTED_VALUE.val()));
    }

    /**
     * check Interest Conversion Stage
     */
    public boolean checkInterestStageConversion(String interestStageVal) {
        return helpersInit.getBaseHelper().checkDatasetEquals(interestStageVal, Objects.requireNonNull(INTEREST_STAGE_SELECTED_VALUE.val()));
    }

    /**
     * check Desire Conversion Stage
     */
    public boolean checkDesireStageConversion(String desireStageVal) {
        return helpersInit.getBaseHelper().checkDatasetEquals(desireStageVal, Objects.requireNonNull(DESIRE_STAGE_SELECTED_VALUE.val()));
    }

    /**
     * check UTM custom tag
     */
    public boolean checkUtmCustomTag(String utmCustom) {
        return helpersInit.getBaseHelper().checkDatasetEquals(utmCustom, Objects.requireNonNull(CUSTOM_TAG_FIELD.val()));
    }

    /**
     * choose 'Capping trigger event'
     */
    public String setCappingTriggerEvent(String cappingTriggerEventValue) {
        String selectedCappingTriggerEventValue = cappingTriggerEventValue != null ? helpersInit.getBaseHelper().selectCustomText(VIDEO_CAPPING_EVENTS.shouldBe(visible), cappingTriggerEventValue) : helpersInit.getBaseHelper().selectRandomValue(VIDEO_CAPPING_EVENTS.shouldBe(visible));
        return helpersInit.getBaseHelper().getTextAndWriteLog(selectedCappingTriggerEventValue);
    }

    /**
     * select 'Frequency capping'
     */
    public String selectFrequencyCapping(String frequencyCappingValue) {
        String selectedFrequencyCappingValue = frequencyCappingValue != null ? helpersInit.getBaseHelper().selectCustomText(FREQUENCY_CAPPING_SELECT, frequencyCappingValue) : helpersInit.getBaseHelper().selectRandomValue(FREQUENCY_CAPPING_SELECT);
        return helpersInit.getBaseHelper().getTextAndWriteLog(selectedFrequencyCappingValue);
    }

    /**
     * set 'Capping limit'
     */
    public String setCappingLimit(String cappingLimitValue, String frequencyCappingValue) {
        if (frequencyCappingValue == "Unlimited") return null;
            String cappingLimit = cappingLimitValue != null ? cappingLimitValue : randomNumberFromRange(0, 100);
            clearAndSetValue(SEND_NO_MORE_THAN_INPUT, cappingLimit);
            return helpersInit.getBaseHelper().getTextAndWriteLog(cappingLimit);
    }

    /**
     * set 'VAST URL'
     */
    public String setVastUrl() {
        String url = "https://testurlvast" + BaseHelper.getRandomWord(2) + ".com.tz";
        clearAndSetValue(VAST_URL, url);
        return helpersInit.getBaseHelper().getTextAndWriteLog(url);
    }

    /**
     * set 'Price' (vast)
     */
    public String setVastPrice(String vastPrice) {
        String price = vastPrice != null ? vastPrice : helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(0.1, 9.99).replace(",", ".");
        clearAndSetValue(VAST_PRICE, price);
        return helpersInit.getBaseHelper().getTextAndWriteLog(price);
    }

    /**
     * check 'Capping trigger event'
     */
    public boolean checkCappingTriggerEvent(String cappingTriggerEvent) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(VIDEO_CAPPING_EVENTS, cappingTriggerEvent));
    }

    /**
     * check 'Frequency capping'
     */
    public boolean checkFrequencyCapping(String frequencyCappingValue) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(FREQUENCY_CAPPING_SELECT, frequencyCappingValue));
    }

    /**
     * check 'Capping limit'
     */
    public boolean checkCappingLimit(String cappingLimitValue) {
        return helpersInit.getBaseHelper().checkDatasetEquals(cappingLimitValue, Objects.requireNonNull(SEND_NO_MORE_THAN_INPUT.val()));
    }

    /**
     * check 'VAST URL'
     */
    public boolean checkVastUrl(String vastUrl) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VAST_URL.val()).equals(vastUrl);
    }

    /**
     * check 'Price' (vast)
     */
    public boolean checkVastPrice(String vastPrice) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VAST_PRICE.val()).equals(vastPrice);
    }

    /**
     * Set Event Urls
     */
    public boolean setEventUrls(Map<String, String> eventsUrls, boolean enabled) {
        for (Map.Entry<String, String> entry : eventsUrls.entrySet()) {
            switch (entry.getKey()) {
                case "impression" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_IMPRESSION_CHECKBOX.setSelected(true);
                        EVENT_URLS_IMPRESSION_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_IMPRESSION_CHECKBOX.setSelected(false);
                }
                case "first_quartile" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_FIRST_QUARTILE_CHECKBOX.setSelected(true);
                        EVENT_URLS_FIRST_QUARTILE_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_FIRST_QUARTILE_CHECKBOX.setSelected(false);
                }
                case "midpoint" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_MIDPOINT_CHECKBOX.setSelected(true);
                        EVENT_URLS_MIDPOINT_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_MIDPOINT_CHECKBOX.setSelected(false);
                }
                case "third_quartile" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_THIRD_QUARTILE_CHECKBOX.setSelected(true);
                        EVENT_URLS_THIRD_QUARTILE_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_THIRD_QUARTILE_CHECKBOX.setSelected(false);
                }
                case "complete" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_COMPLETE_CHECKBOX.setSelected(true);
                        EVENT_URLS_COMPLETE_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_COMPLETE_CHECKBOX.setSelected(false);
                }
                case "click" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_CLICK_CHECKBOX.setSelected(true);
                        EVENT_URLS_CLICK_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_CLICK_CHECKBOX.setSelected(false);
                }
                case "skip" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (enabled) {
                        EVENT_URLS_SKIP_CHECKBOX.setSelected(true);
                        EVENT_URLS_SKIP_INPUT.sendKeys(entry.getValue());
                    } else EVENT_URLS_SKIP_CHECKBOX.setSelected(false);
                }
            }
        }
        return enabled;
    }

    /**
     * Check Event Urls
     */
    public boolean checkEventUrls(Map<String, String> eventsUrls, boolean isEnabled) {
        int count = 0;
        for (Map.Entry<String, String> entry : eventsUrls.entrySet()) {
            switch (entry.getKey()) {
                case "impression" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_IMPRESSION_CHECKBOX.isSelected() && EVENT_URLS_IMPRESSION_INPUT.getText().equals(entry.getValue()))
                            count++;
                    } else {
                        if (!EVENT_URLS_IMPRESSION_CHECKBOX.isSelected() && !EVENT_URLS_IMPRESSION_INPUT.isEnabled())
                            count++;
                    }
                }
                case "first_quartile" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_FIRST_QUARTILE_CHECKBOX.isSelected() && EVENT_URLS_FIRST_QUARTILE_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_FIRST_QUARTILE_CHECKBOX.isSelected() && !EVENT_URLS_FIRST_QUARTILE_INPUT.isEnabled())
                            count++;
                    }
                }
                case "midpoint" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_MIDPOINT_CHECKBOX.isSelected() && EVENT_URLS_MIDPOINT_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_MIDPOINT_CHECKBOX.isSelected() && !EVENT_URLS_MIDPOINT_INPUT.isEnabled())
                            count++;
                    }
                }
                case "third_quartile" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_THIRD_QUARTILE_CHECKBOX.isSelected() && EVENT_URLS_THIRD_QUARTILE_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_THIRD_QUARTILE_CHECKBOX.isSelected() && !EVENT_URLS_THIRD_QUARTILE_INPUT.isEnabled())
                            count++;
                    }
                }
                case "complete" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_COMPLETE_CHECKBOX.isSelected() && EVENT_URLS_COMPLETE_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_COMPLETE_CHECKBOX.isSelected() && !EVENT_URLS_COMPLETE_INPUT.isEnabled())
                            count++;
                    }
                }
                case "click" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_CLICK_CHECKBOX.isSelected() && EVENT_URLS_CLICK_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_CLICK_CHECKBOX.isSelected() && !EVENT_URLS_CLICK_INPUT.isEnabled())
                            count++;
                    }
                }
                case "skip" -> {
                    EVENT_URL_FIELDSET.scrollTo();
                    if (isEnabled) {
                        if (EVENT_URLS_SKIP_CHECKBOX.isSelected() && EVENT_URLS_SKIP_INPUT.getText().equals(entry.getValue())) {
                            count++;
                        }
                    } else {
                        if (!EVENT_URLS_SKIP_CHECKBOX.isSelected() && !EVENT_URLS_SKIP_INPUT.isEnabled())
                            count++;
                    }
                }
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(count == eventsUrls.size());
    }

    /**
     * is displayed auto retargeting option
     */
    public boolean isDisplayedAutoRetargeting() {
        return AUTORETARGETING_CHECKBOX.scrollTo().isDisplayed();
    }

    /**
     * check disable attribute for tier option
     */
    public boolean checkDisabledTierOption(String tier) {
        return Objects.requireNonNull(TIRE_SELECT.getSelectedValue()).equalsIgnoreCase(tier) && TIRE_SELECT.has(attribute("disabled"));
    }

    public boolean checkMarketingObjective(String campaignMarketingObjective) {
        return campaignMarketingObjective == null || helpersInit.getBaseHelper().checkDatasetEquals(campaignMarketingObjective, Objects.requireNonNull(MARKETING_OBJECTIVE_SELECT.getSelectedValue()));
    }

    public boolean checkMediaSource(String campaignMediaSource) {
        return campaignMediaSource == null ||
                (MEDIA_SOURCE_SELECT.isDisplayed() &&
                        helpersInit.getBaseHelper().checkDatasetEquals(campaignMediaSource,
                                Objects.requireNonNull(MEDIA_SOURCE_SELECT.getSelectedValue())));
    }
}

