package pages.cab.products.helpers;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.List;
import java.util.stream.IntStream;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.AudienceAddLocators.AUDIENCE_NAME_INPUT;
import static pages.cab.products.locators.AudiencesListLocators.*;
import static core.helpers.BaseHelper.waitForAjax;

public class AudiencesListHelper {
    HelpersInit helpersInit;
    Logger log;

    public AudiencesListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Add new audience
     */
    public void addNewAudience() {
        ADD_AUDIENCE_BUTTON.shouldBe(visible).click();
        AUDIENCE_NAME_INPUT.shouldBe(visible, ofSeconds(8));
        log.info("Add new audience");
    }

    /**
     * Delete audience
     */
    public boolean deleteClientAudience(String audienceId) {
        SelenideElement deleteIcon = $(String.format(AUDIENCE_DELETE_ICON, audienceId));
        deleteIcon.shouldBe(visible).click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        waitForAjax();
        return helpersInit.getBaseHelper().checkDataset(true, !deleteIcon.shouldBe(hidden).isDisplayed());
    }

    /**
     * Delete all audiences
     */
    public void deleteAllAudiences() {
        if (AUDIENCE_ID_LOCATORS.size() > 0) {
            List<String> audienceIdsList = AUDIENCE_ID_LOCATORS.texts();
            IntStream.range(0, audienceIdsList.size()).forEach(i -> deleteClientAudience(audienceIdsList.get(i)));
        }
        log.info("There aren't any audiences");
    }
}
