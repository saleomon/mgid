package pages.cab.products.helpers;

import core.base.HelpersInit;
import core.helpers.BaseHelper;
import org.openqa.selenium.By;
import pages.cab.products.logic.CabTeasers;

import java.util.Objects;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.products.locators.TeaserAddLocators.*;
import static testData.project.ClientsEntities.WEBSITE_MGID_GOODHITS_DOMAIN;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class TeaserAddHelper {
    private final HelpersInit helpersInit;
    private boolean isGenerateNewValues = false;
    private boolean isNeedToEditImage = true;
    private boolean isSetSomeFields = false;
    private boolean needToEditCategory = true;
    private boolean isNeedToEditUrl = true;
    private boolean isSetImageFromRequest = false;
    private boolean useDiscount = false;
    private boolean useTitle = false;
    private boolean useDescription = false;
    private boolean useCallToAction = false;
    private boolean usePriceOfClick = true;
    private final boolean useImprovement = false;
    private boolean setIsGenerateAutoTitle = false;

    public TeaserAddHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public void setNeedToEditImage(boolean needToEditImage) {
        isNeedToEditImage = needToEditImage;
    }

    public void setIsGenerateAutoTitle(boolean isGenerateAutoTitle) {
        setIsGenerateAutoTitle = isGenerateAutoTitle;
    }

    public void setNeedToEditUrl(boolean isNeedToEditUrl) {
        this.isNeedToEditUrl = isNeedToEditUrl;
    }

    public void isSetImageFromRequest(boolean isSetImageFromRequest) {
        this.isSetImageFromRequest = isSetImageFromRequest;
    }
    public void isNeedToEditCategory(boolean needToEditCategory) {
        this.needToEditCategory = needToEditCategory;
    }

    public TeaserAddHelper setUseTitle(boolean useTitle) {
        this.useTitle = useTitle;
        return this;
    }

    public TeaserAddHelper setUseDescription(boolean useDescription) {
        this.useDescription = useDescription;
        return this;
    }

    public void setUseCallToAction(boolean useCallToAction) {
        this.useCallToAction = useCallToAction;
    }

    public TeaserAddHelper setUseDiscount(boolean useDiscount) {
        this.useDiscount = useDiscount;
        return this;
    }

    public TeaserAddHelper setGenerateNewValues(boolean generateNewValues) {
        this.isGenerateNewValues = generateNewValues;
        return this;
    }

    public void setUsePriceOfClick(boolean usePriceOfClick) {
        this.usePriceOfClick = usePriceOfClick;
    }

    public TeaserAddHelper setSomeFields(boolean setSomeFields) {
        isSetSomeFields = setSomeFields;
        return this;
    }

    /**
     * Заполнение поля для url
     */
    public String setUrl(String url) {
        String teasersUrl;
        if (!isNeedToEditUrl) {
            return URL_INPUT.getValue();
        } else {
            teasersUrl = url != null ? url : "https://" + WEBSITE_MGID_GOODHITS_DOMAIN;
        }
        if (!URL_INPUT.text().equals(teasersUrl)) {
            clearAndSetValue(URL_INPUT.shouldBe(visible), teasersUrl);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(teasersUrl);
    }

    /**
     * load image
     */
    public String loadImage(String imageName, CabTeasers.OwnershipOfMedia file) {
        if(isSetImageFromRequest) {
            IMAGES_POPUP_OPEN.click();
            BLOCKS_OF_IMAGES.first().click();
            SUBMIT_IMAGE_FROM_REQUEST_BUTTON.click();

            return helpersInit.getBaseHelper().extractUrls(BLOCKS_OF_IMAGES.first().attr("style")).get(0);
        }
        if (!isNeedToEditImage) return null;
        if (isSetSomeFields && imageName == null && !useImprovement) {
            return null;
        }

        return loadImageByLocal(imageName, file);
    }


    /**
     * load image
     */
    public void rotateValidFormats(Boolean state) {
        if(state != null) {
            SHOW_VALID_FORMATS.setSelected(state);
        }
    }

    /**
     * load image
     */
    public void loadImageCloudinary(String imageName, CabTeasers.OwnershipOfMedia file) {
        String image = imageName == null || isGenerateNewValues ?
                LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg" :
                LINK_TO_RESOURCES_IMAGES + imageName;
        IMAGE_FIELD.scrollTo().sendKeys(image);

        $("[name=\"uploadImageSource\"]").selectRadio(file.getValue());
        $("[class*=\"saveImageUploadSource").click();

        waitForAjax();

        helpersInit.getBaseHelper().getTextAndWriteLog(IMAGE_INPUT.val());
    }

    /**
     * load local image
     */
    private String loadImageByLocal(String imageName, CabTeasers.OwnershipOfMedia file) {
        String image = imageName == null || isGenerateNewValues ?
                LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg" :
                LINK_TO_RESOURCES_IMAGES + imageName;

        if (imageName != null && imageName.equals("")) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(IMAGE_INPUT.val());
        }
        IMAGE_FIELD.scrollTo().sendKeys(image);

        $("[name=\"uploadImageSource\"]").selectRadio(file.getValue());
        $("[class*=\"saveImageUploadSource").click();
        waitForAjax();

        return helpersInit.getBaseHelper().getTextAndWriteLog(IMAGE_INPUT.val());
    }

    /**
     * set 'Title'
     */
    public String setTitle(String title) {
        String titleNews = null;
        if (setIsGenerateAutoTitle) return title;
        if (isSetSomeFields && title == null) return null;
        if (useTitle) {
            titleNews = title == null ? "Title teaser's " + BaseHelper.getRandomWord(3) : title;
            clearAndSetValue(TITLE_INPUT, titleNews);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(titleNews);
    }

    /**
     * set 'Description'
     */
    public String setDescription(String description) {
        String desc = null;
        if (isSetSomeFields && description == null) return null;
        if (useDescription) {
            desc = description == null ? "descriptionteaser" + BaseHelper.getRandomWord(2) : description;
            clearAndSetValue(DESCRIPTION_INPUT, desc);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(desc);
    }

    /**
     * set 'CallToAction'
     */
    public String setCallToAction(String callToAction) {
        String call = null;
        if (useCallToAction) {
            if (isSetSomeFields && callToAction == null) return null;

            call = (callToAction == null || isGenerateNewValues) ?
                    "Call something " + BaseHelper.getRandomWord(2) :
                    callToAction;
            clearAndSetValue(CALL_TO_ACTION_INPUT, call);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(call);
    }

    /**
     * choose 'Category'
     */
    public String chooseCategory(String categoryId) {
        if(!needToEditCategory) return categoryId;
        if (isSetSomeFields && categoryId == null) return null;

        if (categoryId == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT));
        }
        return helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT, categoryId);
    }

    /**
     * choose 'teaser type'
     */
    public String chooseTeaserType(String teaserType) {
        if (isSetSomeFields && teaserType == null) return null;

        if (teaserType == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(TYPE_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(TYPE_SELECT, teaserType);
    }

    /**
     * choose 'teaser landing'
     */
    public String chooseLanding(String landing) {
        if (isSetSomeFields && landing == null) return null;

        if (landing == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(LANDING_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(LANDING_SELECT, landing);
    }

    /**
     * choose 'life time'
     */
    public String chooseLifeTime(String lifeTime) {
        if (lifeTime == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(LIVETIME_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(LIVETIME_SELECT, lifeTime);
    }

    /**
     * choose 'life time'
     */
    public String chooseLifeTimeEdit(String lifeTime) {
        if (isSetSomeFields && lifeTime == null) return null;

        if (lifeTime == null || isGenerateNewValues) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(LIVETIME_EDIT_SELECT));
        }
        return helpersInit.getBaseHelper().selectCustomValue(LIVETIME_EDIT_SELECT, lifeTime);
    }

    /**
     * set 'price of click'
     */
    public String setPriceOfClick(String priceOfClick) {
        if (!usePriceOfClick) return null;

        String price = (priceOfClick == null || isGenerateNewValues) ?
                helpersInit.getBaseHelper().randomNumberDouble1CharAfterDot(12.5, 15.5) :
                priceOfClick;
        if (price.contains(",")) price = price.replace(",", ".");
        clearAndSetValue(CPC_INPUT, price);
        return helpersInit.getBaseHelper().getTextAndWriteLog(price);
    }

    /**
     * Заполнение цены тизера в блоке цен
     */
    public String fillProductPrice(String price) {
        String productPrice = null;
        if (useDiscount) {
            if (isSetSomeFields && price == null) return null;

            if (price == null || isGenerateNewValues) {
                productPrice = String.valueOf(helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(10.10, 49.90)).replace(",", ".");
                clearAndSetValue(PRICE_INPUT.shouldBe(visible), productPrice);
            } else {
                productPrice = price;
                clearAndSetValue(PRICE_INPUT.shouldBe(visible), price);
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(productPrice);
    }

    /**
     * Заполнение старой цены тизера в блоке цен
     */
    public String fillOldProductPrice(String oldPrice) {
        String oldProductPrice = null;
        if (useDiscount) {
            if (isSetSomeFields && oldPrice == null) return null;

            if (oldPrice == null || isGenerateNewValues) {
                oldProductPrice = String.valueOf(helpersInit.getBaseHelper().randomNumberDouble2CharsAfterDot(50.10, 99.90)).replace(",", ".");
                clearAndSetValue(PRICE_OLD_INPUT.shouldBe(visible), oldProductPrice);
            } else {
                oldProductPrice = oldPrice;
                clearAndSetValue(PRICE_OLD_INPUT.shouldBe(visible), oldPrice);
            }
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(oldProductPrice);
    }

    /**
     * Выбор валюти в блоке цен тизера
     */
    public String selectCurrency(String currency) {
        if (useDiscount) {
            if (isSetSomeFields && currency == null) return null;

            if (currency != null) {
                helpersInit.getBaseHelper().selectCustomValue(CURRENCY_SELECT, currency);
                return currency;
            } else {
                return helpersInit.getBaseHelper().selectRandomValue(CURRENCY_SELECT);
            }
        }
        return null;
    }

    /**
     * Заполнение скидки тизера в блоке цен
     */
    public String setProductDiscount(String discount) {
        if (useDiscount) {
            if (isSetSomeFields && discount == null) return null;

            if (discount != null && !isGenerateNewValues) {
                clearAndSetValue(DISCOUNT_INPUT, discount);
                return helpersInit.getBaseHelper().getTextAndWriteLog(discount);
            }
            return helpersInit.getBaseHelper().getTextAndWriteLog(DISCOUNT_INPUT.val());
        }
        return null;
    }

    /**
     * switch 'use improvement' checkbox
     */
    public void useImprovementSwitch() {
        if (USE_IMPROVEMENT_CHECKBOX.isDisplayed())
            markUnMarkCheckbox(useImprovement, USE_IMPROVEMENT_CHECKBOX);
    }

    /**
     * save teaser
     */
    public void saveTeaser() {
        SAVE_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void saveToDraft() {
        SAVE_TO_DRAFT.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check 'category'
     */
    public boolean checkCategory(String categoryId, boolean needToCheck) {
        System.out.println("addd - " + needToCheck);
        return !needToCheck || helpersInit.getBaseHelper().getTextAndWriteLog(Objects.requireNonNull(CATEGORY_SELECT.find(By.cssSelector("[data-id='" + categoryId + "']")).attr("class")).contains("selected-cat"));
    }

    /**
     * check 'url'
     */
    public boolean checkUrl(String domain) {
        return helpersInit.getBaseHelper().checkDatasetContains(URL_INPUT.text(), domain);
    }

    /**
     * check 'title'
     */
    public boolean checkTitle(String title) {
        return helpersInit.getBaseHelper().checkDatasetEquals(title.replace("\\", ""), TITLE_INPUT.text());
    }

    /**
     * check 'description'
     */
    public boolean checkDescription(String description) {
        return !useDescription || helpersInit.getBaseHelper().checkDatasetEquals(description, DESCRIPTION_INPUT.text());
    }

    /**
     * check 'life time'
     */
    public boolean checkLifeTime(String lifeTime) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(LIVETIME_EDIT_SELECT, lifeTime));
    }

    /**
     * check 'price'
     */
    public boolean checkPrice(String price) {
        return !useDiscount || comparisonData(parseDouble(Objects.requireNonNull(PRICE_INPUT.val())), parseDouble(price), 0.9);
    }

    /**
     * check 'old price'
     */
    public boolean checkOldPrice(String oldPrice) {
        return !useDiscount || comparisonData(parseDouble(Objects.requireNonNull(PRICE_OLD_INPUT.val())), parseDouble(oldPrice), 0.9);
    }

    /**
     * check 'discount'
     * при проверке сделан костыль так как при не известныч причинах скидка при редактировании увеличивается на 1
     */
    public boolean checkDiscount(String discount) {
        return !useDiscount || helpersInit.getBaseHelper().comparisonData(helpersInit.getBaseHelper().parseInt(helpersInit.getBaseHelper().getTextAndWriteLog(DISCOUNT_INPUT.val())), helpersInit.getBaseHelper().parseInt(discount), 1);
    }

    /**
     * check 'currency' select
     */
    public boolean checkCurrency(String currency) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(CURRENCY_SELECT, currency));
    }

    /**
     * check adType in create teaser for task
     * check TYPE_SELECT has 1 option
     * check TYPE_SELECT has only customType
     */
    public boolean checkTeaserTypeForTaskInterface(String type) {
        return TYPE_SELECT.findAll("option").size() == 1 &&
                TYPE_SELECT.$("[value=" + type + "]").exists();
    }

}
