package pages.cab.products.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import core.helpers.BaseHelper;
import pages.cab.products.logic.CabTeasers;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TeaserListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class TeaserListHelper {
    private final HelpersInit helpersInit;
    private final Logger log;
    private boolean useDescription = false;
    private boolean useDiscount = false;
    private boolean isNeedToEditImage = true;
    private boolean needToEditCategory = true;

    public TeaserListHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public TeaserListHelper setNeedToEditImage(boolean needToEditImage) {
        isNeedToEditImage = needToEditImage;
        return this;
    }

    public void isNeedToEditCategory(boolean needToEditCategory) {
        this.needToEditCategory = needToEditCategory;
    }
    public TeaserListHelper setUseDescription(boolean useDescription) {
        this.useDescription = useDescription;
        return this;
    }

    public TeaserListHelper setUseDiscount(boolean useDiscount) {
        this.useDiscount = useDiscount;
        return this;
    }

    /**
     * client_id filter is displayed
     */
    public boolean isClientIdFilterDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(FILTER_CLIENT_ID_INPUT.isDisplayed());
    }

    /**
     * check 'url'(domain)
     */
    public boolean checkUrl(String url) {
        return helpersInit.getBaseHelper().checkDatasetContains(URL_INPUT.val(), url);
    }

    /**
     * check 'Title'
     */
    public boolean checkTitle(String title) {
        return helpersInit.getBaseHelper().checkDatasetEquals(INLINE_TITLE_LOCATOR.text(), title);
    }

    /**
     * get count teasers on page
     */
    public int getCountTeasersOnPage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(TEASER_ID_LOCATORS).size());
    }

    /**
     * check 'Description'
     */
    public boolean checkDescription(String description) {
        return !useDescription || helpersInit.getBaseHelper().checkDatasetEquals(INLINE_DESCRIPTION_LABEL.text(), description);
    }

    /**
     * check 'Category' by id
     */
    public boolean checkCategoryId(String categoryId, boolean needToCheckCategory) {
        if (needToCheckCategory) {
            for (SelenideElement s : CATEGORIES_LABEL) {
                if (!Objects.requireNonNull(s.attr("data-category-id")).equals(categoryId)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * check 'teaser type'
     */
    public boolean checkTeaserType(String type) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AD_TYPES_INLINE.texts().stream().allMatch(i -> i.toLowerCase().equals(type)));
    }

    /**
     * check 'Landing'
     */
    public boolean checkLanding(String landing) {
        return helpersInit.getBaseHelper().checkDatasetEquals(LANDING_TYPES_INLINE.text().toLowerCase(), landing);
    }

    /**
     * check 'live time'
     */
    public boolean checkLiveTime(String liveTime) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(LIVE_TIME_SELECT, liveTime));
    }

    /**
     * check 'price of click'
     */
    public boolean checkPriceOfClick(String priceOfClick) {
        return helpersInit.getBaseHelper().checkDatasetContains(CPC_INPUT.val(), priceOfClick);
    }

    /**
     * check 'Price'
     */
    public boolean checkPrice(String price) {
        return !useDiscount || comparisonData(parseDouble(Objects.requireNonNull(PRODUCT_PRICE_INPUT.val())), parseDouble(price), 0.9);
    }

    /**
     * check 'Price old'
     */
    public boolean checkPriceOld(String priceOld) {
        return !useDiscount || comparisonData(parseDouble(Objects.requireNonNull(PRODUCT_OLD_PRICE_INPUT.val())), parseDouble(priceOld), 0.9);
    }

    /**
     * check 'Discount'
     * при проверке сделан костыль так как при не известныз причинах скидка при редактировании увеличивается на 1
     */
    public boolean checkDiscount(String discount) {
        return !useDiscount || helpersInit.getBaseHelper().comparisonData(helpersInit.getBaseHelper().parseInt(helpersInit.getBaseHelper().getTextAndWriteLog(PRODUCT_DISCOUNT_INPUT.val())), helpersInit.getBaseHelper().parseInt(discount), 1);
    }

    /**
     * check 'Currency'
     */
    public boolean checkCurrency(String currency) {
        return !useDiscount || helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(CURRENCY_SELECT, currency));
    }

    public List<String> getTeasersId() {
        return $$(TEASER_ID_LOCATORS).texts();
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * метод одобрения тизера
     * RKO
     */
    public void approveTeaser() {
        if (APPROVE_ICON.isDisplayed()) {
            log.info("approve teaser - start");
            APPROVE_ICON.shouldBe(visible).click();
            waitForAjax();
            REJECT_ICON.shouldBe(visible);
            log.info("approve teaser - finish");
        }
        if (CONTINUE_WITHOUT_LINKING.isDisplayed()) CONTINUE_WITHOUT_LINKING.click();
        waitForAjax();
        log.info("approve icon isn't displayed");
    }

    /**
     * метод отклонения тизера
     * (клик по иконке, выбор причины, сохранение формы)
     * Subnet(0/2/3/5) - по умолчанию - 0
     * RKO
     */
    public void rejectTeaser(String reason, String... subnetId) {
        if (REJECT_ICON.isDisplayed()) {
            rejectIconClick();
            chooseRejectReason(reason, subnetId);
            saveRejectReasonForm();
            if (CONTINUE_WITHOUT_LINKING.isDisplayed()) CONTINUE_WITHOUT_LINKING.click();
        }
    }

    /**
     * сохраняем форму "Select the reason for rejection"
     */
    public void saveRejectReasonForm() {
        REJECT_FORM_SUBMIT_BUTTON.shouldBe(visible).hover().click();
        log.info("Changes were saved");
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * метод выбора причины отклонения тизера и его отклонение
     * numberOfReason - принимает порядковый номер причины 0-82 и причину remoderation
     * Subnet(0/2/3/5) - по умолчанию - 0
     */
    public void chooseRejectReason(String numberOfReason, String... subnetId) {
        String subnet = subnetId.length > 0 ? subnetId[0] : "0";

        //choose reject reason
        executeJavaScript(
                "var numb = " + numberOfReason + ";" +
                        "$('.rejection-reason[data-subnet=" + subnet + "]')[numb].click();" +
                        "$('.addCommentPlus[data-subnet=" + subnet + "]')[numb].click();" +
                        "$('.commentField[data-subnet=" + subnet + "]')[numb].append('test sok');");
        log.info("Comment for reason was filled");
    }


    /**
     * кликаем по иконке "Reject Teaser"
     * RKO
     */
    public void rejectIconClick() {
        log.info("rejectIconClick");
        REJECT_ICON.shouldBe(visible).click();
        REJECT_FORM.shouldBe(visible);
    }


    /**
     * Удаление тизера через иконку
     */
    public boolean deleteTeaser() {
        REJECT_FORM.shouldBe(hidden, ofSeconds(8));
        TEASER_ID_LOCATOR.shouldBe(visible);
        if (DELETE_ICON.isDisplayed()) {
            log.info("deleteTeaser - start");
            DELETE_ICON.shouldBe(visible, ofSeconds(8)).click();
            REASON_DELETE_TEASER_FIELD.shouldBe(visible).val("test delete teaser");
            REASON_DELETE_TEASER_BUTTON.click();
            waitForAjaxLoader();
            UNDELETE_ICON.shouldBe(visible, ofSeconds(12));
            log.info("deleteTeaser - finish");
        }
        return UNDELETE_ICON.is(visible);
    }

    /**
     * select all teasers checkboxes
     */
    public void selectAllTeasers() {
        log.info("selectAllTeasers");
        SELECT_ALL_CHECKBOXES_LINK.shouldBe(visible).scrollTo().click();
    }

    /**
     * choose mass actions
     */
    public void chooseMassActions(String action) {
        log.info("chooseMassActions");
        MASS_ACTIONS_SELECT.selectOptionByValue(action);
        waitForAjax();
    }

    /**
     * submit mass actions
     */
    public void submitMassActions() {
        MASS_ACTION_SUBMIT.click();
        waitForAjaxLoader();
        checkErrors();
    }

    public void selectRejectReasons() {
        MASS_ACTION_SELECT_REJECT_REASONS.click();
        waitForAjaxLoader();
        checkErrors();
    }

    public void submitRejectReasons() {
        REJECT_REASONS_SUBMIT.click();
        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * mass change 'landing type'
     */
    public String editLandingTypeOnMassAction(String... customLanding) {
        if (customLanding.length > 0) {
            return helpersInit.getBaseHelper().selectCustomValue(MASS_LANDING_SELECT, customLanding[0]);
        }
        return helpersInit.getBaseHelper().selectRandomValue(MASS_LANDING_SELECT);
    }

    /**
     * mass change 'Edit category'
     */
    public String editCategoryOnMassAction(String...category) {
        return category.length == 0 ? helpersInit.getBaseHelper().selectValueFromParagraphListJs(MASS_CATEGORY_SELECT) : helpersInit.getBaseHelper().selectValueFromParagraphListJs(MASS_CATEGORY_SELECT, category[0]);
    }

    /**
     * mass change 'Ad type'
     */
    public String editAdTypeOnMassAction(String... customType) {
        if (customType.length > 0) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomValue(MASS_AD_TYPE_SELECT, customType[0]));
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(MASS_AD_TYPE_SELECT));
    }

    /**
     * mass change part title - set old value
     */
    public void setOldTitleMass(String titleOld) {
        clearAndSetValue(MASS_CHANGE_TITLE_OLD_VALUE_INPUT, titleOld);
    }

    /**
     * mass change part title - set new value
     */
    public void setNewTitleMass(String titleNew) {
        clearAndSetValue(MASS_CHANGE_TITLE_NEW_VALUE_INPUT, titleNew);
    }

    /**
     * click radio 'Copy to one campaign'
     */
    public void clickCopyToOneCampaignRadioMass() {
        executeJavaScript("$(\"div[aria-labelledby='ui-dialog-title-copy_teasers_dialog']\").width(800).css(\"margin-left\", \"200px\")");
    }

    /**
     * click radio 'Copy to many campaign'
     */
    public void clickCopyToManyCampaignsRadioMass() {
        executeJavaScript("$(\"div[aria-labelledby='ui-dialog-title-copy_teasers_dialog']\").width(800).css(\"margin-left\", \"200px\")");
    }

    /**
     * set CPC for copy teaser all campaigns
     */
    public void setCpcForCopyAllCampaigns(String cpc) {
        COPY_CPC_FOR_ALL_CAMPAIGNS_INPUT.shouldBe(visible).sendKeys(cpc);
    }

    /**
     * set ids of campaigns
     */
    public void fillCampaignIds(int... campaignIds) {
        for (int i = 0; i < campaignIds.length; i++) {
            COPY_CAMPAIGN_IDS_INPUT.get(i).shouldBe(visible).sendKeys(String.valueOf(campaignIds[i]));
            for (int j = i; j < campaignIds.length-1; j++) $("img[src*='plus2.png']").click();
        }
    }

    public void enableBlockingDueCopying(boolean state) {
        BLOCK_TEASER_DUE_COPYING_CHECKBOX.asFixedIterable().forEach(elem -> markUnMarkCheckbox(state, elem));
    }

    /**
     * click execute button for copy teaser into campaigns
     */
    public void clickCopyTeaserToCampaigns() {
        log.info("click execute button");
        COPY_BUTTON.click();
//        waitForAjaxLoader();
        checkErrors();
    }

    /**
     * check amount of teaser at  interface
     */
    public boolean checkAmountOfTeaser(int expectedAmount) {
        return helpersInit.getBaseHelper().checkDataset(expectedAmount, TEASER_IDS_LOCATOR.size());
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ICON_APAC.isDisplayed());
    }

    /**
     * Метод редактирует ландинг тизера инлайн
     * При пустом значении "newLandingType" будет выбираться рандомный тип лендинга
     *
     * @param isDeleteLpTags - delete or save LP tags
     * @param newLandingType - новый лендинг тизера
     */
    public void chooseLandingInline(boolean isDeleteLpTags, String... newLandingType) {
        String landing;
        log.info("Open popup for landing types editing");
        clickInvisibleElementJs(LANDING_TYPES_INLINE.scrollTo().shouldBe(visible, ofSeconds(10)));
        waitForAjaxLoader();
        LANDING_SELECT.shouldBe(visible, ofSeconds(5)).findAll(By.tagName("option")).get(0).shouldBe(visible, ofSeconds(10));
        if (newLandingType.length != 0) {
            log.info("Set landing type: " + newLandingType[0]);
            landing = newLandingType[0];
            LANDING_SELECT.shouldBe(visible, ofSeconds(8)).selectOptionContainingText(landing.toUpperCase());
        } else {
            helpersInit.getBaseHelper().selectRandomValue(LANDING_SELECT);
        }
        log.info("Save selected landing type");
        LANDING_SELECT_SUBMIT.click();
        waitForAjax();

        // tags cloud on inline/mass change LP displayed TAGS popup with DELETE OR SAVE BUTTONS
        if (TAGS_DELETE_BUTTON.isDisplayed()) {
            log.info("TAGS DELETE OR SAVE BUTTONS -> are displayed");
            if (isDeleteLpTags) clickButtonDeleteTags();
            else clickButtonSaveTags();
        }

        LANDING_SELECT_SUBMIT.shouldNot(visible, ofSeconds(5));
        waitForAjaxLoader();
    }

    /**
     * Метод редактирует тип тизера инлайн
     *
     * @param type - новый тип тизера
     */
    public void chooseAdTypeInline(String... type) {
        String ad;
        log.info("Open popup for ad types editing");
        AD_TYPE_INLINE.shouldBe(visible, ofSeconds(5)).click();
        waitForAjaxLoader();
        AD_TYPE_POPUP_SELECT.findAll(By.tagName("option")).get(0).shouldBe(visible);
        if (!AD_TYPE_POPUP_SELECT.has(attribute("disabled"))) {
            if (type.length != 0) {
                ad = type[0];
                log.info("Set ad type: " + ad);
                AD_TYPE_POPUP_SELECT.shouldBe(visible, ofSeconds(5)).selectOptionByValue(ad);
            } else {
                ad = helpersInit.getBaseHelper().selectRandomValue(AD_TYPE_POPUP_SELECT);
                log.info("Set ad type: " + ad);
            }
        } else {
            ad = AD_TYPE_POPUP_SELECT.attr("value");
            log.info("Ad type select is disabled - " + ad);
        }
        log.info("Save selected ad type");
        AD_TYPE_POPUP_SUBMIT.click();
        AD_TYPE_POPUP_SELECT.shouldBe(hidden);
        waitForAjaxLoader();
    }

    /**
     * изменяем тайтл инлайн
     */
    public String editTitleInline(String... title) {
        INLINE_TITLE_LOCATOR.shouldBe(visible).doubleClick();
        String teaserTitle = title.length > 0 ? title[0] : "Test title_edit_" + BaseHelper.getRandomWord(3);
        CLEAR_FIELD_BUTTON.click();
        TITLE_INPUT.sendKeys(teaserTitle + Keys.ENTER);
        TITLE_INPUT.shouldBe(hidden);
        waitForAjax();
        sleep(3000);
        return teaserTitle;
    }

    ///////////////////////////////// TAGS - START  //////////////////////////////////////////////////////////

    /**
     * clear all tags
     */
    public void clearAllTags() {
        openTagsCloudForm();
        TAGS_ELEMENTS_LIST.filter(Condition.attribute("class","active")).asDynamicIterable().forEach(SelenideElement::click);
        saveTagsForm();
    }

    /**
     * открываем форму с выбором TAGS CLOUD
     */
    public void openTagsCloudForm() {
        if (ICON_TAGS_CLOUD_ON.isDisplayed()) {
            clickInvisibleElementJs(ICON_TAGS_CLOUD_ON.shouldBe(visible));
        } else {
            clickInvisibleElementJs(ICON_TAGS_CLOUD_OFF.shouldBe(visible, ofSeconds(8)));
        }
    }

    /**
     * сохраняем форму
     */
    public void saveTagsForm() {
        TAGS_FORM_SAVE_BUTTON.click();
        log.info("TAGS_FORM_SAVE_BUTTON - click");
        log.info("overlay is hidden");
        waitForAjaxLoader();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        TAGS_FORM_SAVE_BUTTON.shouldBe(hidden);
    }

    /**
     * выбираем рандомные теги для разных типов(TITLE_CHECK, IMAGE_CHECK, LINK_CHECK)
     */
    public Multimap<String, String> chooseRandomTags(List<String> tagsType, boolean isMassTags, String...customTags) {
        Multimap<String, String> tags = ArrayListMultimap.create();
        List<String> tagsCloudName;
        String form = isMassTags ? "massTagsDialog" : "tagsDialog";

        for (String type : tagsType) {

            //получаем список елементов тегов для конкретного типа
            ElementsCollection tagsElements = $$("#" + form + " .tagsBlock [data-name]:not([style*='none'])[data-type=" + type + "]");

            // получаем рандомные имена тегов
            tagsCloudName = customTags != null ? Arrays.asList(customTags) : helpersInit.getBaseHelper().getRandomCountValue(tagsElements.size(), 3).stream()
                    .map(i -> tagsElements.get(i).attr("data-name"))
                    .collect(Collectors.toList());

            //выбираем(включаем) их
            tagsCloudName.forEach(i -> tagsElements.findBy(attribute("data-name", i)).click());

            //записываем отобранные теги в коллекцию tags
            tagsCloudName.forEach(i -> tags.put(type, i));
            tagsCloudName.forEach(System.out::println);
        }
        return tags;
    }

    /**
     * проверка отображения тегов после сохранения
     */
    public boolean checkChooseTags(Multimap<String, String> tags, List<String> tagsType, boolean isDeleteTags) {
        String tagsClass = isDeleteTags ? "" : "active";

        return tagsType.stream()
                .allMatch(k -> tags.get(k).stream()
                        .allMatch(i -> Objects.requireNonNull($$("#tagsDialog .tagsBlock [data-name]:not([style*='none'])[data-type=" + k + "]").findBy(attribute("data-name", i))
                                .attr("class")).equals(tagsClass)));
    }

    /**
     * появляющийся поп-ап после инлайн редактирования тайтла или lp с возможностью удалить теги
     */
    public void clickButtonDeleteTags() {
        log.info("clickButtonDeleteTags");
        TAGS_DELETE_BUTTON.shouldBe(visible).click();
        waitForAjax();
        waitForAjaxLoader();
    }

    /**
     * filter teasers by product
     */
    public void filterTeasersByProduct(String regions) {
        sendKey(PRODUCT_FILTER, regions);
        FILTER_BUTTON.click();
        waitForAjax();
    }

    /**
     * mark show certified checkbox
     */
    public void enableShowCertifiedCheckbox(boolean state) {
        markUnMarkCheckbox(state, SHOW_CERTIFIED_CHECKBOX);
    }

    /**
     * check filter result
     */
    public boolean checkFilterResult() {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(TEASER_ID_LOCATORS).size() > 0);
    }

    /**
     * check editing description inline
     */
    public String editDescriptionInline(String teaserDescription) {
        String description = teaserDescription == null ?
                "Description " + BaseHelper.getRandomWord(3) :
                teaserDescription;
        INLINE_DESCRIPTION_LABEL./*scrollIntoView("{display: \"inline-block\"}").*/doubleClick();
        CLEAR_FIELD_BUTTON.click();
        DESCRIPTION_INPUT.sendKeys(description + Keys.ENTER);
        CATEGORY_LABEL.click();
        DESCRIPTION_INPUT.shouldBe(hidden);
        waitForAjax();
        sleep(3000);
        return description;
    }

    /**
     * check editing call to action inline
     */
    public String editCallToActionInline(String callToAction) {
        String description = callToAction == null ?
                "callToAction " + BaseHelper.getRandomWord(3) :
                callToAction;
        INLINE_CALL_TO_ACTION_LABEL.scrollIntoView(true).doubleClick();
        CLEAR_FIELD_CALL_TO_ACTION_BUTTON.click();
        CALL_TO_ACTION_INPUT.sendKeys(description);
        CATEGORY_LABEL.click();
        waitForAjax();
        return description;
    }

    /**
     * check is teaser ids locator visible
     */
    public boolean isVisibleTeaserIdLocator() {
        return TEASER_ID_LOCATOR.isDisplayed();
    }

    /**
     * check is teaser ids locator visible
     */
    public boolean checkPosibilityOfEditingCallToAction() {
        return INLINE_CALL_TO_ACTION_LABEL.isDisplayed();
    }

    /**
     * появляющийся поп-ап после инлайн редактирования тайтла или lp с возможностью сохранить теги
     */
    public void clickButtonSaveTags() {
        log.info("clickButtonSaveTags");
        TAGS_SAVE_BUTTON.shouldBe(visible).click();
        SAVE_BUTTON.shouldBe(hidden, ofSeconds(8));
        waitForAjax();
        waitForAjaxLoader();
    }

    /**
     * закрываем форму с тегами по иконке "Close"
     */
    public void closeTagsForm() {
        if (TAGS_FORM_CLOSE_BUTTON.isDisplayed()) {
            clickInvisibleElementJs(TAGS_FORM_CLOSE_BUTTON);
            waitForAjaxLoader();
        }
    }

    /**
     * метод проверки корректной работы по поиску тегов
     */
    public boolean checkWorkSearchingTagsFilter() {
        String temp = TAGS_ELEMENTS_LIST.get(randomNumbersInt(TAGS_ELEMENTS_LIST.size())).attr("data-name");
        log.info("searchTags - " + temp);
        if (Objects.requireNonNull(temp).length() > 3) {
            temp = temp.substring(0, 3);
        }
        TAGS_FORM_SEARCH.sendKeys(temp);
        String finalTemp = temp;
        TAGS_ELEMENTS_LIST.shouldBe(CollectionCondition.allMatch("checkWorkSearchingTagsFilter", i -> i.getAttribute("data-name").contains(finalTemp)));
        return true;
    }

    ///////////////////////////////// TAGS - FINISH  //////////////////////////////////////////////////////////

    /**
     * check exist custom value(option) in mass action select
     */
    public boolean checkExistCustomValueInMassActions(String value) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MASS_ACTIONS_SELECT.$("[value=" + value + "]").exists());
    }

    /**
     * check exist custom value(option) in mass action select
     */
    public boolean checkDisplayedTeasersOfCampaign(String... value) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$(CAMPAIGNS_IDS_LOCATOR).texts().stream().allMatch(element -> element.contains(value[0]) || element.contains(value[1])));
    }

    /**
     * Get Block teaser icon title
     */
    public String getBlockTeaserTitle() {
        return UNBLOCK_TEASER_ICON.attr("title");
    }

    /**
     * Unblock teaser
     */
    public boolean unBlockTeaser() {
        TEASER_ID_LOCATOR.shouldBe(visible);
        if (UNBLOCK_TEASER_ICON.isDisplayed()) {
            UNBLOCK_TEASER_ICON.click();
            waitForAjax();
        }
        return BLOCK_TEASER_ICON.is(visible);
    }

    public boolean blockTeaserByIcon(boolean withCloaking, String blockedReasonValue) {
        TEASER_ID_LOCATOR.shouldBe(visible);
        BLOCK_TEASER_ICON.click();
        chooseBlockTypeWithReason(withCloaking, blockedReasonValue);
        BLOCK_TEASER_CONFIRM.click();
        return BLOCK_TEASER_CONFIRM.is(hidden);
    }

    public void blockTeaserByMass(boolean withCloaking, String blockedReasonValue) {
        TEASER_ID_LOCATOR.shouldBe(visible);
        SELECT_REASON_BUTTON.click();
        chooseBlockTypeWithReason(withCloaking, blockedReasonValue);
        BLOCK_TEASER_CONFIRM.click();
    }

    public void blockTeaserBySchedule(boolean withCloaking, String blockedReasonValue) {
        TEASER_ID_LOCATOR.shouldBe(visible);
        BLOCK_BY_SCHEDULE_BUTTON.click();
        TIME_INTERVALS.get(randomNumbersInt(TIME_INTERVALS.size())).click();
        if (withCloaking) {
            BLOCK_TEASER_WITH_CLOAKING_RADIO_SCHEDULE.click();
            BLOCK_TEASER_WITH_CLOAKING_INPUT_SCHEDULE.sendKeys(blockedReasonValue);
        } else {
            BLOCK_TEASER_REGULAR_RADIO_SCHEDULE.click();
            BLOCK_TEASER_REGULAR_INPUT_SCHEDULE.sendKeys(blockedReasonValue);
        }
        waitForAjax();
        CONFIRM_BUTTON.click();
    }

    /**
     * Check blocking off teaser which is blocked by client in Dashboard
     */
    public boolean isTeaserBlockedByClient() {
        helpersInit.getBaseHelper().waitDisplayedElement(UNBLOCK_TEASER_BY_CLIENT_ICON);
        return UNBLOCK_TEASER_BY_CLIENT_ICON.isDisplayed();
    }

    /**
     * Check blocking off teaser which is blocked in Dashboard by Account manager (go through gogol)
     */
    public boolean isTeaserBlockedByAccountManager() {
        helpersInit.getBaseHelper().waitDisplayedElement(UNBLOCK_TEASER_BY_ACC_MANAGER_ICON);
        return UNBLOCK_TEASER_BY_ACC_MANAGER_ICON.isDisplayed();
    }

    /**
     * Check blocking off teaser which created in campaign with flag 'block_before_show'
     */
    public boolean isTeaserBlockedBySystem() {
        helpersInit.getBaseHelper().waitDisplayedElement(UNBLOCK_TEASER_BY_SYSTEM_ICON);
        return UNBLOCK_TEASER_BY_SYSTEM_ICON.isDisplayed();
    }

    /**
     * Fill reason for blocking teaser
     */
    public void chooseBlockTypeWithReason(boolean cloaking, String blockedReasonValue) {
        if (cloaking) {
            BLOCK_TEASER_WITH_CLOAKING_RADIO.click();
            BLOCK_TEASER_WITH_CLOAKING_INPUT.sendKeys(blockedReasonValue);
            waitForAjax();
        } else {
            BLOCK_TEASER_REGULAR_RADIO.click();
            BLOCK_TEASER_REGULAR_INPUT.sendKeys(blockedReasonValue);
        }
        waitForAjax();
    }

    /**
     * get amount of teasers at list
     */
    public Integer getAmountOfTeasers() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(TEASER_IDS_LOCATOR.size());
    }

    /**
     * is visible search feed icon
     */
    public boolean checkVisibilityOfSearchFeedIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SEARCH_FEED_ICON.isDisplayed());
    }

    /**
     * Проставляем или убираем google add manager
     */
    public void setRemoveGoogleAddManager(boolean isEnabled) {
        if(isEnabled && !ICON_GOOGLE_ADD_MANAGER_ENABLED.isDisplayed()) {
            ICON_GOOGLE_ADD_MANAGER_DISABLED.click();
            ICON_GOOGLE_ADD_MANAGER_ENABLED.shouldBe(visible);
        } else if (!isEnabled && !ICON_GOOGLE_ADD_MANAGER_DISABLED.isDisplayed()) {
            ICON_GOOGLE_ADD_MANAGER_ENABLED.click();
            ICON_GOOGLE_ADD_MANAGER_DISABLED.shouldBe(visible);
        }
        waitForAjax();
        checkErrors();
        log.info("The state of google add manager was changed - " + isEnabled);
    }

    public void rotateValidFormats(boolean state) {
        SHOW_VALID_FORMATS.setSelected(state);
    }

    public void saveImageBlock() {
        SAVE_IMAGE_BLOCK.click();
        waitForAjax();
        checkErrors();
    }

    public void loadImageCloudinaryInline(String imageName, CabTeasers.OwnershipOfMedia file) {
        if(isNeedToEditImage) {
            String image = imageName == null ?
                    LINK_TO_RESOURCES_IMAGES + "Stonehenge.jpg" :
                    LINK_TO_RESOURCES_IMAGES + imageName;
            CLOUDINARY_IMAGE_FIELD.scrollTo().sendKeys(image);

            $("[name=\"uploadImageSource\"]").selectRadio(file.getValue());
            $("[class*=\"saveImageUploadSource").click();

            waitForAjax();

            helpersInit.getBaseHelper().getTextAndWriteLog(CLOUDINARY_IMAGE_INPUT.val());
        }
    }

    public String getUrlParams() {
       return URL_PARAMS_FIELD.getOwnText();
    }

    public void setNewPrice(int priceValue) {
        clearAndSetValue(MASS_NEW_PRICE_VALUE_INPUT, String.valueOf(priceValue));
    }
}
