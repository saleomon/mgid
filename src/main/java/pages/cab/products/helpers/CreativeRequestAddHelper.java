package pages.cab.products.helpers;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import core.helpers.BaseHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static libs.hikari.utils.UtilsHelper.randomNumbersInt;
import static pages.cab.products.locators.CreativeRequestAddLocators.*;
import static pages.cab.products.locators.CreativeRequestsListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CreativeRequestAddHelper {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final boolean isSetSomeFields = false;

    public CreativeRequestAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Select website
     */
    public String selectWebsite(String urlWebSiteVal) {
        if (isSetSomeFields && urlWebSiteVal == null) {
            return null;
        } else if (urlWebSiteVal != null) {
            helpersInit.getBaseHelper().selectCustomValue(WEBSITE_SELECT, urlWebSiteVal);
        } else {
            helpersInit.getBaseHelper().selectRandomValue(WEBSITE_SELECT);
        }
        return helpersInit.getBaseHelper().getTextAndWriteLog(WEBSITE_SELECT.getSelectedText());
    }

    /**
     * Fill 'Link for the ads' input
     */
    public void fillLinkForAds(String linkForAds) {
        String link;
        if (isSetSomeFields && linkForAds == null) {
            return;
        } else if (linkForAds != null) {
            link = "https://" + linkForAds;
        } else {
            link = "https://" + BaseHelper.getRandomWord(5) + "-random-site-name.com";
        }
        clearAndSetValue(LINK_FOR_ADS_INPUT, link);
        helpersInit.getBaseHelper().getTextAndWriteLog(link);
    }

    /**
     * Fill 'Preview link' input
     */
    public String fillPreviewLink(String previewLinkVal) {
        String previewLink;
        if (isSetSomeFields && previewLinkVal == null) {
            return null;
        } else
            previewLink = Objects.requireNonNullElseGet(previewLinkVal, () -> "random Preview Link " + getRandomWord(5));
        clearAndSetValue(PREVIEW_LINK_INPUT, previewLink);
        return helpersInit.getBaseHelper().getTextAndWriteLog(previewLink);
    }

    /**
     * Fill 'Legal approve' field
     */
    public String fillLegalApprove(String legalApproveVal) {
        String legalApprove;
        if (isSetSomeFields && legalApproveVal == null) {
            return null;
        } else
            legalApprove = Objects.requireNonNullElseGet(legalApproveVal, () -> "random legalApprove " + getRandomWord(5));
        clearAndSetValue(LEGAL_APPROVE_INPUT, legalApprove);
        return helpersInit.getBaseHelper().getTextAndWriteLog(legalApprove);
    }

    /**
     * Fill 'Targeted audience' field
     */
    public String fillTargetedAudience(String targetedAudienceVal) {
        String targetedAudience;
        if (isSetSomeFields && targetedAudienceVal == null) {
            return null;
        } else
            targetedAudience = Objects.requireNonNullElseGet(targetedAudienceVal, () -> "random Targeted Audience " + getRandomWord(5));
        clearAndSetValue(TARGETED_AUDIENCE_FIELD, targetedAudience);
        return helpersInit.getBaseHelper().getTextAndWriteLog(targetedAudience);
    }

    /**
     * Fill 'Offer review' field
     */
    public String fillOfferReview(String offerReviewVal) {
        String offerReview;
        if (isSetSomeFields && offerReviewVal == null) {
            return null;
        } else
            offerReview = Objects.requireNonNullElseGet(offerReviewVal, () -> "random Offer Review" + getRandomWord(5));
        clearAndSetValue(OFFER_REVIEW_FIELD, offerReview);
        return helpersInit.getBaseHelper().getTextAndWriteLog(offerReview);
    }

    /**
     * Fill 'Comment for teasermakers' field
     */
    public String fillCommentForTeasermakers(String commentForTeaserMakersVal) {
        String commentForTeaserMakers;
        if (isSetSomeFields && commentForTeaserMakersVal == null) {
            return null;
        } else
            commentForTeaserMakers = Objects.requireNonNullElseGet(commentForTeaserMakersVal, () -> "random Comment For TeaserMakers " + getRandomWord(5));
        clearAndSetValue(COMMENT_FOR_TEASERMAKERS_FIELD, commentForTeaserMakers);
        return helpersInit.getBaseHelper().getTextAndWriteLog(commentForTeaserMakers);
    }

    /**
     * Fill the 'Publishers details' field
     */
    public String fillPublishersDetails(String publisherDetailsVal) {
        String publisherDetails;
        if (isSetSomeFields && publisherDetailsVal == null) {
            return null;
        } else
            publisherDetails = Objects.requireNonNullElseGet(publisherDetailsVal, () -> "random Publisher Details " + getRandomWord(5));
        clearAndSetValue(PUBLISHERS_DETAILS_FIELD, publisherDetails);
        return helpersInit.getBaseHelper().getTextAndWriteLog(publisherDetails);
    }

    /**
     * Fill 'Comment for moderators' field
     */
    public String fillCommentForModerators(String commentForModeratorsVal) {
        String commentForModerators;
        if (isSetSomeFields && commentForModeratorsVal == null) {
            return null;
        } else
            commentForModerators = Objects.requireNonNullElseGet(commentForModeratorsVal, () -> "random Comment For Moderators " + getRandomWord(5));
        clearAndSetValue(COMMENT_FOR_MODERATORS_FIELD, commentForModerators);
        return helpersInit.getBaseHelper().getTextAndWriteLog(commentForModerators);
    }

    /**
     * Fill 'Teasers amount' field
     */
    public String fillTeasersAmount(String amountVal) {
        String amount;
        if (isSetSomeFields && amountVal == null) {
            return null;
        } else if (amountVal != null) {
            amount = amountVal;

        } else {
            amount = helpersInit.getBaseHelper().randomNumbersString(1);
        }
        clearAndSetValue(TEASERS_AMOUNT_FIELD, amount);
        return helpersInit.getBaseHelper().getTextAndWriteLog(amount);
    }

    /**
     * Fill 'CPC for all regions, US cents' field
     */
    public String fillPrice(String priceVal, boolean isEditRequest) {
        String price;
        SelenideElement cpcInput = isEditRequest ? EDIT_REQUEST_CPC_FOR_ALL_REGIONS_FIELD : CPC_FOR_ALL_REGIONS_FIELD;
        if (isSetSomeFields && priceVal == null) {
            return null;
        } else
            price = Objects.requireNonNullElseGet(priceVal, () -> helpersInit.getBaseHelper().randomNumbersString(2) + ".3");
        clearAndSetValue(cpcInput, price);
        return helpersInit.getBaseHelper().getTextAndWriteLog(price);
    }

    /**
     * Choose 'Priority' - create request
     */
    public String choosePriority(String priorityValue, boolean isEditRequest) {
        SelenideElement prioritySelect = isEditRequest ? EDIT_REQUEST_PRIORITY_SELECT : PRIORITY_SELECT;
        if (isSetSomeFields && priorityValue == null) {
            return null;
        } else if (priorityValue != null) {
            helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(prioritySelect, priorityValue));
        } else {
            helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomText(prioritySelect));
        }
        return prioritySelect.getSelectedText();
    }

    /**
     * Notify manager when the request is closed
     */
    public void switchNotifyManagerCheckbox(Boolean notifyManagerCheckboxState) {
        if (isSetSomeFields && notifyManagerCheckboxState == null) {
        } else if (notifyManagerCheckboxState != null) {
            NOTIFY_MANAGER_CHECKBOX.setSelected(notifyManagerCheckboxState);
            helpersInit.getBaseHelper().getTextAndWriteLog(NOTIFY_MANAGER_CHECKBOX.isSelected());
        } else {
            NOTIFY_MANAGER_CHECKBOX.setSelected(!NOTIFY_MANAGER_CHECKBOX.isSelected());
            helpersInit.getBaseHelper().getTextAndWriteLog(NOTIFY_MANAGER_CHECKBOX.isSelected());
        }
    }

    /**
     * Notify manager when the request is closed
     */
    public void selectNotifyNativeEditor() {
        helpersInit.getBaseHelper().selectRandomValue(NOTIFY_NATIVE_EDITOR_SELECT);
    }

    /**
     * Submit form
     */
    public void submitForm() {
        SUBMIT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Get requestId
     */
    public String readRequestId() {
        if(REQUEST_ID.isDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(REQUEST_ID.shouldBe(visible).text().trim());
        }
        return "";
    }

    /**
     * Click 'Edit request' icon
     */
    public void clickEditRequestIcon() {
        EDIT_REQUEST_ICON.shouldBe(visible).click();
        waitForAjax();
    }

    /**
     * Submit 'Edit request' form (popup)
     */
    public void submitRequest() {
        SUBMIT_REQUEST_BUTTON.click();
        waitForAjax();
        checkErrors();
        EDIT_REQUEST_POPUP.shouldBe(hidden, ofSeconds(12));
    }

    public void loadImageForRequest(int amountOfImages) {
        String[] imageContainer = new String[]{"Stonehenge.jpg", "mount.jpg", "dracaena-cinnabari.jpg"};
        int randomIndex;
        for(int i = 0; i < amountOfImages; i++) {
            randomIndex = randomNumbersInt(imageContainer.length);

            String image = LINK_TO_RESOURCES_IMAGES + imageContainer[randomIndex];

            IMAGE_INPUT.scrollTo().sendKeys(image);
            ADD_IMAGES_EDIT_POPUP_BUTTON.click();
            waitForAjax();
            checkErrors();
        }
    }

    public Map<String, String> fillImageInformation(int amountOfImages, String...text) {
        Map<String, String> imageInformation = new HashMap<>();
        String comment;
        int sizeOptions;


        for(int i = 0; i < amountOfImages; i++) {
            comment = text.length > 0 ? text[i] : getRandomWord(20);
            sizeOptions = randomNumbersInt($("[id='image-link_" + (i+1) + "']").shouldBe(visible).$$("option").size());

            clearAndSetValue($("[id='image-comment_" + (i+1) + "']"), comment);
            $("[id='image-link_" + (i+1) + "']").selectOption(sizeOptions);

            imageInformation.put("comment_" +  (i+1), comment);
            imageInformation.put("link_" + (i+1), $("[id='image-link_" + (i+1) + "']").$$("option").get(sizeOptions).val());
            log.info("comment " + (i+1) + " - " + comment);
            log.info("link " + (i+1) + " - " + $("[id='image-link_" + (i+1) + "']").$$("option").get(sizeOptions).val());
            waitForAjax();
            checkErrors();
        }
        return imageInformation;
    }

    /**
     * set CPC for copy teaser all campaigns
     */
    public void setCpcForCopyAllCampaigns(String cpc) {
        COPY_CPC_FOR_ALL_CAMPAIGNS_INPUT.shouldBe(visible).sendKeys(cpc);
    }

    /**
     * set ids of campaigns
     */
    public void fillCampaignIds(int... campaignIds) {
        for (int i = 0; i < campaignIds.length; i++) {
            COPY_CAMPAIGN_IDS_INPUT.get(i).shouldBe(visible).sendKeys(String.valueOf(campaignIds[i]));
            for (int j = i; j < campaignIds.length-1; j++) ADD_CAMPAIGN_BUTTON.click();
        }
    }

    /**
     * click execute button for copy teaser into campaigns
     */
    public void clickCopyTeaserToCampaigns() {
        log.info("click execute button");
        COPY_BUTTON.click();
        checkErrors();
    }
}