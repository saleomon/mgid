package pages.cab.products.helpers;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.logic.CabTargets;
import core.helpers.BaseHelper;

import java.util.Objects;

import static com.codeborne.selenide.Condition.disabled;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TargetAddlocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class TargetAddHelper {
    private HelpersInit helpersInit;
    private Logger log;

    public TargetAddHelper setSomeFields(boolean setSomeFields) {
        isSetSomeFields = setSomeFields;
        return this;
    }

    private boolean isSetSomeFields = false;

    public TargetAddHelper(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    /**
     * Set target name
     */
    public String setTargetName(String targetName, String targetType) {
        if (TARGET_NAME_INPUT.is(disabled)) {
            log.info("TARGET_NAME_INPUT -> is disabled");
            return null;
        }
        String name;
        if (isSetSomeFields && targetName == null) {
            return null;
        } else {
            name = targetName != null ? targetName : "Autotest " + BaseHelper.getRandomWord(5) + ", type " + targetType;
        }
        clearAndSetValue(TARGET_NAME_INPUT, name);
        return helpersInit.getBaseHelper().getTextAndWriteLog(name);
    }

    /**
     * Set target description
     */
    public String setTargetDescription(String targetDescription, String targetType) {
        String description;
        if (isSetSomeFields && targetDescription == null) {
            return null;
        } else {
            description = targetDescription != null ? targetDescription : "Default description, target type - " + targetType;
        }
        clearAndSetValue(TARGET_DESCRIPTION_INPUT, description);
        return helpersInit.getBaseHelper().getTextAndWriteLog(description);
    }

    /**
     * Submit target
     */
    public void submitTarget() {
        SUBMIT_TARGET_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * Choose URL type
     */
    private String chooseUrlType(String customUrlType) {
        String urlType;
        if (isSetSomeFields && customUrlType == null) {
            return null;
        } else if (customUrlType == null) {
            CabTargets.UrlType[] urlTypes = CabTargets.UrlType.values();
            urlType = urlTypes[randomNumbersInt(urlTypes.length)].getUrlType();
        } else {
            urlType = customUrlType;
        }
        URL_TARGET_SELECT.shouldBe(visible, ofSeconds(10)).selectOptionByValue(urlType);
        return helpersInit.getBaseHelper().getTextAndWriteLog(urlType);
    }

    /**
     * Set settings url target
     */
    public String[] setPageLinkTarget(String[] targetPageLink) {
        String[] urlType = new String[2];
        if (isSetSomeFields && targetPageLink == null) {
            return null;
        } else if (targetPageLink != null) {
            urlType = targetPageLink;
            urlType[0] = chooseUrlType(urlType[0]);
            clearAndSetValue(URL_TARGET_INPUT, urlType[1]);
        } else {
            urlType[0] = chooseUrlType(null);
            urlType[1] = chooseUrlTarget(urlType[0]);
            clearAndSetValue(URL_TARGET_INPUT, urlType[1]);
        }
        return urlType;
    }

    /**
     * Choose URL target type
     */
    public String chooseUrlTarget(String urlType) {
        String link;
        CabTargets.UrlType type = CabTargets.UrlType.getUrlType(urlType);
        link = switch (type) {
            case START_FROM -> "http://test_auto";
            case CONTAINS -> "temp_auto";
            case ENDS_WITH -> "com.ua";
        };
        return link;
    }

    /**
     * Choose random targets type
     */
    public String chooseTargetType(String customType) {
        CabTargets.TargetType[] targetsType = CabTargets.TargetType.values();
        String type;
        if (isSetSomeFields && customType == null) {
            return null;
        } else
            type = customType != null ? customType : targetsType[randomNumbersInt(targetsType.length)].getTargetType();
        $("#" + type).click();
        return helpersInit.getBaseHelper().getTextAndWriteLog(type);
    }

    /**
     * Selection of conversion category
     */
    public String selectConversionCategory(String targetConversionCategory) {
        if (isSetSomeFields && targetConversionCategory == null) {
            return null;
        } else if (targetConversionCategory != null && !targetConversionCategory.equals("selected")) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomValue(CONVERSION_CATEGORY_SELECT, targetConversionCategory));
        } else if (targetConversionCategory != null && targetConversionCategory.equals("selected")) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(CONVERSION_CATEGORY_SELECT, "Conversion category"));
        } else {
            return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(CONVERSION_CATEGORY_SELECT));
        }
    }

    /**
     * Set target settings 'Website visitors' tab
     */
    public String[] setWebsiteVisitorTarget(String[] customSettings) {
        String[] domainVisitorsSettings = new String[2];
        if (isSetSomeFields && customSettings == null) {
            return null;
        } else if (customSettings != null) {
            domainVisitorsSettings = customSettings;
        } else {
            domainVisitorsSettings[0] = helpersInit.getBaseHelper().randomNumbersString(10);
            domainVisitorsSettings[1] = BaseHelper.getRandomWord(5) + ".com.ua";
        }
        clearAndSetValue(VISITED_TARGET_VALUE_INPUT, domainVisitorsSettings[0]);
        clearAndSetValue(VISITED_TARGET_DOMAIN_INPUT, domainVisitorsSettings[1]);
        helpersInit.getBaseHelper().getTextAndWriteLog(domainVisitorsSettings[0]);
        helpersInit.getBaseHelper().getTextAndWriteLog(domainVisitorsSettings[1]);
        return domainVisitorsSettings;
    }

    /**
     * Set target settings 'Event' tab
     */
    public String setEventTarget(String targetIdentifier) {
        String eventTargetId;
        if (isSetSomeFields && targetIdentifier == null) {
            return null;
        } else {
            eventTargetId = targetIdentifier != null ? targetIdentifier : "click" + BaseHelper.getRandomWord(5);
        }
        clearAndSetValue(EVENT_TARGET_INPUT, eventTargetId);
        return helpersInit.getBaseHelper().getTextAndWriteLog(eventTargetId);
    }

    /**
     * Set target settings 'Regular expression' tab
     */
    public String setRegExpTarget(String targetRegExp) {
        String regExp;
        if (isSetSomeFields && targetRegExp == null) {
            return null;
        } else {
            regExp = targetRegExp != null ? targetRegExp : "[" + BaseHelper.getRandomWord(5) + "]";
        }
        clearAndSetValue(REGEXP_TARGET_INPUT, regExp);
        return helpersInit.getBaseHelper().getTextAndWriteLog(regExp);
    }

    /**
     * Set target settings 'Postback' tab
     */
    public String setPostbackTarget(String targetPostbackEventName) {
        String val;
        if (isSetSomeFields && targetPostbackEventName == null) {
            return null;
        } else {
            val = targetPostbackEventName != null ? targetPostbackEventName : "buy" + BaseHelper.getRandomWord(3);
        }
        clearAndSetValue(POSTBACK_TARGET_INPUT, val);
        return helpersInit.getBaseHelper().getTextAndWriteLog(val);
    }

    /**
     * Check target settings 'Page link' tab
     */
    public boolean checkPageLinkTarget(String[] targetURL) {
        log.info("targetURL[0] - " + targetURL[0]);
        return helpersInit.getBaseHelper().checkSelectedCurrentValInSelectListForCabJs(URL_TARGET_SELECT, targetURL[0]) &&
                helpersInit.getBaseHelper().checkDatasetEquals(chooseUrlTarget(targetURL[0]), Objects.requireNonNull(URL_TARGET_INPUT.val()));
    }

    /**
     * Check target settings 'Website visitors' tab
     */
    public boolean checkWebsiteVisitorTarget(String[] mass) {
        return helpersInit.getBaseHelper().checkDatasetEquals(mass[0], Objects.requireNonNull(VISITED_TARGET_VALUE_INPUT.val())) &&
                helpersInit.getBaseHelper().checkDatasetEquals(mass[1], Objects.requireNonNull(VISITED_TARGET_DOMAIN_INPUT.val()));
    }

    /**
     * Check target settings 'Event' tab
     */
    public boolean checkEventTarget(String targetIdentifier) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetIdentifier, Objects.requireNonNull(EVENT_TARGET_INPUT.val()));
    }

    /**
     * Check target settings 'Regular expression' tab
     */
    public boolean checkRegExpTarget(String targetRegExp) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetRegExp, Objects.requireNonNull(REGEXP_TARGET_INPUT.val()));
    }

    /**
     * Check target settings 'Postback' tab
     */
    public boolean checkPostbackTarget(String targetPostbackEventName) {
        return helpersInit.getBaseHelper().checkDatasetEquals(targetPostbackEventName, Objects.requireNonNull(POSTBACK_TARGET_INPUT.val()));
    }
}