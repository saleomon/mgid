package pages.cab.products.helpers;

import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.products.locators.ClientListLocators.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class ClientListHelper {
    private final HelpersInit helpersInit;

    public ClientListHelper(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * choose random value in 'Currency' filter
     */
    public String chooseFilterCurrency() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomText(FILTER_CURRENCY_SELECT.shouldBe(visible)));
    }

    /**
     * choose random value in 'Currency' filter
     */
    public String filterClientsByLowPriority() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectRandomValue(FILTER_LOW_PRIORITY_SELECT.shouldBe(visible)));
    }

    /**
     * click 'Filter' button
     */
    public void clickFilterButton() {
        FILTER_SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * get currency type
     */
    public String getCurrencyType() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MG_WALLET_LABEL
                .text()
                .replaceAll("[\\d. ]", ""));
    }

    /**
     * check visibility of campaign
     */
    public boolean checkPresenceCampaignAtListInterface(String name) {
        return $("[title='" + name + "']").exists();
    }

    /**
     * get clientId
     */
    public String getClientId() {
        if (CLIENT_ID_LABEL.isDisplayed()) {
            return helpersInit.getBaseHelper().getTextAndWriteLog(CLIENT_ID_LABEL.text());
        }
        return null;
    }

    /**
     * check client login in list interface
     */
    public boolean checkClientLoginInList(String login) {
        return helpersInit.getBaseHelper().checkDatasetEquals(LOGIN_CLIENT_LABEL.text(), login);
    }

    /**
     * check client email in list interface
     */
    public boolean checkClientEmailInList(String email) {
        return helpersInit.getBaseHelper().checkDatasetEquals(EMAIL_CLIENT_LABEL.text(), email);
    }
}
