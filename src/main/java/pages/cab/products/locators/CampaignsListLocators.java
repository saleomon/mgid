package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CampaignsListLocators {
    public static final SelenideElement CONTEXT_TARGETING_FIELD = $x("//*[@class='campaigns-targeting']//span[@class='key' and text()='Contexts targeting:']");
    public static final SelenideElement SENTIMENTS_TARGETING_FIELD = $x("//*[@class='campaigns-targeting']//span[@class='key' and text()='Sentiments targeting:']");
    public static final SelenideElement CAMPAIGN_NAME_TEXT = $("[class=value][title]");
    public static final SelenideElement CAMPAIGN_ID_LOCATOR = $x("//span[contains(@id, 'rk_id_')]");
    public static final String CAMPAIGN_ID_FIELD = "#rk_id_%s_%s";
    public static final ElementsCollection CAMPAIGN_ID_LOCATORS = $$x("//span[contains(@id, 'rk_id_')]");
    public static final SelenideElement FILTER_WIDGET_OFF_ICON = $("[id*=filtersoff_]");
    public static final SelenideElement FILTER_WIDGET_CLEAR_POPUP_ACCESS_BUTTON = $x(".//div[contains(@class, 'ui-draggable')]//span[contains(text(), 'Yes')]");
    public static final ElementsCollection MANAGER_LABEL = $$("[id^='c_kurator_']");
    public static final SelenideElement APAC_ICON = $x(".//*[contains(@src, 'apac_icon.svg')]");
    public static final SelenideElement SEARCH_FEED_ICON = $("img[src*='type_search_feed.png']");
    public static final SelenideElement CAMPAIGN_TYPE_POPUP = $("[src*='campaign_type.png']");
    public static final SelenideElement CAMPAIGN_CATEGORY_SELECT = $("[id='cat_sel_dropdown']");
    public static final SelenideElement ACCEPT_CHANGE_CAMPAIGN_BUTTON = $("[id='acceptButton']");


    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
    public static final SelenideElement FILTER_FORM = $("#FormId");

    //Actions
    public static final SelenideElement FILTER_EXCEPT_ICON = $("a[id^=filter_2_]");
    public static final SelenideElement FILTER_ONLY_ICON = $("a[id^=filter_1_]");
    public static final SelenideElement SCHEDULE_ICON = $(".time_targeting");
    public static final SelenideElement FILTER_STOP_ICON = $("[id^=filtersoff_]");
    public static final SelenideElement UNBLOCK_CAMPAIGN_ICON = $("img[src*='forbidden.png']");

    public static final String MOVE_TO_THE_TRASH_BIN_ICON = "#trash-ico-%s";
    public static final SelenideElement RESTORE_CAMPAIGN_ICON = $("[src*='restore.gif']");
    public static final SelenideElement PAUSE_CAMPAIGN_ICON = $x("//img[contains(@src, 'play.png')]");
    public static final SelenideElement LOADING_CAMPAIGN_ICON = $x("//img[contains(@src, 'loading.gif')]");
    public static final SelenideElement START_CAMPAIGN_ICON = $x("//img[contains(@src, 'forbidden.png')]");
    public static final SelenideElement COPYRIGHT_ICON = $("img[src*='copyright.png']");
    public static final SelenideElement ROTATE_UA_DISABLED = $("img[src*='can-rotate-ua-grey.svg']");
    public static final SelenideElement ROTATE_UA_ICON = $("img[src*='can-rotate-ua-']");

    //Clone popup
    public static final SelenideElement CLONE_ICON = $(".campaign-copy-link");
    public static final SelenideElement CLONE_FORM = $("#campaign-copy-dialog");
    public static final SelenideElement CLONE_NAME_INPUT = $("#newName");
    public static final SelenideElement CLONE_COPY_CATEGORY_CHECKBOX = $("#copyCategory");
    public static final SelenideElement CLONE_COPY_LANGUAGE_CHECKBOX = $("#copyLanguage");
    public static final SelenideElement CLONE_COPY_SHOW_NAME_CHECKBOX = $("#copyShowName");
    public static final SelenideElement CLONE_COPY_IS_WHITE_CHECKBOX = $("#copyIsWhite");
    public static final SelenideElement CLONE_COPY_SCHEDULE_CHECKBOX = $("#copySchedule");
    public static final SelenideElement CLONE_COPY_AD_LIFETIME_CHECKBOX = $("#copyAdLivetime");
    public static final SelenideElement CLONE_COPY_LIMITS_CHECKBOX = $("#copyLimits");
    public static final SelenideElement CLONE_COPY_GEO_TARGETING_CHECKBOX = $("#copyGeoTargeting");
    public static final SelenideElement CLONE_COPY_BROWSER_TARGETING_CHECKBOX = $("#copyBrowsersTargeting");
    public static final SelenideElement CLONE_COPY_OS_TARGETING_CHECKBOX = $("#copyOSTargeting");
    public static final SelenideElement CLONE_COPY_LANGUAGE_TARGETING_CHECKBOX = $("#copyLanguagesTargeting");
    public static final SelenideElement CLONE_COPY_CONNECTION_TYPE_TARGETING_CHECKBOX = $("#copyTrafficTypeTargeting");
    public static final SelenideElement CLONE_COPY_SELECTIVE_BIDDING_CHECKBOX = $("#copyQualityFactors");
    public static final SelenideElement CLONE_COPY_CONVERSION_SETTINGS_CHECKBOX = $("#copyConversionSettings");
    public static final SelenideElement CLONE_COPY_FILTERS_CHECKBOX = $("#copyFilters");
    public static final SelenideElement CLONE_COPY_UTM_AND_GA_TAG_CHECKBOX = $("#copyUtm");
    public static final SelenideElement CLONE_USE_ONLY_SAME_LANGUAGE_WIDGETS_CHECKBOX = $("#copyUseOnlySameLanguageWidgets");
    public static final SelenideElement CLONE_MARKETING_OBJECTIVE_CHECKBOX = $("#copyMarketingObjective");
    public static final SelenideElement CLONE_VIEWABILITY_CHECKBOX = $("#copyViewability");
    public static final SelenideElement CLONE_SAVE_BUTTON = $(".button-save");
    public static final SelenideElement CLONE_LANGUAGE_SELECT = $("#newLanguage");
    public static final SelenideElement CLONE_CATEGORY_SELECT = $("#cat_copy_dropdown");

    // other locators
    public static final SelenideElement READ_POPUP_BUTTON = $x("//*[text()='Read']");
    public static final String CAMPAIGN_SUBTYPE = "//td[contains(.,'%s')]";
    public static final SelenideElement REJECTED_REASON_SELECT = $(".rejected_reason_select");
    public static final SelenideElement PAUSE_CAMPAIGN_BUTTON = $x("//*[text()='Pause campaign']");

    //compliant
    public static final SelenideElement IAP_COMPLIANT_POPUP_TEXT = $("[id='js-confirm-dialog'] h6");
    public static final SelenideElement COMPLIANT_ALL_COUNTRY_ICON = $("img[src*='all_multi_geo_compliant.png']");
    public static final SelenideElement COMPLIANT_PART_COUNTRY_ICON = $("img[src*='particular_multi_geo_compliant.png']");
    public static final SelenideElement COMPLIANT_ICON_POPUP = $("[id*='compliantFlag']");
    public static final ElementsCollection ALL_COMPLIANT_CHECKBOXES = $$("[class='countriesCompliant']");
    public static final SelenideElement SUBMIT_POPUP_BUTTON = $("[class*='saveCompliantGeo'] [class='ui-button-text']");
    public static final SelenideElement ROMANIA_ICON = $("img[src*='ro.png']");
    public static final SelenideElement ITALY_ICON = $("img[src*='it.png']");
    public static final SelenideElement SPAIN_ICON = $("img[src*='es.png']");
    public static final SelenideElement CZECH_ICON = $("img[src*='cz.png']");
    public static final SelenideElement POLAND_REG_ICON = $("img[src*='pl.png']");
    public static final SelenideElement COMPLIANT_FILTER = $("[id='compliant']");
    public static final SelenideElement COMPLIANT_POPUP_TEXT = $("[id='js-confirm-dialog'] div");

    //pagination
    public static final SelenideElement TOTAL_AMOUNT_OF_DISPLAYED_ENTITIES = $("[class='paginator'] [class*='right']");

}
