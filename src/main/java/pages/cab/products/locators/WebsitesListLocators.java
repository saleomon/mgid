package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WebsitesListLocators {
    //info keys
    public static final SelenideElement SITE_ID_LOCATOR = $("[id*='s_id_']");
    public static final SelenideElement DOMAIN_FIELD = $("[id*='domain_']");
    public static final ElementsCollection MANAGER_LABEL = $$("[id^='c_kurator_']");

    //filter
    public static final SelenideElement DOMAIN_FILTER_INPUT = $("[id='domain']");
    public static final SelenideElement FILTER_BUTTON = $("[id='btnsubmit']");
}
