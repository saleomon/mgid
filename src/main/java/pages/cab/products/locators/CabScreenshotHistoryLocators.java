package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CabScreenshotHistoryLocators {
    public static final SelenideElement CHECKBOX_SCREENSHOT = $("[class='screenshot-history-mass']");
    public static final SelenideElement SEARCH_OFFERS_FIELD = $("[class*='product-offer-search']");
    public static final SelenideElement SEARCH_OFFERS_SUBMIT_BUTTON = $("[id='product-offer-submit']");
    public static final SelenideElement AUTOCOMPLETE_ELEMENT = $("[class*='ui-corner-all']");
    public static final SelenideElement ADVERT_NAME_ICON = $("[class='certifieOffer']");
    public static final ElementsCollection PRODUCTS = $$("[class*='offerName']");

    //certifate block
    public static final SelenideElement UPLOAD_POPUP_BUTTON = $("[class='screen-certificate-upload']");
    public static final SelenideElement POPUP_UPLOAD_CERTIFICATE_BLOCK = $("[id='screenshot-certificate-upload-popup']");
    public static final ElementsCollection UPLOADED_CERTIFICATES = $$("[id='uploaded-files'] a");
    public static final SelenideElement DELETE_CERTIFICATE_BUTTON = $("[id*='remove']");
    public static final SelenideElement DOWNLOAD_CERTIFICATE_BUTTON = $("[class='fa fa-download']");
    public static final SelenideElement VERIFIED_ICON = $("[class*='copyrightVerification verificationStatus']");

}
