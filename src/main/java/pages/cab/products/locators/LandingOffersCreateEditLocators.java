package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class LandingOffersCreateEditLocators {
    public static final SelenideElement PRODUCT_NAME = $("[id='offerName']");
    public static final SelenideElement PAGE_LINK = $("[id='offerPageLink']");
    public static final SelenideElement OFFER_MANUFACTURER = $("[id='offerManufacturer']");
    public static final ElementsCollection GEO_REGIONS_MGID = $$("[class=\"tabs_content active\"] [id=\"subnet_0\"] [class=\"tier-list__item\"]");
    public static final SelenideElement ALL_ENABLED_REGIONS_WHITE_MGID = $("[id='content_enabledCountries'] [id='subnet_0'] [class='tierName active-all']");
    public static final SelenideElement PART_ENABLED_REGIONS_WHITE_MGID = $("[id='content_enabledCountries'] [id='subnet_0'] [class*='tierName active']");
    public static final SelenideElement ALL_ENABLED_REGIONS_WHITE_ADSKEEPER = $("[id='content_enabledCountries'] [id='subnet_2'] [class='tierName active-all']");
    public static final SelenideElement PART_ENABLED_REGIONS_WHITE_ADSKEEPER = $("[id='content_enabledCountries'] [id='subnet_2'] [class='tierName active']");
    public static final SelenideElement ALL_ENABLED_REGIONS_BLACK_MGID = $("[id=\"content_disabledCountries\"] [id=\"subnet_0\"] [class=\"tierName active-all\"]");
    public static final SelenideElement PART_ENABLED_REGIONS_BLACK_MGID = $("[id=\"content_disabledCountries\"] [id=\"subnet_0\"] [class=\"tierName active-all\"]");
    public static final SelenideElement PART_ENABLED_REGIONS_BLACK_ADSKEEPER = $("[id=\"content_disabledCountries\"] [id=\"subnet_2\"] [class*=\"tierName active\"]");
    public static final SelenideElement ALL_ENABLED_REGIONS_BLACK_ADSKEEPER = $("[id=\"content_disabledCountries\"] [id=\"subnet_2\"] [class=\"tierName active-all\"]");
    public static final ElementsCollection GEO_REGIONS_ADSKEEPER = $$("[class=\"tabs_content active\"] [id=\"subnet_2\"] [class=\"tier-list__item\"]");
    public static final SelenideElement WHITE_LIST_GEO_REGIONS = $("[id='list_enabledCountries']");
    public static final ElementsCollection ENABLED_COUNTRIES_WHITE_LIST_MGID = $$("[id=\"content_enabledCountries\"] [id=\"subnet_0\"] [class=\"countrySpan active\"]");
    public static final ElementsCollection ENABLED_COUNTRIES_BLACK_LIST_ADSKEEPER = $$("[id=\"content_disabledCountries\"] [id=\"subnet_2\"] [class=\"countrySpan active\"]");
    public static final ElementsCollection DISABLED_COUNTRIES_BLACK_LIST_ADSKEEPER = $$("[id=\"content_disabledCountries\"] [id=\"subnet_2\"] [class=\"countrySpan\"]");
    public static final ElementsCollection DISABLED_COUNTRIES_WHITE_LIST_MGID = $$("[id=\"content_enabledCountries\"] [id=\"subnet_0\"] [class=\"countrySpan\"]");
    public static final SelenideElement BLACK_LIST_GEO_REGIONS = $("[id='list_disabledCountries']");
    public static final SelenideElement SELECT_ALL_WHITE_GEO_MGID_CHECKBOX = $("[id='content_enabledCountries'] [id='subnet_0'] [name='selectAllTier']");
    public static final SelenideElement SELECT_ALL_BLACK_GEO_MGID_CHECKBOX = $("[id='content_disabledCountries'] [id='subnet_0'] [name='selectAllTier']");
    public static final SelenideElement SELECT_CASINO_AND_GAMBLING_BLACK_GEO_MGID_CHECKBOX = $("[id='content_disabledCountries'] [id='subnet_0'] [name='casinosAndGambling']");
    public static final SelenideElement SELECT_ALL_WHITE_GEO_ADSKEEPER_CHECKBOX = $("[id='content_enabledCountries'] [id='subnet_2'] [name='selectAllTier']");
    public static final SelenideElement SELECT_ALL_BLACK_GEO_ADSKEEPER_CHECKBOX = $("[id='content_disabledCountries'] [id='subnet_2'] [name='selectAllTier']");
    public static final SelenideElement SELECT_CASINO_AND_GAMBLING_BLACK_GEO_ADSKEEPER_CHECKBOX = $("[id='content_disabledCountries'] [id='subnet_2'] [name='casinosAndGambling']");
    public static final ElementsCollection ACTIVE_WHITE_GEO_ALL = $$("[id='content_enabledCountries'] [class=\"tierName active-all\"]");
    public static final ElementsCollection ACTIVE_WHITE_GEO_PART = $$("[id='list_enabledCountries'][class='availability active tabs_caption__item']");
    public static final SelenideElement ARROW_DOWN_TIER_1_ADSKEEPER = $("[id='subnet_2'] [name='Tier 1'] [class='arrow-down']");
    public static final SelenideElement SAVE_OFFER = $("[class*='saveOffer'] [class='ui-button-text']");
    public static final ElementsCollection MARKED_REGIONS_MGID = $$("[id='subnet_0'] [class='countrySpan active']");
    public static final SelenideElement UNITED_STATES_CDB_ACTIVE = $("[class='cbdStates active']");
    public static final SelenideElement ERROR_MESSAGE = $("[class*='error'] li");
    public static final ElementsCollection REMOVE_CERTIFICATE_ICON = $$("[id*='remove']");
    public static final String CERTIFICATE_NAME_LABEL = "//*[text()='%s']";
    public static final SelenideElement UPLOAD_CERTIFICATES_BUTTON = $("[id='fileUploader']");
    public static final SelenideElement DOWNLOAD_CERTIFICATE_BUTTON = $("[class*='fa-download']");
    public static final SelenideElement FILTER_BY_CERTIFICATE = $("[class='offersWithCertificateLink']");
    public static final ElementsCollection OFFERS_AMOUNT = $$("[class='tsr-land-offs__name']");
    public static final SelenideElement ADVERT_NAME_ICON = $("[class='from-show-name-offer']");
}
