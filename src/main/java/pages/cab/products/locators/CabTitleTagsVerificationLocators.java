package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CabTitleTagsVerificationLocators {
    ///actions
    public static final SelenideElement OK_ACTION_BUTTON = $("[onclick*='actionOk']");
    public static final SelenideElement NOT_OK_ACTION_BUTTON = $("[onclick*='actionNotOk']");
    public static final SelenideElement IGNORE_ACTION_BUTTON = $("[onclick*='actionIgnore']");
    public static final SelenideElement TAGS_FORM_SAVE_BUTTON = $("#btnTagsSave");
    public static final ElementsCollection TAGS_ELEMENTS = $$("#tagsDialog .tagsBlock [data-name]:not([style*='none'])[data-type=title]");



    //other
    public static final SelenideElement TOTAL_AMOUNT_OF_DISPLAYED_ROWS = $("[class='paginator'] [class*='right']");
    public static final SelenideElement EMPTY_TABLE_LABEL = $("[class*='empty']");
    public static final SelenideElement LINK_TO_TEASERS_WITH_SAME_TITLE_LOCATOR = $("[class*='teaser-quantity'] a");


    //filters
    public static final SelenideElement TITLE_INPUT = $("[id='title']");
    public static final SelenideElement FILTER_BUTTON = $("[id='btnsubmit']");
    public static final SelenideElement SELECT_LANGUAGE = $("[id='language']");
    public static final SelenideElement SELECT_TRANSLATION_LANGUAGE = $("[id='languageTranslation']");

}
