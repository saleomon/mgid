package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CampaignsSitesFiltersLocators {
    public static final SelenideElement ADD_WEBSITE_INPUT = $("#siteInput");
    public static final SelenideElement SUBMIT_BUTTON = $("#submitCheckbxs");
    public static final SelenideElement DOMAIN_FIELD = $("#tickersT");
}
