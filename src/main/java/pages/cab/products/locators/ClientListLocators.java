package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class ClientListLocators {

    //filters
    public static final SelenideElement FILTER_CURRENCY_SELECT = $("#client_currency");
    public static final SelenideElement FILTER_LOW_PRIORITY_SELECT = $("#lowPriority");
    public static final SelenideElement FILTER_BY_LOGIN_INPUT = $("#login");
    public static final SelenideElement FILTER_SUBMIT_BUTTON = $("#btnsubmit");

    //client labels settings
    public static final SelenideElement MG_WALLET_LABEL = $("[id^=mg_wallet_]");
    public static final SelenideElement CLIENT_ID_LABEL = $("[id^=c_id_]");
    public static final ElementsCollection CLIENT_IDS_LABEL = $$("[id^=c_id_]");
    public static final SelenideElement LOGIN_CLIENT_LABEL = $("[id*=c_login]");
    public static final SelenideElement EMAIL_CLIENT_LABEL = $("[id*=c_email] a");
    public static final SelenideElement PHONE_CLIENT_LABEL = $("[id*=c_phone]");
    public static final SelenideElement SKYPE_CLIENT_LABEL = $("span[id*=c_skype]");
    public static final SelenideElement TELEGRAM_CLIENT_LABEL = $("[id*=c_telegram]");
    public static final SelenideElement NAME_CLIENT_LABEL = $("[id*=c_name]");

    //checkboxes
    public static final SelenideElement CLOUDINARY_CHECKBOX = $("[id='use_cloudinary']");

    //actions
    public static final SelenideElement COPYRIGHT_ICON = $("img[src*='copyright.png']");
    public static final SelenideElement COPYRIGHT_WITH_LINK_ICON = $("img[src*='copyright.svg']");
    public static final SelenideElement LINK_COPYRIGHT_ICON = $("[class='campaign-row'] td>a");
    public static final SelenideElement ALLOW_ROTATE_RUSSIAN_DOMAINS = $("[id*='allowRussianDomains_']");
    public static final SelenideElement PAYMENT_TERMS_ICON = $("img[src*='payment-terms.png']");
    public static final SelenideElement PAYMENT_TERMS_POPUP_BUTTON = $("[id='paymentTermsFileUpload']");
    public static final SelenideElement SAVE_PAYMENT_TERMS_BUTTON = $("[aria-labelledby*='payment-terms-file-upload-popup'] button");
    public static final SelenideElement SAVE_PAYMENT_TERMS_BUTTON_POPUP = $x("//span[text()='Submit']");
    public static final SelenideElement PAYMENT_TERMS_NET_INPUT = $("[id='paymentTermsNet']");
    public static final SelenideElement PAYMENT_TERMS_FILE_NAME = $("[id='uploaded-files'] a");
    public static final SelenideElement PAYMENT_TERMS_FILE_UPLOAD = $("[id='fileUploader']");
    public static final SelenideElement PAYMENT_TERMS_FILE_REMOVE_BUTTON = $("[id*='remove']");
    public static final SelenideElement PAYMENT_TERMS_SELECT = $("[id='paymentTerms']");

}
