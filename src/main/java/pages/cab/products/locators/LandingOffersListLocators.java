package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class LandingOffersListLocators {
    public static final SelenideElement ADD_NEW_PRODUCT_BUTTON = $("[class='add-teaser-offer-btn']");
    public static final SelenideElement SEARCH_FIELD = $("[id='offerNameTplForSearch']");
    public static final SelenideElement SEARCH_BUTTON = $("[id='landing-offers-search-button']");
    public static final SelenideElement DELETE_BUTTON = $("[class='remove-teaser-offer-btn']");
    public static final SelenideElement CONFIRM_DELETION_BUTTON = $x("//*[text()='Yes']");
    public static final SelenideElement EDIT_PRODUCT_BUTTON = $("[class='edit-teaser-offer-btn']");
    public static final SelenideElement WHITE_LIST_GEO_MGID = $x("//*[@class='tsr-land-offs__logo-0']/..//*[@class='tsr-land-offs__enable']/*[@class='tier-name']");
    public static final SelenideElement WHITE_LIST_GEO_COUNTRIES_MGID = $x("//*[@class='tsr-land-offs__logo-0']/..//*[@class='tsr-land-offs__enable']/*[@class='tier-countries']");
    public static final SelenideElement BLACK_LIST_GEO_MGID = $x("//*[@class='tsr-land-offs__logo-0']/../*[@class='tsr-land-offs__blocked']/*[@class='tier-name']");
    public static final SelenideElement WHITE_LIST_GEO_ADSKEEPER = $x("//*[@class='tsr-land-offs__logo-2']/../*[@class='tsr-land-offs__enable']/*[@class='tier-name']");
    public static final SelenideElement BLACK_LIST_GEO_ADSKEEPER = $x("//*[@class='tsr-land-offs__logo-2']/../*[@class='tsr-land-offs__blocked']/*[@class='tier-name']");
    public static final SelenideElement BLACK_LIST_GEO_COUNTRIES_ADSKEEPER = $x("//*[@class='tsr-land-offs__logo-2']/..//*[@class='tsr-land-offs__blocked']/*[@class='tier-countries']");
    public static final SelenideElement SEARCH_RESULTS = $("[id='teaserLandOffersResult'] [class='tsr-land-offs__row']");
    public static final SelenideElement GO_TO_TEASERS_ICON = $("[title='Go to teasers']");

    ///////////////////////////////////////////  EDIT FORM LOCATOR //////////////////////////////////////
    public static final SelenideElement PRODUCT_NAME = $("[id='offerName']");

}
