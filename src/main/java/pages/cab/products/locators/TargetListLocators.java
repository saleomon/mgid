package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TargetListLocators {
    public static final SelenideElement TARGET_ADD_BUTTON = $("input[value='Add target']");
    public static final String TARGET_DELETE_ICON = "[onclick*='deleteTarget(%s']";
    public static final ElementsCollection TARGET_ID_LOCATORS = $$x(".//tr[@class='main_table']/td[@class='a-center'][1]");
    public static final String TARGET_ID_LOCATOR = ".//tr[td[text()='%s']]/td[1]";

}
