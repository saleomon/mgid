package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class GlobeLocators {
    public final static SelenideElement GEO_PRICE_INPUT = $(".text-geo-price-input");
    public final static SelenideElement SAVE_MASS_POC_BUTTON = $("#setMassPOC");
    public final static SelenideElement CHECK_ALL_CHECKBOX = $("[class*='branch'] [type='checkbox']");
}
