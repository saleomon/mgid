package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CreativeLocators {

    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");

    // Columns
    // Information
    public static final SelenideElement REQUEST_ID_FIELD = $x(".//div[span[@class='key' and text()='Request ID:']]/span[@class='value']");
    public static final SelenideElement PRIORITY_FIELD = $x(".//div[span[@class='key' and text()='Priority:']]/span[@class='value']");
    public static final ElementsCollection MANAGER_LABEL = $$x(".//div[span[text()='Manager']]/*[@class='value']");

    // Amount
    public static final SelenideElement AMOUNT_FIELD = $(".creative_give_add_tasks>label");
    public static final SelenideElement AMOUNT_TEASERS_IN_TASK_INPUT = $("[name='receive_tasks']");
    public static final SelenideElement ASSUME_TASK_BUTTON = $(".takeTask_Button");
    public static final SelenideElement TEASERS_AMOUNT_INPUT = $("[class='teasers_amount']");
    public static final SelenideElement TEASERS_TYPE_INPUT = $("[class='teasers_type']");
    public static final SelenideElement CREATE_TASK_BUTTON = $("[id='btnCreate'] [class='ui-button-text']");
    public static final String AD_TYPES_LABEL = "td.ad-type";

    // Creative
    public static final SelenideElement ADD_TEASER_ICON = $(".ghits-add");
    public static final SelenideElement AMOUNT_OF_DRAFTS = $("[class='tasks base'] a[href*='internal_drafts']");
    public static final SelenideElement DELETE_TASK_BUTTON = $("[id*='cancelTask']");
    public static final SelenideElement REJECT_REASON_INPUT = $("[id*='reject-reason']");
    public static final SelenideElement CANCEL_TASK_BUTTON = $("[id*='reject-submit']");
    public static final SelenideElement REJECTED_TASK_ICON = $("[src*='rejected_tasks.png']");

    //Give task
    public static final SelenideElement OPEN_MANAGE_TASK_POPUP = $("[class*='giveTask_Button']");
    public static final SelenideElement CONTAINER_SELECTED_TEASER_MAKERS = $("[class*='selected connected-list']");
    public static final ElementsCollection ENABLED_IMAGES = $$("[class='request-data__item active']");
    public static final SelenideElement APPLY_EDIT_REQUEST_BUTTON = $x("//*[text()='Apply']");
    public static final SelenideElement SAVE_GIVE_TASK_POPUP_BUTTON = $("[id='giveTask_OkButton']");
    public static final ElementsCollection COMMENTS_LABEL = $$("[class*='request-data__comment']");
    public static final ElementsCollection LINKS_LABEL = $$("[class*='request-data__link']");
}