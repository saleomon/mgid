package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CreativeRequestAddLocators {
    public static final SelenideElement WEBSITE_SELECT = $("#siteDomain");
    public static final SelenideElement LINK_FOR_ADS_INPUT = $("#creative_conditions-linkForAds");
    public static final SelenideElement PREVIEW_LINK_INPUT = $("#creative_conditions-previewLink");
    public static final SelenideElement LEGAL_APPROVE_INPUT = $("#creative_conditions-legalApprove");
    public static final SelenideElement TARGETED_AUDIENCE_FIELD = $("#creative_conditions-targetAudience");
    public static final SelenideElement OFFER_REVIEW_FIELD = $("#creative_conditions-offerReview");
    public static final SelenideElement COMMENT_FOR_TEASERMAKERS_FIELD = $("#creative_conditions-commentTeaserMakers");
    public static final SelenideElement PUBLISHERS_DETAILS_FIELD = $("#creative_conditions-publishersDetailsForTeasermakers");
    public static final SelenideElement COMMENT_FOR_MODERATORS_FIELD = $("#creative_conditions-commentModerators");
    public static final SelenideElement TEASERS_AMOUNT_FIELD = $("#amount_teasers");
    public static final SelenideElement CPC_FOR_ALL_REGIONS_FIELD = $("#all");
    public static final SelenideElement PRIORITY_SELECT = $("#priority");
    public static final SelenideElement NOTIFY_BRIEF_CHECKBOX = $("#sendMailWhenCloseAnalyst");
    public static final SelenideElement NOTIFY_MANAGER_CHECKBOX = $("#sendMailWhenCloseCreator");
    public static final SelenideElement NOTIFY_NATIVE_EDITOR_SELECT = $("[id='nativeEditor']");
    public static final SelenideElement SUBMIT_BUTTON = $("#submit");
    public static final SelenideElement ADD_COPY_CONDITIONS_BUTTON = $("[id='enable_copy_settings']");

    //copy settings popup
    public static final SelenideElement ADD_CAMPAIGN_BUTTON = $("img[src*='plus2.png']");
    public static final ElementsCollection COPY_CAMPAIGN_IDS_INPUT = $$("[id=copy_option_list] [name=campaign_id]");
    public static final SelenideElement COPY_BUTTON = $("#doCopyBtn");
    public static final SelenideElement COPY_CPC_FOR_ALL_CAMPAIGNS_INPUT = $("#cpc_for_all");
}