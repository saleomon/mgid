package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class AudienceAddLocators {
    // Audiences
    public static final SelenideElement AUDIENCE_NAME_INPUT = $("#name");
    public static final SelenideElement AUDIENCE_DESCRIPTION_INPUT = $("#description");
    public static final SelenideElement AUDIENCE_TARGETS_SELECT = $("[name*='targets'][name*='targetId']");
    public static final SelenideElement AUDIENCE_EXPIRE_SELECT = $("[name*='targets'][name*='expire']");
    public static final SelenideElement SUBMIT_AUDIENCE_BUTTON = $("#submit");
    public static final SelenideElement AUDIENCE_CONDITION_SELECT = $("[name*='targets'][name*='type']");
    public static final SelenideElement ADD_CONDITION_BUTTON = $("#add_button");
}
