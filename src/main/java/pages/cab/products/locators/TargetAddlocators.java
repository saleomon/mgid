package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class TargetAddlocators {
    public static final SelenideElement TARGET_NAME_INPUT = $("#name");
    public static final SelenideElement TARGET_DESCRIPTION_INPUT = $("#description");
    public static final SelenideElement SUBMIT_TARGET_BUTTON = $("#submit");
    public static final SelenideElement URL_TARGET_SELECT = $("[name*='url'][name*='[type]']");
    public static final SelenideElement URL_TARGET_INPUT = $("[name*='url'][name*='[value]']");
    public static final SelenideElement VISITED_TARGET_VALUE_INPUT = $("#visited-value");
    public static final SelenideElement VISITED_TARGET_DOMAIN_INPUT = $("#visited-domain");
    public static final SelenideElement EVENT_TARGET_INPUT = $("#event-identificator");
    public static final SelenideElement REGEXP_TARGET_INPUT = $("#regexp_content #value");
    public static final SelenideElement POSTBACK_TARGET_INPUT = $("#postback-value");
    public static final SelenideElement CONVERSION_CATEGORY_SELECT = $("[id = 'categoryId']");
}
