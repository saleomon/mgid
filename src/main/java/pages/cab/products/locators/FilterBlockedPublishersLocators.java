package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class FilterBlockedPublishersLocators {

    public static final SelenideElement CHECKED_WIDGET_COUNT = $("#checked-count");
    public static final SelenideElement IS_CHECKED_CLASS = $(".is-checked");
    public static final SelenideElement IS_UNCHECKED_CLASS = $(".is-unchecked");
    public static final String WIDGETS_ROW = "#contentArea li .row-content";
    public static final SelenideElement CHOOSE_FILE_BUTTON = $("#sources_list");
    public static final SelenideElement ACTIONS_WITH_LOAD_FILE_SELECT = $("#do");
    public static final SelenideElement SAVE_BUTTON = $("#fileSave");
    public static final SelenideElement EXPORT_BUTTON_CAB = $("[class='export-filters-wrapper'] a");

    public static final SelenideElement SUCCESS_LOAD_FILE_LABEL = $("#refuse-until-validation-error");
    public static final SelenideElement CHECK_STAY_WIDGETS_FILTER = $("#send-campaign-filters-form");
    public static final SelenideElement SEARCH_WIDGET_INPUT = $("#search-in-filters");
    public static final SelenideElement WIDGET_LABEL = $(".marked");
    public static final SelenideElement GREEN_SOURCE = $("[class*='row green'] [class='row-content']");
    public static final SelenideElement SEARCH_SOURCE = $("[id=search-in-filters]");
    public static final SelenideElement COEFFICIENT_CAB_LABEL = $("[class='qf_value']");
}
