package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WebsitesAddLocators {
    public static final SelenideElement DOMAIN_INPUT = $("[id='url']");
    public static final SelenideElement COMMENT_INPUT = $("[id='comments']");
    public static final SelenideElement ALLOW_ADDING_TEASER_SUBDOMAIN_CHECKBOX = $("[id='add_goods_on_subdomains']");
    public static final SelenideElement ALLOW_ADDING_DOMAIN_OTHER_CLIENTS_CHECKBOX = $("[id='allow_other_clients']");
    public static final SelenideElement SAVE_BUTTON = $("[id='save']");
    public static final SelenideElement SUBMIT_BUTTON = $("[id='submit']");
    public static final SelenideElement DOMAIN_FIELD = $("[rel='noopener']");
}
