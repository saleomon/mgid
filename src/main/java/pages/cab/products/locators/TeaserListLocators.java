package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TeaserListLocators {
    //general
    public static final SelenideElement FILTER_CLIENT_ID_INPUT = $("#client_id");
    public static final SelenideElement FILTER_ANIMATION_VIDEO = $("[id='image_type']");
    public static final SelenideElement TEASER_ID_LOCATOR = $("[id*='t_id_']");
    public static final String TEASERS_ID_LOCATOR = "[id*='t_id_%s_%s']";
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
    public static final SelenideElement SEARCH_FEED_ICON = $("img[src*='type_search_feed.png']");
    public static final SelenideElement TEASER_STATUS = $("[id*='onModerationStatus'] [class='value']");
    public static final String TEASER_ID_LOCATORS = "[id*='t_id_']:not([class='hidden'])";
    public static final SelenideElement CANCEL_MODERATION_INTERNAL_DRAFTS = $("[src*='undo.png']");
    public static final SelenideElement CANCEL_MODERATION_CLIENT_DRAFTS = $("[src*='cancel_moderation.svg']");
    public static final SelenideElement SEND_TO_MODERATION = $("[src*='send-on-moderation-button.svg']");
    public static final SelenideElement REDO_ICON = $("[src*='add.png']");
    public static final SelenideElement PREVIEW_IN_INFORMER_LINK = $("a[title*='Teaser preview']");
    public static final ElementsCollection MAIN_SCREENSHOT_BLOCK = $$("[id='plagiarismContainer'] tr td:nth-child(1) a");
    public static final ElementsCollection PERCENTAGE_BLOCK = $$("[id='plagiarismContainer'] [class*='match']");
    public static final SelenideElement PLAGIARISM_ICON = $("[src*='plagiarism'][src*='svg']");
    public static final SelenideElement PLAGIARISM_POPUP = $("[class*='popup-plagiarism-landing-check']");
    public static final SelenideElement MIRRORING_IMAGES_LABEL = $("[id*='teaserMirrorImage']");
    public static final SelenideElement REMODERATION_BUTTON = $("[id*='send-on-karantin']");

    // approve
    public static final SelenideElement APPROVE_ICON = $("[id*='approve-teaser-']");
    public static final ElementsCollection APPROVE_ICONS = $$("[id*='approve-teaser-']");
    public static final SelenideElement CONTINUE_WITHOUT_LINKING = $x("//*[text()='Continue without linking']");
    public static final SelenideElement LINK_OFFER_BUTTON = $x("//*[text()='Link offers manually']");
    public static final SelenideElement ADVERTISER_NAME_LABEL = $("[class*=teaser-showname]");
    public static final SelenideElement CONTINUE_APPROVE_REJECT_BUTTON = $x("//*[text()='Apply with approve']");
    //block/unblock
    public static final SelenideElement UNBLOCK_TEASER_ICON = $("[id*='buttonUnblock']");
    public static final SelenideElement BLOCK_TEASER_WITH_CLOAKING_RADIO = $("[class*=popup-teaser-block-reason] [id='blockTypeCloaking']");
    public static final SelenideElement BLOCK_TEASER_WITH_CLOAKING_RADIO_SCHEDULE = $("[id='set_deferred_action_dialog'] [id='blockTypeCloaking']");
    public static final SelenideElement BLOCK_TEASER_WITH_CLOAKING_INPUT = $("[class*=popup-teaser-block-reason] [id='blockCloakingReasonInput']");
    public static final SelenideElement BLOCK_TEASER_WITH_CLOAKING_INPUT_SCHEDULE = $("[id='set_deferred_action_dialog'] [id='blockCloakingReasonInput']");
    public static final SelenideElement BLOCK_TEASER_REGULAR_RADIO = $("[class*=popup-teaser-block-reason] [id='blockTypeRegular']");
    public static final SelenideElement BLOCK_TEASER_REGULAR_RADIO_SCHEDULE = $("[id='set_deferred_action_dialog'] [id='blockTypeRegular']");
    public static final SelenideElement BLOCK_TEASER_REGULAR_INPUT = $("[class*=popup-teaser-block-reason] [id='blockReasonInput']");
    public static final SelenideElement BLOCK_TEASER_REGULAR_INPUT_SCHEDULE = $("[id='set_deferred_action_dialog'] [id='blockReasonInput']");
    public static final SelenideElement BLOCK_TEASER_CONFIRM = $("[class*=popup-teaser-block-reason] [class*='ui-dialog-buttonpane'] button");
    public static final SelenideElement BLOCK_TEASER_ICON = $("[id*='buttonBlock']");
    public static final SelenideElement UNBLOCK_TEASER_BY_CLIENT_ICON = $("[id*='buttonUnblock'][src*='red_client']");
    public static final SelenideElement UNBLOCK_TEASER_BY_ACC_MANAGER_ICON = $("[id*='buttonUnblock'][src*='red_account_manager']");
    public static final SelenideElement UNBLOCK_TEASER_BY_SYSTEM_ICON = $("[id*='buttonUnblock'][src*='red_system']");
    public static final SelenideElement UNBLOCK_TEASER_BY_MODERATOR_ICON = $("[id*='buttonUnblock'][src*='red_moderator']");

    // reject
    public static final SelenideElement REJECT_ICON = $("[id*='reject-teaser-']");
    public static final SelenideElement REJECT_REASON = $("[class='rejReasonsBlock']");
    public static final ElementsCollection REJECT_ICONS = $$("[id*='reject-teaser-']");
    public static final SelenideElement REJECT_FORM = $("#reject-form");
    public static final SelenideElement REJECT_FORM_SUBMIT_BUTTON = $("#submitReject");

    //delete
    public static final SelenideElement DELETE_ICON = $("[id*=buttonDelete]");
    public static final SelenideElement REASON_DELETE_TEASER_FIELD = $("#deleteReason");
    public static final SelenideElement REASON_DELETE_TEASER_BUTTON = $x(".//div[*[@id='deleteReasonForm']]//span[text()='Ok']");
    public static final SelenideElement UNDELETE_ICON = $("[id*=buttonUndelete]");

    //csr
    public static final SelenideElement CSR_MEDIUM_ICON = $("[src*='safety_ranking_medium.svg']");
    public static final SelenideElement CSR_HIGH_ICON = $("[src*='safety_ranking_high.svg']");

    //price
    public static final SelenideElement PRODUCT_PRICE_INPUT = $("[data-element=price]");

    //old price
    public static final SelenideElement PRODUCT_OLD_PRICE_INPUT = $("[data-element=price_old]");

    //discount
    public static final SelenideElement PRODUCT_DISCOUNT_INPUT = $("[data-element=discount]");

    //currency
    public static final SelenideElement CURRENCY_SELECT = $("[data-element=currencyid]");

    //CPC (price of click)
    public static final SelenideElement CPC_INPUT = $("[data-element=price_of_click]");
    public static final ElementsCollection CPC_INPUTs = $$("[data-element=price_of_click]");

    //URL (link, domain)
    public static final SelenideElement URL_INPUT = $("[id*=tmp_url_]");
    public static final SelenideElement URL_PARAMS_FIELD = $(".url_row span[id*=url] i");

    //title
    public static final SelenideElement INLINE_TITLE_LOCATOR = $(".black_bold_title");
    public static final SelenideElement TITLE_INPUT = $("[id*=tmp_title_]");

    //description
    public static final SelenideElement INLINE_DESCRIPTION_LABEL = $(".editableSpan[id*='advert_text']");
    public static final SelenideElement DESCRIPTION_INPUT = $("[id*='tmp_advert_text_']");

    //feeds provider
    public static final SelenideElement FEEDS_PROVIDER_INLINE = $("[id*='campaign_searchFeedProvider']");
    public static final SelenideElement FEEDS_PROVIDER_INPUT = $("[id*='searchFeedProvider-element_']");

    //call to action
    public static final SelenideElement INLINE_CALL_TO_ACTION_LABEL = $("[class*='editable'][id*='call_to_action']");
    public static final SelenideElement CALL_TO_ACTION_INPUT = $("[id*='tmp_call_to_action']");

    //live time
    public static final SelenideElement LIVE_TIME_SELECT = $("[data-element=ad_livetime]");

    //image
    public static final SelenideElement IMAGE_POPUP_BUTTON = $("[class='img-cloudinary'] [id*='img']");
    public static final SelenideElement VIDEO_POPUP_BUTTON = $("[class='img-cloudinary'] video");
    public static final SelenideElement IMAGE_CONTAINER = $("[id='imagePreviewHolder']");
    public static final SelenideElement SHOW_VALID_FORMATS = $("[id='validFormatsOnlyCheckbox']");
    public static final SelenideElement SAVE_IMAGE_BLOCK = $x("//*[text()='Submit']");
    public static final SelenideElement CLOUDINARY_IMAGE_FIELD = $("#cloudinaryImageFile");
    public static final SelenideElement CLOUDINARY_IMAGE_INPUT = $("#cloudinaryImageLink");
    public static final SelenideElement IMAGE_BLOCK_TEASER_LIST = $("[id*='img']");
    public static final SelenideElement VIDEO_BLOCK_TEASER_LIST = $("[id*='video'][class*='dialogImgEdit']");

    //IAP
    public static final SelenideElement ENABLED_COMPLIANT_IAP_ICON = $("[title='IAP Compliant']");
    public static final SelenideElement DISABLED_IAP_ICON = $("[title='IAP Not Compliant']");

    //teaser type(adType)
    public static final SelenideElement AD_TYPE_INLINE = $(".teaser_type_settings[id*='adTypes']");
    public static final ElementsCollection AD_TYPES_INLINE = $$(".teaser_type_settings[id*='adTypes']");
    public static final SelenideElement AD_TYPE_POPUP_SELECT = $("#js-adTypesselect");
    public static final SelenideElement AD_TYPE_POPUP_SUBMIT = $("#js-adTypesselect~[value='Submit']");

    //landing
    public static final SelenideElement LANDING_TYPES_INLINE = $(".landing_type_settings[id*=landingTypes]");
    public static final SelenideElement LANDING_SELECT = $("#js-landingTypesselect");
    public static final SelenideElement LANDING_SELECT_SUBMIT = $("#js-landingTypesselect~[value='Submit']");

    //others
    public static final SelenideElement VERIFY_ICON = $("img[src*='moderationVerified']");
    public static final SelenideElement NOT_NEED_VERIFICATION = $("[id='autoModerated'] [value='notNeedVerification']");
    public static final SelenideElement CATEGORY_LABEL = $("[id*='id_category']");
    public static final SelenideElement CATEGORY_DROP_DOWN_SELECT_LIST = $("[id*='category'][id*='selector']");
    public static final SelenideElement CATEGORY_SELECT_INLINE = $("[id*='category'][id*='dropdown']");
    public static final SelenideElement SUBMIT_CATEGORY_POPUP = $x("//*[text()='Submit']");
    public static final ElementsCollection CATEGORIES_LABEL = $$("[id*='id_category']");
    public static final ElementsCollection TEASER_IDS_LOCATOR = $$("[id*='t_id_']:not([class='hidden'])");
    public static final SelenideElement SUSPECTED_OF_CLOAKING = $("[class='suspectedOfCloaking']");
    public static final String CAMPAIGNS_IDS_LOCATOR = "[id*='rk_id']";

    //filters
    public static final SelenideElement PRODUCT_FILTER = $("[id='offer_name']");
    public static final SelenideElement SHOW_CERTIFIED_CHECKBOX = $("[id='only_certified_offers']");
    public static final SelenideElement FILTER_BY_STATUS = $("[id='status']");
    public static final SelenideElement FILTER_BY_AUTOMODERATE = $("[id='autoModerated']");
    public static final SelenideElement FILTER_BY_MODERATOR_ACTION = $("[id='moderatorActionsBox']");
    public static final SelenideElement FILTER_BY_KYC_NOT_VERIFIED_CHECKBOX = $("#kycTypes-1");
    public static final SelenideElement FILTER_BY_KYC_VERIFIED_CHECKBOX = $("#kycTypes-2");
    public static final SelenideElement FILTER_BY_KYC_BLOCKED_CHECKBOX = $("#kycTypes-3");
    public static final SelenideElement FILTER_BY_KYC_REJECT_CHECKBOX = $("#kycTypes-4");


    ///////////////////////////// MASS actions locators /////////////////////////////////////
    //general
    public static final SelenideElement MASS_ACTION_SUBMIT = $("#group-submit");
    public static final SelenideElement MASS_ACTION_SELECT_REJECT_REASONS = $("#select-reasons");
    public static final SelenideElement REJECT_REASONS_SUBMIT = $("[id='submitReject'] span");
    public static final SelenideElement MASS_ACTIONS_SELECT = $("#group-actions");
    public static final SelenideElement SELECT_ALL_CHECKBOXES_LINK = $("[href*='.selectAll']");
    public static final SelenideElement ICON_APAC = $x(".//*[contains(@src, 'apac_icon.svg')]");
    public static final SelenideElement COPYRIGHT_ICON = $("img[src*='copyright.png']");


    //Block
    public static final SelenideElement SELECT_REASON_BUTTON = $("[id='teaser-block-select-reason']");
    public static final SelenideElement BLOCK_BY_SCHEDULE_BUTTON = $("[class='set_deferred_action']");
    public static final ElementsCollection TIME_INTERVALS = $$("[id='tab_deferred_block'] [class='deferred_action_days_buttons'] input");
    public static final SelenideElement CONFIRM_BUTTON = $x("//*[text()='OK']");

    //category
    public static final SelenideElement MASS_CATEGORY_SELECT = $("#massCategory_dropdown");

    //landing (landingchange)
    public static final SelenideElement MASS_LANDING_SELECT = $("#landing_types");

    //dfp
    public static final SelenideElement ICON_GOOGLE_ADD_MANAGER_ENABLED = $x(".//img[contains(@src, 'ghits_dfp_flag_on.svg')]");
    public static final SelenideElement ICON_GOOGLE_ADD_MANAGER_DISABLED = $x(".//img[contains(@src, 'ghits_dfp_flag_off.svg')]");

    //adType (typechange)
    public static final SelenideElement MASS_AD_TYPE_SELECT = $("#ad_types");
    public static final SelenideElement MASS_TYPE_POPUP_SUCCESS = $("#typechangeResultDialog");
    public static final SelenideElement MASS_TYPE_POPUP_SUCCESS_CLOSE_BUTTON = $("[aria-labelledby*=typechangeResultDialog] button");

    //Change part of the title (changeTitle)
    public static final SelenideElement MASS_CHANGE_TITLE_OLD_VALUE_INPUT = $("#change_title_old_value");
    public static final SelenideElement MASS_CHANGE_TITLE_NEW_VALUE_INPUT = $("#change_title_new_value");

    //CPC
    public static final SelenideElement MASS_NEW_PRICE_VALUE_INPUT = $("#poc");

    //copy teaser (copy)
    //copy settings popup
    public static final SelenideElement COPY_CPC_FOR_ALL_CAMPAIGNS_INPUT = $("#cpc_for_all");
    public static final SelenideElement COPY_BUTTON = $("#doCopyBtn");
    public static final ElementsCollection COPY_CAMPAIGN_IDS_INPUT = $$("[id=copy_option_list] [name=campaign_id]");
    public static final ElementsCollection BLOCK_TEASER_DUE_COPYING_CHECKBOX = $$("[id='copy_teasers_dialog'] [id*='copy_opt_r'] [type=checkbox]");

    //TAGS CLOUD
    public static final SelenideElement ICON_TAGS_CLOUD_OFF = $("[src*='tags-0.png']");
    public static final SelenideElement ICON_TAGS_CLOUD_ON = $("[src*='tags-1.png']");
    public static final ElementsCollection TAGS_ELEMENTS_LIST = $$("#tagsDialog .tagsBlock [data-name]:not([style*='none'])");
    public static final SelenideElement TAGS_FORM_SAVE_BUTTON = $("#btnTagsSave");
    public static final SelenideElement TAGS_FORM_CLOSE_BUTTON = $("[class*=closethick]");
    public static final SelenideElement TAGS_FORM_SEARCH = $("#searchTags");
    public static final SelenideElement TAGS_DELETE_BUTTON = $("#btnTagsTeaserDelete");
    public static final SelenideElement TAGS_SAVE_BUTTON = $("#btnTagsTeaserSave");
    public static final SelenideElement TITLE_TAGS_BLOCK_MASS = $("[id='massTagsDialog'] [class='titleBlockTitle']");
    public static final SelenideElement TITLE_TAGS_BLOCK_SINGLE = $("[id='tagType__title']");
    public static final SelenideElement SAVE_BUTTON = $("#save");


    //compliant
    public static final SelenideElement COMPLIANT_ALL_COUNTRY_ICON = $("img[src*='all_multi_geo_compliant.png']");
    public static final SelenideElement COMPLIANT_PART_COUNTRY_ICON = $("img[src*='particular_multi_geo_compliant.png']");
    public static final SelenideElement COMPLIANT_ICON_POPUP = $("[id*='compliantFlag']");
    public static final ElementsCollection COMPLIANT_ICONS_POPUP = $$("[id*='compliantFlag']");
    public static final ElementsCollection ALL_COMPLIANT_CHECKBOXES = $$("[class='countriesCompliant']");
    public static final ElementsCollection ALL_COMPLIANT_SELECTED_CHECKBOXES = $$("[class='countriesCompliant'][checked]");
    public static final SelenideElement SAVE_COMPLIANT_BUTTON = $("[class*='saveCompliantGeo'] [class='ui-button-text']");
    public static final SelenideElement ROMANIA_ICON = $("[id*='compliantFlag'][src*='ro.png']");
    public static final SelenideElement ITALY_ICON = $("[id*='compliantFlag'][src*='it.png']");
    public static final SelenideElement SPAIN_ICON = $("[id*='compliantFlag'][src*='es.png']");
    public static final SelenideElement CZECH = $("[id*='compliantFlag'][src*='cz.png']");
    public static final SelenideElement POLAND_ICON = $("[id*='compliantFlag'][src*='pl.png']");
    public static final SelenideElement COMPLIANT_FILTER = $("[id='compliant']");
    public static final SelenideElement SAVE_COMPLIANT_SETTINGS = $("[class*='saveCompliantGeo'] [class='ui-button-text']");
    public static final SelenideElement DOWNLOAD_CERTIFICATE_BUTTON = $("[class*='fa-download']");
    public static final ElementsCollection DOWNLOAD_CERTIFICATE_BUTTONS = $$("[class*='fa-download']");
    public static final SelenideElement FILTER_BY_CERTIFICATE = $("[id='only_teasers_with_certificate']");

    //offer
    public static final SelenideElement LINKED_OFFERS = $("[class='existOfferNames']");
    public static final ElementsCollection DISPLAYED_OFFERS = $$("[class='offer-popup-content'] a");
    public static final SelenideElement APPLY_WITH_APPROVE = $x("//*[text()='Apply with approve']");

    //other
    public static final SelenideElement CLEAR_FIELD_BUTTON = $("[class=teaser_wrap] [class*=clear-btn-visible]");
    public static final SelenideElement CLEAR_FIELD_CALL_TO_ACTION_BUTTON = $("[class*=teaser-table-cell-row] [class*=clear-btn-visible]");

    //Teaser info
    public static final String ADVERTISER_NAME_FIELD = "//textarea[@id='tmp_campaign_showname_%s']/preceding-sibling::span[1]";
    public static final String ADVERTISER_NAME_INPUT = "//textarea[@id='tmp_campaign_showname_%s']";
    public static final String ADVERTISER_NAME_TEMP_FIELD = "#campaign_showname_%s";

    //auto category
    public static final SelenideElement ACCEPT_AUTO_CATEGORY_BUTTON = $("[id*='autoCategoryBlock'] [class*='fa-check-square']");
    public static final SelenideElement REJECT_AUTO_CATEGORY_BUTTON = $("[id*='autoCategoryBlock'] [class*='fa-window-close']");
    public static final SelenideElement CATEGORY_NAME_LABEL = $("[class='categoryName']");
    public static final SelenideElement AUTO_CATEGORY_LABEL = $("[id*='autoCategoryBlock']");

    //kyc icons
    public static final SelenideElement KYC_NOT_VERIFIED_ICON = $("img[src*='kyc_status_1.png']");
    public static final ElementsCollection KYC_NOT_VERIFIED_ICONS = $$("img[src*='kyc_status_1.png']");
    public static final SelenideElement KYC_VERIFIED_ICON = $("img[src*='kyc_status_2.png']");
    public static final ElementsCollection KYC_VERIFIED_ICONS = $$("img[src*='kyc_status_2.png']");
}
