package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CampaignsAddLocators {
    public static final SelenideElement SAVE_BUTTON =  $("#sbtbutton");

    /////////////////////////// General Settings   ////////////////////////////////
    public static final SelenideElement GENERAL_SETTINGS_FIELDSET = $("[id='fieldset-general_settings']");
    public static final SelenideElement TYPE_SELECT = $("[id='campaign_types']");
    public static final SelenideElement FEEDS_PROVIDER_INPUT = $("[id='searchFeedProvider']");
    public static final SelenideElement SUB_TYPE_SELECT = $("[id='video_subtype']");
    public static final SelenideElement PAYMENT_MODEL_SELECT = $("[id='video_payment_model']");
    public static final SelenideElement MARKETING_OBJECTIVE_SELECT = $("#marketingObjective");
    public static final SelenideElement MEDIA_SOURCE_SELECT = $("#mediaSource");
    public static final SelenideElement TIRE_SELECT = $("[id='tier']");
    public static final SelenideElement MARK_AS_WHITE_CHECKBOX = $("[id='is_white']");
    public static final SelenideElement MIRROR_SELECT = $("[id='mirror']");
    public static final SelenideElement NAME_FIELD = $("[id='name']");
    public static final SelenideElement DISPLAYED_NAME_FIELD = $("[id='show_name']");
    public static final SelenideElement KEYWORD_FIELD = $("[id='search_feed_keyword']");
    public static final SelenideElement CATEGORY_SELECT = $("[id='cat_select_dropdown']");
    public static final SelenideElement CHOOSEN_CATEGORY_SELECT = $("#cat_select_selector-button");
    public static final SelenideElement CATEGORY_SELECTED = $("[class*='selected-cat']");
    public static final SelenideElement LANGUAGE_SELECT = $("[id='campaign_lang']");
    public static final SelenideElement LANGUAGE_SELECTED = $("[id='campaign_lang'] [ selected]");
    public static final SelenideElement SHOW_TEASERS_ONLY_ON_SITES_IN_THIS_LANGUAGE_CHECKBOX = $("#use_only_same_language_widgets");
    public static final SelenideElement BLOCK_TEASER_AFTER_CREATION_CHECKBOX = $("[id='block_before_show']");
    public static final SelenideElement BLOCK_BY_MANAGER_CHECKBOX = $("[id='manager_delete']");
    public static final SelenideElement ROTATE_IN_ADSKEEPER_CHECKBOX = $("[id='adskeeper_on']");
    public static final SelenideElement ROTATE_IN_MGID_CHECKBOX = $("[id='marketgid_on']");
    public static final SelenideElement ACF_FIELD = $("[id='factor']");
    public static final SelenideElement SHOW_PROBABILITY_MAX_FIELD = $("[id='probability_max']");
    public static final SelenideElement INCLUDE_REPORTS_QUOTA_CHECKBOX = $("[id='enableQuotas']");
    public static final SelenideElement COUNT_BAD_QUOTAS_CALENDAR = $("[id='badSitesQuotas']");
    public static final SelenideElement SHOW_PROBABILITY_RSS_FIELD = $("[id='suspicious_blocks_quota']");
    public static final SelenideElement SET_VALUE_CHECKBOX = $("[id='suspicious_blocks_quota_manual']");
    public static final SelenideElement DO_NOT_SHOW_QUALITY_FACTOR_CHECKBOX = $("[id='not_use_quality_factor']");
    public static final SelenideElement DO_NOT_SEND_TO_GEO_EDGE_CHECKBOX = $("[id='disable_landing_check']");

    ///////////////////////////////    Capping   //////////////////////////////////////
    public static final SelenideElement TEASER_IMPRESSION_FREQUENCY_FIELD = $("[id='click_capping_frequency']");
    public static final SelenideElement DO_NOT_SHOW_ALL_TEASERS_AFTER_CLICK_CHECKBOX = $("[id='click_capping_whole_campaign']");
    public static final SelenideElement DO_NOT_SHOW_ALL_TEASERS_AFTER_ACTION_CHECKBOX = $("[id='use_conversion_capping']");
    public static final SelenideElement LIMIT_TOTAL_IMPRESSIONS_CHECKBOX = $("[id='use_limit_total_impressions']");
    public static final SelenideElement IMPRESSION_LIMIT_FIELD = $("[id='max_show_per_impressions_limit']");
    public static final SelenideElement IMPRESSION_LIMIT_PERIOD_FIELD = $("[id='max_show_per_visitor_period']");

    ///////////////////////////////    Video capping   //////////////////////////////////////
    public static final SelenideElement VIDEO_CAPPING_EVENTS = $("#video_capping_event");
    public static final SelenideElement FREQUENCY_CAPPING_SELECT = $("#video_capping_interval");
    public static final SelenideElement SEND_NO_MORE_THAN_INPUT = $("#video_capping_events_limit");

    ///////////////////////////////    VAST/VPAID   //////////////////////////////////////
    public static final SelenideElement VAST_URL = $("#vast_url");
    public static final SelenideElement VAST_PRICE = $("#vast_cpm");

    /////////////////////////////////    Limits   ////////////////////////////////////////
    public static final SelenideElement BLOCK_BY_SCHEDULE_CHECKBOX = $("[id='proc_tblock']");
    public static final SelenideElement AD_START_DAY_CALENDAR = $("[id='when_autostart']");
    public static final SelenideElement AD_END_DAY_CALENDAR = $("[id='limit_date']");
    public static final SelenideElement LIMITS_BLOCK_RADIO = $("[id='limit_type-element'] [type='radio']");
    public static final SelenideElement SELECTED_OPTION_LIMITS_BLOCK_RADIO = $("[id='limit_type-element'] [type='radio'][checked='checked']");
    public static final String AD_CAMPAIGN_LIMITS_RADIO = "[name=limit_type]";
    public static final SelenideElement LIMIT_PER_DAY_BUDGET_FIELD = $("[id='limitPerDay']");
    public static final SelenideElement LIMIT_GENERAL_BUDGET_FIELD = $("[id='limitPerCampaign']");
    public static final SelenideElement LIMIT_PER_DAY_CLICKS_FIELD = $("[id='limitClicksPerDay']");
    public static final SelenideElement LIMIT_GENERAL_CLICKS_FIELD = $("[id='limitClicks']");
    public static final SelenideElement LIMIT_PER_DAY_CONVERSION_FIELD = $("[id='limitConversionsPerDay']");
    public static final SelenideElement LIMIT_GENERAL_CONVERSION_FIELD = $("[id='limitConversions']");
    public static final SelenideElement APPLY_FOR_ALL_REGIONS_CHECKBOX = $("[id='ignore_poc_limits_detailed-ignore_price_apply_for_all']");
    public static final SelenideElement IGNORE_POC_LIMITS_CHECKBOX = $("[id='ignore_poc_limits']");
    public static final SelenideElement CONTAINER_FOR_PRICES_BLOCK = $("[id='geo-group-prices-limits-tree-container']");
    public static ElementsCollection PARETN_ELEMENTS = $$("[id='ignore_poc_limits_detailed-element'] [class='tree-pointer']");
    public static ElementsCollection LIST_REGIONS = $$x(".//div[@id='geo-group-prices-limits-tree']//div[@class='node-body' and not(div[@class='node-expander'])]/div[@class='node-label']/span");
    public static ElementsCollection LIST_PRICES = $$x(".//div[@id='geo-group-prices-limits-tree']//div[@class='node-body' and not(div[@class='node-expander'])]//input[@class='text-geo-price-input']");
    public static SelenideElement AD_LIFETIME_SELECT = $("[id='ad_livetime']");
    public static final SelenideElement UNLIMITED_RADIOBUTTON = $("#limit_type-unlimited");
    public static final SelenideElement SET_LIMITS_RADIOBUTTON = $("#limit_type-complete_limits");
    public static final SelenideElement BUDGETS_LIMITS_CHECKBOX = $("#enable_budgets_limits");
    public static final SelenideElement AD_CAMPAIGN_DAYLIMIT_INPUT = $("#limitPerDay");
    public static final SelenideElement GENERAL_CAMPAIGN_LIMIT_BUDGET_INPUT = $("#limitPerCampaign");
    public static final SelenideElement IMPRESSIONS_LIMIT_CHECKBOX = $("#enable_impressions_limits");
    public static final SelenideElement CLICKS_LIMIT_CHECKBOX = $("#enable_clicks_limits");
    public static final SelenideElement IMPRESSIONS_DAYLIMIT_INPUT = $("#limit_impressions_per_day");
    public static final SelenideElement GENERAL_CAMPAIGN_LIMIT_IMPRESSIONS_INPUT = $("#limit_impressions");
    public static final SelenideElement SPLIT_BUDGET_EVENTLY_THROUGH_DAY_CHECKBOX = $("#useFloatingLimit");
    public static final SelenideElement COMPLETE_VIEWS_LIMITS_CHECKBOX = $("[id='enable_complete_views_limits']");
    public static final SelenideElement COMPLETE_VIEWS_DAYLIMIT_INPUT = $("#complete_daily_limit");
    public static final SelenideElement COMPLETE_VIEWS_GENERAL_LIMIT_INPUT = $("#complete_general_limit");

    /////////////////////////////////    Targeting   ////////////////////////////////////////
    ///////////////////////////////////////  Targeting block    /////////////////////////////////////////
    public static final SelenideElement ENABLE_TARGETING_BLOCK_RADIO = $("#targeting_status-1");
    public static final SelenideElement LOCATION_OUT_BLOCK = $("[class='location-out']");
    public static final SelenideElement OS_OUT_BLOCK = $("[class='os-out']");
    public static final SelenideElement BROWSER_OUT_BLOCK = $("[class='browser-out']");
    public static final SelenideElement LANGUAGE_OUT_BLOCK = $("[class='language-out']");
    public static final SelenideElement CONNECTION_OUT_BLOCK = $("[class='connectionType-out']");
    public static final SelenideElement PROVIDER_OUT_BLOCK = $("[class='provider-out']");
    public static final SelenideElement PLATFORM_OUT_BLOCK = $("[class='platform-out']");
    public static final SelenideElement SOCDEM_OUT_BLOCK = $("[class='socdem-out']");
    public static final SelenideElement INTERESTS_OUT_BLOCK = $("[class='interest-out']");
    public static final String ROTATION_IN_UKRAINE_TARGETING_BLOCK_TEXT = "[class*='targeting-alert']";
    public static final String TARGETS_OUT_BLOCK = "[class='%s-out']";
    public static final SelenideElement BUNDLE_OUT_BLOCK = $("[class='bundle-out']");
    public static final SelenideElement LOCATION_TARGET = $("#location");
    public static final SelenideElement OS_TARGET = $("#os");
    public static final SelenideElement BROWSER_TARGET = $("#browser");
    public static final SelenideElement BROWSER_LANGUAGE_TARGET = $("#language");
    public static final SelenideElement CONNECTION_TARGET = $("#connectionType");
    public static final String CHOSEN_TARGET_ELEMENT = "[id='%s-out'] [class='targeting-list-el %s'][data-id='%s']";
    public static final String CHOSEN_TARGET_ELEMENT_MULTIPLE = "[id='%s-out'] [class='targeting-list-elmultiple'][data-id='%s']";
    public static final String CHOSEN_TARGET_ELEMENT_WITHOUT_TYPE = "[id='%s-out'] [class*='targeting-list-el'][data-id='%s']";
    public static final String NOT_EXPANDED_ELEMENTS = "[class='targeting-in'] [class='targeting-list-el'] [class*='targeting-expand']";
    public static final String TARGET_ELEMENTS_LEVEL_2 = "[id='%s-in'] li[level='2']";
    public static final String TARGET_ELEMENTS_LEVEL_0 = "[id='%s-in'] li[level='0']";
    public static final SelenideElement PROVIDER_TARGET = $("[id='provider']");
    public static final SelenideElement PUBLISHER_CATEGORIES_TAB = $("[id='platform']");
    public static final SelenideElement DEMOGRAPHICS_TAB = $("[id='socdem']");
    public static final SelenideElement INTERESTS_TAB = $("[id='interest']");
    public static final String TARGETS_TAB = "[id='%s']";
    public static final SelenideElement BUNDLES_TAB = $("[id='bundle']");
    public static final SelenideElement AUDIENCE_TAB = $("[id='audience']");
    public static final SelenideElement TRAFFIC_TYPE_TAB = $("[id='trafficType']");
    public static final SelenideElement CONTEXT_TAB = $("[id='context']");
    public static final SelenideElement CONTEXT_OUT_BLOCK = $("[class='context-out']");
    public static final SelenideElement SENTIMENTS_TAB = $("[id='sentiments']");
    public static final SelenideElement SENTIMENTS_OUT_BLOCK = $("[class='sentiments-out']");
    public static final SelenideElement PHONE_PRICE_RANGES_TAB = $("[id='phonePriceRange']");
    public static final SelenideElement PHONE_PRICE_RANGES_OUT_BLOCK = $("[class='phonePriceRange-out']");

    public static SelenideElement TARGETINGS_LEGEND = $x(".//legend[text()='Targetings']");
    public static final SelenideElement USE_TARGETING_RADIO = $("[id='targeting_switch_status'] [value='1']");
    public static final SelenideElement REMOVE_SELECTED_TARGET_BUTTON = $("[class='targeting-out-title'] [class='targeting-btn targeting-remove-all']");
    public static final ElementsCollection REMOVE_SELECTED_TARGET_BUTTONS = $$("[class='targeting-out-title'] [class='targeting-btn targeting-remove-all']");
    // location
    public static final SelenideElement LOCATION_TARGET_SELECTED = $("[class='location-out'] [class*='targeting-list-el'][level='0']");
    // os
    public static final SelenideElement OS_TARGET_SELECTED = $("[class='os-out'] [class*='targeting-list-el'][level='0']");
    // browser
    public static final SelenideElement BROWSER_TARGET_SELECTED = $("[class='browser-out'] [class*='targeting-list-el'][level='0']");
    // browser language
    public static final SelenideElement LANGUAGE_TARGET_SELECTED = $("[class='language-out'] [class*='targeting-list-el'][level='0']");
    // provider
    public static final SelenideElement DEMOGRAPHICS_SELECTED = $("[class='socdem-out'] [class*='targeting-list-el'][level='0']");
    // bundles
    public static final SelenideElement BUNDLES_SELECTED = $("[class='bundle-out'] [class*='targeting-list-el'][level='0']");
    // connection
    public static final SelenideElement CONNECTION_TYPE_SELECTED = $("[id='connectionType-out'] [class*='targeting-list-el'][level='0']");
    // viewability
    public static final SelenideElement VIEWABILITY_SELECTED = $("[id='viewability-out'] [class*='targeting-list-el'][level='0']");
   // retargeting
    public static final SelenideElement AUTORETARGETING_CHECKBOX = $("[id='use_autoretargeting']");

    //percentage of displaying
    public static final SelenideElement PERCENTAGE_OF_DISPLAYS = $("[id='show_percent']");
    public static final SelenideElement PERCENTAGE_CHECKBOX = $("[class='special-country-checkbox']");
    public static ElementsCollection LIST_OF_SUB_REGIONS = $$("li [class='show_percent-li-region'] input");
    public static ElementsCollection LIST_OF_COUNTRY = $$("li[class='show_percent-li-country'] input");
    public static final SelenideElement REGION_ELEMENT = $("li[class='show_percent-li-country'] input");
    public static final SelenideElement COUNTRY_CHECKBOX = $("[class='special-country-checkbox']");
    public static final SelenideElement MATCHED_VISITORS = $("#dspOnlyMatched");

    ///////////////////////      DSP settings   //////////////////////////////////////////////////
    public static final SelenideElement NATIVE_VERSION = $("[id='dspNativeVersion']");
    public static final SelenideElement DSP = $("[id='dspId']");
    public static final SelenideElement COOPERATION_TYPE = $("[id='dspCooperationType']");
    public static final SelenideElement SHOW_PROBABILITY_DSP = $("[id='dspSettings-probability']");
    public static final SelenideElement SEND_SITE_INFO_SELECT = $("[id='dspSettings-dspSendSiteInfo']");
    public static final SelenideElement VAST_BIDFLOOR_FIELD = $("#dspSettings-vastBidfloor");
    public static final SelenideElement BID_REQUEST_URL_US_MID_INPUT = $("#bidRequestUrlByDcSubform-bidRequestUrlByDc3");
    public static final SelenideElement VIDEO_DSP = $("[id='auctionType']");
    public static final SelenideElement SELECTED_SEND_SITE_INFO = $("[id='dspSettings-dspSendSiteInfo'] [selected]");

    ////////////////////////////////////////// Links Settings   ///////////////////////////////////////////////
    public static final SelenideElement UTM_CHECKBOX = $("[id='utm_flag']");
    public static final SelenideElement UTM_MEDIUM_TAG_FIELD = $("[id='utm-utm_medium']");
    public static final SelenideElement UTM_SOURCE_TAG_FIELD = $("[id='utm-utm_source']");
    public static final SelenideElement UTM_CAMPAIGN_TAG_FIELD = $("[id='utm-utm_campaign']");
    public static final SelenideElement CUSTOM_TAG_CHECKBOX = $("[id='user_utm_flag']");
    public static final SelenideElement CUSTOM_TAG_FIELD = $("[id='user_utm-utm_custom']");
    public static final SelenideElement CUSTOM_TAG_DESCRIPTION = $("#fieldset-user_utm .description");

    //////////////////////////////////////////   Convertion stages settings   ///////////////////////////////////////////////
    public static final SelenideElement CONVERSION_STAGES_SETTINGS = $("#fieldset-conversion");
    public static final SelenideElement INTEREST_STAGE_CHECKBOX = $("[id='conversion-conversion_interest-use_stage']");
    public static final SelenideElement DESIRE_STAGE_CHECKBOX = $("[id='conversion-conversion_decision-use_stage']");
    public static final SelenideElement ACTION_STAGE_CHECKBOX = $("[id='conversion-conversion_buy-use_stage']");
    public static final SelenideElement CONVERSIONS_STAGE_RADIO = $("[id='importGaTargets-0']");
    public static final SelenideElement INTEREST_CONVERSION_FIELD = $("[id='conversion-conversion_interest-cpa']");
    public static final SelenideElement DESIRE_CONVERSION_FIELD = $("[id='conversion-conversion_decision-cpa']");
    public static final SelenideElement ACTION_CONVERSION_FIELD = $("[id='conversion-conversion_buy-cpa']");
    public static final SelenideElement INTEREST_STAGE_SELECT = $("[id='conversion-conversion_interest-target']");
    public static final SelenideElement INTEREST_STAGE_COUNTING_SELECT = $("[id='conversion-conversion_interest-deduplicateConversions']");
    public static final SelenideElement DESIRE_STAGE_COUNTING_SELECT = $("[id='conversion-conversion_decision-deduplicateConversions']");
    public static final SelenideElement ACTION_STAGE_COUNTING_SELECT = $("[id='conversion-conversion_buy-deduplicateConversions']");
    public static final SelenideElement INTEREST_STAGE_SELECTED_VALUE = $x("//*[@id='conversion-conversion_interest-target']/option[@selected='selected']");
    public static final SelenideElement DESIRE_STAGE_SELECT = $("[id='conversion-conversion_decision-target']");
    public static final SelenideElement DESIRE_STAGE_SELECTED_VALUE = $x("//*[@id='conversion-conversion_decision-target']/option[@selected='selected']");
    public static final SelenideElement ACTION_STAGE_SELECT = $("[id='conversion-conversion_buy-target']");
    public static final SelenideElement ACTION_STAGE_SELECTED_VALUE = $x("//*[@id='conversion-conversion_buy-target']/option[@selected='selected']");
    public static final SelenideElement CPA_CORRECTION_FIELD = $("[id='costPerActionCorrection']");

    //////////////////////////////////////////  Autooptimization    /////////////////////////////////////////////////
    public static final SelenideElement TEASER_ROTATION_LIMIT = $("[id='auto_opt_subform-price_optimization_rotation_limit_teasers']");
    public static final SelenideElement CAMPAIGN_ROTATION_LIMIT = $("[id='auto_opt_subform-price_optimization_rotation_limit_campaign']");
    public static final SelenideElement TEASER_CALCULATION_LIMIT = $("[id='auto_opt_subform-price_optimization_calculation_limit_teasers']");
    public static final SelenideElement CAMPAIGN_CALCULATION_LIMIT = $("[id='auto_opt_subform-price_optimization_calculation_limit_campaign']");
    public static final SelenideElement CONVERSIONS_TO_REFILL = $("[id='auto_opt_subform-price_optimization_conversions_to_refill']");
    public static final SelenideElement OPTIMIZATION_LIMIT_BY_WIDGET = $("[id='auto_opt_subform-price_optimization_limit']");
    public static final SelenideElement SELECTIVE_BIDDING_WEIGHT = $("[id='auto_opt_subform-price_optimization_quality_factor_weight']");
    public static final SelenideElement USE_AUTOOPTIMIZATION_CHECKBOX = $("[id='auto_opt_subform-use_price_optimization']");

    ///////////////////////////////////////////   Push elements      //////////////////////////////////////////////
    public static final SelenideElement FIELD_FOR_PUSH_ICON = $("[id='push_icon_link']");
    public static final SelenideElement INPUT_FOR_PUSH_ICON = $("[id='push_icon']");
    public static final SelenideElement ANY_ELEMENT_FOR_CLICK = $("[id='fieldset-push_icon_subform'] legend");

    ///////////////////////////////////////////   Event Urls      //////////////////////////////////////////////
    public static final SelenideElement EVENT_URL_FIELDSET = $("[id='fieldset-event_url_settings']");
    public static final SelenideElement EVENT_URLS_IMPRESSION_CHECKBOX = $("[id='video_campaign_event_impression_checkbox']");
    public static final SelenideElement EVENT_URLS_IMPRESSION_INPUT = $("[id='video_campaign_event_impression']");
    public static final SelenideElement EVENT_URLS_FIRST_QUARTILE_CHECKBOX = $("[id='video_campaign_event_first_quartile_checkbox']");
    public static final SelenideElement EVENT_URLS_FIRST_QUARTILE_INPUT = $("[id='video_campaign_event_first_quartile']");
    public static final SelenideElement EVENT_URLS_MIDPOINT_CHECKBOX = $("[id='video_campaign_event_midpoint_checkbox']");
    public static final SelenideElement EVENT_URLS_MIDPOINT_INPUT = $("[id='video_campaign_event_midpoint']");
    public static final SelenideElement EVENT_URLS_THIRD_QUARTILE_CHECKBOX = $("[id='video_campaign_event_third_quartile_checkbox']");
    public static final SelenideElement EVENT_URLS_THIRD_QUARTILE_INPUT = $("[id='video_campaign_event_third_quartile']");
    public static final SelenideElement EVENT_URLS_COMPLETE_CHECKBOX = $("[id='video_campaign_event_complete_checkbox']");
    public static final SelenideElement EVENT_URLS_COMPLETE_INPUT = $("[id='video_campaign_event_complete']");
    public static final SelenideElement EVENT_URLS_CLICK_CHECKBOX = $("[id='video_campaign_event_click_checkbox']");
    public static final SelenideElement EVENT_URLS_CLICK_INPUT = $("[id='video_campaign_event_click']");
    public static final SelenideElement EVENT_URLS_SKIP_CHECKBOX = $("[id='video_campaign_event_skip_checkbox']");
    public static final SelenideElement EVENT_URLS_SKIP_INPUT = $("[id='video_campaign_event_skip']");

    //other
    public static final SelenideElement CONFIRM_BUTTON = $x("//*[text()='Ok']");
    public static final SelenideElement RESTRICTION_POPUP = $(".ui-dialog");

    //certificate
    public static final String CERTIFICATE_NAME_LABEL = "//*[text()='%s']";
    public static final ElementsCollection REMOVE_CERTIFICATE_ICON = $$("[id*='remove']");
}
