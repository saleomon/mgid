package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TeaserAddLocators {
    ///////////////////////////// Create teaser  //////////////////////////////
    public static final SelenideElement URL_INPUT = $("#sunriseField_url");
    public static final SelenideElement USE_IMPROVEMENT_CHECKBOX = $("#useImprovement");
    public static final SelenideElement IMAGE_FIELD = $("#imageFile");
    public static final SelenideElement IMAGE_INPUT = $("#imageLink");
    public static final SelenideElement IMAGE_UPLOAD_FORM = $("#uploadForm");
    public static final SelenideElement IMAGE_LEFT_CROP = $("#hLeft");
    public static final SelenideElement IMAGE_TOP_CROP = $("#hTop");
    public static final SelenideElement TITLE_INPUT = $("#sunriseField_title");
    public static final SelenideElement DESCRIPTION_INPUT = $("#sunriseField_advert_text");
    public static final SelenideElement CALL_TO_ACTION_INPUT = $("#sunriseField_call_to_action");
    public static final SelenideElement CATEGORY_SELECT = $("#cat_dropdown_dropdown");
    public static final SelenideElement TYPE_SELECT = $("#ad_types");
    public static final SelenideElement LANDING_SELECT = $("#landing_types");
    public static final SelenideElement LIVETIME_SELECT = $("#adLivetime");
    public static final SelenideElement LIVETIME_EDIT_SELECT = $("select[id*=ad_livetime]");
    public static final SelenideElement CPC_INPUT = $("#sunriseField_price_of_click");
    public static final SelenideElement PRICE_INPUT = $("#sunriseField_price");
    public static final SelenideElement PRICE_OLD_INPUT = $("#sunriseField_price_old");
    public static final SelenideElement DISCOUNT_INPUT = $("#sunriseField_discount");
    public static final SelenideElement CURRENCY_SELECT = $("#sunriseField_currencyid");
    public static final SelenideElement SAVE_BUTTON = $("#save");
    public static final SelenideElement SAVE_TO_DRAFT = $("#draft_btn");
    public static final SelenideElement AMOUNT_OF_DRAFTS = $("#draft_teasers_all_inner");
    public static final SelenideElement IMAGE_BLOCK_VIEW = $("[id='imagePreviewHolder']");
    public static final SelenideElement SHOW_VALID_FORMATS = $("[id='validFormatsOnlyCheckbox']");
    public static final SelenideElement IMAGES_POPUP_OPEN = $("[id='open_images']");
    public static final ElementsCollection BLOCKS_OF_IMAGES = $$("[class='image-item__bg']");
    public static final SelenideElement SUBMIT_IMAGE_FROM_REQUEST_BUTTON = $x("//*[text()='Submit']");
    public static final SelenideElement MAX_CPC_POPUP_AGREE_PRICE = $("a[id='continue-save']");
    public static final SelenideElement MAX_PRICE_OF_CLICK_POPUP = $("#maxPriceOfClickPopup");
    public static final SelenideElement GET_AUTO_TITLE_BUTTON = $("[id='getAutoTitlesBtn']");
    public static final SelenideElement TITLE_SUGGESTION_SELECT = $("[id='title_suggestions_type']");
    public static final ElementsCollection RESET_DATA_BUTTONS = $$("[class*=clear-btn-visible]");
    public static final SelenideElement MANUAL_FOCAL_POINT_BUTTON = $("[id='manual-control']");
    public static final SelenideElement MANUAL_FOCAL_CONFIRM_BUTTON = $("[id='manual-control-confirm']");
    public static final SelenideElement MANUAL_FOCAL_CANCEL_BUTTON = $("[id='manual-control-cancel']");
    public static final SelenideElement MANUAL_FOCAL_DEFAULT_BUTTON = $("[id='manual-control-default']");
    public static final SelenideElement MANUAL_FOCAL_EDIT_BUTTON = $("[id='manual-control-edit']");
    public static final SelenideElement FOCAL_POINT_ICON = $("[id='crosshair']");

    //getty images
    public static final SelenideElement STOCK_GETTY_IMAGES_TAB = $("[class='ghits-tabs'] [data-tab='stock']");
    public static final SelenideElement DOWNLOADED_GETTY_IMAGES_CHECKBOX = $("[id='isGettyEditedCheckbox']");
    public static final SelenideElement UPLOAD_IMAGE_TAB = $("[class='ghits-tabs'] [data-tab='upload']");
    public static final ElementsCollection GETTY_IMAGES = $$("[class*='stock-gallery-elements-item_']");
    public static final ElementsCollection GETTY_IMAGES_PHRASES_DELETE_BUTTON = $$("[class='stock-gallery-search-item-remove-btn']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_PHRASE = $("[class='stock-gallery-search-item-text']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_INPUT = $("[class='stock-gallery-image-search']");
    public static final SelenideElement GALLERY_NOTIFICATION_BLOCK = $("[class='stock-gallery-notification']");
    public static final SelenideElement SAVE_IMAGE_BUTTON = $("[id='saveImgBtn']");
    public static final SelenideElement SAVE_MANUAL_IMAGE_BUTTON = $("[id='saveManualImagesBtn']");
    public static final SelenideElement LOAD_MANUAL_IMAGES_TAB = $("[id='manual-upload-btn']");
    public static final SelenideElement FORMAT_VIEW_BLOCK = $("[class*='stock-gallery-type-options']");
    public static final SelenideElement SORT_BY_FOR__VIEW_BLOCK = $("[class='stock-gallery-sortBy-options']");
    public static final SelenideElement NEXT_PAGE_GALLERY_BUTTON = $("[class='stock-gallery-pagination-item-btn next']");
    public static final SelenideElement ACTIVE_PAGE_GALLERY = $("[class='stock-gallery-pagination-item active']");

    ///////////////////////////// Edit teaser  //////////////////////////////


}
