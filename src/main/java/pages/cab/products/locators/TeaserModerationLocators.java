package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class TeaserModerationLocators {
    public static final SelenideElement BRAND_ICON = $x(".//*[contains(@src, 'campaignTierBrand.png')]");
    public static final SelenideElement WHITEHAT_ICON = $x(".//*[contains(@src, 'campaignTierWhitehat.png')]");
    public static final SelenideElement APAC_ICON = $x(".//*[contains(@src, 'apac_icon.svg')]");
    public static final SelenideElement APAC_ICON_ZION = $x(".//*[@class='zion-popup__geo']/*[contains(@src, 'apac_icon.svg')]");
    public static final SelenideElement LINK_CHECK_BUTTON = $(".zion-buttons [data-phase=url_check]");
    public static final SelenideElement LP_CHECK_BUTTON = $(".zion-buttons [data-phase=lp_check]");
    public static final SelenideElement IMAGE_CHECK_BUTTON = $(".zion-buttons [data-phase=image_check]");
    public static final SelenideElement TITLE_CHECK_BUTTON = $(".zion-buttons [data-phase=title_check]");
    public static final SelenideElement CLOSE_ZION_POPUP = $(".zion-popup__close-btn");
    public static final SelenideElement START_MODERATION = $("a[class*='moderation-start ']");
    public static final SelenideElement SHOW_SUGGESTED_REASONS_BUTTON = $("[class='auto-reject-suggestion-button']");
    public static final SelenideElement COMPONENT_LABEL = $("[class='scrollContent'] [class='auto-reject-component-title']");
    public static final SelenideElement REJECT_REASON_LABEL = $("[class='scrollContent'] [class='auto-rejected-reason-text']");
    public static final SelenideElement REJECT_COMMENT_LABEL = $("[class='scrollContent'] [class='auto-reject-comment']");
    public static final SelenideElement APPLY_SUGGESTED_REASONS_BUTTON = $("[class='apply-suggested-rejection-reason'");
    public static final ElementsCollection ACTIVE_REJECTED_REASONS = $$("#reject-form input:checked ~ label");
    public static final ElementsCollection LIST_OF_FILLED_COMMENTS_FOR_REJECTION_REASONS = $$(".rejection-reasons .comment-text");
    public static final SelenideElement AMOUNT_AUTO_REJECT_REASONS_LABEL = $("[class='autoModeration']");
    public static final SelenideElement STOP_MODERATION = $("[class*='moderation-stop ']");
    public static final SelenideElement EDIT_PRODUCT_BUTTON = $("[class='offer_actions'] [class='fa fa-pencil']");
    public static final SelenideElement FILTER_BY_CERTIFICATE = $("[id='only_teasers_with_certificate']");
    public static final SelenideElement FILTER_BY_FEED_PROVIDER = $("[id='searchFeedProvider']");
    public static final SelenideElement FILTER_BY_AUTOAPPROVE = $("[id='recommended_to_approve']");
    public static final SelenideElement LINK_OFFER_BUTTON = $x("//*[text()='Link offers manually']");
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
    public static final SelenideElement IMAGE_POPUP_BUTTON = $("[class*='dialogImgEdit']");
    public static final SelenideElement IMAGE_POPUP_MANUAL_LOAD_BUTTON = $("[id*='imgQuarantine']");
    public static final SelenideElement IMAGE_BLOCK_TEASER_LIST = $("[id*='img']");
    public static final SelenideElement CLOUDINARY_IMAGE_FIELD = $("#cloudinaryImageFile");
    public static final SelenideElement CLOUDINARY_IMAGE_INPUT = $("#cloudinaryImageLink");
    public static final SelenideElement ADVERT_NAME_ICON = $("img[src*='advertiser_name']");
    public static final ElementsCollection MAIN_SCREENSHOT_BLOCK = $$("[id='plagiarismContainer'] tr td:nth-child(1) a");
    public static final ElementsCollection SECONDARY_SCREENSHOT_BLOCK = $$("[id='plagiarismContainer'] tr td:nth-child(3) a");
    public static final ElementsCollection PERCENTAGE_BLOCK = $$("[id='plagiarismContainer'] [class*='match']");
    public static final SelenideElement PLAGIARISM_ICON = $("[src*='plagiarism'][src*='svg']");
    public static final SelenideElement PLAGIARISM_POPUP = $("[class*='popup-plagiarism-landing-check']");

    //TAGS CLOUD
    public static final SelenideElement TAGS_FORM_SEARCH = $(".searchTags");
    public static final SelenideElement TAGS_FORM_SAVE = $("#btnTagsSave");
    public static final SelenideElement TAGS_DELETE_BUTTON = $("#btnTagsTeaserDelete");
    public static final SelenideElement TAGS_SAVE_BUTTON = $("#btnTagsTeaserSave");
    public static final SelenideElement SEARCH_TAGS_INPUT = $("[class='searchTags']");
    public static final ElementsCollection DISPLAYED_TITLE_TAGS = $$("[class='tagsBlock'] [data-type='title']:not([style*='none'])");
    public static final SelenideElement TITLE_TAGS_SECTION = $("[class='titleBlockTitle']");

    ///////////////////////////Mass Actions ///////////////////////////////////////////////////////////////////////////
    //General
    public static final SelenideElement SELECT_ALL_CHECKBOXES_LINK = $("#checkAllGoods");
    public static final SelenideElement MASS_ACTIONS_SELECT = $("#doaction");
    public static final SelenideElement MASS_ACTION_SUBMIT = $("#doactionbutton");
    public static final SelenideElement LANGUAGE_ICON = $("[class='errorLanguages']");
    public static final ElementsCollection TEASERS_ID_LOCATOR = $$("[id*='t_id_']:not([class='hidden'])");
    public static final SelenideElement COPYRIGHT_ICON = $("img[src*='copyright.png']");


    //Landing(landingchange)
    public static final SelenideElement MASS_LANDING_SELECT = $("#landing_types");

    ////////////////////////// InLine ///////////////////////////////////////////////////////////////////////////////
    //Title
    public static final SelenideElement INLINE_TITLE_LABEL = $("[class*='teaser-data'] div[id*=title]");
    public static final SelenideElement INLINE_TITLE_INPUT = $("td>div>textarea[id*=tmp_title_]");
    public static final SelenideElement TEASERS_TITLE = $("[class*='teaser-title']");
    public static final SelenideElement TITLE_ERROR_ICON = $(".teaser-title[id*='title']+*+img[src*='error-16.png']");

    //Title-Zion
    public static final SelenideElement INLINE_TITLE_ZION_LABEL = $("[id*=zion_title]");
    public static final SelenideElement INLINE_TITLE_ZION_INPUT = $("[id*=zion_tmp_title_]");

    //Image-Zion
    public static final SelenideElement VALID_FORMATS_ONLY = $("[id*='validFormatsOnlyZion']");
    public static final ElementsCollection TAGS_ELEMENTS_LIST = $$("[id*='tagsDialog'] [class='tag  active']");

    public static final SelenideElement IMAGE_BLOCK_VIEW = $("[id*='zion-popup__imgBig']");
    public static final SelenideElement IMAGE_CONTAINER = $("[id='imagePreviewHolder']");
    public static final ElementsCollection IMAGES_CONTAINER = $$("[class='zion-popup-manual-image-preview']");
    public static final SelenideElement MEDIA_CONTAINER = $("[class='zion-popup-cloudinary-image-check-image']");

    //Landing
    public static final SelenideElement INLINE_LANDING_LABEL = $(".landing_type_settings");
    public static final SelenideElement INLINE_LANDING_SELECT = $("#js-landingTypesselect");
    public static final SelenideElement INLINE_LANDING_SUBMIT_BUTTON = $("#landingTypes-settings-form [value='Submit']");

    //description
    public static final SelenideElement INLINE_DESCRIPTION_LABEL = $(".editableSpan[id*='advert_text']");
    public static final SelenideElement DESCRIPTION_INPUT = $("[id*='tmp_advert_text_']");
    public static final SelenideElement TEASERS_DESCRIPTION = $("[class*='teaser-description']");
    public static final SelenideElement DESCRIPTION_ERROR_ICON = $(".editableSpan[id*='advert_text']+*+img[src*='error-16.png']");

    //call to action
    public static final SelenideElement INLINE_CALL_TO_ACTION_LABEL = $("[class*='editable'][id*='call_to_action']");
    public static final SelenideElement CALL_TO_ACTION_INPUT = $("[id*='tmp_call_to_action']");

    //others
    public static final SelenideElement CATEGORY_LABEL = $("[id*='id_category']");
    public static final SelenideElement FEEDS_PROVIDER_INLINE = $("[id*='campaign_searchFeedProvider_']");
    public static final SelenideElement FEEDS_PROVIDER_INPUT = $("[name*='campaign_searchFeedProvider']");
    public static final ElementsCollection ACTIVE_TAGS = $$("[class='active']");

    // reject
    public static final SelenideElement REJECT_ICON = $("[id*='reject-teaser-']");
    public static final SelenideElement REJECT_FORM = $("#reject-form");
    public static final SelenideElement REJECT_FORM_SUBMIT_BUTTON = $("#submitReject");
    public static final SelenideElement REJECT_REASON_BUTTON_MASS = $("#select-reasons");

    //dfp
    public static final SelenideElement ICON_GOOGLE_ADD_MANAGER_ENABLED = $x(".//img[contains(@src, 'ghits_dfp_flag_on.svg')]");
    public static final SelenideElement ICON_GOOGLE_ADD_MANAGER_DISABLED = $x(".//img[contains(@src, 'ghits_dfp_flag_off.svg')]");

    // approve
    public static final SelenideElement APPROVE_ICON = $("[id*='approve-teaser-']");
    public static final SelenideElement CONTINUE_WITHOUT_LINKING = $x("//*[text()='Continue without linking']");
    public static final SelenideElement CONTINUE_APPROVE_REJECT_BUTTON = $x("//*[text()='Apply with approve']");
    public static final SelenideElement AUTO_APPROVE_LABEL = $("[class*='moderation-start'] [class='autoModeration']");
    public static final ElementsCollection AUTO_APPROVE_LABELS = $$("[class*='moderation-start'] [class='autoModeration']");

    // product
    public static final SelenideElement UPLOAD_CERTIFICATES_BUTTON = $("[id='fileUploader']");

    //lp check
    public static final SelenideElement DOWNLOAD_CERTIFICATE_BUTTON = $("[class*='fa-download']");
    public static final ElementsCollection DOWNLOAD_CERTIFICATE_BUTTONS = $$("[class*='fa-download']");

    //getty
    public static final SelenideElement STOCK_GETTY_IMAGES_TAB = $("[class='ghits-tabs'] [data-tab='stock']");
    public static final SelenideElement VIDEO_POPUP_BUTTON = $("[class='img-cloudinary'] video");
    public static final ElementsCollection GETTY_IMAGES = $$("[class*='stock-gallery-elements-item-el']");
    public static final ElementsCollection GETTY_IMAGES_PHRASES_DELETE_BUTTON = $$("[class='stock-gallery-search-item-remove-btn']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_PHRASE = $("[class='stock-gallery-search-item-text']");
    public static final SelenideElement GETTY_IMAGES_SEARCH_INPUT = $("[class='stock-gallery-image-search']");
    public static final SelenideElement SAVE_IMAGE_BUTTON = $("[id='saveImgBtn']");

    //Teaser info
    public static final String ADVERTISER_NAME_FIELD = "//textarea[@id='tmp_campaign_showname_%s']/preceding-sibling::span[1]";
    public static final String ADVERTISER_NAME_INPUT = "//textarea[@id='tmp_campaign_showname_%s']";
    public static final String ADVERTISER_NAME_TEMP_FIELD = "#campaign_showname_%s";
    public static final SelenideElement MIRRORING_IMAGES_LABEL = $("[id*='teaserMirrorImage']");

    //focal point
    public static final SelenideElement MANUAL_FOCAL_POINT_BUTTON = $("[id='manual-control']");
    public static final SelenideElement FOCAL_POINT_ICON = $("[id='crosshair']");
    public static final SelenideElement MANUAL_FOCAL_CONFIRM_BUTTON = $("[id='manual-control-confirm']");
    public static final SelenideElement MANUAL_FOCAL_DEFAULT_BUTTON = $("[id='manual-control-default']");

}
