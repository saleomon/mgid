package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class AudiencesListLocators {
    public static final SelenideElement ADD_AUDIENCE_BUTTON = $("input[value='Add audience']");
    public static final String AUDIENCE_DELETE_ICON = "[data-audience_id='%s'].delete-audience";
    public static final ElementsCollection AUDIENCE_ID_LOCATORS = $$x(".//tr[@class='main_table']/td[@class='a-center'][1]");
}
