package pages.cab.products.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class CreativeRequestsListLocators {

    // Columns
    // Information
    public static final SelenideElement REQUEST_ID = $x(".//div[span[text()='Request ID:']]/span[@class='value']");
    public static final SelenideElement PRIORITY_FIELD = $x(".//div[span[@class='key' and text()='Priority:']]/span[@class='value']");
    public static final ElementsCollection MANAGER_LABEL = $$x(".//div[span[text()='Manager:']]/*[@class='value']");
    public static final SelenideElement POPUP_WITH_ASSIGNED_INFORMATION = $("[class*='ui-dialog ui-widget'][style*='block']");
    public static final ElementsCollection ASSIGNED_IMAGES_POPUP = $$("[style*='block'] [id='task-images-list'] img[src]");

    // AdTypes
    public static final String AD_TYPE_INPUT = ".teaser_ad_type_val";
    public static final SelenideElement PG_INPUT = $("[data-type=pg]");
    public static final SelenideElement PG13_INPUT = $("[data-type=pg13]");
    public static final SelenideElement R_INPUT = $("[data-type=r]");
    public static final SelenideElement NC17_INPUT = $("[data-type=nc17]");
    public static final SelenideElement NSFW_INPUT = $("[data-type=nsfw]");

    // Actions
    public static final SelenideElement EDIT_REQUEST_ICON = $(".creative-editing");
    public static final SelenideElement ACCEPT_REQUEST_ICON = $("[id*=delDiv] img");
    public static final SelenideElement GO_TO_TASKS_ICON = $("[id*=z_icon_]");
    public static final SelenideElement CLOSE_REQUEST_ICON = $("[id*=stopimg][src*=stop]");
    public static final SelenideElement LOADING_ICON = $x("//img[contains(@src, 'loading.gif')]");
    public static final SelenideElement ADD_PRODUCT_ICON = $("[class='add-teaser-offer-btn']");
    public static final SelenideElement ASSIGNED_IMAGES_ICON = $("img[src*='stock_task-assigned.png']");

    // Website
    public static final SelenideElement WEB_SITES_FIELD = $(".sites");

    // Conditions
    public static final SelenideElement TEASER_REQUEST_LABELS_BLOCK = $(".col-cond div");

    // Amount
    public static final SelenideElement AMOUNT_LABEL = $(".total_amount");

    // CPC
    public static final SelenideElement PRICE_LABEL = $("[class*='creativeOrderStatus']>td:nth-child(5)");

    // Status
    public static final SelenideElement STATUS_LABEL = $("[id*=rstatus]");

    // Edit request popup
    public static final SelenideElement EDIT_REQUEST_POPUP = $("#edit_block");
    public static final SelenideElement EDIT_REQUEST_CPC_FOR_ALL_REGIONS_FIELD = $("#priceAll");
    public static final SelenideElement EDIT_REQUEST_PRIORITY_SELECT = $x(".//dd[@id='priority-element']/select");
    public static final SelenideElement SUBMIT_REQUEST_BUTTON = $("#doPostBtn");
    public static final SelenideElement ADD_IMAGES_EDIT_POPUP_BUTTON = $("[id='addImage']");
    public static final SelenideElement IMAGE_INPUT = $("[id='imageFile']");
    public static final SelenideElement CLOSE_POPUP_EDIT_REQUEST_BUTTON = $("[class*='closethick']");
    public static final ElementsCollection DELETE_IMAGE_BUTTONS = $$("[class='image-item__del']");
    public static final ElementsCollection IMAGE_CONTAINERS = $$("[class='add-image__container'] img");
    public static final SelenideElement IMAGE_COMMENT_FIELD = $("[id*='image-comment']");
    public static final SelenideElement IMAGE_LINK_FIELD = $("[id='imageLink']");

    // Additional rows
    public static final SelenideElement REQUEST_CLOSING_REASON_INPUT = $("[id*=campaign-block-reason-]");
    public static final SelenideElement REJECT_REQUEST_BUTTON = $x(".//span[text()='Reject request']");

    // Filter
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");
}