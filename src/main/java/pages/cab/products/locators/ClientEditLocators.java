package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ClientEditLocators {

    public static final SelenideElement EMAIL_CLIENT_INPUT = $("#main_email");
    public static final SelenideElement CLIENT_IS_ACTIVE_CHECKBOX = $("#status");
    public static final SelenideElement ALLOWED_MANUAL_PUSH_CHECKBOX = $("#can_see_manual_push");
    public static final SelenideElement ALLOWED_CLIENT_TO_DELETE_TEASERS_CHECKBOX = $("#can_drop_ghits");
    public static final SelenideElement TOTAL_EXPENSES_STAT_CHECKBOX = $("#show_summ_expenses");
    public static final SelenideElement SUBMIT_BUTTON = $("[id='submit']");

    //settings
    public static final SelenideElement CAN_CREATE_PUSH_CAMPAIGN_CHECKBOX = $("#can_create_push_campaign");
    public static final SelenideElement VERIFIED_CLIENT_CHECKBOX = $("#is_verified_by_manager");
    public static final SelenideElement USE_CSR_CHECKBOX = $("#use_creative_safety_ranking");
    public static final SelenideElement MIRRORING_IMAGE_CHECKBOX = $("[id='allowTeaserImageMirroring']");
}
