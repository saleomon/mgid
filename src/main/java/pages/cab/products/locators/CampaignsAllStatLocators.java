package pages.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class CampaignsAllStatLocators {
    public static final SelenideElement COUNTED_CLICKS_TOTAL_CELL = $x("(.//tr[@id='totalRow']/td[count(//th[.='Counted clicks']/preceding-sibling::th)])[1]");
}
