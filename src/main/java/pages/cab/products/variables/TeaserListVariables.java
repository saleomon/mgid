package pages.cab.products.variables;

public class TeaserListVariables {
    public static String titleTeaserIsBlockedByClient = "Teaser blocked by client";
    public static String titleTeaserIsBlockedByAccountManager = "Teaser blocked by account manager";
    public static String titleTeaserIsBlockedBySystem = "Teaser blocked by system";
    public static String titleTeaserIsBlockedByModerator = "Teaser blocked by moderator";
}
