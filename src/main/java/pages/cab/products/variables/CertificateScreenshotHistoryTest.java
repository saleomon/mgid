package pages.cab.products.variables;

public class CertificateScreenshotHistoryTest {
    public static String messageForModerator = "Good day,\n" +
            "                    In the campaign %s (Worldwide) for Teaser %s, there were changes to the files confirming the copyright for the landing page.\n" +
            "                    http://local-admin.mgid.com/cab/goodhits/ghits-screenshots-history/id/%s\n" +
            "                ";
    public static String messageForModeratorKyiv = "Добрый день,\n" +
            "                    В кампании %s (Мир) для тизеров %s, было изменения файлов подтверждающих наличие копирайта у лендинга.\n" +
            "                    http://local-admin.mgid.com/cab/goodhits/ghits-screenshots-history/id/%s";
}
