package pages.cab.products.variables;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CampaignsVariables {
    public static final String changeCampaignTypeAlertText = "When changing the ad campaign type from Content to Products type, the chosen category in Product type campaign will be set for all existing teasers within the ad campaign. The CPC will be automatically increased to the minimum allowed in this particular ad campaign type, teasers will be blocked.";
    public static final String messageRemoveCertificate = "Certificate file removed successfully";
    public static final String cantEditCampaignMessage = "Access to the required section denied";
    public static final String messageUkraineNotRotated = "Teasers of the campaign (%s) with a language other than Ukrainian will not be rotated in Ukraine. \n" +
            "According to Article 32 of the Law of Ukraine No. 2704-VIII \"On securing the function of the Ukrainian state movement\".";
    public static final String subjectUkraineNotRotated = "Teasers of the campaign with a language other than Ukrainian will not be shown in Ukraine";
    public static final String messageRotateUa = "Teasers of the campaign with a language other than Ukrainian will not be rotated in Ukraine. According to article 32 of the Law of Ukraine No. 2704-VIII \"On the safety of the function of the Ukrainian language and state\".";

    public static Map<String, Object> clonedCampaignSettingsAll = Stream.of(new Object[][]{
            {"showName", "For widgets"},
            {"isWhite", true},
            {"onlySameLanguageWidgets", true},
            {"copySchedule", true},
            {"copyAdLivetime", "30"},
            {"limits", "budget_limits"},
            {"geoTargeting", "112"},
            {"browserTargeting", "19"},
            {"osTargeting", "tablet"},
            {"languageTargeting", "22"},
            {"connectionTypeTargeting", "2"},
            {"utmCustom", "mgclida=sub_id&sub_id={click_id}"},
            {"bundleTargeting", "11"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1]));

    public static Map<String, String> campaignConversionsSettings = Stream.of(new Object[][]{
            {"conversionSettingsInterest", "1"},
            {"conversionSettingsDesire", "2"},
            {"conversionSettingsAuction", "3"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));



    public static Map<String, Object> clonedCampaignSettings = Stream.of(new Object[][]{
//   toDo AIA after https://jira.mgid.com/browse/VID-4288        {"limits", "budget_limits"},
            {"geoTargeting", "112"},
            {"browserTargeting", "19"},
            {"osTargeting", "tablet"},
            {"languageTargeting", "22"},
            {"utmCustom", "mgclida=sub_id&sub_id={click_id}"},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1]));

    public static Map<String, Object> clonedCampaignSettingsViewability = Stream.of(new Object[][]{
            {"mediaSource", "1"},
            {"viewabilityTargeting", "9"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1]));

    public static List<String> widgetsCampaignFilter = Arrays.asList(
            "testsitemg.com [1](1)",
            "testsitemg.com [2](2)",
            "testsitemg.com [3](3)",
            "testsitemg.com [4](4)",
            "testsitemg.com [5](5)",
            "testsitemg.com [6](6)",
            "testsiteadsk.com [7](7)",
            "testsiteadsk.com [8](8)",
            "testsiteadsk.com [9](9)",
            "testsiteadsk.com [10](10)",
            "testsiteadsk.com [11](11)",
            "newdomainzcb.com [12](19)");

    public static Map<String, String> widgetsAndFactorForPublisherEfficiency = Stream.of(new Object[][]{
            {"1", "0.10"},
            {"2", "1.00"},
            {"3", "2.30"},
            {"4", "2.20"},
            {"5", "0.20"},
            {"6", "0.30"},
            {"7", "0.40"},
            {"8", "0.50"},
            {"9", "0.60"},
            {"10", "0.70"},
            {"11", "0.80"},
            {"12", "10.00"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    public static Map<String, String> eventUrls = Stream.of(new Object[][]{
            {"impression", "https://video.test/vast/?event=impression"},
            {"first_quartile", "https://video.test/vasr/?event=quartile1"},
            {"midpoint", "https://vast.test/event/?event=midpoint"},
            {"third_quartile", "https://video.test/vasr/?event=quartile3"},
            {"complete", "https://video.test/vasr/?event=complete"},
            {"click", "https://video.test/vasr/?event=click"},
            {"skip", "https://video.test/vasr/?event=skip"}
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

}
