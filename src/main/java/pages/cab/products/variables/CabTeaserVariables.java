package pages.cab.products.variables;

public class CabTeaserVariables {
    public final static String error_offer_missing            = "Ad can't be approved, as offer on page landing is not defined. Please add offer using green\n" +
            "                    plus icon on LP check tab.";

    public static final String messageMissingZpl              = "Results of copying ads \n" +
            " The ad you're trying to copy is not ZPL compliant (%s)";
    public static final String messageMissingRac              = "Results of copying ads \n" +
            " The ad you're trying to copy is not RAC compliant (%s)";
    public static final String messageMissingAutocompliant    = "Results of copying ads \n" +
            " The ad you're trying to copy is not Autocontrol compliant (%s)";
    public static final String messageMissingIap              = "Results of copying ads \n" +
            " The ad you're trying to copy is not IAP compliant (%s)";
    public static final String messageMissingPoland              = "Results of copying ads \n" +
            " The ad you're trying to copy is not Poland Reg compliant (%s)";
    public static String languageIconTooltip                  = "The title language does not match the campaign language";
    public static String imagesLinkFromRequest                = "http://local.s3.eu-central-1.amazonaws.com/";


}
