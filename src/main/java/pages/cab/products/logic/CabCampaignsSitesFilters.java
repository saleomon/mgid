package pages.cab.products.logic;

import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;


import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.products.locators.CampaignsSitesFiltersLocators.ADD_WEBSITE_INPUT;
import static pages.cab.products.locators.CampaignsSitesFiltersLocators.SUBMIT_BUTTON;

public class CabCampaignsSitesFilters {

    private final HelpersInit helpersInit;
    private final Logger log;

    public CabCampaignsSitesFilters(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void addWebsiteAndSaveSettings(String websiteDomain) {
        clearAndSetValue (ADD_WEBSITE_INPUT, websiteDomain);
        SUBMIT_BUTTON.click();
        checkErrors();
        waitForAjax();
    }
}
