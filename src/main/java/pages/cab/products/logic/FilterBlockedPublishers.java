package pages.cab.products.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.FilterBlockedPublishersHelper;
import core.helpers.AuthorizationHelper;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.products.locators.FilterBlockedPublishersLocators.COEFFICIENT_CAB_LABEL;
import static pages.cab.products.locators.FilterBlockedPublishersLocators.EXPORT_BUTTON_CAB;

public class FilterBlockedPublishers {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final FilterBlockedPublishersHelper filterBlockedPublishersHelper;
    AuthorizationHelper authorizationHelper;


    public FilterBlockedPublishers(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        filterBlockedPublishersHelper = new FilterBlockedPublishersHelper(log, helpersInit);
        authorizationHelper = new AuthorizationHelper(log);
    }

    /**
     * load(import) publishers for Product Campaign
     */
    public boolean importPublishersForProductCampaign(String fileName) {
        filterBlockedPublishersHelper.chooseLoadFile(fileName);
        filterBlockedPublishersHelper.chooseActionWithLoadFile("add");
        filterBlockedPublishersHelper.saveForm();
        return filterBlockedPublishersHelper.isLoadFileSuccess();
    }

    public boolean isMarkedWidgetInFilter(String[] elements) {
        return filterBlockedPublishersHelper.isMarkedWidgetInFilter(elements);
    }

    public boolean checkCoefficientCab(String coef) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COEFFICIENT_CAB_LABEL.text().contains(coef));
    }

    /**
     * switch on widget in filter
     */
    public boolean switchOnWidgetInFilter(int widgetId) {
        return filterBlockedPublishersHelper.switchOnWidgetInFilter(String.valueOf(widgetId));
    }

    /**
     * get data checked widgets
     */
    public ArrayList<String> getDataCheckedWidgets(String... options) {

        if (Arrays.stream(options).anyMatch(i -> i.matches("all|filters"))) {
            filterBlockedPublishersHelper.openCheckedPublishers();
            log.info("getDataCheckedWidgets");
            return filterBlockedPublishersHelper.getDataCheckedWidgets();
        }
        return null;
    }

    public void chooseFileForImport(String fileName) {
        filterBlockedPublishersHelper.chooseLoadFile(fileName);
    }

    public void selectImportOptionsAndSave(String option) {
        filterBlockedPublishersHelper.chooseActionWithLoadFile(option);
        filterBlockedPublishersHelper.saveForm();
    }

    public void clickButtonCsvCab() {
        EXPORT_BUTTON_CAB.shouldBe(visible).click();
    }
}
