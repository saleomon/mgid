package pages.cab.products.logic;

import pages.cab.products.helpers.PublishersEfficiencyHelper;

import java.util.HashMap;

public class PublishersEfficiency {

    PublishersEfficiencyHelper publishersEfficiencyHelper;

    public PublishersEfficiency() {
        publishersEfficiencyHelper = new PublishersEfficiencyHelper();
    }

    /**
     * get list widgets ID and their Factor - to MAP
     */
    public HashMap<String, String> getWidgetsIdAndFactor() {
        return publishersEfficiencyHelper.getWidgetsIdAndFactor();
    }
}
