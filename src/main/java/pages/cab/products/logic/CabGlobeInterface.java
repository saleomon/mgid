package pages.cab.products.logic;

import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.products.locators.GlobeLocators.*;
import static core.helpers.BaseHelper.waitForAjax;

public class CabGlobeInterface {

    public CabGlobeInterface(){}

    public void selectAllGeoRegions(){
        CHECK_ALL_CHECKBOX.scrollTo().click();
    }

    public void setNewPriceOfClick(String value){
//        GEO_PRICE_INPUT.clear();
        GEO_PRICE_INPUT.sendKeys(value);
        GEO_PRICE_INPUT.sendKeys(Keys.ENTER);
    }

    public void clickSaveButtonForSetNewPrice(){
        SAVE_MASS_POC_BUTTON.shouldBe(visible).click();
        waitForAjax();
    }


}
