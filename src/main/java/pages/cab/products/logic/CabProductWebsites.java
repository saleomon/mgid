package pages.cab.products.logic;

import core.base.HelpersInit;
import pages.cab.products.helpers.WebsiteAddHelper;
import pages.cab.products.helpers.WebsiteListHelper;

import static pages.cab.products.locators.WebsitesListLocators.MANAGER_LABEL;


public class CabProductWebsites {
    private final HelpersInit helpersInit;
    private final WebsiteAddHelper websiteAddHelper;
    private final WebsiteListHelper websiteListHelper;
    String domain;
    String comment;
    boolean allowAddTeasersSubdomain = true;
    boolean allowAddDomainOtherPartner = true;
    String websiteId;


    public CabProductWebsites(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        websiteAddHelper = new WebsiteAddHelper(helpersInit);
        websiteListHelper = new WebsiteListHelper(helpersInit);
    }


    public CabProductWebsites setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public CabProductWebsites setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public CabProductWebsites setAllowAddTeasersSubdomain(boolean allowAddTeasersSubdomain) {
        this.allowAddTeasersSubdomain = allowAddTeasersSubdomain;
        return this;
    }

    public CabProductWebsites setAllowAddDomainOtherPartner(boolean allowAddDomainOtherPartner) {
        this.allowAddDomainOtherPartner = allowAddDomainOtherPartner;
        return this;
    }

    public String getWebsiteId() {
        return websiteId;
    }


    /**
     * create site goodhits
     */
    public void createWebsite() {
        domain = websiteAddHelper.fillDomain(domain);
        comment = websiteAddHelper.fillComment(comment);
        websiteAddHelper.allowAddingTeasersSubdomain(allowAddTeasersSubdomain);
        websiteAddHelper.allowAddingDomainToPartners(allowAddDomainOtherPartner);
        websiteAddHelper.saveSettings();
        getSiteIdFromListInterface();
    }

    /**
     * edit site goodhits
     */
    public void editWebsite() {
        comment = websiteAddHelper.fillComment(comment);
        websiteAddHelper.allowAddingTeasersSubdomain(allowAddTeasersSubdomain);
        websiteAddHelper.allowAddingDomainToPartners(allowAddDomainOtherPartner);
        websiteAddHelper.submitSettings();
    }

    /**
     * check website edit interfase
     */
    public boolean checkWebsiteEditInterface() {
        return websiteAddHelper.checkDomain(domain) &&
                websiteAddHelper.checkComment(comment) &&
                websiteAddHelper.checkAllowAddingTeasersSubdomain(allowAddTeasersSubdomain) &&
                websiteAddHelper.checkAllowAddingDomainToPartners(allowAddDomainOtherPartner);
    }

    /**
     * get site id
     */
    public void getSiteIdFromListInterface() {
        websiteId = websiteListHelper.getSiteIdFromListInterface();
    }

    /**
     * check displayed domain
     */
    public boolean checkDomainAtWebsiteListInterface() {
        return websiteListHelper.checkDomainAtWebsiteListInterface(domain);
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        websiteListHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }
}
