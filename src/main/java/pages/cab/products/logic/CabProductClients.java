package pages.cab.products.logic;

import com.codeborne.selenide.Condition;
import core.base.HelpersInit;
import pages.cab.crm.helper.CrmAddHelper;
import pages.cab.products.helpers.ClientEditHelper;
import pages.cab.products.helpers.ClientListHelper;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.products.locators.ClientEditLocators.*;
import static pages.cab.products.locators.ClientListLocators.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_FILES;

public class CabProductClients {

    private final HelpersInit helpersInit;
    private final ClientListHelper clientListHelper;
    private final ClientEditHelper clientEditHelper;

    private String clientId;
    private String login;
    private String email;
    private CrmAddHelper.PaymentTerms paymentTerms;
    private HashMap<String, String> paymentTermsOptions;

    public CabProductClients(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        clientListHelper = new ClientListHelper(helpersInit);
        clientEditHelper = new ClientEditHelper(helpersInit);
    }

    public CabProductClients setEmail(String email) {
        this.email = email;
        return this;
    }

    public CabProductClients setLogin(String login) {
        this.login = login;
        return this;
    }

    public String getClientId(){
        return clientId;
    }

    /**
     * check work 'Currency' filter
     */
    public boolean checkWorkCurrencyFilter(){
        String currency = clientListHelper.chooseFilterCurrency();
        clientListHelper.clickFilterButton();
        return helpersInit.getBaseHelper().checkDatasetEquals(currency, clientListHelper.getCurrencyType());
    }

    /**
     * check work 'Low priority' filter
     */
    public String filterClientsByLowPriority(){
        String currency = clientListHelper.filterClientsByLowPriority();
        clientListHelper.clickFilterButton();
        return currency;
    }

    /**
     * check work 'Login' filter
     */
    public void filterClientsByLogin(String login) {
        helpersInit.getBaseHelper().setAttributeValueJS(FILTER_BY_LOGIN_INPUT, login);
        clientListHelper.clickFilterButton();
    }

    /**
     * get amount of clients at list
     */
    public int getAmountClients() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CLIENT_IDS_LABEL.size());
    }

    /**
     * get client login
     */
    public String getClientLogin() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LOGIN_CLIENT_LABEL.text());
    }

    /**
     * is displayed 'Low priority' filter
     */
    public boolean isDisplayedLowPriorityFilter(){
        return FILTER_LOW_PRIORITY_SELECT.isDisplayed();
    }

    /**
     * check work 'Low priority' filter
     */
    public int getAmountDisplayedClients(){
        return CLIENT_IDS_LABEL.size();
    }

    public boolean checkPresenceCampaignAtListInterface(String name) {
        return clientListHelper.checkPresenceCampaignAtListInterface(name);
    }

    /**
     * check client RA in list interface
     */
    public boolean checkClientRaInListInterface(){
        return clientListHelper.checkClientLoginInList(email) &&
                clientListHelper.checkClientEmailInList(email);
    }

    /**
     * check client RA in edit interface
     */
    public boolean checkClientRaInEditInterface(){
        boolean clientIsActive = true;
        boolean allowedManualPush = false;
        boolean allowedClientsToDeleteTeasers = true;
        boolean totalExpensesStat = true;
        return clientEditHelper.checkClientEmailInEdit(email) &&
                clientEditHelper.checkIsActiveClient(clientIsActive) &&
                clientEditHelper.checkIsAllowedManualPush(allowedManualPush) &&
                clientEditHelper.checkAllowedClientToDeleteTeasers(allowedClientsToDeleteTeasers) &&
                clientEditHelper.checkTotalExpensesStats(totalExpensesStat);

    }

    /**
     * get clientId
     */
    public String readClientId(){
        clientId = clientListHelper.getClientId();
        return clientId;
    }

    public void resetClientsPassword(int clientId){
        markUnMarkCheckbox(true, $("[id='changePassword_" + clientId + "']"));
    }

    public void saveClientsSettings(){
        clientEditHelper.saveClientsSettings();
    }

    public boolean checkCanCreatePushCampaignSettingsCheckboxIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CAN_CREATE_PUSH_CAMPAIGN_CHECKBOX.isDisplayed());
    }

    public boolean checkStateOfCheckboxForCreatePushCampaignSettings() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                CAN_CREATE_PUSH_CAMPAIGN_CHECKBOX.shouldBe(visible).isSelected());
    }

    public CabProductClients setCanCreatePushCampaign(boolean canCreatePushCampaign) {
        CAN_CREATE_PUSH_CAMPAIGN_CHECKBOX.setSelected(canCreatePushCampaign);
        return this;
    }

    public boolean checkVerifiedClientSettingsCheckboxIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VERIFIED_CLIENT_CHECKBOX.isDisplayed());
    }

    public boolean checkStateOfVerifiedClientSettingsCheckbox() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                VERIFIED_CLIENT_CHECKBOX.shouldBe(visible).isSelected());
    }

    public CabProductClients setVerifiedClient(boolean verifiedClientState) {
        VERIFIED_CLIENT_CHECKBOX.setSelected(verifiedClientState);
        helpersInit.getBaseHelper().checkAlertAndClose();
        return this;
    }

    public boolean isDisplayVerifiedIcon() {
        return COPYRIGHT_ICON.isDisplayed();
    }

    public boolean isDisplayCopyrightIcon() {
        return COPYRIGHT_WITH_LINK_ICON.isDisplayed();
    }

    public String getLinkCopyrightIcon() {
        return LINK_COPYRIGHT_ICON.attr("href");
    }

    public void changeStateAllowRotateRuDomainFlag() {
        ALLOW_ROTATE_RUSSIAN_DOMAINS.click();
        waitForAjax();
    }
    public boolean checkStateAllowRotateRuDomainFlag(String state) {
        return $("img[src*='allowRussianDomains" + state + ".svg']").isDisplayed();
    }

    public boolean checkUseCsrIsDisplayed() {
        return USE_CSR_CHECKBOX.isDisplayed();
    }

    public void setStateMirroringImagesCheckbox(boolean state) {
         markUnMarkCheckbox(state, MIRRORING_IMAGE_CHECKBOX);
    }
    public boolean checkStateOfMirroringImagesCheckbox(boolean state) {
         return checkIsCheckboxSelected(state, MIRRORING_IMAGE_CHECKBOX);
    }

    public boolean isDisplayedMirroringImageCheckbox() {
         return MIRRORING_IMAGE_CHECKBOX.isDisplayed();
    }

    public void clickPaymentTermsIcon(){
        PAYMENT_TERMS_ICON.click();
        waitForAjax();
        checkErrors();
    }

    public void deletePaymentTermsFile(){
        PAYMENT_TERMS_FILE_REMOVE_BUTTON.click();
        waitForAjax();
    }

    public void saveSettingsOfPaymentTermsPopup(){
        SAVE_PAYMENT_TERMS_BUTTON_POPUP.click();
        waitForAjax();
    }

    public CrmAddHelper.PaymentTerms choosePaymentTerms(CrmAddHelper.PaymentTerms paymentMethod) {
        if(paymentMethod == null) return null;
        helpersInit.getBaseHelper().selectCustomText(PAYMENT_TERMS_SELECT, paymentMethod.getPaymentTermsValue());
        return paymentMethod;
    }

    public String setPaymentTermsNet(String...val) {
        String randomNumber = val.length == 0 ? randomNumberFromRange(1, 10) : val[0];
        PAYMENT_TERMS_NET_INPUT.clear();
        PAYMENT_TERMS_NET_INPUT.sendKeys(randomNumber);
        return randomNumber;
    }

    public String setPaymentTermsFile(String...fileName) {
        String file = fileName.length == 0 ? "certif_pdf.pdf" : fileName[0];
        PAYMENT_TERMS_FILE_UPLOAD.sendKeys(LINK_TO_RESOURCES_FILES + file);
        PAYMENT_TERMS_FILE_REMOVE_BUTTON.shouldBe(Condition.visible);
        waitForAjax();
        return file;
    }

    public boolean checkPaymentTerms(CrmAddHelper.PaymentTerms paymentTerms, Map<String, String> paymentTermsOptions) {
        return PAYMENT_TERMS_SELECT.text().equalsIgnoreCase(paymentTerms.getPaymentTermsValue()) &&
                helpersInit.getBaseHelper().checkDatasetContains(PAYMENT_TERMS_FILE_NAME.text(), paymentTermsOptions.get("file").split(".pdf")[0]) &&
                switch (paymentTerms) {
                    case POST_PAYMENT, MIXED_PAYMENT -> helpersInit.getBaseHelper().checkDatasetEquals(PAYMENT_TERMS_NET_INPUT.val(), paymentTermsOptions.get("net"));
                    default -> true;
                };
    }

    public boolean isDisplayedPaymentTermsIcon(){
        return PAYMENT_TERMS_ICON.isDisplayed();
    }
}
