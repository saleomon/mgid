package pages.cab.products.logic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.CreativeRequestAddHelper;
import pages.cab.products.helpers.CreativeRequestsListHelper;

import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static pages.cab.products.locators.CreativeRequestAddLocators.ADD_COPY_CONDITIONS_BUTTON;
import static pages.cab.products.locators.CreativeRequestsListLocators.*;
import static core.helpers.BaseHelper.clearAndSetValue;

public class CabCreativeRequests {
    private final HelpersInit helpersInit;
    private final Logger log;
    private final CreativeRequestAddHelper creativeRequestAddHelper;
    private final CreativeRequestsListHelper creativeRequestListHelper;

    private String requestId;
    private String previewLink;
    private String legalApprove;
    private String targetAudience;
    private String offerReview;
    private String teasersAmount;
    private String clickPrice;
    private String priority;
    private String website;
    private String commentForTeasermakers;
    private String commentForModerators;
    private String publishersDetails;
    private String imagesCommentText;
    private int amountOfImages;
    private Boolean notifyManagerCheckbox;
    private Map imageInformation;

    public CabCreativeRequests(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        this.creativeRequestAddHelper = new CreativeRequestAddHelper(log, helpersInit);
        this.creativeRequestListHelper = new CreativeRequestsListHelper(log, helpersInit);
    }

    public String getRequestId() {
        return requestId;
    }

    public String getPriority() {
        return priority;
    }

    public String getTeasersAmount() {
        return teasersAmount;
    }

    public CabCreativeRequests setAmountOfImages(int amountOfImages) {
        this.amountOfImages = amountOfImages;
        return this;
    }

    public void setImagesCommentText(String imagesCommentText) {
        this.imagesCommentText = imagesCommentText;
    }

    public CabCreativeRequests setTeasersAmount(String teasersAmount) {
        this.teasersAmount = teasersAmount;
        return this;
    }

    public void createTeaserCreativeRequest() {
        website = creativeRequestAddHelper.selectWebsite(website);
        creativeRequestAddHelper.fillLinkForAds(website);
        setCommonSettingsForTeaserRequest(false);
        creativeRequestAddHelper.loadImageForRequest(amountOfImages);
        imageInformation = creativeRequestAddHelper.fillImageInformation(amountOfImages);
        creativeRequestAddHelper.submitForm();
        requestId = creativeRequestAddHelper.readRequestId();
        log.info("Creating teaser request - success!");
    }

    /**
     * Edit teaser request
     */
    public void editTeaserRequest() {
        setCommonSettingsForTeaserRequest(true);
        creativeRequestAddHelper.loadImageForRequest(amountOfImages);
        imageInformation = creativeRequestAddHelper.fillImageInformation(amountOfImages);
        creativeRequestAddHelper.submitRequest();
        log.info("Editing teaser request - success!");
    }

    public void editImageInformationAndSaveSettings() {
        imageInformation = creativeRequestAddHelper.fillImageInformation(amountOfImages, imagesCommentText);
        creativeRequestAddHelper.submitRequest();
    }

    public void openPopupEditRequest() {
        creativeRequestAddHelper.clickEditRequestIcon();
    }

    public void editTeaserPrice() {
        creativeRequestAddHelper.fillPrice(clickPrice, true);
    }

    public void deleteImageFromRequest(int indexElement) {
        DELETE_IMAGE_BUTTONS.get(indexElement).hover().click();
        helpersInit.getBaseHelper().getTextFromAlert();
    }

    public void closePopupEditRequest() {
        CLOSE_POPUP_EDIT_REQUEST_BUTTON.click();
    }

    public boolean checkAttachedImagesInformation() {
        return helpersInit.getBaseHelper().checkDataset(IMAGE_CONTAINERS.size(), amountOfImages) &&
                imageInformation.keySet().stream().allMatch( elem-> helpersInit.getBaseHelper().checkDatasetEquals(imageInformation.get(elem).toString(), Objects.requireNonNull($("[id='image-" + elem + "']").val())));
    }

    private void setCommonSettingsForTeaserRequest(boolean isEditRequest) {
        previewLink = creativeRequestAddHelper.fillPreviewLink(previewLink);
        legalApprove = creativeRequestAddHelper.fillLegalApprove(legalApprove);
        targetAudience = creativeRequestAddHelper.fillTargetedAudience(targetAudience);
        offerReview = creativeRequestAddHelper.fillOfferReview(offerReview);
        commentForTeasermakers = creativeRequestAddHelper.fillCommentForTeasermakers(commentForTeasermakers);
        publishersDetails = creativeRequestAddHelper.fillPublishersDetails(publishersDetails);
        commentForModerators = creativeRequestAddHelper.fillCommentForModerators(commentForModerators);
        teasersAmount = creativeRequestAddHelper.fillTeasersAmount(teasersAmount);
        clickPrice = creativeRequestAddHelper.fillPrice(clickPrice, isEditRequest);
        priority = creativeRequestAddHelper.choosePriority(priority, isEditRequest);
        creativeRequestAddHelper.switchNotifyManagerCheckbox(notifyManagerCheckbox);
        creativeRequestAddHelper.selectNotifyNativeEditor();
    }

    /**
     * Check teaser request
     */
    public boolean checkTeaserRequest() {
        log.info("checkTeaserRequest");
        return creativeRequestListHelper.checkWebSiteInListInterface(website) &&
                creativeRequestListHelper.checkLinkForAds(website) &&
                creativeRequestListHelper.checkPreviewLink(previewLink) &&
                creativeRequestListHelper.checkLegalApprove(legalApprove) &&
                creativeRequestListHelper.checkTargetedAudience(targetAudience) &&
                creativeRequestListHelper.checkOfferReview(offerReview) &&
                creativeRequestListHelper.checkCommentForTeasermakers(commentForTeasermakers) &&
                creativeRequestListHelper.checkCommentForModerators(commentForModerators) &&
                creativeRequestListHelper.checkAmount(teasersAmount) &&
                creativeRequestListHelper.checkPrice(clickPrice) &&
                creativeRequestListHelper.checkPriority(priority) &&
                creativeRequestListHelper.checkStatusRequest("On moderation");
    }

    /**
     * Approve teaser request
     */
    public boolean approveTeaserRequest() {
        return creativeRequestListHelper.approveTeaserRequest();
    }

    public void clickApproveIcon() {
        creativeRequestListHelper.clickApproveIcon();
    }

    /**
     * Delete teaser creative request
     */
    public boolean deleteTeaserRequest() {
        log.info("Delete teaser creative request");
        return creativeRequestListHelper.deleteTeaserRequest();
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        creativeRequestListHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }

    /**
     * open form add product
     */
    public void openFormAddProduct() {
        creativeRequestListHelper.openFormAddProduct();
    }

    /**
     * check visibility of icon add product
     */
    public boolean checkVisibilityAddProductIcon() {
        return creativeRequestListHelper.checkVisibilityAddProductIcon();
    }

    public boolean isDisplayedAdTypeInputs() {
        return creativeRequestListHelper.isDisplayedAdTypeInputs();
    }

    public void validationAdType(String value) {
        ElementsCollection adTypesInput = $$(AD_TYPE_INPUT);
        helpersInit.getBaseHelper().getRandomCountValue(adTypesInput.size(), 2)
                .forEach(i -> adTypesInput.get(i).val(value));
    }

    public void setPgType(String value) {
        creativeRequestListHelper.setPgType(value);
    }

    public void setPg13Type(String value) {
        creativeRequestListHelper.setPg13Type(value);
    }

    public void setRType(String value) {
        creativeRequestListHelper.setRType(value);
    }

    public void setNc17Type(String value) {
        creativeRequestListHelper.setNc17Type(value);
    }

    public void setNsfwType(String value) {
        creativeRequestListHelper.setNsfwType(value);
    }

    public void openFormCopyTeaser() {
        ADD_COPY_CONDITIONS_BUTTON.click();
    }

    public void loadImagesForRequest() {
        creativeRequestAddHelper.loadImageForRequest(amountOfImages);
    }

    public String  checkValueOfImageCommentField() {
        return IMAGE_COMMENT_FIELD.getValue();
    }

    public void fillImageLinkField(String val) {
        clearAndSetValue(IMAGE_LINK_FIELD, val);
        ADD_IMAGES_EDIT_POPUP_BUTTON.click();
    }

    public void openPopupWithAssignedImages() {
        ASSIGNED_IMAGES_ICON.scrollTo().click();
        POPUP_WITH_ASSIGNED_INFORMATION.shouldBe(Condition.visible);
    }

    public int checkAmountOfDisplayedImages() {
        return ASSIGNED_IMAGES_POPUP.size();
    }

    public boolean isDisplayedIconAssignedImages() {
        return ASSIGNED_IMAGES_ICON.isDisplayed();
    }

    /**
     * Method fill settings for copying teaser to the campaigns
     */
    public void copyTeaserByMassActionsToOneCampaign(int campaignId) {
        creativeRequestAddHelper.setCpcForCopyAllCampaigns("15");
        creativeRequestAddHelper.fillCampaignIds(campaignId);
        creativeRequestAddHelper.clickCopyTeaserToCampaigns();
    }

    public void prepareTeaserCreativeRequestWithCopyingToCampaign(int campaignId) {
        website = creativeRequestAddHelper.selectWebsite(website);
        creativeRequestAddHelper.fillLinkForAds(website);
        setCommonSettingsForTeaserRequest(false);
        creativeRequestAddHelper.loadImageForRequest(amountOfImages);
        imageInformation = creativeRequestAddHelper.fillImageInformation(amountOfImages);
        openFormCopyTeaser();
        copyTeaserByMassActionsToOneCampaign(campaignId);
        creativeRequestAddHelper.submitForm();
    }
}