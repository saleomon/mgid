package pages.cab.products.logic;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.CreativeHelper;
import core.helpers.BaseHelper;

import java.time.Duration;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static pages.cab.products.locators.CreativeLocators.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabCreative {
    private final Logger log;
    private final CreativeHelper creativeHelper;
    private final HelpersInit helpersInit;

    public CabCreative(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        this.creativeHelper = new CreativeHelper(log, helpersInit);
    }

    /**
     * check base settings in creative interface after create Creative requests
     */
    public boolean checkRequestsForCreative(String requestId, String priority, String amount) {
        log.info("check_requests_for_creative");
        return creativeHelper.checkRequestId(requestId) &&
                creativeHelper.checkPriority(priority) &&
                creativeHelper.checkAmount(amount);
    }

    /**
     * Prepare creative for create teaser and get link for teaser creation interface
     */
    public String goToCreateTeaser() {
        return creativeHelper.getLinkToAddTeaserForm();
    }

    public boolean isDisplayedIconCreateTeaser() {
        return ADD_TEASER_ICON.isDisplayed();
    }

    /**
     * Prepare creative for create teaser and get link for teaser creation interface
     */
    public void assumeTask(String amountTask, String... customAdType) {
        creativeHelper.assumeTask(amountTask, customAdType);
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        creativeHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }

    public String getCountTeasers(String type) {
        String[] mass = $x("//label[contains(.,'" + type + " :')]").parent().text().split("\n");

        for (String v : mass) {
            if (v.contains("Needed " + type.toUpperCase()))
                return v.substring(v.indexOf("of") + 3);
        }
        return null;
    }

    /**
     * check custom adType in taking tasks
     */
    public boolean checkAdTypeInTakingTasks(String type) {
        return creativeHelper.checkAdTypeInTakingTasks(type);
    }

    public String getAmountOfDrafts() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AMOUNT_OF_DRAFTS.text());
    }

    public String getLinkToDrafts() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AMOUNT_OF_DRAFTS.attr("href"));
    }

    public void deleteTask() {
        DELETE_TASK_BUTTON.scrollTo().click();
        REJECT_REASON_INPUT.sendKeys(BaseHelper.getRandomWord(10));
        CANCEL_TASK_BUTTON.click();
        DELETE_TASK_BUTTON.shouldBe(Condition.hidden);
        DELETE_TASK_BUTTON.shouldBe(Condition.hidden);
        checkErrors();
    }

    public boolean isDisplayedRejectIcon() {
        REJECTED_TASK_ICON.shouldBe(Condition.visible);
        return true;
    }

    public void openGivenTaskPopup() {
        OPEN_MANAGE_TASK_POPUP.click();
        checkErrors();
    }

    public void addTeaserMakerByIcon(String teaserMaker) {
        $("[title='" + teaserMaker + "'] a").click();
        checkErrors();
    }

    public void chooseTeaserAmountAndAdType(String teaserMaker, String amount, String...adType) {
        $("[title='" + teaserMaker + "'] [class='teasers_amount']").sendKeys(amount);
        if(adType.length == 0) {
            helpersInit.getBaseHelper().selectRandomValue($("[title='" + teaserMaker + "'] [class='teasers_type']"));
        } else $("[title='" + teaserMaker + "'] [class='teasers_type']").selectOptionByValue(adType[0]);
        checkErrors();
    }

    public void chooseImagesForTask(String teaserMaker, String...images) {
        openEditRequest(teaserMaker);
        ENABLED_IMAGES.asFixedIterable().forEach(SelenideElement::click);

        Arrays.asList(images).forEach(el->$("[src*='" + el + "']").click());
        APPLY_EDIT_REQUEST_BUTTON.click();
        checkErrors();
    }

    public void openEditRequest(String teaserMaker) {
        $x("//li[text()='" + teaserMaker + "']").doubleClick();
        checkErrors();
    }

    public void saveGiveTaskSettings() {
        SAVE_GIVE_TASK_POPUP_BUTTON.click();
        SAVE_GIVE_TASK_POPUP_BUTTON.shouldBe(Condition.hidden, Duration.ofSeconds(6000));
        checkErrors();
    }

    public int getAmountOfDisplayedMarkedImages() {
        return ENABLED_IMAGES.size();
    }

    public int getAmountOfRequestDataComments() {
        return COMMENTS_LABEL.size();
    }

    public int getAmountOfRequestDataLinks() {
        return LINKS_LABEL.size();
    }
}