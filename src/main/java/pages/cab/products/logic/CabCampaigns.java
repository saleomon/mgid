package pages.cab.products.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.CampaignListHelper;
import pages.cab.products.helpers.CampaignsAddHelper;
import testData.project.TargetingType;

import java.util.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.CampaignsAddLocators.*;
import static pages.cab.products.locators.CampaignsListLocators.*;
import static pages.cab.products.locators.LandingOffersCreateEditLocators.UPLOAD_CERTIFICATES_BUTTON;
import static pages.cab.products.variables.CampaignsVariables.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CabCampaigns {
    private HelpersInit helpersInit;
    private final Logger log;
    private final CampaignListHelper campaignsListHelper;
    private final CampaignsAddHelper campaignsAddHelper;

    public CabCampaigns(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        campaignsListHelper = new CampaignListHelper(log, helpersInit);
        campaignsAddHelper = new CampaignsAddHelper(log, helpersInit);
        this.helpersInit = helpersInit;
    }

    String campaignType;
    String campaignSubType;
    String campaignPaymentModel = "cpm";
    String campaignMarketingObjective;
    String campaignMediaSource;
    String campaignTier;
    String mirror;
    String certificate;
    boolean markCampaignAsWhite = false;
    String campaignName;
    String feedProvider;
    String campaignId;
    String campaignNameWidgetDisplaying;
    String campaignKeyword;
    String campaignCategory;
    String campaignLanguage;
    boolean blockTeaserAfterCreation = false;
    boolean isShowsOnlyForThisLanguage = false;
    boolean blockedByManager = false;
    boolean rotateAdskeeperSubnet = true;
    String acf;
    String showProbabilityMax;
    boolean includeReportQuota = false;
    String badSitesQuotas;
    boolean setValue = false;
    boolean dontIncludeQualityFactor = false;
    boolean dontSendCampaignToGeoEdge = true;
    String teaserImpressionFrequency;
    boolean dontShowAllTeasersAfterClick = false;
    boolean dontShowAllTeasersAfterConversion = false;
    String dontShowAllTeasersAfterConversionPeriod;
    boolean limitTotalImpressionFrequency;
    String limitTotalImpressionFrequencyValue;
    String limitTotalImpressionFrequencyValuePeriod;
    String impresionLimit;
    String impresionLimiPeriod;
    boolean blockAccordingSchedule = false;
    String adEndDate;
    String limitsOption;
    String dayOption;
    String generalOption;
    List<String> limits;
    boolean unlimitedState = false;
    boolean ignoreMinPrice = false;
    public Map<String, String> ignoreMinPriceMap;
    String adLifetime;
    String percentageOfDisplay;
    String auctionType;
    String auctionDspValue;
    String dsp;
    String sendSiteValue;
    String cooperationType;
    String nativeVersion;
    String showProbabilityDsp;
    Map<String, String> percentageOfRegions;
    boolean useTargeting;
    boolean useDsp = true;
    boolean useSendSiteInfo = false;
    boolean useCooperationSite = false;
    boolean useNativeVersion = false;
    boolean onlyMathedVisitors = false;
    JsonObject targetingElements = new JsonObject();
    String[] targetSections;
    boolean isEnabledCustomTag = true;
    String utmType;
    Map<String, String> utms;
    boolean utmTaggingGa = true;
    Conversion useConversion;
    public Map<String, String> sensorsOfCustomConversion = new HashMap<>();
    boolean useAutooptimization = false;
    boolean isLoadIconForPush = false;
    boolean isCustomTaggingSwitchOn;
    String filterType;
    String cappingTriggerEvent;
    String frequencyCapping;
    String cappingLimit;
    String vastUrl;
    String vastPrice;
    Map<String, String> eventUrls;
    boolean eventUrlsIsEnabled;

    public String getCampaignLanguage() {
        return campaignLanguage;
    }

    /**
     * CompliantType for campaign types
     */
    public enum CompliantType {
        RAC("13"),
        ZPL("16"),
        IAP("22"),
        AUTOCOMPLIANT("18"),
        ARPP("20"),
        CONAR("45"),
        NBTC("54"),
        POLAND_REG_COMPLIANT ("19");

        private final String compliantTypeValue;

        CompliantType(String targetTypeValue) {
            this.compliantTypeValue = targetTypeValue;
        }

        public String getCompliantType() {
            return compliantTypeValue;
        }

    }

    /**
     * Subtypes for Video campaigns
     */
    public enum VideoCampaignSubTypes {
        VAST("VAST"),
        PREBID("Prebid"),
        DIRECT_ADS("directAds");

        private final String videoCampaignSubTypeValue;

        VideoCampaignSubTypes(String subTypeValue) {
            this.videoCampaignSubTypeValue = subTypeValue;
        }

        public String getVideoCampaignSubTypeValue() {
            return videoCampaignSubTypeValue;
        }
    }


    public String getCampaignName() {
        return campaignName;
    }

    public String getShowProbabilityDsp() {
        return showProbabilityDsp;
    }

    public CabCampaigns setEventUrls(Map<String, String> eventUrls, boolean isEnabled) {
        this.eventUrls = eventUrls;
        this.eventUrlsIsEnabled = isEnabled;
        return this;
    }

    public CabCampaigns setShowsOnlyForThisLanguage(boolean isEnabled) {
        this.isShowsOnlyForThisLanguage = isEnabled;
        return this;
    }

    public CabCampaigns setConversion(Conversion conversion) {
        this.useConversion = conversion;
        return this;
    }

    public CabCampaigns setCertificate(String certificate) {
        this.certificate = certificate;
        return this;
    }

    public CabCampaigns setCampaignKeyword(String campaignKeyword) {
        this.campaignKeyword = campaignKeyword;
        return this;
    }

    public CabCampaigns setFeedsProvider(String feedProvider) {
        this.feedProvider = feedProvider;
        return this;
    }

    public CabCampaigns setCappingLimit(String cappingLimit) {
        this.cappingLimit = cappingLimit;
        return this;
    }

    public CabCampaigns setUtmTaggingGa(boolean utmTaggingGa) {
        this.utmTaggingGa = utmTaggingGa;
        return this;
    }

    public CabCampaigns setIsEnabledCustomTag(boolean isEnabledCustomTag) {
        this.isEnabledCustomTag = isEnabledCustomTag;
        return this;
    }

    public CabCampaigns setVastPrice(String vastPrice) {
        this.vastPrice = vastPrice;
        return this;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }

    public CabCampaigns setUnlimitedState(boolean unlimitedState) {
        this.unlimitedState = unlimitedState;
        return this;
    }

    public void setCustomTaggingSwitchOn(boolean customTaggingSwitchOn) {
        isCustomTaggingSwitchOn = customTaggingSwitchOn;
    }

    public CabCampaigns setTargetSections(String... targetSections) {
        this.targetSections = targetSections;
        return this;
    }

    public CabCampaigns setUseTargeting(boolean useTargeting) {
        this.useTargeting = useTargeting;
        return this;
    }

    public CabCampaigns setCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    public CabCampaigns setCampaignSubType(VideoCampaignSubTypes campaignSubType) {
        this.campaignSubType = campaignSubType.videoCampaignSubTypeValue;
        return this;
    }

    public CabCampaigns setCampaignPaymentModel(String campaignPaymentModel) {
        this.campaignPaymentModel = campaignPaymentModel;
        return this;
    }

    public CabCampaigns setCampaignTier(String campaignTier) {
        this.campaignTier = campaignTier;
        return this;
    }

    public CabCampaigns setAuctionType(String auctionType) {
        this.auctionType = auctionType;
        return this;
    }

    public CabCampaigns setUseSendSiteInfo(boolean useSendSiteInfo) {
        this.useSendSiteInfo = useSendSiteInfo;
        return this;
    }

    public CabCampaigns setUseNativeVersion(boolean useNativeVersion) {
        this.useNativeVersion = useNativeVersion;
        return this;
    }

    public CabCampaigns setOnlyMathedVisitors(boolean onlyMathedVisitors) {
        this.onlyMathedVisitors = onlyMathedVisitors;
        return this;
    }


    public CabCampaigns setCampaignName(String campaignName) {
        this.campaignName = campaignName;
        return this;
    }

    public CabCampaigns setUtmType(String utmType) {
        this.utmType = utmType;
        return this;
    }

    public CabCampaigns setCampaignCategory(String campaignCategory) {
        this.campaignCategory = campaignCategory;
        return this;
    }

    public CabCampaigns setCampaignLanguage(String campaignLanguage) {
        this.campaignLanguage = campaignLanguage;
        return this;
    }

    public CabCampaigns setBlockTeaserAfterCreation(boolean blockTeaserAfterCreation) {
        this.blockTeaserAfterCreation = blockTeaserAfterCreation;
        return this;
    }

    public CabCampaigns setBlockedByManager(boolean blockedByManager) {
        this.blockedByManager = blockedByManager;
        return this;
    }

    public CabCampaigns setDontSendCampaignToGeoEdge(boolean dontSendCampaignToGeoEdge) {
        this.dontSendCampaignToGeoEdge = dontSendCampaignToGeoEdge;
        return this;
    }

    public CabCampaigns setLimitsOption(String adLimitsBudget, String dayOption, String generalOption) {
        this.limitsOption = adLimitsBudget;
        this.dayOption = dayOption;
        this.generalOption = generalOption;
        return this;
    }

    public CabCampaigns setIgnoreMinPrice(boolean ignoreMinPrice) {
        this.ignoreMinPrice = ignoreMinPrice;
        return this;
    }

    public CabCampaigns setTargetingElements(JsonObject targetingElements) {
        this.targetingElements = targetingElements;
        return this;
    }

    public CabCampaigns setCampaignMarketingObjective(String campaignMarketingObjective) {
        this.campaignMarketingObjective = campaignMarketingObjective;
        return this;
    }

    public CabCampaigns setCampaignMediaSource(String campaignMediaSource) {
        this.campaignMediaSource = campaignMediaSource;
        return this;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public boolean checkVisibilitySybTypeIcon(String subType) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($x(String.format(CAMPAIGN_SUBTYPE, subType)).exists());
    }

    public CabCampaigns setFrequencyCapping(String frequencyCapping) {
        this.frequencyCapping = frequencyCapping;
        return this;
    }

    /**
     * set state for IsGenerateNewValue for all options
     */
    public CabCampaigns setIsGenerateNewValue(boolean state) {
        campaignsAddHelper.setGenerateNewValues(state);
        return this;
    }

    /**
     * set state for IsGenerateNewValue for all options
     */
    public void setTargetCounter(int amountOfTargets) {
        campaignsAddHelper.setTargetCounter(amountOfTargets);
    }

    /**
     * create campaign
     */
    public void createCampaign() {
        campaignsAddHelper.selectType(campaignType);
        campaignsAddHelper.selectMarketingObjective(campaignMarketingObjective);
        campaignsAddHelper.selectMediaSource(campaignMediaSource);
        campaignTier = campaignsAddHelper.selectTier(campaignTier);
        campaignsAddHelper.markAsWhite(markCampaignAsWhite);
        mirror = campaignsAddHelper.selectMirror(mirror);
        uploadCertificates(certificate);
        campaignName = campaignsAddHelper.fillName(campaignName);
        feedProvider = campaignsAddHelper.fillFeedsProvider(feedProvider);
        campaignNameWidgetDisplaying = campaignsAddHelper.fillNameDisplayedWidgets(campaignNameWidgetDisplaying);
        campaignKeyword = campaignsAddHelper.fillCampaignKeyword(campaignKeyword);
        campaignCategory = campaignsAddHelper.selectCategory(campaignCategory);
        campaignLanguage = campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.blockByManager(blockedByManager);
        campaignsAddHelper.rotateAdskeeperSubnet(rotateAdskeeperSubnet);
        acf = campaignsAddHelper.fillAcf(acf);
        showProbabilityMax = campaignsAddHelper.fillShowProbabilityMax(showProbabilityMax);
        campaignsAddHelper.setValue(setValue);
        campaignsAddHelper.dontIncludeQualityFactor(dontIncludeQualityFactor);
        campaignsAddHelper.dontSendToGeoEdge(dontSendCampaignToGeoEdge);
        campaignsAddHelper.addIconForPush(isLoadIconForPush, LINK_TO_RESOURCES_IMAGES + "dracaena-cinnabari.jpg");
        teaserImpressionFrequency = campaignsAddHelper.fillTeaserImpressionFrequently(teaserImpressionFrequency);
        campaignsAddHelper.dontShowTeaserAfterClick(dontShowAllTeasersAfterClick);
        dontShowAllTeasersAfterConversionPeriod = campaignsAddHelper.dontShowTeaserAfterConversion(dontShowAllTeasersAfterConversion, dontShowAllTeasersAfterConversionPeriod);
        campaignsAddHelper.limitImpressionsFrequently(limitTotalImpressionFrequency);
        limitTotalImpressionFrequencyValue = campaignsAddHelper.fillImpressionLimit(limitTotalImpressionFrequency);
        limitTotalImpressionFrequencyValuePeriod = campaignsAddHelper.fillImpressionLimitPeriod(limitTotalImpressionFrequency);
        campaignsAddHelper.blockingAccordingSchedule(blockAccordingSchedule);
//        adStartDay = campaignsAddHelper.fillAdStartDate(adStartDay);
//        adEndDate = campaignsAddHelper.fillAdEndDate(adEndDate);
        adLifetime = campaignsAddHelper.selectAdLifeTime(adLifetime);
        limits = campaignsAddHelper.setLimits(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.useAutoOptimization(useAutooptimization);
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        percentageOfRegions = campaignsAddHelper.fillShowPercent(percentageOfDisplay);
        auctionDspValue = campaignsAddHelper.auctionTypeDsp(auctionType);
        sendSiteValue = campaignsAddHelper.selectSendSiteInfo(useSendSiteInfo);
        nativeVersion = campaignsAddHelper.selectNativeVersion(useNativeVersion);
        campaignsAddHelper.onlyMatchetVisitors(onlyMathedVisitors);
        ignoreMinPriceMap = campaignsAddHelper.ignoreMinPrice(ignoreMinPrice);
        utms = campaignsAddHelper.useUtmTagging(utmType);
        sensorsOfCustomConversion = campaignsAddHelper.useConversion(useConversion);
        campaignsAddHelper.saveSettings();
    }

    public void createDspCampaign() {
        campaignsAddHelper.selectType(campaignType);
        campaignTier = campaignsAddHelper.selectTier(campaignTier);
        campaignsAddHelper.markAsWhite(markCampaignAsWhite);
        mirror = campaignsAddHelper.selectMirror(mirror);
        campaignName = campaignsAddHelper.fillName(campaignName);
        campaignNameWidgetDisplaying = campaignsAddHelper.fillNameDisplayedWidgets(campaignNameWidgetDisplaying);
        campaignCategory = campaignsAddHelper.selectCategory(campaignCategory);
        campaignLanguage = campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.blockByManager(blockedByManager);
        campaignsAddHelper.rotateAdskeeperSubnet(rotateAdskeeperSubnet);
        acf = campaignsAddHelper.fillAcf(acf);
        showProbabilityMax = campaignsAddHelper.fillShowProbabilityMax(showProbabilityMax);
//        campaignsAddHelper.dontSendToGeoEdge(dontSendCampaignToGeoEdge);
        campaignsAddHelper.addIconForPush(isLoadIconForPush, LINK_TO_RESOURCES_IMAGES + "dracaena-cinnabari.jpg");
        teaserImpressionFrequency = campaignsAddHelper.fillTeaserImpressionFrequently(teaserImpressionFrequency);
        limitTotalImpressionFrequencyValue = campaignsAddHelper.fillImpressionLimit(limitTotalImpressionFrequency);
        limitTotalImpressionFrequencyValuePeriod = campaignsAddHelper.fillImpressionLimitPeriod(limitTotalImpressionFrequency);
        campaignsAddHelper.blockingAccordingSchedule(blockAccordingSchedule);
//        adStartDay = campaignsAddHelper.fillAdStartDate(adStartDay);
//        adEndDate = campaignsAddHelper.fillAdEndDate(adEndDate);
        adLifetime = campaignsAddHelper.selectAdLifeTime(adLifetime);
        limits = campaignsAddHelper.setLimits(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.useAutoOptimization(useAutooptimization);
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        percentageOfRegions = campaignsAddHelper.fillShowPercent(percentageOfDisplay);
        dsp = campaignsAddHelper.chooseDsp(useDsp);
        auctionDspValue = campaignsAddHelper.auctionTypeDsp(auctionType);
        sendSiteValue = campaignsAddHelper.selectSendSiteInfo(useSendSiteInfo);
        cooperationType = campaignsAddHelper.selectCooperationType(useCooperationSite);
        showProbabilityDsp = campaignsAddHelper.fillShowProbabilityDsp(showProbabilityDsp);
        campaignsAddHelper.onlyMatchetVisitors(onlyMathedVisitors);
        ignoreMinPriceMap = campaignsAddHelper.ignoreMinPrice(ignoreMinPrice);
        utms = campaignsAddHelper.useUtmTagging(utmType);
        sensorsOfCustomConversion = campaignsAddHelper.useConversion(useConversion);
        campaignsAddHelper.saveSettings();
    }

    /**
     * edit campaign
     */
    public void editCampaign() {
        campaignsAddHelper.selectType(campaignType);
        campaignsAddHelper.selectTier(campaignTier);
        campaignsAddHelper.markAsWhite(markCampaignAsWhite);
        campaignsAddHelper.selectMirror(mirror);
        campaignsAddHelper.fillName(campaignName);
        feedProvider = campaignsAddHelper.fillFeedsProvider(feedProvider);
        campaignKeyword = campaignsAddHelper.fillCampaignKeyword(campaignKeyword);
        campaignsAddHelper.fillNameDisplayedWidgets(campaignNameWidgetDisplaying);
        campaignsAddHelper.selectCategory(campaignCategory);
        deleteUploadedCertificates();
        uploadCertificates(certificate);
        campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.chooseShowsOnlyForThisLanguage(isShowsOnlyForThisLanguage);
        campaignsAddHelper.blockTeaserAfterCreation(blockTeaserAfterCreation);
        campaignsAddHelper.blockByManager(blockedByManager);
        campaignsAddHelper.rotateAdskeeperSubnet(rotateAdskeeperSubnet);
        campaignsAddHelper.fillAcf(acf);
        limits = campaignsAddHelper.setLimits(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.fillShowProbabilityMax(showProbabilityMax);
        campaignsAddHelper.setValue(setValue);
        campaignsAddHelper.addIconForPush(isLoadIconForPush, LINK_TO_RESOURCES_IMAGES + "dracaena-cinnabari.jpg");
        campaignsAddHelper.dontIncludeQualityFactor(dontIncludeQualityFactor);
        campaignsAddHelper.dontSendToGeoEdge(dontSendCampaignToGeoEdge);
        teaserImpressionFrequency = campaignsAddHelper.fillTeaserImpressionFrequently(teaserImpressionFrequency);
        campaignsAddHelper.dontShowTeaserAfterClick(dontShowAllTeasersAfterClick);
        campaignsAddHelper.dontShowTeaserAfterConversion(dontShowAllTeasersAfterConversion, dontShowAllTeasersAfterConversionPeriod);
        campaignsAddHelper.limitImpressionsFrequently(limitTotalImpressionFrequency);
        campaignsAddHelper.fillImpressionLimit(limitTotalImpressionFrequency);
        campaignsAddHelper.fillImpressionLimitPeriod(limitTotalImpressionFrequency);
        campaignsAddHelper.blockingAccordingSchedule(blockAccordingSchedule);
//        campaignsAddHelper.fillAdStartDate(adStartDay);
//        campaignsAddHelper.fillAdEndDate(adEndDate);
        campaignsAddHelper.selectAdLifeTime(adLifetime);
        campaignsAddHelper.useAutoOptimization(useAutooptimization);
        campaignsAddHelper.clearTargetingSettings(useTargeting);
        auctionDspValue = campaignsAddHelper.auctionTypeDsp(auctionType);
        sendSiteValue = campaignsAddHelper.selectSendSiteInfo(useSendSiteInfo);
        nativeVersion = campaignsAddHelper.selectNativeVersion(useNativeVersion);
        campaignsAddHelper.onlyMatchetVisitors(onlyMathedVisitors);
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        percentageOfRegions = campaignsAddHelper.fillShowPercent(percentageOfDisplay);
        ignoreMinPriceMap = campaignsAddHelper.ignoreMinPrice(ignoreMinPrice);
        utms = campaignsAddHelper.useUtmTagging(utmType);
        sensorsOfCustomConversion = campaignsAddHelper.useConversion(useConversion);
        campaignsAddHelper.saveSettings();
    }

    /**
     * edit campaign
     */
    public void editDspCampaign() {
        campaignsAddHelper.selectTier(campaignTier);
        campaignsAddHelper.markAsWhite(markCampaignAsWhite);
        campaignsAddHelper.selectMirror(mirror);
        campaignsAddHelper.fillName(campaignName);
        campaignsAddHelper.fillNameDisplayedWidgets(campaignNameWidgetDisplaying);
        campaignsAddHelper.selectCategory(campaignCategory);
        campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.blockByManager(blockedByManager);
        campaignsAddHelper.rotateAdskeeperSubnet(rotateAdskeeperSubnet);
        campaignsAddHelper.fillAcf(acf);
        limits = campaignsAddHelper.setLimits(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.fillShowProbabilityMax(showProbabilityMax);
        campaignsAddHelper.addIconForPush(isLoadIconForPush, LINK_TO_RESOURCES_IMAGES + "dracaena-cinnabari.jpg");
//        campaignsAddHelper.dontSendToGeoEdge(dontSendCampaignToGeoEdge);
        teaserImpressionFrequency = campaignsAddHelper.fillTeaserImpressionFrequently(teaserImpressionFrequency);
        campaignsAddHelper.fillImpressionLimit(limitTotalImpressionFrequency);
        campaignsAddHelper.fillImpressionLimitPeriod(limitTotalImpressionFrequency);
        campaignsAddHelper.blockingAccordingSchedule(blockAccordingSchedule);
//        campaignsAddHelper.fillAdStartDate(adStartDay);
//        campaignsAddHelper.fillAdEndDate(adEndDate);
        campaignsAddHelper.selectAdLifeTime(adLifetime);
        campaignsAddHelper.useAutoOptimization(useAutooptimization);
        campaignsAddHelper.clearTargetingSettings(useTargeting);
        auctionDspValue = campaignsAddHelper.auctionTypeDsp(auctionType);
        sendSiteValue = campaignsAddHelper.selectSendSiteInfo(useSendSiteInfo);
        campaignsAddHelper.onlyMatchetVisitors(onlyMathedVisitors);
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        percentageOfRegions = campaignsAddHelper.fillShowPercent(percentageOfDisplay);
        ignoreMinPriceMap = campaignsAddHelper.ignoreMinPrice(ignoreMinPrice);
        utms = campaignsAddHelper.useUtmTagging(utmType);
        sensorsOfCustomConversion = campaignsAddHelper.useConversion(useConversion);
        campaignsAddHelper.saveSettings();
    }

    /**
     * clear saved variables after create or edite campaign
     */
    public CabCampaigns clearSettings() {
        campaignType = null;
        campaignSubType = null;
        campaignPaymentModel = null;
        campaignTier = null;
        mirror = null;
        campaignName = null;
        campaignNameWidgetDisplaying = null;
        campaignCategory = null;
        campaignLanguage = null;
        acf = null;
        showProbabilityMax = null;
        badSitesQuotas = null;
        teaserImpressionFrequency = null;
        dontShowAllTeasersAfterConversionPeriod = null;
        impresionLimit = null;
        adEndDate = null;
        limits = null;
        ignoreMinPriceMap = null;
        adLifetime = null;
        targetSections = null;
        utmType = null;
        utms = null;
        sensorsOfCustomConversion = null;
        certificate = null;
        campaignKeyword = null;
        feedProvider = null;
        clearTargetingJson();
        frequencyCapping = null;
        cappingTriggerEvent = null;
        return this;
    }

    /**
     * clear data targeting json
     */
    public void clearTargetingJson() {
        if (targetingElements != null) {
            while (targetingElements.keySet().iterator().hasNext())
                targetingElements.remove(targetingElements.keySet().iterator().next());
        }
    }

    /**
     * check settings at edit interface
     */
    public boolean checkCampaignSettingsEditInterface() {
        return campaignsAddHelper.checkType(campaignType) &&
                campaignsAddHelper.checkMarketingObjective(campaignMarketingObjective) &&
                campaignsAddHelper.checkMediaSource(campaignMediaSource) &&
                campaignsAddHelper.checkTier(campaignTier) &&
                campaignsAddHelper.checkName(campaignName) &&
                campaignsAddHelper.checkDisplayedName(campaignNameWidgetDisplaying) &&
                campaignsAddHelper.checkFeedsProvider(feedProvider) &&
                campaignsAddHelper.checkCampaignKeyword(campaignKeyword) &&
                campaignsAddHelper.checkCategory(campaignCategory) &&
                campaignsAddHelper.checkLanguage(campaignLanguage) &&
                campaignsAddHelper.checkShowsOnlyForThisLanguage(isShowsOnlyForThisLanguage) &&
                campaignsAddHelper.checkMirror(mirror) &&
                campaignsAddHelper.checkShowProbabilityMax(showProbabilityMax) &&
                campaignsAddHelper.checkCountBadQuotas(badSitesQuotas) &&
//        campaignsAddHelper.checkFillTeaserImpression(teaserImpressionFrequency) &&
                campaignsAddHelper.checkImpressionLimit(impresionLimit) &&
                campaignsAddHelper.checkImpressionLimitPeriod(impresionLimiPeriod) &&
//        campaignsAddHelper.checkAdStartDate(adStartDay) &&
//        campaignsAddHelper.checkAdEndDate(adEndDate) &&
                campaignsAddHelper.checkIconForPush(isLoadIconForPush) &&
                campaignsAddHelper.checkLimits(limitsOption, limits) &&
                //
                campaignsAddHelper.checkTypeLimit(limitsOption) &&
                campaignsAddHelper.checkIgnoreMinPrice(ignoreMinPriceMap) &&
                campaignsAddHelper.checkAdLifeTime(adLifetime) &&
                campaignsAddHelper.checkShowPercent(percentageOfRegions) &&
                campaignsAddHelper.checkOnlyMatchetVisitors(onlyMathedVisitors) &&
                campaignsAddHelper.checkAuctionTypeDsp(auctionType, auctionDspValue) &&
                campaignsAddHelper.checkSelectedSendSiteInfo(sendSiteValue) &&
                campaignsAddHelper.checkUtmTags(utmTaggingGa, isEnabledCustomTag, utms) &&
                campaignsAddHelper.checkOurSensorsConversions(sensorsOfCustomConversion) &&
                campaignsAddHelper.checkMarkAsWhite(markCampaignAsWhite) &&
                campaignsAddHelper.checkBlockTeaserAfterCreation(blockTeaserAfterCreation) &&
                campaignsAddHelper.checkBlockByManager(blockedByManager) &&
                campaignsAddHelper.checkRotateAdskeeperSubnet(rotateAdskeeperSubnet) &&
                campaignsAddHelper.checkIncludingReportQuota(includeReportQuota) &&
                campaignsAddHelper.checkSetValue(setValue) &&
                campaignsAddHelper.checkDontIncludeQualityFactor(dontIncludeQualityFactor) &&
                campaignsAddHelper.checkDontSendToGeoEdge(dontSendCampaignToGeoEdge) &&
                campaignsAddHelper.checkDontShowTeaserAfterClick(dontShowAllTeasersAfterClick) &&
                campaignsAddHelper.checkDontShowTeaserAfterConversion(dontShowAllTeasersAfterConversion) &&
                campaignsAddHelper.checkLimitImpression(limitTotalImpressionFrequency) &&
                campaignsAddHelper.checkBlockAccordingSchedule(blockAccordingSchedule) &&
                campaignsAddHelper.checkSelectedTargeting(targetingElements) &&
                campaignsAddHelper.checkDisplayedCertificates(certificate);
    }

    public void createCampaignWithLimitsChanges() {
        campaignsAddHelper.selectType(campaignType);
        campaignName = campaignsAddHelper.fillName(campaignName);
        campaignsAddHelper.selectCategory(campaignCategory);
        limits = campaignsAddHelper.setLimits(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.saveSettings();
    }

    public void selectLocationTargetAndSaveSettings(String typeTarget, String... countries) {
        TARGETINGS_LEGEND.scrollTo();
        USE_TARGETING_RADIO.click();
        campaignsAddHelper.clearTargetingSettings(true);
        campaignsAddHelper.selectLocationTarget(typeTarget, countries);
        SAVE_BUTTON.click();
        waitForAjax();
        if (CONFIRM_BUTTON.isDisplayed()) {
            CONFIRM_BUTTON.click();
        }
        checkErrors();
    }

    public CabCampaigns selectLocationTarget(String typeTarget, String... countries) {
        TARGETINGS_LEGEND.scrollTo();
        USE_TARGETING_RADIO.click();
        campaignsAddHelper.clearTargetingSettings(true);
        campaignsAddHelper.selectLocationTarget(typeTarget, countries);
        return this;
    }

    /**
     * is displayed auto retargeting option
     */
    public boolean checkVisibilityOfAutoRetargetting() {
        return campaignsAddHelper.isDisplayedAutoRetargeting();
    }

    /**
     * check disable attribute for tier option
     */
    public boolean checkDisabledTierOption(String tier) {
        return campaignsAddHelper.checkDisabledTierOption(tier);
    }

    /**
     * check settings at list interface
     */
    public boolean checkCampaignSettingsListInterface() {
        return campaignsListHelper.checkCampaignNameListInterface(campaignName);

    }

    /**
     * get campaign id from list interface
     */
    public String getCampaignIdFromListInterface() {
        return campaignId = campaignsListHelper.getCampaignId();
    }

    public void saveVideoCampaign(VideoCampaignSubTypes subTypeValue) {
        setVideoCampaignBaseFields();
        switch (subTypeValue) {
            case VAST -> {
                vastUrl = campaignsAddHelper.setVastUrl();
                vastPrice = campaignsAddHelper.setVastPrice(vastPrice);
            }
            case DIRECT_ADS -> {
                eventUrlsIsEnabled = campaignsAddHelper.setEventUrls(eventUrls, eventUrlsIsEnabled);
                campaignsAddHelper.blockTeaserAfterCreation(blockTeaserAfterCreation);
            }
        }
        campaignsAddHelper.saveSettings();
    }

    /**
     * check base fields for video campaign
     */
    public boolean checkVideoCampaignBaseFields() {
        return campaignsAddHelper.checkType(campaignType) &&
                campaignsAddHelper.checkSubType(campaignSubType) &&
                campaignsAddHelper.checkName(campaignName) &&
                campaignsAddHelper.checkCategory(campaignCategory) &&
                campaignsAddHelper.checkLanguage(campaignLanguage) &&
                campaignsAddHelper.checkLimits(limitsOption, limits) &&
                campaignsAddHelper.checkSelectedTargeting(targetingElements) &&
                campaignsAddHelper.checkBlockByManager(blockedByManager) &&
                campaignsAddHelper.checkUtmTags(true, true, utms);
    }

    /**
     * check direct ads video campaign fields
     */
    public boolean checkVideoCampaignDirectAdsFields() {
        return checkVideoCampaignBaseFields() &&
                campaignsAddHelper.checkBlockTeaserAfterCreation(blockTeaserAfterCreation) &&
                campaignsAddHelper.checkEventUrls(eventUrls, eventUrlsIsEnabled);
    }

    /**
     * check if 'Campaign type' - is have custom value
     */
    public boolean isCampaignTypeHaveCustomValue(String value) {
        return campaignsAddHelper.isCampaignTypeHaveCustomValue(value);
    }

    /**
     * check can edit campaign - show Error message
     */
    public boolean canEditVideoCampaign(String errorMessage) {
        return helpersInit.getMessageHelper().checkCustomMessagesCab(errorMessage);
    }

    /**
     * check show select 'campaign type'
     */
    public boolean isShowCampaignTypeSelect() {
        return campaignsAddHelper.isShowCampaignTypeSelect();
    }

    /**
     * create CLONE campaign with copy all options
     */
    public void createCloneCompanyWithCopyAllOptions(boolean isCopyCategory, boolean isCopyLanguage, Map<String, Object> options, boolean isEnableOption) {
        campaignsListHelper.cloneIconClick();
        campaignName = campaignsListHelper.setCloneName();

        campaignsListHelper.setOptionsForClone(isCopyCategory, isCopyLanguage, options, isEnableOption);

        if (!isCopyCategory) campaignCategory = campaignsAddHelper.chooseCloneCategory();
        if (!isCopyLanguage) campaignLanguage = campaignsAddHelper.chooseCloneLanguage();

        campaignsAddHelper.saveClone();
    }

    /**
     * check settings CLONE campaign in LIST INTERFACE
     */
    public boolean checkCloneInListInterface(Map<String, Object> options) {
        int count = 0;
        getCampaignIdFromListInterface();
        log.info("Clone camp ID: " + campaignId);
        if (campaignsListHelper.checkCampaignNameInListInterface(campaignName)) {
            for (Map.Entry<String, Object> entry : options.entrySet()) {
                switch (entry.getKey()) {
                    case "copySchedule":
                        if (campaignsListHelper.isShowScheduleIcon((Boolean) entry.getValue())) count++;
                        break;
                    case "filters":
                        if (filterType.equals("2") ?
                                (campaignsListHelper.isFilterExceptSwitchOn() &&
                                        campaignsListHelper.isFiltersStopIconDisplayed()) :

                                (campaignsListHelper.isFilterOnlyEnabled() &&
                                        campaignsListHelper.isFiltersStopIconDisplayed())) count++;
                        break;
                    default:
                        count++;
                        break;
                }
            }
        }
        log.info("checkCloneInListInterface: " + count + " == " + options.size());
        return count == options.size();
    }

    /**
     * check settings CLONE campaign in EDIT INTERFACE
     */
    public boolean checkOptionsForCloneInEditInterface(Map<String, Object> options) {
        int count = 0;
        if (campaignsAddHelper.checkCategory(campaignCategory) &&
                campaignsAddHelper.checkLanguage(campaignLanguage)) {
            for (Map.Entry<String, Object> entry : options.entrySet()) {
                switch (entry.getKey()) {
                    case "showName":
                        if (campaignsAddHelper.checkDisplayedName(entry.getValue().toString())) count++;
                        break;
                    case "isWhite":
                        if (entry.getValue().equals(campaignsAddHelper.isWhiteCampaign())) count++;
                        break;
                    case "onlySameLanguageWidgets":
                        if (entry.getValue().equals(campaignsAddHelper.isOnlySameLanguageWidgets())) count++;
                        break;
                    case "copySchedule":
                        if (entry.getValue().equals(campaignsAddHelper.isScheduleSwitch())) count++;
                        break;
                    case "copyAdLivetime":
                        if (campaignsAddHelper.checkAdLifeTime(entry.getValue().toString())) count++;
                        break;
                    case "limits":
                        if (campaignsAddHelper.checkTypeLimit(entry.getValue().toString())) count++;
                        break;
                    case "geoTargeting":
                        if (campaignsAddHelper.checkLocationIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "browserTargeting":
                        if (campaignsAddHelper.checkBrowserIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "osTargeting":
                        if (campaignsAddHelper.checkOsIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "languageTargeting":
                        if (campaignsAddHelper.checkLanguageIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "connectionTypeTargeting":
                        if (campaignsAddHelper.checkConnectionTypeIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "conversionSettings":
                        if (campaignsAddHelper.checkAuctionStageConversion(campaignConversionsSettings.get("conversionSettingsAuction")) &&
                                campaignsAddHelper.checkInterestStageConversion(campaignConversionsSettings.get("conversionSettingsInterest")) &&
                                campaignsAddHelper.checkDesireStageConversion(campaignConversionsSettings.get("conversionSettingsAuction")))
                            count++;
                        break;
                    case "utmCustom":
                        if (isCustomTaggingSwitchOn == campaignsAddHelper.isCustomTaggingSwitchOn() &&
                                (!isCustomTaggingSwitchOn || campaignsAddHelper.checkUtmCustomTag(entry.getValue().toString())))
                            count++;
                        break;
                    case "bundleTargeting":
                        if (campaignsAddHelper.checkBundlesIdTargeting(entry.getValue().toString())) count++;
                        break;
                    case "mediaSource":
                        if (campaignsAddHelper.checkMediaSource(entry.getValue().toString())) count++;
                        break;
                    case "viewabilityTargeting":
                        if (campaignsAddHelper.checkViewabilityTargeting(entry.getValue().toString())) count++;
                        break;
                    default:
                        count++;
                        break;
                }
            }
        }
        log.info("checkOptionsForCloneInEditInterface: " + count + " == " + options.size());
        return count == options.size();
    }

    /**
     * conversion type
     */
    public enum Conversion {
        GOOGLE_ANALYTICS("Google Analytics"),
        OUR_SENSORS("Our sensors conversion");

        private final String contractType;

        Conversion(String statuses) {
            this.contractType = statuses;
        }

        public String getContractType() {
            return contractType;
        }
    }

    /**
     * switch on/off publisher filter
     */
    public boolean isFilterWidgetSwitchOn() {
        return campaignsListHelper.clearPublisherFilter();
    }

    /**
     * check state filter after set widget
     */
    public boolean isFilterWidgetShow(String filterId) {
        return campaignsListHelper.isFilterWidgetShow(filterId);
    }

    /**
     * check work 'Curator' filter
     * - expand all spans
     * - choose custom curator
     * - click filter
     * - check tasks by curator
     */
    public boolean checkWorkCuratorFilter(String curator) {
        helpersInit.getMultiFilterHelper().expandMultiFilterAndClickData("curatorsFilterTree", curator);
        campaignsListHelper.submitFilter();
        return helpersInit.getBaseHelper().getTextAndWriteLog(MANAGER_LABEL.size() > 0 &&
                MANAGER_LABEL.texts().stream().allMatch(i -> i.equals(curator)));
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon(String... regions) {
        return campaignsListHelper.checkDisplayingApacIcon() &&
                campaignsListHelper.checkHintApacIcon(regions);
    }

    public void fillFieldValidationForPushIcon(String typeCampaign, String filePath) {
        campaignsAddHelper.selectType(typeCampaign);
        campaignsAddHelper.addIconForPush(true, filePath);
    }

    /**
     * set base video fields
     */
    public CabCampaigns setVideoCampaignBaseFields() {
        campaignsAddHelper.selectType(campaignType);
        campaignsAddHelper.selectSubType(campaignSubType);
        campaignsAddHelper.selectPaymentModel(campaignPaymentModel);
        campaignName = campaignsAddHelper.fillName(campaignName);
        campaignCategory = campaignsAddHelper.selectCategory(campaignCategory);
        campaignLanguage = campaignsAddHelper.selectLanguage(campaignLanguage);
        cappingTriggerEvent = campaignsAddHelper.setCappingTriggerEvent(cappingTriggerEvent);
        frequencyCapping = campaignsAddHelper.selectFrequencyCapping(frequencyCapping);
        cappingLimit = campaignsAddHelper.setCappingLimit(cappingLimit, frequencyCapping);
        limits = campaignsAddHelper.setLimitsForVideoCampaign(limitsOption, dayOption, generalOption, unlimitedState);
        campaignsAddHelper.blockByManager(blockedByManager);
        campaignsAddHelper.clearTargetingSettings(true);
        adEndDate = campaignsAddHelper.fillAdEndDate(adEndDate);
        utms = campaignsAddHelper.useUtmTagging(utmType);
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        return this;
    }

    /**
     * check base fields for VAST/VPAID campaign
     */
    public boolean checkVideoCampaignVastFields() {
        return checkVideoCampaignBaseFields() &&
                campaignsAddHelper.checkCappingTriggerEvent(cappingTriggerEvent) &&
                campaignsAddHelper.checkFrequencyCapping(frequencyCapping) &&
                campaignsAddHelper.checkCappingLimit(cappingLimit) &&
                campaignsAddHelper.checkVastUrl(vastUrl) &&
                campaignsAddHelper.checkVastPrice(vastPrice);
    }

    /**
     * click filter button
     */
    public void pressFilterButton() {
        campaignsListHelper.submitFilter();
    }

    public String getTotalAmountOfCampaigns() {
        return getRegExValue(TOTAL_AMOUNT_OF_DISPLAYED_ENTITIES.text(), "[0-9][0-9]");
    }

    /**
     * is visible search feed icon
     */
    public boolean checkVisibilityOfSearchFeedIcon() {
        return campaignsListHelper.checkVisibilityOfSearchFeedIcon();
    }

    /**
     * unblock campaign
     */
    public void unBlockCampaign() {
        UNBLOCK_CAMPAIGN_ICON.click();

        waitForAjax();
        checkErrors();
    }

    /**
     * check popup message
     */
    public boolean checkPopupMessage(String message) {
        return helpersInit.getBaseHelper().checkDatasetEquals(message, IAP_COMPLIANT_POPUP_TEXT.text());
    }

    /**
     * click read button
     */
    public void clickReadButton() {
        READ_POPUP_BUTTON.click();
        checkErrors();
    }

    /**
     * check visibility unblock icon
     */
    public boolean checkVisibilityUnblockIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(UNBLOCK_CAMPAIGN_ICON.isDisplayed());
    }

    /**
     * select targetting
     */
    public void selectTargeting() {
        targetingElements = campaignsAddHelper.selectTargeting("all");
    }

    public CabCampaigns selectPublisherCategoryTargeting() {
        targetingElements = campaignsAddHelper.selectTargeting(targetSections);
        return this;
    }

    /**
     * choose campaign type
     */
    public CabCampaigns chooseCampaignType(String type) {
        campaignsAddHelper.selectType(type);
        return this;
    }

    /**
     * choose campaign language
     */
    public CabCampaigns chooseCampaignLanguage(String language) {
        campaignsAddHelper.selectLanguage(language);
        return this;
    }

    public void saveCampaign() {
        campaignsAddHelper.saveSettings();
    }

    /**
     * check displayed targeting
     */
    public boolean checkDisplayedTargeting() {
        return campaignsAddHelper.checkSelectedTargeting(targetingElements);
    }

    public boolean checkAllChosenCompliantIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMPLIANT_ALL_COUNTRY_ICON.isDisplayed());
    }

    public void openCompliantPopup() {
        COMPLIANT_ICON_POPUP.click();
        checkErrors();
    }

    public boolean checkSelectedCompliantGeo(CompliantType...types) {
        ALL_COMPLIANT_CHECKBOXES.shouldHave(CollectionCondition.sizeGreaterThan(3));
        return Arrays.stream(types).allMatch(elem -> $("[class='countriesCompliant'][value='" + elem.getCompliantType() + "']").isSelected());
    }

    public boolean checkSelectedCompliantGeo(int expectedSize) {
        return helpersInit.getBaseHelper().checkDataset(expectedSize, ALL_COMPLIANT_CHECKBOXES.filterBy(selected).size());
    }

    public void submitCompliantWithCheckboxes(CompliantType...types) {
        ALL_COMPLIANT_CHECKBOXES.asDynamicIterable().forEach(elem -> markUnMarkCheckbox(false,elem));
        Arrays.stream(types).forEach(elem -> $("[class='countriesCompliant'][value='" + elem.getCompliantType() + "']").click());
        SUBMIT_POPUP_BUTTON.click();
        SUBMIT_POPUP_BUTTON.shouldBe(hidden);
        checkErrors();
    }

    public boolean checkPartChosenCompliantIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMPLIANT_PART_COUNTRY_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed());
    }

    public boolean checkCompliantCountryIcon(String country) {
        return switch (country) {
            case "Romania" -> ROMANIA_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed();
            case "Italy" -> ITALY_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed();
            case "Spain" -> SPAIN_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed();
            case "Czech Republic" -> CZECH_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed();
            case "Poland" -> POLAND_REG_ICON.shouldBe(visible, ofSeconds(8)).isDisplayed();
            default -> throw new IllegalStateException("Unexpected value: " + country);
        };
    }

    public void filterByComplient(String country) {
        COMPLIANT_FILTER.selectOptionByValue(country);
        campaignsListHelper.submitFilter();
        waitForAjax();
    }

    public boolean checkCampaignsCompliantTitlesAllMatch(CompliantType country) {
        $$("[id*='compliantFlag']").shouldBe(
                CollectionCondition.allMatch("checkCampaignsCompliantTitlesAllMatch",
                        i -> i.getAttribute("data-compliance").contains(country.getCompliantType()))
        );
        return true;
    }

    public boolean checkCampaignsCompliantTitlesAnyMatch(CompliantType country) {
        List<Object> list = new ArrayList<>();
        for(SelenideElement s : $$("[id*='compliantFlag']")){
            list.add(s.attr("data-compliance"));
        }
        return list.contains(country.getCompliantType());
    }

    public void uploadCertificates(String...images) {
        if (images[0] != null) {
            Arrays.stream(images).forEach(element -> {
                UPLOAD_CERTIFICATES_BUTTON.sendKeys(element);
                sleep(1000);

            });
        }
    }

    public void deleteUploadedCertificates() {
        REMOVE_CERTIFICATE_ICON.asDynamicIterable().forEach(element->{
            element.click();
            helpersInit.getMessageHelper().checkCustomMessagesCab(messageRemoveCertificate);
        });
    }

    public int getAmountOfCertificates() {
        return REMOVE_CERTIFICATE_ICON.shouldHave(/*CollectionCondition.sizeGreaterThan(expectedSize - 1)*/).size();
    }

    /**
     * Pause campaign
     */
    public void pauseCampaign(){
            campaignsListHelper.pauseCampaign();
    }

    /**
     * Move campaign to trash bin
     */
    public void deleteCampaign(int clientId) {
        SelenideElement deleteIcon = $(String.format(MOVE_TO_THE_TRASH_BIN_ICON, campaignId));
        deleteIcon.shouldBe(Condition.visible).click();
        deleteIcon.shouldBe(hidden);
        $(String.format(CAMPAIGN_ID_FIELD, clientId, campaignId)).shouldBe(hidden);
        checkErrors();

    }

    /**
     * проверка отображения кампании в списке кампаний
     */
    public boolean isDisplayedCampaign(int clientId) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                $(String.format(CAMPAIGN_ID_FIELD, clientId, campaignId)).isDisplayed());
    }

    public void openChangeCampaignTypePopup() {
        CAMPAIGN_TYPE_POPUP.click();
        checkErrors();
    }

    public void changeCampaignTypeAndCategory(String campaignType, String categoryId) {
        $("[id='" + campaignType + "_type']").click();
        helpersInit.getBaseHelper().selectValueFromParagraphListJs(CAMPAIGN_CATEGORY_SELECT, categoryId);
        ACCEPT_CHANGE_CAMPAIGN_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public boolean checkAlertPresenceWithText(boolean isShown) {
        return isShown ? helpersInit.getBaseHelper().checkDatasetEquals(helpersInit.getBaseHelper().getTextFromAlert(), changeCampaignTypeAlertText) : isShown;

    }

    public String chooseLanguageWithoutContext(List<String> languagesForContext) {
        return campaignsAddHelper.chooseLanguageWithoutContext(languagesForContext);
    }

    public CabCampaigns enableTargeting(boolean useTargeting) {
        if (useTargeting) ENABLE_TARGETING_BLOCK_RADIO.shouldBe(visible).click();
        return this;
    }

    public boolean isDisplayedContextTargetingTab() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CONTEXT_TAB.isDisplayed());
    }

    public boolean isDisplayedSentimentsTargetingTab() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SENTIMENTS_TAB.isDisplayed());
    }

    /**
     * Check settings for campaign with context targeting in list interface
     */
    public boolean checkContextSettingsInListInterface(String targetingValue) {
        return campaignsListHelper.checkContextSettingsInListInterface(targetingValue);
    }

    /**
     * Check settings for campaign with Sentiments targeting in list interface
     */
    public boolean checkSentimentsSettingsInListInterface(String targetingValue) {
        return campaignsListHelper.checkSentimentsSettingsInListInterface(targetingValue);
    }

    /**
     * check UTM settings in edit interface
     */
    public boolean checkUTMSettingsInEditCampaignInterface() {
        return campaignsAddHelper.checkUtmTags(utmTaggingGa, isEnabledCustomTag, utms);
    }

    /**
     * check UTM description in edit interface
     */
        public boolean checkUtmDescriptionInEditCampaignInterface(String checkedValue) {
        return CUSTOM_TAG_DESCRIPTION.getOwnText().contains(checkedValue);
    }

    public boolean isDisplayVerifiedIcon() {
        return COPYRIGHT_ICON.isDisplayed();
    }

    public boolean checkStateOfRotateInUkraine() {
        return !ROTATE_UA_DISABLED.isDisplayed();
    }

    public void changeStateOfRotateInUkraine(boolean state) {
            if (state & ROTATE_UA_DISABLED.isDisplayed()) {
                ROTATE_UA_ICON.click();
            } else if (!state & !ROTATE_UA_DISABLED.isDisplayed()) {
                ROTATE_UA_ICON.click();
            }
            waitForAjax();
    }

    public boolean isDisplayedRotateUkraineIcon() {
            return ROTATE_UA_ICON.isDisplayed();
    }

    /**
     * method set target exclude
     */
    public void setTargetsExclude(TargetingType.Targeting chosenTargeting) {
        $(String.format(TARGETS_TAB, chosenTargeting.getTypeValue())).click();
        campaignsAddHelper.chooseRandomTarget("exclude", chosenTargeting.getTypeValue());
    }

    /**
     * method check target presence
     */
    public boolean checkTargetsPresence(TargetingType.Targeting chosenTargeting) {
        return helpersInit.getBaseHelper().getTextAndWriteLog(
                $(String.format(TARGETS_OUT_BLOCK, chosenTargeting.getTypeValue())).isDisplayed());
    }

    public boolean checkWarningAboutRotationInUkraine(String text) {
        return $$(ROTATION_IN_UKRAINE_TARGETING_BLOCK_TEXT).filter(visible).texts().contains(text);
    }

    public boolean checkCampaignLanguage(String text) {
        return campaignsAddHelper.checkLanguage(text);
    }

    public boolean checkCloneCampaignIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(CLONE_ICON.isDisplayed());
    }

    public boolean checkRestoreCampaignIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(RESTORE_CAMPAIGN_ICON.isDisplayed());
    }

    public void pressCloneCampaignIcon() {
        campaignsListHelper.cloneIconClick();
    }

    public boolean checkAdvertiserNameCheckbox() {
        return CLONE_COPY_SHOW_NAME_CHECKBOX.isDisplayed() &&
                Objects.requireNonNull(CLONE_COPY_SHOW_NAME_CHECKBOX.getAttribute("disabled")).equals("true") &&
                !CLONE_COPY_SHOW_NAME_CHECKBOX.isEnabled() &&
                CLONE_COPY_SHOW_NAME_CHECKBOX.isSelected();
    }

    public void finishCloneCampaign() {
        campaignName = campaignsListHelper.setCloneName();
        campaignsAddHelper.saveClone();
    }

    public void changeAdvertiserName(String newAdvertiserName) {
        campaignsAddHelper.fillNameDisplayedWidgets(newAdvertiserName);
        campaignsAddHelper.saveSettings();
    }

    public boolean checkMediaSourceSelectStateInEditCampaignInterface() {
        return MEDIA_SOURCE_SELECT.isEnabled();
    }

    public String getComplaintPopupMessage() {
        return COMPLIANT_POPUP_TEXT.text();
    }

    public boolean isDisplayedCampaignNameForTeasers() {
        return DISPLAYED_NAME_FIELD.isDisplayed();
    }

    public boolean isEnabledCampaignNameForTeasers() {
        return !DISPLAYED_NAME_FIELD.has(attribute("disabled"));
    }

    public boolean isDisplayedConversionBlock() {
        return CONVERSION_STAGES_SETTINGS.isDisplayed();
    }

    /**
     * check rotate in mgid subnet checkbox
     */
    public boolean checkStateRotateMgidSubnet(boolean state) {
        return helpersInit.getBaseHelper().checkDataset(state, ROTATE_IN_MGID_CHECKBOX.isSelected());
    }

    /**
     * check rotate in Adskeeper subnet checkbox
     */
    public boolean checkStateRotateAdskeeperSubnet(boolean state) {
        return campaignsAddHelper.checkRotateAdskeeperSubnet(state);
    }

    public void createCampaignWithRotateInMgid() {
        campaignsAddHelper.selectType(campaignType);
        campaignName = campaignsAddHelper.fillName(campaignName);
        campaignCategory = campaignsAddHelper.selectCategory(campaignCategory);
        campaignLanguage = campaignsAddHelper.selectLanguage(campaignLanguage);
        campaignsAddHelper.clearTargetingSettings(true);
        campaignsAddHelper.saveSettings();
    }

    public boolean checkFilterFormIsDisplayed() {
        return FILTER_FORM.isDisplayed();
    }

    public boolean restrictionPopupIsDisplayed() {
        return RESTRICTION_POPUP.isDisplayed();
    }

    public boolean checkFrequencyCappingIsDisplayed() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(FREQUENCY_CAPPING_SELECT.isDisplayed());
    }

    public boolean checkCappingLimitIsEnabled() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(SEND_NO_MORE_THAN_INPUT.isEnabled());
    }

    public CabCampaigns selectFrequencyCapping() {
        campaignsAddHelper.selectFrequencyCapping(frequencyCapping);
        return this;
    }
}
