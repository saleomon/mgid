package pages.cab.products.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import java.util.Arrays;
import java.util.Objects;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Selenide.$;
import static pages.cab.products.locators.CabTitleTagsVerificationLocators.*;
import static core.helpers.BaseHelper.getRegExValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabTitleTagsVerification {
    private final HelpersInit helpersInit;
    private final Logger log;

    public CabTitleTagsVerification(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public void clickOkAction() {
        OK_ACTION_BUTTON.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        checkErrors();
    }

    public void clickNotOkAction() {
        NOT_OK_ACTION_BUTTON.click();
        checkErrors();
    }

    public void clickIgnoreAction() {
        IGNORE_ACTION_BUTTON.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        checkErrors();
    }

    public String getAmountOfDisplayedRows() {
        return getRegExValue(TOTAL_AMOUNT_OF_DISPLAYED_ROWS.text(), "[0-9]");
    }

    public boolean isEmptyTable() {
        return EMPTY_TABLE_LABEL.isDisplayed();
    }

    public void chooseCustomTags(String...customTags) {

        Arrays.asList(customTags).forEach(i -> TAGS_ELEMENTS.findBy(attribute("data-name", i)).click());

        TAGS_FORM_SAVE_BUTTON.click();
        helpersInit.getBaseHelper().checkAlertAndClose();
        waitForAjax();
        TAGS_FORM_SAVE_BUTTON.shouldBe(hidden);
    }

    public CabTitleTagsVerification fillTitle(String title) {
        TITLE_INPUT.sendKeys(title);
        return this;
    }

    public CabTitleTagsVerification selectLanguage(String option) {
        SELECT_LANGUAGE.selectOptionByValue(option);
        return this;
    }

    public CabTitleTagsVerification selectTranslationLanguage(String option) {
        SELECT_TRANSLATION_LANGUAGE.selectOptionByValue(option);
        return this;
    }

    public void filter() {
        FILTER_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public boolean isDisplayedRowWithTitle(String partOfTitle) {
        return $("[data-title*='" + partOfTitle + "'").isDisplayed();
    }

    public String getLinkWithRedirection() {
        return Objects.requireNonNull(LINK_TO_TEASERS_WITH_SAME_TITLE_LOCATOR.attr("href")).replace("%20", "+");
    }
}
