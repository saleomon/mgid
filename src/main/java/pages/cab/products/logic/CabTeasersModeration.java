package pages.cab.products.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import core.base.HelpersInit;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import pages.cab.products.helpers.TeaserModerationHelper;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static core.helpers.HelperLocators.PAGINATOR_CAB_SELECT;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.CampaignsListLocators.FILTER_BUTTON;
import static pages.cab.products.locators.TeaserModerationLocators.*;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CabTeasersModeration {

    private final TeaserModerationHelper teaserModerationHelper;
    private String description;
    private String callToAction;
    private String title;
    private List<String> images;
    private int focalPointLeft;
    private int focalPointTop;
    private final HelpersInit helpersInit;

    /**
     * Clients target types
     */
    public enum MassAction {
        APPROVE("accept"),
        REJECT("reject"),
        LANDING("landingchange"),
        TAGS("changeTags");

        private final String targetTypeValue;

        MassAction(String targetTypeValue) {
            this.targetTypeValue = targetTypeValue;
        }

        public String getMassAction() {
            return targetTypeValue;
        }

    }

    // tags
    private Multimap<String, String> tags = ArrayListMultimap.create(); // mass tagsType and tag for them

    public CabTeasersModeration(Logger log, HelpersInit helpersInit) {
        teaserModerationHelper = new TeaserModerationHelper(log, helpersInit);
        this.helpersInit = helpersInit;
    }

    public CabTeasersModeration setImages(String... images) {
        this.images = Arrays.asList(images);
        return this;
    }

    public CabTeasersModeration setFocalPoint(int focaLeft, int focalTop) {
        this.focalPointLeft = focaLeft;
        this.focalPointTop = focalTop;
        return this;
    }

    /**
     * check tier option at interface
     */
    public boolean checkDisplayingTierOption(String tierOption) {
        return teaserModerationHelper.checkDisplayingTierOption(tierOption);
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon() {
        return teaserModerationHelper.checkDisplayingApacIcon();
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIconAtZionTab() {
        teaserModerationHelper.clickLinkCheckButton();
        return teaserModerationHelper.checkDisplayingApacIconZionTab();
    }

    public void clickLpCheckButton() {
        teaserModerationHelper.clickLpCheckButton();
    }

    public void downloadCertificate() {
        DOWNLOAD_CERTIFICATE_BUTTON.click();
    }

    public void filterByCertificate() {
        markUnMarkCheckbox(true, FILTER_BY_CERTIFICATE);
        FILTER_BY_CERTIFICATE.scrollTo();
        submitFilter();
    }

    public void filterByFeedsProvider(String provider) {
        FILTER_BY_FEED_PROVIDER.selectOptionContainingText(provider);
        FILTER_BY_FEED_PROVIDER.scrollTo();
        submitFilter();
    }

    public void filterByAutoApprove() {
        markUnMarkCheckbox(true, FILTER_BY_AUTOAPPROVE);
        FILTER_BY_AUTOAPPROVE.scrollTo();
        submitFilter();
    }

    public void submitFilter() {
        FILTER_BUTTON.click();
        checkErrors();
    }

    public boolean isDisplayedDownloadCertificateButton() {
        return DOWNLOAD_CERTIFICATE_BUTTON.isDisplayed();
    }

    public void clickImageCheckButton() {
        teaserModerationHelper.clickImageCheckButton();
    }

    public boolean isDisplayedActiveTags() {
        return !TAGS_ELEMENTS_LIST.isEmpty();
    }

    public void clearAllTags() {
        TAGS_ELEMENTS_LIST.asFixedIterable().forEach(el->{
            el.click();
            waitForAjax();
        });
    }


    public int getAmountOfDisplayedManualImages() {
        return IMAGES_CONTAINER.size();
    }

    public boolean isSelectedRotateValidFormatsOnly() {
        return VALID_FORMATS_ONLY.isSelected();
    }

    public void enableRotateValidFormatsOnly(boolean state) {
        VALID_FORMATS_ONLY.setSelected(state);
    }

    public void clickTitleCheckButton() {
        teaserModerationHelper.clickTitleCheckButton();
    }

    public void deleteAllActiveTags() {
        ACTIVE_TAGS.asDynamicIterable().forEach(el-> { el.click(); waitForAjax();});
    }

    /**
     * метод проверки корректной работы по поиску тегов
     */
    public boolean checkWorkSearchTags(String tagType) {
        return teaserModerationHelper.checkWorkSearchTags(tagType);
    }

    public void chooseRandomTags(String tagType, String... massTags) {
        tags = teaserModerationHelper.chooseRandomTags(tagType, massTags);
    }

    public boolean checkTagsByType(String tagType, boolean isDeleteTags) {
        return teaserModerationHelper.checkChooseTags(tags, tagType, isDeleteTags);
    }

    public void saveTagsForm() {
        teaserModerationHelper.saveTagsForm();
    }

    /**
     * choose lp tags type on mass actions
     */
    public void chooseLpTypeTagOnMassAction() {
        teaserModerationHelper.selectAllTeasers();
        teaserModerationHelper.chooseMassActions(MassAction.TAGS);
    }

    public void editInLineTitle(String...title) {
        teaserModerationHelper.editInLineTitle(title);
    }

    public boolean checkTeasersTitle(String title) {
        return helpersInit.getBaseHelper().checkDatasetEquals(TEASERS_TITLE.text().trim(), title);
    }

    public boolean checkTeasersDescription(String description) {
        return helpersInit.getBaseHelper().checkDatasetEquals(TEASERS_DESCRIPTION.text().trim(), description);
    }

    public void editInLineTitleInZion(String...title) {
        teaserModerationHelper.editInLineTitleInZion(title);
    }

    public boolean checkTitleInZion(String title) {
        return helpersInit.getBaseHelper().checkDatasetEquals(INLINE_TITLE_ZION_LABEL.text(), title);
    }

    public void editInLineImageInZion() {
        MEDIA_CONTAINER.shouldBe(visible).doubleClick();
    }

    public void editLandingInline() {
        teaserModerationHelper.editLandingInline();
    }

    public void editLandingInMassAction() {
        teaserModerationHelper.editLandingInMassAction();
    }

    public void approveTeaserMassAction() {
        teaserModerationHelper.approveTeaserMassAction();
    }

    public void clickApproveOption(CabTeasers.ApproveOption option) {
        $x("//*[text()='" + option.getApproveOption() + "']").click();
        waitForAjax();
    }

    public void closePopupLinkingOffer() {
        if (CONTINUE_WITHOUT_LINKING.isDisplayed()) CONTINUE_WITHOUT_LINKING.click();
    }

    public void rejectTeaserMassAction(String subnet) {
        teaserModerationHelper.selectAllTeasers();
        teaserModerationHelper.chooseMassActions(MassAction.REJECT);
        teaserModerationHelper.clickChooseRejectReason();
        teaserModerationHelper.chooseRejectReason("7", subnet);
        teaserModerationHelper.saveRejectReasonForm();
    }

    public void setFocalPoint(boolean isSaveData) {
        MANUAL_FOCAL_POINT_BUTTON.click();
        FOCAL_POINT_ICON.shouldBe(visible);
        sleep(500);
        executeJavaScript("document.getElementById('crosshair').style.left = '" + focalPointLeft + "px'; document.getElementById('crosshair').style.top = '-" + focalPointTop + "px'");
        sleep(500);
        waitForAjax();
        if (isSaveData) MANUAL_FOCAL_CONFIRM_BUTTON.click();
        waitForAjax();
    }

    public void chooseRejectReason(String subnet, String...rejectReason) {
        Arrays.asList(rejectReason).forEach(el->teaserModerationHelper.chooseRejectReason(el, subnet));
    }

    /**
     * поп-ап который появляется при смене title/landingType - delete button
     */
    public void clickButtonDeleteTags() {
        teaserModerationHelper.clickButtonDeleteTags();
    }
    public void saveRejectReasonForm() {
        teaserModerationHelper.saveRejectReasonForm();
    }

    /**
     * поп-ап который появляется при смене title/landingType - save button
     */
    public void clickButtonSaveTags() {
        teaserModerationHelper.clickButtonSaveTags();
    }

    public CabTeasersModeration setDescription(String description) {
        this.description = description;
        return this;
    }

    public CabTeasersModeration setCallToAction(String callToAction) {
        this.callToAction = callToAction;
        return this;
    }

    /**
     * edit Description inline
     */
    public void editDescriptionInline() {
        description = teaserModerationHelper.editDescriptionInline(description);
    }

    /**
     * edit cal to action inline
     */
    public void editCallToActionInline() {
        description = teaserModerationHelper.editCallToActionInline(callToAction);
    }

    /**
     * approve teaser
     */
    public void approveTeaserWithoutLinkingOffer() {
        teaserModerationHelper.approveTeaser();
    }

    public void startModeration() {
        START_MODERATION.click();
        checkErrors();
    }

    public void clickShowSuggestedReasons() {
        PAGINATOR_CAB_SELECT.scrollTo();
        SHOW_SUGGESTED_REASONS_BUTTON.click();
        checkErrors();
    }

    public void applySuggestedReasonsButton() {
        APPLY_SUGGESTED_REASONS_BUTTON.click();
        checkErrors();
    }

    public void clearAllRejectedReason() {
        ACTIVE_REJECTED_REASONS.asDynamicIterable().forEach(SelenideElement::click);
    }

    public boolean checkEnabledRejectReasons(String...reasons) {
        return new HashSet<>(Arrays.asList(reasons)).containsAll(ACTIVE_REJECTED_REASONS.texts()) && ACTIVE_REJECTED_REASONS.size() > 0;
    }

    public List getListOfCommentsEnabledRejectReasons() {
        return  LIST_OF_FILLED_COMMENTS_FOR_REJECTION_REASONS.asFixedIterable().stream().filter(el-> el.attr("style").equals("display: block;")).collect(Collectors.toList());

    }
    public String  getAutorejectComponent() {
        return COMPONENT_LABEL.text().trim();
    }

    public String  getRejectionReason() {
        return REJECT_REASON_LABEL.text().trim();
    }

    public int getAmountOfAutoRejectReasonsBeforeStartModeration() {
        return Integer.parseInt(Objects.requireNonNull(getRegExValue(AMOUNT_AUTO_REJECT_REASONS_LABEL.text(), "[0-9]+")));
    }

    public boolean isDisplayedAutoRejectionForm() {
        return SHOW_SUGGESTED_REASONS_BUTTON.isDisplayed();
    }

    public void stopModeration() {
        FILTER_BUTTON.scrollTo();
        STOP_MODERATION.click();
        checkErrors();
    }

    /**
     * approve teaser
     */
    public void approveTeaser() {
        APPROVE_ICON.scrollTo().shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }


    /**
     * clickContinueWithApproveRejectButton teaser
     */
    public void clickContinueWithApproveRejectButton() {
        CONTINUE_APPROVE_REJECT_BUTTON.click();
        CONTINUE_APPROVE_REJECT_BUTTON.shouldBe(hidden);
        waitForAjax();
        checkErrors();
    }

    /**
     * check visibility popup linking offer
     */
    public boolean isDisplayedPopupLinkOffer() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LINK_OFFER_BUTTON.isDisplayed());
    }

    public boolean checkVisibilityApproveIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(APPROVE_ICON.isDisplayed());
    }


    public boolean checkVisibilityRejectIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REJECT_ICON.isDisplayed());
    }

    /**
     * reject teaser
     */
    public void rejectTeaser() {
        teaserModerationHelper.rejectTeaser();
    }

    public void clickRejectIconByThumb() {
        FILTER_BUTTON.scrollTo();
        REJECT_ICON.shouldBe(visible).click();
    }


    public void deleteLinkedProduct(String product) {
        $("[data-offer-name='" + product + "'] [class*='fa-remove']").click();
        waitForAjax();
    }

    public boolean checkProductPresence(String product) {
        return $("[data-offer-name='" + product + "']").shouldBe(hidden, ofSeconds(8)).isDisplayed();
    }

    public boolean isDisplayedProductAtPopupLinkingOffer(String product) {
        return $("[class='apply-offer-name'][value='" + product + "']").isDisplayed();
    }

    public boolean checkDisplayingLanguageMismatchIcon() {
        return LANGUAGE_ICON.isDisplayed();
    }

    public String checkToolTipMismatchIcon() {
        return LANGUAGE_ICON.attr("title");
    }

    public void setRemoveGoogleAddManager(boolean neededState) {
        teaserModerationHelper.setRemoveGoogleAddManager(neededState);
    }

    /**
     * Проверка состояния иконки google add manager
     */
    public boolean checkGoogleAddManagerIcon(boolean isEnabled) {
        return isEnabled ? helpersInit.getBaseHelper().getTextAndWriteLog(ICON_GOOGLE_ADD_MANAGER_ENABLED.isDisplayed()) : helpersInit.getBaseHelper().getTextAndWriteLog(ICON_GOOGLE_ADD_MANAGER_DISABLED.isDisplayed());
    }

    public void openEditProductForm() {
        if(START_MODERATION.isDisplayed()) START_MODERATION.click();
        LP_CHECK_BUTTON.click();
        waitForAjax();
        EDIT_PRODUCT_BUTTON.shouldBe(visible).scrollTo().click();
    }

    public Integer getAmountOfIconsDownloadCertificates() {
        return DOWNLOAD_CERTIFICATE_BUTTONS.size();
    }

    public Integer getAmountOfAutoApproveLabels() {
        return AUTO_APPROVE_LABELS.size();
    }

    public Integer getAmountDisplayedTeasers() {
        return TEASERS_ID_LOCATOR.size();
    }

    public Integer getAmountOfTeasersWithFeedsProvider(String provider) {
        return $$x("//span[text()='" + provider + "']").size();
    }

    public void editFeedsProviderInline(String feedsProvider) {
        FILTER_BUTTON.scrollTo().doubleClick();
        FEEDS_PROVIDER_INLINE.shouldBe(visible).doubleClick();
        sendKey(FEEDS_PROVIDER_INPUT.shouldBe(visible), feedsProvider + Keys.ENTER);
        FEEDS_PROVIDER_INPUT.shouldBe(hidden);
        waitForAjax();
    }

    public boolean isVisibleAutoAprove() {
        return AUTO_APPROVE_LABEL.isDisplayed();
    }

    public boolean isActiveCrop(String cropParametr) {
        String srcAttribute = IMAGE_BLOCK_VIEW.attr("src");
        String urlRegex = "\\d{3}x\\d{3}";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(Objects.requireNonNull(srcAttribute));
        String currentFormat = urlMatcher.find() ? srcAttribute.substring(urlMatcher.start(0), urlMatcher.end(0)) : "";
        TEASERS_ID_LOCATOR.first().scrollTo();

        switch (cropParametr) {
            case "16-9" -> {
                return $("img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x540") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("height"), "222") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("width"), "396");            }
            case "3-2" -> {
                return $("img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat,"960x640") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("height"),"264") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("width"), "396");
            }
            case "1-1" -> {
                return $("img[src*='" + cropParametr + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x960") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("height"), "396") &
                        helpersInit.getBaseHelper().checkDatasetEquals($("[id*='zion-popup__imgBig']").attr("width"), "396");
            }
        }
        return false;
    }

    public boolean isDisableCrop(String cropParametr) {
        return $("img[src*='" + cropParametr + "_red.png']").isDisplayed();
    }

    public void enableCropFormat(String cropParametr) {
        $("[src*='" + cropParametr + "']").click();
        sleep(1000);
    }

    public boolean checkImageVisibility() {
        return helpersInit.getBaseHelper().checkDatasetContains(IMAGE_CONTAINER.getAttribute("width"), ("480")) &&
                helpersInit.getBaseHelper().checkDatasetContains(IMAGE_CONTAINER.getAttribute("height"), ("270"));
    }

    public boolean checkErrorIconForTeaserTitle() {
        return TITLE_ERROR_ICON.isDisplayed();
    }

    public boolean checkErrorIconForTeaserDescription() {
        return DESCRIPTION_ERROR_ICON.isDisplayed();
    }

    public boolean checkTitleOfErrorIconForTeaserTitle(String titleValue) {
        return Objects.requireNonNull(TITLE_ERROR_ICON.attr("title")).equals(titleValue);
    }

    public boolean checkTitleOfErrorIconForTeaserDescription(String titleValue) {
        return Objects.requireNonNull(DESCRIPTION_ERROR_ICON.attr("title")).equals(titleValue);
    }

    public boolean isDisplayVerifiedIcon() {
        return COPYRIGHT_ICON.isDisplayed();
    }

    public void openStockGettyImageForm() {
        STOCK_GETTY_IMAGES_TAB.scrollTo().click();
        waitForAjax();
    }

    public void openPopupEditImageInline() {
        IMAGE_POPUP_BUTTON.doubleClick();
        checkErrors();
        waitForAjax();
    }

    public String getCategoryNameAtGettyImagesSearchForm() {
        return GETTY_IMAGES_SEARCH_PHRASE.text();
    }

    public void deleteGettyImageSearchPhrases() {
        GETTY_IMAGES_PHRASES_DELETE_BUTTON.shouldHave(CollectionCondition.sizeGreaterThan(0));
        GETTY_IMAGES_PHRASES_DELETE_BUTTON.asFixedIterable().forEach(SelenideElement::click);
    }

    public void inputGettyImageSearchPhrase(String phrase) {
        GETTY_IMAGES_SEARCH_INPUT.sendKeys(phrase + Keys.ENTER);
    }

    public void savePopupImageSettings() {
        SAVE_IMAGE_BUTTON.click();
        waitForAjax();
    }

    public void clickFirstImageGettyStock() {
        GETTY_IMAGES.first().click();
    }

    public boolean isDisplayedTeaserImage() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IMAGE_BLOCK_TEASER_LIST.isDisplayed()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_TEASER_LIST.attr("height"), "254") &&
                helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_TEASER_LIST.attr("width"), "451");
    }

    public void loadImageCloudinaryInline(String image) {
        teaserModerationHelper.loadImageCloudinaryInline(image);
    }

    public void searchTagMassAction(String val) {
        SEARCH_TAGS_INPUT.sendKeys(val);
    }

    public boolean checkDisplayedTags(String val) {
        return DISPLAYED_TITLE_TAGS.texts().stream().allMatch(el-> el.contains(val));
    }

    public boolean isPresentActiveTags() {
        return !ACTIVE_TAGS.isEmpty();
    }

    public boolean isDisplayedTitleSection() {
        return TITLE_TAGS_SECTION.isDisplayed();
    }

    public String getImageLink() {
        return CLOUDINARY_IMAGE_INPUT.val();
    }

    public boolean isDisplayedShowNameIcon() {
        return ADVERT_NAME_ICON.isDisplayed();
    }

    public String getTipShowNameIcon() {
        return ADVERT_NAME_ICON.attr("title");
    }

    public boolean checkMainScreenshot(List<String> list) {
        MAIN_SCREENSHOT_BLOCK.shouldBe(CollectionCondition.allMatch("checkMainScreenshot", i -> list.contains(i.getAttribute("href"))));
        return helpersInit.getBaseHelper().getTextAndWriteLog(MAIN_SCREENSHOT_BLOCK.size() == list.size());
    }

    public boolean checkSecondaryScreenshot(List<String> list) {
        SECONDARY_SCREENSHOT_BLOCK.shouldBe(CollectionCondition.allMatch("checkSecondaryScreenshot", i -> list.contains(i.getAttribute("href"))));
        return helpersInit.getBaseHelper().getTextAndWriteLog(SECONDARY_SCREENSHOT_BLOCK.size() == list.size());
    }

    public boolean checkAllPercentage(String val) {
        PERCENTAGE_BLOCK.shouldBe(CollectionCondition.allMatch("checkAllPercentage", i -> i.getText().equals(val)));
        return true;
    }

    public void openCopyLeaksScreenshotPopup() {
        FILTER_BUTTON.scrollTo();
        PLAGIARISM_ICON.click();
        waitForAjax();
        PLAGIARISM_POPUP.shouldBe(visible);
    }

    public boolean isDisplayedPlagiarismIcon(String val) {
        return $("[src*='plagiarism-" + val + ".svg']").isDisplayed();
    }
    public String getTeasersAdvertiserName(String teaserId) {
        return $x(String.format(ADVERTISER_NAME_FIELD, teaserId)).getText();
    }

    public String getTeasersAdvertiserNameTemp(String teaserId) {
        return $(String.format(ADVERTISER_NAME_TEMP_FIELD, teaserId)).shouldBe(visible).getText();
    }

    public void changeAdvertiserName(String teaserId, String newValue) {
        $(String.format(ADVERTISER_NAME_TEMP_FIELD, teaserId)).doubleClick();
        sendKey($x(String.format(ADVERTISER_NAME_INPUT, teaserId)).shouldBe(visible), newValue + Keys.ENTER);
        waitForAjax();
        waitForAjaxLoader();
        sleep(1000);
    }

    public boolean isDisplayedMirroringImageIcon(String val) {
        return $("img[src*='teaserMirrorImage" + val + ".svg'").isDisplayed();
    }

    public String getMirroringImagesToolTip() {
        return MIRRORING_IMAGES_LABEL.attr("title");
    }

    public List<String> getListOfDislpayedTeaserIds() {
        return TEASERS_ID_LOCATOR.texts().stream().map(String::trim).collect(Collectors.toList());
    }

    public void loadImageManualListInterface() {
        images.forEach(el-> {
            String currentFormat = prepareStringByRegExr("[1-9]{1,2}_[0-9]", el);
            $("[class='manual-img-upload-modal-file'][name*='" + currentFormat + "']").sendKeys(LINK_TO_RESOURCES_IMAGES + el);
            waitForAjax();
        });
    }

    public void openPopupEditImageInlineManualLoad() {
        IMAGE_POPUP_MANUAL_LOAD_BUTTON.doubleClick();
        checkErrors();
        waitForAjax();
    }

    public void setFocalPointToDefault() {
        MANUAL_FOCAL_DEFAULT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void openPopupEditImageInlineZion() {
        IMAGE_BLOCK_VIEW.doubleClick();
        checkErrors();
        waitForAjax();
    }
}
