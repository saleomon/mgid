package pages.cab.products.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.AudienceAddHelper;
import pages.cab.products.helpers.AudiencesListHelper;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;

public class CabAudiences {
    private final AudiencesListHelper audiencesListHelper;
    private final AudienceAddHelper audienceAddHelper;
    private final HelpersInit helpersInit;

    public CabAudiences(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.audiencesListHelper = new AudiencesListHelper(log, helpersInit);
        this.audienceAddHelper = new AudienceAddHelper(log, helpersInit);
    }

    private String audienceId;

    private String audienceName;
    private String audienceDescription;
    private String audienceCondition;
    private String audienceTarget;
    private String audiencePeriodOfValidity;

    public CabAudiences setAudienceName(String audienceName) {
        this.audienceName = audienceName;
        return this;
    }

    public CabAudiences resetAllVariables() {
        audienceName = null;
        audienceDescription = null;
        audienceCondition = null;
        audienceTarget = null;
        audiencePeriodOfValidity = null;
        return this;
    }

    public String getAudienceId() {
        return audienceId;
    }

    public CabAudiences setSomeFields(boolean setSomeFields) {
        audienceAddHelper.setSomeFields(setSomeFields);
        return this;
    }


    /**
     * Add new condition
     */
    public CabAudiences addNewCondition() {
        audienceAddHelper.addNewCondition();
        return this;
    }

    /**
     * Add new clients audience
     */
    public void addAudience() {
        audiencesListHelper.addNewAudience();
        setAudienceSettings();
    }

    /**
     * Set all audience settings
     */
    public void setAudienceSettings() {
        audienceName = audienceAddHelper.setAudienceName(audienceName);
        audienceDescription = audienceAddHelper.setAudienceDescription(audienceDescription);
        audienceCondition = audienceAddHelper.setAudienceCondition(audienceCondition);
        audienceTarget = audienceAddHelper.setAudienceTarget(audienceTarget);
        audiencePeriodOfValidity = audienceAddHelper.setAudiencePeriodOfValidity(audiencePeriodOfValidity);

        audienceAddHelper.submitAudience();
    }

    /**
     * Get and save audience id
     */
    public void readAndSaveAudienceId() {
        audienceId = $x(".//tr[td[div[contains(text(), '" + audienceName + "')]]]//td[1]").shouldBe(visible, ofSeconds(8)).text();
        helpersInit.getBaseHelper().getTextAndWriteLog(audienceId);
    }

    /**
     * Edit client audience
     */
    public void editClientAudience() {
        audienceName = audienceAddHelper.setAudienceName(audienceName);
        audienceDescription = audienceAddHelper.setAudienceDescription(audienceDescription);
        audienceTarget = audienceAddHelper.setAudienceTarget(audienceTarget);
        audiencePeriodOfValidity = audienceAddHelper.setAudiencePeriodOfValidity(audiencePeriodOfValidity);
        audienceAddHelper.submitAudience();
    }

    /**
     * Check client audience
     */
    public boolean checkClientAudience() {
        return audienceAddHelper.checkAudienceName(audienceName) &&
                audienceAddHelper.checkAudienceDescription(audienceDescription) &&
                audienceAddHelper.checkTargetInAudience(audienceTarget) &&
                audienceAddHelper.checkExpireInAudience(audiencePeriodOfValidity);
    }

    /**
     * Delete client audience
     */
    public boolean deleteClientAudience() {
        return audiencesListHelper.deleteClientAudience(audienceId);
    }

}
