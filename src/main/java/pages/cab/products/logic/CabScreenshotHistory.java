package pages.cab.products.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import core.base.HelpersInit;

import java.util.Objects;

import static com.codeborne.selenide.Selenide.*;
import static pages.cab.products.locators.CabScreenshotHistoryLocators.*;
import static pages.cab.products.locators.TeaserModerationLocators.UPLOAD_CERTIFICATES_BUTTON;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class CabScreenshotHistory {
    private final HelpersInit helpersInit;

    public CabScreenshotHistory(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public void linkOfferWithTeaser(String product) {
        for(int i = 0; i < 10 && SEARCH_OFFERS_SUBMIT_BUTTON.has(Condition.attribute("disabled")); i++) {
            CHECKBOX_SCREENSHOT.click();

            clearAndSetValue(SEARCH_OFFERS_FIELD, product);
            $x("//a[text()='" + product + "']").shouldBe(Condition.visible).hover().click();
            waitForAjax();
            AUTOCOMPLETE_ELEMENT.shouldBe(Condition.hidden);
        }
        clickInvisibleElementJs(SEARCH_OFFERS_SUBMIT_BUTTON);
        waitForAjax();
        checkErrors();
    }

    public void searchProduct(String product) {
        CHECKBOX_SCREENSHOT.click();
        clearAndSetValue(SEARCH_OFFERS_FIELD, product);
        waitForAjax();
    }

    public boolean checkAutocompleteProduct(String product) {
        return helpersInit.getBaseHelper().checkDatasetEquals(AUTOCOMPLETE_ELEMENT.text(), product);
    }

    public void checkVisibilityLinkedProduct(String product) {
        PRODUCTS.shouldBe(CollectionCondition.sizeGreaterThan(0))
                .shouldBe(CollectionCondition.anyMatch("all elements PRODUCTS shouldBe contains " + product + " in href", e -> e.getAttribute("href").contains(product)));
        //return PRODUCTS.stream().anyMatch(elem -> elem.attr("href").contains(product));
    }

    public void deleteLinkedProduct(String product) {
        $("[data-offer-name='" + product + "'] [class*='fa-remove']").click();
        waitForAjax();
    }

    public boolean checkProductPresence(String product) {
        return $("[data-offer='" + product + "']").isDisplayed();
    }

    public void openPopupUploadCertificate() {
        UPLOAD_POPUP_BUTTON.click();
    }

    public void uploadCertificates(String images) {
        if (images != null) {
            UPLOAD_CERTIFICATES_BUTTON.sendKeys(images);
            sleep(1000);
        }
    }

    public boolean checkVisibilityOfCertificate(String images) {
        POPUP_UPLOAD_CERTIFICATE_BLOCK.shouldBe(Condition.visible);
        return UPLOADED_CERTIFICATES.texts().stream().anyMatch(el->el.contains(images));
    }

    public void deleteCertificate() {
        DELETE_CERTIFICATE_BUTTON.click();
        waitForAjax();
    }

    public void downloadCertificates() {
        DOWNLOAD_CERTIFICATE_BUTTON.click();
        waitForAjax();
    }

    public boolean isDisplayVerifiedIcon() {
        return VERIFIED_ICON.isDisplayed();
    }

    public String getTitleOfVerifiedIcon() {
        return VERIFIED_ICON.attr("title");
    }

    public void changeStateVerifiedIcon(boolean state) {
        if (state && Objects.requireNonNull(VERIFIED_ICON.attr("title")).equals("Not verified")) {
            VERIFIED_ICON.click();
        } else if (!state && Objects.requireNonNull(VERIFIED_ICON.attr("title")).equals("Verified")){
            VERIFIED_ICON.click();
        }
        waitForAjax();
    }

    public boolean isDisplayedShowNameIcon() {
        return ADVERT_NAME_ICON.isDisplayed();
    }

    public String getTipShowNameIcon() {
        return ADVERT_NAME_ICON.attr("title");
    }
}
