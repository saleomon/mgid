package pages.cab.products.logic;

import com.codeborne.selenide.Condition;

import static pages.cab.products.locators.CampaignsAllStatLocators.COUNTED_CLICKS_TOTAL_CELL;

public class CabAllStats {

    public CabAllStats() {
    }

    public String getStatisticsTotalClicks() {
        return COUNTED_CLICKS_TOTAL_CELL.shouldBe(Condition.visible).text();
    }
}
