package pages.cab.products.logic;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import core.base.HelpersInit;
import pages.cab.products.helpers.TeaserAddHelper;
import pages.cab.products.helpers.TeaserListHelper;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static libs.hikari.tableQueries.partners.GHits1.selectTeaserByTitle;
import static pages.cab.products.locators.CampaignsListLocators.TOTAL_AMOUNT_OF_DISPLAYED_ENTITIES;
import static pages.cab.products.locators.TeaserAddLocators.DESCRIPTION_INPUT;
import static pages.cab.products.locators.TeaserAddLocators.SHOW_VALID_FORMATS;
import static pages.cab.products.locators.TeaserAddLocators.TITLE_INPUT;
import static pages.cab.products.locators.TeaserAddLocators.URL_INPUT;
import static pages.cab.products.locators.TeaserAddLocators.*;
import static pages.cab.products.locators.TeaserListLocators.*;
import static pages.cab.products.variables.TeaserListVariables.*;
import static core.helpers.BaseHelper.*;
import static core.helpers.ErrorsHelper.checkErrors;
import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class CabTeasers {

    private final HelpersInit helpersInit;
    private final Logger log;
    private final TeaserListHelper teaserListHelper;
    private final TeaserAddHelper teaserAddHelper;

    //create teaser
    private int teaserId;
    private String domain;
    private String image;
    private OwnershipOfMedia imageOwner;
    private List<String> images;
    private String title;
    private String description;
    private String callToAction;
    private String categoryId;
    private String type;
    private String landing;
    private String lifeTime;
    private String priceOfClick;
    private String price;
    private String oldPrice;
    private String currency;
    private String sourceTitleSuggestion;
    private String discount;
    private Boolean isRotateValidFormats;
    private Boolean focalPointState = false;
    private boolean needToBlockAdDueCopyAd = true;
    private boolean needToCheckCategory = true;
    private int focalPointLeft;
    private int focalPointTop;

    // tags
    private List<String> tagsType = new ArrayList<>(); // title|lp|url
    private String[] customTags;
    private Multimap<String, String> tags = ArrayListMultimap.create(); // mass tagsType and tag for them


    public CabTeasers(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
        teaserListHelper = new TeaserListHelper(log, helpersInit);
        teaserAddHelper = new TeaserAddHelper(helpersInit);
    }

    public enum OwnershipOfMedia{
        OWN_IMAGE("0"),
        OPEN_AI_IMAGE("1"),
        GETTY_IMAGES_IMAGE("2");

        private final String value;

        OwnershipOfMedia(String custom) { this.value = custom; }

        public String getValue() {
            return value;
        }
    }

    public enum ApproveOption {
        APPLY_WITH_APPROVE("Apply with approve"),
        APPLY_WITHOUT_APPROVE("Apply without approve"),
        LINKS_OFFER_MANUALLY("Links offers manually"),
        CONTINUE_WITHOUT_LINKING("Continue without linking");

        private final String approveOption;
        ApproveOption(String option) {
            this.approveOption = option;
        }
        public String getApproveOption() {
            return approveOption;
        }
    }

    public CabTeasers isGenerateNewValues(boolean generateNewValues) {
        teaserAddHelper.setGenerateNewValues(generateNewValues);
        return this;
    }
    public CabTeasers setOwnershipForImage(OwnershipOfMedia imageOwner) {
        this.imageOwner = imageOwner;
        return this;
    }

    public CabTeasers needToBlockTeaser(boolean needToBlockAdDueCopyAd) {
        this.needToBlockAdDueCopyAd = needToBlockAdDueCopyAd;
        return this;
    }

    public CabTeasers isSetSomeFields(boolean setSomeFields) {
        teaserAddHelper.setSomeFields(setSomeFields);
        return this;
    }

    public CabTeasers isNeedToEditUrl(boolean isNeedToEditUrl) {
        teaserAddHelper.setNeedToEditUrl(isNeedToEditUrl);
        return this;
    }

    public CabTeasers isSetImageFromRequest(boolean isNeedToEditUrl) {
        teaserAddHelper.isSetImageFromRequest(isNeedToEditUrl);
        return this;
    }

    public CabTeasers isNeedToEditCategory(boolean isNeedToEditUrl) {
        needToCheckCategory = isNeedToEditUrl;
        teaserAddHelper.isNeedToEditCategory(isNeedToEditUrl);
        return this;
    }

    public int getTeaserId() {
        return teaserId;
    }

    public CabTeasers setDomain(String domain) {
        this.domain = domain;
        return this;
    }

    public CabTeasers setImage(String image) {
        this.image = image;
        return this;
    }

    public CabTeasers setFocalPoint(int focaLeft, int focalTop) {
        this.focalPointLeft = focaLeft;
        this.focalPointTop = focalTop;
        return this;
    }

    public CabTeasers setImages(String... images) {
        this.images = Arrays.asList(images);
        return this;
    }

    public CabTeasers setTitle(String title) {
        this.title = title;
        return this;
    }

    public CabTeasers setDescription(String description) {
        this.description = description;
        return this;
    }

    public CabTeasers setCallToAction(String callToAction) {
        this.callToAction = callToAction;
        return this;
    }

    public CabTeasers setCategoryId(String categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getType() {
        return type;
    }

    public CabTeasers setRotateValidFormats(boolean isRotateValidFormats) {
        this.isRotateValidFormats = isRotateValidFormats;
        return this;
    }

    public CabTeasers setPriceOfClick(String priceOfClick) {
        this.priceOfClick = priceOfClick;
        return this;
    }

    public CabTeasers usePriceOfClick(boolean usePriceOfClick) {
        teaserAddHelper.setUsePriceOfClick(usePriceOfClick);
        return this;
    }

    public String getTeaserUrl() {
        return domain;
    }


    public void setTagsType(String... tagsType) {
        this.tagsType.addAll(Arrays.asList(tagsType));
    }

    public void setTags(String... tags) {
        this.customTags = tags;
    }

    public CabTeasers resetAllVariables() {
        domain = null;
        image = null;
        title = null;
        description = null;
        categoryId = null;
        type = null;
        landing = null;
        lifeTime = null;
        priceOfClick = null;
        price = null;
        oldPrice = null;
        currency = null;
        discount = null;
        return this;
    }

    public enum Statuses {
        ON_MODERATION("On moderation"),
        INTERNAL_DRAFTS("Internal drafts"),
        CLIENT_DRAFTS("Client drafts"),
        APPROVED("Approved");

        private final String statusesValue;

        Statuses(String statuses) {
            this.statusesValue = statuses;
        }

        public String getStatusesValue() {
            return statusesValue;
        }
    }

    /**
     * create teaser
     */
    public int createTeaser() {
        domain = teaserAddHelper.setUrl(domain);
        teaserAddHelper.useImprovementSwitch();
        image = teaserAddHelper.loadImage(image, imageOwner);
        indicateFocalPoint();
        teaserAddHelper.rotateValidFormats(isRotateValidFormats);
        title = teaserAddHelper.setTitle(title);
        description = teaserAddHelper.setDescription(description);
        callToAction = teaserAddHelper.setCallToAction(description);
        categoryId = teaserAddHelper.chooseCategory(categoryId);
        type = teaserAddHelper.chooseTeaserType(type);
        landing = teaserAddHelper.chooseLanding(landing);
        lifeTime = teaserAddHelper.chooseLifeTime(lifeTime);
        priceOfClick = teaserAddHelper.setPriceOfClick(priceOfClick);

        // discount block
        price = teaserAddHelper.fillProductPrice(price);
        oldPrice = teaserAddHelper.fillOldProductPrice(oldPrice);
        discount = teaserAddHelper.setProductDiscount(discount);
        currency = teaserAddHelper.selectCurrency(currency);

        teaserAddHelper.saveTeaser();

        return teaserId = getTeaserIdByTitle();
    }

    /**
     * create teaser
     */
    public int saveToDraft() {
        domain = teaserAddHelper.setUrl(domain);
        image = teaserAddHelper.loadImage(image, imageOwner);
        title = teaserAddHelper.setTitle(title);
        description = teaserAddHelper.setDescription(description);
        callToAction = teaserAddHelper.setCallToAction(description);
        categoryId = teaserAddHelper.chooseCategory(categoryId);
        type = teaserAddHelper.chooseTeaserType(type);
        landing = teaserAddHelper.chooseLanding(landing);



        teaserAddHelper.saveToDraft();

        return teaserId = getTeaserIdByTitle();
    }

    public void loadImageCloudinary(String image, OwnershipOfMedia file) {
        teaserAddHelper.loadImageCloudinary(image, file);
    }

    public void loadImageCloudinaryInline(String image, CabTeasers.OwnershipOfMedia file) {
        teaserListHelper.loadImageCloudinaryInline(image, file);
    }

    /**
     * edit teaser
     */
    public boolean editTeaser() {
        domain = teaserAddHelper.setUrl(domain);

        //image block
        teaserAddHelper.useImprovementSwitch();
        image = teaserAddHelper.loadImage(image, imageOwner);
        indicateFocalPoint();

        title = teaserAddHelper.setTitle(title);
        description = teaserAddHelper.setDescription(description);
        categoryId = teaserAddHelper.chooseCategory(categoryId);
        lifeTime = teaserAddHelper.chooseLifeTimeEdit(lifeTime);

        // discount block
        price = teaserAddHelper.fillProductPrice(price);
        oldPrice = teaserAddHelper.fillOldProductPrice(oldPrice);
        discount = teaserAddHelper.setProductDiscount(discount);
        currency = teaserAddHelper.selectCurrency(currency);


        teaserAddHelper.saveTeaser();
        return teaserListHelper.isClientIdFilterDisplayed();
    }

    /**
     * check teaser settings in list interface
     */
    public boolean checkTeaserSettingsInListInterface() {
        return teaserListHelper.checkUrl(domain) &
                teaserListHelper.checkTitle(title) &
                teaserListHelper.checkDescription(description) &
                teaserListHelper.checkCategoryId(categoryId, needToCheckCategory) &
                teaserListHelper.checkTeaserType(type) &
                teaserListHelper.checkLanding(landing) &
                teaserListHelper.checkLiveTime(lifeTime) &
                teaserListHelper.checkPriceOfClick(priceOfClick) &

                // discount block
                teaserListHelper.checkPrice(price) &
                teaserListHelper.checkPriceOld(oldPrice) &
                teaserListHelper.checkDiscount(discount) &
                teaserListHelper.checkCurrency(currency);
    }

    /**
     * check teaser settings in create/edit interface
     */
    public boolean checkTeaserSettingsInCreateInterface() {
        return teaserAddHelper.checkCategory(categoryId, needToCheckCategory) &&
                teaserAddHelper.checkUrl(domain) &&
                teaserAddHelper.checkTitle(title) &&
                teaserAddHelper.checkDescription(description) &&
                teaserAddHelper.checkLifeTime(lifeTime) &&
                teaserAddHelper.checkPrice(price) &&
                teaserAddHelper.checkOldPrice(oldPrice) &&
                teaserAddHelper.checkDiscount(discount) &&
                teaserAddHelper.checkCurrency(currency);
    }

    /**
     * is teaser create - get teaser id by title and campaign id
     */
    public int getTeaserIdByTitle() {
        if (teaserListHelper.isVisibleTeaserIdLocator() && helpersInit.getMessageHelper().isSuccessMessagesCab()) {
            teaserId = selectTeaserByTitle(title);
            return helpersInit.getBaseHelper().getTextAndWriteLog(teaserId);
        }
        return 0;
    }

    public CabTeasers useDiscount(boolean useDiscount) {
        this.teaserAddHelper.setUseDiscount(useDiscount);
        return this;
    }

    public CabTeasers isGenerateAutoTitle(boolean useDiscount) {
        this.teaserAddHelper.setIsGenerateAutoTitle(useDiscount);
        return this;
    }

    public CabTeasers useTitle(boolean useTitle) {
        this.teaserAddHelper.setUseTitle(useTitle);
        return this;
    }

    public CabTeasers useDescription(boolean useDescription) {
        this.teaserAddHelper.setUseDescription(useDescription);
        return this;
    }

    public CabTeasers useManualFocalPoint(boolean usePoint) {
        focalPointState = usePoint;
        return this;
    }

    public CabTeasers setNeedToEditImage(boolean isNeeded) {
        this.teaserAddHelper.setNeedToEditImage(isNeeded);
        this.teaserListHelper.setNeedToEditImage(isNeeded);
        return this;
    }

    public CabTeasers setNeedToEditCategory(boolean isNeeded) {
        needToCheckCategory = isNeeded;
        this.teaserAddHelper.isNeedToEditCategory(isNeeded);
        this.teaserListHelper.isNeedToEditCategory(isNeeded);
        return this;
    }

    public CabTeasers useCallToAction(boolean useCallToAction) {
        this.teaserAddHelper.setUseCallToAction(useCallToAction);
        return this;
    }

    /**
     * Метод отклонения и удаления тизера
     */
    public boolean rejectAndDeleteTeaser(int teaserId) {
        if (!teaserListHelper.getTeasersId().get(0).equals(String.valueOf(teaserId))) {
            log.info("There are no needed teaser");
            return false;
        }
        teaserListHelper.rejectTeaser("7");
        return teaserListHelper.deleteTeaser();
    }

    public void deleteTeaser() {
        teaserListHelper.deleteTeaser();
    }

    public boolean checkCategoryIdInListInterface(String categoryId) {
        return teaserListHelper.checkCategoryId(categoryId, needToCheckCategory);
    }

    public boolean checkAdTypeInListInterface(String type) {
        return teaserListHelper.checkTeaserType(type);
    }

    public boolean checkLandingTypeInListInterface(String type) {
        return teaserListHelper.checkLanding(type);
    }

    /**
     * mass change part title
     */
    public void changeMassTitle(String oldPartTitle, String newPartTitle) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("changeTitle");
        teaserListHelper.setOldTitleMass(oldPartTitle);
        teaserListHelper.setNewTitleMass(newPartTitle);
        teaserListHelper.submitMassActions();
    }

    /**
     * mass change landing type
     */
    public void changeMassLandingType(String... customLanding) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("landingchange");
        landing = teaserListHelper.editLandingTypeOnMassAction(customLanding);
        teaserListHelper.submitMassActions();
    }

    /**
     * get random not selected options for landing type
     */
    public String getRandomNotSelectedLandingType() {
        ElementsCollection options = MASS_LANDING_SELECT.findAll("option:not([value=" + landing + "])");
        return options.get(randomNumbersInt(options.size())).val();
    }

    public String getAmountOfDrafts() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AMOUNT_OF_DRAFTS.text());
    }

    public Integer getAmountOfTeasersWithStatus(String status) {
        return helpersInit.getBaseHelper().getTextAndWriteLog($$x("//*[text()='" + status + "' and @class='value']").size());
    }

    /**
     * get count teasers on page
     */
    public int getCountTeasersOnPage() {
        return teaserListHelper.getCountTeasersOnPage();
    }

    /**
     * Method copying teaser to the campaign by mass actions for one campaign
     */
    public void copyTeaserByMassActionsToOneCampaign(int campaignId) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("copy");
        teaserListHelper.clickCopyToOneCampaignRadioMass();
        teaserListHelper.setCpcForCopyAllCampaigns("15");
        teaserListHelper.fillCampaignIds(campaignId);
        teaserListHelper.clickCopyTeaserToCampaigns();
    }

    /**
     * Method copying teaser to the campaign by mass actions for two campaigns
     */
    public void copyTeaserByMassActionsIntoCampaigns(int... campaignsId) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("copy");
        teaserListHelper.clickCopyToManyCampaignsRadioMass();
        teaserListHelper.setCpcForCopyAllCampaigns("16");
        teaserListHelper.fillCampaignIds(campaignsId);
        teaserListHelper.enableBlockingDueCopying(needToBlockAdDueCopyAd);
        teaserListHelper.clickCopyTeaserToCampaigns();
    }

    /**
     * check amount of teaser at  interface
     */
    public boolean checkAmountOfTeaser(int expectedAmount) {
        return teaserListHelper.checkAmountOfTeaser(expectedAmount);
    }

    /**
     * check displaying apac icon
     */
    public boolean checkDisplayingApacIcon() {
        return teaserListHelper.checkDisplayingApacIcon();
    }

    /**
     * set landing
     * set adType
     * approve teaser
     */
    public void setLandingAndAdTypeAndApproveTeaser() {
        teaserListHelper.chooseLandingInline(true);
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        teaserListHelper.chooseAdTypeInline();
        helpersInit.getMessageHelper().isSuccessMessagesCab();
        teaserListHelper.approveTeaser();
    }

    /**
     * Check teaser URL
     */
    public boolean checkTeasersUrl(String teaserUrl) {
        return teaserListHelper.checkUrl(teaserUrl);
    }

    /**
     * edit title inline
     */
    public String editTitleInline(String...titleValue) {
        title = teaserListHelper.editTitleInline(titleValue);
        return title;
    }

    public void editFeedsProviderInline(String feedsProvider) {
        FEEDS_PROVIDER_INLINE.shouldBe(visible).doubleClick();
        sendKey(FEEDS_PROVIDER_INPUT.shouldBe(visible), feedsProvider + Keys.ENTER);
        FEEDS_PROVIDER_INPUT.shouldBe(hidden);
        waitForAjax();
    }

    public String getTeaserStatus() {
        return TEASER_STATUS.text();
    }

    public void filterByStatus(String value) {
        FILTER_BY_STATUS.selectOptionByValue(value);
        teaserListHelper.submitFilter();
    }


    public void sendToModeration() {
        teaserAddHelper.saveTeaser();
    }

    public void sendToReModeration() {
        REMODERATION_BUTTON.click();
        checkErrors();
    }

    /**
     * Метод редактирует ландинг тизера инлайн
     * При пустом значении "newLandingType" будет выбираться рандомный тип лендинга
     *
     * @param isDeleteLpTags - delete or save LP tags
     * @param newLandingType - новый лендинг тизера
     */
    public void editLandingInline(boolean isDeleteLpTags, String... newLandingType) {
        teaserListHelper.chooseLandingInline(isDeleteLpTags, newLandingType);
    }

    public void editAdInline(String newAdType) {
        teaserListHelper.chooseAdTypeInline(newAdType);
    }

    /**
     * очищаем форму выбора тегов
     */
    public void clearAllTags() {
        teaserListHelper.clearAllTags();
        tags.clear();
    }

    /**
     * открываем форму выбора тегов
     */
    public void openTagsCloudForm() {
        teaserListHelper.openTagsCloudForm();
    }

    /**
     * закрываем форму выбора тегов
     */
    public void closeTagsForm() {
        teaserListHelper.closeTagsForm();
    }

    /**
     * открываем форму с тегами
     * проставляем рандомные теги если customTags.length = 0 или customTags
     * сохраняем форму
     * выбранные типы тегов и сами теги пишутся в tags
     *
     * @param massTags - флаг указывает на масоввые/инлайн действие
     */
    public void chooseTags(boolean massTags, boolean needToClearBefore) {
        if (massTags) {
            chooseTagsOnMassAction();
        } else teaserListHelper.openTagsCloudForm();
        if (needToClearBefore) $$("[data-type='" + tagsType.stream().findFirst().get() + "'][class='active']").asFixedIterable().forEach(SelenideElement::click);
        tags = teaserListHelper.chooseRandomTags(tagsType, massTags, customTags);
        teaserListHelper.saveTagsForm();
    }

    /**
     * поп-ап который появляется при смене title/landingType - delete button
     */
    public void clickButtonDeleteTags() {
        teaserListHelper.clickButtonDeleteTags();
    }

    /**
     * поп-ап который появляется при смене title/landingType - save button
     */
    public void clickButtonSaveTags() {
        teaserListHelper.clickButtonSaveTags();
    }

    /**
     * проверяем выбранные теги
     *
     * @param isDeleteTags - флаг состояния тегов: удалены/сохранены
     */
    public boolean checkChoosenTags(boolean isDeleteTags) {
        openTagsCloudForm();
        boolean state = teaserListHelper.checkChooseTags(tags, tagsType, isDeleteTags);
        closeTagsForm();
        return helpersInit.getBaseHelper().getTextAndWriteLog(state);
    }

    public void setChosenTags(String tagsType, String tag) {
        if (!this.tagsType.contains(tagsType)) this.tagsType.add(tagsType);
        this.tags.put(tagsType,tag);
    }

    /**
     * метод проверки корректной работы по поиску тегов
     */
    public boolean checkWorkSearchingTagsFilter() {
        openTagsCloudForm();
        return teaserListHelper.checkWorkSearchingTagsFilter();
    }

    /**
     * choose lp tags type on mass actions
     */
    public void chooseTagsOnMassAction() {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("changeTags");
    }

    /**
     * check exist custom value(option) in mass action select
     */
    public boolean checkExistCustomValueInMassActions(String value) {
        return teaserListHelper.checkExistCustomValueInMassActions(value);
    }

    /**
     * edit Description inline
     */
    public void editDescriptionInline() {
        description = teaserListHelper.editDescriptionInline(description);
    }

    /**
     * edit cal to action inline
     */
    public void editCallToActionInline() {
        description = teaserListHelper.editCallToActionInline(callToAction);
    }

    /**
     * check is teaser ids locator visible
     */
    public boolean checkPossibilityOfEditingCallToAction() {
        return teaserListHelper.checkPosibilityOfEditingCallToAction();
    }

    public void editCategoryInMass(String...category) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("catchange");
        categoryId = teaserListHelper.editCategoryOnMassAction(category);
        teaserListHelper.submitMassActions();
    }

    public void editAdTypeInMass(String... customType) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("typechange");
        type = teaserListHelper.editAdTypeOnMassAction(customType);
        teaserListHelper.submitMassActions();
    }

    /**
     * choose random adType but some custom type
     */
    public String chooseRandomAdTypeButSome(String... types) {
        String path = Arrays.stream(types)
                .map(i -> "not([value='" + i + "'])")
                .collect(Collectors.joining(":"));

        ElementsCollection options = MASS_AD_TYPE_SELECT.$$("option:" + path);

        return options.get(randomNumbersInt(options.size())).val();
    }

    /**
     * check adType in create teaser for task
     * check TYPE_SELECT has 1 option
     * check TYPE_SELECT has only customType
     */
    public boolean checkTeaserTypeForTaskInterface(String adType) {
        return teaserAddHelper.checkTeaserTypeForTaskInterface(adType);
    }


    /**
     * filter teasers by product
     */
    public void filterTeasersByProduct(boolean useShowCertified, String product) {
        teaserListHelper.enableShowCertifiedCheckbox(useShowCertified);
        teaserListHelper.filterTeasersByProduct(product);
    }

    /**
     * check filter result
     */
    public boolean checkFilterResult(String... campaignId) {
        return teaserListHelper.checkFilterResult() &&
                teaserListHelper.checkDisplayedTeasersOfCampaign(campaignId);
    }

    /**
     * Check if teaser blocked by system
     */
    public boolean isTeaserBlockedBySystem() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserListHelper.isTeaserBlockedBySystem() &&
                helpersInit.getBaseHelper().checkDatasetContains(teaserListHelper.getBlockTeaserTitle(), titleTeaserIsBlockedBySystem));
    }

    /**
     * Check if teaser blocked by client in Dashboard
     */
    public boolean isTeaserBlockedByClient() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserListHelper.isTeaserBlockedByClient() &&
                helpersInit.getBaseHelper().checkDatasetContains(titleTeaserIsBlockedByClient, teaserListHelper.getBlockTeaserTitle()));
    }

    /**
     * Check if teaser blocked by account manager
     */
    public boolean isTeaserBlockedByAccountManager() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserListHelper.isTeaserBlockedByAccountManager() &&
                helpersInit.getBaseHelper().checkDatasetEquals(teaserListHelper.getBlockTeaserTitle(), titleTeaserIsBlockedByAccountManager));
    }

    /**
     * Unblock teaser
     */
    public boolean unBlockTeaser() {
        return teaserListHelper.unBlockTeaser();
    }

    public boolean isTeaserUnblocked() {
        return helpersInit.getBaseHelper().checkDatasetContains(BLOCK_TEASER_ICON.attr("style"), "display: inline");
    }

    /**
     * Unblock teaser
     */
    public void markTeaserGeoAsCompliant(CompliantType...compliantType) {
        COMPLIANT_ICON_POPUP.click();
        Arrays.stream(compliantType).forEach(elem-> markUnMarkCheckbox(true, $("[class='countriesCompliant'][value='" + elem.getCompliantType() + "']")));
        SAVE_COMPLIANT_SETTINGS.click();
        waitForAjax();
        checkErrors();
        SAVE_COMPLIANT_SETTINGS.shouldBe(hidden);
    }

    /**
     * Clients co types
     */
    public enum CompliantType {
        RAC("13"),
        ZPL("16"),
        IAP("22"),
        AUTOCONTROL("18"),

        POLAND_REG_COMPLIANT ("19"),
        ARPP("20"),
        CONAR("45"),
        NBTC("54");

        private final String compliantTypeValue;

        CompliantType(String targetTypeValue) {
            this.compliantTypeValue = targetTypeValue;
        }

        public String getCompliantType() {
            return compliantTypeValue;
        }

    }

    public enum KycTypes{
        NOT_VERIFIED("not verified"),
        VERIFIED("verified"),
        BLOCKED("blocked"),
        REJECT("reject");
        protected final String kycType;

        KycTypes(String kycTypeValue) {
            this.kycType = kycTypeValue;
        }
    }

    /**
     * Block teaser
     */
    public void blockTeaserByIcon(boolean withCloaking, String blockedReasonValue) {
        teaserListHelper.blockTeaserByIcon(withCloaking, blockedReasonValue);
    }

    /**
     * Block teaser by Mass actions
     */
    public void blockTeaserByMassAction(boolean withCloaking, String blockedReason) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("block");
        teaserListHelper.blockTeaserByMass(withCloaking, blockedReason);
    }

    public void blockBySchedule(boolean withCloaking, String blockedReason) {
        teaserListHelper.blockTeaserBySchedule(withCloaking, blockedReason);
    }

    /**
     * Unblock teaser by Mass actions
     */
    public void unblockTeaserByMassAction() {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("unblock");
        teaserListHelper.submitMassActions();
    }

    public void changeStateSuspectCloakingMass(String parameter) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions(parameter);
        teaserListHelper.submitMassActions();
    }

    public void changeStateSuspectCloaking() {
        SUSPECTED_OF_CLOAKING.click();
        waitForAjax();
        checkErrors();
    }

    public boolean isDisplayedSuspectCloakingIcon(String typeOfIcon) {
        return $("[src*='teaserSuspectedOfCloaking" + typeOfIcon + ".svg']").isDisplayed();
    }

    /**
     * press filter button
     */
    public void pressFilterButton() {
        teaserListHelper.submitFilter();
    }

    /**
     * get amount of teasers belong type of campaign at list
     */
    public Integer getAmountOfTeasers() {
        return teaserListHelper.getAmountOfTeasers();
    }

    public String getTotalAmountOfTeasers() {
        return getRegExValue(TOTAL_AMOUNT_OF_DISPLAYED_ENTITIES.text(), "\\d+");
    }


    public boolean checkDisplayedTeasersWithIds(String ...values) {
        return new HashSet<>(TEASER_IDS_LOCATOR.texts()).containsAll(Arrays.asList(values)) && TEASER_IDS_LOCATOR.size() == values.length;
    }

    public Integer getAmountOfTeasersWithFeedsProvider(String provider) {
        return $$x("//span[text()='" + provider + "']").size();
    }

    public void cancelModeration() {
        CANCEL_MODERATION_CLIENT_DRAFTS.click();
        checkErrors();
    }

    public boolean isDisplayedSendToModerationButton() {
        return SEND_TO_MODERATION.isDisplayed();
    }

    public boolean isDisplayedRedoIcon() {
        return REDO_ICON.isDisplayed();
    }

    public void clickRedoIcon() {
        REDO_ICON.click();
        checkErrors();
        waitForAjax();
    }

    public boolean isDisplayedCancelModerationButton() {
        return CANCEL_MODERATION_INTERNAL_DRAFTS.isDisplayed();
    }

    /**
     * is visible search feed icon
     */
    public boolean checkVisibilityOfSearchFeedIcon() {
        return teaserListHelper.checkVisibilityOfSearchFeedIcon();
    }

    /**
     * approve teaser
     */
    public void approveTeaserWithoutLinkingOffer() {
        teaserListHelper.approveTeaser();
    }

    /**
     * approve teaser
     */
    public void approveTeaser() {
        APPROVE_ICON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
    }

    public String getLinkedOffers() {
        return LINKED_OFFERS.text();
    }

    public void applyWithAprove() {
        APPLY_WITH_APPROVE.click();
        waitForAjax();
        checkErrors();
    }

    public boolean isDisplayedOfferAtPopup(String offerName) {
        return DISPLAYED_OFFERS.texts().stream().anyMatch(el -> helpersInit.getBaseHelper().checkDatasetContains(el, offerName));
    }

    /**
     * clickContinueWithApproveRejectButton teaser
     */
    public void clickContinueWithAproveRejectButton() {
        CONTINUE_APPROVE_REJECT_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * clickContinueWithoutLinking teaser
     */
    public void clickContinueWithoutLinking() {
        CONTINUE_WITHOUT_LINKING.click();
        waitForAjax();
        checkErrors();
    }

    /**
     * check visibility approve icon
     */
    public boolean isDisplayedApproveIcon() {
        TEASER_ID_LOCATOR.shouldBe(visible);
        return helpersInit.getBaseHelper().getTextAndWriteLog(APPROVE_ICON.isDisplayed());
    }

    /**
     * check visibility reject icon
     */
    public boolean isDisplayedRejectIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REJECT_ICON.isDisplayed());
    }

    public String getRejectReason() {
        return REJECT_REASON.text();
    }

    /**
     * check visibility active option IAP icon
     */
    public boolean isDisplayedEnabledIapCompliant() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(ENABLED_COMPLIANT_IAP_ICON.isDisplayed());
    }

    public boolean isDisplayedCompliantIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMPLIANT_ICON_POPUP.isDisplayed());
    }

    /**
     * check visibility non active option IAP icon
     */
    public boolean isDisplayedDisabledIapCompliant() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(DISABLED_IAP_ICON.isDisplayed());
    }

    /**
     * check visibility popup linking offer
     */
    public boolean isDisplayedPopupLinkOffer() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(LINK_OFFER_BUTTON.isDisplayed());
    }

    /**
     * approve teaser
     */
    public boolean checkVisibilityApproveIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(APPROVE_ICON.isDisplayed());
    }

    public boolean checkVisibilityRejectIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REJECT_ICON.isDisplayed());
    }

    /**
     * reject teaser
     */
    public void rejectTeaser(String reason) {
        teaserListHelper.rejectTeaser(reason);
    }

    public boolean  checkImageVisibility() {
        return helpersInit.getBaseHelper().checkDatasetContains(IMAGE_CONTAINER.getAttribute("width"), ("480")) &&
         helpersInit.getBaseHelper().checkDatasetContains(IMAGE_CONTAINER.getAttribute("height"), ("270"));
    }

    public boolean  checkRotateValidFormats(boolean state) {
        return SHOW_VALID_FORMATS.isSelected() == state;
    }

    public void setRemoveGoogleAddManager(boolean neededState) {
        teaserListHelper.setRemoveGoogleAddManager(neededState);
    }

    /**
     * Проверка состояния иконки google add manager
     */
    public boolean checkGoogleAddManagerIcon(boolean isEnabled) {
        return isEnabled ? helpersInit.getBaseHelper().getTextAndWriteLog(ICON_GOOGLE_ADD_MANAGER_ENABLED.isDisplayed()) : helpersInit.getBaseHelper().getTextAndWriteLog(ICON_GOOGLE_ADD_MANAGER_DISABLED.isDisplayed());
    }

    public void approveTeaserMassAction() {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("accept");
        teaserListHelper.submitMassActions();
    }

    public void clickApproveOption(CabTeasers.ApproveOption option) {
        $x("//*[text()='" + option.getApproveOption() + "']").click();
        waitForAjax();
        sleep(500);
    }

    public void rejectTeaserMassAction() {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("reject");
        teaserListHelper.selectRejectReasons();
        teaserListHelper.chooseRejectReason("3");
        teaserListHelper.submitRejectReasons();
    }

    public long checkAmountOfDisplayedAcceptIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(APPROVE_ICONS.filter(visible).size());
    }

    public long checkAmountOfDisplayedRejectIcon() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(REJECT_ICONS.filter(visible).size());
    }

    public void filterByComplient(String country) {
        COMPLIANT_FILTER.selectOptionByValue(country);
        teaserListHelper.submitFilter();
    }

    public boolean checkCampaignsCompliantTitlesAllMatch(CabTeasers.CompliantType country) {
        COMPLIANT_ICONS_POPUP.shouldBe(CollectionCondition.allMatch("checkCampaignsCompliantTitlesAllMatch", i -> i.getAttribute("data-compliance").contains(country.getCompliantType())));
        return true;
    }

    public boolean checkCampaignsCompliantTitlesAnyMatch(CabTeasers.CompliantType country) {
        List<Object> list = new ArrayList<>();
        for(SelenideElement s : COMPLIANT_ICONS_POPUP){
            list.add(s.attr("data-compliance"));
        }
        return list.contains(country.getCompliantType());
    }

    public boolean checkAllChosenCompliantIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMPLIANT_ALL_COUNTRY_ICON.isDisplayed());
    }

    public void openCompliantPopup() {
        COMPLIANT_ICON_POPUP.click();
        checkErrors();
    }

    public boolean checkSelectedCompliantGeo(CompliantType...types) {
        return Arrays.stream(types).allMatch(elem -> $("[class='countriesCompliant'][value='" + elem.getCompliantType() + "']").isSelected());
    }

    public boolean checkSelectedCompliantGeo(int expectedSize) {
        return helpersInit.getBaseHelper().checkDataset(expectedSize, ALL_COMPLIANT_SELECTED_CHECKBOXES.shouldHave(sizeGreaterThan(ALL_COMPLIANT_SELECTED_CHECKBOXES.size()-1)).size());
    }

    public void submitCompliantWithCheckboxes(CabTeasers.CompliantType...types) {
        ALL_COMPLIANT_CHECKBOXES.asDynamicIterable().forEach(elem -> markUnMarkCheckbox(false,elem));
        Arrays.stream(types).forEach(elem -> $("[class='countriesCompliant'][value='" + elem.getCompliantType() + "']").click());
        SAVE_COMPLIANT_BUTTON.click();
        SAVE_COMPLIANT_BUTTON.shouldBe(hidden);
        checkErrors();
    }

    public boolean checkPartChosenCompliantIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(COMPLIANT_PART_COUNTRY_ICON.isDisplayed());
    }

    public boolean checkCompliantCountryIcon(String country) {
        return switch (country) {
            case "Romania" -> ROMANIA_ICON.isDisplayed();
            case "Italy" -> ITALY_ICON.isDisplayed();
            case "Spain" -> SPAIN_ICON.isDisplayed();
            case "Czech Republic" -> CZECH.isDisplayed();
            case "Poland" -> POLAND_ICON.isDisplayed();
            default -> throw new IllegalStateException("Unexpected value: " + country);
        };
    }

    public boolean isActiveIconCompliant(String country) {
        return switch (country) {
            case "Romania" -> !helpersInit.getBaseHelper().checkDatasetContains(ROMANIA_ICON.attr("class"),"noOne");
            case "Italy" -> !helpersInit.getBaseHelper().checkDatasetContains(ITALY_ICON.attr("class"), "noOne");
            case "Spain" -> !helpersInit.getBaseHelper().checkDatasetContains(SPAIN_ICON.attr("class"), "noOne");
            case "Czech Republic" -> !helpersInit.getBaseHelper().checkDatasetContains(CZECH.attr("class"), "noOne");
            case "Poland" -> !helpersInit.getBaseHelper().checkDatasetContains(POLAND_ICON.attr("class"), "noOne");
            default -> throw new IllegalStateException("Unexpected value: " + country);
        };
    }

    public void downloadCertificate() {
        DOWNLOAD_CERTIFICATE_BUTTON.click();
        checkErrors();
    }

    public boolean isDisplayedDownloadCertificateButton() {
        return DOWNLOAD_CERTIFICATE_BUTTON.isDisplayed();
    }

    public Integer getAmountOfIconsDownloadCertificates() {
        return DOWNLOAD_CERTIFICATE_BUTTONS.size();
    }

    public void filterByCertificate() {
        markUnMarkCheckbox(true, FILTER_BY_CERTIFICATE);
        FILTER_BUTTON.click();
    }

    public void filterByVideo(String val) {
        FILTER_ANIMATION_VIDEO.selectOptionByValue(val);
        FILTER_ANIMATION_VIDEO.scrollTo();
        FILTER_BUTTON.click();
    }
    public boolean isActiveCropVideo(String cropParameter) {
        return $("img[src*='" + cropParameter + "_blue.png']").isDisplayed();
    }
    public boolean isActiveCrop(String cropParameter) {
        String srcAttribute = IMAGE_BLOCK_VIEW.attr("src");
        String urlRegex = "\\d{3}x\\d{3}";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(Objects.requireNonNull(srcAttribute));
        String currentFormat = urlMatcher.find() ? srcAttribute.substring(urlMatcher.start(0), urlMatcher.end(0)) : "";

        switch (cropParameter) {
            case "16-9" -> {
                return $("img[src*='" + cropParameter + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x540") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"), "270") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "480");
            }
            case "3-2" -> {
                return $("img[src*='" + cropParameter + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat,"960x640") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"),"320") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "480");
            }
            case "1-1" -> {
                return $("img[src*='" + cropParameter + "_blue.png']").isDisplayed() &
                        helpersInit.getBaseHelper().checkDatasetEquals(currentFormat, "960x960") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("height"), "480") &
                        helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_VIEW.attr("width"), "480");
            }
        }
        return false;
    }

    public void enableCropFormat(String cropParametr) {
        $("[src*='" + cropParametr + "']").click();
        sleep(500);
        waitForAjax();
        checkErrors();
    }

    public boolean isDisableCrop(String cropParametr) {
        return $("img[src*='" + cropParametr + "_red.png']").isDisplayed();
    }

    public void openPopupEditImageInline() {
        IMAGE_POPUP_BUTTON.doubleClick();
        checkErrors();
        waitForAjax();
    }

    public void openPopupEditVideoInline() {
        VIDEO_POPUP_BUTTON.doubleClick();
        checkErrors();
        waitForAjax();
    }

    public String getImageLink() {
        return CLOUDINARY_IMAGE_INPUT.val();
    }

    public void editImageBlock() {
        loadImageCloudinaryInline(image, OwnershipOfMedia.OWN_IMAGE);
        teaserListHelper.rotateValidFormats(isRotateValidFormats);
        teaserListHelper.saveImageBlock();
    }

    public boolean checkAvailableImagesFromRequest(String...images) {
        IMAGES_POPUP_OPEN.scrollTo().click();
        List<String> currentImages = new ArrayList<>();
        String[] stringLink;
        String preparedImageName;
        for(SelenideElement s : BLOCKS_OF_IMAGES){
            stringLink = Objects.requireNonNull(s.attr("style")).split("/");
            preparedImageName = stringLink[stringLink.length-1].replace("\");", "");
            currentImages.add(preparedImageName);
        }
        checkErrors();
        return Arrays.stream(images).anyMatch(currentImages::contains);
    }

    public boolean isDisplayedTeaserImageBlock() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(IMAGE_BLOCK_TEASER_LIST.isDisplayed()) &&
                helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_TEASER_LIST.attr("height"), "134") &&
                helpersInit.getBaseHelper().checkDatasetEquals(IMAGE_BLOCK_TEASER_LIST.attr("width"), "238");
    }

    public boolean isDisplayedTeaserVideoBlock() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(VIDEO_BLOCK_TEASER_LIST.isDisplayed());
    }

    public String getActiveGettyImageMediaPage() {
        return ACTIVE_PAGE_GALLERY.text();
    }

    public String getTeaserLinkParamsInListInterface() {
        return teaserListHelper.getUrlParams();
    }

    public boolean isDisplayVerifiedIcon() {
        return COPYRIGHT_ICON.isDisplayed();
    }

    public void openStockGettyImageForm() {
        STOCK_GETTY_IMAGES_TAB.scrollTo().click();
        waitForAjax();
    }

    public void openUploadImageForm() {
        UPLOAD_IMAGE_TAB.scrollTo().click();
        waitForAjax();
    }

    public void clickFirstImageGettyStock() {
        GETTY_IMAGES.first().click();
    }

    public void chooseCategory() {
        teaserAddHelper.chooseCategory(categoryId);
        TITLE_INPUT.click();
        if($("[name=\"uploadImageSource\"]").isDisplayed()) {
            $("[name=\"uploadImageSource\"]").selectRadio(OwnershipOfMedia.OWN_IMAGE.getValue());
            $("[class*=\"saveImageUploadSource").click();
        }
    }

    public String getCategoryNameAtGettyImagesSearchForm() {
        return GETTY_IMAGES_SEARCH_PHRASE.text();
    }

    public String getStockGalleryNotification() {
        return GALLERY_NOTIFICATION_BLOCK.text();
    }

    public void deleteGettyImageSearchPhrases() {
        GETTY_IMAGES_PHRASES_DELETE_BUTTON.shouldHave(sizeGreaterThan(0));
        GETTY_IMAGES_PHRASES_DELETE_BUTTON.asFixedIterable().forEach(SelenideElement::click);
    }

    public void inputGettyImageSearchPhrase(String phrase) {
        GETTY_IMAGES_SEARCH_INPUT.sendKeys(phrase + Keys.ENTER);
    }

    public void savePopupImageSettings() {
        SAVE_IMAGE_BUTTON.click();
        waitForAjax();
    }

    public void savePopupManualImageSettings() {
        SAVE_MANUAL_IMAGE_BUTTON.click();
        waitForAjax();

        //workaround
        if (SAVE_MANUAL_IMAGE_BUTTON.isDisplayed()) {
            SAVE_MANUAL_IMAGE_BUTTON.click();
            waitForAjax();
        }
    }

    public String getLinkForPreviewTeaserInWidget() {
        return PREVIEW_IN_INFORMER_LINK.shouldBe(visible).attr("href");
    }

    public void openManualLoading() {
        LOAD_MANUAL_IMAGES_TAB.scrollTo().click();
    }

    public void loadImageManualCrateInterface() {
        images.forEach(el-> {
            String currentFormat = prepareStringByRegExr("[1-9]{1,2}_[0-9]", el);
            $("[class='upload-manual-image-form-file'][name*='" + currentFormat + "']").sendKeys(LINK_TO_RESOURCES_IMAGES + el);
            waitForAjax();
            checkErrors();
        });
    }

    public void loadImageManualListInterface() {
        images.forEach(el-> {
            String currentFormat = prepareStringByRegExr("[1-9]{1,2}_[0-9]", el);
            $("[class='manual-img-upload-modal-file'][name*='" + currentFormat + "']").sendKeys(LINK_TO_RESOURCES_IMAGES + el);
            waitForAjax();
        });
    }

    public void loadImageManualLoading(String requiredFormat) {
            $("[class='upload-manual-image-form-file'][name*='" + requiredFormat + "']").sendKeys(LINK_TO_RESOURCES_IMAGES + image);
            waitForAjax();
            checkErrors();
    }

    public boolean checkTitleInTeasersListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserListHelper.checkTitle(title));
    }

    public boolean checkDescriptionInTeasersListInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserListHelper.checkDescription(description));
    }

    public boolean checkTitleInTeasersEditInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserAddHelper.checkTitle(title));
    }

    public boolean checkDescriptionInTeasersEditInterface() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(teaserAddHelper.checkDescription(description));
    }

    public void clickGetAutoTitlesButton() {
        GET_AUTO_TITLE_BUTTON.click();
        checkErrors();
    }

    public CabTeasers fillDomain(String value) {
        teaserAddHelper.setUrl(value);
        return this;
    }

    public CabTeasers fillTitle(String value) {
        teaserAddHelper.setTitle(value);
        return this;
    }

    public void fillImageField(String value) {
        IMAGE_INPUT.sendKeys(value);
    }

    public CabTeasers fillAdvertText(String value) {
        teaserAddHelper.setDescription(value);
        return this;
    }

    public String getTextFromTitleField() {
        return TITLE_INPUT.val();
    }

    public boolean checkSelectedSourceTitleSuggestion() {
        return new HashSet<>(TITLE_SUGGESTION_SELECT.$$("option").texts()).containsAll(Arrays.asList("Facebook", "Outbrain"));
    }

    public boolean isDisplayedGetAutoTitlesButton() {
        return GET_AUTO_TITLE_BUTTON.isDisplayed();
    }

    public CabTeasers selectTitleSugestionType(String...value) {
        sourceTitleSuggestion = value.length > 0 ? helpersInit.getBaseHelper().selectCustomValueReturnText(TITLE_SUGGESTION_SELECT, value[0]) : helpersInit.getBaseHelper().selectRandomValue(TITLE_SUGGESTION_SELECT);
        return this;
    }

    public int getAmountResetButtons() {
        return RESET_DATA_BUTTONS.size();
    }

    public void resetAllFieldsByResetButtons() {
        RESET_DATA_BUTTONS.asFixedIterable().forEach(SelenideElement::click);
    }

    public String getDomainValue() {
        return URL_INPUT.val();
    }

    public String getTitleValue() {
        return TITLE_INPUT.val();
    }

    public String getAdvertValue() {
        return DESCRIPTION_INPUT.val();
    }

    public String getImageLinkValue() {
        return IMAGE_INPUT.val();
    }

    public boolean isTeaserPresent(int clientIdValue, int teaserIdValue) {
        return $(String.format(TEASERS_ID_LOCATOR, clientIdValue, teaserIdValue)).isDisplayed();
    }

    public void fillProductPrice(String price) {
        teaserAddHelper.fillProductPrice(price);
    }

    public boolean checkMainScreenshot(List<String> list) {
        MAIN_SCREENSHOT_BLOCK.shouldBe(CollectionCondition.allMatch("sdf", i -> list.contains(i.getAttribute("href"))));
        return helpersInit.getBaseHelper().getTextAndWriteLog(MAIN_SCREENSHOT_BLOCK.size() == list.size());
    }

    public boolean checkAllPercentage(String val) {
        PERCENTAGE_BLOCK.shouldBe(CollectionCondition.allMatch("checkAllPercentage", i -> i.getText().equals(val)));
        return true;
    }

    public void openCopyLeaksScreenshotPopup() {
        PLAGIARISM_ICON.click();
        waitForAjax();
        PLAGIARISM_POPUP.shouldBe(visible);
    }

    public boolean isDisplayedPlagiarismIcon() {
        return PLAGIARISM_ICON.isDisplayed();
    }

    public String getTeasersAdvertiserName(String teaserId) {
        return $x(String.format(ADVERTISER_NAME_FIELD, teaserId)).getText();
    }

    public String getTeasersAdvertiserNameTemp(String teaserId) {
        return $(String.format(ADVERTISER_NAME_TEMP_FIELD, teaserId)).shouldBe(visible).getText();
    }

    public void changeAdvertiserName(String teaserId, String newValue) {
        $(String.format(ADVERTISER_NAME_TEMP_FIELD, teaserId)).doubleClick();
        sendKey($x(String.format(ADVERTISER_NAME_INPUT, teaserId)).shouldBe(visible), newValue + Keys.ENTER);
        waitForAjax();
        waitForAjaxLoader();
        sleep(1000);
    }

    public boolean isEditableAdvertiserNameField(String teaserId) {
        return $(String.format(ADVERTISER_NAME_TEMP_FIELD, teaserId)).isDisplayed();
    }

    public void filterByAutoModeration(String val) {
        FILTER_BY_AUTOMODERATE.selectOptionByValue(val);
        FILTER_BUTTON.click();
        checkErrors();
        waitForAjax();
    }

    public List<String> getListOfDislpayedTeaserIds() {
        return TEASER_IDS_LOCATOR.texts().stream().map(String::trim).collect(Collectors.toList());
    }

    public void editCategoryInline(String categoryId) {
        CATEGORY_LABEL.doubleClick();
        waitForAjax();
        sleep(2000);
        helpersInit.getBaseHelper().selectValueFromParagraphListJs(CATEGORY_SELECT_INLINE, categoryId);
        SUBMIT_CATEGORY_POPUP.click();
        waitForAjax();
        checkErrors();
    }

    public boolean isDisplayedVerifyIcon(String val) {
        return $("img[src*='moderationVerified" + val + ".svg'").isDisplayed();
    }

    public boolean isDisplayedMirroringImageIcon(String val) {
        return $("img[src*='teaserMirrorImage" + val + ".svg'").isDisplayed();
    }

    public String getMirroringImagesToolTip() {
        return MIRRORING_IMAGES_LABEL.attr("title");
    }

    public void clickMirroringImages() {
        MIRRORING_IMAGES_LABEL.scrollTo().click();
        waitForAjax();
    }

    public boolean isDisplayedMirroringImagesIcon() {
        return MIRRORING_IMAGES_LABEL.isDisplayed();
    }

    public boolean isDisplayedNotNeedVerificationOption() {
        return NOT_NEED_VERIFICATION.isDisplayed();
    }


    public boolean isDisplayedAutoModerationFilter() {
        return FILTER_BY_AUTOMODERATE.isDisplayed();
    }

    public void clickVerifyIcon() {
        VERIFY_ICON.click();
        waitForAjax();
    }

    public void indicateFocalPoint() {
        if(focalPointState) {
            setFocalPoint(true);
        }
    }

    public void setFocalPoint(boolean isSaveData) {
        MANUAL_FOCAL_POINT_BUTTON.click();
        FOCAL_POINT_ICON.shouldBe(visible);
        sleep(500);
        executeJavaScript("document.getElementById('crosshair').style.left = '" + focalPointLeft + "px'; document.getElementById('crosshair').style.top = '-" + focalPointTop + "px'");
        sleep(500);
        waitForAjax();
        if (isSaveData) MANUAL_FOCAL_CONFIRM_BUTTON.click();
        waitForAjax();
    }

    public void cancelManualFocalPoint() {
        MANUAL_FOCAL_CANCEL_BUTTON.click();
        waitForAjax();
        checkErrors();
        MANUAL_FOCAL_CANCEL_BUTTON.shouldBe(hidden);
    }

    public boolean isDisplayedFocalPointIcon() {
        return FOCAL_POINT_ICON.isDisplayed();
    }

    public void setFocalPointToDefault() {
        MANUAL_FOCAL_DEFAULT_BUTTON.shouldBe(visible).click();
        waitForAjax();
        checkErrors();
        MANUAL_FOCAL_DEFAULT_BUTTON.shouldBe(hidden);
    }

    public void editDataFocalPoint() {
        MANUAL_FOCAL_EDIT_BUTTON.click();
        MANUAL_FOCAL_EDIT_BUTTON.shouldBe(hidden);
        MANUAL_FOCAL_CANCEL_BUTTON.shouldBe(visible);
        sleep(1000);
        waitForAjax();
        checkErrors();
    }

    public boolean isDisplayedFocalPointManualButton() {
        return MANUAL_FOCAL_POINT_BUTTON.isDisplayed();
    }

    public boolean isDisplayedFocalPointDefaultButton() {
        return MANUAL_FOCAL_DEFAULT_BUTTON.isDisplayed();
    }

    public boolean isDisplayedDownloadGettyImages() {
        return DOWNLOADED_GETTY_IMAGES_CHECKBOX.isDisplayed();
    }

    public boolean isDisplayedModerationActionFilter() {
        return FILTER_BY_MODERATOR_ACTION.isDisplayed();
    }

    public boolean checkCsrMediumIconIsDisplayed() {
        return CSR_MEDIUM_ICON.isDisplayed();
    }

    public boolean checkCsrHighIconIsDisplayed() {
        return CSR_HIGH_ICON.isDisplayed();
    }

    public void openTagsCloudFormByMass() {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("changeTags");
    }

    public boolean isDisplayedTitleTagsBlockMass() {
        return TITLE_TAGS_BLOCK_MASS.isDisplayed();
    }

    public boolean isDisplayedTitleTagsBlockSingle() {
        return TITLE_TAGS_BLOCK_SINGLE.isDisplayed();
    }

    public void selectTypeOfFormatTeaserViewBlock(String value) {
        FORMAT_VIEW_BLOCK.selectOptionByValue(value);
    }
    public void selectSortFilesForViewBlockBy(String value) {
        SORT_BY_FOR__VIEW_BLOCK.selectOptionByValue(value);
    }

    public void clickNextPageOfGalleryFiles() {
        NEXT_PAGE_GALLERY_BUTTON.click();
    }

    public void acceptAutoCategory() {
        ACCEPT_AUTO_CATEGORY_BUTTON.click();
        ACCEPT_AUTO_CATEGORY_BUTTON.shouldBe(hidden);
        checkErrors();
    }

    public void rejectAutoCategory() {
        REJECT_AUTO_CATEGORY_BUTTON.click();
        REJECT_AUTO_CATEGORY_BUTTON.shouldBe(hidden);
        checkErrors();
    }

    public boolean checkDisplayedCategory(String category) {
        return helpersInit.getBaseHelper().checkDatasetEquals(CATEGORY_NAME_LABEL.text(), category);
    }

    public boolean isDisplayedAutoCategory() {
        return AUTO_CATEGORY_LABEL.isDisplayed();
    }

    public void setNewTeasersCpcMass(int priceValue) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("setpoc");
        teaserListHelper.setNewPrice(priceValue);
        teaserListHelper.submitMassActions();
    }
    public void increaseTeasersCpcMass(int priceValue) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("uppoc");
        teaserListHelper.setNewPrice(priceValue);
        teaserListHelper.submitMassActions();
    }
    public void decreaseTeasersCpcMass(int priceValue) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions("downpoc");
        teaserListHelper.setNewPrice(priceValue);
        teaserListHelper.submitMassActions();
    }

    public void setTeasersKycMass(String kycType) {
        teaserListHelper.selectAllTeasers();
        teaserListHelper.chooseMassActions(kycType);
        teaserListHelper.submitMassActions();
    }

    public boolean checkTeasersCpc(String newTeaserPrice) {
        return CPC_INPUTs.asDynamicIterable().stream().allMatch(i -> Objects.requireNonNull(i.getAttribute("value")).contains(newTeaserPrice));
    }

    public void filterTeasersByKyc(KycTypes kycTypeValue) {
        switch (kycTypeValue) {
            case NOT_VERIFIED -> FILTER_BY_KYC_NOT_VERIFIED_CHECKBOX.click();
            case VERIFIED -> FILTER_BY_KYC_VERIFIED_CHECKBOX.click();
            case BLOCKED -> FILTER_BY_KYC_BLOCKED_CHECKBOX.click();
            case REJECT -> FILTER_BY_KYC_REJECT_CHECKBOX.click();
            default -> log.info("kyc type \"" + kycTypeValue + "\" doesn't exist");
        }
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public boolean checkKycIconIsDisplayed(KycTypes kycTypeValue) {
        TEASER_ID_LOCATOR.shouldBe(visible);
        return switch (kycTypeValue) {
            case NOT_VERIFIED -> KYC_NOT_VERIFIED_ICON.isDisplayed();
            case VERIFIED -> KYC_VERIFIED_ICON.isDisplayed();
            default -> false;
        };
    }

    public void clickKycNotVerifiedIcon() {
        KYC_NOT_VERIFIED_ICON.shouldBe(visible).click();
        waitForAjaxLoader();
        waitForAjax();
        checkErrors();
    }

    public void clickKycVerifiedIcon() {
        KYC_VERIFIED_ICON.shouldBe(visible).click();
        waitForAjax();
        waitForAjaxLoader();
        checkErrors();
    }

    public Integer getAmountOfKycVerifiedIcons() {
        return KYC_VERIFIED_ICONS.size();
    }

    public Integer getAmountOfKycNotVerifiedIcons() {
        return KYC_NOT_VERIFIED_ICONS.size();
    }
}
