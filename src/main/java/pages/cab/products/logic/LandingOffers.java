package pages.cab.products.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.LandingOffersCreateEditHelper;
import pages.cab.products.helpers.LandingOffersListHelper;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.sleep;
import static pages.cab.products.locators.LandingOffersCreateEditLocators.*;
import static pages.cab.products.variables.LandingOffersVariables.messageRemoveCertificate;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class LandingOffers {
    private String productName;
    private String pageLink;
    private String offerManafactured;
    private String certificate = null;
    private String whiteListMgid;
    private List<String> whiteListCountriesMgidResult;
    private String[] whiteListCountriesMgidSource;
    private List<String> blackListCountriesAdskeeperResult;
    private String[] blackListCountriesAdskeeperSource;
    private String whiteListAdskeeper;
    private String blackListMgid;
    private String blackListAdskeeper;
    private String linkGoToTeaserIcon;
    private final LandingOffersCreateEditHelper landingOffersCreateEditHelper;
    private final LandingOffersListHelper landingOfferListHelper;

    private final HelpersInit helpersInit;

    public LandingOffers(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        landingOffersCreateEditHelper = new LandingOffersCreateEditHelper(helpersInit);
        landingOfferListHelper = new LandingOffersListHelper(log, helpersInit);
    }

    public LandingOffers setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public String getLinkGoToTeaserIcon() {
        return linkGoToTeaserIcon;
    }

    public LandingOffers setGenerateNewValues(boolean generateNewValues) {
        landingOffersCreateEditHelper.setGenerateNewValues(generateNewValues);
        return this;
    }


    public LandingOffers clearProductDataValues() {
        this.productName = null;
        this.pageLink = null;
        this.whiteListMgid = null;
        this.whiteListCountriesMgidSource = null;
        this.blackListCountriesAdskeeperSource = null;
        this.whiteListAdskeeper = null;
        this.blackListMgid = null;
        this.blackListAdskeeper = null;
        this.certificate = null;
        return this;
    }

    public LandingOffers clearGeoSettings() {
        landingOffersCreateEditHelper.clearGeoSettings();
        return this;
    }

    public LandingOffers setPageLink(String pageLink) {
        this.pageLink = pageLink;
        return this;
    }

    public LandingOffers setCertificate(String certificate) {
        this.certificate = certificate;
        return this;
    }

    public LandingOffers setWhiteListMgid(String whiteListMgid) {
        this.whiteListMgid = whiteListMgid;
        return this;
    }

    public LandingOffers setWhiteListAdskeeper(String whiteListAdskeeper) {
        this.whiteListAdskeeper = whiteListAdskeeper;
        return this;
    }

    public LandingOffers setBlackListMgid(String blackListMgid) {
        this.blackListMgid = blackListMgid;
        return this;
    }

    public void setBlackListAdskeeper(String blackListAdskeeper) {
        this.blackListAdskeeper = blackListAdskeeper;
    }

    public LandingOffers setWhiteListCountriesMgidSource(String[] whiteListCountriesMgid) {
        this.whiteListCountriesMgidSource = whiteListCountriesMgid;
        return this;
    }


    /**
     * create product with required fields
     */
    public void createProduct() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        whiteListMgid = landingOffersCreateEditHelper.chooseWhiteListMgid(whiteListMgid);
        whiteListCountriesMgidResult = landingOffersCreateEditHelper.chooseWhiteListCountriesMgid(whiteListCountriesMgidSource);
        whiteListAdskeeper = landingOffersCreateEditHelper.chooseWhiteListAdskeeper(whiteListAdskeeper, whiteListMgid);
        blackListMgid = landingOffersCreateEditHelper.chooseBlackListMgid(blackListMgid, whiteListMgid);
        blackListAdskeeper = landingOffersCreateEditHelper.chooseBlackListAdskeeper(blackListAdskeeper, whiteListAdskeeper, blackListMgid);
        blackListCountriesAdskeeperResult = landingOffersCreateEditHelper.chooseBlackListCountriesAdskeeper(blackListCountriesAdskeeperSource);

        landingOffersCreateEditHelper.saveOffer();
    }

    public void fillProductName(String productName) {
        this.productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        landingOffersCreateEditHelper.fillPageLinkField(pageLink);
    }

    /**
     * open form create product
     */
    public void openCreateProductForm() {
        landingOfferListHelper.openCreateProductForm();
    }

    /**
     * find product by name at list interface
     */
    public void findProduct(String productName) {
        landingOfferListHelper.findProduct(productName);
    }

    /**
     * delete product and check success message
     */
    public void deleteProduct() {
        landingOfferListHelper.deleteProduct();
    }

    /**
     * open product form for edit
     */
    public void openEditProductForm() {
        landingOfferListHelper.openEditProductForm();
    }

    /**
     * check product at list interface
     */
    public boolean checkProductListInterface() {
        return landingOfferListHelper.checkWhiteListGeoMgid(whiteListMgid, whiteListCountriesMgidResult) &&
                landingOfferListHelper.checkBlackListGeoMgid(blackListMgid) &&
                landingOfferListHelper.checkWhiteListGeoAdskeeper(whiteListAdskeeper) &&
                landingOfferListHelper.checkBlackListGeoAdskeeper(blackListAdskeeper, blackListCountriesAdskeeperResult);
    }
    /**
     * check all black geo setting product list
     */
    public boolean checkAllBlackGeoProductListInterface() {
        return landingOfferListHelper.checkBlackListGeoMgid(blackListMgid) &&
                landingOfferListHelper.checkBlackListGeoAdskeeper(blackListAdskeeper, blackListCountriesAdskeeperResult);
    }
    /**
     * check settings of product created earlier
     */
    public boolean checkCreatedProduct() {
        return landingOffersCreateEditHelper.checkProductName(productName) &
                landingOffersCreateEditHelper.checkPageLink(pageLink) &
                landingOffersCreateEditHelper.checkPageLink(pageLink) &
                landingOffersCreateEditHelper.checkWhiteListMgid(whiteListMgid) &
                landingOffersCreateEditHelper.checkWhiteListCountriesMgid(whiteListCountriesMgidResult, whiteListMgid) &
                landingOffersCreateEditHelper.checkWhiteListAdskeeper(whiteListAdskeeper) &
                landingOffersCreateEditHelper.checkBlackListMgid(blackListMgid) &
                landingOffersCreateEditHelper.checkBlackListAdskeeper(blackListAdskeeper) &
                landingOffersCreateEditHelper.checkDisplayedCertificates(certificate) &
                landingOffersCreateEditHelper.checkBlackListCountriesAdskeeper(blackListCountriesAdskeeperResult, blackListAdskeeper);
    }

    /**
     * edit settings of product
     */
    public void editProduct() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        deleteUploadedCertificates();
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        whiteListMgid = landingOffersCreateEditHelper.chooseWhiteListMgid(whiteListMgid);
        whiteListCountriesMgidResult = landingOffersCreateEditHelper.chooseWhiteListCountriesMgid(whiteListCountriesMgidSource);
        whiteListAdskeeper = landingOffersCreateEditHelper.chooseWhiteListAdskeeper(whiteListAdskeeper, whiteListMgid);
        blackListMgid = landingOffersCreateEditHelper.chooseBlackListMgid(blackListMgid, whiteListMgid);
        blackListAdskeeper = landingOffersCreateEditHelper.chooseBlackListAdskeeper(blackListAdskeeper, whiteListAdskeeper, blackListMgid);
        blackListCountriesAdskeeperResult = landingOffersCreateEditHelper.chooseBlackListCountriesAdskeeper(blackListCountriesAdskeeperSource);

        landingOffersCreateEditHelper.saveOffer();
    }

    public void deleteUploadedCertificates() {
        REMOVE_CERTIFICATE_ICON.asFixedIterable().forEach(element->{
            element.click();
            helpersInit.getMessageHelper().checkCustomMessagesCab(messageRemoveCertificate);
        });
    }

    /**
     * check visibility add product button
     */
    public boolean checkVisibilityAddProductButton() {
        return landingOfferListHelper.checkVisibilityAddProductButton();
    }

    /**
     * check visibility delete product button
     */
    public boolean checkVisibilityDeleteProductButton() {
        return landingOfferListHelper.checkVisibilityDeleteProductButton();
    }

    /**
     * check visibility search results
     */
    public boolean checkVisibilitySearchResults() {
        return landingOfferListHelper.checkVisibilitySearchResults();
    }

    /**
     * check visibility edit product button
     */
    public boolean checkVisibilityEditProductButton() {
        return landingOfferListHelper.checkVisibilityEditProductButton();
    }

    /**
     * check displayed error message
     */
    public boolean checkErrorMessageField(String message) {
        return landingOfferListHelper.checkErrorMessageField(message);
    }

    /**
     * create product without Adskeeper geo settings
     */
    public void createProductWithoutAdskeepersGeoSettings(String subnet) {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        whiteListMgid = landingOffersCreateEditHelper.chooseWhiteListMgid(whiteListMgid);
        whiteListCountriesMgidResult = landingOffersCreateEditHelper.chooseWhiteListCountriesMgid(whiteListCountriesMgidSource);
        landingOffersCreateEditHelper.inactiveSelectedRegionsBySubnet(subnet);

        landingOffersCreateEditHelper.saveOffer();
    }

    /**
     * create product without Adskeeper geo settings
     */
    public void createProductWithoutMgidGeoSettings(String subnet) {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        whiteListMgid = landingOffersCreateEditHelper.chooseWhiteListAdskeeper(whiteListAdskeeper, whiteListMgid);
        landingOffersCreateEditHelper.inactiveSelectedRegionsBySubnet(subnet);

        landingOffersCreateEditHelper.saveOffer();
    }


    /**
     * create product with all white geo settings
     */
    public void createProductWithAllSubnetGeoSettings() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        landingOffersCreateEditHelper.chooseAllWhiteGeoForAllSubnets();
        landingOffersCreateEditHelper.saveOffer();
    }
    /**
     * create product with all black geo settings
     */
    public void createProductWithAllBlackSubnetGeoSettings() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        landingOffersCreateEditHelper.chooseAllBlackGeoForAllSubnets();
        landingOffersCreateEditHelper.saveOffer();
    }
    /**
     * choose Casino and Gambling for Mgid
     */
    public void chooseAllBlackGeoForAllSubnets() {
        landingOffersCreateEditHelper.chooseAllBlackGeoForAllSubnets();
    }

    /**
     * choose Casino and Gambling for Mgid
     */
    public void chooseCasinoAndGamblingForMgid() {
        landingOffersCreateEditHelper.chooseCasinoAndGamblingForMgid();
    }
    /**
     * choose Casino and Gambling for Adskeeper
     */
    public void chooseCasinoAndGamblingForAdskeeper() {
        landingOffersCreateEditHelper.chooseCasinoAndGamblingForAdskeeper();
    }
    /**
     * choose Casino and Gambling for all Subnets
     */
    public void chooseCasinoAndGamblingAllSubnets() {
        landingOffersCreateEditHelper.chooseCasinoAndGamblingAllSubnets();
    }
    /**
     * reject Casino and Gambling for all Subnets
     */
    public void rejectCasinoAndGamblingAllSubnets() {
        landingOffersCreateEditHelper.rejectCasinoAndGamblingAllSubnets();
    }
    /**
     * check Casino and Gambling for Mgid
     */
    public void checkCasinoAndGamblingForMgid() {
        landingOffersCreateEditHelper.checkCasinoAndGamblingForMgid();
    }
    /**
     * check Casino and Gambling for Adskeeper
     */
    public void checkCasinoAndGamblingForAdskeeper() {
        landingOffersCreateEditHelper.checkCasinoAndGamblingForAdskeeper();
    }
    /**
     * check Casino and Gambling for all Subnets
     */
    public void checkCasinoAndGamblingAllSubnetsCheckbox() {
        landingOffersCreateEditHelper.checkCasinoAndGamblingAllSubnetsCheckbox();
    }

    /**
     * create product Casino and Gambling for Mgid
     */
    public void createProductWithCasinoAndGamblingForMgid() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        landingOffersCreateEditHelper.chooseCasinoAndGamblingForMgid();


        landingOffersCreateEditHelper.saveOffer();
    }

    /**
     * create product with Casino and Gambling for Adskeeper
     */
    public void createProductWithCasinoAndGamblingForAdskeeper() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        whiteListMgid = landingOffersCreateEditHelper.chooseWhiteListMgid(whiteListMgid);
        landingOffersCreateEditHelper.chooseCasinoAndGamblingForAdskeeper();



        landingOffersCreateEditHelper.saveOffer();
    }

    /**
     * create product with all Casino and Gambling subnets
     */
    public void createProductWithCasinoAndGamblingAllSubnets() {
        productName = landingOffersCreateEditHelper.fillProductNameField(productName);
        pageLink = landingOffersCreateEditHelper.fillPageLinkField(pageLink);
        landingOffersCreateEditHelper.uploadCertificate(certificate);
        offerManafactured = landingOffersCreateEditHelper.fillOfferManufactured(offerManafactured);
        landingOffersCreateEditHelper.chooseCasinoAndGamblingAllSubnets();


        landingOffersCreateEditHelper.saveOffer();
    }
    /**
     * check all white geo settings
     */
    public boolean checkWhiteListGeo() {
        return landingOffersCreateEditHelper.checkWhiteListGeo();
    }

    /**
     * get chosen elements mgid
     *
     */
    public List<String> getMarkedRegionsMgid() {
        return landingOffersCreateEditHelper.markedRegionsMgid();
    }

    /**
     * is active united states cdb
     */
    public boolean isMarkedUnitedStatesCdb(String region) {
        return landingOffersCreateEditHelper.isMarkedUnitedStatesCdb(region);
    }

    /**
     * check error message product form
     */
    public boolean checkErrorMessageProductForm(String message) {
        return landingOffersCreateEditHelper.checkErrorMessageProductForm(message);
    }

    /**
     * check selected regions
     */
    public boolean checkMgidAndAdskeeperRegions() {
        return landingOffersCreateEditHelper.checkWhiteListMgid(whiteListMgid) &&
                landingOffersCreateEditHelper.checkWhiteListAdskeeper(whiteListAdskeeper) &&
                landingOffersCreateEditHelper.checkBlackListMgid(blackListMgid) &&
                landingOffersCreateEditHelper.checkBlackListAdskeeper(blackListAdskeeper);
    }

    /**
     * get link from go to teasers icon
     */
    public String getLinkOfLinkRedirection() {
        return linkGoToTeaserIcon = landingOfferListHelper.getLinkOfLinkRedirection();
    }

    public int getAmountOfCertificates() {
        return REMOVE_CERTIFICATE_ICON/*.shouldHave(CollectionCondition.sizeGreaterThan(expectedSize - 1))*/.size();
    }

    public boolean checkVisibilityUploadButton() {
        return UPLOAD_CERTIFICATES_BUTTON.isDisplayed();
    }

    public boolean checkNameOfCertificate(String name) {
        return helpersInit.getBaseHelper().checkDatasetContains(name, $x(String.format(CERTIFICATE_NAME_LABEL, name)).text());
    }

    public void uploadCertificates(String...images) {
        if (images != null) {
            Arrays.stream(images).forEach(element -> {
                UPLOAD_CERTIFICATES_BUTTON.sendKeys(element);
                sleep(1000);

            });
        }
    }

    public void downloadCertificate() {
        DOWNLOAD_CERTIFICATE_BUTTON.click();
    }

    public boolean isDisplayedDownloadCertificateButton() {
        return DOWNLOAD_CERTIFICATE_BUTTON.isDisplayed();
    }

    public void filterByCertificate() {
        FILTER_BY_CERTIFICATE.click();
        checkErrors();
        helpersInit.getBaseHelper().amountOfDisplayedEntitiesPerPage("50");
        waitForAjax();
    }

    public int getAmountOfProducts() {
        return OFFERS_AMOUNT.size();
    }

    public boolean isDisplayedShowNameIcon() {
        return ADVERT_NAME_ICON.isDisplayed();
    }

    public String getTipShowNameIcon() {
        return ADVERT_NAME_ICON.attr("title");
    }
}
