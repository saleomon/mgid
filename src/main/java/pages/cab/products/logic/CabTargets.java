package pages.cab.products.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.products.helpers.TargetAddHelper;
import pages.cab.products.helpers.TargetListHelper;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;
import static pages.cab.products.locators.TargetListLocators.TARGET_ID_LOCATOR;

public class CabTargets {
    private final HelpersInit helpersInit;
    private final TargetAddHelper targetAddHelper;
    private final TargetListHelper targetListHelper;

    public CabTargets(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        targetAddHelper = new TargetAddHelper(log, helpersInit);
        targetListHelper = new TargetListHelper(log, helpersInit);
    }

    /**
     * Clients target types
     */
    public enum TargetType {
        PAGE_LINK("url"),
        WEBSITE_VISITOR("visited"),
        EVENT("event"),
        REGEXP("regexp"),
        POSTBACK("postback");

        private final String targetTypeValue;

        TargetType(String targetTypeValue) {
            this.targetTypeValue = targetTypeValue;
        }

        public String getTargetType() {
            return targetTypeValue;
        }

        static public TargetType getTargetType(String targetType) {
            for (TargetType type : TargetType.values()) {
                if (type.getTargetType().equals(targetType)) {
                    return type;
                }
            }
            throw new RuntimeException("Unknown target type");
        }
    }

    public enum UrlType {
        START_FROM("starts"),
        CONTAINS("contain"),
        ENDS_WITH("ends");
        private final String urlTypeValue;

        public String getUrlType() {
            return urlTypeValue;
        }

        UrlType(String urlTypeValue) {
            this.urlTypeValue = urlTypeValue;
        }

        static public UrlType getUrlType(String urlType) {
            for (UrlType type : UrlType.values()) {
                if (type.getUrlType().equals(urlType)) {
                    return type;
                }
            }
            throw new RuntimeException("Unknown URL type");
        }
    }

    private String targetType;
    private String targetConversionCategory;
    private String targetName;
    private String targetDescription;
    private String[] targetPageLink;
    private String[] targetWebsiteVisitors;
    private String targetIdentifier;
    private String targetRegExp;
    private String targetPostbackEventName;
    private String targetId;

    public CabTargets setTargetName(String targetName) {
        this.targetName = targetName;
        return this;
    }

    public CabTargets setTargetConversionCategory(String targetConversionCategory) {
        this.targetConversionCategory = targetConversionCategory;
        return this;
    }

    public CabTargets setTargetType(String targetType) {
        this.targetType = targetType;
        return this;
    }

    /**
     * @param targetPageLink available values {"starts", "contain", "ends", ""}
     */
    public CabTargets setTargetPageLink(String[] targetPageLink) {
        this.targetPageLink = targetPageLink;
        return this;
    }

    public CabTargets setTargetWebsiteVisitors(String[] targetWebsiteVisitors) {
        this.targetWebsiteVisitors = targetWebsiteVisitors;
        return this;
    }

    public CabTargets setTargetIdentifier(String targetIdentifier) {
        this.targetIdentifier = targetIdentifier;
        return this;
    }

    public CabTargets setTargetRegExp(String targetRegExp) {
        this.targetRegExp = targetRegExp;
        return this;
    }

    public CabTargets setTargetPostbackEventsName(String targetPostbackEventName) {
        this.targetPostbackEventName = targetPostbackEventName;
        return this;
    }

    public CabTargets setSomeFields(boolean setSomeFields) {
        targetAddHelper.setSomeFields(setSomeFields);
        return this;
    }

    /**
     * Reset all target variables
     */
    public CabTargets resetAllVariables() {
        targetName = null;
        targetDescription = null;
        targetConversionCategory = null;
        targetType = null;
        targetPageLink = null;
        targetWebsiteVisitors = null;
        targetIdentifier = null;
        targetRegExp = null;
        targetPostbackEventName = null;
        return this;
    }

    public String getTargetId() {
        return targetId;
    }

    /**
     * Add new clients target
     */
    public void addTarget() {
        targetListHelper.addNewTarget();
        setTargetSettings();
    }

    /**
     * Set target settings
     */
    public void setTargetSettings() {
        targetConversionCategory = targetAddHelper.selectConversionCategory(targetConversionCategory);
        targetType = targetAddHelper.chooseTargetType(targetType);
        targetName = targetAddHelper.setTargetName(targetName, targetType);
        targetDescription = targetAddHelper.setTargetDescription(targetDescription, targetType);

        TargetType targetType = TargetType.getTargetType(this.targetType);

        switch (targetType) {
            case PAGE_LINK -> targetPageLink = targetAddHelper.setPageLinkTarget(targetPageLink);
            case WEBSITE_VISITOR -> targetWebsiteVisitors = targetAddHelper.setWebsiteVisitorTarget(targetWebsiteVisitors);
            case EVENT -> targetIdentifier = targetAddHelper.setEventTarget(targetIdentifier);
            case REGEXP -> targetRegExp = targetAddHelper.setRegExpTarget(targetRegExp);
            case POSTBACK -> {
                targetPostbackEventName = targetAddHelper.setPostbackTarget(targetPostbackEventName);
                targetName = "Postback " + targetPostbackEventName;
            }
        }
        targetAddHelper.submitTarget();
    }

    /**
     * Get and save target id
     */
    public void readAndSaveTargetId() {
        targetId = $x(String.format(TARGET_ID_LOCATOR, targetName)).shouldBe(visible, ofSeconds(10)).text();
        helpersInit.getBaseHelper().getTextAndWriteLog(targetId);
    }

    /**
     * Check clients target
     */
    public boolean checkClientTarget() {
        boolean result;
        TargetType type = TargetType.getTargetType(targetType);
        result = switch (type) {
            case PAGE_LINK -> targetAddHelper.checkPageLinkTarget(targetPageLink);
            case WEBSITE_VISITOR -> targetAddHelper.checkWebsiteVisitorTarget(targetWebsiteVisitors);
            case EVENT -> targetAddHelper.checkEventTarget(targetIdentifier);
            case REGEXP -> targetAddHelper.checkRegExpTarget(targetRegExp);
            case POSTBACK -> targetAddHelper.checkPostbackTarget(targetPostbackEventName);
        };
        return helpersInit.getBaseHelper().getTextAndWriteLog(result);
    }

    /**
     * Edit clients target
     */
    public void editTarget() {
        setTargetSettings();
    }

    /**
     * Delete client target
     */
    public boolean deleteClientTarget() {
        return targetListHelper.deleteClientTarget(targetId);
    }

}
