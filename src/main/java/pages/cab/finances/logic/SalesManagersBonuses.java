package pages.cab.finances.logic;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;

import static com.codeborne.selenide.Condition.visible;
import static pages.cab.finances.locators.SalesManagersBonusesLocators.*;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;


public class SalesManagersBonuses {

    private final HelpersInit helpersInit;
    Logger log;

    public SalesManagersBonuses(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.log = log;
    }

    public SalesManagersBonuses chooseFilterUser(String user) {
        helpersInit.getBaseHelper().selectCustomValue(USER_SELECT, user);
        return this;
    }

    public SalesManagersBonuses chooseFilterMonth(String month) {
        helpersInit.getBaseHelper().selectCustomText(MONTH_SELECT, month);
        return this;
    }

    public SalesManagersBonuses chooseFilterYear(String year) {
        helpersInit.getBaseHelper().selectCustomValue(YEAR_SELECT, year);
        return this;
    }

    public void filterData() {
        FILTER_BUTTON.click();
        waitForAjax();
        checkErrors();
    }

    public void clickButtonCsvSalesBonusesCab() {
        EXPORT_BUTTON_SALES_BONUSES_CAB.shouldBe(visible).click();
    }

    public void openPopupWithGrades() {
        POPUP_GRADES_ICON.click();
        checkErrors();
    }

    public void confirmPopup() {
        CONFIRM_POPUP.click();
    }

    public boolean isDisplayPopupWithGrades(SelenideElement elem) {
        return elem.isDisplayed();
    }

    public boolean isDisplayIconPopupWithGrades() {
        return POPUP_GRADES_ICON.isDisplayed();
    }
}
