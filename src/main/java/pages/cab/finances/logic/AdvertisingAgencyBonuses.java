package pages.cab.finances.logic;

import core.base.HelpersInit;

import static core.helpers.ErrorsHelper.checkErrors;
import static pages.cab.finances.locators.AdvertisingAgencyBonusesLocators.*;

public class AdvertisingAgencyBonuses {

    private final HelpersInit helpersInit;

    public AdvertisingAgencyBonuses(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    /**
     * choose 'Status' filter
     */
    public AdvertisingAgencyBonuses chooseStatusFilter(String status) {
        helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(STATUS_SELECT, status));
        return this;
    }

    /**
     * choose 'Subnet' filter
     */
    public AdvertisingAgencyBonuses chooseSubnetFilter(String subnet) {
        helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(SUBNET_SELECT, subnet));
        return this;
    }

    /**
     * choose 'MCM' filter
     */
    public AdvertisingAgencyBonuses chooseMcmFilter(String mcm) {
        helpersInit.getBaseHelper().getTextAndWriteLog(helpersInit.getBaseHelper().selectCustomText(MSM_SELECT, mcm));
        return this;
    }

    /**
     * click 'Filter' button
     */
    public void clickFilterButton() {
        FILTER_SUBMIT_BUTTON.click();
        checkErrors();
    }

    /**
     * get amount of advertiser agency at list
     */
    public int getAmountDisplayedAgencies() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(AGENCY_BONUSES_FIELD_IDS.size());
    }

    /**
     * check displayed MCM Agency icon
     */
    public boolean checkMcmAgencyIconVisibility() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MSM_AA_ICON.isDisplayed());
    }

    /**
     * get amount of MCM Agency icons at list
     */
    public int getAmountMcmAgencyIcons() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(MSM_AA_ICONS.size());
    }
}
