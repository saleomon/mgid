package pages.cab.finances.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class AdvertisingAgencyBonusesLocators {

    public static final SelenideElement SUBNET_SELECT = $("#subnet");
    public static final SelenideElement STATUS_SELECT = $("#status");
    public static final SelenideElement MSM_SELECT = $("#mcm_aa");
    public static final SelenideElement FILTER_SUBMIT_BUTTON = $("#btnsubmit");
    public static final ElementsCollection AGENCY_BONUSES_FIELD_IDS = $$("#content > table > tbody > tr");
    public static final SelenideElement MSM_AA_ICON = $("img[src*='mcmAaFlag.png']");
    public static final ElementsCollection MSM_AA_ICONS = $$("img[src*='mcmAaFlag.png']");
}
