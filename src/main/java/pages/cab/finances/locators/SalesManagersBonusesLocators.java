package pages.cab.finances.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class SalesManagersBonusesLocators {
    public static final SelenideElement USER_SELECT = $("[id='manager']");
    public static final SelenideElement MONTH_SELECT = $("[id='month']");
    public static final SelenideElement YEAR_SELECT = $("[id='year']");
    public static final SelenideElement FILTER_BUTTON = $("[id='btnsubmit']");
    public static final SelenideElement EXPORT_BUTTON_SALES_BONUSES_CAB = $x("//*[text()='Export Excel']");
    public static final SelenideElement POPUP_GRADES_ICON = $("img[src*='grades']");
    public static final SelenideElement CONFIRM_POPUP = $x("//*[text()='OK']");

}
