package pages.cab.video.logic;

import org.apache.logging.log4j.Logger;
import core.base.HelpersInit;
import pages.cab.video.helpers.VideoContentListHelper;

public class VideoContentPage {
    private final HelpersInit helpersInit;
    private final VideoContentListHelper videoContentListHelper;

    public VideoContentPage(Logger log, HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
        this.videoContentListHelper = new VideoContentListHelper(log);
    }

    /**
     * Count presented videos
     */
    public int countVideos() {
        return helpersInit.getBaseHelper().getTextAndWriteLog(videoContentListHelper.countVideoElements());
    }

    /**
     * Do search by video's title part
     */
    public void filterVideoContent(String value) {
        videoContentListHelper.inputSearchStringInFieldTitle(value);
        videoContentListHelper.submitFilter();
    }
}
