package pages.cab.video.locators;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class VideoContentList {
    //filter
    public static final SelenideElement TITLE_INPUT = $("#title");
    public static final SelenideElement FILTER_BUTTON = $("#btnsubmit");

    //table with videos
    public static final ElementsCollection VIDEO_CONTENT_CHECKBOXES = $$(".cbVideo");
}
