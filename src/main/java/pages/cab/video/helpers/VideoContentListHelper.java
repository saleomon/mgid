package pages.cab.video.helpers;

import org.apache.logging.log4j.Logger;

import static pages.cab.video.locators.VideoContentList.*;
import static core.helpers.BaseHelper.clearAndSetValue;
import static core.helpers.BaseHelper.waitForAjax;
import static core.helpers.ErrorsHelper.checkErrors;

public class VideoContentListHelper {
    private final Logger log;

    public VideoContentListHelper(Logger log) {
        this.log = log;
    }

    /**
     * Count videos in table
     */
    public int countVideoElements() {
        return VIDEO_CONTENT_CHECKBOXES.size();
    }

    /**
     * click 'Filter' button
     */
    public void submitFilter() {
        FILTER_BUTTON.click();
        log.info("submitFilter");
        checkErrors();
        waitForAjax();
    }

    /**
     * Input search string
     */
    public void inputSearchStringInFieldTitle(String value) {
        clearAndSetValue(TITLE_INPUT, value);
    }
}
