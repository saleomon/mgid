package libs.mongoDb.video;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;
import static libs.mongoDb.connections.BaseOpenSession.connectToMongo;
import static libs.mongoDb.connections.BaseOpenSession.getCollection;

public class VideoGroups {
    public static void insertVideoGroup(String query) {
        try {
            MongoCollection videoGroups = getCollection("video", "video_groups");
            BasicDBObject videoGroup1 = BasicDBObject.parse(query);
            videoGroups.insertOne(new Document(videoGroup1));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
    }

    public static void insertVideoGroups(String ...queries) {
        try {
            MongoCollection videoGroups = getCollection("video", "video_groups");
            ArrayList<Document> docs = new ArrayList<>();
            for (String query : queries){
                BasicDBObject videoGroup = BasicDBObject.parse(query);
                docs.add(new Document(videoGroup));
            }
            videoGroups.insertMany(docs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
    }

    public static ArrayList getVideoGroups() {
        ArrayList videoGroupes = null;
        try {
            MongoCollection videoGroups = getCollection("video", "video_groups");
            videoGroupes = null;
            for (Document doc : (Iterable<Document>) videoGroups.find()) {
                videoGroupes = new ArrayList<>(doc.values());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
        return videoGroupes;
    }

    public static Object getVideoGroup(String key, String value, String field) {
        FindIterable videoGroup = null;
        try {
            MongoCollection videoGroups = getCollection("video", "video_groups");
            videoGroup = videoGroups.find(eq(key, value)).projection(include(field));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
        return videoGroup;
    }
}
