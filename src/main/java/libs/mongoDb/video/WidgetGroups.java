package libs.mongoDb.video;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import static libs.mongoDb.connections.BaseOpenSession.connectToMongo;
import static libs.mongoDb.connections.BaseOpenSession.getCollection;

public class WidgetGroups {
    public static void insertWidgetGroup(String query) {
        MongoCollection widgetGroups = getCollection("video", "widget_groups");
        BasicDBObject widgetGroup = BasicDBObject.parse(query);
        widgetGroups.insertOne(new Document(widgetGroup));
        connectToMongo().close();
    }
}
