package libs.mongoDb.video;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;

import static libs.mongoDb.connections.BaseOpenSession.connectToMongo;
import static libs.mongoDb.connections.BaseOpenSession.getCollection;

public class ProvidedVideo {
    public static void insertVideos(String ...queries) {
        try {
            MongoCollection<Document> videos = getCollection("video", "provided_video");
            ArrayList<Document> docs = new ArrayList<>();
            for (String query : queries){
                BasicDBObject video = BasicDBObject.parse(query);
                docs.add(new Document(video));
            }
            videos.insertMany(docs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
    }
}
