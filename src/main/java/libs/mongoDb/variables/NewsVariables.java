package libs.mongoDb.variables;

public class NewsVariables {
    public static String newsItem1 = "{" +
            "'_id':{'$numberLong':'1'}," +
            "'hashUrl':'TgKwqowzB3sVgWZDI8AWFQ'," +
            "'img_height':587," +
            "'visits':" +
            "{'$numberLong':" +
            "'2012903'}," +
            "'exchangeId':" +
            "{'$numberLong':" +
            "'2001'}," +
            "'siteId':" +
            "{'$numberLong':" +
            "'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'createdAt':" +
            "{'$date':" +
            "'2020-11-01T12:25:06.072Z'}," +
            "'advert':" +
            "false," +
            "'campaign':" +
            "0," +
            "'newsId':" +
            "{'$numberLong':" +
            "'1'}," +
            "'url':" +
            "'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'domain':'herbeauty.co'," +
            "'img_width':880,'" +
            "img_hash':'f8zLSOGi8l2oxq4BiDhBXw'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'satellite':false," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'," +
            "'lastCtr':0.11273957158962795," +
            "'type':'article'," +
            "'language':'en'," +
            "'monkeylearn':{" +
            "'text':''," +
            "'entities':[]," +
            "'languages':[{'category_id':2324967,'probability':1,'label':'Germanic'}," +
            "{'category_id':2324970," +
            "'probability':1," +
            "'label':'English-en'}]}," +
            "'showsAll':{" +
            "'$numberLong':'10782'}," +
            "'lastCtrVisits':3548," +
            "'lastCtrClicks':4," +
            "'lastCtrUpdateTime':{'$date':'2020-11-04T12:45:14.356Z'}," +
            "'clicksAll':{'$numberLong':'103'}," +
            "'lastVisitedAt':{'$date':'2020-11-16T18:09:28.351Z'}," +
            "'guaranteed_clicks':103" +
            "}";

    public static String newsItem2 = "{" +
            "'_id':{'$numberLong':'2'}," +
            "'hashUrl':'TgKwqowzB3sVgWZDI8AWFQ'," +
            "'img_height':587," +
            "'visits':" +
            "{'$numberLong':" +
            "'2012903'}," +
            "'exchangeId':" +
            "{'$numberLong':" +
            "'2001'}," +
            "'siteId':" +
            "{'$numberLong':" +
            "'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'createdAt':" +
            "{'$date':" +
            "'2020-11-01T12:25:06.072Z'}," +
            "'advert':" +
            "false," +
            "'campaign':" +
            "0," +
            "'newsId':" +
            "{'$numberLong':" +
            "'2'}," +
            "'url':" +
            "'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'domain':'herbeauty.co'," +
            "'img_width':880,'" +
            "img_hash':'f8zLSOGi8l2oxq4BiDhBXw'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'satellite':false," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'," +
            "'lastCtr':0.11273957158962795," +
            "'type':'article'," +
            "'language':'en'," +
            "'monkeylearn':{" +
            "'text':''," +
            "'entities':[]," +
            "'languages':[{'category_id':2324967,'probability':1,'label':'Germanic'}," +
            "{'category_id':2324970," +
            "'probability':1," +
            "'label':'English-en'}]}," +
            "'showsAll':{" +
            "'$numberLong':'10782'}," +
            "'lastCtrVisits':3548," +
            "'lastCtrClicks':4," +
            "'lastCtrUpdateTime':{'$date':'2020-11-04T12:45:14.356Z'}," +
            "'clicksAll':{'$numberLong':'103'}," +
            "'lastVisitedAt':{'$date':'2020-11-16T18:09:28.351Z'}" +
            "}";

    public static String newsItem3 = "{" +
            "'_id':{'$numberLong':'3'}," +
            "'hashUrl':'TgKwqowzB3sVgWZDI8AWFQ'," +
            "'img_height':587," +
            "'visits':" +
            "{'$numberLong':" +
            "'2012903'}," +
            "'exchangeId':" +
            "{'$numberLong':" +
            "'2001'}," +
            "'siteId':" +
            "{'$numberLong':" +
            "'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':" +
            "{'$numberLong':'3'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'domain':'herbeauty.co'," +
            "'img_width':880,'" +
            "img_hash':'f8zLSOGi8l2oxq4BiDhBXw'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'," +
            "'lastCtr':0.11273957158962795," +
            "'type':'article'," +
            "'language':'en'," +
            "'languages':[{'category_id':2324967,'probability':1,'label':'Germanic'}," +
            "{'category_id':2324970," +
            "'probability':1," +
            "'label':'English-en'}]}," +
            "'showsAll':{" +
            "'$numberLong':'10782'}," +
            "'lastCtrVisits':3548," +
            "'lastCtrClicks':4," +
            "'clicksAll':{'$numberLong':'103'}," +
            "'lastVisitedAt':{'$date':'2020-11-16T18:09:28.351Z'}," +
            "}";

    public static String newsItem4 = "{" +
            "'_id':{'$numberLong':'4'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'4'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'domain':'herbeauty.co'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'," +
            "'type':'article'," +
            "'language':'en'" +
            "}";

    public static String newsItem5 = "{" +
            "'_id':{'$numberLong':'5'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem6 = "{" +
            "'_id':{'$numberLong':'6'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem7 = "{" +
            "'_id':{'$numberLong':'7'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem8 = "{" +
            "'_id':{'$numberLong':'8'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem9 = "{" +
            "'_id':{'$numberLong':'9'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem10 = "{" +
            "'_id':{'$numberLong':'10'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";

    public static String newsItem11 = "{" +
            "'_id':{'$numberLong':'11'}," +
            "'exchangeId':{'$numberLong':'2001'}," +
            "'siteId':{'$numberLong':'2004'}," +
            "'description':" +
            "'Couples therapy may sound like a scary thing, but it’s actually pretty popular nowadays. " +
            "Longtime love birds Dax Sheppard and Kristin Bell attribute their happiness to counseling early on, " +
            "and it’s not just something that only the “about-to-be-divorced” club does. " +
            "Young, normal couples do it all the time and it could help your relationship move towards " +
            "the “happily-ever-after” club instead. Doesn’t sound so bad, right?'," +
            "'newsId':{'$numberLong':'5'}," +
            "'url':'https://herbeauty.co/relationships/15-signs-you-should-start-going-to-couples-therapy/'," +
            "'title':'15 Signs You Should Start Going To Couples Therapy'," +
            "'img_link':'https://img-cdn.herbeauty.co/wp-content/uploads/2020/10/Celebrity-Couples-Perfect-Meeting-Stories-1.jpg'," +
            "'status':'processed'" +
            "}";
}
