package libs.mongoDb.variables;

public class VideoVariables {

    public static final String video1 = "{'_id' : ObjectId('5e2889d8ba019d51ab614d32'), " +
            "'title' : '5 овочів та фруктів, які можна сушити, а мі й не знали', " +
            "'description' : 'Знаючи, який овоч чи фрукт за що відповідає, можливо навіть позбутися деяких хвороб. " +
            "В нашій добірці ви знайдете овочі та фрукти, які можна сушити'," +
            "'thumbnail' : '//video-native.mgid.com/vrimages/2018-03-22/5cf7261599884a1c30280e7dec...'," +
            "'to_sync_thumbnail' : 3," +
            "'active' : true," +
            "'toSync' : 3," +
            "'provider_id' : 0," +
            "'last_update' : '2018-03-22T10:16:39.104Z'" +
            "'pub_date': '2018-03-22T10:14:39.953Z'" +
            "'last_updated_user_id': 'Museua'," +
            "'credit': 'Mgid creative'," +
            "'guid': '5ab3820f2409441886369851'," +
            "'url': 'https://video-native.s3.us-east-1.amazonaws.com/temp/2018-03-22/Museua/f673351e43cc8e95305e2b4703deb36c.webm'," +
            "'cdn_link': 'http://video-native.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'media_files': [{" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'width': 1890," +
            "'height': 1018" +
            "}, {" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9_360.mp4'," +
            "'width': 640," +
            "'height': 360" +
            "}]," +
            "'video_teaser_id': 1," +
            "'impressions': 0}";

    public static final String video2 = "{'_id' : ObjectId('5e28892136f624341f746c13'), " +
            "'title' : '5 неожиданных последствий Covid-19, которые проявляются позже', " +
            "'description' : 'Те, кому повезло перенести «ковид» в легкой форме, не всегда осознают долгосрочные " +
            "последствия недуга. А ведь коронавирус влияет не только на легкие, он затрагивает практически все органы'," +
            "'thumbnail' : '//video-native.mgid.com/vrimages/2018-03-22/5cf7261599884a1c30280e7dec...'," +
            "'to_sync_thumbnail' : 3," +
            "'active' : true," +
            "'toSync' : 3," +
            "'provider_id' : 0," +
            "'last_update' : '2018-03-22T10:16:39.104Z'" +
            "'pub_date': '2018-03-22T10:14:39.953Z'" +
            "'last_updated_user_id': 'Museua'," +
            "'credit': 'Mgid creative'," +
            "'guid': '5ab3820f2409441886369851'," +
            "'url': 'https://video-native.s3.us-east-1.amazonaws.com/temp/2018-03-22/Museua/f673351e43cc8e95305e2b4703deb36c.webm'," +
            "'cdn_link': 'http://video-native.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'media_files': [{" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'width': 1890," +
            "'height': 1018" +
            "}, {" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9_360.mp4'," +
            "'width': 640," +
            "'height': 360" +
            "}]," +
            "'video_teaser_id': 2," +
            "'impressions': 0}";

    public static final String video3 = "{'_id' : ObjectId('5e288d13eef7fd691c52d775'), " +
            "'title' : '6 Bravest Celebrity Outfits', " +
            "'description' : 'Here we go! It is time to look at 5 stars who have pushed the boundaries over the years " +
            "in the shortest, riskiest and downright raunchy outfits ever'," +
            "'thumbnail' : '//video-native.mgid.com/vrimages/2018-03-22/5cf7261599884a1c30280e7dec...'," +
            "'to_sync_thumbnail' : 3," +
            "'active' : true," +
            "'toSync' : 3," +
            "'provider_id' : 0," +
            "'last_update' : '2018-03-22T10:16:39.104Z'" +
            "'pub_date': '2018-03-22T10:14:39.953Z'" +
            "'last_updated_user_id': 'Museua'," +
            "'credit': 'Mgid creative'," +
            "'guid': '5ab3820f2409441886369851'," +
            "'url': 'https://video-native.s3.us-east-1.amazonaws.com/temp/2018-03-22/Museua/f673351e43cc8e95305e2b4703deb36c.webm'," +
            "'cdn_link': 'http://video-native.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'media_files': [{" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9.mp4'," +
            "'width': 1890," +
            "'height': 1018" +
            "}, {" +
            "'file': 'http://video-cdn.mgid.com/provided_video/2018-03-22/6ecc6063d3a9936e5c3c28c26259d6c9_360.mp4'," +
            "'width': 640," +
            "'height': 360" +
            "}]," +
            "'video_teaser_id': 3," +
            "'impressions': 0}";
}
