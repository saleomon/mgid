package libs.mongoDb.connections;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import static core.service.PropertiesManager.*;

public class BaseOpenSession {
    public static MongoClient connectToMongo() {
        MongoClient mongoClient = null;
        try {
            mongoClient = MongoClients.create("mongodb://" +
                    getResourceByName(ConfigValue.MONGO_HOST) + ":" +
                    getResourceByName(ConfigValue.MONGO_PORT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mongoClient;
    }

    public static MongoCollection getCollection(String dataBaseName, String collectionName) {
        MongoDatabase database = connectToMongo().getDatabase(dataBaseName);
        return database.getCollection(collectionName);
    }
}