package libs.mongoDb.exchange;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

import java.util.ArrayList;

import static libs.mongoDb.connections.BaseOpenSession.connectToMongo;
import static libs.mongoDb.connections.BaseOpenSession.getCollection;

public class News {
    public static void insertNews(String... queries) {
        try {
            MongoCollection news = getCollection("exchange", "news");
            ArrayList<Document> docs = new ArrayList<>();
            for (String query : queries) {
                BasicDBObject newsItem = BasicDBObject.parse(query);
                docs.add(new Document(newsItem));
            }
            news.insertMany(docs);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectToMongo().close();
        }
    }
}
