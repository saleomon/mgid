package libs.clickhouse.connections;

import com.clickhouse.jdbc.ClickHouseConnection;
import com.clickhouse.jdbc.ClickHouseDriver;
import com.clickhouse.jdbc.ClickHouseStatement;
import org.apache.commons.dbutils.DbUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static libs.clickhouse.helper.ClickHouseHelper.loadChData;
import static core.service.PropertiesManager.ConfigValue;
import static core.service.PropertiesManager.getResourceByName;
import static testData.clickHouse.ClickHouseDbNames.ClickHouseDbs;


public class ClickHouseSession {
    private ClickHouseConnection connection;
    private ClickHouseStatement statement;
    private final ClickHouseDriver driver = new ClickHouseDriver();
    private ClickHouseDbs dbNames = ClickHouseDbs.STATS;

    public ClickHouseSession setDbNames(ClickHouseDbs dbNames) {
        this.dbNames = dbNames;
        return this;
    }

    public void insertDamp(String dampName){
        getConnectionDb();
        System.out.println(dampName);
        String[] inserts = loadChData(dampName);
        try {
            for(String query : inserts) {
                statement.executeQuery(query);
            }
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        closeAllConnections();
    }

    public List<List<String>> selectQueryList(String query){
        getConnectionDb();
        ResultSet result;
        List<List<String>> listOfListElements = new ArrayList<>();

        try {
            result = statement.executeQuery(query);
            int elementSize = result.getMetaData().getColumnCount();
            while (result.next()) {
                List<String> list = new ArrayList<>();
                for(int i = 1; i <= elementSize; i++) {
                   list.add(result.getString(i));
                }
                listOfListElements.add(list);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeAllConnections();
        return listOfListElements;
    }

    public boolean isHaveAlreadyData(){
        boolean state = false;
        getConnectionDb();
        try {
            state = statement.executeQuery("SELECT site FROM widget_statistics_summ LIMIT 10").next();
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        closeAllConnections();
        return state;
    }

    public String executeQuery(String query, String searchFieldName){
        ResultSet state;
        getConnectionDb();
        try {
            state = statement.executeQuery(query);
            while(state.next()) {
                return state.getString(searchFieldName);
            }
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        closeAllConnections();
        return null;
    }

    public ResultSet executeQuery(String query){
        ResultSet state;
        getConnectionDb();
        try {
            state = statement.executeQuery(query);
            while(state.next()) {
                return state;
            }
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        closeAllConnections();
        return null;
    }

    public ArrayList<String> executeQueryAndReturnList(String query){
        ArrayList<String> list = new ArrayList<>();
        ResultSet state;
        getConnectionDb();
        try {
            state = statement.executeQuery(query);
            while(state.next()) {
                list.add(state.getString("clicks"));
            }
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        closeAllConnections();
        return list;
    }

    private String getConnectionUrlClickHouseStatistics(){
        return "jdbc:clickhouse://" +
                getResourceByName(ConfigValue.CLICKHOUSE_HOST) +
                ":" +
                getResourceByName(ConfigValue.CLICKHOUSE_PORT) +
                "/" + dbNames.getTypeValue() + "?serverTimezone=UTC";
    }

    private void getConnectionDb() {
        try {
            connection = driver.connect(
                    getConnectionUrlClickHouseStatistics(),
                    new Properties()
            );

            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void closeAllConnections(){
        DbUtils.closeQuietly(statement);
        DbUtils.closeQuietly(connection);
    }
}
