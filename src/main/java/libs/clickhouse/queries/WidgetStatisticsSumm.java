package libs.clickhouse.queries;

import libs.clickhouse.connections.ClickHouseSession;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static testData.project.OthersData.timeZone;

public class WidgetStatisticsSumm extends ClickHouseSession {

    private ResultSet data;

    public void getSumForCustomWidgetFieldsFromClickHouse(int widgetId, ArrayList<String> dates){
        data = executeQuery(queryForWidgetStat(widgetId, dates));
    }

    public void getSumForCustomWebsiteFieldsFromClickHouse(int websiteId, ArrayList<String> dates){
        data = executeQuery(queryForWebsiteStat(websiteId, dates));
    }

    public String getString(int i){
        try {
            return data.getString(i);
        } catch (SQLException e) {
            System.err.println("Catch " + e);
        }
        return null;
    }

    private String getDateString(ArrayList<String> dates, String timestampName){
        return dates.get(0).equals("all") ?

                " " + timestampName + " <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', '" + timeZone + "'" +
                        "    )" :

                " " + timestampName + " >= toUnixTimestamp(" +
                        "        '" + dates.get(0) + " 00:00:00', '" + timeZone + "'" +
                        "    )" +
                        " AND " + timestampName + " <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', '" + timeZone + "'" +
                        " )";

    }

    public String queryForWidgetStat(int widgetId, ArrayList<String> dates){
        return "SELECT" +
                "    SUM(ad_requests) AS ad_requests," +
                "    SUM(impressions) AS impressions," +
                "    round(" +
                "            SUM(wages)," +
                "            2" +
                "        ) AS wages," +
                "    CAST(SUM(clicks) as int) AS clicks," +
                "    CAST(SUM(internal_clicks) as int) AS internal_clicks" +
                " FROM" +
                "    (" +
                "        SELECT" +
                "            date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            traffic_type," +
                "            traffic_source," +
                "            pubsrcid," +
                "            ad_requests," +
                "            impressions," +
                "            clicks," +
                "            revenue AS wages," +
                "            video_wages," +
                "            over_guarantee_wages," +
                "            paid_clicks," +
                "            internal_clicks" +
                "        FROM" +
                "            (" +
                "                SELECT" +
                "                    toDate(" +
                "                            hour_timestamp, '" + timeZone + "'" +
                "                        ) AS date," +
                "                    composite," +
                "                    country," +
                "                    CAST(device, 'String') AS device," +
                "                    os," +
                "                    traffic_type," +
                "                    traffic_source," +
                "                    CAST(pubsrcid, 'String') AS pubsrcid," +
                "                    if (" +
                "                                composite IN (" +
                "                                SELECT" +
                "                                    toUInt32(composite)" +
                "                                FROM" +
                "                                    dictionaries.widgets_info" +
                "                                WHERE" +
                "                                        category_platform IN (149, 150)" +
                "                                  AND is_rtb = 0" +
                "                            )," +
                "                                real_shows," +
                "                                shows" +
                "                        ) AS ad_requests," +
                "                    real_shows AS impressions," +
                "                    CAST(0, 'Decimal(18, 3)') AS clicks," +
                "                    if(" +
                "                                type = 'video_wage'," +
                "                                CAST(0, 'Decimal(18, 9)')," +
                "                                wages_publisher_currency" +
                "                        ) AS revenue," +
                "                    CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "                    over_guarantee_wages_publisher_currency AS over_guarantee_wages," +
                "                    toUInt64(0) AS paid_clicks," +
                "                    toUInt64(0) AS internal_clicks" +
                "                FROM" +
                "                    widget_statistics_summ" +
                "                WHERE" +
                "                        composite IN (" + widgetId + ") AND " +
                getDateString(dates, "hour_timestamp") +
                "                )" +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    timestamp, '" + timeZone + "'" +
                "                ) AS date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            traffic_type," +
                "            traffic_source," +
                "            CAST(pubsrcid, 'String') AS pubsrcid," +
                "            toUInt64(0) AS ad_requests," +
                "            toUInt64(0) AS impressions," +
                "            CAST(clicks, 'Decimal(18, 3)') * sign AS clicks," +
                "            wages_publisher_currency * sign AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            clicks * sign as paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_clicks_goods" +
                "        WHERE" +
                "                composite IN (" + widgetId + ") AND " +
                getDateString(dates, "timestamp") +
                "          AND (" +
                "                    calculated_click = 1" +
                "                OR timestamp >= toUnixTimestamp(" +
                "                        now() - toIntervalHour(3)" +
                "                )" +
                "            )" +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                ) AS date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            CAST(traffic_type, 'String') AS traffic_type," +
                "            '' AS traffic_source," +
                "            CAST(pubsrcid, 'String') AS pubsrcid," +
                "            toUInt64(0) AS ad_requests," +
                "            toUInt64(0) AS real_shows," +
                "            CAST(0, 'Decimal(18, 3)') AS clicks," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_shows_rtb_summ" +
                "        WHERE" +
                "                composite IN (" + widgetId + ") AND " +
                getDateString(dates, "hour_timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                ) AS date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            CAST(traffic_type, 'String') AS traffic_type," +
                "            '' AS traffic_source," +
                "            CAST(pubsrcid, 'String') AS pubsrcid," +
                "            toUInt64(0) AS ad_requests," +
                "            toUInt64(0) AS real_shows," +
                "            CAST(0, 'Decimal(18, 3)') AS clicks," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_shows_cpm_summ" +
                "        WHERE" +
                "                composite IN (" + widgetId + ") AND " +
                getDateString(dates, "hour_timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    timestamp, '" + timeZone + "'" +
                "                ) AS date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            traffic_type," +
                "            traffic_source," +
                "            CAST(pubsrcid, 'String') AS pubsrcid," +
                "            toUInt64(0) AS ad_requests," +
                "            toUInt64(0) AS impressions," +
                "            CAST(clicks, 'Decimal(18, 3)') AS clicks," +
                "            CAST(0, 'Decimal(18, 9)') AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            clicks as internal_clicks" +
                "        FROM" +
                "            widget_statistics_clicks_iexchange" +
                "        WHERE" +
                "                composite IN (" + widgetId + ") AND " +
                getDateString(dates, "timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                ) AS date," +
                "            composite," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            traffic_type," +
                "            traffic_source," +
                "            '' AS pubsrcid," +
                "            toUInt64(0) AS ad_requests," +
                "            toUInt64(0) AS impressions," +
                "            CAST(clicks, 'Decimal(18, 3)') AS clicks," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_google_ads" +
                "        WHERE" +
                "                composite IN (" + widgetId + ") AND " +
                getDateString(dates, "hour_timestamp") +
                "        )" +
                " ORDER BY" +
                "    wages desc";
    }
    
    public String queryForWebsiteStat(int websiteId, ArrayList<String> dates){
        return "SELECT" +
                "    SUM(page_views) AS page_views," +
                "    SUM(page_impressions) AS page_impressions," +
                "    round(" +
                "            SUM(wages)," +
                "            2" +
                "        ) AS wages," +
                "    CAST(SUM(clicks) as int) AS clicks," +
                "    CAST(SUM(internal_clicks) as int) AS internal_clicks" +
                " FROM" +
                "    (" +
                "        SELECT" +
                "            date," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            traffic_type," +
                "            traffic_source," +
                "            clicks," +
                "            page_views," +
                "            page_impressions," +
                "            revenue as wages," +
                "            video_wages," +
                "            over_guarantee_wages," +
                "            paid_clicks," +
                "            internal_clicks" +
                "        FROM" +
                "            (" +
                "                SELECT" +
                "                    toDate(" +
                "                            hour_timestamp, '" + timeZone + "'" +
                "                        ) AS date," +
                "                    country," +
                "                    device," +
                "                    os," +
                "                    if(" +
                "                                    traffic_type == ''" +
                "                                OR traffic_type == '-'," +
                "                                    'Not defined'," +
                "                                    traffic_type" +
                "                        ) AS traffic_type," +
                "                    if(" +
                "                                    traffic_source == ''" +
                "                                OR traffic_source == '-'," +
                "                                    'Not defined'," +
                "                                    traffic_source" +
                "                        ) AS traffic_source," +
                "                    CAST(0, 'Decimal(18, 3)') AS clicks," +
                "                    toUInt64(page_views) as page_views," +
                "                    toUInt64(page_impressions) as page_impressions," +
                "                    if(" +
                "                                type = 'video_wage'," +
                "                                CAST(0, 'Decimal(18, 9)')," +
                "                                wages_publisher_currency" +
                "                        ) AS revenue," +
                "                    CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "                    over_guarantee_wages_publisher_currency AS over_guarantee_wages," +
                "                    toUInt64(0) AS paid_clicks," +
                "                    toUInt64(0) AS internal_clicks" +
                "                FROM" +
                "                    widget_statistics_summ PREWHERE site = " + websiteId +
                "                WHERE " +
                getDateString(dates, "hour_timestamp") +
                "                )" +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(timestamp, '" + timeZone + "') as date," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            if(" +
                "                            traffic_type == ''" +
                "                        OR traffic_type == '-'," +
                "                            'Not defined'," +
                "                            traffic_type" +
                "                ) AS traffic_type," +
                "            if(" +
                "                            traffic_source == ''" +
                "                        OR traffic_source == '-'," +
                "                            'Not defined'," +
                "                            traffic_source" +
                "                ) AS traffic_source," +
                "            CAST(clicks, 'Decimal(18, 3)') * sign AS clicks," +
                "            toUInt64(0) AS page_views," +
                "            toUInt64(0) AS page_impressions," +
                "            wages_publisher_currency * sign AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            clicks * sign as paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_clicks_goods PREWHERE site = " + websiteId +
                "        WHERE " +
                getDateString(dates, "timestamp") +
                "          AND (" +
                "                    calculated_click = 1" +
                "                OR timestamp >= toUnixTimestamp(" +
                "                        now() - toIntervalHour(3)" +
                "                )" +
                "            )" +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                )," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            CAST(traffic_type, 'String') AS traffic_type," +
                "            'Not defined' AS traffic_source," +
                "            CAST(0, 'Decimal(18, 3)') AS clicks," +
                "            toUInt64(0) AS page_views," +
                "            toUInt64(0) AS page_impressions," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_shows_rtb_summ PREWHERE site = " + websiteId +
                "        WHERE " +
                getDateString(dates, "hour_timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                )," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            CAST(traffic_type, 'String') AS traffic_type," +
                "            'Not defined' AS traffic_source," +
                "            CAST(0, 'Decimal(18, 3)') AS clicks," +
                "            toUInt64(0) AS page_views," +
                "            toUInt64(0) AS page_impressions," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_shows_cpm_summ PREWHERE site = " + websiteId  +
                "        WHERE " +
                getDateString(dates, "hour_timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(timestamp, '" + timeZone + "') AS date," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            if(" +
                "                            traffic_type == ''" +
                "                        OR traffic_type == '-'," +
                "                            'Not defined'," +
                "                            traffic_type" +
                "                ) AS traffic_type," +
                "            if(" +
                "                            traffic_source == ''" +
                "                        OR traffic_source == '-'," +
                "                            'Not defined'," +
                "                            traffic_source" +
                "                ) AS traffic_source," +
                "            CAST(clicks, 'Decimal(18, 3)') AS clicks," +
                "            toUInt64(0) AS page_views," +
                "            toUInt64(0) AS page_impressions," +
                "            CAST(0, 'Decimal(18, 9)') AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            clicks as internal_clicks" +
                "        FROM" +
                "            widget_statistics_clicks_iexchange PREWHERE site = " + websiteId +
                "        WHERE " +
                getDateString(dates, "timestamp") +
                "        UNION ALL" +
                "        SELECT" +
                "            toDate(" +
                "                    hour_timestamp, '" + timeZone + "'" +
                "                )," +
                "            country," +
                "            CAST(device, 'String') AS device," +
                "            os," +
                "            if(" +
                "                            traffic_type == ''" +
                "                        OR traffic_type == '-'," +
                "                            'Not defined'," +
                "                            traffic_type" +
                "                ) AS traffic_type," +
                "            if(" +
                "                            traffic_source == ''" +
                "                        OR traffic_source == '-'," +
                "                            'Not defined'," +
                "                            traffic_source" +
                "                ) AS traffic_source," +
                "            CAST(clicks, 'Decimal(18, 3)') AS clicks," +
                "            toUInt64(0) AS page_views," +
                "            toUInt64(0) AS page_impressions," +
                "            wages_publisher_currency AS wages," +
                "            CAST(0, 'Decimal(18, 6)') AS video_wages," +
                "            CAST(0, 'Decimal(18, 6)') AS over_guarantee_wages," +
                "            toUInt64(0) AS paid_clicks," +
                "            toUInt64(0) AS internal_clicks" +
                "        FROM" +
                "            widget_statistics_google_ads PREWHERE site = " + websiteId +
                "        WHERE " +
                getDateString(dates, "hour_timestamp") +
                "        )" +
                " ORDER BY" +
                "    wages desc";
    }
}
