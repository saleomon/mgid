package libs.clickhouse.queries;

import libs.clickhouse.connections.ClickHouseSession;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class PublisherStatistics extends ClickHouseSession {

    private ResultSet data;

    /**
     * @param dataType - site = ?/composite = ?/""
     */
    public ResultSet getSumForCustomFieldsFromClickHouse(String dataType, ArrayList<String> dates, String... dB_fields){
        String fields = Arrays.stream(dB_fields).map(i -> "sum(" + i + ")").collect(Collectors.joining(", "));

        dataType = dataType.equals("") ? "" : dataType + " AND";

        data = executeQuery(
                " SELECT " + fields +
                        " FROM publishers_statistics" +
                        " WHERE " + dataType +
                        getDateStringForHourTimestamp(dates)
        );

        return data;
    }

    public String getString(int i){
        try {
            return data.getString(i);
        } catch (SQLException e) {
            System.err.println("Catch " + e);
        }
        return null;
    }

    private String getDateStringForHourTimestamp(ArrayList<String> dates){
        return dates.get(0).equals("all") ?

                " hour_timestamp <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                        "    )" :

                " hour_timestamp >= toUnixTimestamp(" +
                "        '" + dates.get(0) + " 00:00:00', 'Pacific/Honolulu'" +
                "    )" +
                " AND hour_timestamp <= toUnixTimestamp(" +
                "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                " )";

    }

    private String getDateStringForTimestamp(ArrayList<String> dates){
        return dates.get(0).equals("all") ?

                " timestamp <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                        "    )" :

                " timestamp >= toUnixTimestamp(" +
                "        '" + dates.get(0) + " 00:00:00', 'Pacific/Honolulu'" +
                "    )" +
                " AND timestamp <= toUnixTimestamp(" +
                "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                " )";
    }

    public ArrayList<String> getSumGoodsClickForAllWidgetsBySite(ArrayList<String> dates, int sizeCollection){

        ArrayList<String> soldClicks = executeQueryAndReturnList(
                    "select composite, sum(goods_clicks_accepted) as clicks " +
                            " from publishers_statistics " +
                            " where site = 1 AND" +
                            getDateStringForHourTimestamp(dates) +
                            " group by composite" +
                            " order by composite;"
            );


        if(soldClicks.size() < sizeCollection) {
            for (int i = soldClicks.size(); i < sizeCollection; i++) {
                soldClicks.add(i, "0");
            }
        }
        return soldClicks;
    }

    public ResultSet getSumByPushSubscribersStatisticsFromClickHouse(int siteIdValue, ArrayList<String> dates) {
        data = executeQuery(
                "SELECT\n" +
                        "    sum(push_form_requests),\n" +
                        "    sum(push_form_impressions),\n" +
                        "    sum(push_subscribe_browser_form)\n" +
                        "FROM\n" +
                        "    audience_hub_events\n" +
                        "WHERE site = " + siteIdValue + " AND" +
                        getDateStringForHourTimestamp(dates)
        );
        return data;
    }

    public ResultSet getSumByPushStatisticsEventsFromClickHouse(String siteDomainValue, ArrayList<String> dates) {
        data = executeQuery(
                "SELECT\n" +
                        "    countIf(event = 'receive'),\n" +
                        "    countIf(event = 'click'),\n" +
                        "    countIf(event = 'send')\n" +
                        "FROM\n" +
                        "    push_statistics_events\n" +
                        "WHERE domain = '" + siteDomainValue + "' AND" +
                        getDateStringForTimestamp(dates)
        );
        return data;
    }
}
