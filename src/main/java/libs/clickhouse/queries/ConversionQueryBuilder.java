package libs.clickhouse.queries;

import libs.clickhouse.connections.ClickHouseSession;

import java.util.List;

public class ConversionQueryBuilder extends ClickHouseSession {

    public List<List<String>> selectFromConversionStats(Integer campaignId, String typeConversion, String stateConversion, String filter1, String filter2) {
        String filter2Builder = stringFilterBuilderSelectConversionStats(filter2);
        String filter1Builder = stringFilterBuilderSelectConversionStats(filter1);
        String stateConversionBuilder = stringStateConversionBuilder(stateConversion, typeConversion);

        String select =  "SELECT\n" +
                "    first as first,\n" +
                "    sum(clicksSum) as clicks,\n" +
                "    round(sum(spentSum) / 1000000, 2) AS spent,\n" +
                "    sum(buySum) as buySum,\n" +
                "    sum(decisionSum) as decisionSum,\n" +
                "    sum(interestSum) as interestSum,\n" +
                "    toDecimal32(sum(revenueSum) / 1000000, 2) as revenue,\n" +
                "    second as second\n" +
                "FROM\n" +
                "    (\n" +
                "     SELECT\n" +
                "         " + filter1Builder + " as first,\n" +
                "         0 as clicksSum,\n" +
                "         0 as spentSum,\n" +
                "         sumIf(count, stage = 'buy') as buySum,\n" +
                "         sumIf(count, stage = 'decision') as decisionSum,\n" +
                "         sumIf(count, stage = 'interest') as interestSum,\n" +
                "         sum(revenue) as revenueSum,\n" +
                "         " + filter2Builder + " as second\n" +
                "     FROM\n" +
                "         conversions\n" +
                "     WHERE\n" +
                "             campaign = "+ campaignId +"\n" +
                "     GROUP BY\n" +
                "         first,\n" +
                "         second\n" +
                "     UNION ALL\n" +
                "     SELECT\n" +
                "         " + filter1Builder + " as first,\n" +
                "         sum(clicks * sign) as clicksSum,\n" +
                "         sum(spent_campaign * sign) as spentSum,\n" +
                "         0 as buySum,\n" +
                "         0 as decisionSum,\n" +
                "         0 as interestSum,\n" +
                "         0 as revenueSum,\n" +
                "         "  + filter2Builder + " as second\n" +
                "     FROM\n" +
                "         widget_clicks_goods\n" +
                "     WHERE\n" +
                "             campaign = "+ campaignId +"\n" +
                "       AND rejected_reason NOT IN (19, 20, 21)\n" +
                "       AND clicks > 0\n" +
                "     GROUP BY\n" +
                "         first,\n" +
                "         second\n" +
                "        )\n" +
                "GROUP BY\n" +
                "    first,\n" +
                "    second\n" +
                stateConversionBuilder +
                "ORDER BY\n" +
                "    clicks desc";

        return selectQueryList(select);
    }


    /**
     * метод создает стрингу для запроса в кликхаус в зависимости от выбраного фильтра в интерфейсе статистики конверсий
     */
    private String stringFilterBuilderSelectConversionStats(String filter) {
        return switch (filter) {
            case "country" -> "toString(dictGetString('countries', 'name',toUInt64(country)))";
            case "teaser" -> "toString(teaser)";
            case "ticker" ->
                    "toString(if(dictGetInt8('g_partners_1', 'use_selective_bidding_v2', toUInt64(campaign)) = 1, source_id, widget_goods))";
            case "device_os" ->
                    "toString(concat(dictGetString('os', 'device_type', toUInt64(os)), ' ', dictGetString(\n" +
                            "            'os', 'name', toUInt64(os)), ' ', dictGetString('os', 'version', toUInt64(os))))";
            case "region" -> "toString(dictGetString('regions', 'name', toUInt64(region)))";
            case "connection_type" ->
                    "toString(multiIf(`device` not in ['tablet', 'mobile'], 'non mobile', `connection_type` = 0,'undefined', `connection_type` = 1,'wifi', 'carrier'))";
            case "browser" -> "toString(dictGetString('browsers', 'name', toUInt64(browser)))";
            case "adv_traffic_type" -> "toString(adv_traffic_type)";
            case "phones_price_range" -> "toString(phones_price_range)";
            case "uid_subuid" -> "toString(concat(toString(widget_goods), 's', toString(source_id)))";
            default -> null;
        };
    }

    /**
     * метод создает стрингу для запроса в кликхаус в зависимости от выбраной опции состояния конверсии в интерфейсе статистики конверсий
     */
    private String stringStateConversionBuilder(String stateConversion, String typeConversion) {
        return switch (stateConversion) {
            case "all" -> "";
            case "with" -> "HAVING " + typeConversion + "Sum > 0 ";
            case "without" -> "HAVING " + typeConversion + "Sum = 0 ";
            default -> null;
        };
    }

}
