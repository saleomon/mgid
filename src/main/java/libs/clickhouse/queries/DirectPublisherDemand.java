package libs.clickhouse.queries;

import libs.clickhouse.connections.ClickHouseSession;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class DirectPublisherDemand extends ClickHouseSession {

    private ResultSet data;

    /**
     * @param dataType - site = ?/composite = ?/""
     */
    public void getSumForCustomFieldsFromClickHouse(String dataType, ArrayList<String> dates, String... dB_fields){
        String fields = Arrays.stream(dB_fields).map(i -> "sum(" + i + ")").collect(Collectors.joining(", "));

        dataType = dataType.equals("") ? "" : dataType + " AND";

        data = executeQuery(
                " SELECT " + fields +
                        " FROM direct_publisher_demand" +
                        " WHERE " + dataType +
                        getPreparedDate(dates)
        );

    }

    public String getFieldsValueByIndex(int i){
        try {
            return data.getString(i);
        } catch (SQLException e) {
            System.err.println("Catch " + e);
        }
        return null;
    }

    private String getPreparedDate(ArrayList<String> dates){
        return dates.get(0).equals("all") ?

                " hour_timestamp <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                        "    )" :

                " hour_timestamp >= toUnixTimestamp(" +
                        "        '" + dates.get(0) + " 00:00:00', 'Pacific/Honolulu'" +
                        "    )" +
                        " AND hour_timestamp <= toUnixTimestamp(" +
                        "        '" + dates.get(1) + " 23:59:59', 'Pacific/Honolulu'" +
                        " )";

    }
}
