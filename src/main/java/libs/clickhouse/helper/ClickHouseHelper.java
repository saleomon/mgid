package libs.clickhouse.helper;

import java.io.File;
import java.util.Scanner;

public class ClickHouseHelper {

    public static String[] loadChData(String filePath){
        StringBuilder text = new StringBuilder();
        try {
            File file = new File(filePath);
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                text.append(scanner.nextLine().trim());
            }
        }
        catch (Exception e){
            System.err.println("Catch " + e);
        }
        return text.toString().split(";");
    }
}
