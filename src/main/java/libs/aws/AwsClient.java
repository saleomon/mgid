package libs.aws;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.BucketCrossOriginConfiguration;
import com.amazonaws.services.s3.model.CORSRule;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import com.amazonaws.services.s3.model.ObjectListing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_IMAGES;

public class AwsClient {

    private final String awsHost = "http://local-aws.mgid.com:80";
    private final String region = "eu-central-1";
    private final String bucketName = "local";

    private AmazonS3 s3Client;

    private BucketCrossOriginConfiguration configuration;

    private void init(){
        try {
            // Create two CORS rules.
            List<CORSRule.AllowedMethods> rule1AM = new ArrayList<>();
            rule1AM.add(CORSRule.AllowedMethods.PUT);
            rule1AM.add(CORSRule.AllowedMethods.POST);
            rule1AM.add(CORSRule.AllowedMethods.DELETE);
            CORSRule rule1 = new CORSRule().withId("CORSRule1").withAllowedMethods(rule1AM)
                    .withAllowedOrigins(List.of("http://*.example.com"));

            List<CORSRule.AllowedMethods> rule2AM = new ArrayList<>();
            rule2AM.add(CORSRule.AllowedMethods.GET);
            CORSRule rule2 = new CORSRule().withId("CORSRule2").withAllowedMethods(rule2AM)
                    .withAllowedOrigins(List.of("*")).withMaxAgeSeconds(3000)
                    .withExposedHeaders(List.of("x-amz-server-side-encryption"));

            List<CORSRule> rules = new ArrayList<>();
            rules.add(rule1);
            rules.add(rule2);

            // Add the rules to a new CORS configuration.
            configuration = new BucketCrossOriginConfiguration();
            configuration.setRules(rules);

            AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder
                    .EndpointConfiguration(
                    awsHost, region);

            s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(new EnvironmentVariableCredentialsProvider())
                    .withEndpointConfiguration(endpointConfiguration)
                    .withPathStyleAccessEnabled(true)
                    .build();

        } catch (SdkClientException e) {
            e.printStackTrace();
        }
    }

    public void createBucket(String... imageName) {
        init();

        if (!s3Client.doesBucketExistV2(bucketName)) {
            s3Client.createBucket(new CreateBucketRequest(bucketName, region));
            s3Client.setBucketCrossOriginConfiguration(bucketName, configuration);
        }

        for(String name : imageName){
            if(!isKeyExist(name)){
                s3Client.putObject(bucketName, name, new File(LINK_TO_RESOURCES_IMAGES + name));
            }
        }
    }

    private boolean isKeyExist(String imageHash){
        ObjectListing object_listing = s3Client.listObjects(bucketName);

        return object_listing.getObjectSummaries()
                .stream()
                .anyMatch(i -> i.getKey().equals(imageHash));
    }
}
