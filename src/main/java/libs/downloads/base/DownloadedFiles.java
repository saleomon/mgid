package libs.downloads.base;

import libs.downloads.Downloads;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

public class DownloadedFiles extends Downloads {
    public DownloadedFiles(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }

    public String getFileName() {
        return super.getFileName();
    }

    public void getDownloadedFile() {
        getFilePathRemote();
    }
}
