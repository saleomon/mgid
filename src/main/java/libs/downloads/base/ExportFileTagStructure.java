package libs.downloads.base;

import libs.downloads.Downloads;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.openqa.selenium.remote.SessionId;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.codeborne.selenide.Selenide.sleep;

public class ExportFileTagStructure extends Downloads {
    private final String xmlFileName = "xmlDataFile";
    private Document xmlDocument;

    public ExportFileTagStructure() {
    }

    public ExportFileTagStructure(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }


    /**
     * метод скачивает файл, после чего локальный файл готов для работы с ним
     */
    private void getAndLoadXmlFile() {
        FileInputStream file;
        try {
            // получаем файл (.xml)
            file = new FileInputStream(getLocalXmlFile());

            // costil
            sleep(1000);

            // читаем xml файл для дальнейшей работы с ним
            xmlDocument = Jsoup.parse(file, null, "", Parser.xmlParser());
            deleteLocalFile();
        } catch (Exception e) {
            log.info("Catch " + e);
        }
    }

    /**
     * копируем XML-файл в локальный файл - после чего файл 'xmlFileName' доступен для чтения
     */
    private File getLocalXmlFile(String... nameOfFile) {
        try {
            Files.copy(getFilePathRemote(nameOfFile), Paths.get(xmlFileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            log.info("Catch " + e);
        }
        return Paths.get(xmlFileName).toFile();
    }

    /**
     * метод проверяет все строчки в документе на наличие в неё errors (Fatal, Warning, Notice)
     */
    private boolean checkErrorsInXmlFile() {
        String value = String.valueOf(xmlDocument);
        if (value.contains("Fatal Error") |
                value.contains("Warning") |
                value.contains("Notice")) {

            log.info(value);

            return true;
        }
        return false;
    }

    /**
     * close all file and delete their
     */
    public void closeAllFile() {
        try {
            File xmlFile = new File(xmlFileName);
            //закрываем работу со всеми активними файлами
            if (xmlFile.exists()) xmlFile.delete();
            //все активные файлы закрыты
        } catch (Exception e) {
            System.err.println("catch " + e);
        }
    }

    /**
     * Get tag value from XML document
     */
    public String getTagValueFromXml(String tagName) {
        return xmlDocument.select(tagName).text();
    }
}
