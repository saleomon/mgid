package libs.downloads.base;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import libs.downloads.Downloads;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.openqa.selenium.remote.SessionId;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.sleep;
import static libs.downloads.locators.ExportInDashboardLocators.*;

public class ExportFileTableStructure extends Downloads {

    private Workbook workbook;
    private final String csvFileName = "csvDataFile";
    private final String xlsFileName = "xlsFile";
    private int customRow = 1;

    public ExportFileTableStructure(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }

    public ExportFileTableStructure setCustomRowNumber(int customRow) {
        this.customRow = customRow;
        return this;
    }

    /**
     * Базовый метод дешборда для скачивания csv/excel файлов
     * Состоит из
     * 1. clickButtonCsv() - только ui клик (и появляется браузерная закачка файла)
     * 2. getAndLoadCsvFile() - загружаем для дальнейшей работы скачиваемый файл
     * 3. getCustomRowNumber() - получаем порядковый номер строчки с которой планируем работать -
     * для этого нам нужно получить пересечение заголовка колонки и одного из значений этой колонки
     * customColumnName  - название колонки в которой находится значение ячейки
     * customColumnValue - значение ячейки из строчки порядковый номер которой нужно получить
     * @return - checkErrorsInCsvFile() - проверяем в скачаном файле наличие ошибок 'Fatal Error|Warning|Notice'
     */
    public boolean loadExportedFileDash() {
        clickButtonCsvDash();
        getAndLoadCsvFile();
        return checkErrorsInCsvFile();
    }

    /**
     * Базовый метод каба для скачивания csv/excel файлов
     * Состоит из
     * 1. clickButtonCsv() - только ui клик (и появляется браузерная закачка файла)
     * 2. getAndLoadCsvFile() - загружаем для дальнейшей работы скачиваемый файл
     * 3. getCustomRowNumber() - получаем порядковый номер строчки с которой планируем работать ->
     * для этого нам нужно получить пересечение заголовка колонки и одного из значений этой колонки
     * customColumnName  - название колонки в которой находится значение ячейки
     * customColumnValue - значение ячейки из строчки порядковый номер которой нужно получить
     * @return - checkErrorsInCsvFile() - проверяем в скачаном файле наличие ошибок 'Fatal Error|Warning|Notice'
     */
    public boolean loadExportedFileCab() {
        getAndLoadCsvFile();
        return checkErrorsInCsvFile();
    }

    /**
     * check column for Arrays rows from rows:[1],... to rows:[columnValue.length]
     * @param columnValue - expected result значение с которым нужно сравнить
     */
    public boolean checkColumnForAllRowsInFile(String columnName, String... columnValue) {
        // получаем значение ячейки на пересечении columnName :: rowNumber -> (actual result)
        String cell;
        int matchesWithExpectedData = 0;

        for (int i = 1; i <= columnValue.length; i++) {
            cell = String.valueOf(workbook
                    .getSheetAt(0)
                    .getRow(i)
                    .getCell(getColumn(columnName))).replace("\u00A0", "");

            // сравниваем значение ячейки cell(actual result) с expected (columnValue)
            if (cell.equalsIgnoreCase(columnValue[i - 1]) ||
                    cell.contains(columnValue[i - 1]) ||
                    columnValue[i - 1].contains(cell)) {
                matchesWithExpectedData++;

            } else {
                System.err.println(Thread.currentThread().getStackTrace()[3].getMethodName() + ":" + cell + " != " + Arrays.toString(columnValue));

                return false;
            }
        }

        return matchesWithExpectedData == columnValue.length;
    }

    public boolean checkColumnForCustomRow(String columnName, String columnValue) {
        String cell = String.valueOf(workbook
                .getSheetAt(0)
                .getRow(customRow)
                .getCell(getColumn(columnName))).replace("\u00A0", "");

        return cell.equalsIgnoreCase(columnValue) ||
                cell.contains(columnValue) ||
                columnValue.contains(cell);
    }


    /**
     * helper - который сравнивает значение конкретного поля документа с переданным значением
     *
     * @param columnNumber - номер колонки значение ячейки которой нужно проверить
     * @param columnValue  - expected result значение с которым нужно сравнить
     */
    public boolean checkCellsValueByColumnNumber(int columnNumber, String... columnValue) {
        // получаем значение ячейки на пересечении columnName :: rowNumber -> (actual result)
        String cell;

        int matchesWithExpectedData = 0;
        for (int i = 0; i <= columnValue.length - 1; i++) {
            cell = String.valueOf(workbook
                    .getSheetAt(0)
                    .getRow(i)
                    .getCell(columnNumber)).replace("\u00A0", "");

            // сравниваем значение ячейки cell(actual result) с expected (columnValue)
            if (cell.equalsIgnoreCase(columnValue[i]) ||
                    cell.contains(columnValue[i]) ||
                    columnValue[i].contains(cell)) {
                matchesWithExpectedData++;
            } else {
                System.err.println(Thread.currentThread().getStackTrace()[3].getMethodName() + ":" + cell + " != " + Arrays.toString(columnValue));
                return false;
            }
        }

        return matchesWithExpectedData == columnValue.length;
    }

    /**
     * метод проверяет что все переданные headers отображаются в скачаном документе
     *
     * @param headers - массив названий колонок по порядку
     */
    public boolean checkShowAllColumnNames(String[] headers, int... rowNumberForHeader) {
        int rowNumber = rowNumberForHeader.length > 0 ? rowNumberForHeader[0] : 0;
        ArrayList<String> list = new ArrayList<>();
        Row rowIterator = workbook.getSheetAt(0).getRow(rowNumber);
        Iterator<Cell> cellIterator = rowIterator.cellIterator();

        while (cellIterator.hasNext()) {
            String s = cellIterator.next().getStringCellValue().trim().replace("\uFEFF", "");
            if (!s.equals("")) list.add(s);
        }

        for (String header : headers) {
            if (!list.contains(header)) {
                log.info("headers: " + header);
                list.forEach(j -> log.info("list: " + j));
                return false;
            }
        }

        return true;
    }

    /**
     * метод кликает и скачивает файл, после чего локальный файл готов для работы с ним
     */
    private void getAndLoadCsvFile(char... delimiter) {
        File file;
        try {
            // получаем файл (.csv)
            file = getLocalFile();

            // costil
            sleep(1000);

            // проверяем расширение файла, если .csv, то конвертируем в .xls
            if (extension.equals("csv")) {

                // конвертируем csv to xls
                // получаем xls файл
                file = convertCsvToXls(file, delimiter);
            }

            // читаем xls файл для дальнейшей работы с ним
            workbook = WorkbookFactory.create(file);
            deleteLocalFile();
        } catch (Exception e) {
            log.info("Catch " + e);
        }
    }

    /**
     * копируем файл в локальный файл - после чего файл 'csvFileName' доступен для чтения
     */
    private File getLocalFile(String... nameOfFile) {
        Path of = Path.of(csvFileName);
        try {
            Files.copy(getFilePathRemote(nameOfFile), of, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            log.info("Catch " + e);
        }
        return of.toFile();
    }

    /**
     * получаем порядковый номер строчки с которой планируем работать ->
     * для этого нам нужно получить пересечение заголовка колонки и одного из значений этой колонки
     */
    private void getCustomRowNumber(String columnName, String columnValue) {
        int column = getColumn(columnName);
        workbook
                .getSheetAt(0)
                .forEach(row -> row.forEach(cell -> {
                    if (String.valueOf(row.getCell(column)).contains(columnValue)) {
                        customRow = row.getRowNum();
                    }
                }));
        log.info("customRow: " + customRow);
    }

    /**
     * получаем порядковый номер колонки
     */
    private Integer getColumn(String headerName) {
        int number = 0;

        for (int i = 0; i < 2; i++) {
            Row rowIterator = workbook.getSheetAt(0).getRow(i);
            Iterator<Cell> cellIterator = rowIterator.cellIterator();

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.getCellType().name().equals("STRING") &&
                        cell.getStringCellValue().trim().replace("\uFEFF", "").equals(headerName)) {
                    number = cell.getColumnIndex();
                    i = 2;
                    break;
                }
            }
        }
        return number;
    }

    /**
     * метод конвертации csv to xls
     */
    private File convertCsvToXls(File csvFile, char... delimiter) {
        Sheet sheet;
        CSVReader reader = null;
        Workbook workBook = null;
        FileOutputStream fileOutputStream = null;

        try {
            // Get the CSVReader Instance & Specify The Delimiter To Be Used
            String[] nextLine;

            if (delimiter.length > 0) {
                reader = new CSVReaderBuilder(new InputStreamReader(new FileInputStream(csvFile))).
                        withCSVParser(new CSVParserBuilder().withSeparator(delimiter[0]).build()).build();
            } else reader = new CSVReader(new InputStreamReader(new FileInputStream(csvFile)));

            workBook = new HSSFWorkbook();

            sheet = workBook.createSheet("sheet3");


            int rowNum = 0;
            log.info("Creating New .Xls File From The Already Generated .Csv File");
            while ((nextLine = reader.readNext()) != null) {
                Row currentRow = sheet.createRow(rowNum++);
                for (int i = 0; i < nextLine.length; i++) {
                    if (NumberUtils.isDigits(nextLine[i])) {
                        currentRow.createCell(i).setCellValue(nextLine[i]);
                    } else if (NumberUtils.isNumber(nextLine[i])) {
                        currentRow.createCell(i).setCellValue(Double.parseDouble(nextLine[i]));
                    } else {
                        currentRow.createCell(i).setCellValue(nextLine[i]);
                    }
                }
            }

            log.info("The File Is Generated At The Following Location?= " + xlsFileName);

            fileOutputStream = new FileOutputStream(xlsFileName);
            workBook.write(fileOutputStream);
        } catch (Exception exObj) {
            log.info("Exception In convertCsvToXls() Method?=  " + exObj);
        } finally {
            try {
                // Closing The Excel Workbook Object
                assert workBook != null;
                workBook.close();

                // Closing The File-Writer Object
                assert fileOutputStream != null;
                fileOutputStream.close();

                // Closing The CSV File-ReaderObject
                reader.close();
            } catch (IOException ioExObj) {
                log.info("Exception While Closing I/O Objects In convertCsvToXls() Method?=  " + ioExObj);
            }
        }
        return new File(xlsFileName);
    }

    /**
     * click csv button to export file
     */
    private void clickButtonCsvDash() {
        EXPORT_BUTTON_DASH.shouldBe(visible).click();
        EXPORT_POPUP.shouldBe(attribute("style", "display: block;"));
        CSV_EXPORT_BUTTON.shouldBe(visible).click();
    }

    /**
     * метод проверяет все строчки в документе на наличие в неё errors (Fatal, Warning, Notice)
     */
    private boolean checkErrorsInCsvFile() {
        int[] count = new int[1];
        workbook.getSheetAt(0).forEach(row -> row.forEach(cell -> {
            if (helpCheckErrors(cell)) {
                count[0]++;
            }
        }));
        return count[0] == 0;
    }

    /**
     * help метод для checkErrorsInCsvFile()
     */
    private boolean helpCheckErrors(Cell cell) {
        String value = String.valueOf(cell);
        if (value.contains("Fatal Error") |
                value.contains("Warning") |
                value.contains("Notice")) {

            log.info(value);

            return true;
        }
        return false;
    }

    /**
     * close all file and delete their
     */
    public void closeAllFile() {
        try {
            File csvFile = new File(csvFileName);
            File xlsFile = new File(xlsFileName);
            //закрываем работу со всеми активними файлами
            if (workbook != null) workbook.close();
            if (csvFile.exists()) csvFile.delete();
            if (xlsFile.exists()) xlsFile.delete();
            //все активные файлы закрыты
        } catch (Exception e) {
            System.err.println("catch " + e);
        }
    }
}
