package libs.downloads.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ExportInDashboardLocators {

    public static final SelenideElement EXPORT_BUTTON_DASH = $("[class*=csv_pdf]");
    public static final SelenideElement EXPORT_POPUP = $(".export_popup");
    public static final SelenideElement CSV_EXPORT_BUTTON = $("[class='csv']");
}
