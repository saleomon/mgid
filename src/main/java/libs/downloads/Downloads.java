package libs.downloads;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.remote.SessionId;
import core.service.PropertiesManager;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.sleep;
import static core.service.PropertiesManager.getResourceByName;

public class Downloads {
    protected Logger log;
    protected SessionId sessionId;
    private final String host = getResourceByName(PropertiesManager.ConfigValue.HOST);
    private String fileName;
    private String fileNameForBrowserLink;
    protected String extension;

    public Downloads(SessionId sessionId, Logger log) {
        this.log = log;
        this.sessionId = sessionId;
    }

    public Downloads() {
    }

    public String getFileName() { return fileName;}

    /**
     * delete local file
     */
    protected void deleteLocalFile() {
        try {
            Runtime.getRuntime().exec("curl -X DELETE http://" + host + ":4444/download/" + sessionId + "/" + fileName.replaceAll(" ", "%20"));
        } catch (Exception e) {
            System.err.println("Catch " + e);
        }
    }

    /**
     * метод делает два действия
     * 1. идём в директорию :4444/download/ - именно в эту папку selenoid складывает скаченные файлы из браузера
     *  - дожидаемся пока файл появится в папке
     * 2. дожидаемся пока файл докачается в папку download
     */
    protected InputStream getFilePathRemote(String... nameOfFile) {
        InputStream in = null; int time_0 = 0, time = 0;
        Elements links = null;
        try {
            do {
                try {
                    do {
                        Document doc = Jsoup.connect("http://" + host + ":4444/download/" + sessionId).get();
                        doc.childNodeSize();
                        links = doc.getElementsByTag("a");
                        links.forEach(el-> System.out.println(Thread.currentThread().getStackTrace()[3].getMethodName() + "links - " + el));
                        time_0++;
                        sleep(500);
                    }
                    while (links.size() == 0 && time_0 < 100);
                    if(nameOfFile.length > 0) {
                        for (Element link : links) {
                            System.out.println(Thread.currentThread().getStackTrace()[3].getMethodName() + "links loop - " + link.text());
                            System.out.println(Thread.currentThread().getStackTrace()[3].getMethodName() + "href loop - " + link.attr("href"));
                            if (link.text().contains(nameOfFile[0])) {
                                fileName = link.text();
                                fileNameForBrowserLink = link.attr("href");
                            }
                        }
                    } else {
                        System.out.println(Thread.currentThread().getStackTrace()[3].getMethodName() + "else block text loop - " + links.get(0).text());
                        System.out.println(Thread.currentThread().getStackTrace()[3].getMethodName() + "else block href loop - " + links.get(0).attr("href"));
                        fileName = links.get(0).text();
                        fileNameForBrowserLink = links.get(0).attr("href");
                    }
                    String path = "http://" + host + ":4444/download/" + sessionId + "/" + fileNameForBrowserLink;

                    // получаем расширение файла
                    extension = FilenameUtils.getExtension(path);

                    in = new URL(path).openStream();
                    System.out.println("in - " + in.toString());
                } catch (FileNotFoundException f){
                    sleep(600);
                    time++;
                }
            }
            while (Objects.requireNonNull(links).stream().anyMatch(link->link.text().contains("crdownload")) && time < 60);
        }
        catch (Exception e){

            log.info("Catch " + e);
        }
        return in;
    }
}
