package libs.hikari.utils;

import com.zaxxer.hikari.HikariDataSource;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import static libs.hikari.utils.PoolConnections.*;
import static libs.hikari.utils.PoolConnections.MySqlBds.BANK;

public class BankInstructions {
    public static HikariDataSource dsBank = dataSource(BANK);

    public static BigDecimal getConnectionPartnersSelectColumn(String queryText) {
        ResultSet rs;
        try {
            try (Connection db = getConnection(dsBank)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        return rs.getBigDecimal(1);
                    }
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
