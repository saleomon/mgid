package libs.hikari.utils;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class UtilsHelper {
    /**
     * Возвращает случайное число из диапазона от 0 до заданного числа
     */
    public static int randomNumbersInt(int bound) {
        return new Random().nextInt(bound);
    }

    /**
     * join List to String and join ','
     */
    public static String joinListToString(List list){
        return (String) list.stream().map(Object::toString).collect(Collectors.joining(","));
    }

}

