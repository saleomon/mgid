package libs.hikari.utils;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PoolConnections {

    public static HikariDataSource ds;

    public static HikariDataSource dataSource(MySqlBds dbName) {
        HikariConfig configPartners = new HikariConfig();
        configPartners.setJdbcUrl("jdbc:mysql://" +  System.getenv("DB_HOST") + ":" + System.getenv("DB_PORT") + "/" + dbName.getTypeValue() + "?serverTimezone=UTC");
        configPartners.setUsername(System.getenv("DB_USER"));
        configPartners.setPassword(System.getenv("DB_PASS"));
        configPartners.setMaximumPoolSize(100);
        return new HikariDataSource(configPartners);
    }

    public static Connection getConnection(HikariDataSource ds){
        try {
            return ds.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public enum MySqlBds {
        PARTNERS("partners"),
        ADMIN("admin"),
        BANK("bank"),
        GENERAL("general");

        private final String dbName;

        MySqlBds(String targetValue) {
            this.dbName = targetValue;
        }

        public String getTypeValue() {
            return dbName;
        }
    }

    public static void setAuthLoginVariables(Connection conn) {
        try {
            try (PreparedStatement query =
                         conn.prepareStatement("SET @auth_login :='test'")) {
                query.executeUpdate();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
