package libs.hikari.utils;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import static libs.hikari.utils.PoolConnections.MySqlBds.GENERAL;

public class GeneralInstructions extends PoolConnections{

    public static HikariDataSource dbGeneral = dataSource(GENERAL);

    public static String getConnectionPartnersSelectColumn(String queryText) {
        ResultSet rs;
        try {
            try (Connection db = getConnection(dbGeneral)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        return rs.getString(1);
                    }
                    rs.close();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
