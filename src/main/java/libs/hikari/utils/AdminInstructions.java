package libs.hikari.utils;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static libs.hikari.utils.PoolConnections.MySqlBds.ADMIN;

public class AdminInstructions extends PoolConnections {

    public static HikariDataSource dsAdmin = dataSource(ADMIN);

    public static void getConnectionAdminUpdate(String queryText) {
        try {
            try (Connection db = getConnection(dsAdmin)) {
                setAuthLoginVariables(db);
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                             query.executeUpdate();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static List<String> getConnectionPartnersSelectList(String queryText) {
        ResultSet rs;
        List<String> list = new ArrayList<>();
        try {
            try (Connection db = getConnection(dsAdmin)) {
                try (PreparedStatement query =
                             db.prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        list.add(rs.getString(1));
                    }
                    rs.close();
                    return list;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static Map<String, String> getConnectionAdminSelectFewColumns(String queryText) {
        ResultSet rs;
        Map<String, String> map = new HashMap<>();
        String[] arrayOfColumns = queryText.substring(queryText.indexOf(" "), queryText.indexOf("from")).trim().split(", ");
        try {
            try (Connection db = getConnection(dsAdmin)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        for(int i = 0; i < arrayOfColumns.length; i++) {
                            map.put(arrayOfColumns[i], rs.getString(i + 1));
                        }
                    }
                    rs.close();
                    return map;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * @return - 1 column from 1 row
     */
    public static String getConnectionAdminSelectColumn(String queryText) {
        ResultSet rs;
        try {
            try (Connection db = getConnection(dsAdmin)) {
                try (PreparedStatement query =
                             db.prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        return rs.getString(1);
                    }
                    rs.close();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
