package libs.hikari.utils;

import com.zaxxer.hikari.HikariDataSource;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;
import java.sql.*;
import java.util.*;

import static libs.hikari.utils.PoolConnections.MySqlBds.PARTNERS;

public class PartnersInstructions extends PoolConnections {
    public static HikariDataSource dsPartners = dataSource(PARTNERS);
    private static final String LINE_SEPARATOR = System.getProperty("line.separator", "\n");

    public static void getConnectionPartnersExecuteQuery(String queryText) {
        try {
            try (Connection db = getConnection(dsPartners)) {
                setAuthLoginVariables(db);
                try (PreparedStatement query =
                             db.prepareStatement(queryText)) {
                    query.executeUpdate();
                    db.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isEmptySelect(String queryText) {
        ResultSet rs;
        try (Connection db = getConnection(dsPartners)) {
            try (PreparedStatement query =
                         Objects.requireNonNull(db).prepareStatement(queryText)) {
                rs = query.executeQuery();
                while (rs.next()) {
                    return rs.getString(1).isEmpty();
                }
                rs.close();
            }
            return true;
        } catch (SQLException ex) {
            System.out.println("Database connection failure: "
                    + ex.getMessage());
        }
        return false;
    }

    public static boolean getConnectionPartnersDelete(String queryText) {
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             db.prepareStatement(queryText)) {
                    return query.execute();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * @return - 1 column from ALL rows
     */
    public static List<String> getConnectionPartnersSelectList(String queryText) {
        ResultSet rs;
        List<String> list = new ArrayList<>();
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        list.add(rs.getString(1));
                    }
                    rs.close();
                    return list;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return - ALL column(join ',') from ALL rows
     */
    public static ArrayList<String> getConnectionPartnersSelectAllColumns(String queryText) {
        ResultSet rs;
        ArrayList<String> list = new ArrayList<>();
        ResultSetMetaData metadata;
        int columnCount;
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    metadata = rs.getMetaData();
                    columnCount = metadata.getColumnCount();
                    while (rs.next()) {
                        StringBuilder row = new StringBuilder();
                        for (int i = 1; i <= columnCount; i++) {
                            row.append(rs.getString(i)).append(", ");
                        }
                        list.add(row.toString());
                    }
                    rs.close();
                    return list;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return - 1 column from 1 row
     */
    public static String getConnectionPartnersSelectColumn(String queryText) {
        ResultSet rs;
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             db.prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        return rs.getString(1);
                    }
                    rs.close();
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * @return - 1,2 columns from ALL rows
     */
    public static List<Object> getConnectionPartnersSelectFieldsList(String queryText) {
        ResultSet rs;
        List<Object> list = new ArrayList<>();
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        list.add(rs.getString(1));
                        list.add(rs.getString(2));
                    }
                    rs.close();
                    return list;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * @return all columns which set in query from 1 row
     */
    public static Map<String, String> getConnectionPartnersSelectFewColumns(String queryText) {
        ResultSet rs;
        Map<String, String> map = new HashMap<>();
        String[] arrayOfColumns = queryText.substring(queryText.indexOf(" "), queryText.indexOf("from")).trim().split(", ");
        try {
            try (Connection db = getConnection(dsPartners)) {
                try (PreparedStatement query =
                             Objects.requireNonNull(db).prepareStatement(queryText)) {
                    rs = query.executeQuery();
                    while (rs.next()) {
                        for (int i = 0; i < arrayOfColumns.length; i++) {
                            map.put(arrayOfColumns[i], rs.getString(i + 1));
                        }
                    }
                    rs.close();
                    return map;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public static void pushDumpToTable(String path) {
        String line;
        StringBuilder script = new StringBuilder();
        try {
            Reader reader = new BufferedReader(new FileReader(path));
            BufferedReader lineReader = new BufferedReader(reader);
            while ((line = lineReader.readLine()) != null) {
                if (line.length() == 0) {
                    getConnectionPartnersExecuteQuery(script.toString());
                    script.delete(0, script.length() - 1);
                } else if (line.charAt(0) == '-') {

                } else if ((line.charAt(0) == 'I' || line.charAt(0) == 'U' || line.charAt(0) == 'D' || line.charAt(0) == '(') && line.charAt(line.length() - 1) == ';') {
                    script.append(line);
                    script.append(LINE_SEPARATOR);
                    getConnectionPartnersExecuteQuery(script.toString());
                    script.delete(0, script.length() - 1);
                } else {
                    script.append(line);
                    script.append(LINE_SEPARATOR);
                }
            }
            if (script.toString().length() > 1) {
                getConnectionPartnersExecuteQuery(script.toString());
            }
            System.out.println(path);
        } catch(Exception e) {
            line = "Error executing: " + script + ".  Cause: ";
            System.out.println(line);
            System.out.println(e);
        }
    }
}

