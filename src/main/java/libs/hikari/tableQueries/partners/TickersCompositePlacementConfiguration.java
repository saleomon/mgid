package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class TickersCompositePlacementConfiguration {

    public  List<String> getPlacementType(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectList("select type from tickers_composite_placement_configuration where tickers_composite_id =" + tickersCompositeId);
    }
}
