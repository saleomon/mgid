package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TickersCategories {
    public void deleteAllCategories(int... tickers_uid){
        String values = Arrays.stream(tickers_uid).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from tickers_categories where tickers_uid IN(" + values + ")");
    }

    public void insertCategories(int tickers_uid, int... id_category){
        String val = Arrays.stream(id_category).mapToObj(i -> "(" + tickers_uid + ", " + i + ")").collect(Collectors.joining(","));
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into tickers_categories(tickers_uid, id_category) VALUES " + val + ";");
    }
}
