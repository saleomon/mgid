package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.ArrayList;

public class WidgetsSlackNotificationSettings {

    /**
     * check current row in query with concat data ( from Map and other arg )
     */
    public ArrayList<String> getAllDataWidgetsSlackNotificationByWidgetId(int widgetId){
        return PartnersInstructions.getConnectionPartnersSelectAllColumns("select widget_id," +
                "alert_type," +
                "delta, " +
                "delta_number, " +
                "hour_from, " +
                "hour_to, " +
                "period " +
                "from widgets_slack_notification_settings " +
                "where widget_id = " + widgetId);
    }
}
