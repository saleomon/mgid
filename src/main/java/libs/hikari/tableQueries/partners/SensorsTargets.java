package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SensorsTargets {
    public static int insertTarget(int clientId, String targetName, int conversionCategory, String ident, String unionType){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into sensors_targets (client_id, ident, sensors_targets_conversion_categories_id, name, union_type) values ("+
                clientId + ", '" +
                ident + "', " +
                conversionCategory + ", '" +
                targetName + "', '" +
                unionType + "');");

        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select id from sensors_targets where name ='" + targetName + "'")));
    }

    public void makeDeletedAllClientsTargets(int clientId){
        PartnersInstructions.getConnectionPartnersExecuteQuery(String.format("update `sensors_targets` SET `droped`= 1 WHERE client_id = %s;",
                clientId));
    }

    public boolean isExistEventTarget(String name, String description, String event, boolean fulfilmentOfAllConditions){
        String union_type = !fulfilmentOfAllConditions
                ?
                "AND"
                :
                "OR";
        return PartnersInstructions.getConnectionPartnersSelectList("select id from sensors_targets " +
                "where name ='" + name + "' " +
                "and description = '" + description + "' " +
                "and union_type = '" + union_type + "' " +
                "and ident = '" + event + "'").size() > 0;
    }

    public boolean isExistVisitedTarget(String name, String description, String visited, String domain){
        return PartnersInstructions.getConnectionPartnersSelectList(
                "select st.id from sensors_targets st " +
                "join sensors_target_conditions stc " +
                "on st.id = stc.target_id " +
                "where name ='" + name + "' " +
                "and description = '" + description + "' " +
                "and stc.type IN('visited', 'contain')\n" +
                "and stc.value IN(" + Integer.parseInt(visited) + ", '" + domain + "')").size() == 2;
    }

    public boolean isExistUrlTarget(String name, String description, LinkedHashMap<String, String> urlData){
        String types = (new ArrayList<>(urlData.values())).stream()
                .collect(Collectors.joining("', '","'","'"));

        String values = (new ArrayList<>(urlData.keySet())).stream()
                .collect(Collectors.joining("', '","'","'"));

        return PartnersInstructions.getConnectionPartnersSelectList(
                "select st.id from sensors_targets st " +
                        "join sensors_target_conditions stc " +
                        "on st.id = stc.target_id " +
                        "where name ='" + name + "' " +
                        "and description = '" + description + "' " +
                        "and stc.type IN(" + types + ")\n" +
                        "and stc.value IN(" + values + ")").size() == urlData.size();
    }

    public boolean isExistClickersTarget(String name, String description, List clickersData){
        String values = (String) clickersData.stream()
                .collect(Collectors.joining("', '","'","'"));

        return PartnersInstructions.getConnectionPartnersSelectList(
                "select st.id from sensors_targets st " +
                        "join sensors_target_conditions stc " +
                        "on st.id = stc.target_id " +
                        "where name ='" + name + "' " +
                        "and description = '" + description + "' " +
                        "and stc.type like 'ad_clickers'\n" +
                        "and stc.value IN(" + values + ")").size() == clickersData.size();
    }

    public boolean isExistViewersTarget(String name, String description, List viewersData){
        String values = (String) viewersData.stream()
                .collect(Collectors.joining("', '","'","'"));

        return PartnersInstructions.getConnectionPartnersSelectList(
                "select st.id from sensors_targets st " +
                        "join sensors_target_conditions stc " +
                        "on st.id = stc.target_id " +
                        "where name ='" + name + "' " +
                        "and description = '" + description + "' " +
                        "and stc.type like 'ad_viewers'\n" +
                        "and stc.value IN(" + values + ")").size() == viewersData.size();
    }
}