package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ClientsSitesDefaultWidgetCategories {

    public void deleteAllCategories(int... site_id){
        String values = Arrays.stream(site_id).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from clients_sites_default_widget_categories where site_id IN(" + values + ")");
    }

    public void insertWidgetCategories(int site_id){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into clients_sites_default_widget_categories(site_id, category_id) VALUES (" + site_id + ", 100);");
    }

    public boolean isExistCategoryInWidget(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select category_id from clients_sites_default_widget_categories where site_id = " + site_id).size() > 0;
    }

    public List<String> getSelectedCategoriesNames(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select g_category.name FROM g_category " +
                "join clients_sites_default_widget_categories on g_category.id=clients_sites_default_widget_categories.category_id " +
                "where clients_sites_default_widget_categories.site_id = " + site_id);
    }
}
