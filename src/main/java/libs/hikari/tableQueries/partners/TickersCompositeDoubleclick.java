package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class TickersCompositeDoubleclick {

    public void setDoubleClickParameters(int tickers_composite_id, String parameters){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into tickers_composite_doubleclicks " +
                "(tickers_composite_id, parameters) values " +
                "(" + tickers_composite_id + ", " + parameters + ");");
    }

    public String getDoubleClickParameters(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select parameters from tickers_composite_doubleclicks where tickers_composite_id = " + tickers_composite_id);
    }
}
