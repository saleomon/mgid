package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;

public class News1 {

    public void updateModerationStatus(String status, int... newsIds){
        String s = Arrays.toString(newsIds).replaceAll("[\\[\\]]", "");
        PartnersInstructions.getConnectionPartnersExecuteQuery("update news_1 set moderation_status = '" + status + "', " +
                "moderator = 'user_auto', " +
                "karantin = 1, " +
                "droped = 1, " +
                "who_drop = null, " +
                "when_drop = null, " +
                "reason = null, " +
                "ad_types = 'r', " +
                "reason_if_drop_karantin = null " +
                "where id in(" + s + ")");
    }

    public String getWhenChange(String newsId) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("" +
                "select when_change from news_1 where id = " + newsId);
    }

    public void updateActiveStatus(String newsId) {
        PartnersInstructions.getConnectionPartnersExecuteQuery("update news_1 set " +
                "karantin = 0, " +
                "droped = 0 " +
                "where id = " + newsId);
    }

    public static int selectNewsIdByTitle(String title) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select id from news_1 where title like '" + title + "'")));
    }

    public static int selectShowOnTranzPage(int newsId) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select show_on_tranz_page from news_1 where id = " + newsId)));
    }


}
