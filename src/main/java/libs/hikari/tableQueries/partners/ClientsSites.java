package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class ClientsSites {

    public int selectSiteId(String domain){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select id from ClientsSitesEntity where domain ='" + domain + "'")));
    }

    public void updateCooperationType(int websiteId, String cooperationTypes){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set cooperation_types_priority = '[\"" + cooperationTypes + "\"]' where id = " + websiteId);
    }

    public void updateTier(int tierId, int websiteId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set tier_id = '" + tierId + "' where id = " + websiteId);
    }

    public void updateCategory(int categoryPlatformId, int websiteId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set category_platform = '" + categoryPlatformId + "' where id = " + websiteId);
    }

    public void updateAntifraudBlocked(int antifraud_blocked, int websiteId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set antifraud_blocked = '" + antifraud_blocked + "' where id = " + websiteId);
    }

    public void updateToDefaultPostModerationSettings(int... websiteId){
        String values = Arrays.stream(websiteId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set " +
                "post_moderation_enabled = 1, " +
                "post_moderation_options = '{}', " +
                "post_moderation_comment = '' " +
                "where id IN(" + values + ")");
    }

    public String getPostModerationEnabled(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select post_moderation_enabled from clients_sites where id = " + site_id);
    }

    public String getPostModerationClicks(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select clicks_postmoderation from clients_sites where id = " + site_id);
    }

    public String getSiteLanguageId(String siteId) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select languages_id from clients_sites where id = " + siteId);
    }

    public String getPushProvidersId(String siteId) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select push_providers_id from clients_sites where id = " + siteId);
    }

    public String getSourcesId(String siteId) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select sources_id from clients_sites where id = " + siteId);
    }

    public String getDefaultWidgetCategoriesFilter(int siteId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select default_widget_categories_filter from clients_sites where id = " + siteId);
    }

    public void updateDefaultWidgetCategoriesFilter(int siteId, int filterValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites set default_widget_categories_filter = " + filterValue + " where id = " + siteId);
    }

    public String getMirrorDomains(String siteId) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select mirror_domains from clients_sites where id = " + siteId);
    }

    public void updatePushProvidersId(int push_providers_id, int category_platform, int websiteId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites " +
                "set push_providers_id =" + push_providers_id +
                ", category_platform =" + category_platform +
                ", sources_id=null" +
                " where id =" + websiteId);
    }

    public void updateSourcesId(int sources_id, int category_platform, int websiteId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites " +
                "set sources_id =" + sources_id +
                ", category_platform =" + category_platform +
                ", push_providers_id=null" +
                " where id =" + websiteId);
    }

    public int getCoppaValue(String siteId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select coppa from clients_sites where id ='" + siteId + "'")));
    }
}
