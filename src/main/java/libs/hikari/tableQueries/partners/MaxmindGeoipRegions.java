package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.stream.Collectors;

public class MaxmindGeoipRegions {

    public List getListRegionsByCountry(String countries){
        return PartnersInstructions.getConnectionPartnersSelectList("select id from maxmind_geoip_regions where country_id IN(" + countries + ")");
    }

    public List getListCountriesByRegion(List<String> regionName){
        String joinedString = regionName
                .stream()
                .collect(Collectors.joining("', '","'","'"));
        return PartnersInstructions.getConnectionPartnersSelectList("select country_id from maxmind_geoip_regions where name IN(" + joinedString + ")");
    }

    public List getListRegionsByName(List<String> regionName){
        String joinedString = regionName
                .stream()
                .collect(Collectors.joining("', '","'","'"));
        String geoIdentificationId = PartnersInstructions.getConnectionPartnersSelectColumn("select geo_identification_id from maxmind_geoip_regions where name IN(" + joinedString + ")");
        return PartnersInstructions.getConnectionPartnersSelectList("select id from maxmind_geoip_regions where geo_identification_id = " + geoIdentificationId);
    }

    public List getListZonesByCountries(String countries){
        return PartnersInstructions.getConnectionPartnersSelectList("select distinct geo_zones_id from maxmind_geoip_regions where country_id IN(" + countries + ")");
    }

    public List getListZonesByRegions(List<String> regionName){
        String joinedString = regionName
            .stream()
            .collect(Collectors.joining("', '","'","'"));
        return PartnersInstructions.getConnectionPartnersSelectList("select distinct geo_zones_id from maxmind_geoip_regions where name IN(" + joinedString + ")");
    }
}
