package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class ModeratorsHints {
    public void updateStatusModerationHints(int status, int...teaserId){
        String values = Arrays.stream(teaserId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("update moderators_hints set hint_status = " + status + " where id in(" + values + ")");
    }
    public int getHintEntityId(int teaserId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn(
                "select id from moderators_hints where teaser_id = " + teaserId)));    }
    public int getHintsStatus(int teaserId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn(
                "select hint_status from moderators_hints where teaser_id = " + teaserId)));
    }

    public boolean isPresentHintForTeaser(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select hint_status from moderators_hints where teaser_id = " + teaserId) != null;
    }

    public ModeratorsHints truncateTable(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("TRUNCATE TABLE partners.moderators_hints;");
        return this;
    }

    public void pushDumpToTable(){
        PartnersInstructions.pushDumpToTable("mysql/partners/hints/moderators_hints.sql");
    }
}
