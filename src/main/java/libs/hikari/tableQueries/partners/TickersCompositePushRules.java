package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class TickersCompositePushRules {

    public String getDomainForRule(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select name from tickers_composite_push_rules where tickers_composite_id =" + tickersCompositeId);
    }

    public int getCompositeLifetime0(int tickersCompositeId){
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select composite_lifetime0 from tickers_composite_push_rules where tickers_composite_id =" + tickersCompositeId)));
    }

    public int getCompositeLifetime1(int tickersCompositeId){
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select composite_lifetime1 from tickers_composite_push_rules where tickers_composite_id =" + tickersCompositeId)));
    }
}
