package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class PublishersAdsModeration {

    public boolean selectIsExistPublishersAds(int client_site_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select * from publishers_ads_moderation where client_site_id = " + client_site_id).size() > 0;
    }
}
