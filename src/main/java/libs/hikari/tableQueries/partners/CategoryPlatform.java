package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class CategoryPlatform {

    public boolean getPlatformType(String categoryId){
        return Boolean.parseBoolean(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select platform_type from category_platform where id = " + categoryId)));
    }

    public boolean isPlatformTypePush(String categoryId){
        return PartnersInstructions.getConnectionPartnersSelectList("select * from category_platform where platform_type = 'push' and id = " + categoryId).size() > 0;
    }
}
