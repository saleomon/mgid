package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class DirectPublisherDemandAds {

    public static int selectAddByTitle(String title) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select id from direct_publisher_demand_ads where title = '" + title + "'")));
    }

    public int getDroped(String title) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select droped from direct_publisher_demand_ads where title = '" + title + "'")));
    }

    public int getEnabled(String title) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select enabled from direct_publisher_demand_ads where title = '" + title + "'")));
    }

    public String getCloudinaryEffects(String title){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select cloudinary_effects from direct_publisher_demand_ads where title = '" + title + "'");
    }

    public void updateCreatedAtTime(String createdAtTime, int id){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update direct_publisher_demand_ads set created_at = '" + createdAtTime + "' where id = " + id);
    }
}
