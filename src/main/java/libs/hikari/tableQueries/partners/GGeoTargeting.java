package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class GGeoTargeting {

    /**
     * @param mode - include/exclude
     */
    public void updateGGeoTargeting(int campaignId, String countries, String regions, String oldRegions, String zonesIds, String mode){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_geo_targeting " +
                "set enabled = '" + regions + "', " +
                "disabled = '" + oldRegions + "', " +
                "enabled_countries = '" + countries + "', " +
                "enabled_countries_full = '" + countries + "', " +
                "enabled_geo_zones = '" + zonesIds + "', " +
                "enabled_geo_zones_full = '" + zonesIds + "', " +
                "mode = '" + mode + "' " +
                "where id = " + campaignId
        );
    }

    public String getEnabledRegions(int campaignId) {
        long time = System.currentTimeMillis() / 1000L;

        String regions = PartnersInstructions.getConnectionPartnersSelectColumn("select enabled from g_geo_targeting where id = " + campaignId);
        String neededJson = Arrays.stream(regions.split(",")).map(i -> "\"" + i + "\"")
                .collect(Collectors.joining( ":" + time + ",","{", "}"));
        return neededJson.replace("}", ":" + time + "}");
    }
}
