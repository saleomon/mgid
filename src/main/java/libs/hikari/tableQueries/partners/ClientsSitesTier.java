package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class ClientsSitesTier {

    public List getListTier(){
        return PartnersInstructions.getConnectionPartnersSelectFieldsList("select id, name from clients_sites_tier");
    }
}
