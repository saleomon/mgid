package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class GBlocksCampaignTier {

    GBlocks gBlocks = new GBlocks();

    public String getCampaignTier(int g_blocks_id){
        return String.valueOf(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectList("select uid from g_blocks_campaign_tier where uid = " + g_blocks_id)).size());
    }

    public boolean checkCampaignTier(String tierValue, int g_blocks_id){
        String g_blocks_campaign_tier_val = getCampaignTier(g_blocks_id);
        String g_blocks_val = gBlocks.getCampaignTier(g_blocks_id);

        return g_blocks_campaign_tier_val.equals(tierValue) &
                ((tierValue.equals("2") & g_blocks_val.equals("1")) || tierValue.equals(g_blocks_val));
    }
}
