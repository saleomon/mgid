package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class Clients {

    public void updateClientsGodmodeBlock(String value, String clientId) {
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients set godmode_block = " + value + " where id = " + clientId);
    }

    public int getAmountClientsWithPriority(String priority) {
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from clients  where low_priority_client = " + priority)));
    }

    public String getAdDarknessCheckboxInNewClient(String email) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select can_change_ad_darkness from clients where name = " + "'" + email + "'");
    }

    public String getCloudinaryFlag(String id) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_cloudinary from clients  where id = " + id);
    }

    public String getOptions(String id) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select options from clients  where id = " + id);
    }

    public String getCanVerifyIdenfyByName(String clientName) {
        return PartnersInstructions.getConnectionPartnersSelectColumn("select can_verify_kyc from clients where name = '" + clientName + "'");
    }

    public void updateClientsCanVerifyIdenfy(int clientId, String value) {
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients set can_verify_kyc = '" + value + "' where id = " + clientId);
    }

    public void updateClientsDashboardFirstDayOfTheWeek(String value, String clientId) {
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients set dashboard_first_day_week = '" + value + "' where id = " + clientId);
    }
}
