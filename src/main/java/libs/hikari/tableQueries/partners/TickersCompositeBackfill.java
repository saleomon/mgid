package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class TickersCompositeBackfill {

    public int getChildTickersCompositeId(int parent_tickers_composite_id){
        String val = PartnersInstructions.getConnectionPartnersSelectColumn("select child_tickers_composite_id from tickers_composite_backfill where parent_tickers_composite_id = " + parent_tickers_composite_id);
        return val != null ? Integer.parseInt(val) : 0;
    }
}
