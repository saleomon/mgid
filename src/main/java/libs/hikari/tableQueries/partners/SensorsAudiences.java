package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class SensorsAudiences {
    public List selectAllClientAudiences(int clientId){
        return PartnersInstructions.getConnectionPartnersSelectList(String.format("select id from sensors_audiences where client_id = %s AND DROPED = 0",
                clientId));
    }
}