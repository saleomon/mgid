package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class TeasersLandingsOffers {
    public String selectFromShowName(String offerName) {
        return PartnersInstructions.getConnectionPartnersSelectColumn(String.format("select from_show_name from teasers_landings_offers where offer_name = '%s';", offerName));
    }
}
