package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class GBlocks {

    public int getWrongShowsEnabled(int tickerCompositeId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select g.wrong_shows_enabled from g_blocks g join tickers_composite t on g.tickers_composite_id=t.id where t.id = " + tickerCompositeId)));
    }

    public void updateAdsFilter(int g_blocks_id, int filterValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks set ads_filter = " + filterValue + " where id = " + g_blocks_id);
    }

    public void updatePushProvidersId(int push_providers_id, int category_platform, int gBlocksId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks " +
                "set push_providers_id =" + push_providers_id +
                ", category_platform =" + category_platform +
                ", sources_id=null" +
                " where id =" + gBlocksId);
    }

    public void updateSourcesId(int sourcesId, int category_platform, int gBlocksId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks " +
                "set sources_id =" + sourcesId +
                ", category_platform =" + category_platform +
                ", push_providers_id=null" +
                " where id =" + gBlocksId);
    }

    public void updateUseCat(int g_blocks_id, int filterValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks set use_cat = " + filterValue + " where id = " + g_blocks_id);
    }

    public void updatePushProvider(int g_blocks_id){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks set push_providers_id = 1 where id = " + g_blocks_id);
    }

    public String getPushProviderId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select push_providers_id from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getUpdatedTime(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select updated_time from g_blocks where id = " + g_blocks_id);
    }

    public int getRtbEnabled(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select rtb_enabled from g_blocks where id = " + g_blocks_id)));
    }

    public String getIncludeFakeTeasers(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select include_fake_teasers from g_blocks where id = " + g_blocks_id);
    }

    public int getOnlyOwnSubnet(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select only_own_subnet from g_blocks where id = " + g_blocks_id)));
    }

    public int getShowDescription(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select show_description from g_blocks where id = " + g_blocks_id)));
    }

    public String getCampaignTier(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select campaign_tier from g_blocks where id = " + g_blocks_id);
    }

    public int getNoCalcGhitsCtr(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select no_calc_ghits_ctr from g_blocks where id = " + g_blocks_id)));
    }

    public int getBlockDuplicate(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select block_duplicate from g_blocks where id = " + g_blocks_id)));
    }

    public int getSpyFeed(int tickers_composite_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select spy_feed from g_blocks where tickers_composite_id = " + tickers_composite_id)));
    }

    public int getAgencyFeeToWages(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select agency_fee_to_wages from g_blocks where id = " + g_blocks_id)));
    }

    public int getGBlocksId(int tickers_composite_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select id from g_blocks where tickers_composite_id = " + tickers_composite_id)));
    }

    public String getPagesCount(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select pages_count from g_blocks where id = " + g_blocks_id);
    }

    public String getDuplicatesCount(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select duplicates_count from g_blocks where id = " + g_blocks_id);
    }

    public String getOurBlockCount(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select our_block from g_blocks where id = " + g_blocks_id);
    }

    public String getUseTranzPage(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_tranz_page from g_blocks where id = " + g_blocks_id);
    }

    public void setUseTranzPageByCompositeId(int tickers_composite_id, int stepValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_blocks set use_tranz_page = " + stepValue + " where id = " + tickers_composite_id);
    }

    public String getUseTranzPageByCompositeId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_tranz_page from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getRtbEnabledByCompositeId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select rtb_enabled from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getWrongClicksEnabledByCompositeId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select wrong_clicks_enabled from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getUseAfTransitPageByCompositeId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_af_transit_page from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public int getLiabilityByCompositeId(int tickers_composite_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select liability from g_blocks where tickers_composite_id = " + tickers_composite_id)));
    }

    public String getLiabilityCommentByCompositeId(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select liability_comment from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getDefaultMinCpcDecreasingFactor(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select default_min_cpc_decreasing_factor from g_blocks where id = " + g_blocks_id);
    }

    public String getMinimalPriceOfClick(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select minimal_price_of_click from g_blocks where id = " + g_blocks_id);
    }

    public String getAnimation(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select animation from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getAnimationAddGranularity(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select animation_add_granularity from g_blocks where id = " + g_blocks_id);
    }

    public String getFemaleContent(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select female_content from g_blocks where id = " + g_blocks_id);
    }

    public String getMirror(int tickers_composite_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select mirror from g_blocks where tickers_composite_id = " + tickers_composite_id);
    }

    public String getUseCat(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_cat from g_blocks where id = " + g_blocks_id);
    }

    public String getSourcesId(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select sources_id from g_blocks where id = " + g_blocks_id);
    }
}
