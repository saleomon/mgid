package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class GBlocksSubnets {

    public boolean isSetGBlocksSubnets(int g_blocks_id){
        return Boolean.parseBoolean(String.valueOf(PartnersInstructions.getConnectionPartnersSelectList("select * from g_blocks_subnets where block_uid = " + g_blocks_id).size() > 0));
    }
}
