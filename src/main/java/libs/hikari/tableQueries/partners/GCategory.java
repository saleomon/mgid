package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.Objects;

public class GCategory {

    //disable_default_category=1
    public boolean isExistDisableDefaultCategory(){
        return !Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectList("select id from g_category where disable_default_category=1")).isEmpty();
    }

    public void updateDisableDefaultCategoryValue(int gCategoryId, int disableDefaultCategory){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_category set disable_default_category = " + disableDefaultCategory + " where id = " + gCategoryId);
    }

    public List<String> getCategoriesAllowedLoadCertificate(String campaignType){
         return PartnersInstructions.getConnectionPartnersSelectList("select id from g_category where options like '%\"allowAddCertificate\": true%' and visible = 1 and campaign_types like '%" + campaignType +"%' and id not in(select DISTINCT parent_id from g_category)");
    }

    public List<String> getCategoriesDisallowLoadCertificate(String campaignType){
        return PartnersInstructions.getConnectionPartnersSelectList("select id from g_category where options not like '%\"allowAddCertificate\": true%' and visible = 1 and campaign_types like '%" + campaignType +"%'  and id not in(select DISTINCT parent_id from g_category)");
    }

    public void updateAllowAnimationFieldForCategory(int category, int state){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_category set allow_animation = " + state + " where id = " + category);
    }
}
