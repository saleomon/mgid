package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class GHitsTags {
    public void deleteAllTags(int... teaserId){
        String values = Arrays.stream(teaserId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from g_hits_tags where g_hits_id IN(" + values + ")");
    }
}
