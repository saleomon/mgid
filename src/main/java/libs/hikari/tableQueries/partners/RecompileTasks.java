package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class RecompileTasks {

    public String getRecompileTasksStatus(){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select status from recompile_tasks where id = 1");
    }

    public void insertTask(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into recompile_tasks(id, `sql`, status, droped) " +
                "VALUES (2, 'SELECT * FROM tickers_composite WHERE id in(1,117,118,123,124,125,126,127,128,129,130,131,133,134)', 'wait', 0);");
    }

    public void insertTask1(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into recompile_tasks(id, `sql`, status, droped) " +
                "VALUES (3, 'SELECT * FROM tickers_composite WHERE id in(135,136,261,262,263,264,265,266,267,293,294,295,296,299)', 'wait', 0);");
    }

    public void insertTask2(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into recompile_tasks(id, `sql`, status, droped) " +
                "VALUES (4, 'SELECT * FROM tickers_composite WHERE id in(304,305,309,310,313,314,315,316,317,318,319,320,322,323)', 'wait', 0);");
    }

    public void insertTask3(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into recompile_tasks(id, `sql`, status, droped) " +
                "VALUES (5, 'SELECT * FROM tickers_composite WHERE id in(324,326,327,328,329,330,331,332,333,334,335,336,337,338)', 'wait', 0);");
    }

    public void insertTask4(){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into recompile_tasks(id, `sql`, status, droped) " +
                "VALUES (6, 'SELECT * FROM tickers_composite WHERE id in(339,340,341,342,343,344,345,346,347,423,430,2004)', 'wait', 0);");
    }
}
