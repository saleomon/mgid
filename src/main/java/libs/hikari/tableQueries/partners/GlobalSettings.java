package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.List;

public class GlobalSettings {
    public List<String> getContextTargetingLanguageIds() {
        return Arrays.asList(PartnersInstructions.getConnectionPartnersSelectColumn(
                "select value from global_settings where `key` = 'context_targeting_lang_ids'").replaceAll("\\s+","")
                .split(",", -1));
    }

    public String getLibVersion(){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select JSON_EXTRACT(value, '$.\"1.11\"') from global_settings where `key` = 'widget_lib_map'");
    }
}
