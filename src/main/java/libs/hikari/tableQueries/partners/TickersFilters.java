package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TickersFilters {

    public void deleteAllFilters(int... tickers_uid){
        String values = Arrays.stream(tickers_uid).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from tickers_filters where tickers_uid IN(" + values + ")");
    }

    public void insertFilters(int tickers_uid, int... categoriesId){
        String val = Arrays.stream(categoriesId).mapToObj(i -> "(" + tickers_uid + ", " + i + ")").collect(Collectors.joining(","));
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into tickers_filters(tickers_uid, src_id) VALUES " + val + ";");
    }
}
