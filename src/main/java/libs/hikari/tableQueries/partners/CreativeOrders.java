package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class CreativeOrders {

    public void setCreativeRequestOnModerationStatusAndDeleteAllTasks(int creativeRequestId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from creative_tasks where order_id = " + creativeRequestId);
        PartnersInstructions.getConnectionPartnersExecuteQuery("update creative_orders " +
                "set status = 0, ad_types = NULL where id = " + creativeRequestId);
    }

    public String selectOfferName(String creativeRequestId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select offer_name from creative_orders where id = " + creativeRequestId);
    }
}
