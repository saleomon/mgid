package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;
import org.apache.commons.collections.map.HashedMap;

import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.sleep;
import static libs.hikari.utils.PartnersInstructions.getConnectionPartnersSelectList;

public class MailPull {

    private final Map<String, Integer> countersLetters = new HashedMap();

    public boolean checkCustomMail(String subject, String...containsBody){
        int countNewLetters = containsBody.length;

        if(waitNewLetters(subject, countNewLetters)) {

            List<String> letters =  getConnectionPartnersSelectList("select body from mail_pull where subject = '" + subject + "' ORDER BY ID DESC LIMIT " + countNewLetters);

            letters = Objects.requireNonNull(letters)
                    .stream()
                    .map(i -> i.contains("Teasers were copied") ? i.substring(0, i.lastIndexOf("/")+1) : i)
                    .collect(Collectors.toList());
            //System.out.println(letters.size());
            letters.forEach(el -> System.out.println("From db - " + el));
            Arrays.asList(containsBody).forEach(System.out::println);

            return new HashSet<>(letters).containsAll(Arrays.asList(containsBody));
        }
        return false;
    }

    public boolean checkCustomMailBody(String containsBody){
        if(waitNewLettersBody(1, containsBody)) {

            List<String> letters =  getConnectionPartnersSelectList("select body from mail_pull where body like '%" + containsBody.replace("'", "_") + "%' ORDER BY ID DESC LIMIT 1");

            letters = Objects.requireNonNull(letters)
                    .stream()
                    .map(i -> i.contains("Teasers were copied") ? i.substring(0, i.lastIndexOf("/")+1) : i)
                    .collect(Collectors.toList());
            letters.forEach(el -> System.out.println("From db - " + el));
            List.of(containsBody).forEach(System.out::println);

            return new HashSet<>(letters).contains(containsBody);
        }
        return false;
    }

    /**
     * get count letters for custom subject
     */
    public void getCountLettersBySubject(String...subject) {
        countersLetters.clear();
        Arrays.asList(subject).forEach(el-> countersLetters.put(el, Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from mail_pull where subject = '" + el + "'")))));
    }

    /**
     * get count letters for custom subject
     */
    public Map<String, Integer> getCountLettersByBody(String...body) {
        countersLetters.clear();
        Arrays.asList(body).forEach(el-> countersLetters.put(el, Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from mail_pull where body like '%" + el.replace("'", "_") + "%'")))));
        return countersLetters;
    }

    /**
     * get count letters for custom subject
     */
    public void getCountLettersByClientEmail(String...clientEmail){
        Arrays.asList(clientEmail).forEach(el-> countersLetters.put(el, Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from mail_pull where `to` = '" + el + "'")))));
    }

    /**
     * wait before mail_pull should be count letters equals (getCountLetters + countNewLetters)
     */
    private boolean waitNewLetters(String subject, int countNewLetters){
        int count = 0;
        while ((countNewLetters + countersLetters.get(subject)) != Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from mail_pull where subject = '" + subject + "'")))
        && count < 10){
            sleep(1000);
            count++;
        }
        return count < 10;
    }

    /**
     * wait before mail_pull should be count letters equals (getCountLetters + countNewLetters)
     */
    private boolean waitNewLettersBody(int countNewLetters, String body){
        int count = 0;
        while ((countNewLetters + countersLetters.get(body)) != Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from mail_pull where body like '%" + body.replace("'", "_") + "%'")))
                && count < 10){
            sleep(1000);
            count++;
        }
        return count < 10;
    }

    /**
     * wait before mail_pull should be count letters equals (getCountLetters + countNewLetters)
     */
    private boolean waitNewLettersByClientEmail(String clientEmail, int countNewLetters){
        int count = 0;
        while ((countNewLetters + countersLetters.get(clientEmail)) != Integer.parseInt(
                Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn(
                        "select count(id) from mail_pull where `to` = '" + clientEmail + "'"))) && count < 10){
            sleep(1000);
            count++;
        }
        return count < 5;
    }

    /**
     * Get letter body from mail
     */
    public String getBodyFromMail(String subject) {

        if (waitNewLetters(subject, 1)) {
            return PartnersInstructions.getConnectionPartnersSelectColumn("select body from mail_pull where subject = '" + subject + "' ORDER BY ID DESC LIMIT 1");
        }
        return "There is no needed letters!";
    }

    /**
     * Get letter body from mail
     */
    public String getMailByBody(String body) {

        if (waitNewLettersBody(1, body)) {
            return PartnersInstructions.getConnectionPartnersSelectColumn("select body from mail_pull where body like '%" + body.replace("'", "_") + "%' ORDER BY ID DESC LIMIT 1");
        }
        return "There is no needed letters!";
    }

    /**
     * Get letter body from mail
     */
    public List<String> getListOfBodyFromMail(String subject) {
         return getConnectionPartnersSelectList("select body from mail_pull where subject = '" + subject + "' ORDER BY ID DESC LIMIT 1");
    }


    /**
     * Get letter body from mail
     */
    public String getBodyFromMailByClientEmail(String clientEmail) {

        if (waitNewLettersByClientEmail(clientEmail, 1)) {

            return PartnersInstructions.getConnectionPartnersSelectColumn("select body from mail_pull where `to` = '" + clientEmail + "' ORDER BY ID DESC LIMIT 1");
        }
        return "There is no needed letters!";
    }
}
