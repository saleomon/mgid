package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TickersCompositeAttributes {

    public String getAntifraudParameters(int tickers_composite_id, String keyValue){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select value from tickers_composite_attributes " +
                "where tickers_composite_id = " + tickers_composite_id +
                " and name = '" + keyValue + "';");
    }

    public void setAntifraudParameters(int tickers_composite_id, String keyName, String keyValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into tickers_composite_attributes " +
                "(tickers_composite_id, name, value) values " +
                "(" + tickers_composite_id + ", " + keyName + ", " + keyValue + ");");
    }

    public List<String> getListOfAttributesValues(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectAllColumns("select name, value from tickers_composite_attributes where tickers_composite_id = " + tickersCompositeId);
    }
}
