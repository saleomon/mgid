package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class AclRolesRightsDashboard {

    public void insertAclRolesRightDashboardCustom(Integer roleId, String... privileges){
        Arrays.stream(privileges)
                .forEach(i -> PartnersInstructions.getConnectionPartnersExecuteQuery(
                        "insert into acl_roles_rights_dashboard (role_id, acl_roles_rights_dashboard.right) " +
                                "select * from ( select " + roleId + ", 'default/" + i + "') AS tmp " +
                                "where not exists (" +
                                "select acl_roles_rights_dashboard.right from acl_roles_rights_dashboard where role_id = " + roleId + " " +
                                "and acl_roles_rights_dashboard.right = 'default/" + i + "') limit 1;"));
    }

    public void deleteAclRolesRightDashboardCustom(Integer roleId, String... privilege){
        String values = Arrays.stream(privilege).map(i -> "'default/" + i + "'").collect(Collectors.joining(", "));

        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from acl_roles_rights_dashboard " +
                "where role_id = " + roleId + " " +
                "and acl_roles_rights_dashboard.right IN(" + values + ");");
    }
}
