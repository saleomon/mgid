package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ClientsSitesPostModeration {

    public void deletePostModerationBySite(int... siteId){
        String values = Arrays.stream(siteId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from clients_sites_post_moderation where clients_sites_id IN(" + values + ")");
    }
}
