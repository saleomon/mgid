package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class ClientsAutoBilling {
    public int getAutoPaymentFlag(String clientId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select auto_pay from clients_auto_billing  where client_id = " + clientId)));
    }
}
