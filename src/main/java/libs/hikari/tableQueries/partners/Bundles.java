package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class Bundles {

    public List selectBundleName(String tier_id, String category_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select TRIM(b.name) FROM tier_category_bundles t " +
                "join bundles b on t.bundle_id=b.bundles_id " +
                "where t.tier_id = " + tier_id +
                " and " +
                "t.category_platform_id = " + category_id);
    }
}
