package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;
import pages.cab.products.logic.CabTeasers;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GHits1 {
    public static int selectTeaserByTitle(String title) {
        return Integer.parseInt(String.valueOf(PartnersInstructions.getConnectionPartnersSelectColumn("select id from g_hits_1 where title like '" +         title.replace("'", "\\'")
        + "'")));
    }

    public void updateTeaserTitle(int teaserId, String title){
       PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set title = '" + title + "' where id = '" + teaserId + "'");
    }

    public void updateDraftStatus(int teaserId, int value){
       PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set draft = " + value + " where id = " + teaserId + "");
    }

    public void updateTeaserTitleAndApprove(int teaserId, String title){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set title = '" + title + "', karantin = 0 where id = '" + teaserId + "'");
    }

    public void updateOption(int teaserId, CabTeasers.CompliantType...text){
        String parameters = Arrays.stream(text)
                .map(elem -> "" + elem.getCompliantType().concat(""))
                .collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set options = '{\"compliant\": [ " + parameters + " ]}' where id = '" + teaserId + "'");
    }

    public void updateBlockedOption(int teaserId, int value){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set blocked = " + value + " where id = '" + teaserId + "'");
    }

    public void updateTeaserDescription(int teaserId, String description){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set advert_text = '" + description + "' where id = '" + teaserId + "'");
    }

    public void updateTeaserCallToAction(int teaserId, String callToAction){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set call_to_action = '" + callToAction + "' where id = '" + teaserId + "'");
    }

    public void updateTeaserModerationStatus(int teaserId, int karantineVal){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set karantin = " + karantineVal + " where id = '" + teaserId + "'");
    }

    public void updateTeaserSendApprove(int... teasersId){
        String values = Arrays.stream(teasersId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_hits_1 set karantin = 0 where id IN(" + values + ")");
    }

    /**
     * delete all teasers by custom campaign but one teaser(save this teaser)
     */
    public void deleteTeasersByCampaignsButOne(int teaserSaveId, int... campaignsId){
        String values = Arrays.stream(campaignsId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from g_hits_1 where id not in(" + teaserSaveId + ") and id_partner IN(" + values + ")");
    }

    /**
     * get amount of teasers with selected type
     */
    public String selectAmountOfTeasersWithCampaignType(String campaignType){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from g_hits_1 where id_partner in (select id from g_partners_1 where campaign_types = '" + campaignType + "')");
    }

    public String getOptionsValue(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select options from g_hits_1 where id = " + teaserId);
    }

    public int getBlockedValue(int teaserId){
        return Integer.parseInt((Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select blocked from g_hits_1 where id = " + teaserId))));
    }

    public int getAnimationValue(int teaserId){
        return Integer.parseInt((Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select animation from g_hits_1 where id = " + teaserId))));
    }

    public String getCategoryIdValue(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select id_category from g_hits_1 where id = " + teaserId);
    }

    public String getVerifiedModeration(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select verified_moderation from g_hits_1 where id = " + teaserId);
    }
    public int getQuarantineValue(int teaserId){
        return Integer.parseInt((Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select karantin from g_hits_1 where id = " + teaserId))));
    }

    public List<String> getClientVideoTeasersIds(boolean isQuarantine, int clientId){
        String additionalParameter = isQuarantine ? " and karantin = 1" : "";
        return PartnersInstructions.getConnectionPartnersSelectList("select id from g_hits_1 where video = 1 and id_partner in (select id from g_partners_1 where client_id = " + clientId + ")" + additionalParameter);
    }

    public int getVideoValue(int teaserId){
        return Integer.parseInt((Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select video from g_hits_1 where id = " + teaserId))));
    }

    public String getSellCoords(int teaserId){
        return Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select sel_coords from g_hits_1 where id = " + teaserId));
    }

    public String getVideoLinksValue(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select video_images_links from g_hits_1 where id = " + teaserId);
    }

    public String getGettyImagesFlag(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select gettyimages_id from g_hits_1 where id = " + teaserId);
    }
}
