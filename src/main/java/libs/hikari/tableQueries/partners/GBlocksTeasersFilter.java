package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class GBlocksTeasersFilter {

    public void clearAllTeasersInFilter(int g_blocks_uid){
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from g_blocks_teasers_filter where g_blocks_uid = " + g_blocks_uid);
    }

    public void insertTeasersInAdsFilter(int g_blocksId, int... teaserIds){
        String val = Arrays.stream(teaserIds).mapToObj(i -> "(" + g_blocksId + ", " + i + ")").collect(Collectors.joining(","));

        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into g_blocks_teasers_filter (g_blocks_uid, g_hits_1_id) VALUES " + val + ";");
    }
}
