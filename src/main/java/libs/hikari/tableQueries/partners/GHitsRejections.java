package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class GHitsRejections {
    public List<String> getRejectionReasons(int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectList("select reason from g_hits_rejections where g_hits_1_id = " + teaserId);
    }
}
