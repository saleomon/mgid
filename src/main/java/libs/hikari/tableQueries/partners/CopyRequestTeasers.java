package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class CopyRequestTeasers {
    public int selectTeaserIdAfterCopy(int campaignId, int teaserId){
        List idsList = PartnersInstructions.getConnectionPartnersSelectList("select teaser.result_teaser_id FROM copy_request_teasers teaser JOIN copy_requests request ON request.id = teaser.copy_request_id and request.destination_campaign_id = " + campaignId + " and teaser.teaser_id = " + teaserId);
        return idsList.get(idsList.size()-1) == null ? null : Integer.parseInt(String.valueOf(idsList.get(idsList.size() - 1)));
    }

    public boolean isPresentRecordTeaserCopyRequest(int teaserId){
        return null != PartnersInstructions.getConnectionPartnersSelectColumn("select teaser_id FROM copy_request_teasers where teaser_id = " + teaserId);
    }

    public boolean checkPresenceRecordingForTeaser(int campaignId, int teaserId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select teaser.result_teaser_id FROM copy_request_teasers teaser JOIN copy_requests request ON request.id = teaser.copy_request_id and request.destination_campaign_id = " + campaignId + " and teaser.teaser_id = " + teaserId) != null;
    }
}
