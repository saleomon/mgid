package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class Tickers {
    public String getUseCat(int tickersId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_cat from tickers where id = " + tickersId);
    }

    public String getUpdatedTime(int tickersId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select updated_time from tickers where id = " + tickersId);
    }

    public int getIsOriginalTitle(int tickersId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select is_original_title from tickers where id = " + tickersId)));
    }

    public void updateUseCat(int tickersId, int filterValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers set use_cat = " + filterValue + " where id = " + tickersId);
    }

    public void updateFilter2(int tickersId, int filterValue){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers set filter_2 = " + filterValue + " where id = " + tickersId);
    }

    public int getTickersId(int tickers_composite_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select id from tickers where tickers_composite_id = " + tickers_composite_id)));
    }
}
