package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.time.LocalDateTime;

public class CurrentStatus {

    public int getValue(String key){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select value from current_status where current_status.key = '" + key + "'"));
    }

    public String getCurrentTimestamp(){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select current_timestamp");
    }

    public void updateValue(int day){
        PartnersInstructions.getConnectionPartnersExecuteQuery(
                "UPDATE partners.current_status " +
                        " set value = " + day +
                        " where current_status.key IN('g_blocks_rotate_start_date','g_blocks_rotate_date','p_news_rotate_start_date','p_news_rotate_date','g_blocks_end_update_date');"
        );
    }

    public void updateValue(LocalDateTime date){
        PartnersInstructions.getConnectionPartnersExecuteQuery(
        "UPDATE partners.current_status " +
        " set value = " + date.getDayOfMonth() + ", " +
                " when_change = " +
                " case" +
                " when current_status.key IN('g_blocks_rotate_start_date', 'g_blocks_rotate_date'," +
                                      "'p_news_rotate_start_date', 'p_news_rotate_date') then '" + date + "'" +
                " when current_status.key = 'g_blocks_end_update_date' then '" + date.plusHours(1) + "'" +
                " end" +
                " where current_status.key IN('g_blocks_rotate_start_date','g_blocks_rotate_date','p_news_rotate_start_date','p_news_rotate_date','g_blocks_end_update_date');"
        );
    }
}
