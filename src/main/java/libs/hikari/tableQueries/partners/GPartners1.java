package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;
import testData.project.TargetingType.Targeting;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class GPartners1 {

    public void updateTierOption(int campaignId, String tierOption){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_partners_1 " +
                "set tier = '" + tierOption+ "' " +
                "where id = '" + campaignId + "'");
    }

    public void updateOptionCompliant(int campaignId, String...text){
        String options = PartnersInstructions.getConnectionPartnersSelectColumn("select options from g_partners_1 where id = " + campaignId);
        String parameters = Arrays.stream(text).collect(Collectors.joining("\", \"","\"","\""));
        options = Objects.requireNonNull(options).substring(0, options.indexOf("\"compliant\":[") +13);
        options += parameters + "]}'";
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_partners_1 set " +
                "options = '" + options +
                " where id = '" + campaignId + "'");
    }

    /**
     * @param campaignId - id
     * @param option - location, os, language, browser, provider, platform, socdem, interest
     * @param value - include/exclude
     */
    public void updateGeo(int campaignId, Targeting option, String value){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update g_partners_1 " +
                "set geo = 1, " +
                "options = '" + setTargetingMode(campaignId, option, value) + "' " +
                "where id = '" + campaignId + "'");
    }

    public static String setTargetingMode(int campaignId, Targeting option, String value){
        String options = PartnersInstructions.getConnectionPartnersSelectColumn("select options from g_partners_1 where id = " + campaignId);
        options = Objects.requireNonNull(options).replace("\"" + option.getTypeValue() + "_targeting_mode\":\"" + (value.equals("include") ? "exclude" : "include") + "\"", "\"" + option.getTypeValue() + "_targeting_mode\":\"" + value + "\"");
        return options;
    }

    /**
     * get amount of campaigns with selected type
     */
    public String selectAmountOfCampaignsWithType(String campaignType){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from g_partners_1 where " +
                "campaign_types = '" + campaignType +"' and auto_delete = 0");
    }

    public String getShowProbabilityCh(String campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select show_probability_ch from g_partners_1 where " +
                "id = '" + campaignId +"'");
    }

    /**
     * get total amount of campaigns
     */
    public int selectLastCampaignId(){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn(
                "select max(id) from g_partners_1")));
    }

    /**
     * get total amount of campaigns
     */
    public int selectShowsOnlyForThisLanguageValue(String campaignId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn(
                "select use_only_same_language_widgets from g_partners_1 where id = " + campaignId)));
    }

    public String selectCampaignType(int campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn(
                "select campaign_types from g_partners_1 where id = " + campaignId);
    }

    public String selectCampaignCategory(int campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn(
                "select g_category_id from g_partners_1 where id = " + campaignId);
    }

    public String selectCampaignAdvertiserName(String campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn(
                "select show_name from g_partners_1 where id = " + campaignId);
    }

    public String selectCampaignShowNameTemp(String campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn(
                "select show_name_temp from g_partners_1 where id = " + campaignId);
    }

    public String selectCampaignUpdatedTime(int campaignId){
        return PartnersInstructions.getConnectionPartnersSelectColumn(
                "select updated_time from g_partners_1 where id = " + campaignId);
    }
}
