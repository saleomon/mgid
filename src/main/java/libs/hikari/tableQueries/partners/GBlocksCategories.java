package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GBlocksCategories {

    public void deleteAllCategories(int... g_blocks_uid){
        String values = Arrays.stream(g_blocks_uid).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from g_blocks_categories where g_blocks_uid IN(" + values + ")");
    }

    public void insertCategories(int g_blocks_uid){
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into g_blocks_categories(g_blocks_uid, g_category_id) VALUES (" + g_blocks_uid + ", 100);");
    }

    public void insertCategories(int g_blocks_uid, int... categoriesId){
        String val = Arrays.stream(categoriesId).mapToObj(i -> "(" + g_blocks_uid + ", " + i + ")").collect(Collectors.joining(","));
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into g_blocks_categories(g_blocks_uid, g_category_id) VALUES " + val + ";");
    }

    public boolean isExistCategoryInWidget(int g_blocks_uid){
        return PartnersInstructions.getConnectionPartnersSelectList("select g_category_id from g_blocks_categories where g_blocks_uid = " + g_blocks_uid).size() > 0;
    }

    public int getCategoriesCountForWidget(int g_blocks_uid){
        return PartnersInstructions.getConnectionPartnersSelectList("select g_category_id from g_blocks_categories where g_blocks_uid = " + g_blocks_uid).size();
    }

    public String getAdOrLandingTypeForWidgetByCategoryID(int g_blocks_uid, int g_category_id, String type){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select " + type + " from g_blocks_categories where g_blocks_uid = " + g_blocks_uid + " and g_category_id = " + g_category_id);
    }

    public List getAdAndLandingTypeForWidgetByCategoryID(int g_blocks_uid, int g_category_id){
        return PartnersInstructions.getConnectionPartnersSelectFieldsList("select landing_types, ad_types from g_blocks_categories where g_blocks_uid = " + g_blocks_uid + " and g_category_id = " + g_category_id);
    }

    public List getAdTypesForWidget(int g_blocks_uid){
        return PartnersInstructions.getConnectionPartnersSelectList("select ad_types from g_blocks_categories where g_blocks_uid = " + g_blocks_uid);
    }

    public List getLandingTypesForWidget(int g_blocks_uid){
        return PartnersInstructions.getConnectionPartnersSelectList("select landing_types from g_blocks_categories where g_blocks_uid = " + g_blocks_uid);
    }

    public String isSelectedCategoryInWidget(int g_blocks_uid, int g_category_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select g_category_id from g_blocks_categories where g_blocks_uid = " + g_blocks_uid + " and g_category_id = " + g_category_id);
    }

}
