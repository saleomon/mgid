package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class TickersComposite {

    public String getOptions(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select options from tickers_composite where id = " + tickersCompositeId);
    }

    public void updateOptions(int tickersCompositeId, String options){
        String old_options = getOptions(tickersCompositeId);
        String new_options = options.equals("")
                ? ""
                : options;
        if(!old_options.equals("") & !new_options.equals("")){
            new_options = old_options.substring(0, old_options.length()-1) + "," + new_options + "}";
        }
        if(old_options.equals("") & !new_options.equals("")){
            new_options = "{" + options + "}";
        }
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set options = '" + new_options + "' where id = " + tickersCompositeId);
    }

    public void updateAntifraudOptions(int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set antifraud_options = '{\"ssp_sanctions_enabled\": true}' where id = " + tickersCompositeId);
    }

    public void updatePaginator(int paginator, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set paginator = " + paginator + " where id = " + tickersCompositeId);
    }

    public void updateShadowDomEnabled(int shadow_dom_enabled, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set shadow_dom_enabled = " + shadow_dom_enabled + " where id = " + tickersCompositeId);
    }

    public void updateMirror(String mirror, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set mirror = (select name from subnet_mirrors where name = '" + mirror + "') where id = '" + tickersCompositeId + "'");
    }

    public void updateDefaultJs(String use_default_js, String default_js, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set use_default_js = " + use_default_js +
                ", default_js = '" + default_js +
                "' where id = " + tickersCompositeId
        );
    }

    public void updateDoubleClickSettings(int tickersCompositeId, boolean doubleClickDesktop, int doubleClickDesktopDelay, boolean doubleClickMobile, int doubleClickMobileDelay){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set random_clicks_disabled = " + (doubleClickMobile ? 1 : 0) + ", " +
                "random_clicks_disabled_desktop = " + (doubleClickDesktop ? 1 : 0) + ", " +
                "desktop_doubleclick_delay = " + doubleClickDesktopDelay + ", " +
                "mobile_doubleclick_delay = " + doubleClickMobileDelay +
                " where id = '" + tickersCompositeId + "'");
    }

    public void updateIframe(int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set iframe = '800*600'" +
                " where id = " + tickersCompositeId
        );
    }

    public void updateCategoryPlatform(int category_platform, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set category_platform = " + category_platform +
                " where id = " + tickersCompositeId);
    }

    public void updateEnabled(int enabled, int tickersCompositeId){
        int status = enabled == 0 ? 17 : 12; // 17 : 0 -> block ; 12 : 1 -> unblock
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set enabled = " + enabled +
                ", status = " + status +
                " where id = " + tickersCompositeId);
    }

    public void updateDeletedWidget(int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set enabled = 1, "  +
                "status = 12, " +
                "droped = 1" +
                " where id = " + tickersCompositeId);
    }

    /**
     * 0 - всё отключено
     * 1 - всё включено (логотип и надпись)
     * 2 - включена только надпись
     * 3 - включён только логотип
     */
    public void updateAdLink(int adLink, int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set ad_link = " + adLink + ", " +
                "where id = " + tickersCompositeId);
    }

    public void clearDoubleClickSettings(int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set random_clicks_disabled = 0, " +
                "random_clicks_disabled_desktop = 0, " +
                "desktop_doubleclick_delay = 0, " +
                "mobile_doubleclick_delay = 0 " +
                "where id = '" + tickersCompositeId + "'");
    }

    public void clearStopWordsSettings(int tickersCompositeId){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set word_filter = 0, " +
                "stop_words = '', " +
                "word_number = NULL " +
                "where id = '" + tickersCompositeId + "'");
    }

    public int getDescriptionRequired(int g_blocks_id){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select description_required from tickers_composite t join g_blocks g" +
                " on t.id=g.tickers_composite_id where g.id = " + g_blocks_id)));
    }

    public String getUpdatedTime(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select updated_time from tickers_composite where id = " + tickersCompositeId);
    }

    public String getAllowMultipleWidgets(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select allow_multiple_widgets from tickers_composite where id = " + tickersCompositeId);
    }

    public String getShadowDomEnabled(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select shadow_dom_enabled from tickers_composite where id = " + tickersCompositeId);
    }

    public String getAutorefresh(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select autorefresh from tickers_composite where id = " + tickersCompositeId);
    }

    public String getPlaceReservation(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select use_place_reservation from tickers_composite where id = " + tickersCompositeId);
    }

    public String getVideoSsp(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select video_ssp from tickers_composite where id = " + tickersCompositeId);
    }

    public int getClickableDelay(int tickersCompositeId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select clickable_delay from tickers_composite where id = " + tickersCompositeId)));
    }

    public int getSourcesTypesId(int tickersCompositeId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select sources_types_id from tickers_composite where id = " + tickersCompositeId)));
    }

    public static int getCountNews(int tickersCompositeId){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count_news from tickers_composite where id = " + tickersCompositeId)));
    }

    public String getActivateDelay(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select activate_delay from tickers_composite where id = " + tickersCompositeId);
    }

    public String getRefreshAdsTime(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select refresh_ads_time from tickers_composite where id = " + tickersCompositeId);
    }

    public String getBackButtonOptions(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select back_button_options from tickers_composite where id = " + tickersCompositeId);
    }

    public void updateBackButtonOptions(int tickersCompositeId, String backButtonOptions){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update tickers_composite " +
                "set back_button_options = '" + backButtonOptions + "' where id = " + tickersCompositeId);
    }

    public String getStatus(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select status from tickers_composite where id = " + tickersCompositeId);
    }

    public String getDroped(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select droped from tickers_composite where id = " + tickersCompositeId);
    }

    public String getEnabled(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select enabled from tickers_composite where id = " + tickersCompositeId);
    }

    public String getRefreshAdsBy(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select refresh_ads_by from tickers_composite where id = " + tickersCompositeId);
    }

    public String  getNativeRedirector(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select native_redirector from tickers_composite where id = " + tickersCompositeId);
    }

    public String getAdblockTemplate(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select adblock_template from tickers_composite where id = " + tickersCompositeId);
    }

    public String getCtaRequired(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select cta_required from tickers_composite where id = " + tickersCompositeId);
    }

    public String getAdblockIntegration(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select adblock_integration from tickers_composite where id = " + tickersCompositeId);
    }

    public String getTemplate(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select template from tickers_composite where id = " + tickersCompositeId);
    }

    public String getStyles(int tickersCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select styles from tickers_composite where id = " + tickersCompositeId);
    }

    public String getTickersCompositeXmlWidgetToken(int tickersCompositeId)
        {
            return PartnersInstructions.getConnectionPartnersSelectColumn("select token from tickers_composite where id = " + tickersCompositeId);
        }
}
