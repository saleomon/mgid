package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class ClientsSitesStatuses {

    public String getSiteStatus(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select status_id from clients_sites_statuses where id = " + site_id);
    }

    public String getSiteMirror(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select mirror from clients_sites_statuses where id = " + site_id);
    }

    public String getRefuseReason(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select refuse_reason from clients_sites_statuses where id = " + site_id);
    }

    public String getDroped(int site_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select droped from clients_sites_statuses where id = " + site_id);
    }

    public void updateSiteStatus(int site_id, int status){
        PartnersInstructions.getConnectionPartnersExecuteQuery("update clients_sites_statuses " +
                "set status_id = " + status +
                ", droped = 0" +
                ", refuse_reason = ''" +
                " where id = " + site_id);
    }
}
