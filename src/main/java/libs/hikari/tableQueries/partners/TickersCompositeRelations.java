package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class TickersCompositeRelations {

    public int getChildTickersCompositeId(int parent_tickers_composite_id){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select child_tickers_composite_id from tickers_composite_relations where rules like '%adblock%' and parent_tickers_composite_id = " + parent_tickers_composite_id));
    }

    public int getChildDontDropedTickersCompositeId(int parent_tickers_composite_id){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select child_tickers_composite_id from tickers_composite_relations where droped = 0 and parent_tickers_composite_id = " + parent_tickers_composite_id));
    }

    public int getDropedByParent(int parent_tickers_composite_id){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select droped from tickers_composite_relations where parent_tickers_composite_id = " + parent_tickers_composite_id));
    }

    public int getDropedByChild(int child_tickers_composite_id){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select droped from tickers_composite_relations where child_tickers_composite_id = " + child_tickers_composite_id));
    }
}
