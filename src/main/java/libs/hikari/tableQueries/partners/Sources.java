package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class Sources {

    public String getSourcesName(String sources_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select name from sources where sources_id = " + sources_id);
    }

    public String getSourcesType(String sources_id){
        return PartnersInstructions.getConnectionPartnersSelectColumn("select type from sources where sources_id = " + sources_id);
    }
}
