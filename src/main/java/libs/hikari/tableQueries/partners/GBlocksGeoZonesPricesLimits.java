package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.Map;

public class GBlocksGeoZonesPricesLimits {

    public boolean checkMinimalCpc(int g_blocks_uid, Map<String, String> map){
        return map.entrySet().stream().allMatch( entry -> {
            String listRegions = PartnersInstructions.getConnectionPartnersSelectList("select geo_zones_ids from geo_zones_groups where geo_zones_groups_id = " + entry.getKey()).get(0).toString();
            List list =
                    PartnersInstructions.getConnectionPartnersSelectList("select price from g_blocks_geo_zones_prices_limits where g_blocks_uid = " + g_blocks_uid + " and geo_zones_id in(" + listRegions + ")");
            return list.stream().allMatch(i -> i.toString().contains(entry.getValue().replace(".", "")));
        });
    }
}
