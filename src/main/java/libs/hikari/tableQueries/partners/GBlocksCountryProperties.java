package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.Map;

public class GBlocksCountryProperties {

    public boolean checkCountryCpm(int g_blocks_uid, Map<String, String> map){
        return map.entrySet().stream().allMatch( entry -> {
            List list =
                    PartnersInstructions.getConnectionPartnersSelectList("select rtb_min_cpm from g_blocks_country_properties where g_blocks_uid = " + g_blocks_uid + " and country_id in(" + entry.getKey() + ")");
            return list.stream().allMatch(i -> i.toString().contains(entry.getValue()));
        });
    }
}
