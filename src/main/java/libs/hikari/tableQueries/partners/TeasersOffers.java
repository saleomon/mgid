package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class TeasersOffers {
    public List selectOfferName(int teaserId) {
        return PartnersInstructions.getConnectionPartnersSelectList("select offer_name from teasers_offers where g_hits_1_id =  " + teaserId);
    }

    public  List selectAddedType(int teaserId) {
        return PartnersInstructions.getConnectionPartnersSelectList("select added_type from teasers_offers where g_hits_1_id =  " + teaserId);
    }

}
