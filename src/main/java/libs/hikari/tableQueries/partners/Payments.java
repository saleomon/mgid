package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

public class Payments {

    public int getCountPayments(String clientId){
        return Integer.parseInt(PartnersInstructions.getConnectionPartnersSelectColumn("select count(id) from payments where client_id =" + clientId + " and status = 2"));
    }

    public void pushDumpToTable(String path) {
        PartnersInstructions.pushDumpToTable(path);
    }
}
