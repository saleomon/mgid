package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Objects;

public class OfferCertificates {
    public int getAmountOfProductsWithCertificates(){
        return Integer.parseInt(Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectColumn("select count(offer_name) from offer_certificates")));
    }
}
