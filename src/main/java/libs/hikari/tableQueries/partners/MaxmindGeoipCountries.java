package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class MaxmindGeoipCountries {

    public List<String> getCountriesId(List<String> regionName){
        String joinedString = regionName
                .stream()
                .collect(Collectors.joining("', '","'","'"));
        return PartnersInstructions.getConnectionPartnersSelectList("select id from maxmind_geoip_countries where name IN(" + joinedString + ")");

    }

    public List<String> getExcludeCountriesId(List<String> regionName){
        String joinedString = regionName
                .stream()
                .collect(Collectors.joining("', '","'","'"));
        return PartnersInstructions.getConnectionPartnersSelectList("select id from maxmind_geoip_countries where name not IN(" + joinedString + ")");
    }

    public List<String> getDisallowedCountriesForCertificatUploading(){
        return PartnersInstructions.getConnectionPartnersSelectList("select name from maxmind_geoip_countries where options not like '%\"allowAddCertificate\": true%'");
    }

    public List<String> getAllowedCountriesForCertificatUploading(){
        return PartnersInstructions.getConnectionPartnersSelectList("select name from maxmind_geoip_countries where options like '%\"allowAddCertificate\": true%'");
    }

    public List<String> getListCountries(){
        String maxmindCountriesIds = Objects.requireNonNull(PartnersInstructions.getConnectionPartnersSelectList("select maxmind_geoip_countries_id from geo_team_countries_config")).stream()
                .collect(Collectors.joining("', '","'","'"));
        return PartnersInstructions.getConnectionPartnersSelectList("select name from maxmind_geoip_countries where id in (" +maxmindCountriesIds +")");
    }

}
