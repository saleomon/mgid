package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BundlesGBlocks {

    public List getBundlesId(int tickerCompositeId){
        return PartnersInstructions.getConnectionPartnersSelectList(
                "select b.bundles_id from bundles_g_blocks b join g_blocks " +
                        "gb on b.g_blocks_uid = gb.uid " +
                        "where gb.tickers_composite_id = " + tickerCompositeId);
    }

    public boolean selectIsExistBundlesId(int g_blocks_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select bundles_id from bundles_g_blocks where g_blocks_uid = " + g_blocks_id).size() > 0;
    }
    
    public void insertBundles(int g_blocks_id, int... bundles){
        String val = Arrays.stream(bundles).mapToObj(i -> "(" + i + ", " + g_blocks_id + ")").collect(Collectors.joining(","));
        PartnersInstructions.getConnectionPartnersExecuteQuery("insert into bundles_g_blocks(bundles_id, g_blocks_uid) VALUES " + val + ";");
    }
}
