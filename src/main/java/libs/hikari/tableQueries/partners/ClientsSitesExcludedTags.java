package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ClientsSitesExcludedTags {

    public void deleteAllTags(int... teaserId){
        String values = Arrays.stream(teaserId).mapToObj(String::valueOf).collect(Collectors.joining(", "));
        PartnersInstructions.getConnectionPartnersExecuteQuery("delete from clients_sites_excluded_tags where clients_sites_id IN(" + values + ")");
    }

    public boolean isTickersCompositeIdHasDfpFlag(int clients_sites_id, String tickers_composite_id){
        String val = PartnersInstructions.getConnectionPartnersSelectColumn("select tickers_composite_id from clients_sites_excluded_tags where clients_sites_id = " + clients_sites_id);
        return val.contains(tickers_composite_id);
    }
}
