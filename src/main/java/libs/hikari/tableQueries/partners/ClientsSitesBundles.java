package libs.hikari.tableQueries.partners;

import libs.hikari.utils.PartnersInstructions;

import java.util.List;

public class ClientsSitesBundles {

    public List getBundlesId(int client_site_id){
        return PartnersInstructions.getConnectionPartnersSelectList("select bundle_id from clients_sites_bundles where client_site_id = " + client_site_id);
    }
}
