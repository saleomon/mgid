package libs.hikari.tableQueries.bank;

import libs.hikari.utils.BankInstructions;

import java.math.BigDecimal;

public class CurrencyRates {
    public BigDecimal getCurrencyRate(String currency) {
        return BankInstructions.getConnectionPartnersSelectColumn(
                "select rate from currency_rates " +
                        "where dt > NOW() - INTERVAL 4 DAY and currency = " + currency + " order by dt desc");
    }
}
