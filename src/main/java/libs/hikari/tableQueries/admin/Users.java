package libs.hikari.tableQueries.admin;

import libs.hikari.utils.AdminInstructions;

import java.util.Objects;

public class Users {

    public static boolean selectIsExistCustomUserCab(String userLogin){
        return AdminInstructions.getConnectionPartnersSelectList("select login from users where login ='" + userLogin + "'").size() > 0;
    }

    public static int selectBankLegalEntitiesId(String login){
        return Integer.parseInt(Objects.requireNonNull(AdminInstructions.getConnectionAdminSelectColumn("select bank_legal_entities_id from users where login ='" + login + "'")));
    }

    public String getIsBlockedFlag(String login){
        return Objects.requireNonNull(AdminInstructions.getConnectionAdminSelectColumn("select is_blocked from users where login ='" + login + "'"));
    }

    /**
     * get total amount of advertiser agencies at list
     */
    public int selectAmountOfEnabledAgencies(){
        return Integer.parseInt(Objects.requireNonNull(AdminInstructions.getConnectionAdminSelectColumn(
                "select count(u.login) as allAgencies " +
                        "from admin.users as u join partners.managers_accounts as m on u.login = m.manager " +
                        "where u.accompanying_manager != '' and u.status in (1, 4)")));
    }

    /**
     * get total amount of advertiser agencies with bonuses
     */
    public int selectAmountOfAgenciesWithBonuses(){
        return Integer.parseInt(Objects.requireNonNull(AdminInstructions.getConnectionAdminSelectColumn(
                "select count(u.login) as allAgenciesWithBonuses " +
                        "from admin.users as u join partners.managers_accounts_flow as m on u.login = m.manager " +
                        "join partners.managers_accounts m1 on m1.manager = u.login")));
    }

    /**
     * get amount of advertiser agencies at list without MCM
     */
    public int selectAmountOfEnabledAgenciesWithoutMCM(){
        return Integer.parseInt(Objects.requireNonNull(AdminInstructions.getConnectionAdminSelectColumn(
                "select count(u.login) as allAgencies " +
                        "from admin.users as u join partners.managers_accounts as m on u.login = m.manager " +
                        "where u.accompanying_manager != '' and u.status in (1, 4) and m.mcm_aa = 0")));
    }

}
