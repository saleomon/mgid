package libs.hikari.tableQueries.admin;

import libs.hikari.utils.AdminInstructions;

import java.util.Arrays;
import java.util.stream.Collectors;

public class RolesPrivilegesDevelopment {

    public void insertRolesPrivilegesDevelopmentCustom(Integer roleId, String... privileges){
        Arrays.stream(privileges)
                .forEach(i -> AdminInstructions.getConnectionAdminUpdate(
                        "insert roles_privileges_development (role_id, privilege) " +
                        "select * from ( select " + roleId + ", 'default/" + i + "') AS tmp " +
                        "where not exists (" +
                        "select privilege from roles_privileges_development where role_id = " + roleId + " " +
                        "and privilege = 'default/" + i + "') limit 1;"));
    }

    public void deleteRolesPrivilegesDevelopmentCustom(Integer roleId, String... privilege){
        String values = Arrays.stream(privilege).map(i -> "'default/" + i + "'").collect(Collectors.joining(", "));

        AdminInstructions.getConnectionAdminUpdate("delete from roles_privileges_development " +
                "where role_id = " + roleId + " " +
                "and privilege IN(" + values + ");");
    }
}
