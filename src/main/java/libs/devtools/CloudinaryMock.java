package libs.devtools;

import com.github.kklisura.cdt.protocol.types.fetch.RequestPattern;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

import java.util.*;

public class CloudinaryMock extends BaseMock {


    public enum ResponseWithImages {
        MOUNT("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/mount.jpg\",\"isAnimation\":false,\"previewLink\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/480x270\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL21vdW50LmpwZw==.jpg?v=1628226920-C5DRVxBt6b5iU_DjL6EJRIgu5MUrlhw7rBjsIh14fIE\",\"availableLinks\":{\"16:9\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL21vdW50LmpwZw==.jpg?v=1628226920-4Jv-ah7fDwpGsNM2OmUJEnXQoE6jfuS7Yi9WFcUvxso\",\"3:2\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x640\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL21vdW50LmpwZw==.jpg?v=1628226920-RKkA_NM5H6PV9mSesvsfGBikCghbldUcIj3brCem-JA\",\"1:1\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x960\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL21vdW50LmpwZw==.jpg?v=1628226920-B0slsUtK5cruAODFxGjDdyS2PytqEAvOQVBRlDoVwR4\"},\"previewFormat\":\"16:9\",\"resetNeeded\":true,\"defaultEffect\":\"e_sharpen:100\",\"imageSource\":\"0\"}}"),
        GEERAF("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/geeraf.png\",\"isAnimation\":false,\"previewLink\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2dlZXJhZi5wbmc=.jpg?v=1626275671-j3ywHom5xcnlTLdMq1-oed5BFiMnzG3mPGTN_TzT0k8\",\"availableLinks\":{\"16:9\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2dlZXJhZi5wbmc=.jpg?v=1626275671-j3ywHom5xcnlTLdMq1-oed5BFiMnzG3mPGTN_TzT0k8\",\"3:2\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x640\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2dlZXJhZi5wbmc=.jpg?v=1626275671-cxAQhA5T5D873PwIdXzNncNgGnwNe20xsxzEk8AbJc4\",\"1:1\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x960\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2dlZXJhZi5wbmc=.jpg?v=1626275672-7p0SME7uw5DXZYar_lcjJj4_fb6t4FtpLu2I4X4DKoA\"},\"previewFormat\":\"16:9\",\"effectChange\":false,\"imageSource\":\"0\"}}"),
        STONEHENGE("{\"status\":\"success\",\"data\":{\"link\":\"https:\\/\\/local.s3.eu-central-1.amazonaws.com\\/Stonehenge.jpg\",\"isAnimation\":false,\"previewLink\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL1N0b25laGVuZ2UuanBn.jpg?v=1626275671-j3ywHom5xcnlTLdMq1-oed5BFiMnzG3mPGTN_TzT0k8\",\"availableLinks\":{\"16:9\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL1N0b25laGVuZ2UuanBn.jpg?v=1626275671-j3ywHom5xcnlTLdMq1-oed5BFiMnzG3mPGTN_TzT0k8\",\"3:2\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x640\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL1N0b25laGVuZ2UuanBn.jpg?v=1626275671-cxAQhA5T5D873PwIdXzNncNgGnwNe20xsxzEk8AbJc4\",\"1:1\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x960\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL1N0b25laGVuZ2UuanBn.jpg?v=1626275672-7p0SME7uw5DXZYar_lcjJj4_fb6t4FtpLu2I4X4DKoA\"},\"previewFormat\":\"16:9\",\"effectChange\":false,\"imageSource\":\"0\"}}"),
        HAPPINESS("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happiness.jpeg\",\"isAnimation\":false,\"previewLink\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/480x270\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2hhcHBpbmVzcy5qcGVn.jpg?v=1637819825-XrSCINDJJGhaKkez4UDxLrIqB83P3CSTmg2nZDjVnQI\",\"availableLinks\":{\"16:9\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x540\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2hhcHBpbmVzcy5qcGVn.jpg?v=1637819825-tkP5cGB_r328bFtWkRRhabWXUJ3LOe2IujOGZmPTZGI\",\"3:2\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x640\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2hhcHBpbmVzcy5qcGVn.jpg?v=1637819825-dA584_bhK0i-K7qoH_7DMNprET0EoppmH0Mq9gCIuA8\",\"1:1\":\"https:\\/\\/s-img.mgid.com\\/g\\/-\\/960x960\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tL2hhcHBpbmVzcy5qcGVn.jpg?v=1637819825-7GMc9uAQD3PcHDIhmjzkZIIR5Q8hfjrNkEp0wV2j4qE\"},\"previewFormat\":\"16:9\",\"resetNeeded\":true,\"defaultEffect\":\"e_sharpen:100\",\"gettyImagesId\":\"1289220545\",\"imageSource\":\"0\"}}"),
        WALKMAN("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/3_2_ex.gif\",\"isAnimation\":true,\"isImproved\":false,\"preview\":\"https:\\/\\/s-img.mgid.com\\/g\\/1\\/680x453\\/-\\/aHR0cDovL2xvY2FsLnMzLmV1LWNlbnRyYWwtMS5hbWF6b25hd3MuY29tLzNfMl9leA==.gif\",\"ar\":\"3:2\",\"imageSource\":\"0\"}}"),
        MANAGERS_RESET_TRUE("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"isAnimation\":false,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\"},\"previewFormat\":\"16:9\",\"resetNeeded\":true,\"defaultEffect\":\"e_sharpen:100\",\"imageSource\":\"0\"}}"),
        MANAGERS_RESET_FALSE("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"isAnimation\":false,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\"},\"previewFormat\":\"16:9\",\"resetNeeded\":false,\"defaultEffect\":\"e_sharpen:100\",\"imageSource\":\"0\"}}"),
        MANAGERS_DASH_SET_FOCAL("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"isAnimation\":null,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\"},\"effectChange\":true,\"gettyImagesId\":\"4556\",\"imageSource\":\"0\"}}"),
        MANAGERS_DASH_SET_VIDEO("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"isAnimation\":null,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/managers_croped.mp4\"},\"effectChange\":false,\"gettyImagesId\":\"4556\",\"imageSource\":\"0\"}}"),
        HAPPY_WOMAN("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"isAnimation\":false,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\"},\"previewFormat\":\"16:9\",\"resetNeeded\":true,\"defaultEffect\":\"e_sharpen:100\",\"imageSource\":\"0\"}}"),
        HAPPY_WOMAN1("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\"}}"),
        HAPPY_KID_FROM_GETTY("{\"status\":\"success\",\"data\":{\"link\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"isAnimation\":false,\"isVideo\":true,\"previewLink\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"availableLinks\":{\"16:9\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"3:2\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\",\"1:1\":\"http:\\/\\/local.s3.eu-central-1.amazonaws.com\\/happy_woman.mp4\"},\"previewFormat\":\"16:9\",\"resetNeeded\":false,\"defaultEffect\":\"e_sharpen:100\",\"gettyImagesId\":\"1212123\",\"imageSource\":\"2\"}}");
        private final String responseWithImages;

        ResponseWithImages(String responseWithImages) {
            this.responseWithImages = responseWithImages;
        }

        public String getResponseWithImages() {
            return responseWithImages;
        }

    }


    public CloudinaryMock(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }

    public void prepareCloudinaryResponse(ResponseWithImages imagesForResponce, String pattern) {
        setCountDownLatchCountRequest(1);
        // event -> request paused if url contains pattern
        fetch.onRequestPaused(e -> {
            fetch.fulfillRequest(
                    e.getRequestId(),
                    200,
                    new ArrayList<>(),
                    null,
                    collectResponseBodyCloudinary(imagesForResponce.getResponseWithImages()),
                    null);
            countDownLatch.countDown();

        });

        // pattern
        RequestPattern requestPattern1 = new RequestPattern();
        requestPattern1.setUrlPattern(pattern);
        fetch.enable(List.of(requestPattern1), false);

        // set page
        page.enable();

    }

    private String collectResponseBodyCloudinary(String imagesForResponse){
        return new String(Base64.getEncoder().encode(imagesForResponse.getBytes()));
    }
}
