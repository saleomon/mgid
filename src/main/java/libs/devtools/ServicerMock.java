package libs.devtools;

import com.github.kklisura.cdt.protocol.types.fetch.HeaderEntry;
import com.github.kklisura.cdt.protocol.types.network.Cookie;
import com.google.common.collect.Multimap;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;
import testData.project.Subnets;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.getRegExValue;
import static libs.hikari.tableQueries.partners.TickersComposite.getCountNews;
import static testData.project.publishers.ServicerData.*;
import static testData.project.Subnets.*;

public class ServicerMock extends BaseMock {

    private final Logger log;
    private Object servicer_widgetId;
    private int[] widgetIds;
    private final ArrayList<String> consoleMessages = new ArrayList<>();
    private int cappingCountDown = 0;
    private String standName;
    private Subnets.SubnetType subnetType;
    private String responseParams = "";
    private String evaluateJsBeforeLoadScript;
    private String muid = null;
    private String pvid;
    private String requestUrl;
    private List<String> teasersForBodyResponse;
    private Map<String, Object> headers;
    private final List<String> requestList = new ArrayList<>();

    // https://c.mgid.com/vs/ (video team)
    private final List<String> cMgidVsList = new ArrayList<>();
    private Map<String, String> localStorage = new HashMap<>();
    private Map<String, String> sessionStorage = new HashMap<>();
    private RequestType requestType = RequestType.DEFAULT;
    private ResponseType responseType = ResponseType.DEFAULT;
    private boolean isSendRequestOnServicer = false;
    private List<HeaderEntry> cVsHeaders = new ArrayList<>();
    private Map<NativeBackfill, String> nativeBackfillParams = new HashMap<>();
    private int responseCountVideoAds = 1;
    private boolean enabledVideoAd = true;
    private Map<VideoFormats, Boolean> enableVideoFormats = new HashMap<>();
    private boolean isMobileEmulation = false;

    public enum RequestType {
        REPLACE_ID,
        DEFAULT
    }

    public enum ResponseType {
        EMPTY_RESPONSE,
        CUSTOM_BODY,
        VIDEO,
        DEFAULT
    }

    public enum VsEvents{
        COMPLETE("advideocomplete"),
        LOAD("adloaded"),
        VIEW("inview"),
        START("startad"),
        IMPRESSION("impression"),
        VI_IMPRESSION("viewable_impression"),
        PROGRESS_100("vr_progress_100"),
        REQUEST("requestad");

        private final String field;

        VsEvents(String field) {
            this.field = field;
        }
    }

    public enum NativeBackfill {
        DOMAIN,
        SITE_ID
    }

    public enum VideoFormats {
        DESKTOP("desktop"),
        MOBILE("mobile");

        private final String field;

        VideoFormats(String field) {
            this.field = field;
        }
    }

    public ServicerMock(SessionId sessionId, Logger log, MobileEmulationSettings mobileEmulationSettings) {
        super(sessionId, log, mobileEmulationSettings);
        this.log = log;
    }

    public ServicerMock setStandName(String standName) {
        this.standName = standName;
        return this;
    }

    public void setNativeBackfillParams(Map<NativeBackfill, String> nativeBackfillParams) {
        this.nativeBackfillParams = nativeBackfillParams;
    }

    public ServicerMock setWidgetIds(int... widgetIds) {
        this.widgetIds = widgetIds;
        setCountDownLatchCountRequest(1);
        return this;
    }

    public ServicerMock setMobileEmulation(boolean mobileEmulation) {
        isMobileEmulation = mobileEmulation;
        return this;
    }

    public ServicerMock setEvaluateJsBeforeLoadScript(String evaluateJsBeforeLoadScript) {
        this.evaluateJsBeforeLoadScript = evaluateJsBeforeLoadScript;
        return this;
    }

    public ServicerMock setResponseCountVideoAds(int responseCountVideoAds) {
        this.responseCountVideoAds = responseCountVideoAds;
        return this;
    }

    public ServicerMock setEnableVideoFormats(Map<VideoFormats, Boolean> enableVideoFormats) {
        this.enableVideoFormats = enableVideoFormats;
        return this;
    }

    public ServicerMock setEnabledVideoAd(boolean enabledVideoAd) {
        this.enabledVideoAd = enabledVideoAd;
        return this;
    }

    public ServicerMock setSubnetType(Subnets.SubnetType subnetType) {
        this.subnetType = subnetType;
        return this;
    }

    public ServicerMock setTeasersForBodyResponse(List<String> teasersForBodyResponse) {
        this.teasersForBodyResponse = teasersForBodyResponse;
        return this;
    }

    public ServicerMock setRequestType(RequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public ServicerMock setResponseType(ResponseType responseType) {
        this.responseType = responseType;
        return this;
    }

    public boolean isSendRequestOnServicer() {
        return isSendRequestOnServicer;
    }

    public ArrayList<String> getConsoleMessages() {
        return consoleMessages;
    }

    public ServicerMock setResponseParams(String responseParams) {
        this.responseParams = "," + responseParams;
        return this;
    }

    public String getMuid() {
        return muid;
    }

    public ServicerMock setMuid(String muid) {
        this.muid = muid;
        return this;
    }

    public Map<String, String> getLocalStorage() {
        return localStorage;
    }

    public Map<String, String> getSessionStorage() {
        readSessionStorage();
        return sessionStorage;
    }

    public List<String> getRequestList() {
        return requestList;
    }

    public Multimap<Object, String> getRequestsMap() {
        return servicerMap;
    }

    public ServicerMock clearServicerMockData(){
        servicerMap.clear();
        localStorage.clear();
        sessionStorage.clear();
        requestList.clear();
        return this;
    }

    public ServicerMock setServicerCountDown(int servicerCountDown){
        setCountDownLatchCountRequest(servicerCountDown);
        return this;
    }

    public ServicerMock setCappingCountDown(int cappingCountDown){
        this.cappingCountDown = cappingCountDown;
        setCountDownLatchCountRequest(cappingCountDown);
        return this;
    }

    public ServicerMock setVpaidCountDown(int vpaidCountDown){
        setCountDownLatchCountRequest(vpaidCountDown);
        return this;
    }

    /**
     * Simple
     * - something
     * Video
     * - enableVideoFormats: desktop/mobile
     * - adsLimit: 2
     * - vpaidDspList: responseCountVideoAds = 1
     * - nvpaidParameters: false(default)
     */
    public ServicerMock interceptionNetworkAndMockThem() {

        String servicerUrlPattern = "(servicer\\.\\D*\\.(com|co\\.uk|io))/\\d*/";
        String servicerVpaidUrlPattern = "(servicer\\.\\D*\\.(com|co\\.uk|io)/vpaid/)";
        String jscUrlPattern = "(jsc\\.\\D*\\.(com|co\\.uk|io))/";
        String cUrlPattern = "(c\\.\\D*\\.(com|co\\.uk|io))/c?";
        String cVsUrlPattern = "https://(c\\.\\D*\\.(com|co\\.uk|io))/vs/";
        setCVsHeaders();

        // event -> request paused if url contains servicer
        fetch.onRequestPaused(e -> {

            String url = e.getRequest().getUrl();
            requestList.add(url);


            if(getRegExValue(url, servicerUrlPattern) != null) {

                log.info("servicerUrlPattern: " + url);

                writeServicerRequestToMap(url);

                //get headers
                headers = e.getRequest().getHeaders();

                // get localStorage
                readLocalStorage();
                readSessionStorage();

                fetch.fulfillRequest(
                        e.getRequestId(),
                        200,
                        new ArrayList<>(),
                        null,
                        collectServicerResponseBody(servicer_widgetId),
                        null
                );
                countDownLatch.countDown();

                isSendRequestOnServicer = true;
            }

            else if(getRegExValue(url, servicerVpaidUrlPattern) != null){

                log.info("servicerVpaidUrlPattern: " + url);

                fetch.fulfillRequest(
                        e.getRequestId(),
                        200,
                        setVpaidHeaders(),
                        null,
                        collectVpaidResponseBody(servicer_widgetId),
                        null
                );
                countDownLatch.countDown();
            }

            else if(getRegExValue(url, jscUrlPattern) != null){

                log.info("jscUrlPattern: " + url);

                fetch.continueRequest(
                        e.getRequestId(),
                        "http://local.s3.eu-central-1.amazonaws.com/" + servicer_widgetId + ".js",
                        null,
                        null,
                        null
                );
            }

            else if(getRegExValue(url, cVsUrlPattern) != null){
                cMgidVsList.add(url);

                fetch.fulfillRequest(
                        e.getRequestId(),
                        200,
                        cVsHeaders,
                        null,
                        null,
                        null
                );
            }

            else if(getRegExValue(url, cUrlPattern) != null
                    && cappingCountDown > 0){

                log.info("cUrlPattern: " + url);

                writeCMgidRequestToMap(url);
                countDownLatch.countDown();
            }

            else {
                fetch.continueRequest(e.getRequestId());
            }

        });

        if(isMobileEmulation) mobileEmulationSettings();

        fetch.enable();
        page.enable();
        console.enable();

        console.onMessageAdded(e -> consoleMessages.add(e.getMessage().getText()));

        // set page
        page.navigate(standName);

        if(evaluateJsBeforeLoadScript != null) {
            runtime.evaluate(evaluateJsBeforeLoadScript);
        }
        sleep(2000);
        getMuidToServicer();
        return this;
    }

    private void writeServicerRequestToMap(String request) {
        // id from servicer url / servicer_widgetId it's id from test argument
        Object widgetId;

        // get url
        servicer_url = request.substring(0, request.indexOf("?")).replace("https://", "");

        switch (requestType) {
            case REPLACE_ID -> {
                servicer_widgetId = String.valueOf(widgetIds[0]);
                widgetId = servicer_widgetId;
            }
            default -> {
                if (request.contains("uniqId")) {
                    String s = Arrays.stream(request.split("&")).filter(i -> i.contains("uniqId=")).findFirst().get();
                    widgetId = s.substring(s.indexOf("=") + 1);
                    servicer_widgetId = String.valueOf(widgetIds[0]);
                } else {
                    servicer_widgetId = servicer_url.substring(servicer_url.indexOf("/") + 1, servicer_url.lastIndexOf("/"));
                    widgetId = servicer_widgetId;
                }
            }
        }

        //get servicer params from request (key=value)
        request = request.replaceAll("https://" + servicer_url + "\\?", "");
        requestUrl = request;

        //put widget id from servicer request and params data from servicer request to map
        servicerMap.put(widgetId, request);

        // get pvid from servicer request
        pvid = parseServicerRequest(widgetId, "pvid");
        log.info("pvid: " + pvid);
    }

    public void writeCMgidRequestToMap(String request) {
        String cMgidComCUrl = request.substring(0, request.indexOf("?")).replace("https://", "");

        String id = Arrays.stream(request.split("&")).filter(i -> i.contains("cid=")).findFirst().get();
        id = id.substring(id.indexOf("=") + 1);

        request = request.replaceAll("https://" + cMgidComCUrl + "\\?", "");
        cMgidComCMap.put(id, request);
    }

    public String parseServicerRequest(Object widgetId, String param, int... requestNumbers){
        Collection<String> request = servicerMap.get(widgetId.toString());
        int requestNumber = requestNumbers.length > 0 ? requestNumbers[0] : 0;
        return parseRequest(request, param, requestNumber);
    }

    public String parseCMgidComCRequest(Object widgetId, String param) {
        Collection<String> request = cMgidComCMap.get(widgetId.toString());
        return parseRequest(request, param, 0);
    }

    public String parseClickUrlRequest(String url, String param){
        Collection<String> request = Collections.singleton(url);
        return parseRequest(request, param, 0);
    }

    private String parseRequest(Collection<String> request, String param, int requestNumber){
        Optional<String> temp;
        temp = Arrays.stream(request.toArray()[requestNumber].toString().split("&"))
                .filter(i -> i.substring(0, i.indexOf("=")).equals(param))
                .findFirst();

        if(temp.isEmpty()) return null;

        String val = temp.get();
        return val.substring(val.indexOf("=")+1);
    }

    public long getCountCurrentRequest(String url){
        return requestList.stream()
                .filter(i -> i.contains(url)).count();
    }

    public void readLocalStorage(){
        localStorage = readStorage("local");
    }

    public void readSessionStorage(){
        sessionStorage = readStorage("session");
    }

    private Map<String, String> readStorage(String storageType){
        Map<String, String> map = new HashMap<>();
        String storage = storageType + "Storage";
        String key, value;
        String[] mass = runtime.evaluate("var mass = '';" +
                "for (var i = 0; i < " + storage + ".length; i++) {" +
                "mass+= " + storage + ".key(i) + ':' + " + storage + ".getItem(" + storage + ".key(i)) + (i!=" + storage + ".length-1 ? ',' : '');" +
                "}").getResult().getValue().toString().split(",");
        for(String m : mass){
            key = m.substring(0, m.indexOf(":"));
            value = m.substring(m.indexOf(":")+1);
            map.put(key, value);
        }
        return map;
    }

    private String collectServicerResponseBody(Object widgetId){
        String requestBody;
        String teasers;
        String loadGoodsName = "";
        boolean isTeasers = true;

        // get load goods name
        switch (subnetType) {
            case SCENARIO_MGID -> loadGoodsName = "MarketGid";
            case SCENARIO_ADSKEEPER -> loadGoodsName = SUBNET_ADSKEEPER_NAME;
            case SCENARIO_IDEALMEDIA_IO -> {
                loadGoodsName = SUBNET_IDEALMEDIA_IO_NAME;
                isTeasers = false;
            }
        }
        switch (responseType) {

            case EMPTY_RESPONSE -> requestBody = String.format(servicerDataEmpty, loadGoodsName, widgetId);

            case CUSTOM_BODY -> {
                teasers = String.join(",", teasersForBodyResponse);

                // collect request
                requestBody = String.format(servicerDataStart, loadGoodsName, widgetId)
                        + teasers
                        + String.format(servicerDataEnd, pvid, muid) + responseParams + "}]);"
                        + String.format(servicerDataIconBlockTeaser, loadGoodsName, widgetId);
            }

            case VIDEO -> {

                if(enableVideoFormats.isEmpty()) {
                    try {
                        super.tearDown();
                        throw new Exception("enableVideoFormats is EMPTY !!!");
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }

                String formats;
                if(enableVideoFormats.size() == 2){
                    formats = String.format(videoFormat, VideoFormats.DESKTOP.field)
                            + ","
                            + String.format(videoFormat, VideoFormats.MOBILE.field);
                }
                else {
                    formats = enableVideoFormats.get(VideoFormats.DESKTOP) ?
                            String.format(videoFormat, VideoFormats.DESKTOP.field) :
                            String.format(videoFormat, VideoFormats.MOBILE.field);
                }

                requestBody = String.format(servicerDataStart, loadGoodsName, widgetId)
                        + videoData
                        + getVideoDataEnd(widgetId, enabledVideoAd, formats, videoLibVersion);
            }

            default -> {
                // get tickers_composite.count_news
                int countNews = getCountNews(Integer.parseInt((String) widgetId));

                // get teasers / news data
                boolean finalIsTeasers = isTeasers;
                teasers = IntStream.range(0, countNews)
                        .mapToObj(i -> finalIsTeasers ? teasersList.get(i) : newsList.get(i))
                        .collect(Collectors.joining(","));

                // collect response
                requestBody = String.format(servicerDataStart, loadGoodsName, widgetId)
                        + teasers
                        + String.format(servicerDataEnd, pvid, muid) + responseParams + "}]);"
                        + String.format(servicerDataIconBlockTeaser, loadGoodsName, widgetId);
            }
        }
        return new String(Base64.getEncoder().encode(requestBody.getBytes()));
    }

    private String collectVpaidResponseBody(Object widgetId){
        String nvpaid = "", vpaidDsp;
        if(!nativeBackfillParams.isEmpty()){
            nvpaid = nvpaidParameters(nativeBackfillParams.get(NativeBackfill.DOMAIN),
                    nativeBackfillParams.get(NativeBackfill.SITE_ID),
                    widgetId);
        }

        vpaidDsp = IntStream.range(0, responseCountVideoAds)
                .mapToObj(i -> vpaidDspList.get(i))
                .collect(Collectors.joining(","));


        return new String(Base64.getEncoder().encode(vpaidData(widgetId, vpaidDsp, nvpaid).getBytes()));
    }

    public BaseMock countDownLatchAwaitWithoutTearDown(long... time) {
        try {
            //await must be wait run 1 -> count event
            if(time.length > 0) countDownLatch.await(time[0], TimeUnit.SECONDS);
            else countDownLatch.await(25, TimeUnit.SECONDS);
        }
        catch (InterruptedException e){
            log.error("Catch " + e);
        }
        //customCountDown = 0;
        return this;
    }

    public BaseMock countDownLatchAwait(String url) {
        int timer = 0;
        try {
            // await must be wait run 1 -> count event
            countDownLatch.await(50, TimeUnit.SECONDS);
            do {
                timer++;
                if(timer >= 2) {
                    sleep(500);
                }
            }
            while (requestList.stream().noneMatch(i -> i.contains(url)) && timer < 15);

            tearDown();
        }
        catch (InterruptedException e){
            log.error("Catch " + e);
        }
        //customCountDown = 0;
        return this;
    }

    public ServicerMock navigate(){
        page.enable();
        page.onLoadEventFired(e -> countDownLatch.countDown());
        page.navigate(standName);
        return this;
    }

    public String getRequestHeader(String header){
        return headers.get(header).toString();
    }

    public ServicerMock scrollTo(){
        sleep(1500);
        runtime.enable();
        runtime.evaluate("$(\"html, body\").animate({ scrollTop: document.body.scrollHeight }, \"slow\");");
        return this;
    }

    public ServicerMock scrollToSlow(){
        runtime.enable();
        runtime.evaluate("function pageScroll(speed) {" +
                "window.scrollBy(0,1);" +
                "setTimeout(pageScroll,speed);" +
                "}");
        runtime.evaluate("pageScroll(1);");
        return this;
    }

    public ServicerMock refresh(){ page.reload(); return this; }

    private void getMuidToServicer(){
        network.enable();
        Optional<Cookie> cookies = network.getAllCookies()
                .stream()
                .filter(i -> i.getName().equals("muidn"))
                .findFirst();

        cookies.ifPresent(cookie -> muid = cookie
                .getValue());
        log.info("muid: " + muid);
    }

    public String getCookie(String cookieName, String cookieDomain) {
        network.enable();

        Optional<Cookie> cookie = network.getAllCookies()
                .stream()
                .filter(c -> c.getName().equals(cookieName) && c.getDomain().equals(cookieDomain))
                .findFirst();

        return cookie.isEmpty() ?
                null :
                cookie
                        .get()
                        .getValue();
    }

    public void deleteCookie(String cookieName, String domain) {
        network.enable();
        network.deleteCookies(cookieName, "", domain, "/");
    }

    public boolean modifyAndCheckServicerRequestWithCmgidRequest(int siteId){
        boolean isHaveSiteIdParam;
        String c_mgid_url;
        List<String> mass_c_mgid;
        List<String> mass_servicer;

        List<String> remove_params_c_mgid = Arrays.asList("cbuster", "tfre", "pageView", "muid", "site", "implVersion", "pvid");
        List<String> remove_params_servicer = Arrays.asList("cbuster", "tfre", "pageView", "w", "h", "cols", "muid", "implVersion",
                "pvid", "ap", "mp4", "sz", "szp", "szl", "id5", "pubmatic", "sessionId", "sessionPage", "sessionNumber", "sessionNumberWeek");

        List c_mgid_urls = requestList.stream()
                .filter(i -> i.contains("https://c.mgid.com/pv/")).collect(Collectors.toList());

        if(c_mgid_urls.size() != 0) {

            // get current c.mgid.com and servicer by site id
            c_mgid_url = c_mgid_urls.stream().filter(i -> i.toString().contains("site=" + siteId)).findFirst().get().toString();
            log.info("c_mgid_url: " + c_mgid_url);

            c_mgid_url = c_mgid_url.replaceAll("https://c.mgid.com/pv/\\?", "");

            // сравниваем контаинс всех параметров из первого запроса во втором и сверяем количество параметров
            mass_c_mgid = new ArrayList<>(Arrays.asList(c_mgid_url.split("&")));
            mass_servicer = new ArrayList<>(Arrays.asList(requestUrl.split("&")));

            // is have 'site=', 'cbuster=', 'implVersion=', 'pageView=' param
            isHaveSiteIdParam = mass_c_mgid.contains("site=" + siteId) &
                    mass_c_mgid.stream().anyMatch(i -> i.contains("cbuster=")) &
                    mass_c_mgid.stream().anyMatch(i -> i.contains("implVersion=")) &
                    mass_c_mgid.stream().anyMatch(i -> i.contains("pvid=")) &
                    mass_c_mgid.stream().anyMatch(i -> i.contains("tfre=")) &
                    mass_c_mgid.stream().anyMatch(i -> i.contains("pageView="));

            // remove params
            mass_servicer.removeIf(i -> remove_params_servicer.contains(i.substring(0, i.indexOf("="))));
            mass_c_mgid.removeIf(i -> remove_params_c_mgid.contains(i.substring(0, i.indexOf("="))));

            return isHaveSiteIdParam &
                    mass_servicer.equals(mass_c_mgid);
        }
        return false;
    }

    private List<HeaderEntry> setVpaidHeaders(){
        HeaderEntry headers_1 = new HeaderEntry();
        headers_1.setName("access-control-allow-credentials");
        headers_1.setValue("true");

        HeaderEntry headers_2 = new HeaderEntry();
        headers_2.setName("access-control-allow-headers");
        headers_2.setValue("Origin, X-Requested-With, Content-Type, Accept");

        HeaderEntry headers_3 = new HeaderEntry();
        headers_3.setName("access-control-allow-origin");
        headers_3.setValue("http://local-apache.com");

        HeaderEntry headers_4 = new HeaderEntry();
        headers_4.setName("content-type");
        headers_4.setValue("text/xml; charset=utf-8");

        return Arrays.asList(headers_1,headers_2,headers_3,headers_4);
    }

    public void setCVsHeaders(){
        if(cVsHeaders.isEmpty()) {
            HeaderEntry headerEntry = new HeaderEntry();
            headerEntry.setName("content-type");
            headerEntry.setValue("image/gif");
            cVsHeaders = List.of(headerEntry);
        }
    }

    public int getVs(VsEvents eventName){
        return (int) cMgidVsList
                .stream()
                .filter(e -> e.contains("e=" + eventName.field))
                .count();
    }

    /**
     * wait when ad finished
     * @param countCompleteAds - number of ad
     */
    public void waitCustomEvent(VsEvents eventName, int countCompleteAds){
        int time = 0;
        while (((int) cMgidVsList
                .stream()
                .filter(e -> e.contains("e=" + eventName.field))
                .count() != countCompleteAds) && time < 90){
            sleep(500);
            time++;
        }
    }
}
