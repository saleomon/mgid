package libs.devtools;

import com.github.kklisura.cdt.protocol.types.fetch.RequestPattern;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;

import static core.helpers.BaseHelper.getRegExValue;

public class IdenfyMock extends BaseMock {

    String requestBody;

    public String getRequestBody() {
        return requestBody;
    }

    public enum Response {
        AML_CHECK("{\"status\":\"success\", \"data\":\"{\\\"AML\\\": [{\\\"status\\\": {\\\"serviceSuspected\\\": false, \\\"serviceUsed\\\": true, \\\"serviceFound\\\": true, \\\"checkSuccessful\\\": true, \\\"overallStatus\\\": \\\"NOT_SUSPECTED\\\"}, \\\"serviceName\\\": \\\"PilotApiAmlV2NameCheck\\\", \\\"uid\\\": \\\"MJAJ6RULD53GYQ02MW0TEAPZH\\\", \\\"errorMessage\\\": null, \\\"data\\\": [{\\\"reason\\\": \\\"USA: AMERICAN MANUFACTURING COUNCIL - TESLA CEO (EX)\\\", \\\"listNumber\\\": \\\"2106118\\\", \\\"suspicion\\\": \\\"PEPS\\\", \\\"isActive\\\": false, \\\"checkDate\\\": \\\"2022-09-19 08:18:16\\\", \\\"isPerson\\\": true, \\\"score\\\": 100, \\\"nationality\\\": \\\"USA\\\", \\\"surname\\\": \\\"MUSK\\\", \\\"dob\\\": \\\"1971\\\", \\\"lastUpdate\\\": \\\"2017-08-22\\\", \\\"name\\\": \\\"ELON\\\", \\\"listName\\\": \\\"PEPS\\\"}], \\\"serviceGroupType\\\": \\\"AML\\\"}]}\"}"),
        COMPANY_CHECK("{\"status\":\"success\", \"data\":\"{\\\"COMPANY\\\": [{\\\"status\\\": {\\\"serviceSuspected\\\": true, \\\"serviceUsed\\\": true, \\\"serviceFound\\\": true, \\\"checkSuccessful\\\": true, \\\"overallStatus\\\": \\\"SUSPECTED\\\"}, \\\"serviceName\\\": \\\"CompanyCheck\\\", \\\"serviceGroupType\\\": \\\"COMPANY\\\", \\\"uid\\\": \\\"KNR6IJ7G3EYNXSVVQNN6ZV3VV\\\", \\\"errorMessage\\\": null, \\\"data\\\": [{\\\"result\\\": {\\\"Name\\\": \\\"MyCompany\\\", \\\"Residence\\\": \\\"LT\\\", \\\"CompanyHasHits\\\": \\\"true\\\", \\\"ScoreAml\\\": \\\"2\\\", \\\"RiskAml\\\": \\\"M\\\", \\\"RiskElements\\\": [{\\\"RiskName\\\": \\\"ScoreAml\\\", \\\"Explanation\\\": \\\"The company is located in a medium risk country\\\"}, {\\\"RiskName\\\": \\\"RiskAml\\\", \\\"Explanation\\\": \\\"The AML risk for this company is MEDIUM\\\"},{\\\"RiskName\\\": \\\"CompanyHasHits\\\", \\\"Explanation\\\": \\\"The company's name is similar to names on sanctions' lists. Please check if the hits apply to the company.\\\"}], \\\"HitsOnLists\\\": [{\\\"Name\\\": \\\"MY AVIATION COMPANY LIMITED\\\", \\\"Forename\\\": \\\"UNKNOWN\\\", \\\"Reason\\\": \\\"IFSR Program\\\", \\\"Nationality\\\": \\\"Unknown\\\", \\\"BirthDate\\\": \\\"UNKNOWN\\\", \\\"HitType\\\": \\\"Sanction\\\", \\\"ListNumber\\\": \\\"24877\\\", \\\"ListName\\\": \\\"OFAC\\\", \\\"Score\\\": \\\"100\\\", \\\"LastUpdateDate\\\": \\\"2018-09-18T00:00:00\\\", \\\"IsPerson\\\": \\\"false\\\", \\\"IsActive\\\": \\\"true\\\"}], \\\"CompaniesFound\\\": []},\\\"companyName\\\": \\\"MyCompany\\\",\\\"country\\\": \\\"LT\\\"}]}]}\"}"),
        LID_CHECK("{\"status\":\"success\", \"data\":\"{\\\"LID\\\": [{\\\"status\\\": {\\\"serviceSuspected\\\": true, \\\"serviceUsed\\\": true, \\\"serviceFound\\\": true, \\\"checkSuccessful\\\": true, \\\"overallStatus\\\": \\\"SUSPECTED\\\"}, \\\"serviceName\\\": \\\"IrdInvalidPapers\\\", \\\"serviceGroupType\\\": \\\"LID\\\", \\\"uid\\\": \\\"KBSZDZPBJ3FS6NOT4L33VZ0KS\\\", \\\"errorMessage\\\": null, \\\"data\\\": [{\\\"documentNumber\\\": \\\"74587539\\\", \\\"documentType\\\": null, \\\"valid\\\": false, \\\"expiryDate\\\": \\\"2018-03-21\\\", \\\"checkDate\\\": \\\"2020-02-02 20:02:02\\\"}]}]}\"}");
        private final String response;

        Response(String response) {
            this.response = response;
        }

        public String getResponse() {
            return response;
        }
    }

    public IdenfyMock(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }

    public void prepareIdenfyResponse(Response response, String pattern) {
        setCountDownLatchCountRequest(1);
        // event -> request paused if url contains pattern
        fetch.onRequestPaused(e -> {
            requestBody = e.getRequest().getPostData();
            fetch.fulfillRequest(
                    e.getRequestId(),
                    200,
                    new ArrayList<>(),
                    null,
                    collectResponseBodyIdenfy(response.getResponse()),
                    null);
            countDownLatch.countDown();

        });

        // pattern
        RequestPattern requestPattern = new RequestPattern();
        requestPattern.setUrlPattern(pattern);
        fetch.enable(Arrays.asList(requestPattern), false);

        // set page
        page.enable();
    }

    private String collectResponseBodyIdenfy(String response){
        return new String(Base64.getEncoder().encode(response.getBytes()));
    }

    public String getRequestedParameter (String pattern){
        return requestBody.length() != 0 ? getRegExValue(requestBody, pattern) : "";
    }
}