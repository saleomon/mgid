package libs.devtools;

public class MobileEmulationSettings {

    public enum Device {
        IPHONE_6,
        GALAXY_S4,
        NEXUS_S,
        XPERIA_Z,
        LG_OPTIMUS_LTE
    }

    private Device defaultDevice = Device.GALAXY_S4;
    private int    height;
    private int    width;
    private double pixelRatio;
    private String userAgent;

    public MobileEmulationSettings() {
        setDevice(defaultDevice);
    }

    public int getHeight() { return height; }

    public void setHeight(int height) { this.height = height; }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public double getPixelRatio() {
        return pixelRatio;
    }

    public String getUserAgent() { return userAgent; }

    public void setDefaultDevice(Device defaultDevice) {
        this.defaultDevice = defaultDevice;
        setDevice(defaultDevice);
    }

    private void setDevice(Device device){
        switch (device){
            case IPHONE_6 -> {
                width = 375;
                height = 667;
                pixelRatio = 2d;
                userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";
            }
            case GALAXY_S4 -> {
                width = 360;
                height = 640;
                pixelRatio = 3d;
                userAgent = "Mozilla/5.0 (Linux; Android 4.2.2; GT-I9505 Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.59 Mobile Safari/537.36";
            }
            case NEXUS_S -> {
                width = 320;
                height = 533;
                pixelRatio = 1.5;
                userAgent = "Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; Nexus S Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
            }
            case XPERIA_Z -> {
                width = 360;
                height = 640;
                pixelRatio = 3d;
                userAgent = "Mozilla/5.0 (Linux; U; Android 4.2; en-us; SonyC6903 Build/14.1.G.1.518) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
            }
            case LG_OPTIMUS_LTE -> {
                width = 424;
                height = 753;
                pixelRatio = 1.7;
                userAgent = "Mozilla/5.0 (Linux; U; Android 2.3; en-us; LG-P930 Build/GRJ90) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
            }
        }

    }
}
