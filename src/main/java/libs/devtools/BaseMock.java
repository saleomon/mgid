package libs.devtools;

import com.github.kklisura.cdt.protocol.commands.Runtime;
import com.github.kklisura.cdt.protocol.commands.*;
import com.github.kklisura.cdt.services.ChromeDevToolsService;
import com.github.kklisura.cdt.services.WebSocketService;
import com.github.kklisura.cdt.services.config.ChromeDevToolsServiceConfiguration;
import com.github.kklisura.cdt.services.exceptions.WebSocketServiceException;
import com.github.kklisura.cdt.services.impl.ChromeDevToolsServiceImpl;
import com.github.kklisura.cdt.services.impl.WebSocketServiceImpl;
import com.github.kklisura.cdt.services.invocation.CommandInvocationHandler;
import com.github.kklisura.cdt.services.utils.ProxyUtils;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import core.service.PropertiesManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static core.service.PropertiesManager.getResourceByName;

public class BaseMock {

    private final Logger log;
    protected String host = getResourceByName(PropertiesManager.ConfigValue.HOST);
    protected ChromeDevToolsService devtools;
    protected Page page;
    protected Fetch fetch;
    protected Network network;
    protected Emulation emulation;
    protected Runtime runtime;
    protected Console console;
    protected SessionId sessionId;
    protected CountDownLatch countDownLatch;
    //protected int customCountDown = 0;
    protected String servicer_url;
    protected Multimap<Object, String> servicerMap = ArrayListMultimap.create();
    protected Multimap<Object, String> cMgidComCMap = ArrayListMultimap.create();
    private MobileEmulationSettings mobileEmulationSettings;


    protected BaseMock(SessionId sessionId, Logger log) {
        this.sessionId = sessionId;
        this.log = log;
    }

    protected BaseMock(SessionId sessionId, Logger log, MobileEmulationSettings mobileEmulationSettings) {
        this.sessionId = sessionId;
        this.log = log;
        this.mobileEmulationSettings = mobileEmulationSettings;
    }

    protected void setCountDownLatchCountRequest(int number) {
        countDownLatch = new CountDownLatch(number);
    }

    public void init() {
        // Init ChromeDevtools client
        WebSocketService webSocketService;
        try {
            if(devtools == null) {
                webSocketService = WebSocketServiceImpl.create(new URI(String.format("ws://" + host + ":4444/devtools/%s/page", sessionId)));
                CommandInvocationHandler commandInvocationHandler = new CommandInvocationHandler();
                Map<Method, Object> commandsCache = new ConcurrentHashMap<>();
                devtools =
                        ProxyUtils.createProxyFromAbstract(
                                ChromeDevToolsServiceImpl.class,
                                new Class[]{WebSocketService.class, ChromeDevToolsServiceConfiguration.class},
                                new Object[]{webSocketService, new ChromeDevToolsServiceConfiguration()},
                                (unused, method, args) ->
                                        commandsCache.computeIfAbsent(
                                                method,
                                                key -> {
                                                    Class<?> returnType = method.getReturnType();
                                                    return ProxyUtils.createProxy(returnType, commandInvocationHandler);
                                                }));
                commandInvocationHandler.setChromeDevToolsService(devtools);

                page = devtools.getPage();
                fetch = devtools.getFetch();
                network = devtools.getNetwork();
                emulation = devtools.getEmulation();
                runtime = devtools.getRuntime();
                console = devtools.getConsole();
            }

        } catch (WebSocketServiceException | URISyntaxException e) {
            log.error("Catch " + e);
        }
    }

    public void tearDown() {
        if (devtools != null || !devtools.isClosed()) {
            devtools.close();
            devtools = null;
        }
    }

    public BaseMock countDownLatchAwait(long... time) {
        try {
            // await must be wait run 1 -> count event
            if(time.length > 0) countDownLatch.await(time[0], TimeUnit.SECONDS);
            else countDownLatch.await(50, TimeUnit.SECONDS);
            tearDown();
        }
        catch (InterruptedException e){
            log.error("Catch " + e);
        }
        //customCountDown = 0;
        return this;
    }

    protected void mobileEmulationSettings(){

        emulation.setUserAgentOverride(mobileEmulationSettings.getUserAgent(), "en", "iOS", null);

        emulation.setDeviceMetricsOverride(
                mobileEmulationSettings.getWidth(),
                mobileEmulationSettings.getHeight(),
                mobileEmulationSettings.getPixelRatio(),
                true);

        emulation.setTouchEmulationEnabled(true);
    }
}
