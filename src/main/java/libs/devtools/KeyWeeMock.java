package libs.devtools;

import com.github.kklisura.cdt.protocol.types.fetch.RequestPattern;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Objects;

import static core.helpers.BaseHelper.getRegExValue;

public class KeyWeeMock extends BaseMock {

    FileInputStream contentFile;
    String requestUrl;

    public KeyWeeMock(SessionId sessionId, Logger log) {
        super(sessionId, log);
    }

    public void prepareTitleRecommendationResponse(String pattern, String fileNameWithResponse) {
        setCountDownLatchCountRequest(1);
        // event -> request paused if url contains pattern
        fetch.onRequestPaused(e -> {
            requestUrl = e.getRequest().getUrl();
            fetch.fulfillRequest(
                    e.getRequestId(),
                    200,
                    new ArrayList<>(),
                    null,
                    collectResponceBodyGettyImages(Objects.requireNonNull(getResponseFromFile(fileNameWithResponse))),
                    null);

            countDownLatch.countDown();

        });

        // pattern
        RequestPattern requestPattern = new RequestPattern();
        requestPattern.setUrlPattern(pattern);
        fetch.enable(Collections.singletonList(requestPattern), false);

        page.enable();
    }

    private String getResponseFromFile(String fileNameWithResponse){
        try {
            contentFile = new FileInputStream("src/main/resources/files/" + fileNameWithResponse + ".txt");
            return IOUtils.toString(contentFile, StandardCharsets.UTF_8);
        } catch (Exception e) {
            System.out.println("Exception - " + e);
        }
        return null;
    }

    private String collectResponceBodyGettyImages(String imagesForResponce){
        return new String(Base64.getEncoder().encode(imagesForResponce.getBytes()));
    }

    public String getLanguageIdFromRequest(){
        return requestUrl.length() != 0 ? Objects.requireNonNull(getRegExValue(requestUrl, "languageId=[0-9]{1,3}")).split("=")[1] : "";
    }

    public String getRequestedParameterUrl(String pattern){
        return requestUrl.length() != 0 ? getRegExValue(requestUrl, pattern) : "";
    }
}
