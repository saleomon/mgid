package libs.dockerClient.utils;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.messages.Container;
import com.spotify.docker.client.messages.ExecCreation;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class DockerHelper {
    DockerClient docker;

    public DockerHelper(DockerClient docker) {
        this.docker = docker;
    }

    /**
     * get container id
     * @param containerName - cab_1, cli_1
     */
    public String getContainerId(String containerName){
        try {
            final List<Container> containers = docker.listContainers();
            for (Container c : containers) {
                if (Objects.requireNonNull(c.names()).get(0).equals(containerName)) {
                    return c.id();
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * get networkId
     */
    public String getNetworkId(String networkName){
        String networkId = null;
        try {
            networkId = Objects.requireNonNull(docker.inspectNetwork(networkName).id());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("networkId: " + networkId);
        return networkId;
    }

    /**
     * get all environments in ...env enr return them in List
     */
    public List<String> getEnvList(String fileName){
        try {
            return Arrays.asList(
                    new Scanner(new File("docker/" + fileName + ".env"))
                            .useDelimiter("\\Z")
                            .next()
                            .split("[\\r\\n]+"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * execute command in container
     * @param containerId - cab_1, cli_1
     * @param commands - ls, pwd, rm
     */
    public String commandExec(String containerId, String... commands){
        ExecCreation execCreation;
        String log = "";
        while (log.equals("")) {
            try {
                execCreation = docker.execCreate(
                        containerId,
                        commands,
                        DockerClient.ExecCreateParam.attachStdout(),
                        DockerClient.ExecCreateParam.attachStderr()
                );

                Thread.sleep(1000);

                try (final LogStream stream = docker.execStart(execCreation.id())) {
                    log = stream.readFully();
                    return log;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
