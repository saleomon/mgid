package libs.dockerClient.base;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import libs.dockerClient.utils.DockerHelper;

import static core.service.PropertiesManager.ConfigValue;
import static core.service.PropertiesManager.getResourceByName;

public class DockerInit {

    protected DockerHelper dockerHelper;
    protected String  networkId;

    protected final DockerClient docker = new DefaultDockerClient(getResourceByName(ConfigValue.DOCKER_SOCKET));

    protected final String ciJobId = getResourceByName(ConfigValue.CI_JOB_ID);

    protected String networkName = "/" +
            (!ciJobId.contains("local_ci_job") ? ciJobId + "_default" : "traefik_webgateway");

    protected DockerInit() {
        dockerHelper = new DockerHelper(docker);
    }

    public void getNetworkId() {
        // get current network id
        networkId = dockerHelper.getNetworkId(networkName);
    }

    /**
     * execute command in container
     * @param containerName - cab_1, cli_1
     * @param commands - ls, pwd, rm
     */
    public String dockerExec(String containerName, String... commands) {
        return dockerHelper.commandExec(
                dockerHelper.getContainerId(containerName),
                commands
        );
    }

}
