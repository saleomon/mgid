package libs.dockerClient.base;

import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;

import static libs.dockerClient.base.Images.mySqlImage;

public class DockerMySql extends DockerInit {

    protected String containerNameMySql = "/" +
            (!ciJobId.contains("local_ci_job") ? ciJobId + "-sok.mysql.intra-1" : "docker-sok.mysql.intra-1");

    public void reStartContainer(){
        String containerMySqlId = dockerHelper.getContainerId(containerNameMySql);
        try {
            docker.stopContainer(containerMySqlId, 0);
            docker.removeContainer(containerMySqlId);
            startMySqlContainer(configureMySqlImage());
        } catch (Exception e) {
            System.err.println("Catch: " + e);
        }
    }

    private ContainerConfig configureMySqlImage() {
        return ContainerConfig.builder()
                .image(mySqlImage + System.getenv("DATABASES_DICTIONARY_BRANCH"))
                .hostname("sok.mysql.intra")
                .env(dockerHelper.getEnvList("mysql"))
                .build();
    }

    private void startMySqlContainer(ContainerConfig config) throws DockerException, InterruptedException {
        final ContainerCreation creation = docker.createContainer(config, containerNameMySql.substring(1));
        System.out.println("creation: " + creation);
        getNetworkId();
        // add cli container to current network id(networkId)
        docker.connectToNetwork(creation.id(), networkId);
        docker.startContainer(creation.id());
    }
}
