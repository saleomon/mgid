package libs.dockerClient.base;

import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import com.spotify.docker.client.messages.RegistryAuth;
import testData.project.CliCommandsList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static com.codeborne.selenide.Selenide.sleep;
import static com.spotify.docker.client.DockerClient.LogsParam.stderr;
import static com.spotify.docker.client.DockerClient.LogsParam.stdout;
import static libs.dockerClient.base.Images.cliImage;

public class DockerCli extends DockerInit{

    public static String cliTag;
    private String containerCliCronId;
    ArrayList<String> consumersId = new ArrayList<>();

    public void pullContainer(){
        System.out.println("pullContainer - start");
        cliTag = getTagBranchFromCab();

        RegistryAuth auth = RegistryAuth.builder().username(System.getenv("GITLAB_CI_TOKEN"))
                .password(System.getenv("CI_JOB_TOKEN"))
                .serverAddress(System.getenv("CI_REGISTRY"))
                .build();
        try {
                docker.pull(cliImage + cliTag, auth);
        } catch (Exception e) {
            if(e.getMessage().contains("Image not found")) {
                System.out.println("There are no image with branch tag - " + System.getenv("BRANCH") + ". Starting pulling image with main tag");
                try {
                    cliTag = "main";
                    docker.pull(cliImage + cliTag, auth);
                } catch (Exception exeption) {
                    exeption.printStackTrace();
                }
            } else {
                e.printStackTrace();
            }
        }
        System.out.println("pullContainer - finish");
    }

    private String getTagBranchFromCab() {
        String cabImageWithTag = "";
        try {
            cabImageWithTag = docker.listContainers().stream().filter(el->el.image().contains("cab:")).findFirst().get().image();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cabImageWithTag.substring(cabImageWithTag.indexOf(":")+1);
    }

    /**
     * run and stop cron
     * @param cronName - cron/consumer name
     * @param additionalCommands - additional commands for run cron/consumer (-c, -r, etc.)
     */
    public void runAndStopCron(CliCommandsList.Cron cronName, String... additionalCommands) {
        final ContainerConfig config;
        try {
            config = configureCliImage(cronName.getCronValue(), additionalCommands);

            containerCliCronId = startCliContainer(config);

            for (int time = 0; time < 60 && docker.listContainers().stream().anyMatch(el-> el.id().contains(containerCliCronId)); time++) {
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * run and stop cron
     * @param cronName - cron/consumer name
     * @param additionalCommands - additional commands for run cron/consumer (-c, -r, etc.)
     */
    public void runCronWithoutStop(CliCommandsList.Cron cronName, String... additionalCommands) {
        final ContainerConfig config;
        try {
            config = configureCliImage(cronName.getCronValue(), additionalCommands);
            containerCliCronId = startCliContainer(config);
            sleep(4000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * run consumer
     * @param consumerName - cron/consumer name
     * @param additionalCommands - additional commands for run cron/consumer (-c, -r, etc.)
     */
    public void runConsumer(CliCommandsList.Consumer consumerName, String... additionalCommands) {
        final ContainerConfig config;
        try {
            config = configureCliImage(consumerName.getConsumerValue(), additionalCommands);
            consumersId.add(startCliContainer(config));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method make stopped consumer by two ways:
     * - by default without parameter action - stop consumer immediately
     * - by action(container log) - waiting for set action when appear it at container log
     */
    public void stopConsumer(String...action){
        try {
            if(action.length > 0) {
                for(String id : consumersId) {
                    for (int time = 0; time < 60 && !getContainerLogs(id).contains(action[0]); time++) {
                        Thread.sleep(1000);
                    }
                    System.out.println(getContainerLogs(id));

                    // Stop container
                    docker.stopContainer(id,1);

                    // Remove container
                    docker.removeContainer(id);
                }
            }
            else {
                for(String id : consumersId) {
                    // get log container
                    System.out.println(getContainerLogs(id));

                    // Stop container
                    docker.stopContainer(id, 1);

                    // Remove container
                    docker.removeContainer(id);
                }
            }
            consumersId.clear();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getContainerLogs(String containerId) throws InterruptedException, DockerException {
        try (LogStream logs = docker.logs(containerId, stdout(), stderr())) {
            return logs.readFully();
        }
    }

    private ContainerConfig configureCliImage(String commandName, String...additionalCommands) {
        return ContainerConfig.builder()
                .image(cliImage + cliTag)
                .env(dockerHelper.getEnvList("cli"))
                .cmd(Stream.concat(Arrays.stream(new String[]{commandName}), Arrays.stream(additionalCommands)).toArray(String[]::new))
                .build();
    }

    private String startCliContainer(ContainerConfig config) throws DockerException, InterruptedException {
        final ContainerCreation creation = docker.createContainer(config);
        System.out.println("creation: " + creation);
        getNetworkId();
        // add cli container to current network id(networkId)
        docker.connectToNetwork(creation.id(), networkId);
        docker.startContainer(creation.id());
        return creation.id();
    }

}
