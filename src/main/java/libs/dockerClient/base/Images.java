package libs.dockerClient.base;

public class Images {
    public static final String cliImage = "registry.mgid.com/marketgid/cli:";
    public static final String mySqlImage = "registry.mgid.com/marketgid/databases:";
}
