package libs.dockerClient.base;

import static com.codeborne.selenide.Selenide.sleep;

public class DockerKafka extends DockerInit{

    protected String containerNameKafka = "/" +
            (!ciJobId.contains("local_ci_job") ? ciJobId + "-kafka-node1.intra-1" : "docker-kafka-node1.intra-1");

    private String containerKafkaId;

    /**
     * /opt/kafka/bin/kafka-topics.sh --list --bootstrap-server kafka-node1.intra:9092 -> list topics
     * /opt/kafka/bin/kafka-consumer-groups.sh --bootstrap-server kafka-node1.intra:9092 --describe --group widgets_recompile_main
     */
    public boolean isAllPartitionsFinished(Topics topicName){
        String[] val;
        String containerKafkaId = dockerHelper.getContainerId(containerNameKafka);

        String kafkaLog = dockerHelper.commandExec(containerKafkaId, "/opt/kafka/bin/kafka-consumer-groups.sh", "--bootstrap-server", "kafka-node1.intra:9092", "--describe", "--group", topicName.getTopicsValue());
        System.out.println("kafkaLog: " + kafkaLog);

        String[] list = kafkaLog.split("\n");
        for(String s : list){
            val = s.split("[| \t\n\r]+");
            if(s.contains(topicName.getTopicsValue()) && !val[4].equals("0")){
                if(!val[4].equals(val[3])) {
                    return false;
                }
            }
        }
        return true;
    }

    private String getContainerKafkaId() {
        containerKafkaId = dockerHelper.getContainerId(containerNameKafka);
        return containerKafkaId;
    }

    public void createTopic(Topics topicName){
        String kafkaLog = dockerHelper.commandExec(containerKafkaId, "/opt/kafka/bin/kafka-topics.sh", "--create", "--topic", topicName.getTopicsValue(), "--partitions", "5", "--replication-factor", "1", "--bootstrap-server", "kafka-node1.intra:9092");
        System.out.println("kafkaLog: " + kafkaLog);

    }

    public void waitUntilAllPartitionsFinished(Topics groupName){
        int count = 0;
        while(!isAllPartitionsFinished(groupName) && count < 180){
            sleep(1000);
            count++;
        }
    }

    public String[] getExistedTopics(){
        return dockerHelper.commandExec(getContainerKafkaId(), "/opt/kafka/bin/kafka-topics.sh", "--bootstrap-server", "kafka-node1.intra:9092", "--list").split("\n");
    }

    public enum Topics {
        WIDGETS_RECOMPILE_MAIN("widgets_recompile_main"),
        NEWS_GENERATE_IMAGE_HASH("news_generate_image_hash"),
        POPULATE_TEASER_TAGS("populate_teasers_tags"),
        GENERATE_IMAGE_HASH("consumer_default_group_id"),
        UPDATE_DUPLICATES_SPELL_ERROR("update_duplicates_spell_error"),
        TEASER_LANDINGS("clients_campaigns_teasers_landings_changed"),
        TEASER_AD_TYPE_MASS("mass_teasers_ad_type_changed"),
        POPULATE_TEASERS_TAGS("populate_teasers_tags"),
        LEMATIZATION_WORD("lemmatization_word"),
        TEASER_DUPLICATE_UPDATE_SPELL_CHECK("teaser_duplicate_update_spell_check"),
        DEVELOPMENT_ROUTE_UTILIZATION("development_route_utilization"),
        OVERALL_LOG("overall_log"),
        BULK_ACTION_UNBLOCK_BATCHES("bulk_actions_batches__teaser__unblock"),
        BULK_ACTION_BLOCK_BATCHES("bulk_actions_batches__teaser__block"),
        BULK_ACTION_MAIN_TOPIC("bulk_actions_main"),
        BULK_ACTION_UPDATE_TITLE("bulk_actions_batches__teaser__update_title_part");

        private final String topicsValue;

        Topics(String statuses) {
            this.topicsValue = statuses;
        }

        static public Topics getType(String pType) {
            for (Topics type : Topics.values()) {
                if (type.getTopicsValue().equals(pType)) {
                    return type;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getTopicsValue() {
            return topicsValue;
        }
    }
}
