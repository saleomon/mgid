package libs.dockerClient.base;

import static com.codeborne.selenide.Selenide.sleep;
import static core.helpers.BaseHelper.getRegExValue;

public class DockerCab extends DockerInit{
    private String responseAfterRequest;

    protected String containerNameCab = "/" +
            (!ciJobId.contains("local_ci_job") ? ciJobId + "-cab-1" : "docker-cab-1");

    int counter = 0;
    public String requestForAuthorizationCode(String userCab)  {
        responseAfterRequest = dockerExec(containerNameCab, "curl",
                        "http://local-oauth.mgid.com:3000/api/v2/authorize",
                        "-X",
                        "POST",
                        "--data",
                        "response_type=code&grant_type=password&client_id=alpha_admin&login=" + userCab +"&password=hHPhMNoNxG&recaptcha=xxxxxx");

        while (getAuthorizationCodeFromResponse() == null && counter < 3) {
            sleep(5);
            counter++;
            requestForAuthorizationCode(userCab);
        }

        return getAuthorizationCodeFromResponse();
    }

    private String  getAuthorizationCodeFromResponse() {
        return getRegExValue(responseAfterRequest, "[A-Z0-9]{48}");
    }

    public String getContainerCabId() {
        try {
            return docker.listContainers().stream().filter(el->el.image().contains("cab:")).findFirst().get().id();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
