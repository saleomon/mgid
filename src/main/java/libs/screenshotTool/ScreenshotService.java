package libs.screenshotTool;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import core.helpers.BaseHelper;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.Coords;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import static testData.project.ResourcePaths.LINK_TO_RESOURCES_SCREENSHOTS;

public class ScreenshotService extends AShot{
    Logger log;
    int diffSizeTrigger = 5;
    String screenshotName;
    Screenshot actualScreenshot;
    Screenshot expectedScreenshot;
    String path = System.getenv("SELENOID_HOST").equalsIgnoreCase("localhost") ? "screenshotContainer/" : "/artifacts/screenshotContainer/";

    public ScreenshotService(Logger log) {
        this.log = log;
    }

    public ScreenshotService setDiffSizeTrigger(int val) {
        diffSizeTrigger = val;
        return  this;
    }

    @Override
    public Screenshot takeScreenshot(WebDriver driver) {
        super.shootingStrategy(ShootingStrategies.viewportPasting(300)).coordsProvider(new WebDriverCoordsProvider());
        actualScreenshot = super.takeScreenshot(driver);
        screenshotName = Thread.currentThread().getStackTrace()[2].getMethodName() + "_" + BaseHelper.getRandomWord(3) + "_";

        new File(path + "actual/").mkdirs();
        try {
            ImageIO.write(actualScreenshot.getImage(), "PNG", new File(path + "actual/" + screenshotName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return actualScreenshot;
    }

    @Override
    public Screenshot takeScreenshot(WebDriver driver, WebElement element) {
        super.shootingStrategy(ShootingStrategies.viewportPasting(300)).coordsProvider(new WebDriverCoordsProvider());
        actualScreenshot = super.takeScreenshot(driver, element);
        screenshotName = Thread.currentThread().getStackTrace()[2].getMethodName() + "_" + BaseHelper.getRandomWord(3);
        new File(path + "actual/").mkdirs();
        try {
            ImageIO.write(actualScreenshot.getImage(), "PNG", new File(path + "actual/" + screenshotName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return actualScreenshot;
    }

    public Screenshot takeScreenshotSimple(WebDriver driver) {
        super.shootingStrategy(ShootingStrategies.simple())
                .coordsProvider(new WebDriverCoordsProvider());
        actualScreenshot = super.takeScreenshot(driver);
        screenshotName = Thread.currentThread().getStackTrace()[2].getMethodName() + "_" + BaseHelper.getRandomWord(3);

        new File(path + "actual/").mkdirs();
        try {
            ImageIO.write(actualScreenshot.getImage(), "PNG", new File(path + "actual/" + screenshotName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return actualScreenshot;
    }

    public void takeScreenshotOfFailedTest(String testName) {
        super.shootingStrategy(ShootingStrategies.viewportPasting(300)).coordsProvider(new WebDriverCoordsProvider());
        actualScreenshot = super.takeScreenshot(WebDriverRunner.getWebDriver());
        new File(path + "failedTests/").mkdirs();
        try {
            ImageIO.write(actualScreenshot.getImage(), "PNG", new File(path + "failedTests/" + testName + "_" + BaseHelper.getRandomWord(3) +".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Screenshot getExpectedScreenshot(String name) {
        try {
            expectedScreenshot = new Screenshot(ImageIO.read(new File(LINK_TO_RESOURCES_SCREENSHOTS + name)));
            expectedScreenshot.setIgnoredAreas(actualScreenshot.getIgnoredAreas());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return expectedScreenshot;
    }

    public boolean compareScreenshots(Screenshot actualScreenshot, Screenshot expectedScreenshot) {
        ImageDiff diff = new ImageDiffer().makeDiff(expectedScreenshot, actualScreenshot).withDiffSizeTrigger(diffSizeTrigger);
        if(diff.hasDiff()) {
            try {
                new File(path + "difference/").mkdirs();
                int amountOfDifferentPixels = diff.getDiffSize();
                ImageIO.write(diff.getMarkedImage(), "PNG", new File(path + "difference/" + screenshotName  + amountOfDifferentPixels + "px_diff.png"));
                log.info(screenshotName  + " actual screenshot has difference with expected in pixels  - " + amountOfDifferentPixels);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return !diff.hasDiff();
    }

    public ScreenshotService addIgnoreElementByCoords(SelenideElement rootElement, String ignoredElementRelativeRoot) {
        rootElement.getShadowRoot().findElements(By.cssSelector(ignoredElementRelativeRoot)).forEach(elem ->
                {
                    int x, y, width, height;
                    x = elem.getLocation().getX();
                    y = elem.getLocation().getY();

                    String croppedWidth  = elem.getCssValue("width");
                    String croppedHeight = elem.getCssValue("height");

                    width = Integer.parseInt(prepareStringForInt(croppedWidth));
                    height = Integer.parseInt(prepareStringForInt(croppedHeight));

                    this.addIgnoredArea(new Coords(x,y,width,height));
                }
        );
        return this;
    }

    private String prepareStringForInt(String value) {
        return value.substring(0, value.contains(".") ? value.indexOf(".") : value.indexOf("px"));
    }

    public ScreenshotService addIgnoreElement(By element) {
        this.addIgnoredElement(element);
        return this;
    }
}
