package core.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static java.time.Duration.ofSeconds;

public class MultiFilterHelper extends BaseHelper {

    public MultiFilterHelper(Logger log) {
        BaseHelper.log = log;
    }

    /**
     * @param baseElementId - base id multiFilter
     * @param dopParam -
     */
    public void expandMultiFilter(String baseElementId, String... dopParam) {
        int count = 0;
        String pathTree = "#" + baseElementId + " span.fancytree-has-children:not(.fancytree-expanded)";
        while ($(pathTree).exists()) {
            $$(pathTree).shouldBe(CollectionCondition.sizeGreaterThan(0), ofSeconds(8));
            ElementsCollection list = $$(pathTree);

            for (SelenideElement s : list) {
                do{
                    if(dopParam.length > 0){
                        s.$(".fancytree-expander").shouldBe(exist).hover().click();
                    }
                    else {
                        s.$(".fancytree-expander").shouldBe(visible).scrollIntoView(true).hover().click();
                    }
                    count++;
                }
                while (!Objects.requireNonNull(s.attr("class")).contains("fancytree-expanded") && count < 3);
                count = 0;

                s.$(".fancytree-expander").shouldBe(visible);
            }
        }
        log.info("expand tree: success");
        sleep(2000);
    }

    public void clickValueInMultiFilter(String... value) {
        int count;
        for (String v : value) {
            count = 0;
            do {
                $x(".//span[span[text()=\"" + v + "\"]]/*[@class='fancytree-checkbox']").shouldBe(exist).scrollIntoView(true).hover().click();
                if (count > 0) sleep(500);
                count++;
            }
            while (!$x(".//span[span[text()=\"" + v + "\"]]").is(attributeMatching("class", ".+fancytree-selected.+")) && count < 10);
        }
    }

    public ArrayList<String> chooseCustomValueInMultiFilterAndGetTheirData(int countValue) {
        int customCategory;
        ArrayList<String> chooseCategoryInEditInterface = new ArrayList<>();

        log.info("отримуємо колекцію усіх елементів дерева");
        String listCheckboxes = ".//li[span[span[@class='fancytree-checkbox']] and not(ul)]//span[@class='fancytree-checkbox']";
        ElementsCollection list = $$x(listCheckboxes);
        ArrayList<Integer> listCategoryId = getRandomCountValue(list.size() - 1, countValue);

        log.info("вибираємо 4 рандомні категорії та записуємо їх у масив");
        for (Integer integer : listCategoryId) {
            list.get(customCategory = integer).shouldBe(visible).hover().click();
            sleep(500);
            String listTitles = ".//li[span[span[@class='fancytree-title']] and not(ul)]//span[@class='fancytree-title']";
            chooseCategoryInEditInterface.add($$x(listTitles).get(customCategory).text());
        }
        return chooseCategoryInEditInterface;
    }

    public List<String> getNameAllSelectedValueInMultiFilter(String baseElementId) {
        List<String>list = new ArrayList<>($$("#" + baseElementId + " .fancytree-selected .fancytree-title").texts());
        list.forEach(i -> log.info("getNameAllSelectedValueInMultiFilter: " + i));
        return list;
    }

    /**
     * open (expand all spans in multi filter (Curator))
     * and click custom data
     */
    public void expandMultiFilterAndClickData(String baseElementId, String value, String... dopParam) {
        expandMultiFilter(baseElementId, dopParam);
        clickValueInMultiFilter(value);
    }

    public void clearAllSelectedCategoryInFilter(SelenideElement element) {
        log.info("clear all category in filter");
        if(convertRgbaToHex(element.getCssValue("background-color")).matches("#0075ff|#df481c")){
            log.info("element.getCssValue(\"background-color\"): " + element.getCssValue("background-color"));
            if(Objects.requireNonNull(element.parent().attr("class")).contains("fancytree-selected")) {
                element.click();
            } else {
                element.doubleClick();
                log.info("doubleClick");
            }
        }
        element.shouldBe(Condition.cssValue("background-color", "rgba(255, 255, 255, 1)"));
        log.info("clear all category success");
    }
}
