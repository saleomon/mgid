package core.helpers;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class HelperLocators {

    public static final String ERRORS_CUSTOM_FIELD = ".errors li";
    public static final SelenideElement GOGOL_ICON = $x(".//a[*[contains(@src, 'cablink.png')]]");
    public static final SelenideElement MGID_CAB_LOGO = $("img[src*='logo_mgid.svg']");
    public static final SelenideElement CONFIRM_BUTTON = $("#confirm-ok");
    public static final SelenideElement CONFIRM_CANCEL = $("#confirm-cancel");
    public static final SelenideElement AJAX_LOADER = $(".ajaxOverlay");
    public static final SelenideElement AJAX_LOADER_POPUP = $(".ajaxOverlayPopup");
    public static final SelenideElement POP_UP = $("div.popup, .popup__confirmation.popup");
    public static final SelenideElement EMERGENCY_MODE_MESSAGE = $("[id='emergencyMode']");
    public static final SelenideElement POP_UP_CLOSE = $(".popup_close");
    public static final SelenideElement DATE_PERIOD_LABEL = $(".calendar");
    public static final SelenideElement DATE_START_INPUT = $("#dateStart");
    public static final SelenideElement DATE_END_INPUT = $("#dateEnd");
    public static final SelenideElement DATE_PERIOD_SELECT = $("#cusel-scroll-dateInterval");
    public static final SelenideElement DATE_PERIOD_SUBMIT = $x(".//*[div[@class='calendar']]/*[text()='Apply' or text()='Применить']");
    public static final SelenideElement PAGINATOR_CAB_SELECT = $("[name=PagintorItemsPerPage]");
    public static final SelenideElement PROFILE_BLOCK = $("[class='user']");



    /////////////////////   Dashboard login page   ////////////////////////
    public static final SelenideElement LOGIN_DASHBOARD_INPUT = $("[id='login']");
    public static final SelenideElement PASS_DASHBOARD_INPUT = $("[id='password']");
    public static final SelenideElement SUBMIT_BUTTON = $("[name='signin']");

    /////////////////////   Dashboard new login page   ////////////////////////
    public static final SelenideElement EMAIL_DASHBOARD_INPUT = $("input[type='email']");
    public static final SelenideElement PASSWORD_DASHBOARD_INPUT = $("input[type='password']");
    public static final SelenideElement SIGNIN_BUTTON = $("button[type='submit']");
}
