package core.helpers.sorted;

import java.util.Random;

public class SortedType {

    public enum SortType {
        ASC("asc"),
        DESC("desc");

        private final String sortValue;

        SortType(String sortValue) {
            this.sortValue = sortValue;
        }

        public String getSortValue() {
            return sortValue;
        }
    }

    public SortType randomPeriod(){
        int t = new Random().nextInt(SortType.values().length);
        return SortType.values()[t];
    }

}
