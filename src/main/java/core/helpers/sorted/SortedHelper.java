package core.helpers.sorted;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import core.helpers.BaseHelper;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;

public class SortedHelper extends BaseHelper {

    String elementsPath = ".//thead[1]//a[@sortby]";

    public SortedHelper(Logger log) {
        BaseHelper.log = log;
    }

    /**
     * choose random or custom column by header <th>
     *
     * @param sort -> asc/desc
     */
    private String chooseSortedElement(SortedType.SortType sort, String... columnName) {
        SelenideElement element;

        // get headers collections
        ElementsCollection elements = $$x(elementsPath);

        String sortType = sort.getSortValue().equals("asc") ? "desc" : "asc";

        // wait 1 header visible and get random header or custom header
        elements.get(1).shouldBe(visible);

        if (columnName.length > 0) element = elements.findBy(attribute("sortby", columnName[0]));
        else element = elements.get(randomNumbersInt(elements.size()));

        //modify href
        String href_attr = element.attr("href");
        String s = Objects.requireNonNull(href_attr).contains("/asc/") ? "/asc/" : "/desc/";
        if (!s.contains(sort.getSortValue())) {
            href_attr = href_attr.replace(s, "/" + sort.getSortValue() + "/");
            setAttributeJS(element, "href", href_attr);
        }

        //set attribute for make active custom column
        setAttributeJS(element, "class", "ups dot " + sortType + " active-sort touch-no-tooltip has-tooltip");

        //click sort header
        element.click();
        waitForAjaxLoader();
        return getTextAndWriteLog(element.attr("sortby"));
    }


    /**
     * get data for current column
     */
    public ElementsCollection getTableColumnData(String columnName) {
        return $$x(".//table//tr[not(@class='totalRow')]//td[count(//thead[1]//th[div//a[@sortby='" + columnName + "']]/preceding-sibling::th)+1][not(contains(@class, 'space'))]");
    }

    public ElementsCollection getTableColumnDataByText(String columnName) {
        return $$x(".//table//tr[not(@class='totalRow')]//td[count(//thead[1]//th[div[contains(text(), '" + columnName + "')]]/preceding-sibling::th)+1][not(contains(@class, 'phpdebugbar-widgets'))]");
    }

    /**
     * проверяем сортировку данных в масиве
     */
    private boolean checkSort(Object[] mass, SortedType.SortType sortType) {
        int count = 0, count_1 = 1;
        log.info("SortType - " + sortType.getSortValue());
        Arrays.asList(mass).forEach(System.out::println);
        //     * по убыванию 1
        //     * по возростанию -1
        int bigDecimalSortType = sortType.getSortValue().equals("desc") ? 1 : -1;

        for (int i = 0; i < mass.length; i++) {
            for (int k = i + 1; k < mass.length; k++) {
                if ((getBigDecimal(mass[i]).compareTo(getBigDecimal(mass[k])) == bigDecimalSortType) | (getBigDecimal(mass[i]).compareTo(getBigDecimal(mass[k])) == 0)) {
                    count++;
                    if (count == (mass.length - (i + 1))) {
                        count_1++;
                        count = 0;
                    }
                }
            }
        }
        return count_1 == mass.length;
    }

    /**
     * принимает Object
     * возвращает BigDecimal
     */
    private BigDecimal getBigDecimal(Object value) {
        BigDecimal ret = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                ret = new BigDecimal((String) value);
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = BigDecimal.valueOf(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
            }
        }
        return ret;
    }

    /**
     * check sort for dashboard - custom or random
     *
     * @param customColumn - id, clicks, wags
     */

    public boolean checkSorted(SortedType.SortType sortType, String... customColumn) {
        // choose sorted column by random header
        String columnName = chooseSortedElement(sortType, customColumn);

        // get column data
        Object[] data = getTableColumnData(columnName).texts().toArray();

        log.info(sortType);
        // check sorted
        return getTextAndWriteLog(checkSort(data, sortType));
    }

    /**
     * check sort for dashboard - new interface (selective at least)
     *
     * @param customColumn - cpc, clicks, spent
     */
    public boolean checkSortedData(SortedType.SortType sortType, Object[] customColumn) {
        return getTextAndWriteLog(checkSort(customColumn, sortType));
    }

    public boolean checkSortedText(SortedType.SortType sortType, String... customColumn) {
        // choose sorted column by random header
        String columnName = chooseSortedElement(sortType, customColumn);

        // get column data
        List<String> data = getTableColumnData(columnName).texts();

        log.info(sortType);
        // check sorted
        return isSorted(data, sortType);
    }

    public boolean isSorted(List<String> list, SortedType.SortType sortType)
    {
        for (int i = 1; i < list.size(); i++) {
            if(sortType == SortedType.SortType.DESC) {
                if (list.get(i - 1).compareTo(list.get(i)) < 0) {
                    return false;
                }
            }
            else {
                if (list.get(i - 1).compareTo(list.get(i)) > 0) {
                    return false;
                }
            }
        }

        return true;
    }

    public Object[] getSortedData(SortedType.SortType sortType, String... customColumn) {
        // choose sorted column by random header
        String columnName = chooseSortedElement(sortType, customColumn);

        // get column data
        return getTableColumnData(columnName).texts().toArray();
    }
}
