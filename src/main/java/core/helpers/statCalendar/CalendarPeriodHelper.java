package core.helpers.statCalendar;

import core.helpers.BaseHelper;
import core.helpers.statCalendar.CalendarForStatistics.CalendarPeriods;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;

public class CalendarPeriodHelper extends BaseHelper {

    private String dateStart, dateEnd;
    private ZoneId zoneId;
    private LocalDate dayNow;
    private LocalDate customDateStart;
    private LocalDate customDateEnd;

    public CalendarPeriodHelper() {
    }


    /**
     * choose custom period in FILTER calendar
     * <p>
     * INTERVAL("interval"),
     * TODAY("today"),
     * YESTERDAY("yesterday"),
     * LAST_SEVEN("lastSeven"),
     * THIS_WEEK("thisWeek"),
     * LAST_WEEK("lastWeek"),
     * LAST_30_DAYS("last30Days"),
     * THIS_MONTH("thisMonth"),
     * LAST_MONTH("lastMonth"),
     * ALL("all");
     */
    public ArrayList<String> getCalendarPeriodDays(CalendarPeriods period) {
        zoneId              = ZoneId.of("Pacific/Honolulu");
        dayNow              = LocalDate.now(zoneId);
        customDateStart     = period.getDateStart();
        customDateEnd       = period.getDateEnd();

        switch (period) {
            case TODAY:
                today();
                break;
            case YESTERDAY:
                yesterday();
                break;
            case LAST_SEVEN:
                lastSeven();
                break;
            case THIS_WEEK:
                thisWeek();
                break;
            case LAST_WEEK:
                lastWeek();
                break;
            case LAST_30_DAYS:
                last30Days();
                break;
            case THIS_MONTH:
                thisMonth();
                break;
            case LAST_MONTH:
                lastMonth();
                break;
            case ALL:
                all();
                break;
            case INTERVAL:
                if (customDateStart == null) {
                    interval();
                } else {
                    custom();
                }
                break;
        }
        return new ArrayList<>(Arrays.asList(dateEnd, dateStart));
    }

    private void today() {
        dateStart = dateFormat(dayNow);
        dateEnd = dateStart;
    }

    private void yesterday() {
        dateStart = dateFormat(dayNow.minusDays(1));
        dateEnd = dateStart;
    }

    private void lastSeven() {
        dateStart = dateFormat(dayNow.minusDays(1));
        dateEnd = dateFormat(dayNow.minusDays(7));
    }

    private void thisWeek() {
        dateStart = dateFormat(dayNow);
        dateEnd = dateFormat(dayNow.with(DayOfWeek.MONDAY));
    }

    private void lastWeek() {
        LocalDate startOfLastWeek = dayNow.minusDays(dayNow.getDayOfWeek().plus(1).ordinal() + 6);
        LocalDate endOfLastWeek = dayNow.minusDays(dayNow.getDayOfWeek().plus(1).ordinal());
        dateStart = dateFormat(endOfLastWeek);
        dateEnd = dateFormat(startOfLastWeek);
    }

    private void last30Days() {
        dateStart = dateFormat(dayNow.minusDays(1));
        dateEnd = dateFormat(LocalDate.now(zoneId).minusDays(30));
    }

    private void thisMonth() {
        dateStart = dateFormat(dayNow);
        dateEnd = dateFormat(dayNow.minusMonths(0).withDayOfMonth(1));
    }

    private void lastMonth() {
        dateStart = dateFormat(dayNow.minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
        dateEnd = dateFormat(dayNow.minusMonths(1).withDayOfMonth(1));
    }

    private void all() {
        dateStart = dateFormat(dayNow);
        dateEnd = "all";
    }

    private void interval() {
        int dayStart = randomNumberFromRangeInt(1, dayNow.getDayOfMonth());
        int dayEnd = randomNumberFromRangeInt(dayStart, dayNow.getDayOfMonth());
        dateStart = dateFormat(dayNow.withDayOfMonth(dayEnd));
        dateEnd = dateFormat(dayNow.withDayOfMonth(dayStart));
    }

    private void custom() {
        dateStart = dateFormat(customDateEnd);
        dateEnd = dateFormat(customDateStart);
    }

    private String dateFormat(LocalDate date) {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd").format(date);
    }
}
