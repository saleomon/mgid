package core.helpers.statCalendar;

import java.time.LocalDate;
import java.util.Random;

public class CalendarForStatistics {

    public enum CalendarPeriods {
        INTERVAL("interval"),
        TODAY("today"),
        YESTERDAY("yesterday"),
        LAST_SEVEN("lastSeven"),
        THIS_WEEK("thisWeek"),
        LAST_WEEK("lastWeek"),
        LAST_30_DAYS("last30Days"),
        THIS_MONTH("thisMonth"),
        LAST_MONTH("lastMonth"),
        ALL("all");

        private final String periodValue;
        private LocalDate dateStart;
        private LocalDate dateEnd;

        CalendarPeriods(String periodValue) {
            this.periodValue = periodValue;
        }

        public String getPeriodValue() {
            return periodValue;
        }

        public CalendarPeriods setDateStart(LocalDate dateStart) {
            this.dateStart = dateStart;
            return this;
        }

        public CalendarPeriods setDateEnd(LocalDate dateEnd) {
            this.dateEnd = dateEnd;
            return this;
        }

        public LocalDate getDateStart() {
            return dateStart;
        }

        public LocalDate getDateEnd() {
            return dateEnd;
        }
    }

    public CalendarPeriods randomPeriod(){
        int t = new Random().nextInt(CalendarPeriods.values().length);
        return CalendarPeriods.values()[t];
    }
}
