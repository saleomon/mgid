package core.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ex.AlertNotFoundException;
import core.helpers.statCalendar.CalendarForStatistics;
import io.qameta.allure.Allure;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.*;

import java.awt.*;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.HelperLocators.*;
import static java.time.Duration.ofSeconds;

public class BaseHelper {

    protected static Logger log;
    DecimalFormat decimalFormat1;
    static DecimalFormat decimalFormat2;
    long nanoTime_start;

    static String clearInputField = Keys.CONTROL + "a" + Keys.CONTROL;

    public BaseHelper() {
        log = LogManager.getLogger(getClass());
        decimalFormat1 = new DecimalFormat("#.#");
        decimalFormat2 = new DecimalFormat("#.##");
    }

    public void deleteVideoIfTestSuccess(URL videoUrl) {
        HttpURLConnection con;
        try {
            log.info("deleteVideoIfTestSuccess");
            do {
                HttpURLConnection.setFollowRedirects(false);
                con = (HttpURLConnection) videoUrl.openConnection();
                con.setRequestMethod("HEAD");
            }
            while (con.getResponseCode() != HttpURLConnection.HTTP_OK);

            Runtime.getRuntime().exec("curl -X DELETE " + videoUrl);
        } catch (Exception e) {
            log.error("Catch deleteVideoIfTestSuccess " + e);
        }
    }

    public static InputStream getSelenoidVideo(URL videoUrl) {
        HttpURLConnection con;
        try {
            do {
                HttpURLConnection.setFollowRedirects(false);
                con = (HttpURLConnection) videoUrl.openConnection();
                con.setRequestMethod("HEAD");
            }
            while (con.getResponseCode() != HttpURLConnection.HTTP_OK);
            return videoUrl.openStream();
        } catch (Exception e) {
            log.info("getSelenoidVideo: " + e.getMessage());
        }
        return null;
    }

    public void attachVideoToAllure(URL videoUrl) {
        try {
            InputStream inputStream = getSelenoidVideo(videoUrl);
            Allure.addAttachment("Video", "video/mp4", inputStream, "mp4");
        } catch (Exception e) {
            log.info("attachVideoToAllure");
            e.printStackTrace();
        }
    }

    /**
     * send keys with total clear input
     */
    public static void sendKey(SelenideElement locator, String value) {
        locator.sendKeys(clearInputField + value);
    }

    public void dragAndDrop(SelenideElement el_why, SelenideElement el_where){
        actions()
                .dragAndDrop(el_why, el_where)
                .build()
                .perform();
    }

    /**
     * get selected value in select list (options/span)
     */
    public String getSelectedValue(SelenideElement element) {
        return element.find(By.className("cuselActive")).attr("val");
    }


    /**
     * Рандомный метод(1-9a-zA-Z) - генерирует случайное слово и возвращает его
     */
    public static String getRandomWord(int t) {
        String AB = "abcdefghijklmnopqrstuvwxyz";

        StringBuilder sb = new StringBuilder(t);

        for (int i = 0; i < t; i++) {
            sb.append(AB.charAt(new Random().nextInt(AB.length())));
        }
        return String.valueOf(sb);
    }

    /**
     * Возвращает случайное число от 0 до бесконечности - разрядность числа передаётся в метод
     */
    public String randomNumbersString(int t) {
        String AB = "123456789";

        StringBuilder sb = new StringBuilder(t);

        for (int i = 0; i < t; i++) {
            sb.append(AB.charAt(new Random().nextInt(AB.length())));
        }
        return String.valueOf(sb);
    }

    /**
     * Возвращает случайное число из диапазона от 0 до заданного числа
     */
    public static int randomNumbersInt(int bound) {
        return new Random().nextInt(bound);
    }

    /**
     * get random value from Array
     */
    public String getRandomFromArray(String[] mass) {
        return mass[new Random().nextInt(mass.length)];
    }

    /**
     * get random value from List
     */
    public String getRandomFromList(List<String> mass) {
        return mass.get(new Random().nextInt(mass.size()));
    }

    /**
     * get random value from Array
     */
    public static int getRandomFromArray(int[] mass) {
        return mass[new Random().nextInt(mass.length)];
    }

    /**
     * get random value from Array
     */
    public boolean getRandomFromArray(boolean[] mass) {
        return mass[new Random().nextInt(mass.length)];
    }

    /**
     * Возвращает случайное число в указанном диапазоне от и до
     */
    public static String randomNumberFromRange(int from, int till) {
        return String.valueOf(from + (int) (Math.random() * ((till - from) + 1)));
    }

    /**
     * Возвращает случайную дату
     */
    public Date randomDate() {
        long ms = -946771200000L + (Math.abs(new Random().nextLong()) % (70L * 365 * 24 * 60 * 60 * 1000));
        return new Date(ms);
    }

    /**
     * Меняет формат записи даты и возвращает строку
     */
    public String dateFormat(Date date, String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }


    /**
     * Возвращает случайное число в указанном диапазоне от и до
     */
    public static int randomNumberFromRangeInt(int from, int till) {
        return from + (int) (Math.random() * ((till - from) + 1));
    }

    /**
     * Возвращает набор случайных неповторяющихся чисел
     * Диапазон от 0 до list_size. Второй параметр количество случайных чисел
     *
     * @param list_size   - размер коллекции/массива
     * @param count_value - количество случайных чисел
     * @return - возвращаем массив случайніх чисел
     */
    public ArrayList<Integer> getRandomCountValue(int list_size, int count_value) {
        ArrayList<Integer> mass = new ArrayList<>();
        int count = 0, t;
        if (list_size > 0) {
            do {
                t = Integer.parseInt(randomNumberFromRange(0, list_size - 1));
                if (mass.size() < 1) {
                    mass.add(t);
                    count++;
                } else if (!mass.contains(t)) {
                    mass.add(t);
                    count++;
                }
            } while (count != count_value);
        }
        return mass;
    }

    public String[] getRandomValuesFromList(List<String> values, int count_value) {
        ArrayList<String> listWithFilledData = new ArrayList<>();
        getRandomCountValue(values.size(), count_value).forEach(i -> listWithFilledData.add(values.get(i)));
        return listWithFilledData.toArray(new String[count_value]);
    }

    /**
     * парсим string to Integer
     */
    public Integer parseInt(String element) {
        return element != null ?
                Integer.parseInt(element.replace(" ", "")) :
                null;
    }

    /**
     * парсим WebElement to Integer
     */
    public Integer parseInt(WebElement element) {
        return Integer.parseInt($(element).innerText().replace(" ", "").replace("\n", ""));
    }

    public Integer getNumberValueFromCell(WebElement element) {
        return Integer.parseInt($(element).innerText().replaceAll("\\(*[0-9]*[0-9]?\\.*[0-9]*[0-9]%\\)", "").replace(" ", ""));
    }

    /**
     * сетим value елементу через JS
     */
    public void setAttributeValueJS(SelenideElement element, String value) {
        executeJavaScript("arguments[0].value='" + value + "'", element);
    }

    /**
     * сетим value елементу через JS
     */
    public void setAttributeJS(SelenideElement element, String attr_name, String attr_value) {
        executeJavaScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element,
                attr_name,
                attr_value);
    }

    /**
     * очищаем поле и устанавливаем новое значение
     */
    public static void clearAndSetValue(SelenideElement locator, String value) {
        locator.setValue(value);
    }

    /**
     * select custom value in SelectList
     */
    public String selectCustomValue(WebElement id, String value) {
        $(id).selectOptionByValue(value);
        return value;
    }

    /**
     * select custom value in SelectList (return text)
     */
    public String selectCustomValueReturnText(WebElement id, String value) {
        SelenideElement select = $(id);
        select.selectOptionByValue(value);
        return select.getSelectedText();
    }

    /**
     * select custom text in SelectList
     */
    public String selectCustomText(WebElement id, String text) {
        $(id).selectOptionContainingText(text);
        return text;
    }

    /**
     * select random element in Select and return Value
     */
    public String selectRandomValue(SelenideElement id) {
        ElementsCollection options = id.findAll("option:not([selected]):not([value='']):not([value='all']):not([value='0']):not([value='-1']):not([style*='none'])");

        /* check select size == 0 -> return selected text */
        int size = options.size();
        if (size == 0) {
            return id.getSelectedValue();
        }

        // check select size > 0 -> get and selected random value -> return text
        String value = options.get(randomNumbersInt(size)).val();
        id.selectOptionByValue(value);

        return id.getSelectedValue();
    }

    /**
     * select random element in Select and return TEXT
     */
    public String selectRandomText(WebElement id) {
        SelenideElement select = $(id);
        ElementsCollection options = select.findAll("option:not([selected]):not([value=''])");

        // check select size == 0 -> return selected text
        int size = options.size();
        if (size == 0) {
            return select.getSelectedText();
        }

        // check select size > 0 -> get and selected random value -> return text
        String value = options.get(randomNumbersInt(size)).val();
        select.selectOptionByValue(value);

        return getTextAndWriteLog(select.getSelectedText());
    }

    String optionsRemoveIconForMultiSelect, optionsForMultiSelect;
    SelenideElement selectForMultiSelect, selectLabelForMultiSelect;

    public ArrayList<String> selectCustomValuesForMultiSelect(String elementId, List<String> values){
        ArrayList<String> bundles = new ArrayList<>();

        helperForMultiSelect(elementId);

        for(String val : values){
            if (!selectForMultiSelect.exists()) selectLabelForMultiSelect.click();
            bundles.add(val);
            $("#select2-" + elementId + "-results>li[id$='" + val + "']").hover().click();
        }

        return bundles;
    }

    public ArrayList<String> selectRandomValuesForMultiSelect(String elementId, int amountRandomElements){
        int size;
        ArrayList<String> bundles = new ArrayList<>();

        helperForMultiSelect(elementId);

        size = $$(optionsForMultiSelect).size();
        ArrayList<Integer> data = getRandomCountValue(size - 1, amountRandomElements);
        ElementsCollection list = $$(optionsForMultiSelect);
        for (int d : data) {
            if (!selectForMultiSelect.exists()) selectLabelForMultiSelect.click();
            bundles.add(list.get(d).text());
            list.get(d).hover().click();
        }

        return bundles;
    }

    private void helperForMultiSelect(String elementId){
        optionsRemoveIconForMultiSelect = ".//*[select[@id='" + elementId + "']]//span[contains(@class, '__remove')]";
        optionsForMultiSelect = "#select2-" + elementId + "-results>li";
        selectForMultiSelect = $("#select2-" + elementId + "-results");
        selectLabelForMultiSelect = $x(".//*[select[@id='" + elementId + "']]//li[contains(@class, 'search')]");

        //clear old bundles
        $$x(optionsRemoveIconForMultiSelect).asFixedIterable().forEach(
                i -> $$x(optionsRemoveIconForMultiSelect).get(0).click()
        );

        if(!selectForMultiSelect.exists()) selectLabelForMultiSelect.click();
        $$(optionsForMultiSelect).shouldBe(CollectionCondition.sizeGreaterThan(0));
    }

    public boolean checkValuesForMultiSelect(String elementId, List<String> bundles) {
        String BUNDLE_SELECTED_ELEMENTS = ".select2-selection__rendered>li[data-select2-id]:not(span)";

        List list = $("#" + elementId).parent().$$(BUNDLE_SELECTED_ELEMENTS)
                .texts()
                .stream()
                .map(i -> i.replace("×", ""))
                .collect(Collectors.toList());

        return  getTextAndWriteLog(new HashSet<>(bundles).containsAll(list));
    }

    /**
     * select random element for select in has HIDE options
     */
    public void selectCustomValInHideSelectOptions(WebElement id, String tier) {
        int index = 0;
        SelenideElement select = $(id);
        ElementsCollection options = select.findAll("option:not([selected]):not([value=''])");

        for (int i = 0; i < options.size(); i++) {
            if (Objects.equals(options.get(i).val(), tier)) {
                index = i;
                break;
            }
        }

        executeJavaScript("arguments[0].selectedIndex = " + index, select);
    }

    /**
     * select random element for select in has HIDE options
     */
    public String selectRandomTextInHideSelectOptions(WebElement id) {
        SelenideElement select = $(id);
        ElementsCollection options = select.findAll("option:not([selected]):not([value=''])");

        // check select size == 0 -> return selected text
        int size = options.size();
        if (size == 0) {
            return options.get(0).val();
        }

        // check select size > 0 -> get and selected random value -> return text
        int value = randomNumbersInt(size);
        executeJavaScript("arguments[0].selectedIndex = " + value, select);

        return getTextAndWriteLog(options.get(value).val());
    }

    /**
     * Выбирает опцию из селектора, состоящего из "<p>"
     * Если параметр customOption не передан, выбирается случайная опция
     * В параметр customOption надо передавать нужное значение data-id
     */
    public String selectValueFromParagraphListJs(SelenideElement element, String... customOption) {
        element.shouldBe(exist);
        ElementsCollection list = element.findAll(By.cssSelector("p[data-id][data-has-children='0']"));
        String val = customOption.length > 0 ?
                customOption[0] :
                list.get(randomNumbersInt(list.size())).attr("data-id");

        executeJavaScript("arguments[0].click()", element.find(By.cssSelector("[data-id='" + val + "']")));

        return getTextAndWriteLog(val);
    }

    /**
     * select current value in SelectList(SelenideElement) JS
     */
    public String selectCurrentValInSelectListJS(SelenideElement element, String type) {
        executeJavaScript("arguments[0].click()", element.find(By.cssSelector("[val='" + type + "']")));
        return type;
    }

    /**
     * select current value in SelectList(SelenideElement) JS
     */
    public String selectCurrentValInSelectListJSWithoutWFA(SelenideElement element, String type) {
        executeJavaScript("arguments[0].click()", element.find(By.cssSelector("[val='" + type + "']")));
        return type;
    }

    /**
     * select random value in selectList for Dashboard
     */
    public String selectRandomValSelectListDashJs(SelenideElement element) {
        ElementsCollection list = element.shouldBe(exist).findAll(By.cssSelector("span:not([style*='none']):not(.cuselActive)"));
        String value = list.get(randomNumbersInt(list.size())).attr("val");
        while (Objects.requireNonNull(value).equalsIgnoreCase("")) {
            value = list.get(randomNumbersInt(list.size())).attr("val");
        }
        executeJavaScript("arguments[0].click()", element.find(By.cssSelector("[val='" + value + "']")));
        return value;
    }

    /**
     * choose custom period in calendar
     * if set interval period - fill start and end date
     */
    public void selectCalendarPeriodJs(CalendarForStatistics.CalendarPeriods period, ArrayList<String> periods) {
        DATE_PERIOD_LABEL.shouldBe(visible).click();
        DATE_END_INPUT.shouldBe(visible);
        executeJavaScript("arguments[0].click()", DATE_PERIOD_SELECT.find(By.cssSelector("span[val='" + getTextAndWriteLog(period.getPeriodValue()) + "']")));
        if (period.getPeriodValue().equals("interval")) {
            String format = "dd.MM.yyyy";
            String dateStart = changeDateFormat(periods.get(0), format);
            String dateEnd = changeDateFormat(periods.get(1), format);
            DATE_START_INPUT.val(dateStart);
            DATE_END_INPUT.val(dateEnd);
        }
        clickInvisibleElementJs(DATE_PERIOD_SUBMIT);
        waitForAjax();
    }

    private String changeDateFormat(String date, String format) {
        return getTextAndWriteLog(DateTimeFormatter.ofPattern(format).format(LocalDate.parse(date)));
    }

    /**
     * метод проверяет что в селектлисте(Каб) выбран елемент с [selected="selected"]
     */
    public boolean checkSelectedCurrentValInSelectListForCabJs(SelenideElement element, String value) {
        return executeJavaScript("return arguments[0].getAttribute('selected');", element.find(By.cssSelector("[value='" + value + "']"))) != null;
    }

    /**
     * метод проверяет что в селектлисте(Дешборд) выбран елемент с [class='cuselActive']
     */
    public boolean checkSelectedCurrentValInSelectListForDashJs(SelenideElement element, String type) {
        return executeJavaScript("return arguments[0].getAttribute('class');", element.find(By.cssSelector("[val='" + type + "']"))) != null &&
                Objects.requireNonNull(executeJavaScript("return arguments[0].getAttribute('class');", element.find(By.cssSelector("[val='" + type + "']")))).equals("cuselActive");
    }

    /**
     * получаем css style который находится после :before(::before)
     */
    public String getCssValueBeforeJs(SelenideElement element, String style) {
        return Objects.requireNonNull(executeJavaScript("return window.getComputedStyle(arguments[0], ':before').getPropertyValue('" + style + "');", element)).toString();
    }

    /**
     * method make checkbox selected or not
     */
    public static void markUnMarkCheckbox(boolean state, SelenideElement locator) {
        if (state) {
            if (!locator.shouldBe(visible).hover().isSelected()) {
                clickInvisibleElementJs(locator);
            }
        } else {
            if (locator.shouldBe(visible).hover().isSelected()) {
                clickInvisibleElementJs(locator);
            }
        }
    }

    /**
     * method check state of checkbox
     */
    public static boolean checkIsCheckboxSelected(boolean state, SelenideElement locator) {
        if (state) {
            if (!locator.isSelected()) {
                log.info("Checkbox should be selected. Current state - not selected. Locator of element - " + locator.getAttribute("id"));
                return false;
            }
            log.info("Checkbox is selected. Locator - " + locator.getAttribute("id"));
        } else {
            if (locator.isSelected()) {
                log.info("Checkbox shouldn't be selected. Current state - selected. Locator of element - " + locator.getAttribute("id"));
                return false;
            }
            log.info("Checkbox isn't selected. Locator - " + locator.getAttribute("id"));
        }
        return true;
    }

    /**
     * method check disabled state of element
     */
    public static boolean checkIsElementDisabled(boolean state, SelenideElement locator) {
        if (state && !locator.isEnabled()) {
            log.info("Element is disabled. Locator -> " + locator.getAttribute("id"));
            return true;
        } else if (!state && locator.isEnabled()) {
            log.info("Element is enabled. Locator -> " + locator.getAttribute("id"));
            return true;
        } else
        return false;
    }

    /**
     * method check state of element
     */
    public static boolean checkIsElementSelected(boolean state, SelenideElement locator) {
        if (state && locator.isSelected()) {
            log.info("Element is selected. Locator -> " + locator.getAttribute("id"));
            return true;
        } else if (!state && !locator.isSelected()) {
            log.info("Element is not selected. Locator -> " + locator.getAttribute("id"));
            return true;
        } else
        return false;
    }

    /**
     * get value and clear all except regEx
     */
    public static String getRegExValue(String value, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            return value.substring(start, end);
        }
        return null;
    }


    /**
     * click element(SelenideElement) JS
     */
    public static void clickInvisibleElementJs(SelenideElement element) {
        String script = "var object = arguments[0];"
                + "var theEvent = document.createEvent(\"MouseEvent\");"
                + "theEvent.initMouseEvent(\"click\", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);"
                + "object.dispatchEvent(theEvent);";
        executeJavaScript(script, element);
    }

    /**
     * возвращает случайное число из диапазона double from, double till - с одним знаком после запятой(.#)
     */
    public String randomNumberDouble1CharAfterDot(double from, double till) {
        return decimalFormat1.format(Double.parseDouble(String.valueOf(from + Math.random() * ((till - from))).replace(",", "."))).replace(",", ".");
    }

    /**
     * возвращает случайное число из диапазона double from, double till - с двумя знаками после запятой(.##)
     */
    public String randomNumberDouble2CharsAfterDot(double from, double till) {
        return decimalFormat2.format(Double.parseDouble(String.valueOf(from + Math.random() * ((till - from))).replace(",", ".")));
    }

    /**
     * метод для Дешборда
     * возвращает есть ли у елемента classDisCusel (задизейблен ли елемент)
     */
    public boolean checkStateOfDisplayed(SelenideElement element, boolean state) {
        return checkDataset(state, Objects.requireNonNull(element.attr("class")).contains("classDisCusel"));
    }

    /**
     * проверяем есть ли елемент на странице
     */
    public boolean checkStateOfExists(SelenideElement element, boolean state) {
        return checkDataset(state, element.exists());
    }

    public static JSONObject stringToJson(String str) {
        JSONParser parser = new JSONParser();
        try {
            return ((JSONObject) parser.parse(str));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * get text and write log this
     */
    public String getTextAndWriteLog(String text) {
        log.info(getMethodCallName() + " -> '" + text + "'");
        return text != null ? text.trim() : null;
    }

    /**
     * get text and write log this
     */
    public Integer getTextAndWriteLog(Integer value) {
        log.info(getMethodCallName() + " -> " + value);
        return value;
    }

    /**
     * get text and write log this
     */
    public Double getTextAndWriteLog(Double value) {
        log.info(getMethodCallName() + " -> " + value);
        return value;
    }

    /**
     * get text and write log this
     */
    public Long getTextAndWriteLog(Long value) {
        log.info(getMethodCallName() + " -> " + value);
        return value;
    }

    /**
     * get text and write log this
     */
    public boolean getTextAndWriteLog(boolean state) {
        log.info(getMethodCallName() + " -> " + state);
        return state;
    }

    /**
     * get text and write log this
     */
    public int getTextAndWriteLog(int value) {
        log.info(getMethodCallName() + " -> " + value);
        return value;
    }

    /**
     * метод проверяет погрешность между двумя числами
     * 1 и 2 параметр числа которые нужно проверить
     * 3 - допустимая погрешность
     */
    public static boolean comparisonData(double first, double second, double error) {
        if ((first - second <= error && first - second >= 0.00) |
                (second - first <= error && second - first >= 0.00)) return true;
        log.info(first + "!=" + second + ", error: " + error);
        return false;
    }

    /**
     * метод проверяет погрешность между двумя числами
     * 1 и 2 параметр числа которые нужно проверить
     * 3 - допустимая погрешность
     */
    public boolean comparisonData(BigDecimal first, BigDecimal second, BigDecimal error) {
        BigDecimal directSubtraction = first.subtract(second);
        BigDecimal reverseSubtraction = second.subtract(first);
        if (((directSubtraction.compareTo(error) <= 0) && directSubtraction.compareTo(BigDecimal.valueOf(0)) >= 0) |
                (reverseSubtraction.compareTo(error) <= 0 && reverseSubtraction.compareTo(BigDecimal.valueOf(0.00)) >= 0.00))
            return true;
        log.info(first + "!=" + second + ", error: " + error);
        return getTextAndWriteLog(false);
    }

    /**
     * метод проверяет погрешность между двумя числами
     * 1 и 2 параметр числа которые нужно проверить
     * 3 - допустимая погрешность
     */
    public boolean comparisonData(int first, int second, int error) {
        if ((first - second <= error && first - second >= 0) |
                (second - first <= error && second - first >= 0)) return true;

        log.info(first + "!=" + second + ", error: " + error);
        return getTextAndWriteLog(false);
    }


    /**
     * парсим string to double
     */
    public static Double parseDouble(String element) {
        return Double.parseDouble(element.replace(",", ".").replace(" ", ""));
    }

    public static Double parseDouble(WebElement element) {
        String val = $(element).innerText();
        if (val.equals("0")) return 0.0;

        return Double.parseDouble(val
                .replace(",", ".")
                .replaceAll(" ", "")
                .replace("\u00A0", ""));
    }

    public static Double formatDouble(Double element) {
        return Double.parseDouble(decimalFormat2.format(element).replace(",", "."));
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * help method for switch on/off tumbler
     */
    public static void switchTumblerInDash(boolean isSwitchOn, SelenideElement tumbler) {
        if ((isSwitchOn && tumbler.text().equalsIgnoreCase("off")) ||
                (!isSwitchOn && tumbler.text().equalsIgnoreCase("on"))) {
            clickInvisibleElementJs(tumbler);
        }
    }

    /**
     * метод возвращает имя метода в котором был вызыван хелпер
     */
    public String getMethodCallName() {
        return Thread.currentThread().getStackTrace()[3].getMethodName();
    }

    /**
     * метод возвращает имя метода в котором был вызыван хелпер
     */
    public String getMethodCallNameLog() {
        return Thread.currentThread().getStackTrace()[4].getMethodName();
    }


    /**
     * Get actual and expected results and write current to log for boolean
     */
    public boolean writeStateToLog(boolean state, boolean expectedResult, boolean actualResult) {
        log.info(getMethodCallNameLog() + " -> " + state + " (Exp = " + expectedResult + ", Act = " + actualResult + ")");
        return state;
    }

    /**
     * Get actual and expected results and write current to log for String
     */
    public boolean writeStateToLog(boolean state, String expectedResult, String actualResult) {
        log.info(getMethodCallNameLog() + " -> " + state + " (Exp = " + expectedResult + ", Act = " + actualResult + ")");
        return state;
    }

    /**
     * Get actual and expected results and write current to log for Integer
     */
    public boolean writeStateToLog(boolean state, Integer expectedResult, Integer actualResult) {
        log.info(getMethodCallNameLog() + " -> " + state + " (Exp = " + expectedResult + ", Act = " + actualResult + ")");
        return state;
    }

    /**
     * Get actual and expected results and write current to log for Double
     */
    public boolean writeStateToLog(boolean state, Double expectedResult, Double actualResult) {
        log.info(getMethodCallNameLog() + " -> " + state + " (Exp = " + expectedResult + ", Act = " + actualResult + ")");
        return state;
    }


    /**
     * Compare expected and actual results for boolean
     */
    public boolean checkDataset(boolean expectedResult, boolean actualResult) {
        return expectedResult == actualResult || writeStateToLog(false, expectedResult, actualResult);
    }

    /**
     * Compare expected and actual results for String equals
     */
    public boolean checkDatasetEquals(String expectedResult, String actualResult) {
        return (actualResult.equals(expectedResult) || actualResult.equalsIgnoreCase(expectedResult)) || writeStateToLog(false, expectedResult, actualResult);
    }

    /**
     * Compare expected and actual results for String contains
     */
    public boolean checkDatasetContains(String expectedResult, String actualResult) {
        return (actualResult.contains(expectedResult) || expectedResult.contains(actualResult)) || writeStateToLog(false, expectedResult, actualResult);
    }

    /**
     * Compare expected and actual results for Integer
     */
    public boolean checkDataset(Integer expectedResult, Integer actualResult) {
        return expectedResult.equals(actualResult) || writeStateToLog(false, actualResult, expectedResult);
    }

    /**
     * Compare expected and actual results for double
     */
    public boolean checkDataset(Double expectedResult, Double actualResult) {
        return expectedResult.equals(actualResult) || writeStateToLog(false, expectedResult, actualResult);
    }

    /**
     * get string by reg expression
     */
    public static String prepareStringByRegExr(String regExr, String rawStrinfg) {
        Pattern pattern = Pattern.compile(regExr, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(rawStrinfg);

        return urlMatcher.find() ? rawStrinfg.substring(urlMatcher.start(0), urlMatcher.end(0)) : "";
    }

    public static void waitForAjaxVisible() {
        AJAX_LOADER.shouldBe(visible);
    }

    /**
     * ожидаем завершения отработки всех ajax-запросов на странице
     */
    public static void waitForAjax() {
        int count = 0;
        String gifLocator = "img[src*='ajax-loader.gif']";
        try {
            do {
                if (AJAX_LOADER.isDisplayed() || $$(gifLocator).filter(visible).size() > 0 ||
                        !((Long) executeJavaScript("return jQuery.active") == 0) ||
                        !Objects.requireNonNull(executeJavaScript("return document.readyState")).toString().equals("complete")) {
                    sleep(500);
                    count++;
                }
            }
            while ((AJAX_LOADER.isDisplayed() || $$(gifLocator).filter(visible).size() > 0) && count < 40);
        } catch (Exception e) {
            System.err.println("Catch " + e);
        }
    }

    public static void waitForAjaxPopup() {
        int count = 0;
        try {
            do {
                if (AJAX_LOADER_POPUP.isDisplayed()) {
                    sleep(500);
                    count++;
                }
            }
            while (AJAX_LOADER_POPUP.isDisplayed() && count < 20);
        } catch (Exception e) {
            System.err.println("Catch " + e);
        }
    }

    /**
     * ожидаем завершения отработки всех ajax-запросов на странице
     */
    public static void waitForAjaxLoader() {
        int count = 0;
        SelenideElement ajax = $("#loaderGif,.ajaxLoader,#loading-popup,#loading,#overlay,#load-image");
        try {
            do {
                if (ajax.isDisplayed() ||
                        !((Long) executeJavaScript("return jQuery.active") == 0) ||
                        !Objects.requireNonNull(executeJavaScript("return document.readyState")).toString().equals("complete")) {
                    sleep(500);
                    count++;
                }
            }
            while (ajax.isDisplayed() && count < 60);
        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }

    /**
     * ждём пока елемент не появится на странице
     * макс - 10с
     * метод принимает SelenideElement element который ожидаем
     */
    public void waitDisplayedElement(SelenideElement element) {
        log.info("wait displaying of element");
        int time = 0;
        try {
            do {
                if (!element.exists() || !element.isDisplayed()) {
                    sleep(500);
                    refresh();
                    time++;
                    log.info("waitDisplayedElement -> element don't visible - waits");
                }
            }
            while (!element.exists() && time < 20);
        } catch (Exception e) {
            log.error("Catch " + e);
            log.error("element isn't displayed");
        }
        log.info("element is displayed");
    }

    /**
     * ждём пока елемент  исчезнет на странице
     * макс - 10с
     * метод принимает SelenideElement element который должен исчезнуть
     */
    public boolean waitDisappearingElement(SelenideElement element) {
        int time = 0;
        try {
            do {
                if (element.exists()) {
                    sleep(500);
                    refresh();
                    time++;
                    log.info("waitDisplayedElement -> element don't visible - waits");
                }
            }
            while (element.exists() && time < 20);
        } catch (Exception e) {
            log.error("Catch " + e);
            log.error("element isn't displayed");
        }
        getTextAndWriteLog(!element.exists());
        getTextAndWriteLog(!element.isDisplayed());
        return !element.isDisplayed();
    }

    /**
     * click confirm button in all popUp for dashboard
     */
    public static void isConfirmDisplayed() {
        if (CONFIRM_BUTTON.exists()) {
            sleep(300);
            CONFIRM_BUTTON.click();
            log.info("click confirm button");
            waitForAjax();
        }
    }

    /**
     * click confirm button in all popUp for dashboard
     */
    public void amountOfDisplayedEntitiesPerPage(String option) {
        if (PAGINATOR_CAB_SELECT.isDisplayed()) {
            PAGINATOR_CAB_SELECT.selectOptionByValue(option);
            waitForAjax();
        }
    }

    /**
     * click confirm button in all popUp for dashboard without waiting for Ajax
     */
    public void isConfirmDisplayedWithoutWFA() {
        if (CONFIRM_BUTTON.exists()) {
            CONFIRM_BUTTON.shouldBe(visible).click();
            log.info("click confirm button");
        }
    }

    public void clickConfirmCancel() {
        CONFIRM_CANCEL.shouldBe(visible, ofSeconds(10)).click();
    }

    /**
     * helper for check can't edit field (title, description, landing, url) inline
     */
    public boolean checkCantEditFieldInline(SelenideElement inlineLabel, SelenideElement inputOrSelect) {
        inlineLabel.shouldBe(visible).doubleClick();
        waitForAjax();
        return checkDatasetEquals(inlineLabel.attr("data-edit-allow"), "0") &&
                !inputOrSelect.exists() | !inputOrSelect.isDisplayed();
    }


    /**
     * Конвертирует RGBA в HEX, возвращает String HEX цвет
     */
    public static String convertRgbaToHex(String s) {
        String s1 = s.substring(s.indexOf('(') + 1, s.indexOf(')')).replace(" ", "");
        String[] s2 = s1.split(",");
        Color color = new Color(Integer.parseInt(s2[0]), Integer.parseInt(s2[1]), Integer.parseInt(s2[2]));
        String hex = Integer.toHexString(color.getRGB() & 0xffffff);
        if (hex.length() > 2 && hex.length() < 6 && !hex.equals("2a7b1")) {
            hex = "00" + hex;
        } else if (hex.length() <= 2) {
            hex = "00000" + hex;
        } else if (hex.equals("2a7b1")) {
            hex = "0" + hex;
        } else {
            return "#" + convertRegularHexToShorthand(hex);
        }
        return "#" + convertRegularHexToShorthand(hex);
    }

    /**
     * Convert regular HEX format into Shorthand HEX, if needed
     */
    public static String convertRegularHexToShorthand(String colorValue) {
        char[] arrayFromString = colorValue.toCharArray();
        if ((arrayFromString[0] == arrayFromString[1]) && (arrayFromString[2] == arrayFromString[3]) && (arrayFromString[4] == arrayFromString[5])) {
            return arrayFromString[0] + String.valueOf(arrayFromString[2]) + arrayFromString[4];
        }
        return colorValue;
    }

    /**
     * check show alert, if alert true - alert confirm
     */
    public boolean checkAlertAndClose() {
        try {
            switchTo().alert().accept();
            log.info("alert -> present");
            return true;
        } catch (AlertNotFoundException e) {
            log.info("alert isn't present");
            return false;
        } catch (UnhandledAlertException e) {
            log.info("alert caught UnhandledAlertException exception");
            checkAlertAndClose();
            return false;
        }
    }

    /**
     * получаем текст с alert
     * и подтверждаем его
     */
    public String getTextFromAlert() {
        try {
            Alert alert = switchTo().alert();
            log.info("alert -> present");
            String text = alert.getText();
            alert.accept();
            return text;
        } catch (AlertNotFoundException e) {
            log.info("alert isn't present");
            return null;
        } catch (UnhandledAlertException e) {
            log.info("unexpected alert open");
            return null;
        }
    }


    /**
     * Checking if CSS class for web element input is present
     */
    public boolean checkErrorSelectionField(String locator, String... isEditInterface) {
        if (isEditInterface.length > 0) {
            $(".popup #" + locator + "[class*='incorrect']").shouldBe(visible);
        } else {
            $("#" + locator + "[class*='incorrect']").shouldBe(visible);
        }
        log.info("Error selection is displayed. Field locator - " + locator);
        return true;
    }

    /**
     * Wait for Ajax call to finish
     */
    public void waitForReadyPage() {
        executeJavaScript("return jQuery.active == 0");
    }

    /**
     * universal method for close all popUp form in Dashboard
     */
    public void closePopup(SelenideElement... element) {
        int count = 0;
        POP_UP_CLOSE.shouldBe(visible, ofSeconds(12));

        SelenideElement popUpClose = element.length > 0 ?
                element[0] :
                POP_UP_CLOSE;
        try {
            do {
                if (popUpClose.isDisplayed()) {
                    popUpClose.shouldBe(visible, ofSeconds(8)).click();
                } else {
                    sleep(500);
                    count++;
                }
            }
            while (popUpClose.isDisplayed() && count < 30);
            POP_UP.shouldBe(hidden, ofSeconds(12));
        } catch (Exception e) {
            log.error("Catch " + e);
        }
        log.info("closePopup - OK");
    }

    public String getPopupText() {
        return getTextAndWriteLog(POP_UP.shouldBe(visible).text());
    }

    /**
     * refresh current page
     */
    public void refreshCurrentPage() {
        refresh();
    }

    /**
     * is shown emergency mode error cab
     */
    public boolean isEmergencyModePresentCab() {
        return EMERGENCY_MODE_MESSAGE.exists();
    }

    /**
     * выбираем период в календаре - передаём int период
     */
    public void selectCalendarPeriodJs(int period) {
        DATE_PERIOD_SELECT.shouldBe(exist);
        executeJavaScript("arguments[0].click()", DATE_PERIOD_SELECT.findElements(By.tagName("span")).get(period));
        clickInvisibleElementJs(DATE_PERIOD_SUBMIT);
        waitForAjax();
    }

    /**
     * select current text in SelectList(SelenideElement) JS
     */
    public void selectCurrentTextInSelectListJS(SelenideElement element, String type) {
        executeJavaScript("arguments[0].click()", element.find(By.xpath(
                ".//span[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '"
                        + type.toLowerCase() + "')]")));
    }

    /**
     * Returns a list with all links contained in the input
     */
    public ArrayList<String> extractUrls(String text) {
        ArrayList<String> containedUrls = new ArrayList<>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }

    /**
     * Получаем сумму елементов List from double values
     */
    public Double getSumDouble(ElementsCollection collectionDouble) {
        return collectionDouble.texts().stream().mapToDouble(el -> Double.parseDouble(el.trim().replace(" ", ""))).sum();
    }

    /**
     * Получаем сумму елементов List from integer values
     */
    public Integer getSumInteger(ElementsCollection collectionDouble) {
        return collectionDouble.texts().stream().mapToInt(el -> Integer.parseInt(el.trim())).sum();
    }

    public boolean alertIsShow() {
        Alert alert = null;
        try {
            alert = switchTo().alert();
        } catch (AlertNotFoundException e) {
            log.info(e);
        }
        return alert != null;
    }

    public void timerStart() {
        nanoTime_start = System.nanoTime();
    }

    public boolean finishAndCheckTimer(int expected_time, int error) {
        long nanoTime_finish = System.nanoTime();
        long actual_time = getTextAndWriteLog(TimeUnit.SECONDS.convert((nanoTime_finish - nanoTime_start), TimeUnit.NANOSECONDS));
        return comparisonData(actual_time, expected_time, error);
    }

    public String getCurrentHour(String timeZoneValue) {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone(timeZoneValue)); // creates a new calendar instance
        calendar.setTime(date);
        return String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
    }

    public Date getCurrentDateWithDateOffset(String timeZoneValue, int offsetValue) {
        Date date = new Date();
        Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone(timeZoneValue));
        calendar.setTime(date);
        int currentDateWithOffset = calendar.get(Calendar.DATE) + offsetValue;
        if (currentDateWithOffset < calendar.getActualMinimum(Calendar.DATE)) {
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + currentDateWithOffset);
        } else if (offsetValue < 0) {
            calendar.roll(Calendar.DATE, offsetValue);
        }
        if (currentDateWithOffset > calendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
            int currentDateWithOffsetForNewDate = currentDateWithOffset - calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
            calendar.set(Calendar.DATE, currentDateWithOffsetForNewDate);
        }
        return calendar.getTime();
    }

    public String getYesterdayDate(String timeZoneValue, String dateFormat) {
        LocalDate yesterday = LocalDate.now(ZoneId.of(timeZoneValue)).minusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return yesterday.format(formatter);
    }

    public static <T extends Comparable<T>> boolean isEqualsLists(List<T> list1, List<T> list2){
        if (list1 == null && list2 == null) {
            return true;
        }
        else if(list1 == null || list2 == null) {
            return false;
        }
        else if(list1.size() != list2.size()) {
            return false;
        }

        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1);
        Collections.sort(list2);

        return list1.equals(list2);
    }
}