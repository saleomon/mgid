package core.helpers;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.Logs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;

public class ErrorsHelper {

    protected static Logger log;

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    public ErrorsHelper(Logger log) {
        ErrorsHelper.log = log;
    }

    public static Boolean check500Error() {
        try {
            if ($x(".//h2[contains(text(), '500 ')]").exists() ||
                    $(".error-code").exists() ||
                    $x(".//h1[contains(text(), 'Something went wrong')]").exists()) {
                writeError("ERROR: 500 Application error detected! - " + url());
                log.error("ERROR: 500 Application error detected! - " + url());
                if ($(".error-code").exists()) {
                    writeError("ERROR: 500 Application error detected! - " + $(".error-code").text());
                    log.error("ERROR: 500 Application error detected! - " + $(".error-code").text());
                }
                return false;
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    public static Boolean check404Error() {
        try {
            if ($x(".//h2[contains(text(), '404')]").exists()) {
                writeError("ERROR: 404 Application error detected! - " + url());
                log.error("ERROR: 404 Application error detected! - " + url());
                return false;
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    public static Boolean check403Error() {
        try {
            if ($x(".//h2[contains(text(), '403')]").exists()) {
                writeError("ERROR: 403 | Forbidden | Exception - " + url());
                log.error("ERROR: 403 | Forbidden | Exception - " + url());
                return false;
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    public static Boolean checkPHPFatalError() {
        try {
            if ($x(".//*[contains(text(), 'Fatal error') or contains(text(), 'Fatal Error')]").exists()) {
                writeError("ERROR: PHP fatal error output detected! - " + url() + $x(".//body").text());
                log.error("ERROR: PHP fatal error output detected! - " + url() + $x(".//body").text());
                return false;
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    public static Boolean checkPHPWarning() {
        try {
            if ($x(".//b[contains(text(), 'Warning') and not(self::script)]").exists()) {
                String url = url();
                if(!url.contains("informers-preview/momentary/")) {
                    writeError("ERROR: PHP warning output detected! - " + url);
                    log.error("ERROR: PHP warning output detected! - " + url);
                    return false;
                }
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    public static Boolean checkPHPNotice() {
        try {
            if ($x(".//b[contains(text(), 'Notice') and not(self::script)]").exists()) {
                writeError("ERROR: PHP notice output detected! - " + url());
                log.error("ERROR: PHP notice output detected! - " + url());
                return false;
            }
        } catch (NoSuchElementException e) {
            log.error("Catch " + e);
        }
        return true;
    }

    /**
     * check errors:
     * 400
     * 500
     * warnings
     * notice
     * console errors
     */
    public static boolean checkErrors() {
        return checkJSConsoleErrors() &
                check500Error() &
                check404Error() &
                check403Error() &
                checkPHPFatalError() &
                checkPHPWarning() &
                checkPHPNotice();
    }

    public static void writeError(String text) {
        try {
            writeErrorForErrorContainer(text);
        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }

    public static void writeErrorForErrorContainer(String text) throws IOException {
        String testName = Thread.currentThread().getStackTrace()[7].getMethodName().contains("invoke0") ? Thread.currentThread().getStackTrace()[6].getMethodName() : Thread.currentThread().getStackTrace()[7].getMethodName();
        String envPath = System.getenv("SELENOID_HOST").equalsIgnoreCase("localhost") ? "errorContainer/errorContainer.txt" : "/artifacts/errorContainer/errorContainer.txt";

        new File("/artifacts/errorContainer/").mkdir();

        FileWriter writerForErrorContainer = new FileWriter(envPath, true);
        writerForErrorContainer.write(testName + " - " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " ERROR [" + " - " + ANSI_RED + text + ANSI_RESET + "] \n");
        writerForErrorContainer.write("+-----------------------------------------------+\n");
        writerForErrorContainer.flush();
        writerForErrorContainer.close();
    }

    public static Boolean  checkJSConsoleErrors() {
        int count = 0;
        String message;
        try {
            Logs jsLog = getWebDriver().manage().logs();

            List<LogEntry> logsEntries = jsLog.get("browser").getAll();

            for (LogEntry entry : logsEntries) {

                message = entry.getMessage();

                if(!(message.matches(".*(" +
                        "hotjar|" +
                        "mc.yandex.ru|" +
                        "mc.yandex.com|" +
                        "an.yandex.ru|" +
                        "braintreegateway|" +
                        "Facebook Pixel|" +
                        "Synchronous XMLHttpRequest|" +
                        "DOM|" +
                        "jquery-1|" +
                        "_quadratic|" +
                        "usemessages|" +
                        "linkedin|" +
                        "certif_exten|" +
                        "clarity.ms|" +
                        "sprites.svg|" +
                        "facebook.com|" +
                        "s-img.mgid.com|" +
                        "bat.bing.com|" +
                        "apis.google.com|" +
                        "imgh/video|" +
                        "chromewebdata|" +
                        "clientId is not defined|" +
                        "teasersImagesError|" +
                        "favicon.ico|" +
                        "hsleadflows|" +
                        //toDo https://jira.mgid.com/browse/MT-6263
                        "(reading 'images')|" +
                        "app.hubspot.com|" +
                        "publisher-cookie|" +
                        "idsync.rlcdn.com|" +
                        "MG_AUTHORIZATION_LOGO|" +
                        "i18n.mgid.com|" +
                        "Stonehenge.jpg|" +
                        "mount.jpg|" +
                        "rtb-csync.smartadserver.com|" +
                        "Cannot read property 'style' of null|" +
                        "Undefined js translate key|" +
                        "a.idealmedia.io/touchpoints-sensor.js|" +
                        "local-amp.com:8000/dist.3p|" +
                        "OpenGL, Performance|" +
                        "googletagmanager|" +
                        "sync.inmobi.com|" +
                        "image2.pubmatic.com|" +
                        "ads.pubmatic.com|" +
                        "u.4dex.io|" +
                        "local.units.mgid.com|" +
                        "pixel.rubiconproject.com|" +
                        "cs.admanmedia.com|" +
                        "gum.criteo.com|" +
                        "dashboard.idealmedia.io/js/jquery.fancytree-all.min.|" +
                        "beforeInitHooks hook ConsentsBlock|" +
                        "observerTeasersHooks hook ViewabilityRefreshBlock|" +
                        "observerWidgetHooks hook ViewabilityRefreshBlock|" +
                        "observerWidgetHooks hook setWidgetRealShowTime|" +
                        "beforePrepareCappingDataHooks hook AmpRenderBlock|" +
                        "observerWidgetHooks hook MainBlock|" +
                        "observerTeasersHooks hook CollectViewDataBlock|" +
                        "observerWidgetHooks hook CollectViewDataBlock|" +
                        "Request image autoCounter load error|" +
                        "Request autoCounter stopped - already defined on page|" +
                        "Autoplacement is not supported in Cross Origin Frame|" +
                        "Autoplacement Smart Informer: Article not found|" +
                        "Cannot resize element and overflow is not available|" +
                        "lbs.eu-1-id5-sync.com|" +
                        "static.zdassets.com).*") |
                        message.contains("app.hubspot.com") |
                        (message.contains("https://servicer") && message.contains("server responded with a status of 403")))
                ) {

                    if (message.contains("ReferenceError")) {
                        log.info("WRITE ERROR ReferenceError 1  -  " + message);

                        writeError("Catching ReferenceError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR ReferenceError 2  -  " + message);

                        count++;
                    } else if(message.contains("https://servicer") && message.contains("server responded with a status of 403")){
                        break;
                    } else if (message.contains("EvalError")) {
                        log.info("WRITE ERROR EvalError 1  -  " + message);

                        writeError("Catching EvalError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR EvalError 2  -  " + message);

                        count++;
                    } else if (message.contains("InternalError")) {
                        log.info("WRITE ERROR InternalError 1  -  " + message);

                        writeError("Catching InternalError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR InternalError 2  -  " + message);

                        count++;
                    } else if (message.contains("RangeError")) {
                        log.info("WRITE ERROR RangeError 1  -  " + message);

                        writeError("Catching RangeError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR RangeError 2  -  " + message);

                        count++;
                    } else if (message.contains("TypeError")) {
                        log.info("WRITE ERROR TypeError 1  -  " + message);

                        writeError("Catching TypeError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR TypeError 2  -  " + message);

                        count++;
                    } else if (message.contains("URIError")) {
                        log.info("WRITE ERROR URIError 1  -  " + message);

                        writeError("Catching URIError: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR URIError 2  -  " + message);

                        count++;
                    } else if (message.contains("Uncaught")) {
                        log.info("WRITE ERROR Uncaught 1  -  " + message);

                        writeError("Catching Uncaught Message: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR Uncaught 2  -  " + message);

                        count++;
                    } else if (message.contains("Failed to load resource")) {
                        log.info("WRITE ERROR Failed to load resource 1  -  " + message);

                        writeError("Failed to load resource: " + url() + ", ERROR MESSAGE - " + message);
                        log.info("WRITE ERROR Failed to load resource 2  -  " + message);

                        count++;
                    } else {
                        log.info("WRITE ERROR Other error 1  -  " + message);

                        writeError("Other error - " + url() + " - " + message);
                        log.info("WRITE ERROR Other error 2  -  " + message);

                        count++;
                    }
                }
            }
        } catch (UnhandledAlertException e) {
            log.info("WRITE ERROR UnhandledAlertException  -  " + e);

        } catch (Exception e) {
            log.info("WRITE ERROR Exception  -  " + e);

            log.error("Catch " + e);
        }
        return count == 0;
    }

}
