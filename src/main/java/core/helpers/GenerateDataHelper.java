package core.helpers;

import com.github.javafaker.Faker;

import java.util.Date;

public class GenerateDataHelper {
    public Faker faker = new Faker();

    /**
     * Generate human name
     */
    public String generateName() {
        return faker.name().firstName();
    }

    /**
     * Generate human last name
     */
    public String generateSurname() {
        return faker.name().lastName();
    }

    /**
     * Generate date of birth for ages between 18 and 65 years
     */
    public Date generateBirthDate() {
        return faker.date().birthday();
    }

    /**
     * Generate e-mail
     */
    public String generateEmail() {
        return faker.internet().emailAddress();
    }

    public String generateCompanyName() {
        return faker.company().name();
    }
}
