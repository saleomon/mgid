package core.helpers;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.Logger;

import java.time.Duration;
import java.util.*;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static core.helpers.HelperLocators.ERRORS_CUSTOM_FIELD;

public class MessageHelper extends BaseHelper {

    private final String GENERAL_MESSAGES_DASH = "[id*='notifications'] [class='text']";
    private final String GENERAL_MESSAGES_DASH_NEW = "[automation-id='tui-notification-alert__content']";

    public MessageHelper(Logger log) {
        BaseHelper.log = log;
    }

    /**
     * help method for get Map<String, String> message in CAB
     * this method get text and type message
     * message type
     * - success
     * - notice
     * - error
     */
    private Map<String, String> getMessagesMap(boolean isClean) {
        Map<String, String> messages = new HashMap<>();
        int time = 0;
        long count_message;
        do {
            count_message = executeJavaScript("return jsMessages.messages.length");
            if (time > 0) {
                sleep(500);
            }
            time++;
        }
        while (count_message == 0 && time < 8);

        if (count_message > 0) {

            Object[] listObj = ((ArrayList<?>) Objects.requireNonNull(executeJavaScript("return jsMessages.messages;"))).toArray();

            for (Object o : listObj) {
                messages.put((String) new ObjectMapper().convertValue(o, Map.class).get("text"),
                        (String) new ObjectMapper().convertValue(o, Map.class).get("type"));
            }
            messages.forEach((k, v) -> log.info("messages cab -> text: " + k + "type: " + v));

            sleep(1500);
            if (isClean) executeJavaScript("jsMessages.clean();");
        }
        return messages;
    }

    /**
     * help method for get ArrayList message in CAB
     * this method get only text message
     */
    private ArrayList getMessages() {
        ArrayList messages = new ArrayList<>();
        int time = 0;
        long count_message;
        do {
            count_message = executeJavaScript("return jsMessages.messages.length");
            if (time > 0) {
                sleep(500);
            }
            time++;
        }
        while (count_message == 0 && time < 20);

        if (count_message > 0) {

            messages = executeJavaScript("var r = []; jsMessages.messages.forEach(item => {{ r.push(item.text)}} ); return r;");
            Objects.requireNonNull(messages).forEach((k) -> log.info("messages cab -> text: " + k));

        }
        return messages;
    }

    public boolean isDisplayedErrorMessageCab() {
        return Integer.parseInt(Objects.requireNonNull(executeJavaScript("return jsMessages.messages.length")).toString()) > 0;

    }

    /**
     * wait and check custom message
     */
    public boolean checkCustomMessagesCab(String messages) {
        int time = 0, count;
        boolean result;
        ArrayList list_messages;


        //wait jsMessages.messages.length > 0
        do {
            count = Integer.parseInt(Objects.requireNonNull(executeJavaScript("return jsMessages.messages.length")).toString());
            if (time > 0) {
                sleep(500);
            }
            time++;
        }
        while (count == 0 && time < 20);

        time = 0;


        // wait custom jsMessages.messages
        do {
            result = executeJavaScript("return jsMessages.messages.some(item => { if(item.text === `" +
                    messages + "` | item.text.includes(`" + messages + "`)) return true; }  ); ");
            if (time > 0) {
                sleep(500);
            }
            time++;
        }
        while (!result && time < 20);


        // show jsMessages.messages in log
        list_messages = executeJavaScript("var r = []; jsMessages.messages.forEach(item => {{ r.push(item.text)}} ); return r;");
        Objects.requireNonNull(list_messages).forEach((k) -> log.info("messages cab -> text: " + k));

        if (result) {
            sleep(1500);
            executeJavaScript("jsMessages.clean();");
        }

        return result;
    }

    /**
     * clean jsMessages.messages, need to get next message after new action
     */
    public void cleanJsMessage() {
        executeJavaScript("jsMessages.messages = [];");
    }

    /**
     * check message dash
     */
    public boolean checkMessageDash(String... messages) {
        $$(GENERAL_MESSAGES_DASH).get(0).scrollTo();
        ElementsCollection list = $$(GENERAL_MESSAGES_DASH).filter(visible);
        list.shouldBe(CollectionCondition.sizeGreaterThan(0), Duration.ofSeconds(8));
        Arrays.stream(messages).forEach(el->log.info("Message expected - " + el));
        list.texts().forEach(el->log.info("Message dash - " + el));
        log.info("checkMessageDash,list: " + list.size());
        list.get(0).scrollTo().shouldBe(visible);
        return getTextAndWriteLog(Arrays.stream(messages).allMatch(i -> list.texts().contains(i.toUpperCase())));
    }

    /**
     * check message dash new
     */
    public boolean checkMessageDashNew(String... messages) {
        sleep(1500);
        ElementsCollection list = $$(GENERAL_MESSAGES_DASH_NEW);
        list.shouldBe(CollectionCondition.sizeGreaterThan(0), Duration.ofSeconds(8));
        log.info("checkMessageDash,list: " + list.size());
        list.get(0).scrollTo().shouldBe(visible);
        return getTextAndWriteLog(Arrays.stream(messages).allMatch(i -> list.texts().contains(i)));
    }

    /**
     * get notification from dash
     */
    public String getMessageDash() {
        ElementsCollection list = $$(GENERAL_MESSAGES_DASH);
        log.info("GENERAL_MESSAGES_DASH.size(): " + list.size());
        if (list.size() > 0) {
            return getTextAndWriteLog(list.get(0).text());
        }
        return getTextAndWriteLog("");
    }

    /**
     * check that custom message has in list message
     */
    public boolean checkMessagesCab(String messages) {
        ArrayList values = getMessages();
        return values.contains(messages);
    }

    /**
     * get message CAB if we have only part message
     * exp. (we needto get id in message)
     */
    public String getCustomMessageValueCab(String message) {
        ArrayList<String> values = getMessages();
        for (String v : values) {
            if (v.contains(message))
                return v;
        }
        return null;
    }

    /**
     * check is show success message( and check that don't show 'error' message)
     */
    public boolean isSuccessMessagesCab(boolean isClean) {
        Map<String, String> map = getMessagesMap(isClean);
        return map.containsValue("success") &&
                !map.containsValue("error");
    }

    /**
     * check is show success message( and check that don't show 'error' message)
     */
    public boolean isSuccessMessagesCab() {
        return getTextAndWriteLog(isSuccessMessagesCab(true));
    }

    /**
     * check is show success message( and check that don't show 'error' message)
     */
    public boolean isSuccessMessagesCabWithoutClean() {
        return getTextAndWriteLog(
                isSuccessMessagesCab(false));
    }

    /**
     * method check contains error for custom field
     */
    public boolean checkErrorMessageCabForCustomField(String textMessage) {
        return getTextAndWriteLog($$(ERRORS_CUSTOM_FIELD).texts().stream().anyMatch(m ->
                getTextAndWriteLog(m).equals(textMessage)));
    }

    /**
     * is shown emergency mode error dash
     */
    public boolean isEmergencyModePresentDash() {
        String message = getMessageDash();
        return message.matches("НА ДАННЫЙ МОМЕНТ РАБОТА ВОЗМОЖНА ТОЛЬКО В РЕЖИМЕ ПРОСМОТРА ДАННЫХ, ВНЕСЕНИЕ ИЗМЕНЕНИЙ ВРЕМЕННО НЕВОЗМОЖНО|" +
                "ЦІЄЇ МИТІ РОБОТА МОЖЛИВА ТІЛЬКИ В РЕЖИМІ ПЕРЕГЛЯДУ ДАНИХ, ВНОСИТИ ЗМІНИ ТИМЧАСОВО НЕМОЖЛИВО|" +
                "CURRENTLY, THE DASHBOARD WORKS IN READ-ONLY MODE. YOU CANNOT MAKE ANY CHANGES|" +
                "ВОЗНИКЛА ОШИБКА! ПОЖАЛУЙСТА, ПОПРОБУЙТЕ ПОВТОРИТЬ ПОЗЖЕ.|" +
                "ВИНИКЛА ПОМИЛКА! БУДЬ ЛАСКА, СПРОБУЙТЕ ПОВТОРИТИ ПІЗНІШЕ.|" +
                "AN ERROR HAS OCCURRED. PLEASE TRY AGAIN LATER");
    }
}
