package core.helpers;

import libs.hikari.tableQueries.admin.RolesPrivilegesDevelopment;
import libs.hikari.tableQueries.partners.AclRolesRightsDashboard;

public class PrivilegesHelper {
    private final RolesPrivilegesDevelopment rolesPrivilegesDevelopment = new RolesPrivilegesDevelopment();
    private final AclRolesRightsDashboard aclRolesRightsDashboard = new AclRolesRightsDashboard();

    /**
     * change roles privilege state if needed
     *
     * @param roleId     - role id
     * @param privileges - section with privilege name. Example: admin/cab-link
     */
    public void changeRolesPrivilegeCab(boolean isOnPrivilege, Integer roleId, String... privileges) {
        if (isOnPrivilege) {
            rolesPrivilegesDevelopment.insertRolesPrivilegesDevelopmentCustom(roleId, privileges);
        } else {
            rolesPrivilegesDevelopment.deleteRolesPrivilegesDevelopmentCustom(roleId, privileges);
        }
    }

    /**
     * change roles privilege state if needed
     *
     * @param roleId     - role id
     * @param privileges - section with privilege name. Example: admin/cab-link
     */
    public void changeRolesPrivilegeDash(boolean isOnPrivilege, Integer roleId, String... privileges) {
        if (isOnPrivilege) {
            aclRolesRightsDashboard.insertAclRolesRightDashboardCustom(roleId, privileges);
        } else {
            aclRolesRightsDashboard.deleteAclRolesRightDashboardCustom(roleId, privileges);
        }
    }
}
