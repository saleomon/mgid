package core.helpers.date;

public class DateFormat {
    public enum DateFormats {
        DD_MM_YYYY("dd-MM-yyyy"),
        MM_DD_YYYY("MM-dd-yyyy"),
        YYYY_MM_DD("yyyy-MM-dd"),
        YYYY_DD_MM("yyyy-dd-MM"),
        HH_MM_SS("HH:mm:ss.SSS"),
        YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss");
        private final String format;

        DateFormats(String format) {
            this.format = format;
        }

        public String getFormat() {
            return format;
        }
    }
}
