package core.helpers.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateHelper {
    SimpleDateFormat formatter;
    Calendar calendar;

    public DateHelper() {
        calendar = new GregorianCalendar();
    }

    public DateHelper setTimeZone(TimeZones.TimeZone timeZoneValue) {
        calendar.setTimeZone(TimeZone.getTimeZone(timeZoneValue.getTimeZone()));
        return this;
    }
    public String getDateWithFormat(DateFormat.DateFormats format) {
        formatter = new SimpleDateFormat(format.getFormat());
        return formatter.format(calendar.getTime());
    }
    public DateHelper setHourOffset(int offset) {
        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + offset);
        return this;
    }
    public DateHelper setDayOffset(int offset) {
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + offset);
        return this;
    }
    public DateHelper setMonthOffSet(int offset) {
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + offset);
        return this;
    }
    public DateHelper setYearOffSet(int offset) {
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + offset);
        return this;
    }
    public int getHour() {
        return calendar.get(Calendar.HOUR);
    }
    public int getDay() {
        return calendar.get(Calendar.DAY_OF_MONTH);
    }
    public int getMonth() {
        return calendar.get(Calendar.MONTH) + 1;
    }
    public int getYear() {
        return calendar.get(Calendar.YEAR);
    }

}
