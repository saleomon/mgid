package core.helpers.date;

public class TimeZones {
    public enum TimeZone {
        GMT_10("GMT-10");
        private final String zone;

        TimeZone(String zone) {
            this.zone = zone;
        }

        public String getTimeZone() {
            return zone;
        }
    }
}
