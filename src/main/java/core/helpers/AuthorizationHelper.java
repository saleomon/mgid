package core.helpers;


import com.fasterxml.jackson.databind.ObjectMapper;
import libs.dockerClient.base.DockerCab;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.openqa.selenium.Cookie;
import testData.project.AuthUserCabData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static core.helpers.HelperLocators.GOGOL_ICON;
import static core.helpers.HelperLocators.PROFILE_BLOCK;
import static java.time.Duration.ofSeconds;
import static testData.project.EndPoints.*;
import static testData.project.Subnets.SubnetType;

public class AuthorizationHelper {

    protected Logger log;
    private String responseAfterRequest;
    private final String cookieName = "local_admin_id_token";
    public static final String logOutCab = "auth/logout";
    public static final String signOutDash = "user/signout";
    private final String pathToFileWithExistedCookie = "cookieStorage/cookieStorage.json";
    private final String pathToFileWithCabId = "cookieStorage/cabId.txt";
    public static Map<String, String> usersTokens = new HashMap<>();
    public static String cabId;
    private final DockerCab dockerCab;

    public AuthorizationHelper(Logger log) {
        this.log = log;
        dockerCab = new DockerCab();
    }

    /**
     * go to custom link Cab
     *
     */
    public void goLinkCab(String... link) {
        String url, tempUrl = "";

        if (link.length > 0 && !link[0].contains("cab/")) {
            tempUrl = link[0];
        } else if (link.length > 0 && link[0].contains("cab/")) {
            tempUrl = link[0].substring(link[0].indexOf("cab/") + 4);
        }

        url = cabLink + tempUrl;
        open(url);
    }

    /**
     * go to custom link Dash
     */
    public void goLinkDashboard(String link, SubnetType subnet) {
        String url, host;

        if (link.contains("dashboard") | link.contains("panel")) {
            url = link;
        } else {
            host = switch (subnet) {
                case SCENARIO_MGID -> mgidLink;
                case SCENARIO_ADSKEEPER -> adskeeperLink;
                case SCENARIO_IDEALMEDIA_IO -> idealmediaIoLink;
                case SCENARIO_EPEEX -> epeexLink;
                case SCENARIO_ADGAGE -> adgageLink;
            };
            url = host + "/" + link;
        }

        open(url);
    }

    /**
     * redirect cab - dash by gogol
     */
    public void goByGogol() {
        open(Objects.requireNonNull(GOGOL_ICON
                .shouldBe(visible)
                .attr("href")));
        PROFILE_BLOCK.shouldBe(visible, ofSeconds(8));
    }

    /**
     * auth Cab and go to custom link
     * accepts part of the link as input after <a href="https://local-admin.mgid.com/cab/">https://local-admin.mgid.com/cab/</a>
     * RKO
     */

    public void setCookieForAuthorization(AuthUserCabData.AuthorizationUsers user) {
            String[] cookie = usersTokens.get(user.getLoginUser()).split("=");
            getWebDriver().manage().addCookie(new Cookie(cookie[0], cookie[1]));
    }

    public void deleteAuthorizationCookie(AuthUserCabData.AuthorizationUsers user) {
        String[] cookie = usersTokens.get(user.getLoginUser()).split("=");
        getWebDriver().manage().deleteCookie(new Cookie(cookie[0],cookie[1]));
    }

    private String getCookieForAuthorization() {
        return BaseHelper.getRegExValue(responseAfterRequest, cookieName + "=.+?(?=;)");
    }

    private void requestForCookie(String authorizationCode) {
        String lineFromRequest;
        try {
            Process processInfo = Runtime.getRuntime().exec("curl http://local-admin.mgid.com/cab/auth2/token -d grant_type=authorization_code&source=native&rrrrrrrr=rrrrrrrr&code=" + authorizationCode + " -v");

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(processInfo.getErrorStream()));
        while ((lineFromRequest = stdError.readLine()) != null) {
            if(lineFromRequest.contains(cookieName)) {
                responseAfterRequest = lineFromRequest;
            }
        }
        } catch (IOException e) {
            log.info(e);
        }
    }

    public void getCookieForUsersAuthorization(AuthUserCabData.AuthorizationUsers...users) throws IOException {
        log.info("getCookieForUsersAuthorization - start");
        if (System.getenv("CI_JOB_ID").equalsIgnoreCase("local_ci_job")) {
                String dataCabIdFromFile = new File(pathToFileWithCabId).exists() ? IOUtils.toString(new FileInputStream(pathToFileWithCabId),  StandardCharsets.UTF_8) : "";
                cabId = dockerCab.getContainerCabId();

                if(dataCabIdFromFile.equalsIgnoreCase(cabId)) {
                    usersTokens = getExistentCookieFromFile();
                } else {
                    usersTokens = getNewCookie(users);

                    JSONObject authData = new JSONObject();
                    authData.putAll(usersTokens);

                    Files.write(Paths.get(pathToFileWithExistedCookie),  authData.toJSONString().getBytes());
                    Files.write(Paths.get(pathToFileWithCabId),  cabId.getBytes());
                }
        } else usersTokens = getNewCookie(users);

        log.info("getCookieForUsersAuthorization - finish");
    }

    private HashMap getExistentCookieFromFile() throws IOException {
        String jsonString = IOUtils.toString(new FileInputStream(pathToFileWithExistedCookie), StandardCharsets.UTF_8);
        return new ObjectMapper().readValue(jsonString, HashMap.class);
    }

    private HashMap<String, String> getNewCookie(AuthUserCabData.AuthorizationUsers...users) {
        HashMap<String, String> authData = new HashMap();

        Arrays.asList(users).forEach(user -> {
                String authorizationCode = dockerCab.requestForAuthorizationCode(user.getLoginUser());
                requestForCookie(authorizationCode);
                authData.put(user.getLoginUser(), getCookieForAuthorization());
        });
        return authData;
    }
}
