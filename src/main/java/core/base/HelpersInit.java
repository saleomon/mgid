package core.base;

import core.helpers.*;
import core.helpers.date.DateHelper;
import core.helpers.sorted.SortedHelper;
import core.helpers.statCalendar.CalendarPeriodHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HelpersInit {
    Logger log;
    BaseHelper baseHelper;
    ErrorsHelper errorsHelper;
    MessageHelper messageHelper;
    MultiFilterHelper multiFilterHelper;
    PrivilegesHelper privilegesHelper;
    SortedHelper sortedHelper;
    CalendarPeriodHelper calendarPeriodHelper;
    AuthorizationHelper authorizationHelper;
    GenerateDataHelper generateDataHelper;
    DateHelper dateHelper;

    public HelpersInit() {
        log = LogManager.getLogger(getClass());
        this.baseHelper = new BaseHelper();
        this.errorsHelper = new ErrorsHelper(log);
        this.messageHelper = new MessageHelper(log);
        this.multiFilterHelper = new MultiFilterHelper(log);
        this.privilegesHelper = new PrivilegesHelper();
        this.sortedHelper = new SortedHelper(log);
        this.calendarPeriodHelper = new CalendarPeriodHelper();
        this.authorizationHelper = new AuthorizationHelper(log);
        this.generateDataHelper = new GenerateDataHelper();
        this.dateHelper = new DateHelper();
    }

    public BaseHelper getBaseHelper() {
        return baseHelper;
    }

    public PrivilegesHelper getPrivilegesHelper() {
        return privilegesHelper;
    }

    public MultiFilterHelper getMultiFilterHelper() {
        return multiFilterHelper;
    }

    public MessageHelper getMessageHelper() {
        return messageHelper;
    }

    public ErrorsHelper getErrorsHelper() {
        return errorsHelper;
    }

    public SortedHelper getSortedHelper() {
        return sortedHelper;
    }

    public CalendarPeriodHelper getCalendarPeriodHelper() {
        return calendarPeriodHelper;
    }

    public AuthorizationHelper getAuthorizationHelper() {
        return authorizationHelper;
    }

    public GenerateDataHelper getGenerateDataHelper() {
        return generateDataHelper;
    }
    public DateHelper getDateHelper() {
        return dateHelper;
    }
}
