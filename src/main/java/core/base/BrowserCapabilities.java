package core.base;

import com.codeborne.selenide.WebDriverRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.open;
import static core.service.PropertiesManager.ConfigValue;
import static core.service.PropertiesManager.getResourceByName;

public class BrowserCapabilities {
    RemoteWebDriver driver;
    private Logger log;
    private final String host = getResourceByName(ConfigValue.HOST);
    public static String videoRecorderMode;

    public BrowserCapabilities() {
    }

    public void initBrowserCapabilities(String currentClassName) {
        if (System.getenv("SELENOID_HOST").equalsIgnoreCase("localhost")) videoRecorderMode = "enabled";
        else videoRecorderMode = System.getProperty("VIDEO_RECORDING");
        try {
            String localSessionName = currentClassName + "_" + new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss").format(new Date()) + ".mp4";
            log = LogManager.getLogger(getClass());

            Map<String, Object> selenoidOptions = new HashMap<>();
            selenoidOptions.put("screenResolution", "1280x800");
            selenoidOptions.put("enableVideo", videoRecorderMode.equals("enabled"));

            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized");
            chromeOptions.addArguments("disable-gpu");
            chromeOptions.addArguments("remote-allow-origins=*");
            chromeOptions.setCapability(CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            if (System.getenv("SELENOID_HOST").equalsIgnoreCase("localhost")) {
                selenoidOptions.put("enableVNC", videoRecorderMode.equals("enabled"));
                selenoidOptions.put("version", "vnc_" + videoRecorderMode + "_image");
                selenoidOptions.put("name", localSessionName);
            }
            chromeOptions.setCapability("selenoid:options", selenoidOptions);

            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", "/home/selenium/Downloads");
            chromeOptions.setExperimentalOption("prefs", chromePrefs);
            chromeOptions.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

            driver = new RemoteWebDriver(new URL("http://" + host + ":4444/wd/hub"), chromeOptions);
            driver.setFileDetector(new LocalFileDetector());
            WebDriverRunner.setWebDriver(driver);
            open();

        } catch (Exception e) {
            log.error("Catch: " + e);
        }
    }
}
