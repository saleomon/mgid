package core.base;

import org.apache.logging.log4j.Logger;
import pages.cab.administration.logic.BlacklistDfp;
import pages.cab.administration.logic.Users;
import pages.cab.advertisingAgencies.logic.AdvertisingAgencies;
import pages.cab.authorizationForm.logic.Authorization;
import pages.cab.crm.logic.CrmClients;
import pages.cab.finances.logic.*;
import pages.cab.products.helpers.LandingOffersCreateEditHelper;
import pages.cab.products.logic.*;
import pages.cab.publishers.logic.*;
import pages.cab.publishers.logic.finances.CabPublishersClientsPayouts;
import pages.cab.publishers.logic.stats.CabStatsInformersByClients;
import pages.cab.publishers.logic.stats.CabStatsInformersByComposites;
import pages.cab.publishers.logic.stats.CabStatsInformersByCountries;
import pages.cab.publishers.logic.stats.CabStatsInformersBySites;
import pages.cab.publishers.logic.widgets.*;
import pages.cab.video.logic.VideoContentPage;
import pages.dash.advertiser.logic.*;
import pages.dash.publisher.logic.*;
import pages.dash.signup.logic.SignUp;
import pages.dash.userProfile.logic.UserProfile;

/**
 * Created by user on 05.09.2016.
 */
public class PagesInit {
    // cab
    private CrmClients crmClients;
    private CabWebsites cabWebsites;
    private CabProductWebsites cabProductWebsites;
    private CabExchangeCampaigns cabExchangeCampaigns;
    private CabNews cabNews;
    private CabNewsModeration cabNewsModeration;
    private CabCampaigns cabCampaigns;
    private CabCampaignsSitesFilters cabCampaignsSitesFilters;
    private CabCompositeSettings cabCompositeSettings;
    private CabGoodsSettings cabGoodsSettings;
    private CabExchangeSettings cabExchangeSettings;
    private CabStatsInformersBySites cabStatsInformersBySites;
    private CabStatsInformersByComposites cabStatsInformersByComposites;
    private CabStatsInformersByCountries cabStatsInformersByCountries;
    private CabStatsInformersByClients cabStatsInformersByClients;
    private CabInformersCode cabInformersCode;
    private CabWidgetsList cabWidgetsList;
    private CabInformersPreviewMomentary cabInformersPreviewMomentary;
    private CabTeasers cabTeasers;
    private CabTitleTagsVerification cabTitleTagsVerification;
    private SalesManagersBonuses salesManagersBonuses;
    private CabScreenshotHistory cabScreenshotHistory;
    private CabTeasersModeration cabTeasersModeration;
    private CabPublisherClients cabPublisherClients;
    private CabProductClients cabProductClients;
    private LandingOffers landingOffers;
    private LandingOffersCreateEditHelper landingOffersCreateEditHelper;
    private BlacklistDfp blacklistDfp;
    private FilterBlockedPublishers filterBlockedPublishers;
    private CabTargets cabTargets;
    private CabAudiences cabAudiences;
    private SelectiveBidding selectiveBidding1;
    private RulesBaseOptimization rulesBaseOptimization;
    private SelectiveBiddingNew selectiveBinding2;
    private CabCreativeRequests cabCreativeRequests;
    private CabCreative cabCreative;
    private AdvertisingAgencies advertisingAgencies;
    private PublishersEfficiency publishersEfficiency;
    private CabPublishersClientsPayouts cabPublishersClientsPayouts;
    private VideoContentPage videoContent;
    private CabInformersManageVideoCfg manageVideoCfg;
    private CabInformersManageVideo manageVideo;
    private CabEditCode cabEditCode;
    private CabAllStats cabAllStats;
    private Users users;
    private Authorization authorization;
    private AdvertisingAgencyBonuses agencyBonuses;

    //dash
    private SignUp signUp;
    private Widget widgetClass;
    private Website websiteClass;
    private Audiences audiences;
    private Campaigns campaigns;
    private Teasers teaserClass;
    private ConversionStats conversionStats;
    private DetailedGoodStats detailedGoodStats;
    private WidgetStatistic widgetStatisticClass;
    private WebsiteStatistics websiteStatisticsClass;
    private Payouts payouts;
    private InternalAds internalAds;
    private BlockedContent blockedContent;
    private News newsClass;
    private AdvertisersBalance advertisersBalance;
    private UserProfile userProfile;
    private DirectDemand directDemand;
    private AdsModeration adsModeration;
    private TrafficInsights trafficInsights;

    protected PagesInit() {
    }

    protected void init(HelpersInit helpersInit, Logger log) {
        try {
            crmClients = new CrmClients(helpersInit);
            cabWebsites = new CabWebsites(log, helpersInit);
            cabExchangeCampaigns = new CabExchangeCampaigns(log, helpersInit);
            cabNews = new CabNews(log, helpersInit);
            cabNewsModeration = new CabNewsModeration(log, helpersInit);
            widgetClass = new Widget(log, helpersInit);
            websiteClass = new Website(log, helpersInit);
            cabCompositeSettings = new CabCompositeSettings(log, helpersInit);
            cabGoodsSettings = new CabGoodsSettings(log, helpersInit);
            cabExchangeSettings = new CabExchangeSettings(log, helpersInit);
            cabStatsInformersBySites = new CabStatsInformersBySites();
            cabStatsInformersByComposites = new CabStatsInformersByComposites();
            cabStatsInformersByCountries = new CabStatsInformersByCountries();
            cabStatsInformersByClients = new CabStatsInformersByClients();
            cabInformersCode = new CabInformersCode(log, helpersInit);
            cabWidgetsList = new CabWidgetsList(log, helpersInit);
            cabInformersPreviewMomentary = new CabInformersPreviewMomentary();
            cabCampaigns = new CabCampaigns(log, helpersInit);
            cabCampaignsSitesFilters = new CabCampaignsSitesFilters(log, helpersInit);
            cabProductWebsites = new CabProductWebsites(helpersInit);
            cabTeasers = new CabTeasers(log, helpersInit);
            cabTitleTagsVerification = new CabTitleTagsVerification(log, helpersInit);
            salesManagersBonuses = new SalesManagersBonuses(log, helpersInit);
            cabScreenshotHistory = new CabScreenshotHistory(helpersInit);
            cabPublisherClients = new CabPublisherClients(log, helpersInit);
            cabProductClients = new CabProductClients(helpersInit);
            blacklistDfp = new BlacklistDfp(helpersInit);
            filterBlockedPublishers = new FilterBlockedPublishers(log, helpersInit);
            conversionStats = new ConversionStats(helpersInit);
            cabTargets = new CabTargets(log, helpersInit);
            cabAudiences = new CabAudiences(log, helpersInit);
            selectiveBinding2 = new SelectiveBiddingNew(log, helpersInit);
            selectiveBidding1 = new SelectiveBidding(log, helpersInit);
            rulesBaseOptimization = new RulesBaseOptimization(log, helpersInit);
            cabCreativeRequests = new CabCreativeRequests(log, helpersInit);
            cabCreative = new CabCreative(log, helpersInit);
            cabTeasersModeration = new CabTeasersModeration(log, helpersInit);
            advertisingAgencies = new AdvertisingAgencies(helpersInit);
            agencyBonuses = new AdvertisingAgencyBonuses(helpersInit);
            landingOffers = new LandingOffers(log, helpersInit);
            landingOffersCreateEditHelper = new LandingOffersCreateEditHelper(helpersInit);
            audiences = new Audiences(log, helpersInit);
            campaigns = new Campaigns(log, helpersInit);
            detailedGoodStats = new DetailedGoodStats();
            teaserClass = new Teasers(log, helpersInit);
            publishersEfficiency = new PublishersEfficiency();
            widgetStatisticClass = new WidgetStatistic(log, helpersInit);
            websiteStatisticsClass = new WebsiteStatistics(log, helpersInit);
            payouts = new Payouts(log, helpersInit);
            cabPublishersClientsPayouts = new CabPublishersClientsPayouts(log, helpersInit);
            videoContent = new VideoContentPage(log, helpersInit);
            internalAds = new InternalAds(log, helpersInit);
            signUp = new SignUp(helpersInit, log);
            blockedContent = new BlockedContent(helpersInit);
            manageVideoCfg = new CabInformersManageVideoCfg();
            manageVideo = new CabInformersManageVideo(helpersInit, log);
            cabEditCode = new CabEditCode(log, helpersInit);
            newsClass = new News(log, helpersInit);
            advertisersBalance = new AdvertisersBalance();
            userProfile = new UserProfile(helpersInit);
            directDemand = new DirectDemand(helpersInit, log);
            adsModeration = new AdsModeration(helpersInit, log);
            cabAllStats = new CabAllStats();
            users = new Users(helpersInit, log);
            authorization = new Authorization(helpersInit);
            trafficInsights = new TrafficInsights(log, helpersInit);

        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }

    public CrmClients getCrmClients() {
        return crmClients;
    }

    public CabWebsites getCabWebsites() {
        return cabWebsites;
    }

    public CabProductWebsites getCabProductWebsites() {
        return cabProductWebsites;
    }

    public CabCampaigns getCabCampaigns() {
        return cabCampaigns;
    }

    public CabCampaignsSitesFilters getCabCampaignsSitesFilters() {
        return cabCampaignsSitesFilters;
    }

    public CabTeasers getCabTeasers() {
        return cabTeasers;
    }
    public CabTitleTagsVerification getCabTitleTagsVerification() {
        return cabTitleTagsVerification;
    }
    public SalesManagersBonuses getSalesManagersBonuses() {
        return salesManagersBonuses;
    }

    public AdvertisingAgencyBonuses getAgencyBonuses() {
        return agencyBonuses;
    }

    public CabScreenshotHistory getScreenshotHistory() {
        return cabScreenshotHistory;
    }

    public CabTeasersModeration getCabTeasersModeration() {
        return cabTeasersModeration;
    }

    public CabExchangeCampaigns getCabExchangeCampaigns() {
        return cabExchangeCampaigns;
    }

    public CabNews getCabNews() {
        return cabNews;
    }

    public Widget getWidgetClass() {
        return widgetClass;
    }

    public Website getWebsiteClass() {
        return websiteClass;
    }

    public CabPublisherClients getCabPublisherClients() {
        return cabPublisherClients;
    }

    public BlacklistDfp getBlacklistDfp() {
        return blacklistDfp;
    }

    public FilterBlockedPublishers getFilterBlockedPublishers() {
        return filterBlockedPublishers;
    }

    public CabTargets getCabTargets() {
        return cabTargets;
    }

    public CabAudiences getCabAudiences() {
        return cabAudiences;
    }

    public SelectiveBiddingNew getSelectiveBinding2() {
        return selectiveBinding2;
    }

    public SelectiveBidding getSelectiveBidding1() {
        return selectiveBidding1;
    }

    public RulesBaseOptimization getRulesBaseOptimization() {
        return rulesBaseOptimization;
    }

    public CabProductClients getCabProductClients() {
        return cabProductClients;
    }

    public LandingOffers getLandingOffers() {
        return landingOffers;
    }

    public LandingOffersCreateEditHelper getLandingOffersCreateEditHelper() {
        return landingOffersCreateEditHelper;
    }

    public CabCreativeRequests getCabCreativeRequests() {
        return cabCreativeRequests;
    }

    public CabCreative getCabCreative() {
        return cabCreative;
    }

    public AdvertisingAgencies getAdvertisingAgencies() {
        return advertisingAgencies;
    }

    public CabNewsModeration getCabNewsModeration() {
        return cabNewsModeration;
    }

    public Audiences getAudiences() {
        return audiences;
    }

    public Campaigns getCampaigns() {
        return campaigns;
    }

    public Teasers getTeaserClass() {
        return teaserClass;
    }

    public ConversionStats getConversionStats() {
        return conversionStats;
    }

    public DetailedGoodStats getDetailedGoodStats() {
        return detailedGoodStats;
    }

    public PublishersEfficiency getPublishersEfficiencyClass() {
        return publishersEfficiency;
    }

    public WidgetStatistic getWidgetStatisticClass() {
        return widgetStatisticClass;
    }

    public WebsiteStatistics getWebsiteStatisticsClass() {
        return websiteStatisticsClass;
    }

    public Payouts getPayoutsClass() {
        return payouts;
    }

    public CabPublishersClientsPayouts getCabPublisherPayouts() {
        return cabPublishersClientsPayouts;
    }

    public VideoContentPage getVideoContent() {
        return videoContent;
    }

    public InternalAds getInternalAds() {
        return internalAds;
    }

    public CabCompositeSettings getCabCompositeSettings() {
        return cabCompositeSettings;
    }

    public CabWidgetsList getCabWidgetsList() {
        return cabWidgetsList;
    }

    public SignUp getSignUp() {
        return signUp;
    }

    public CabStatsInformersBySites getCabStatsInformersBySites() {
        return cabStatsInformersBySites;
    }

    public CabInformersCode getCabInformersCode() {
        return cabInformersCode;
    }

    public BlockedContent getBlockedContent() {
        return blockedContent;
    }

    public CabInformersManageVideoCfg getCabInformersManageVideoCfg() {
        return manageVideoCfg;
    }

    public CabInformersManageVideo getCabInformersManageVideo() { return manageVideo; }

    public CabEditCode getCabEditCode() { return cabEditCode; }

    public News getNewsClass() {
        return newsClass;
    }

    public CabInformersPreviewMomentary getCabInformersPreviewMomentary() { return cabInformersPreviewMomentary; }

    public CabGoodsSettings getCabGoodsSettings() { return cabGoodsSettings; }

    public CabExchangeSettings getCabExchangeSettings() { return cabExchangeSettings; }

    public AdvertisersBalance getAdvertisersBalance() {return advertisersBalance;}

    public UserProfile getUserProfile() {return userProfile;}

    public CabAllStats getCabAllStats() {return cabAllStats;}

    public DirectDemand getDirectDemand() {return directDemand;}

    public AdsModeration getAdsModeration() {return adsModeration;}

    public Users getUsers() {return users;}
    public Authorization getAuthorization() {return authorization;}

    public TrafficInsights getTrafficInsights() {return trafficInsights;}

    public CabStatsInformersByComposites getCabStatsInformersByComposites() {
        return cabStatsInformersByComposites;
    }

    public CabStatsInformersByCountries getCabStatsInformersByCountries() {
        return cabStatsInformersByCountries;
    }

    public CabStatsInformersByClients getCabStatsInformersByClients() {
        return cabStatsInformersByClients;
    }
}