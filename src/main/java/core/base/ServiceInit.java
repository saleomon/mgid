package core.base;

import libs.devtools.*;
import libs.dockerClient.base.DockerCli;
import libs.dockerClient.base.DockerKafka;
import libs.downloads.base.DownloadedFiles;
import libs.downloads.base.ExportFileTableStructure;
import libs.downloads.base.ExportFileTagStructure;
import libs.screenshotTool.ScreenshotService;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.SessionId;

public class ServiceInit {

    // mock
    private ServicerMock servicerMock;
    private CloudinaryMock cloudinaryMock;
    private GettyImagesMock gettyImagesMock;
    private KeyWeeMock keyWeeMock;
    private IdenfyMock idenfyMock;

    // export files(download csv, xls, exel)
    private ExportFileTableStructure exportFileTableStructure;
    private ExportFileTagStructure exportFileTagStructure;
    private DownloadedFiles downloadedFiles;

    // screenshot
    private ScreenshotService screenshotService;

    // docker container/image
    private DockerCli dockerCli;
    private DockerKafka dockerKafka;

    public ServiceInit() {
    }

    public ServicerMock getServicerMock() {
        servicerMock.init();
        return servicerMock;
    }

    public CloudinaryMock getCloudinaryMock() {
        cloudinaryMock.init();
        return cloudinaryMock;
    }

    public GettyImagesMock getGettyImagesMock() {
        gettyImagesMock.init();
        return gettyImagesMock;
    }

    public KeyWeeMock getKeyWeeMock() {
        keyWeeMock.init();
        return keyWeeMock;
    }

    public IdenfyMock getIdenfyMock() {
        idenfyMock.init();
        return idenfyMock;
    }

    public ScreenshotService getScreenshotService() {
        return screenshotService;
    }

    public ExportFileTableStructure getExportFileTableStructure() {
        return exportFileTableStructure;
    }

    public ExportFileTagStructure getExportFileTagStructure() {
        return exportFileTagStructure;
    }

    public DownloadedFiles getDownloadedFiles() {
        return downloadedFiles;
    }

    public DockerCli getDockerCli() {
        return dockerCli;
    }

    public DockerKafka getDockerKafka() {
        return dockerKafka;
    }

    public void initService(SessionId sessionId, Logger log, MobileEmulationSettings mobileEmulationSettings) {
        servicerMock = new ServicerMock(sessionId, log, mobileEmulationSettings);
        cloudinaryMock = new CloudinaryMock(sessionId, log);
        gettyImagesMock = new GettyImagesMock(sessionId, log);
        keyWeeMock = new KeyWeeMock(sessionId, log);
        idenfyMock = new IdenfyMock(sessionId, log);
        exportFileTableStructure = new ExportFileTableStructure(sessionId, log);
        downloadedFiles = new DownloadedFiles(sessionId, log);
        exportFileTagStructure = new ExportFileTagStructure(sessionId, log);
        dockerCli = new DockerCli();
        dockerKafka = new DockerKafka();
        screenshotService = new ScreenshotService(log);
    }
}
