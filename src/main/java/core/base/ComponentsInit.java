package core.base;

import components.cab.products.logic.AccountMenu;
import components.cab.products.logic.ModerationHints;

public class ComponentsInit {
    private ModerationHints moderationHints;
    private AccountMenu accountMenu;

    protected ComponentsInit() {}

    protected void init(HelpersInit helpersInit) {
        moderationHints = new ModerationHints(helpersInit);
        accountMenu = new AccountMenu(helpersInit);
    }

    public ModerationHints getModerationHints() {
        return moderationHints;
    }

    public AccountMenu getAccountMenu() {
        return accountMenu;
    }

}
