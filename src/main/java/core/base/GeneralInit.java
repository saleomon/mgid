package core.base;

import libs.aws.AwsClient;
import libs.devtools.MobileEmulationSettings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import testData.clickHouse.OperationDataClickHouse;
import testData.mysql.OperationDataMySql;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.hasWebDriverStarted;

public class GeneralInit {

    protected BrowserCapabilities browserCapabilities;
    protected Logger log;
    protected PagesInit pagesInit;
    protected ComponentsInit componentsInit;
    protected ServiceInit serviceInit;
    protected HelpersInit helpersInit;
    protected OperationDataMySql operationMySql;
    protected OperationDataClickHouse operationCh;
    protected AwsClient awsClient;
    protected SessionId sessionId;

    public GeneralInit() {
        browserCapabilities     = new BrowserCapabilities();
        log                     = LogManager.getLogger(getClass());
        pagesInit               = new PagesInit();
        componentsInit          = new ComponentsInit();
        operationMySql          = new OperationDataMySql();
        operationCh             = new OperationDataClickHouse();
        serviceInit             = new ServiceInit();
        helpersInit             = new HelpersInit();
        awsClient               = new AwsClient();
    }

    public void init(String currentClassName, MobileEmulationSettings mobileEmulationSettings) {
        try {
            if (!hasWebDriverStarted()) {
                browserCapabilities.initBrowserCapabilities(currentClassName);
                sessionId = ((RemoteWebDriver) getWebDriver()).getSessionId();
                serviceInit.initService(sessionId, log, mobileEmulationSettings);
                pagesInit.init(helpersInit, log);
                componentsInit.init(helpersInit);
            }
        } catch (Exception e) {
            log.error("Catch " + e);
        }
    }
}
