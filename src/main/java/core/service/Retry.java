package core.service;

import org.testng.*;

public class Retry implements IRetryAnalyzer {
    private int retryCount = 0;

    public boolean retry(ITestResult result) {

        int maxRetryCount = 1;
        if (retryCount < maxRetryCount) {
            retryCount++;
            return true;
        }
        return false;
    }
}

