package core.service.constantTemplates;

public class ConstantsInit {
    //EPICS
    public static final String TEASERS = "Teasers";
    public static final String CAMPAIGNS = "Campaigns";
    public static final String CLIENTS = "Clients";
    public static final String OFFERS = "Offers";
    public static final String FILTERS = "Filters";
    public static final String USERS = "Users";

    //FEATURES
    public static final String TAGS = "Tags";
    public static final String CRM = "CRM";
    public static final String GETTY_IMAGES = "Getty Images";
    public static final String MANUAL_UPLOAD_IMAGES = "Manual Upload Images";
    public static final String IMAGE_EXTENSIONS = "Image Extension";
    public static final String FOCAL_POINT = "Focal Point";
    public static final String IMAGE_FORMAT = "Image Format";
    public static final String IMAGE_VALIDATION = "Image Validation";
    public static final String CSR = "CSR";
    public static final String PAYMENT_INFORMATION = "Payment Information";
    public static final String MODERATORS_HINTS = "Moderators Hints";
    public static final String AUTO_CATEGORY = "Auto Category";
    public static final String COMPLIANT = "Compliant";
    public static final String COPY_TEASER = "Copy Teasers";
    public static final String BLOCK_UNBLOCK = "Block/Unblock";
    public static final String IMAGE_MIRRORING = "Image Mirroring";
    public static final String CERTIFICATES = "Certificates";
    public static final String ADVERT_NAME = "Advert Name";
    public static final String AUTOGENERATION_TITLES = "Autogeneration of Titles";
    public static final String TITLES = "Titles";
    public static final String REJECT = "Reject Teaser";
    public static final String BLOCK_AUTHORIZATION = "Block Authorization";
    public static final String MASS_ACTION = "Mass Action";
    public static final String APPROVE = "Approve";
    public static final String LIMITS_OF_TEASERS = "Limits of teasers";
    public static final String LEGAL_ENTITY = "Legal entity";
    public static final String AA_MCM = "Advertising agency MCM";
    public static final String TITLE_AND_DESCRIPTION = "Title and Description";
    public static final String AA_BONUSES = "Advertising agency Bonuses";



    //STORIES
    public static final String TITLE_TAGS_VERIFICATION_INTERFACE = "Title Tags Verification Interface";
    public static final String TEASER_LIST_CAB_INTERFACE = "Teasers List Cab Interface";
    public static final String TEASER_INLINE_CAB_INTERFACE = "Teasers List Cab Interface";
    public static final String TEASER_CREATE_CAB_INTERFACE = "Teasers Create Cab Interface";
    public static final String TEASER_CREATIVE_ADD_CAB_INTERFACE = "Teasers Creative Add Cab Interface";
    public static final String TEASER_EDIT_CAB_INTERFACE = "Teasers Edit Cab Interface";
    public static final String TEASER_QUARANTINE_ZION_INTERFACE = "Teasers Edit Cab Interface";
    public static final String TEASER_QUARANTINE_INLINE_INTERFACE = "Teasers Edit Cab Interface";
    public static final String TEASER_QUARANTINE_LIST_INTERFACE = "Teasers Edit Cab Interface";
    public static final String TEASER_CREATE_DASH_INTERFACE = "Teasers Create Dash Interface";
    public static final String TEASER_EDIT_DASH_INTERFACE = "Teasers Edit Dash Interface";
    public static final String TEASER_LIST_DASH_INTERFACE = "Teasers List Dash Interface";
    public static final String TEASER_LANDING_OFFERS = "Teaser Landing Offers";
    public static final String CREATIVE_LIST_CAB_INTERFACE = "Creative List Cab Interface";
    public static final String AUTHORIZATION_INTERFACE = "Authorization Interface";
    public static final String ADVERTISER_AGENCIES_LIST_INTERFACE = "Advertising agencies list Interface";
    public static final String ADVERTISER_AGENCY_BONUSES_INTERFACE = "Advertising agency bonuses Interface";
    public static final String USER_EDIT_CAB_INTERFACE = "User Edit Cab Interface";
    public static final String USER_CREATE_CAB_INTERFACE = "User Create Cab Interface";


    //CLIENTS
    public static final String CLIENT_GOODHITS_EDIT_INTERFACE = "Client Goodhits Edit Interface";
    public static final String CLIENT_GOODHITS_LIST_INTERFACE = "Client Goodhits List Interface";
    public static final String CLIENT_CRM_LIST_INTERFACE = "Client CRM List Interface";
    public static final String CLIENT_CRM_CREATE_INTERFACE = "Client CRM Create Interface";
    public static final String CLIENT_CRM_EDIT_INTERFACE = "Client CRM Edit Interface";

    //OWNERS
    public static final String NIO = "NIO";
    public static final String RKO = "RKO";
    public static final String MVV = "MVV";
    public static final String AIA = "AIA";
    public static final String HOV = "HOV";
    public static final String KOB = "KOB";

}
