package core.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    private final Properties property;

    private PropertiesManager() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
        property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final PropertiesManager INSTANCE = new PropertiesManager();

    public static String getResourceByName(ConfigValue name){
        return INSTANCE.property.getProperty(name.getValue());
    }

    public enum ConfigValue {
        HOST("host"),
        MYSQL_HOST("mySql_host_db"),
        MYSQL_PORT("mySql_port_db"),
        MYSQL_USER("mySql_user_db"),
        MYSQL_PASS("mySql_pass_db"),
        CLICKHOUSE_HOST("clickHouse_host_db"),
        CLICKHOUSE_PORT("clickHouse_port_db"),
        MONGO_HOST("mongo_host_db"),
        MONGO_PORT("mongo_port_db"),
        CI_JOB_ID("ci_job_id"),
        DOCKER_SOCKET("docker_socket");

        private final String value;

        ConfigValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
