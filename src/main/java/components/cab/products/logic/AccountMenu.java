package components.cab.products.logic;

import core.base.HelpersInit;

import static components.cab.products.locators.AccountMenuLocators.USER_NAME_LABEL;

public class AccountMenu {
    private final HelpersInit helpersInit;

    public AccountMenu(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public boolean isDisplayedUserAccountLabel() {
        return USER_NAME_LABEL.isDisplayed();
    }
}
