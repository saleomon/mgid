package components.cab.products.logic;

import core.base.HelpersInit;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static components.cab.products.locators.ModerationHintsLocators.*;
import static core.helpers.ErrorsHelper.checkErrors;

public class ModerationHints {

    private final HelpersInit helpersInit;

    public ModerationHints(HelpersInit helpersInit) {
        this.helpersInit = helpersInit;
    }

    public int getAmountOfHints() {
        return Integer.parseInt(HINTS_COUNTER.text());
    }

    public boolean isDisplayHintsCounter() {
        return HINTS_COUNTER.isDisplayed();
    }

    public void openHintsBox() {
        NOTIFICATION_ICON.click();
        checkErrors();
    }

    public void closeTeaserHintWithId(int teaserId) {
        $("[data-e2e='hint-elem-" + teaserId + "'] [icon='tuiIconCloseLarge']").click();
        checkErrors();
    }

    public String getLinkFromHint(int hintId) {
        return $("[data-e2e='hint-elem-" + hintId + "'] a").attr("href");
    }

    public void clearAllNotifications() {
        CLEAR_ALL_NOTIFICATION.click();
        checkErrors();
    }

    public boolean checkVisibilityHintWithTeaser(int hintId) {
        return $("[data-e2e='hint-elem-" + hintId + "']").isDisplayed();
    }

    public boolean checkNotificationMessage(int teaserId, String message) {
        sleep(1000);
        return helpersInit.getBaseHelper().checkDatasetEquals($("[data-e2e='hint-elem-" + teaserId + "'] [class='t-content']").text().trim(), message);
    }

    public boolean isDisplayedNotificationIcon() {
        return NOTIFICATION_ICON.isDisplayed();
    }

}
