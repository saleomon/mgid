package components.cab.products.locators;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ModerationHintsLocators {
    public static final SelenideElement HINTS_COUNTER = $("mgx-panel-moderation-hints [class*=badged-content]");
    public static final SelenideElement NOTIFICATION_ICON = $("[icon='mguiIconNotifications']");
    public static final SelenideElement CLEAR_ALL_NOTIFICATION = $("[data-e2e='clear-all']");

}
