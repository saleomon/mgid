package testData.clickHouse;

public class ClickHouseDbNames {

    public enum ClickHouseDbs {
        STATS("stats"),
        DICTIONARIES("dictionaries"),
        PRICE_ENGINE("price_engine");

        private final String dbName;

        ClickHouseDbs(String targetValue) {
            this.dbName = targetValue;
        }

        public String getTypeValue() {
            return dbName;
        }
    }
}
