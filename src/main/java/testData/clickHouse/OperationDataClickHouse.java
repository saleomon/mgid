package testData.clickHouse;

import libs.clickhouse.connections.ClickHouseSession;
import libs.clickhouse.queries.ConversionQueryBuilder;

public class OperationDataClickHouse extends ClickHouseSession{
    private ConversionQueryBuilder conversionQueryBuilder;

    /**
     * isHaveAlreadyData() - check ClickHouse if has data
     * insertDamp - add new damp in this method
     */
    public OperationDataClickHouse insertBaseData(){
        if(!isHaveAlreadyData()) {

            //stats
            insertDamp("clickHouse/widget_statistics_summ.sql");
            insertDamp("clickHouse/conversion.sql");
            insertDamp("clickHouse/widget_clicks_goods.sql");
            insertDamp("clickHouse/publishers_statistics.sql");
            insertDamp("clickHouse/widget_statistics_clicks_iexchange.sql");
            insertDamp("clickHouse/widget_statistics_shows_rtb_summ.sql");
            insertDamp("clickHouse/widget_statistics_shows_cpm_summ.sql");
            insertDamp("clickHouse/widget_statistics_google_ads.sql");
            insertDamp("clickHouse/widget_statistics_video.sql");
            insertDamp("clickHouse/advertiser_statistics.sql");
            insertDamp("clickHouse/reach_meter_data_website.sql");
            insertDamp("clickHouse/reach_meter_data_push.sql");
            insertDamp("clickHouse/teaser_statistics_summ.sql");
            insertDamp("clickHouse/direct_publisher_demand.sql");
            insertDamp("clickHouse/site_statistics.sql");
            insertDamp("clickHouse/clients_muidn.sql");

            //selective binding
            insertDamp("clickHouse/advertisers_selective_bidding.sql");

            //push stats
            insertDamp("clickHouse/audience_hub_events.sql");
            insertDamp("clickHouse/push_statistics_events.sql");

            //price_engine
            setDbNames(ClickHouseDbNames.ClickHouseDbs.PRICE_ENGINE).insertDamp("clickHouse/price_engine/auction_data_min_weights.sql");

        }
        return this;
    }

    public OperationDataClickHouse initDbS(){
        conversionQueryBuilder = new ConversionQueryBuilder();
        return this;
    }

    public ConversionQueryBuilder getConversionQueryBuilder() {
        return conversionQueryBuilder;
    }
}
