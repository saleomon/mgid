package testData.project;

public class EndPoints {

    public static final String cabLink              = "http://local-admin.mgid.com/cab/";
    public static final String cabLinkSignIn        = "http://local-admin.mgid.com/auth/sign-in";
    public static final String mgidLink             = "http://local-dashboard.mgid.com";
    public static final String epeexLink            = "http://local-panel.epeex.io";
    public static final String adskeeperLink        = "http://local-dashboard.adskeeper.co.uk";
    public static final String adgageLink           = "http://local-panel.adgage.es";
    public static final String idealmediaIoLink     = "http://local-dashboard.idealmedia.io";
    public static final String widgetTemplateUrl    = "http://local-apache.com/stands/%s.html";
    public static final String ampTemplateUrl       = "http://local-amp.com:8000/examples/%s.html";

    /////////////// PUBLISHERS CLIENTS /////////////////////////////////
    public static final String clientWagesEditUrl = "wages/clients-edit/id/";

    /////////////// PUBLISHERS SITES /////////////////////////////////
    public static final String sitesWagesListUrl = "wages/sites?id=";
    public static final String sitesWagesEditUrl = "wages/sites-edit/id/";

    /////////////// PUBLISHERS WIDGETS /////////////////////////////////
    public static final String widgetsWidgetsUrl = "wages/widgets";
    public static final String widgetsCustomIdUrl = "wages/widgets?c_id=";

    //Goods widget setting URL
    public static final String goodWidgetSettingUrl = "wages/informers-edit/type/goods/uid/";

    //Tickers composite settings edit URL
    public static final String compositeEditUrl = "wages/informers-edit/type/composite/id/";

    //Exchange widget settings URL
    public static final String exchangeWidgetSettingsUrl = "wages/informers-edit/type/news/id/";

    //Edit code URL
    public static final String editCodeUrl = "wages/informers-settings/id/";

    //Widget List Settings
    public static String wagesWidgetsUrl(Subnets.SubnetType subnetId, int clientId){
        if(subnetId.getTypeValue().contains("-")){
            String[] mass = subnetId.getTypeValue().split("-");
            return "wages/widgets/?subnet={\"subnet\":" + mass[0] + ",\"mirror\":\"" + mass[1] + "\"}" +
                    "&client_id=" + clientId;
        }
         return "wages/widgets/?subnet={\"subnet\":" + subnetId.getTypeValue() + ",\"mirror\":null}" +
                 "&client_id=" + clientId;
    }

    public static String wagesWidgetsUrl(Subnets.SubnetType subnetId, int clientId, String domain){
        return wagesWidgetsUrl(subnetId, clientId) +
                "&domain=" + domain;
    }

}
