package testData.project;

public class ClientsEntities {
    /////////////////////////// CLIENTS CREDENTIALS ////////////////////////////
    public static final String CLIENTS_EMAIL                = "sok.autotest@mgid.com";
    public static final String CLIENTS_EPEEX_EMAIL          = "sok.autotest.epeex@mgid.com";
    public static final String CLIENTS_ADGAGE_EMAIL         = "sok.autotest.adgage@mgid.com";
    public static final String CLIENTS_IDEALMEDIA_IO_EMAIL  = "sok.idealmedia_io@mgid.com";
    public static final String CLIENTS_PASSWORD             = "xTrnoYtVc";

    public static String CLIENTS_PAYOUTS_LOGIN = "sok.autotest.payouts@mgid.com";
    public static String CLIENTS_PAYOUTS_EURO_LOGIN = "sok.autotest.payouts.eur@mgid.com";
    public static String CLIENTS_PAYOUTS_ASIA_LOGIN = "payments.asia.client.2015@mgid.com";
    public static String CLIENTS_ADS_TXT_LOGIN = "for.ads.txt.publisher.2021@mgid.com";
    public static String CLIENTS_VIDEO_LOGIN = "video.widgets.publisher@test.com";
    public static String CLIENTS_PAYOUTS_UKRAINE_LOGIN = "sok.autotest.payouts.ukraine@mgid.com";
    public static String CLIENT_FOR_DISABLE_403_ONLY = "payments.asia.client.2016@mgid.com";

    //For registration in dashboard
    public static String NEW_ADVERTISER_LOGIN_MGID = "self.register.advertiser@test.com";
    public static String NEW_ADVERTISER_LEGAL_ENTITY_MGID = "self.register.advertiser.legal@test.com";
    public static String NEW_CLIENT_LOGIN_MGID = "self.register.client@test.com";
    public static String NEW_PUBLISHER_LOGIN_MGID = "self.register.publisher.mgid@test.com";
    public static String NEW_PUBLISHER_LEGAL_ENTITY_MGID = "self.register.publisher.mgid.legal@test.com";

    public static String NEW_ADVERTISER_LOGIN_ADSKEEPER = "self.register.advertiser.adskeeper@test.com";
    public static String NEW_PUBLISHER_LOGIN_ADSKEEPER = "self.register.publisher.adskeeper@test.com";

    public static String NEW_PUBLISHER_LOGIN_IDEALMEDIA_IO = "self.register.publisher.idealmedia.io@test.com";

    public static String NEW_ADVERTISER_LOGIN_ADGAGE = "self.register.advertiser.adgage@test.com";

    public static String NEW_ADVERTISER_LOGIN_EPEEX = "self.register.advertiser.epeex@test.com";
    public static String NEW_PUBLISHER_LOGIN_EPEEX = "self.register.publisher.epeex@test.com";

    public static String NEW_CLIENT_PASSWORD = "1q2w3e4r";
    public static String NEW_RESTORED_PASSWORD = "4r3e2w1q";

    public static String NEW_PUBLISHER_DOMAIN_MGID = "self-register-publisher-mgid.com";

    public static String NEW_PUBLISHER_DOMAIN_EPEEX = "self-register-publisher-epeex.com";

    ////////////////////////////// CLIENTS /////////////////////////////////
    public static int CLIENT_MGID_ID = 1;

    public static int CLIENT_MGID_RUB_ID = 5;
    public static int CLIENT_MGID_UAH_ID = 6;
    public static int CLIENT_MGID_EUR_ID = 7;
    public static int CLIENT_MGID_INR_ID = 8;

    //clients for payouts
    public static int CLIENT_MGID_USD_ID = 2001;
    public static int CLIENT_ADSKEEPER_USD_ID = 2002;
    public static int CLIENT_ADSKEEPER_PAYOUTS_ID = 2028;

    public static int CLIENT_IDEALMEDIA_USD_ID = 2003;

    public static int CLIENT_MGID_EURO_ID = 2004;
    public static int CLIENT_MGID_PAYOUTS_ID = 2006;

    ///////////////////////////// SITES ////////////////////////////////////

    public static int WEBSITE_MGID_WAGES_ID = 1;
    public static String WEBSITE_MGID_WAGES_DOMAIN = "testsitemg.com";

    public static int WEBSITE_ADSKEEPER_WAGES_ID = 2;
    public static String WEBSITE_ADSKEEPER_WAGES_DOMAIN = "testsiteadsk.com";

    public static int WEBSITE_IDEALMEDIA_WAGES_ID = 4;
    public static String WEBSITE_IDEALMEDIA_WAGES_DOMAIN = "testsiteideal.com";

    public static int WEBSITE_MGID_WAGES_INT_EXCHANGE_ID = 5;
    public static String WEBSITE_MGID_GOODHITS_DOMAIN = "testsitegoodsmg.com";

    public static int WEBSITE_MGID_WAGES_ID_REJECTED = 17;

    public static int WEBSITE_FOR_USD_CLIENT_MGID = 2001;
    public static int WEBSITE_FOR_USD_CLIENT_ADSKEEPER = 2002;
    public static int WEBSITE_FOR_INTERNAL_NEWS_MGID = 2004;

    public static int WEBSITE_MGID_WITHOUT_ADS_TXT_ID = 2003;

    public static int WEBSITE_MGID_FOR_ADS_TXT_ID = 2006;

    public static int WEBSITE_MGID_FOR_VIDEO_ID = 2008;
    public static int WEBSITE_IDEALMEDIA_FOR_VIDEO_ID = 2009;

    ///////////////////////////// EXCHANGE CAMPAIGNS ////////////////////////
    public static int EXCHANGE_CAMPAIGN_IDEALMEDIA_0_ID = 1;
    public static int EXCHANGE_CAMPAIGN_IDEALMEDIA_ID = 2;

    //////////////////////////////////// CAMPAIGNS //////////////////////////////

    public static  int CAMPAIGN_MGID_PRODUCT = 1;

    public static  int CAMPAIGN_MGID_CONTENT = 2;

    public static  int CAMPAIGN_MGID_SEARCH_FEED = 1001;

    public static  int CAMPAIGN_MGID_PUSH = 3;

    public static  int CAMPAIGN_MGID_CPM = 1120;

    /////////////////////////////////// TEASERS  //////////////////////////////////
    public static  int TEASER_MGID_APPROVE = 1;

    public static  int TEASER_MGID_1_APPROVE = 3;
    public static  int TEASER_MGID_2_APPROVE = 4;

    public static  int TEASER_PUSH_1_APPROVE = 11;
    public static  int TEASER_PUSH_2_APPROVE = 12;

    //////////////////////////// WIDGETS //////////////////////
    //mgid
    public static int MGID_WIDGET_UNDER_ARTICLE_ID = 1;
    public static int MGID_WIDGET_IN_ARTICLE_ID = 2;
    public static int MGID_WIDGET_IN_ARTICLE_WITH_DETAILED_ID = 94;
    public static int MGID_WIDGET_HEADLINE_ID = 3;
    public static int MGID_WIDGET_HEADER_ID = 4;
    public static int MGID_WIDGET_SIDEBAR_ID = 5;
    public static int MGID_WIDGET_SIDEBAR_WITH_DETAILED_ID = 93;
    public static int MGID_WIDGET_BANNER_ID = 6;
    public static int MGID_WIDGET_IN_ARTICLE_ID_WITH_CHILD = 23;
    public static int MGID_WIDGET_SMART_ID = 101;
    public static int MGID_WIDGET_IN_SITE_NOTIFICATION_ID = 418;

    //adskeeper
    public static int ADSKEEPER_WIDGET_UNDER_ARTICLE_ID = 7;
    public static int ADSKEEPER_WIDGET_HEADER_ID = 8;
    public static int ADSKEEPER_WIDGET_SIDEBAR_ID = 9;
    public static int ADSKEEPER_WIDGET_POPUP_ID = 10;
    public static int ADSKEEPER_WIDGET_IN_SITE_NOTIFICATION_ID = 11;
    public static int ADSKEEPER_WIDGET_MOBILE_EXIT_ID = 47;
    public static int ADSKEEPER_WIDGET_IN_ARTICLE_ID = 56;
    public static int ADSKEEPER_WIDGET_FEED_ID = 62;

    //IDEALMEDIA_IO
    public static int IDEALMEDIA_WIDGET_UNDER_ARTICLE_ID = 12;
    public static int IDEALMEDIA_WIDGET_HEADER_ID = 13;
    public static int IDEALMEDIA_WIDGET_SIDEBAR_ID = 14;
    public static int IDEALMEDIA_WIDGET_SIDEBAR_WITH_DETAILED_ID = 91;
    public static int IDEALMEDIA_WIDGET_IN_ARTICLE_ID = 15;
    public static int IDEALMEDIA_WIDGET_IN_ARTICLE_WITH_DETAILED_ID = 92;
    public static int IDEALMEDIA_WIDGET_TEXT_ON_IMAGE_ID = 16;
    public static int IDEALMEDIA_WIDGET_BANNER_ID = 17;
    public static int IDEALMEDIA_WIDGET_CAROUSEL_ID = 18;
    public static int IDEALMEDIA_WIDGET_SMART_ID = 102;

    public static final int MGID_PUSH_WIDGET_ID = 19;
    // next free tickers_composite.id = 21 (push need 3 ids)

}
