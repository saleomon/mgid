package testData.project;

public class CliCommandsList {

    public enum Cron {
        //Крон подсчета количества тизеров для РК по разным статусам
        COUNT_TEASERS_FOR_RK("campaigns-goods:hits-counts"),
        //Крон подсчёта хешей
        CALCULATE_HASH("teasers:calculate-perceptual-hash"),
        //Клонирование виджетов
        CLONE_WIDGETS("informers:widgets-clone"),
        //Обнуляет товарные категории для виджетов
        BULK_ACTION("widgets:bulk-action"),
        //Крон создает отчет для отправки клиенту на почту
        AUTO_REPORT("clients:publisher-stat-reports"),
        //Крон закриття заявок
        CLEAR_CREATIVE_ORDERS("clear-creative-orders"),
        //Крон перегенерации виджетов
        RECOMPILE_WIDGETS_CRON("informers:recompile-tasks:handle"),
        //Крон копирования виджетов
        COPY_TEASERS("teaser-goods:copy-teaser"),
        AUTO_APROVE("teasers-moderation:auto-approving"),
        UPDATE_TEASER_TO_MODERATION("teasers:moderation:moderate-client-unblocked"),
        //Крон отклонения тизеров для заблокированного клиента
        REJECT_TEASERS_BLOCKED_CLIENTS("teasers:moderation:reject-client-blocked"),
        LANDING_DOMAINS("teasers-goods:landing-domains"),
        CAMPAIGN_SHOW_NAME_TEMP("campaigns-goods:set-product-campaign-show-name-temp"),
        MIRROR_IMAGE_CHANGE("teasers-goods:mirror-image-change"),
        RULES_BASED_OPTIMIZATION("campaigns-goods:apply-rule-based-optimization"),
        MODERATOR_HINTS("teasers-moderation:make-moderators-hints"),
        IDENFY_SUSPECTED_CRON("antifraud:client:kyc:handle-suspected");

        private final String cronValue;

        public String getCronValue() {
            return cronValue;
        }

        Cron(String cronValue) {
            this.cronValue = cronValue;
        }

    }

    public enum Consumer {
        //Консьюмер обновления дублированых тайтлов дескрипшенов
        TEASERS_UPDATE_DUPLICATE("consumer:teasers:update-duplicates-spell-error-fix"),
        //Консьюмер обновления тегов для тизеров с одинаковим хешом
        TEASERS_UPDATE_TAGS("consumer:teaser-goods:update-teasers-tags-from-donor"),
        TEASERS_LANDING_TAGS("consumer:teasers-goods:landing:change-client-campaigns-landing"),
        //Консьюмер копирования тизеров
        GENERATE_IMAGE_HASH("consumer:teaser:generate-image-hash"),
        //Консьюмер перегенерации виджетов
        RECOMPILE_WIDGETS_CONSUMER("consumer:widgets:recompile"),
        //Консьюмер перегенерации віджетів з шаблонами
        TEMPLATE_REFILL_WIDGETS_CONSUMER("consumer:widgets:template-refill"),
        //Консьюмер изменения ad type
        AD_TYPE_CHANGING("consumer:teasers-goods:mass-edit:change-ad-type"),
        //Consumer for linking advertiser and offer name
        OFFER_CONSUMER("consumer:teaser-goods:offers:update-show-name-offers"),
        //Consumer for linking advertiser and offer name
        RELATED_IMAGES("consumer:news:generate-image-hash"),
        BULK_ACTION_UNBLOCK("consumer:bulk-actions:batches:teaser:unblock"),
        BULK_ACTION_UPDATE_TITLES("consumer:bulk-actions:batches:teaser:update-title-part"),
        BULK_ACTION_UPDATE_TITLES1("consumer:bulk-actions:batches:teaser:update_title"),
        BULK_ACTION_BLOCK("consumer:bulk-actions:batches:teaser:block"),
        BULK_ACTION_MAIN("consumer:bulk-actions:main"),
        BULK_ACTION_HANDLER("bulk-actions:handler");

        private final String cronValue;

        public String getConsumerValue() {
            return cronValue;
        }

        Consumer(String cronValue) {
            this.cronValue = cronValue;
        }
    }
}
