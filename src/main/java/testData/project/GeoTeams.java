package testData.project;

public class GeoTeams {

    public enum GeoTeam {
        INDIA("india"),
        APAC("apac"),
        CIS("cis"),
        EUROPE("europe"),
        LA("la"),
        VN("vn");


        private final String geoTeamValue;

        GeoTeam(String geoTeamValue) {
            this.geoTeamValue = geoTeamValue;
        }

        public String getGeoTeamValue() {
            return geoTeamValue;
        }

    }
}
