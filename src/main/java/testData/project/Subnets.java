package testData.project;

public class Subnets {

    ///////////////// SUBNETS(name) /////////////////////////////////////

    public static final String SUBNET_MGID_NAME             = "Mgid";
    public static final String SUBNET_ADSKEEPER_NAME        = "Adskeeper";
    public static final String SUBNET_IDEALMEDIA_IO_NAME    = "Idealmedia";


    public enum SubnetType {

        SCENARIO_MGID("0"),
        SCENARIO_EPEEX("0-epeex"),
        SCENARIO_ADGAGE("0-adgage"),
        SCENARIO_ADSKEEPER("2"),
        SCENARIO_IDEALMEDIA_IO("3-idealmedia");
        private final String typeValue;

        SubnetType(String type) {
            typeValue = type;
        }

        public static SubnetType getType(String pType) {
            for (SubnetType type: SubnetType.values()) {
                if (type.getTypeValue().equals(pType)) {
                    return type;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getTypeValue() {
            return typeValue;
        }
    }
}
