package testData.project;

public class RoleIdsDash {
    public static Integer ADMIN_MGID_ROLE = 6;
    public static Integer MANAGER_MGID_ROLE = 7;
    public static Integer ADMIN_ADSKEEPER_ROLE = 1;
    public static Integer ADMIN_IDEALMEDIA_ROLE = 16;
    public static Integer MANAGER_IDEALMEDIA_ROLE = 17;
}
