package testData.project;

public class ResourcePaths {
    /////////////////////////////////  Paths for images  ///////////////////////////////
    public static final String LINK_TO_RESOURCES_SCREENSHOTS = "src/main/resources/expectedScreenshots/";
    public static final String LINK_TO_RESOURCES_FILES = "src/main/resources/files/";
    public static final String LINK_TO_RESOURCES_IMAGES = "src/main/resources/images/";
    public static final String LINK_IMAGE = "http://www.bugaga.ru/uploads/posts/2016-08/1471555196_subbota-25.jpg";
}
