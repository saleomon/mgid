package testData.project.publishers;

import testData.project.Subnets.SubnetType;

public class VideoCfgData {

    SubnetType subnet;
    String servicerUrl;

    public VideoCfgData() {
        this.subnet = SubnetType.SCENARIO_MGID;
        this.servicerUrl = "mgid.com";
    }

    public VideoCfgData(SubnetType subnet) {
        this.subnet = subnet;
        if(this.subnet == SubnetType.SCENARIO_IDEALMEDIA_IO){
            this.servicerUrl = "idealmedia.io";
        }
    }


    public String generateInstreamCfgJson(int widgetId, boolean enableDesktop, boolean enableMobile){
        String formats;
        String desktopInstreamFormat = "{\"performance\":true,\"loop\":true,\"sliderAlign\":\"bottom right\",\"name\":\"native\",\"device\":\"desktop\",\"maximp\":1,\"sticky\":true,\"sliderSize\":\"s\"}";
        String mobileInstreamFormat = "{\"performance\":true,\"loop\":true,\"sliderAlign\":\"bottom\",\"name\":\"native\",\"device\":\"mobile\",\"sticky\":true}";
        if(enableDesktop && enableMobile){
            formats = desktopInstreamFormat + "," + mobileInstreamFormat;
        }
        else {
            formats = enableDesktop ? desktopInstreamFormat : mobileInstreamFormat;
        }

        String prepareInstreamJson =
                "{" +
                    "\"vpaid\": {" +
                        "\"adsLimit\": 1," +
                        "\"totalTimeout\": 60," +
                        "\"singleTimeout\": 30," +
                        "\"maxSinglePlayers\": 5" +
                    "}," +
                    "\"enabled\": \"1\"," +
                    "\"formats\": [" +
                        "%s" +
                    "]," +
                    "\"adPlayers\": [" +
                        "{" +
                            "\"name\": \"vpaidPlayer\"," +
                            "\"engine\": \"vpaid\"," +
                            "\"adSource\": \"https://servicer.%s/vpaid/%s?pl=1&w={width}&h={height}&maxduration=30&dnt={dnt}&page={url}&uspString={ccpa}&consentData={gdpr_consent}&format={format}&schedule={schedule}&ref={referer_url}\"," +
                            "\"skipoffset\": 5" +
                        "}" +
                    "]," +
                    "\"autoStart\": \"visibleNotPause\"," +
                    "\"adSchedule\": [" +
                        "{" +
                            "\"offset\": 0," +
                            "\"adPlayers\": [" +
                                "\"vpaidPlayer\"" +
                            "]" +
                        "}," +
                        "{" +
                            "\"offset\": \"pre\"," +
                            "\"loadAds\": \"pageLoaded\"," +
                            "\"adPlayers\": [" +
                                "\"vpaidPlayer\"" +
                            "]" +
                        "}" +
                    "]," +
                    "\"preset_type\": \"instream\"" +
                "}";
        return String.format(prepareInstreamJson, formats, this.servicerUrl, widgetId);
    }

    public String generateOutstreamCfgJson(int widgetId, boolean enableDesktop, boolean enableMobile){
        String formats;
        String desktopOutstreamFormat = "{\"name\":\"outstream\",\"device\":\"desktop\",\"maximp\":1,\"sticky\":false,\"teaserHeight\":\"1\",\"maximp\":1,\"teaserIndex\":\"0\",\"moveToBody\":false,\"teaserSize\":\"1\",\"adsPlace\":\"over\"}";
        String mobileOutStreamFormat = "{\"name\":\"outstream\",\"device\":\"mobile\",\"sticky\":false,\"teaserHeight\":1,\"maximp\":1,\"teaserIndex\":0,\"teaserSize\":1,\"adsPlace\":\"over\"}";
        if(enableDesktop && enableMobile){
            formats = desktopOutstreamFormat + "," + mobileOutStreamFormat;
        }
        else {
            formats = enableDesktop ? desktopOutstreamFormat : mobileOutStreamFormat;
        }

        String prepareOutstreamJson =
                "{" +
                    "\"vpaid\": {" +
                        "\"adsLimit\": 1," +
                        "\"totalTimeout\": 60," +
                        "\"singleTimeout\": 30," +
                        "\"maxSinglePlayers\": 5" +
                    "}," +
                    "\"enabled\": \"1\"," +
                    "\"formats\": [" +
                        "%s" +
                    "]," +
                    "\"adPlayers\": [" +
                        "{" +
                            "\"name\": \"vpaidPlayer\"," +
                            "\"engine\": \"vpaid\"," +
                            "\"adSource\": \"https://servicer.%s/vpaid/%s?pl=1&w={width}&h={height}&maxduration=30&dnt={dnt}&page={url}&uspString={ccpa}&consentData={gdpr_consent}&format={format}&schedule={schedule}&ref={referer_url}\"," +
                            "\"skipoffset\": 5" +
                        "}" +
                    "]," +
                    "\"autoStart\": \"visibleNotPause\"," +
                    "\"preset_type\": \"outstream-over\"" +
                "}";
        return String.format(prepareOutstreamJson, formats, this.servicerUrl, widgetId);
    }
}
