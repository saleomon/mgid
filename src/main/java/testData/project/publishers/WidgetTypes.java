package testData.project.publishers;

public class WidgetTypes {

    public enum Types {
        SMART("smart"),
        HEADLINE("headline-in-picture"),
        HEADER("header-widget"),
        UNDER_ARTICLE("under-article-widget"),
        SIDEBAR("sidebar-widget"),
        BANNER("banner"),
        MOBILE("mobile-widget"),
        FEED("feed"),
        EXIT_POP_UP("exit-pop-up"),
        MOBILE_EXIT("mobile-exit"),
        IN_SITE_NOTIFICATION("in-site-notification"),
        PASSAGE("passage"),
        TEXT_ON_IMAGE("text-on-image"),
        CAROUSEL("carousel-super"),
        IN_ARTICLE("in-article");

        private final String type;

        Types(String type) {
            this.type = type;
        }

        public static Types getType(String target) {
            for(Types t : Types.values()){
                if(t.getTypeValue().equals(target)){
                    return t;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getTypeValue() {
            return type;
        }
    }

    public enum SubTypes {

        UNDER_ARTICLE_CARDS("under-article-widget-cards"),
        UNDER_ARTICLE_PAIR_CARDS("under-article-widget-pair-cards"),
        UNDER_ARTICLE_LIGHT_CARDS("under-article-widget-light-cards"),
        UNDER_ARTICLE_RECTANGULAR("under-article-widget-rectangular"),
        UNDER_ARTICLE_SQUARE("under-article-widget-square"),
        UNDER_ARTICLE_SQUARE_BLACK("under-article-widget-square-black"),
        UNDER_ARTICLE_MAIN("under-article-widget-main"),
        UNDER_ARTICLE_LIGHT("under-article-widget-light"),
        UNDER_ARTICLE_DARK("under-article-widget-dark"),
        UNDER_ARTICLE_CARD_MEDIA("under-article-widget-card-media"),

        IN_ARTICLE_MAIN("in-article-main"),
        IN_ARTICLE_CAROUSEL("in-article-carousel-super"),
        IN_ARTICLE_CAROUSEL_NEW("in-article-carousel-new"),
        IN_ARTICLE_IMPACT("in-article-impact"),
        IN_ARTICLE_DOUBLE_PICTURE("in-article-double-picture"),
        IN_ARTICLE_CARD_MEDIA("in-article-card-media"),

        SMART_MAIN("smart-main"),
        SMART_BLUR("smart-blur"),
        SMART_PLUS("smart-plus"),

        FEED_MAIN("feed-main"),
        FEED_LIGHT("feed-light"),

        HEADER_RECTANGULAR("header-widget-rectangular"),
        HEADER_SQUARE("header-widget-square"),
        HEADER_LIGHT("header-widget-light"),
        HEADER_DARK("header-widget-dark"),

        SIDEBAR_CARDS("sidebar-widget-cards"),
        SIDEBAR_LIGHT("sidebar-widget-light"),
        SIDEBAR_DARK("sidebar-widget-dark"),
        SIDEBAR_RECTANGULAR_2("sidebar-widget-rectangular-2"),
        SIDEBAR_RECTANGULAR("sidebar-widget-rectangular"),
        SIDEBAR_SQUARE_SMALL("sidebar-widget-square-small"),
        SIDEBAR_IMPACT("sidebar-widget-impact"),
        SIDEBAR_BLUR("sidebar-widget-blur"),
        SIDEBAR_FRAME("sidebar-widget-frame"),

        BANNER_728x90_1("banner-728x90_1"),
        BANNER_728x90_2("banner-728x90_2"),
        BANNER_300x250_4("banner-300x250_4"),
        BANNER_300x250_3("banner-300x250_3"),
        BANNER_300x250_2("banner-300x250_2"),
        BANNER_300x250_1("banner-300x250_1"),
        BANNER_320x100("banner-320x100"),
        BANNER_320x50("banner-320x50"),
        BANNER_470x325("banner-470x325"),
        BANNER_970x250("banner-970x250"),
        BANNER_970x90("banner-970x90"),
        BANNER_120x600("banner-120x600"),
        BANNER_160x600("banner-160x600"),
        BANNER_240x400("banner-240x400"),
        BANNER_300x600("banner-300x600"),

        CAROUSEL_MAIN("carousel-super-main"),
        CAROUSEL_VERTICAL("carousel-super-vertical"),

        IN_SITE_NOTIFICATION_MAIN("in-site-notification-main"),
        IN_SITE_NOTIFICATION_MEDIA("in-site-notification-media-grey"),
        IN_SITE_NOTIFICATION_CHAT("in-site-notification-chat"),

        NONE("none");


        private final String type;

        SubTypes(String type) {
            this.type = type;
        }

        public static SubTypes getType(String target) {
            for(SubTypes t : SubTypes.values()){
                if(t.getTypeValue().equals(target)){
                    return t;
                }
            }
            throw new RuntimeException("unknown type");
        }

        public String getTypeValue() {
            return type;
        }
    }

}

