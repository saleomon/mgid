package testData.project.publishers;

import java.util.Arrays;
import java.util.List;

public class ServicerData {

    public static String videoLibVersion = "1.11.";

    public static final List<String> teasersList = Arrays.asList(
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Teaser 10\",\"Tired\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser 9\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser 8\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser 7\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser 6\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser 5\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"4\",\"1\",\"Teaser 4\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/4/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"3\",\"1\",\"Teaser 3\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/3/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"2\",\"1\",\"Teaser 2\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/2/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"1\",\"1\",\"Teaser 1\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/1/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Teaser 10\",\"Tired\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser 9\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser 8\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser 7\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser 6\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser 5\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"4\",\"1\",\"Teaser 4\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/4/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"3\",\"1\",\"Teaser 3\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/3/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"2\",\"1\",\"Teaser 2\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/2/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"1\",\"1\",\"Teaser 1\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/1/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"10\",\"78\",\"Teaser 10\",\"Tired\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/10/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"9\",\"78\",\"Teaser 9\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/9/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"8\",\"78\",\"Teaser 8\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/8/i/78/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"7\",\"1\",\"Teaser 7\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/7/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"6\",\"1\",\"Teaser 6\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/6/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"5\",\"1\",\"Teaser 5\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/5/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"4\",\"1\",\"Teaser 4\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/4/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"3\",\"1\",\"Teaser 3\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/3/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"2\",\"1\",\"Teaser 2\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/2/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]",
            "[\"Campaign Altz hvdbq\",\"1\",\"1\",\"Teaser 1\",\"\",\"0\",\"\",\"\",\"\",\"rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.mgid.com/ghits/1/i/1/0/pp/2/2?h=rPtuGGeMEE3gSUTzU3Iao-2Ut5Yyv6Fuc4JkgEq1PZZXmvH7hlpHqE_u8gOiW1PH&rid=e3e068e9-80ad-11ea-99b1-d094662f8ab5\",\"adc\":[],\"sdl\":1,\"dl\":\"\",\"type\":\"w\",\"clicktrackers\":[],\"cta\":\"Learn more\"}\n]"
    );

    public static final List<String> newsList = Arrays.asList(
            "[\"testsitelenta.com\",\"3\",\"1\",\"Newstitle_3\",\"Description_3\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/3/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"4\",\"1\",\"Newstitle_4\",\"Description_4\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/4/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"5\",\"1\",\"Newstitle_5\",\"Description_5\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/5/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"6\",\"1\",\"Newstitle_6\",\"Description_6\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/6/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"7\",\"1\",\"Newstitle_7\",\"Description_7\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/7/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"8\",\"1\",\"Newstitle_8\",\"Description_8\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/8/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"9\",\"1\",\"Newstitle_9\",\"Description_9\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/9/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"10\",\"1\",\"Newstitlepnl\",\"Descriptiontesttxm\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/10/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"11\",\"1\",\"Newstitledzt\",\"Descriptiontesttjf\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/11/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"12\",\"1\",\"Newstitlebmh\",\"Descriptiontestthg\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/12/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"20\",\"1\",\"Newstitle_20\",\"Description_1\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/20/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"9\",\"1\",\"Newstitlealk\",\"Descriptiontestmjh\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/9/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"10\",\"1\",\"Newstitlepnl\",\"Descriptiontesttxm\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/10/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"11\",\"1\",\"Newstitledzt\",\"Descriptiontesttjf\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/11/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"12\",\"1\",\"Newstitlebmh\",\"Descriptiontestthg\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/12/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"3\",\"1\",\"Newstitle_3\",\"Description_3\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/3/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"4\",\"1\",\"Newstitle_4\",\"Description_4\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/4/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"5\",\"1\",\"Newstitle_5\",\"Description_5\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/5/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"6\",\"1\",\"Newstitle_6\",\"Description_6\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/6/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"7\",\"1\",\"Newstitle_7\",\"Description_7\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/7/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"8\",\"1\",\"Newstitle_8\",\"Description_8\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/8/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"9\",\"1\",\"Newstitle_9\",\"Description_9\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/9/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"10\",\"1\",\"Newstitlepnl\",\"Descriptiontesttxm\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/10/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"11\",\"1\",\"Newstitledzt\",\"Descriptiontesttjf\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/11/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"12\",\"1\",\"Newstitlebmh\",\"Descriptiontestthg\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/12/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"20\",\"1\",\"Newstitle_20\",\"Description_1\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/20/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"9\",\"1\",\"Newstitlealk\",\"Descriptiontestmjh\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/9/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"10\",\"1\",\"Newstitlepnl\",\"Descriptiontesttxm\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/10/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"11\",\"1\",\"Newstitledzt\",\"Descriptiontesttjf\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/11/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"12\",\"1\",\"Newstitlebmh\",\"Descriptiontestthg\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/12/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]",
            "[\"testsitelenta.com\",\"20\",\"1\",\"Newstitle_20\",\"Description_1\",\"0\",\"\",\"\",\"\",\"LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/monochromeImage.jpg\",\"l\":\"//local.idealmedia.com/pnews/20/i/12/pp/5/3/?h=LTM0GIn5SWAWBBdkxd5nBFrbPvMaqxflNXn4QIilortrZtmjp6bJ03uFJqEZ_Kd9&utm_campaign=testformigrfate.com&utm_source=testformigrfate.com&utm_medium=referral&rid=3e849f56-846c-11ea-964e-d09466576dad&ts=admin.mgid.com&tt=Referral&cpm=1\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"category\":\"Automotive\",\"type\":\"e\",\"clicktrackers\":[],\"cta\":\"Узнать больше\"}\n]"
    );

    public static String servicerDataStart = "_mgq.push([\"%sLoadGoods%s\",[\n";
    public static String servicerDataEnd = "]," +
            "{" +
            "\"awc\":{}," +
            "\"dt\":\"desktop\"," +
            "\"ts\":\"\"," +
            "\"tt\":\"Direct\"," +
            "\"isBot\":0," +
            "\"h2\":\"2PEU-hPIYsqs1nRMNiIvQFlBrYxyUp2E5JVj1l4HvK8*\"," +
            "\"ats\":0," +
            "\"rid\":\"e3e068e9-80ad-11ea-99b1-d094662f8ab5\"," +
            "\"pvid\":\"%s\"," +
            "\"muidn\":\"%s\"," +
            "\"dnt\":0," +
            "\"cv\":3";
    public static String servicerDataIconBlockTeaser = "_mgq.push([\"%sCReject%s\"]);";
    public static String servicerDataEmpty = "var _mgq=_mgq||[]; _mgq.push(['%sDefaultComposite%s']);_mgqp();";
    public static String videoData = "[\"\",\"603\",\"1\",\"Is Facebook Secretly Building Satellite Internet?\",\"Facebook is quietly pushing ahead with plans to deploy a satellite network. Does SpaceX know about it? What is the reaction? Is it a digital war starting between the two giants?\",\"0\",\"\",\"\",\"\",\"qTkCz4m9eyjUPlwpCeYE6w36LshGXkbWDVwWmGEh6YvO2kQxRKsyQSG5Oj70srItkjAsiBiIizYgVihN8b8BCQ**\",{\"i\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"l\":\"//video-native.mgid.com/provided_video/2018-07-02/3829db3f127847d34a05f816eb665e42.mp4\",\"adc\":[],\"sdl\":0,\"dl\":\"\",\"type\":\"v\",\"media-type\":\"static\",\"clicktrackers\":[],\"cta\":\"\",\"cdt\":\"\"}]";


    public static String getVideoDataEnd(Object widgetId, boolean enabledAd, String formats, String libVersion){
        return "],{" +
                "\"awc\":{}," +
                "\"dt\":\"desktop\"," +
                "\"ts\":\"admin.mgid.com\"," +
                "\"tt\":\"Referral\"," +
                "\"isBot\":0," +
                "\"h2\":\"BG8XnXgq8paLGz1wQY7TOtZQZS9zQCJ_qvXhfG6aXTU*\"," +
                "\"ats\":0," +
                "\"rid\":\"451f3fa8-512b-11ed-bdf3-e43d1a2a04aa\"," +
                "\"pvid\":\"183fa176299b0bfffb7\"," +
                "\"iv\":11," +
                "\"brid\":5," +
                "\"muidn\":\"m7dqYIYlaa0d\"," +
                "\"dnt\":0," +
                "\"cv\":3," +
                "\"config\":" +
                    "{" +
                        "\"vpaid\": " +
                            "{" +
                                "\"adsLimit\": 2, " +
                                "\"totalTimeout\": 60, " +
                                "\"singleTimeout\": 30, " +
                                "\"maxSinglePlayers\": 5" +
                            "}, " +
                        "\"enabled\": \"1\", " +
                        "\"formats\": " +
                            "[" +
                                formats +
                            "], " +
                        "\"adPlayers\": " +
                            "[" +
                                "{" +
                                    "\"name\": \"vpaidPlayer\", " +
                                    "\"engine\": \"vpaid\", " +
                                    "\"adSource\": \"https://servicer.mgid.com/vpaid/" + widgetId + "?pl=1&w={width}&h={height}&maxduration=30&dnt={dnt}&page={url}&uspString={ccpa}&consentData={gdpr_consent}&format={format}&schedule={schedule}&ref={referer_url}\", " +
                                    "\"skipoffset\": 5" +
                                "}" +
                            "], " +
                        "\"autoStart\": \"visibleNotPause\", " +
                        "\"adSchedule\": " +
                            "[" +
                                (enabledAd ? adSchedule : "") +
                            "]," +
                        "\"uuid\":\"451f3fa8-512b-11ed-bdf3-e43d1a2a04aa\"," +
                        "\"subid\":0," +
                        "\"parentCid\":" + widgetId + "," +
                        "\"vast\":[\"//servicer.mgid.com/" + widgetId + "/?vast=1\"]," +
                        "\"wages_types\":\"video_content,goods\"," +
                        "\"templateId\":0" +
                    "}," +
                "\"vr_playlist\":" +
                    "[" +
                        "{\"id\":\"1072\",\"title\":\"На Эвересте образовалась пробка из людей\",\"img\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"link\":\"//video-native.mgid.com/provided_video/2019-05-24/df77dd99da044dbfa5130bf98a3cd138.mp4\",\"hash\":\"\",\"videoExt\":\"eyJpbXByZXNzaW9ucyI6Mjg1MzAwMjQsImR1cmF0aW9uIjozNiwiZGVzY3JpcHRpb24iOiJcXHUwNDIyXFx1MDQ0YlxcdTA0NDFcXHUwNDRmXFx1MDQ0N1xcdTA0MzBcXHUwNDNjIFxcdTA0MzZcXHUwNDM1XFx1MDQzYlxcdTA0MzBcXHUwNDRlXFx1MDQ0OVxcdTA0MzhcXHUwNDQ1IFxcdTA0M2ZcXHUwNDNlXFx1MDQzYVxcdTA0M2VcXHUwNDQwXFx1MDQzOFxcdTA0NDJcXHUwNDRjIFxcdTA0MzNcXHUwNDNlXFx1MDQ0MFxcdTA0M2RcXHUwNDQzXFx1MDQ0ZSBcXHUwNDMyXFx1MDQzNVxcdTA0NDBcXHUwNDQ4XFx1MDQzOFxcdTA0M2RcXHUwNDQzIFxcdTA0M2ZcXHUwNDQwXFx1MDQzOFxcdTA0NDhcXHUwNDNiXFx1MDQzZVxcdTA0NDFcXHUwNDRjIFxcdTA0NDdcXHUwNDMwXFx1MDQ0MVxcdTA0MzBcXHUwNDNjXFx1MDQzOCBcXHUwNDQxXFx1MDQ0MlxcdTA0M2VcXHUwNDRmXFx1MDQ0MlxcdTA0NGMgXFx1MDQzMiBcXHUwNDNlXFx1MDQ0N1xcdTA0MzVcXHUwNDQwXFx1MDQzNVxcdTA0MzRcXHUwNDM4LiBcXHUwNDI1XFx1MDQzZVxcdTA0NDBcXHUwNDNlXFx1MDQ0OFxcdTA0MzBcXHUwNDRmIFxcdTA0M2ZcXHUwNDNlXFx1MDQzM1xcdTA0M2VcXHUwNDM0XFx1MDQzMCBcXHUwNDQxXFx1MDQzZlxcdTA0M2VcXHUwNDQxXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQ0MlxcdTA0MzJcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNiXFx1MDQzMCBcXHUwNDNmXFx1MDQzZVxcdTA0NGZcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0NGUgXFx1MDQ0MVxcdTA0M2VcXHUwNDQyXFx1MDQzNVxcdTA0M2QgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzZlxcdTA0M2YgXFx1MDQzMFxcdTA0M2JcXHUwNDRjXFx1MDQzZlxcdTA0MzhcXHUwNDNkXFx1MDQzOFxcdTA0NDFcXHUwNDQyXFx1MDQzZVxcdTA0MzIiLCJ0aHVtYm5haWwiOiJodHRwczovL3MtaW1nLm1naWQuY29tL2wvLS80OTJ4Mjc3Ly0vYUhSMGNEb3ZMM1pwWkdWdkxXNWhkR2wyWlM1dFoybGtMbU52YlM5MmNtbHRZV2RsY3k5eWRTOHlNREU1TFRBMUxUSTBMelEyTm1WaU1EWTBNemd4T1ROaU16ZGlPR1kxTVRBeE4ySTVaREZrWldFMUxtcHdady5qcGciLCJncG1kQ2F0ZWdvcnkiOnsiY2F0MSI6MCwiY2F0MiI6MH19\"}," +
                        "{\"id\":\"3017\",\"title\":\"Here's Why We Are Addicted To Cake Decoration Videos\",\"img\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"link\":\"//video-native.mgid.com/provided_video/2020-08-04/c0becef0c3722a4a2b3cd56eb046b67e.mp4\",\"hash\":\"\",\"videoExt\":\"eyJpbXByZXNzaW9ucyI6MTEzMjMyMjQsImR1cmF0aW9uIjo1MiwiZGVzY3JpcHRpb24iOiJIb3cgY2FuIGFueW9uZSBzdGF5IGluZGlmZmVyZW50IHRvIHRoZXNlIHNhdGlzZnlpbmcgdmlkZW9zPyBUb2dldGhlciB3aXRoIHNsaW1lcyBhbmQgY3V0dGluZyBzb2FwIHRoZXkgYmVjYW1lIG91ciBmYXZvdXJpdGUgY29udGVudCBpbiBhbGwgc29jaWFsIG5ldHdvcmtzIiwidGh1bWJuYWlsIjoiaHR0cHM6Ly9zLWltZy5tZ2lkLmNvbS9sLy0vNDkyeDI3Ny8tL2FIUjBjRG92TDNacFpHVnZMVzVoZEdsMlpTNXRaMmxrTG1OdmJTOTJjbWx0WVdkbGN5OTFjeTh5TURJd0xUQTRMVEEwTHpReE56SXhPV015WVRreE1UWTNNMlJrWkRZeE56TmxOVGRsWXpVM09HUm1MbXB3WncuanBnIiwiZ3BtZENhdGVnb3J5Ijp7ImNhdDEiOjAsImNhdDIiOjB9fQ==\"}," +
                        "{\"id\":\"3123\",\"title\":\"Top 5 Of The Most Instagrammed Destinations\",\"img\":\"http://local.s3.eu-central-1.amazonaws.com/Stonehenge.jpg\",\"link\":\"//video-native.mgid.com/provided_video/2020-10-09/5c5de02b12dc611c9346daa561716ba2.mp4\",\"hash\":\"\",\"videoExt\":\"eyJpbXByZXNzaW9ucyI6MTEzNzAyNzksImR1cmF0aW9uIjo4OCwiZGVzY3JpcHRpb24iOiJXaGVuIHlvdSBjaGVjayB0aGUgaGFzaHRhZ3MgLSB5b3Ugd2lsbCBzZWUgdGhhdCBtb3N0IG9mIHRyYXZlbCBibG9nZ2VycyB2aXNpdCB0aGUgc2FtZSBzcG90cyBpbiBkaWZmZXJlbnQgdGltZS4gVGhlc2UgYXJlIHRoZSBwbGFjZXMgd2hlcmUgb25lIGNhbiBnZXQgbWFydmVsbG91cyBpbWFnZXMiLCJ0aHVtYm5haWwiOiJodHRwczovL3MtaW1nLm1naWQuY29tL2wvLS80OTJ4Mjc3Ly0vYUhSMGNEb3ZMM1pwWkdWdkxXNWhkR2wyWlM1dFoybGtMbU52YlM5MmNtbHRZV2RsY3k5MWN5OHlNREl3TFRFd0xUQTVMMkk1TURkbU1UUTNZek5rT1RKallqRm1ZVE5rTkRRNE1UQmxOVEE0TlRaa0xtcHdady5qcGciLCJncG1kQ2F0ZWdvcnkiOnsiY2F0MSI6MCwiY2F0MiI6MH19\"}" +
                    "]," +
                "\"lib\":\"" + libVersion + "\"" +
                "}]);";
    }

    public static String vpaidData(Object widgetId, String vpaidDspList, String nvpaidParameters) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<VAST version=\"2.0\">" +
                        "<Ad id=\"VPAID\">" +
                            "<InLine>" +
                                "<AdSystem version=\"1.11\">MGVPAID</AdSystem>" +
                                "<AdTitle>VPAID CAT</AdTitle>" +
                                "<Creatives>" +
                                    "<Creative>" +
                                        "<Linear>" +
                                            "<MediaFiles>" +
                                                "<MediaFile delivery=\"progressive\" type=\"application/javascript\" scalable=\"1\" apiFramework=\"VPAID\" width=\"220\" height=\"124\"><![CDATA[ https://video-native.mgid.com/scripts/mgvpaid.umd.js ]]></MediaFile>" +
                                            "</MediaFiles>" +
                                            "<Duration>00:00:23</Duration>" +
                                            "<AdParameters>" +
                                                "<![CDATA[ " +
                                                    "{" +
                                                    "\"cid\":" + widgetId + "," +
                                                    "\"uuid\":\"21f0109a-9ee6-11ed-854d-e43d1a2a96ea\"," +
                                                    "\"domain\":\"video-native.mgid.com\"," +
                                                    "\"vpaidDspList\":[" +
                                                        vpaidDspList +
                                                    "]," +
                                                    "\"maxSinglePlayers\":5," +
                                                    "\"totalTimeout\":60," +
                                                    "\"singleTimeout\":30," +
                                                    "\"adsLimit\":2" +
                                                    nvpaidParameters +
                                                    "}" +
                                                "]]>" +
                                            "</AdParameters>" +
                                        "</Linear>" +
                                    "</Creative>" +
                                "</Creatives>" +
                            "</InLine>" +
                        "</Ad>" +
                    "</VAST>";
    }
    /**
     * nvpaid => native backfill
     * Params to String format:
     * 1. domain
     * 2. widgetId
     * 3. siteId
     * 4. widgetId
     */
    public static String nvpaidParameters(String domain, String siteId, Object widgetId) {
        return ",\"nvpaidParameters\":" +
                "{" +
                    "\"file\":\"http://local.s3.eu-central-1.amazonaws.com/local/t/e/" + domain + "." + widgetId + ".js\"," +
                    "\"id\":\"M" + siteId + "ScriptRootC" + widgetId + "\"," +
                    "\"duration\":5," +
                    "\"styleUrl\":\"https://video-native.mgid.com/mgPlayer/css/mgvpaid.css\"" +
                "}";
    }

    public static List<String> vpaidDspList = Arrays.asList(
                    "{\"url\":\"https://pubads.g.doubleclick.net/gampad/ads?iu=/21775744923/external/single_preroll_skippable\\u0026sz=640x480\\u0026ciu_szs=300x250%2C728x90\\u0026gdfp_req=1\\u0026output=vast\\u0026unviewed_position_start=1\\u0026env=vp\\u0026impl=s\\u0026correlator=\",\"showHash\":\"uDN6cwtbk0ZinW-1h_N4K2YbD7whltdmRH4rMEIzAcYBN9JyyKaAluhpg81POFcFrsC5zB7ThejeLq3ov8Afyw**\",\"price\":9980}",
                    "{\"url\":\"https://pubads.g.doubleclick.net/gampad/ads?iu=/21775744923/external/single_preroll_skippable\\u0026sz=640x480\\u0026ciu_szs=300x250%2C728x90\\u0026gdfp_req=1\\u0026output=vast\\u0026unviewed_position_start=1\\u0026env=vp\\u0026impl=s\\u0026correlator=\",\"showHash\":\"k_-pQ6vkO7T1MRyd5SS1pGYbD7whltdmRH4rMEIzAcb_N1z_jfmxwfYUG6jQOBR0ROFJza7lSax3hUJ4sqqWlg**\",\"price\":1000}"
    );

    private static final String adSchedule =
                "{" +
                    "\"offset\": 0, " +
                    "\"adPlayers\": " +
                        "[" +
                            "\"vpaidPlayer\"" +
                        "]" +
                "}, " +
                "{" +
                    "\"offset\": \"pre\", " +
                    "\"loadAds\": \"pageLoaded\", " +
                    "\"adPlayers\": " +
                        "[" +
                            "\"vpaidPlayer\"" +
                        "]" +
                "}";

    public static String videoFormat =
            "{" +
                "\"loop\": true, " +
                "\"name\": \"native\", " +
                "\"device\": \"%s\", " +
                "\"maximp\": 1, " +
                "\"sticky\": false, " +
                "\"inArticle\": false, " +
                "\"expandable\": false, " +
                "\"moveToBody\": false, " +
                "\"clickToPlay\": true, " +
                "\"performance\": true, " +
                "\"inlinePlacement\": 1, " +
                "\"closeButtonEnable\": 1" +
            "}";
}

