package testData.project;

public class OthersData {

    public static final String LINK_TO_RESOURCES_FILES = "src/main/java/testData/files/";
    public static final String MAX_STRING_VALUE_256 = "dfhlsshdfasjhdfoertuoerjglkdfgkdsklfglkndslkfnlgdgsdfgsdfgsdfgsdfgsrtergdfbcxcvfgsdfgsdgfdsfgtyjjkhjkhgjkhjkhjkvxcvzsrgdrgdfgsdfgdsfghdfghfghdfghdfghdfgnfdthdsdfasgasfgdfgsdfgfdghtyujtmkgfjkhgkjghddzkljdshfjksndfjasndkfjnakssdnfjnjsndfjnsdddjjjjJJJJJJJJJJJ";
    public static final String mgidLogo = "https://cdn.mgid.com/images/mgid/mgid_ua.svg";
    public static final String adskeeperLogo = "https://cdn.adskeeper.co.uk/images/adskeeper_svg.svg";
    public static final String idealmediaIoLogo = "http://idealmedia.io/";
    public static final String idealmediaIoBannerLogo = "https://cdn.idealmedia.io/images/idealmedia/circle_logo_io.png";
    public static final String timeZone = "Pacific/Honolulu";
}
