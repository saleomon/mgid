package testData.project;

public class AuthUserCabData {
    public static final String userPasswordCab = "hHPhMNoNxG";
    public static final String userLoginCab = "user_auto";

    //RA
    public static final String advertisingAgencyUser = "AA_agency";

    //others
    public static final String wagesCurator = "sokwages";
    public static final String goodhitsCurator = "sokgoodhits";
    public static final String indiaProductUser = "radjiev.arambytan";
    public static final String apacPublisherUser = "serg.Kovac";
    public static final String cisProductUser = "quatar.Help";
    public static final String europeProductUser = "mariush.werpakovick";
    public static final String indiaPublisherUser = "aby.babyn";
    public static final String cisPublisherUser = "djordj.maikl";
    public static final String europePublisherUser = "yoka.ono";

    public enum AuthorizationUsers {
        GENERAL_USER("user_auto"),
        PRIVILEGE_USER("user_auto_privileges"),
        AA_USER("AA_agency"),
        TEASER_MAKER_USER_1("beaver_with_beer"),
        TEASER_MAKER_USER_2("herring_in_fur_coat"),
        TEASER_MODERATION_USER("horse_in_coat"),
        NEWS_MAKER_STAFFER_USER("mila.jovovich"),
        NEWS_MODERATION_USER("zlata.ognevich"),
        ADMIN_USER("user_admin"),
        AA_FAT_CAT("fat.cat"),
        BRAVE_RACOON("brave.racoon"),
        RECOVERY_PASS("recovery_pass");

        private final String loginUser;

        AuthorizationUsers(String loginUser) {
            this.loginUser = loginUser;
        }
        public String getLoginUser() {
            return loginUser;
        }
    }
}