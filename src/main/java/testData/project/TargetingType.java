package testData.project;

public class TargetingType {

    public enum Targeting {
        GEO("location"),
        BROWSER("browser"),
        BROWSER_LANGUAGE("language"),
        DEVICES_OS("os"),
        PROVIDER("provider"),
        PUBLISHER_CATEGORIES("platform"),
        DEMOGRAPHICS("socdem"),
        INTERESTS("interest"),
        BUNDLES("bundle"),
        MOBILE_CONNECTION("connectionType"),
        AUDIENCES("audience"),
        TRAFFIC_TYPE("trafficType"),
        VIEWABILITY("viewability"),
        CONTEXT("context"),
        SENTIMENTS("sentiments"),
        PHONE_PRICE_RANGES("phonePriceRange");

        private final String targetValue;

        Targeting(String targetValue) {
            this.targetValue = targetValue;
        }

        public String getTypeValue() {
            return targetValue;
        }
    }
}
