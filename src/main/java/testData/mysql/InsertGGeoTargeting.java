package testData.mysql;

import libs.hikari.tableQueries.partners.GGeoTargeting;
import libs.hikari.tableQueries.partners.GPartners1;
import libs.hikari.tableQueries.partners.MaxmindGeoipCountries;
import libs.hikari.tableQueries.partners.MaxmindGeoipRegions;
import libs.hikari.utils.PartnersInstructions;
import testData.project.TargetingType.Targeting;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static libs.hikari.utils.UtilsHelper.joinListToString;

public class InsertGGeoTargeting {

    GGeoTargeting gGeoTargeting = new GGeoTargeting();
    GPartners1 gPartners1 = new GPartners1();
    MaxmindGeoipCountries maxmindGeoipCountries = new MaxmindGeoipCountries();
    MaxmindGeoipRegions maxmindGeoipRegions = new MaxmindGeoipRegions();

    public void insertUpdateGeoTargetingInclude(int campaignId, String... name){
        List<String> listName = arrayToListFixCountries(name);

        String countries = joinListToString(maxmindGeoipCountries.getCountriesId(listName));
        String regions = joinListToString(maxmindGeoipRegions.getListRegionsByCountry(countries));
        String zonesIds = joinListToString(maxmindGeoipRegions.getListZonesByCountries(countries));

        // is g_geo_targeting has campaign_id -> update
        // is g_geo_targeting doesn't have campaign_id -> insert
        if(PartnersInstructions.isEmptySelect("select enabled from g_geo_targeting where id =" + campaignId)) {
            PartnersInstructions.getConnectionPartnersExecuteQuery(
                    "update g_geo_targeting set " +
                             "enabled = '" + regions + "', " +
                             "enabled_countries = '" + countries + "', " +
                             "enabled_geo_zones = '" + zonesIds + "', " +
                             "mode = 'include' where id = " + campaignId);
        } else {
            String oldRegions = gGeoTargeting.getEnabledRegions(campaignId);
            gGeoTargeting.updateGGeoTargeting(campaignId, countries, regions, oldRegions, zonesIds, "include");

        }
        gPartners1.updateGeo(campaignId, Targeting.GEO, "include");
    }

    public void insertUpdateGeoTargetingIncludeRegions(int campaignId, String... name){
        List regionNames = Arrays.asList(name);

        String countries = joinListToString(maxmindGeoipRegions.getListCountriesByRegion(regionNames));
        String regions = joinListToString(maxmindGeoipRegions.getListRegionsByName(regionNames));
        String zonesIds = joinListToString(maxmindGeoipRegions.getListZonesByRegions(regionNames));

        // is g_geo_targeting has campaign_id -> update
        // is g_geo_targeting doesn't have campaign_id -> insert
        if(PartnersInstructions.isEmptySelect("select enabled from g_geo_targeting where id =" + campaignId)) {
            PartnersInstructions.getConnectionPartnersExecuteQuery(
                    "update g_geo_targeting set " +
                             "enabled = '" + regions + "', " +
                             "enabled_countries = '" + countries + "', " +
                             "enabled_geo_zones = '" + zonesIds + "', " +
                             "mode = 'include' where id = " + campaignId);
        } else {
            String oldRegions = gGeoTargeting.getEnabledRegions(campaignId);
            gGeoTargeting.updateGGeoTargeting(campaignId, countries, regions, oldRegions, zonesIds, "include");

        }
        gPartners1.updateGeo(campaignId, Targeting.GEO, "include");
    }

    public void insertUpdateGeoTargetingExclude(int campaignId, String... name){
        List<String> listName = arrayToListFixCountries(name);

        String countries = joinListToString(maxmindGeoipCountries.getExcludeCountriesId(listName));
        String regions = joinListToString(maxmindGeoipRegions.getListRegionsByCountry(countries));
        String zonesIds = joinListToString(maxmindGeoipRegions.getListZonesByCountries(countries));

        // is g_geo_targeting has campaign_id -> update
        // is g_geo_targeting doesn't have campaign_id -> insert
        if(PartnersInstructions.isEmptySelect("select enabled from g_geo_targeting where id =" + campaignId)) {
            PartnersInstructions.getConnectionPartnersExecuteQuery(
                    "update g_geo_targeting set " +
                            "enabled = '" + regions + "', " +
                            "enabled_countries = '" + countries + "', " +
                            "enabled_geo_zones = '" + zonesIds + "', " +
                            "mode = 'exclude' where id = " + campaignId);
        } else {
            String oldRegions = gGeoTargeting.getEnabledRegions(campaignId);
            gGeoTargeting.updateGGeoTargeting(campaignId, countries, regions, oldRegions, zonesIds, "exclude");
        }
        gPartners1.updateGeo(campaignId, Targeting.GEO, "exclude");
    }

    private List<String> arrayToListFixCountries(String... name){
        List<String> list = new LinkedList<>(Arrays.asList(name));
        if(Arrays.asList(name).contains("United States")){
            list.add("Virgin Islands, U.S.");
        }
        return list;
    }
}
