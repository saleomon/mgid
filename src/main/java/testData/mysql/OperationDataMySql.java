package testData.mysql;


import libs.hikari.tableQueries.admin.Users;
import libs.hikari.tableQueries.partners.*;

import static libs.hikari.tableQueries.admin.Users.selectIsExistCustomUserCab;
import static libs.hikari.utils.PartnersInstructions.pushDumpToTable;
import static testData.project.AuthUserCabData.userLoginCab;
import static testData.project.publishers.ServicerData.videoLibVersion;

public class OperationDataMySql {

    private InsertGGeoTargeting insertGGeoTargeting;
    private GPartners1 gPartners1;
    private GHits1 gHits1;
    private GHitsRejections gHitsRejections;
    private Users users;
    private OfferCertificates offerCertificates;
    private TeasersOffers teasersOffers;
    private TeasersModerationScore teasersModerationScore;
    private MailPull mailPull;
    private CategoryPlatform categoryPlatform;
    private CopyRequestTeasers copyRequestTeasers;
    private CreativeOrders creativeOrders;
    private ClientsSites clientsSites;
    private ClientsSitesDefaultWidgetCategories clientsSitesDefaultWidgetCategories;
    private ClientsSitesStatuses clientsSitesStatuses;
    private Partners1 partners1;
    private ClientsSitesPostModeration clientsSitesPostModeration;
    private News1 news1;
    private GBlocks gblocks;
    private GBlocksCampaignTier gBlocksCampaignTier;
    private TickersComposite tickersComposite;
    private GBlocksGeoZonesPricesLimits gBlocksGeoZonesPricesLimits;
    private GBlocksCountryProperties gBlocksCountryProperties;
    private GBlocksSubnets gBlocksSubnets;
    private GBlocksCategories gBlocksCategories;
    private GCategory gCategory;
    private GBlocksTeasersFilter gBlocksTeasersFilter;
    private Clients clients;
    private BundlesGBlocks bundlesGBlocks;
    private TickersCompositePushRules tickersCompositePushRules;
    private ClientsPurses clientsPurses;
    private SensorsTargets sensorsTargets;
    private Bundles bundles;
    private ClientsSitesBundles clientsSitesBundles;
    private GHitsTags gHitsTags;
    private ModeratorsHints moderatorsHints;
    private TickersCompositePlacementConfiguration tickersCompositePlacementConfiguration;
    private ClientsSitesTier clientsSitesTier;
    private ClientsSitesExcludedTags clientsSitesExcludedTags;
    private Sources sources;
    private CurrentStatus currentStatus;
    private Payments payments;
    private ClientsAutoBilling clientsAutoBilling;
    private RecompileTasks recompileTasks;
    private GlobalSettings globalSettings;
    private WidgetsSlackNotificationSettings widgetsSlackNotificationSettings;
    private SubnetMirrors subnetMirrors;
    private TickersCategories tickersCategories;
    private TickersFilters tickersFilters;
    private Tickers tickers;
    private MaxmindGeoipCountries maxmindGeoipCountries;
    private TeasersLandingsOffers teasersLandingsOffers;
    private DirectPublisherDemandAds directPublisherDemandAds;
    private PublishersAdsModeration publishersAdsModeration;
    private TickersCompositeRelations tickersCompositeRelations;
    private TickersCompositeBackfill tickersCompositeBackfill;
    private TickersCompositeDoubleclick tickersCompositeDoubleclick;
    private TickersCompositeAttributes tickersCompositeAttributes;
    private TickersCompositeTransitRecaptcha tickersCompositeTransitRecaptcha;
    private PaymentsPack paymentsPack;
    private KycIdenfy kycIdenfy;
    private IndividualBonusesConditionsAA individualBonusesConditionsAA;

    public OperationDataMySql() {}

    public OperationDataMySql insertBaseData(){
        initDbS();
            if(!selectIsExistCustomUserCab(userLoginCab)) {
            importDumpData();
        }
        getVideoLibVersionFromDb();
        return this;
    }

    private void importDumpData() {
        //auth section
        pushDumpToTable("mysql/auth/mgid_clients.sql");
        pushDumpToTable("mysql/auth/mgid_jwt_keys.sql");

        //crouch
        pushDumpToTable("mysql/partners/campaigns/clients_sites_traffic_types_default.sql");

        //general section
        pushDumpToTable("mysql/general/proxy_ip.sql");

        //admin section
        pushDumpToTable("mysql/admin/users/users.sql");
        pushDumpToTable("mysql/admin/users/roles_users.sql");
        pushDumpToTable("mysql/admin/roles/roles.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_136.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_218.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_75.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_76.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_53.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_84.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_25.sql");
        pushDumpToTable("mysql/admin/roles/roles_privileges_development_105.sql");

        //partners section
        pushDumpToTable("mysql/partners/sources/sources.sql");
        pushDumpToTable("mysql/partners/general/acl_roles_rights_dashboard.sql");
        pushDumpToTable("mysql/partners/general/black_ip_exclude.sql");
        pushDumpToTable("mysql/partners/general/global_settings.sql");
        pushDumpToTable("mysql/partners/general/geo_team_countries_config.sql");
        pushDumpToTable("mysql/partners/general/transit_templates.sql");
        pushDumpToTable("mysql/partners/clients/clients_auth_password_hash.sql");
        pushDumpToTable("mysql/partners/clients/createClientTables/clients.sql");
        pushDumpToTable("mysql/partners/clients/createClientTables/clients_contacts.sql");
        pushDumpToTable("mysql/partners/clients/createClientTables/clients_auth.sql");
        pushDumpToTable("mysql/partners/clients/createClientTables/clients_curators.sql");
        pushDumpToTable("mysql/partners/clients/createClientTables/acl_dashboard.sql");
        pushDumpToTable("mysql/partners/clients/clients_auto_billing.sql");
        pushDumpToTable("mysql/partners/clients/client_payment_terms_file.sql");
        pushDumpToTable("mysql/partners/sites/clients_sites.sql");
        pushDumpToTable("mysql/partners/sites/clients_sites_default_widget_categories.sql");
        pushDumpToTable("mysql/partners/sites/clients_sites_excluded_tags.sql");
        pushDumpToTable("mysql/partners/sites/clients_sites_bundles.sql");
        pushDumpToTable("mysql/partners/campaigns/variables/targeting_template.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns.sql");
        pushDumpToTable("mysql/partners/campaigns/external_dsps.sql");
        pushDumpToTable("mysql/partners/campaigns/campaign_targeting.sql");
        pushDumpToTable("mysql/partners/sources/g_partners_1_sources_quality_factors.sql");
        pushDumpToTable("mysql/partners/sources/g_partners_1_sources_quality.sql");
        pushDumpToTable("mysql/partners/sources/g_partners_sources_quality.sql");
        pushDumpToTable("mysql/partners/certificate/certificate.sql");
        pushDumpToTable("mysql/partners/certificate/campaign_certificates.sql");
        pushDumpToTable("mysql/partners/certificate/certificate_files.sql");
        pushDumpToTable("mysql/partners/certificate/client_landing_certificates.sql");
        pushDumpToTable("mysql/partners/campaigns/max_cpc.sql");
        pushDumpToTable("mysql/partners/clients/ssps.sql");
        pushDumpToTable("mysql/partners/general/users_geo_team_counties_config.sql");
        pushDumpToTable("mysql/partners/retention/retention_tool_rules.sql");

        //ads.txt
        pushDumpToTable("mysql/partners/sites/clients_sites_ads_txt.sql");
        pushDumpToTable("mysql/partners/sites/ad_networks_ads_txt.sql");
        pushDumpToTable("mysql/partners/sites/ad_networks_clients_sites.sql");

        //audiences and targets
        pushDumpToTable("mysql/partners/audiences/sensors_audiences.sql");
        pushDumpToTable("mysql/partners/audiences/sensors_targets.sql");
        pushDumpToTable("mysql/partners/audiences/sensors_audience_targets.sql");
        pushDumpToTable("mysql/partners/audiences/sensors_target_conditions.sql");

        //campaigns targeting
        pushDumpToTable("mysql/partners/campaigns/campaigns_browsers_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_category_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_connection_types_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_languages_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_os_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_bundles_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_providers_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_socdem_targeting.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_conversions.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_sources_filters.sql");
//        jdbc.importDumpPartnersSection("mysql/partners/campaigns/g_partners_1_statistics.sql");

        //Video campaigns event URLs
        pushDumpToTable("mysql/partners/campaigns/g_partners_1_vast_dsp.sql");

        //teasers
        pushDumpToTable("mysql/partners/creative/variables_creative_orders.sql");
        pushDumpToTable("mysql/partners/creative/creative_orders.sql");
        pushDumpToTable("mysql/partners/creative/creative_tasks.sql");
        pushDumpToTable("mysql/partners/news/news_1.sql");
        pushDumpToTable("mysql/partners/news/news_1_keywords.sql");
        pushDumpToTable("mysql/partners/news/news_image_hashes.sql");
        pushDumpToTable("mysql/partners/news/news_image_hash_related.sql");

        pushDumpToTable("mysql/partners/teasers/g_hits_image_perceptual_hashes.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_image_hash_related.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_1.sql");
        pushDumpToTable("mysql/partners/teasers/teasers_moderation_score.sql");
        pushDumpToTable("mysql/partners/teasers/spell_check_data.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_rejections.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_1_creative_safety_rankings.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_tags.sql");
        pushDumpToTable("mysql/partners/teasers/tags_collection.sql");
        pushDumpToTable("mysql/partners/teasers/g_hits_autorejections.sql");

        //offers
        pushDumpToTable("mysql/partners/offers/teasers_landings_offers.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings_scan.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings_scan_stat.sql");
        pushDumpToTable("mysql/partners/offers/g_partners_1_teasers_landings.sql");
        pushDumpToTable("mysql/partners/offers/landing_url_screenshot.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings_screenshots_offers.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings_offers_geo.sql");
        pushDumpToTable("mysql/partners/offers/teasers_landings_offer_names.sql");
        pushDumpToTable("mysql/partners/teasers/teasers_offers.sql");
        pushDumpToTable("mysql/partners/offers/landing_screen_project.sql");
        pushDumpToTable("mysql/partners/certificate/offer_certificates.sql");
        pushDumpToTable("mysql/partners/offers/landing_html_hashes.sql");
        pushDumpToTable("mysql/partners/offers/landing_plagiarism_checks.sql");

        pushDumpToTable("mysql/partners/offers/teasers_landings_landing_path.sql");
        pushDumpToTable("mysql/partners/offers/top_level_domains.sql");
        pushDumpToTable("mysql/partners/offers/landing_screen_teaser_status.sql");
        pushDumpToTable("mysql/partners/offers/landing_screen_hash_comments.sql");
        pushDumpToTable("mysql/partners/offers/landing_screen_hash_cloaking.sql");

        //widgets
        pushDumpToTable("mysql/partners/widgets/tickers_composite/variables/template.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite/variables/styles.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite/variables/constructor_template.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite/variables/adblock_styles.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite/variables/adblock_template.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite/tickers_composite.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_publisher_source_filters.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_ab_tests.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_relations.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks/variables/styles.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks/variables/template.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks/variables/constructor_template.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks/g_blocks.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks/g_blocks_categories.sql");
        pushDumpToTable("mysql/partners/widgets/tickers/variables/styles.sql");
        pushDumpToTable("mysql/partners/widgets/tickers/variables/template.sql");
        pushDumpToTable("mysql/partners/widgets/tickers/variables/constructor_template.sql");
        pushDumpToTable("mysql/partners/widgets/tickers/tickers.sql");
        pushDumpToTable("mysql/partners/widgets/tickers/tickers_content_groups.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_push_rules.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_placement_configuration/variables/custom_code.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_placement_configuration/tickers_composite_placement_configuration.sql");
        pushDumpToTable("mysql/partners/widgets/v_blocks/variables/v_blocks_variables.sql");
        pushDumpToTable("mysql/partners/widgets/v_blocks/v_blocks.sql");
        pushDumpToTable("mysql/partners/widgets/v_blocks/v_blocks_g_category_filters.sql");
        pushDumpToTable("mysql/partners/widgets/internal_exchange.sql");
        pushDumpToTable("mysql/partners/widgets/internal_exchange_sites.sql");
        pushDumpToTable("mysql/partners/widgets/publishers_ads_moderation.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_mirror_landing_types.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_additional_widgets.sql");
        pushDumpToTable("mysql/partners/widgets/recompile_tasks.sql");
        pushDumpToTable("mysql/partners/widgets/widgets_slack_notification_settings.sql");
        pushDumpToTable("mysql/partners/widgets/widget_formats.sql");
        pushDumpToTable("mysql/partners/sites/publishers_ads_moderation_site_settings.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_attributes.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_doubleclicks.sql");
        pushDumpToTable("mysql/partners/widgets/g_blocks_hard_limit_categories.sql");
        pushDumpToTable("mysql/partners/widgets/tickers_composite_transit_recaptcha.sql");

        //direct demand
        pushDumpToTable("mysql/partners/directDemand/direct_publisher_demand_ads.sql");
        pushDumpToTable("mysql/partners/directDemand/direct_publisher_demand_ads_clients_sites.sql");

        //campaign filters
        pushDumpToTable("mysql/partners/campaigns/campaigns_filters.sql");
        pushDumpToTable("mysql/partners/campaigns/campaigns_sources_quality.sql");

        //clients purses and payouts
        pushDumpToTable("mysql/partners/payments/clients_purses.sql");
        pushDumpToTable("mysql/partners/payments/payments.sql");
        pushDumpToTable("mysql/partners/payments/payments_for_payment_packs.sql");

        //deposits log
        pushDumpToTable("mysql/partners_changes/clients_deposit_log.sql");

        //statistics section
        pushDumpToTable("mysql/statistics/g_partners_history_1.sql");
        pushDumpToTable("mysql/statistics/g_hits_1_moderation_stat.sql");
        pushDumpToTable("mysql/statistics/clients_history.sql");

        //idenfy
        pushDumpToTable("mysql/partners/idenfy/kyc_idenfy.sql");

        //traffic insights
        pushDumpToTable("mysql/partners/traffic_insights/traffic_insights.sql");

        //rules base optimization
        pushDumpToTable("mysql/partners/optimization_rules/optimization_rules.sql");
        pushDumpToTable("mysql/partners/optimization_rules/optimization_rules_log.sql");

        //rotate in subnet
        pushDumpToTable("mysql/partners/campaigns/g_partners_1_subnets.sql");

        //managers_accounts
        pushDumpToTable("mysql/partners/managers_accounts/managers_accounts.sql");
        pushDumpToTable("mysql/partners/managers_accounts/managers_accounts_flow.sql");

        //bonuses
        pushDumpToTable("mysql/partners/bonuses/individual_bonuses_conditions_aa.sql");
    }

    private void getVideoLibVersionFromDb(){
        videoLibVersion += globalSettings.getLibVersion();
    }

    public OperationDataMySql initDbS(){
        mailPull = new MailPull();
        gPartners1 = new GPartners1();
        gHits1 = new GHits1();
        gHitsRejections = new GHitsRejections();
        users = new Users();
        offerCertificates = new OfferCertificates();
        categoryPlatform = new CategoryPlatform();
        teasersOffers = new TeasersOffers();
        teasersModerationScore = new TeasersModerationScore();
        insertGGeoTargeting = new InsertGGeoTargeting();
        copyRequestTeasers = new CopyRequestTeasers();
        creativeOrders = new CreativeOrders();
        clientsSites = new ClientsSites();
        clientsSitesDefaultWidgetCategories = new ClientsSitesDefaultWidgetCategories();
        clientsSitesStatuses = new ClientsSitesStatuses();
        partners1 = new Partners1();
        clientsSitesPostModeration = new ClientsSitesPostModeration();
        news1 = new News1();
        gblocks = new GBlocks();
        gBlocksCampaignTier = new GBlocksCampaignTier();
        tickersComposite = new TickersComposite();
        gBlocksGeoZonesPricesLimits = new GBlocksGeoZonesPricesLimits();
        gBlocksCountryProperties = new GBlocksCountryProperties();
        gBlocksSubnets = new GBlocksSubnets();
        gBlocksCategories = new GBlocksCategories();
        gCategory = new GCategory();
        gBlocksTeasersFilter = new GBlocksTeasersFilter();
        clients = new Clients();
        bundlesGBlocks = new BundlesGBlocks();
        tickersCompositePushRules = new TickersCompositePushRules();
        clientsPurses = new ClientsPurses();
        sensorsTargets = new SensorsTargets();
        bundles = new Bundles();
        clientsSitesBundles = new ClientsSitesBundles();
        gHitsTags = new GHitsTags();
        moderatorsHints = new ModeratorsHints();
        clientsSitesTier = new ClientsSitesTier();
        clientsSitesExcludedTags = new ClientsSitesExcludedTags();
        tickersCompositePlacementConfiguration = new TickersCompositePlacementConfiguration();
        sources = new Sources();
        currentStatus = new CurrentStatus();
        payments = new Payments();
        clientsAutoBilling = new ClientsAutoBilling();
        recompileTasks = new RecompileTasks();
        globalSettings = new GlobalSettings();
        widgetsSlackNotificationSettings = new WidgetsSlackNotificationSettings();
        subnetMirrors = new SubnetMirrors();
        tickersCategories = new TickersCategories();
        tickersFilters = new TickersFilters();
        tickers = new Tickers();
        maxmindGeoipCountries = new MaxmindGeoipCountries();
        teasersLandingsOffers = new TeasersLandingsOffers();
        directPublisherDemandAds = new DirectPublisherDemandAds();
        publishersAdsModeration = new PublishersAdsModeration();
        tickersCompositeRelations = new TickersCompositeRelations();
        tickersCompositeBackfill = new TickersCompositeBackfill();
        tickersCompositeDoubleclick= new TickersCompositeDoubleclick();
        tickersCompositeAttributes= new TickersCompositeAttributes();
        tickersCompositeTransitRecaptcha = new TickersCompositeTransitRecaptcha();
        paymentsPack = new PaymentsPack();
        kycIdenfy = new KycIdenfy();
        individualBonusesConditionsAA = new IndividualBonusesConditionsAA();
        return this;
    }

    public ClientsSitesExcludedTags getClientsSitesExcludedTags() { return clientsSitesExcludedTags; }
    public ClientsSitesTier getClientsSitesTier() { return clientsSitesTier; }
    public TickersCompositePlacementConfiguration getTickersCompositePlacementConfiguration() { return tickersCompositePlacementConfiguration; }
    public GHitsTags getgHitsTags() { return gHitsTags; }
    public ModeratorsHints getModeratorsHints() { return moderatorsHints; }
    public ClientsSitesBundles getClientsSitesBundles() { return clientsSitesBundles; }
    public Bundles getBundles() { return bundles; }
    public SensorsTargets getSensorsTargets() { return sensorsTargets; }
    public ClientsPurses getClientsPurses() { return clientsPurses; }
    public TickersCompositePushRules getTickersCompositePushRules() { return tickersCompositePushRules; }
    public BundlesGBlocks getBundlesGBlocks() { return bundlesGBlocks; }
    public Clients getClients() { return clients; }
    public GBlocksTeasersFilter getgBlocksTeasersFilter() { return gBlocksTeasersFilter; }
    public GCategory getgCategory() { return gCategory; }
    public GBlocksCategories getgBlocksCategories() { return gBlocksCategories; }
    public GBlocksSubnets getgBlocksSubnets() { return gBlocksSubnets; }
    public GBlocksCountryProperties getgBlocksCountryProperties() { return gBlocksCountryProperties; }
    public GBlocksGeoZonesPricesLimits getgBlocksGeoZonesPricesLimits() { return gBlocksGeoZonesPricesLimits; }
    public TickersComposite getTickersComposite() { return tickersComposite; }
    public GBlocksCampaignTier getgBlocksCampaignTier() { return gBlocksCampaignTier; }
    public GBlocks getGblocks() { return gblocks; }
    public News1 getNews1() { return news1; }
    public ClientsSitesPostModeration getClientsSitesPostModeration() { return clientsSitesPostModeration; }
    public Partners1 getPartners1() { return partners1; }
    public ClientsSites getClientsSites() { return clientsSites; }
    public CreativeOrders getCreativeOrders() { return creativeOrders; }
    public CopyRequestTeasers getCopyRequestTeasers() { return copyRequestTeasers; }
    public MailPull getMailPull() { return mailPull; }
    public GPartners1 getgPartners1() { return gPartners1; }
    public GHits1 getgHits1() { return gHits1; }
    public GHitsRejections getGHitsRejections() { return gHitsRejections; }
    public Users getUsers() { return users; }
    public OfferCertificates getOfferCertificates() { return offerCertificates; }
    public TeasersOffers getTeasersOffers() { return teasersOffers; }
    public TeasersModerationScore getTeasersModerationScore() { return teasersModerationScore; }
    public InsertGGeoTargeting getInsertGGeoTargeting() {
        return insertGGeoTargeting;
    }
    public ClientsSitesStatuses getClientsSitesStatuses() { return clientsSitesStatuses; }
    public CategoryPlatform getCategoryPlatform() { return categoryPlatform; }
    public Sources getSources() { return sources; }
    public ClientsAutoBilling getClientsAutoBilling() { return clientsAutoBilling; }
    public CurrentStatus getCurrentStatus() {
        return currentStatus;
    }
    public Payments getPayments() {
        return payments;
    }
    public RecompileTasks getRecompileTasks() { return recompileTasks; }
    public GlobalSettings getGlobalSettings() {
        return globalSettings;
    }
    public WidgetsSlackNotificationSettings getWidgetsSlackNotificationSettings() {return widgetsSlackNotificationSettings;}
    public ClientsSitesDefaultWidgetCategories getClientsSitesDefaultWidgetCategories() {return clientsSitesDefaultWidgetCategories;}
    public SubnetMirrors getSubnetMirrors() {return subnetMirrors;}
    public TickersCategories getTickersCategories() {return tickersCategories;}
    public TickersFilters getTickersFilters() {return tickersFilters;}
    public Tickers getTickers() {return tickers;}
    public MaxmindGeoipCountries getMaxmindGeoipCountries() {return maxmindGeoipCountries;}
    public TeasersLandingsOffers getTeasersLandingsOffers() {return teasersLandingsOffers;}
    public DirectPublisherDemandAds getDirectPublisherDemandAds() {return directPublisherDemandAds;}
    public PublishersAdsModeration getPublishersAdsModeration() {return publishersAdsModeration;}
    public TickersCompositeRelations getTickersCompositeRelations() {return tickersCompositeRelations;}
    public TickersCompositeDoubleclick getTickersCompositeDoubleclick() {return tickersCompositeDoubleclick;}
    public TickersCompositeAttributes getTickersCompositeAttributes() {return tickersCompositeAttributes;}
    public TickersCompositeTransitRecaptcha getTickersCompositeTransitRecaptcha() {return tickersCompositeTransitRecaptcha;}
    public TickersCompositeBackfill getTickersCompositeBackfill() {return tickersCompositeBackfill;}
    public PaymentsPack getPaymentsPack() {return paymentsPack;}
    public KycIdenfy getKycIdenfy() {return kycIdenfy;}
    public IndividualBonusesConditionsAA getIndividualBonusesConditionsAA() {return  individualBonusesConditionsAA;}
}
