-- users
INSERT INTO admin.users (login, account_id, password_hash, firstname, lastname, fullname, email, locale, communications) VALUES
('user_auto', 1, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'user_auto', 'user_auto@ex.ua', 'en','{}'),
('sokwages', 2, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'sokwages', 'sokwages@ex.ua', 'en','{}'),
('sokgoodhits', 3, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'sokgoodhits', 'sokgoodhits@ex.ua', 'en','{}'),
('prikhodko.alexander', 4, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'prikhodko.alexander', 'prikhodko.alexander@ex.ua', 'en','{}'),
('djo.razer', 5, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'djo.razer', 'djo.razer@ex.ua', 'en','{}'),
('djim.beam', 6, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'djim.beam', 'djim.beam@ex.ua', 'en','{}'),
('kait.leo', 15, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'kait.leo', 'kait.leo@ex.ua', 'en','{}'),
('zlata.ognevich', 16, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'zlata.ognevich', 'zlata.ognevich@ex.ua', 'en','{}'),
('horse_in_coat', 19, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Duncan', 'Cage', 'horse_in_coat', 'horse_in_coat@ex.ua', 'en','{}'),
('user_auto_privileges', 17, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Valera', 'Buhbuh', 'user_auto_privileges', 'user_auto_privileges@ex.ua', 'en','{}'),
('mila.jovovich', 18, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Milica', 'Jovovich', 'mila.jovovich', 'mila.jovovich@ex.ua', 'en','{}'),
('user_admin', 20, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'user_admin', 'user_admin@ex.ua', 'en','{}'),
('herring_in_fur_coat', 21, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Duncan', 'MacLeod', 'herring_in_fur_coat', 'herring_in_fur_coat@ex.ua', 'en','{}'),
('beaver_with_beer', 22, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Scottie', 'Pippen', 'beaver_with_beer', 'beaver_with_beer@ex.ua', 'en','{}'),
('recovery_pass', 30, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Roma', 'Maliuk', 'recovery_pass', 'user_for_recovery_pass@ukr.net', 'en','{}');
-- ra
INSERT INTO admin.users (login, account_id, password_hash, firstname, lastname, fullname, email, locale, agreement_page, accompanying_manager, ra_type, ra_currency, advertising_agency_subnet, status) VALUES
('AA_agency', 7, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'AA_agency', 'AA_agency@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'arbitration', 'usd', 0, 1),
('weird.ant', 29, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'weird', 'ant', 'weird.ant', 'weird.ant@ex.ua', 'en', 'ra', 'mariush.werpakovick', 'arbitration', 'usd', 0, 1),
('SOK_RA', 31, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RA', 'SOK_RA', 'sok_ra@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RB', 32, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RB', 'SOK_RB', 'sok_rb@ex.ua', 'en', 'ra', 'user_auto_privileges', 'classic', 'eur', 0, 1),
('SOK_RC', 33, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RC', 'SOK_RC', 'sok_rc@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RD', 34, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RD', 'SOK_RD', 'sok_rd@ex.ua', 'en', 'ra', 'user_auto_privileges', 'classic', 'eur', 0, 1),
('SOK_RE', 35, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RE', 'SOK_RE', 'sok_re@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RF', 36, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RF', 'SOK_RF', 'sok_rf@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RG', 37, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RG', 'SOK_RG', 'sok_rg@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RH', 38, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RH', 'SOK_RH', 'sok_rh@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1),
('SOK_RI', 39, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'SOK', 'RI', 'SOK_RI', 'sok_ri@ex.ua', 'en', 'ra', 'prikhodko.alexander', 'classic', 'eur', 0, 1);
-- users with geoTeam
INSERT INTO admin.users (login, account_id, password_hash, firstname, lastname, fullname, email, locale, geo_team, is_blocked) VALUES
('serg.Kovac', 8, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'serg.Kovac', 'serg.Kovac@ex.ua', 'en', '', 0),
('radjiev.arambytan', 9, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'radjiev.arambytan', 'radjiev.arambytan@ex.ua', 'en', 'sales_base', 0),
('quatar.Help', 10, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'quatar.Help', 'quatar.Help@ex.ua', 'en', 'sales_la', 0),
('mariush.werpakovick', 11, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'mariush.werpakovick', 'mariush.werpakovick@ex.ua', 'en', 'sales_us', 0),
('aby.babyn', 12, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'aby.babyn', 'aby.babyn@ex.ua', 'en', 'india', 0),
('djordj.maikl', 13, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'djordj.maikl', 'djordj.maikl@ex.ua', 'en', 'cis', 0),
('yoka.ono', 14, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Ivan', 'Izya', 'yoka.ono', 'yoka.ono@ex.ua', 'en', 'europe', 0),
('AA_MVV_test', 23, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Nikola', 'Tesla', 'NTesla', 'AA_MVV_test@ex.ua', 'en', 'europe', 0),
('wild.elephant', 24, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Wild', 'Elephant', 'wild.elephant', 'wild.elephant@ex.ua', 'en', 'sales_geo', 0),
('tiny.hippo', 25, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Tiny', 'Hippo', 'tiny.hippo', 'tiny.hippo@ex.ua', 'en', 'sales_geo', 0),
('fat.cat', 26, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'Fat', 'Cat', 'fat.cat', 'fat.cat@ex.ua', 'en', 'cis', 0),
('happy.avocado', 27, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'happy', 'avocado', 'happy.avocado', 'happy.avocado@ex.ua', 'en', 'cis', 1),
('brave.racoon', 28, '$argon2id$v=19$m=65536,t=4,p=1$UGVRQURGQ2JhL05JOHFUaA$JOjvVQO/I5IKtpS2U2MPYIl8dV+HHM2FozXpB7UDnfc', 'brave', 'racoon', 'brave.racoon', 'brave.racoon@ex.ua', 'en', 'cis', 1);