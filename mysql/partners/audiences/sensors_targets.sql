INSERT INTO partners.sensors_targets (id, client_id, name, ident, union_type, sensors_targets_conversion_categories_id) VALUES
(   1,    1, 'Hibernate Target name qwe',                                 '', 'AND', 1),
(   2,    1, 'Hibernate Target name asd',                                 '', 'AND', 1),
(   3,    1, 'Hibernate Target name zxc',                                 '', 'AND', 1),
(   4,    1, 'Hibernate Target name cxz',                                 '', 'AND', 1),
(   5,    1, 'Hibernate Target name cxz',                          'clickdb', 'AND', 1),
(1000, 1000,      'Hibernate Target gfd',                                 '', 'AND', 1),
(1001, 1000,      'Hibernate Target rfv',                                 '', 'AND', 1),
(1002, 1000,      'Hibernate Target tgb',                                 '', 'AND', 1),
(1003, 1000,      'Hibernate Target yhm',                                 '', 'AND', 1),
(1004, 1000,      'Hibernate Target cxz',                          'clickdb', 'AND', 1),
(1005, 1000, 'Hibernate Target name lkj',                          'clickdb', 'AND', 1),
(1006, 4,               'Lenta target 1',                                 '', 'AND', 1),
(1007, 4,               'Lenta target 2',                                 '', 'AND', 1),
(1008, 4,               'Lenta target 3',                                 '', 'AND', 1),
(1009, 4,               'Lenta target 4',                                 '', 'AND', 1),
(1010, 4,               'Lenta target 5',                                 '', 'AND', 1),
(1011, 26,             'Adgage target 1',                                 '', 'AND', 1),
(1012, 26,             'Adgage target 2',                                 '', 'AND', 1),
(1013, 26,             'Adgage target 3',                                 '', 'AND', 1),
(1014, 26,             'Adgage target 4',                                 '', 'AND', 1),
(1100, 1,              'DumpedAudience1', 'aab7eeed68c7d3c2a567790293465f79',  'OR', 1),
(1105, 5,                 'Add target 1',                                 '', 'AND', 1),
(1106, 5,                 'Add target 2',                                 '', 'AND', 1),
(1107, 5,                 'Add target 3',                                 '', 'AND', 1),
(1108, 1,                 'Add target 4',                                 '', 'AND', 1),
(1109, 1132,              'Add target 1',                                 '', 'AND', 1),
(1110, 1132,              'Add target 2',                                 '', 'AND', 1),
(1111, 1132,              'Add target 3',                                 '', 'AND', 1);
