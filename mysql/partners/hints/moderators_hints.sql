INSERT INTO moderators_hints (id, teaser_id, value, action, teaser_ids, hint_status, who_add, created_at, updated_at) VALUES
(1000,1000, '{"new": "pg", "old": "r"}',                'adType',          '{\"1010\": [\"1010\"]}', 2, 'user_auto', '2022-09-27 20:30:20', '2022-10-03 08:26:16'),
(1001,1001, '{"new": "pg", "old": "r"}',                'lpType',          '{\"3\": [\"3\"]}',       2, 'user_auto', '2022-09-27 20:30:20', '2022-10-03 08:26:16'),
(1003,1003, '{"new": "226", "old": "241"}',             'category',        '{\"3\": [\"3\"]}',       2, 'user_auto', '2022-09-27 20:30:20', '2022-10-03 08:26:16'),
(1004,1004, '{"new": "1", "old": "0"}',                 'blockByCloaking', '{\"3\": [\"3\"]}',       2, 'user_auto', '2022-09-27 20:30:20', '2022-10-03 08:26:16'),
(1005,1005, '{"new": "landing_page_issue", "old": ""}', 'reject',          '{\"3\": [\"3\"]}',       2, 'user_auto', '2022-09-27 20:30:20', '2022-10-03 08:26:16'),
(1006,1012, '{"new": "pg", "old": "r"}',                'lpType',          '{\"3\": [\"3\"]}',       4, 'user_auto', '2022-09-27 20:30:20', DATE_SUB(CURRENT_DATE(), INTERVAL 2 DAY)),
(1007,1013, '{"new": "pg", "old": "r"}',                'lpType',          '{\"3\": [\"3\"]}',       4, 'user_auto', '2022-09-27 20:30:20', CURRENT_DATE() );