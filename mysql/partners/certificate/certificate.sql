-- g_partners_1
INSERT INTO partners.certificate (id, deleted, who_add, when_add, when_change, certificate_verified) VALUES
( 1, 0, 'user_auto' , current_time, current_time, 0),
( 2, 0, 'user_auto' , current_time, current_time, 0),
( 3, 0, 'user_auto' , current_time, current_time, 0),
( 4, 0, 'user_auto' , current_time, current_time, 0),
( 5, 0, 'user_auto' , current_time, current_time, 0),
( 6, 0, 'user_auto' , current_time, current_time, 0),
( 7, 0, 'user_auto' , current_time, current_time, 0),
( 8, 0, 'user_auto' , current_time, current_time, 0),
( 9, 0, 'user_auto' , current_time, current_time, 1),
( 10, 0, 'user_auto' , current_time, current_time, 1),
( 11, 0, 'user_auto' , current_time, current_time, 0),
( 12, 0, 'user_auto' , current_time, current_time, 1);
