INSERT INTO optimization_rules(optimization_rules_id, rule_type, g_partners_1_id, level, action, required_stat, required_stat_amount,
                               analyze_indicator, analyze_operator, analyze_indicator_value, analyze_stage, period, bid_coefficient) VALUES
(1, 'dynamic_bid_rule', 2040, 'widget', 'set_min',       'spent', 1.000000, 'conversion_cost', null, 0.000000, 'buy', 168, null),
(2, 'fixed_bid_rule',   2041, 'widget',  null,           'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168, 1.000000),
(3, 'block_rule',       2042, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168, null);
INSERT INTO partners.optimization_rules (optimization_rules_id, rule_type, g_partners_1_id, level, action, required_stat, required_stat_amount, analyze_indicator, analyze_operator, analyze_indicator_value, analyze_stage, period) VALUES
(4, 'block_rule', 2044, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168),
(5, 'block_rule', 2046, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168);
INSERT INTO optimization_rules(optimization_rules_id, rule_type, g_partners_1_id, level, action, required_stat, required_stat_amount,
                               analyze_indicator, analyze_operator, analyze_indicator_value, analyze_stage, period, bid_coefficient) VALUES
(6, 'dynamic_bid_rule', 2048, 'widget', 'set_min',       'spent', 1.000000, 'conversion_cost', null, 0.000000, 'buy', 168, null),
(7, 'fixed_bid_rule',   2048, 'widget',  null,           'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168, 1.000000),
(8, 'block_rule',       2048, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168, null);
INSERT INTO partners.optimization_rules (optimization_rules_id, rule_type, g_partners_1_id, level, action, required_stat, required_stat_amount, analyze_indicator, analyze_operator, analyze_indicator_value, analyze_stage, period) VALUES
(9, 'block_rule', 2048, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168),
(10, 'block_rule', 2048, 'widget', 'block-unblock', 'spent', 1.000000, 'conversion_cost', 'gt', 1.000000, 'buy', 168);