-- clients_sites_ads_txt
INSERT INTO `clients_sites_ads_txt` (`client_site_id`, `active`, `updated_time`, `content`, `dash_sent`, `domain`, `block_auto_notifications`, `missing_lines`, `to_notify_by_mail`, `to_notify_in_dash`, `add_video_lines`) VALUES
(3, 1, '2020-01-01 00:00:00', 'mgid.com, 4, DIRECT, d4c29acad76ce94f\n', '2020-01-01 00:00:00', 'lentainform.com', 1, NULL, 0, 0, 1),
(4, 1, '2020-01-01 00:00:00', 'mgid.com, 4, DIRECT, d4c29acad76ce94f\n', '2020-01-01 00:00:00', 'lentainform.com', 1, NULL, 0, 0, 1),
(1, 1, '2020-01-01 00:00:00', 'mgid.com, 1, DIRECT, d4c29acad76ce94f\n', '2020-01-01 00:00:00', '', 1, NULL, 0, 0, 0),
(5, 1, '2020-01-01 00:00:00', 'mgid.com, 1, DIRECT, d4c29acad76ce94f\n', '2020-01-01 00:00:00', 'testsitemgtre.com', 1, NULL, 1, 1, 1),
(2007, 1, '2021-01-19 00:00:00', 'mgid.com, 2021, DIRECT, d4c29acad76ce94f\n', '2021-01-19 00:00:00', '', 1, NULL, 0, 0, 0),
(2008, 1, '2021-02-23 00:00:00', 'mgid.com, 2022, DIRECT, d4c29acad76ce94f\n', '2021-02-23 00:00:00', 'test-site-with-video.com', 1, NULL, 0, 0, 1),
(2009, 1, '2020-01-01 00:00:00', 'mgid.com, 2023, DIRECT, d4c29acad76ce94f\n', '2021-02-24 00:00:00', 'lentainform.com', 1, NULL, 0, 0, 1),
(2011, 1, '2021-05-11 00:00:00', 'mgid.com, 2021, DIRECT, d4c29acad76ce94f\n', '2021-05-11 00:00:00', 'test-site-with-ads-txt-combined.com', 1, 'google.com, pub-2603664881560000, DIRECT, f08c47fec0942fa0', 0, 0, 0),
(2012, 1, '2021-05-11 00:00:00', 'mgid.com, 2021, DIRECT, d4c29acad76ce94f\n', '2021-05-11 00:00:00', 'test-site-with-ads-txt.com', 1, 'google.com, pub-2603664881560000, DIRECT, f08c47fec0942fa0', 0, 0, 0);