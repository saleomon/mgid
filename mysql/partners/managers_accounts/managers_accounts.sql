INSERT INTO partners.managers_accounts (id, incoming, personal, blocked, penalty, bonus, manager, discount_old, discount_new, discount_next_recalculation_date, notification_low_balance_limit_mgc, notification_low_balance_sent, mcm_aa, mcm_rebate_percent, mcm_advertiser) VALUES
(1, 0, 0, 0, 0, 0, 'AA_agency', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(2, 0, 0, 0, 0, 0, 'weird.ant', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(3, 0, 0, 0, 0, 0, 'SOK_RA', NULL, NULL, NULL, '0.000000', 0, 1, 5, 'Strong AA'),
(4, 0, 0, 0, 0, 0, 'SOK_RB', NULL, NULL, NULL, '0.000000', 0, 1, 5, 'Strong AA 2'),
(5, 0, 0, 0, 0, 0, 'SOK_RC', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(6, 0, 0, 0, 0, 0, 'SOK_RD', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(7, 0, 0, 0, 0, 0, 'SOK_RE', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(8, 0, 0, 0, 0, 0, 'SOK_RF', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(9, 0, 0, 0, 0, 0, 'SOK_RG', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(10, 0, 0, 0, 0, 0, 'SOK_RH', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL),
(11, 0, 0, 0, 0, 0, 'SOK_RI', NULL, NULL, NULL, '0.000000', 0, 0, NULL, NULL);