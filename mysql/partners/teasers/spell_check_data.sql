INSERT INTO `spell_check_data` (`id`, `content`, `lang`, `type`, `result`, `errors`, `when_add`) VALUES
(2, 'Teaser Similor title',       'en-US', 'teaser_title',       'error', '[{\"rule\": \"MORFOLOGIK_RULE_EN_US\", \"length\": 5, \"offset\": 0, \"category\": \"spelling\", \"categoryId\": \"TYPOS\", \"replacements\": [\"Similar\", \"Simpler\"]}]', '2020-11-10 16:20:14'),
(3, 'Teaser amazing title',       'en-US', 'teaser_title',          'ok', NULL, '2020-11-10 16:20:14'),
(4, 'Somec text present',                 'en-US', 'teaser_description', 'error', '[{\"rule\": \"MORFOLOGIK_RULE_EN_US\", \"length\": 5, \"offset\": 0, \"category\": \"spelling\", \"categoryId\": \"TYPOS\", \"replacements\": [\"Some\", \"Sem\"]}]', '2020-11-10 16:20:14'),
(5, 'Some intelligent text',      'en-US', 'teaser_description',    'ok', NULL, '2020-11-10 16:20:14');
