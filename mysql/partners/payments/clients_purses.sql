INSERT INTO clients_purses (id, client_id, currency, purse, favorite) VALUES
(2001, 2001, 2,                  '655765788797978', 1),
(2002, 2001, 123,                '655765788797978', 0),
(2003, 2004, 2,                  '655765788797978', 1),
(2004, 2004, 123,                '655765788797978', 0),
(2005, 2006, 123,                '655765788797978', 1),
(2006, 2006, 2,                  '655765788797978', 0),
(2060, 2040, 123,                '655765788797978', 1),
(2061, 2040, 2,                  '655765788797978', 0),
(2053, 2028, 2,                  '655765788797978', 1),
(2058, 2034, 195, 'sok_autotest_payoneer@test.com', 1),
(2059, 2034, 201,                      'U12345678', 1),
(2062, 2074, 2,                  'purse1@test.com', 0),
(2063, 2074, 123,                      'U12345679', 1),
(2064, 2074, 89,         'test_kyc_ind@paypal.com', 0),
(2065, 2075, 2,                'purse2@paypal.com', 1),
(2066, 2075, 123,                      'U12345680', 0),
(2067, 2075, 89,       'test_kyc_legal@paypal.com', 0);
-- for cab.publishers.finances.CabPublishersFinancesClientsPayoutsTests.checkPayoutsColor
INSERT INTO clients_purses (id, client_id, currency, purse, request_date) VALUES
(2000, 2005, 89, 'SOK_AUTOTEST_123@TEST.COM', '2020-09-21 07:20:29');
-- for /wages/clients-payments/ (payments_for_payment_packs.sql)
INSERT INTO clients_purses (id, client_id, currency, purse, favorite, wm_id, purse_details) VALUES
(2007, 2008, 83,  'Z394313302416', 1, 289079653323, ''),
(2008, 2008, 160, 'Bank name<br>Bank address<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 0, null, '{"bank":"Bank name","bank-address":"Bank address","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2009, 2008, 214, '4276380079956881', 0, null, '{"card":"4276380079956881","validity_month":"01","validity_year":"2040","cardholder_name":"Auto","cardholder_second_name":"Test","cardholder_DOB":"2001-10-01","cardholder_address":"O. Dovzhenko Street, 3","cardholder_city":"Kyiv","cardholder_country":"UA"}'),
(2010, 2008, 198, '4276380079956881', 0, null, '{"card":"4276380079956881","validity_month":"01","validity_year":"2040","cardholder_name":"Auto","cardholder_second_name":"Test"}'),
(2011, 2008, 150, 'Tipalti bank transfer', 0, null, ''),
(2012, 2009, 83,  'Z394313302416', 1, 289079653323, ''),
(2013, 2009, 160, 'Bank name<br>Bank address<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 0, null, '{"bank":"Bank name","bank-address":"Bank address","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2014, 2009, 214, '4276380079956881', 0, null, '{"card":"4276380079956881","validity_month":"01","validity_year":"2040","cardholder_name":"Auto","cardholder_second_name":"Test","cardholder_DOB":"2001-10-01","cardholder_address":"O. Dovzhenko Street, 3","cardholder_city":"Kyiv","cardholder_country":"UA"}'),
(2015, 2009, 198, '4276380079956881', 0, null, '{"card":"4276380079956881","validity_month":"01","validity_year":"2040","cardholder_name":"Auto","cardholder_second_name":"Test"}'),
(2016, 2009, 150, 'Tipalti bank transfer', 0, null, ''),
(2029, 2008, 89,  'sok_autotest_2008@paypal.com', 0, null, ''),
(2030, 2008, 169, 'sok_autotest_2008@paypal.com', 0, null, ''),
(2031, 2009, 89,  'sok_autotest_2009@paypal.com', 0, null, ''),
(2032, 2009, 169, 'sok_autotest_2009@paypal.com', 0, null, ''),
(2033, 2024, 223, 'sok_autotest_2024@paypal.com', 0, null, ''),
(2034, 2024, 221,   'Tipalti bank transfer Asia', 0, null, ''),
(2035, 2024, 222, 'Bank name Asia<br>Bank address Asia<br>PBANUA2X<br>Privatbank Asia<br>Hung Bay av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 1, null, '{"bank":"Bank name Asia","bank-address":"Bank address Asia","swift-code":"PBANUA2X","account-name":"Privatbank Asia","address":"Hung Bay av.","account-number":"TA173000010000032003102901026","country":"54","currencyUsd":"usd","currencyLocal":"false"}'),
(2054, 2024, 249, 'Z139359920950', 0, 548891670493, '{"purse":"Z139359920950","name":"Carroll","surname":"Donnelly","birthdate":"1978-03-20","country":"PE"}'),
(2055, 2007, 248, 'Z139359920950', 0, 548891670493, '{"purse":"Z139359920950","name":"Carroll","surname":"Donnelly","birthdate":"1978-03-20","country":"PE"}');
-- for tests about Client's payout region
INSERT INTO clients_purses (id, client_id, currency, purse, favorite, purse_details) VALUES
(2017, 2010, 160, 'Bank name<br>Bank address<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 1, '{"bank":"Bank name","bank-address":"Bank address","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2018, 2011, 222, 'Bank name Asia<br>Bank address Asia<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 1, '{"bank":"Bank name Asia","bank-address":"Bank address Asia","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2019, 2012, 160, 'Bank name<br>Bank address<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 1, '{"bank":"Bank name","bank-address":"Bank address","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2020, 2013, 222, 'Bank name Asia<br>Bank address Asia<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 1, '{"bank":"Bank name Asia","bank-address":"Bank address Asia","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2021, 2014, 222, 'Bank name Asia<br>Bank address Asia<br>PBANUA2X<br>Privatbank<br>Peremogy av.<br>UA173000010000032003102901026<br>1<br>2000-01-01<br>1234567890<br>usd', 0, '{"bank":"Bank name Asia","bank-address":"Bank address Asia","swift-code":"PBANUA2X","account-name":"Privatbank","address":"Peremogy av.","account-number":"UA173000010000032003102901026","country":"1","date_of_birth":"2000-01-01","national_identity_number":"1234567890","currencyUsd":"usd","currencyLocal":"false"}'),
(2022, 2014,  89, 'SOK_AUTOTEST_2022@TEST.COM', 1, ''),
(2023, 2016,  89, 'SOK_AUTOTEST_2023@TEST.COM', 1, ''),
(2024, 2017, 223, 'SOK_AUTOTEST_2024@TEST.COM', 1, ''),
(2025, 2018,  89, 'SOK_AUTOTEST_2025@TEST.COM', 1, ''),
(2026, 2019, 223, 'SOK_AUTOTEST_2026@TEST.COM', 1, ''),
(2027, 2020, 223, 'SOK_AUTOTEST_2026@TEST.COM', 1, ''),
(2028, 2020,   2,            '655765788797978', 0, '');