insert into socdem_interests (id, name,     parent_id, available) values
    (12001, 'Mazda 3', 341, 1),
    (12002, 'Mazda 6', 341, 1),
    (12003, 'Mazda CX5', 341, 1),
    (12004, 'Mazda CX7', 341, 1),
    (12005, 'Mazda CX9', 341, 1);

insert into socdem_interests (id, name, parent_id, available) values
    (12041, 'Mazda CX7 (2006-2009)', 12004, 1),
    (12042, 'Mazda CX7 (2010-2012)', 12004, 1);

insert into socdem_interests (id, name, parent_id, available) values
    (12051, 'White', 12042, 1),
    (12052, 'Red', 12042, 1);
