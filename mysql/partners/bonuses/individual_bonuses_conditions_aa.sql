INSERT INTO partners.individual_bonuses_conditions_aa (id, login, grades, payment_method, comment, status, dangerous_conditions, when_add, when_change) VALUES
(1, 'AA_agency', '{"1": {"rank": 1000, "rate": 10}}', 'bonus', 'test', 1, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22'),
(2, 'SOK_RA',    '{"1": {"rank": 1000, "rate": 10}}', 'external', 'test', 0, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22'),
(3, 'SOK_RB',    '{"1": {"rank": 1000, "rate": 10}}', 'bonus', 'test', 1, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22'),
(4, 'SOK_RC',    '{"1": {"rank": 1000, "rate": 10}}', 'bonus', 'test', 0, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22'),
(5, 'SOK_RH',    '{"1": {"rank": 3000, "rate": 3}, "2": {"rank": 16000, "rate": 5}, "3": {"rank": 20000, "rate": 7}}', 'external', 'test', 1, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22'),
(6, 'SOK_RI',    '{"1": {"rank": 1000, "rate": 10}}', 'bonus', 'test', 1, 0, '2023-02-22 01:36:22', '2023-02-22 01:36:22');