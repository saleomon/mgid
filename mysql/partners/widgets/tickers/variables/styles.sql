SET @styles_tickers = '.mgresponsive {
  display: inherit;
}
.mgbox {
  padding: 0 !important;
  position: relative !important;
  text-align: center;
  vertical-align: top !important;
  margin: 0 auto;
  border-style: solid;
  border-width: 0px;
  border-color: ;
  background-color: ;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  line-height: 100% !important;
  transition: none !important;
  box-sizing: border-box;
}
.mgline {
  position: relative;
}
.mgline .image-with-text {
  position: relative;
}
.mgbox {
  width: 100%;
  max-width: 100%;
}
div.mcimg {
  padding: 0px;
  text-align: center;
}
img.mcimg {
  border-style: solid;
  border-color: #ffffff;
  border-width: 0px;
  width: 100% !important;
  height: auto !important;
  max-width: 492px;
  max-height: 277px;
  box-sizing: border-box;
  display: block;
}
.mctitle {
  margin-top: 10px;
  text-align: left;
}
.mctitle a {
  font-weight: bold;
  font-size: 18px;
  line-height: unset !important;
  font-style: normal;
  text-decoration: none;
  color: #000000;
  font-family: Arial,Helvetica,sans-serif;
}
.mcdesc {
  display: none;
  text-align: center;
}
.mcdesc a {
  font-weight: normal;
  font-size: 12px;
  line-height: 12px;
  font-style: normal;
  text-decoration: none;
  color: #666666;
  font-family: Arial,Helvetica,sans-serif;
}
.mcdomain {
  display: block;
  text-align: left;
}
.mcdomain a {
  font-weight: normal;
  font-size: 13px;
  line-height: 13px;
  font-style: italic;
  text-decoration: none;
  color: #7f7f7f;
  font-family: Arial,Helvetica,sans-serif;
  padding: 4px;
  display: block;
  overflow: hidden;
  text-transform: capitalize;
}
.mcdomain a img.mcimgsrc {
  vertical-align: bottom;
  margin-bottom: -3px;
  height: 20px;
  width: 20px;
  display: inline-block;
}
.mgline {
  background: none repeat scroll 0 0;
  background-color: ;
  cursor: pointer;
  display: inline-block;
  overflow: hidden;
  zoom: 1;
  display: inline;
  padding: 0 !important;
  border-style: solid;
  border-color: #ffffff;
  border-width: 0px;
  width: 32.33333333%;
  max-width: 32.33333333%;
  box-sizing: border-box;
  margin: 10px 0.5%;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  word-wrap: break-word;
}
.mgline .image-container {
  position: relative;
}
.mgline .image-container .mcimgad {
  position: absolute;
  right: 0;
  bottom: 0;
  width: 20px;
  height: 20px;
}
.mgline {
  vertical-align: top;
}
.mgline,
.mgbox {
  min-width: 90px;
}
.mgline[max-width~="120px"] .mcdesc {
  display: none !important;
}
@supports not (flex-wrap: wrap) {
  .mgbox {
    display: block !important;
  }
  .mgline {
    display: inline-block !important;
  }
}
.text-elements a {
  text-decoration: none;
}
div.mcprice {
  text-align: center;
}
div.mcprice span {
  font-weight: bold;
  font-size: 12px;
  line-height: 12px;
  font-style: normal;
  text-decoration: none;
  color: #ffffff;
  font-family: Verdana,Geneva,sans-serif;
}
div.mgbuybox,
div.mgarrowbox {
  display: false;
}
div.mgbuybox,
div.mgarrowbox,
div.mcprice {
  display: none;
}
span.mcpriceold {
  text-decoration: line-through !important;
}
@media (max-width: 480px) {
  .mgline {
    width: 48% !important;
    margin: 1% !important;
    max-width: 48% !important;
  }
}
@media (max-width: 480px) {
  .mgline {
    width: 98% !important;
    margin: 1% !important;
    max-width: 98% !important;
  }
}
.mgline .image-container {
  position: relative;
}
.mgline .image-container .mcimgad {
  width: 70px;
}
.mctitle a:hover {
  color: #7f7f7f;
}
span.mghead {
  float: left;
  font-weight: bold;
  margin-left: 7px;
  min-height: 15px;
}
.mghead {
  font-family: Arial,Helvetica,sans-serif !important;
  color: #000000 !important;
  font-size: 18px !important;
  text-transform: uppercase !important;
}
.mcdomain a {
  color: #7f7f7f;
}
.mgheader {
  width: 100% !important;
}
.mgheader a {
  top: 25px !important;
}
.widgets_logo {
  position: static !important;
}
.mgbox {
  padding-top: 25px !important;
}
.mgline {
  position: relative;
}
.mgline .fake {
  visibility: hidden;
  position: relative;
  padding-top: 4px;
}
.mgline div.mcprice {
  background-color: #7f7f7f;
  padding: 3px 0;
}
.mgline .mctitle {
  margin-top: 2px;
  line-height: 1 !important;
}
.mgline .mcdesc {
  padding-top: 7px;
}
.mgline .mcdomain {
  padding-top: 3px;
}
.mgline .mcdiscount {
  display: none;
}
.mgline .mcprice .mcpriceold {
  font-size: 10px;
  color: #bbbbbb;
}
.mgline:hover .mctitle a {
  color: #7f7f7f;
}
.mgline .image-with-text,
.mgline .mgtobottom {
  width: 100% !important;
  margin: 0 auto;
}
.mgline .image-with-text,
.mgline .mgtobottom {
  max-width: 492px;
}
.mgline {
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.25);
  -moz-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.25);
  -webkit-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.25);
  -o-box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  overflow: hidden;
  background: #fff;
}
.mgline .image-container {
  display: inline-block !important;
}
.mgline .mgtobottom {
  left: 0;
  right: 0;
}
.mgline .mctitle {
  margin-top: 5px;
  min-height: 35px;
  overflow: hidden;
  padding: 0 7px;
}
.mgline .mcdesc {
  padding: 7px 20px 0;
}
.mgline .mcdomain {
  padding: 3px 5px 0;
}
.mcdomain a {
  line-height: unset !important;
  margin-top: 0;
  margin-bottom: 5px;
}
.mcdomain a img.mcimgsrc {
  margin-right: 5px;
}
.mg-slider-overlay {
  overflow: hidden;
  position: relative;
}
.mg-slider-overlay .mg-slider-wrap {
  min-height: 200px;
  position: relative;
  width: 100%;
  -webkit-transform: translate3D(0, 0, 0);
  -moz-transform: translate3D(0, 0, 0);
  -ms-transform: translate3D(0, 0, 0);
  -o-transform: translate3D(0, 0, 0);
  transform: translate3D(0, 0, 0);
  -webkit-transition-duration: 200ms;
  -moz-transition-duration: 200ms;
  -ms-transition-duration: 200ms;
  -o-transition-duration: 200ms;
  transition-duration: 200ms;
}
.mg-slider-overlay .mg-slider-wrap .mg-slider-box {
  left: 0;
  top: 0;
  position: absolute;
  width: 100%;
}
.mg-pagination {
  text-align: center;
}
.mg-pagination .mg-pagination-list {
  display: inline-block;
}
.mg-pagination .mg-pagination-item {
  cursor: pointer;
  display: inline-block;
  *display: inline;
  *zoom: 1;
  width: 8px;
  height: 8px;
  margin: 0 2px;
  background: #ddd;
  border-radius: 50%;
}
.mg-pagination .mg-pagination-item-prev,
.mg-pagination .mg-pagination-item-next {
  background: #f7f7f7;
  border: 1px solid #cecece;
  border-radius: 2px;
  cursor: pointer;
  display: inline-block;
  height: 11px;
  margin: 0 10px;
  padding: 5px 0;
  text-align: center;
  vertical-align: middle;
  width: 29px;
}
.mg-pagination .mg-pagination-item-prev:after,
.mg-pagination .mg-pagination-item-next:after {
  background: url(https://images.dable.io/static/i/prevnext2.png?3) no-repeat 0 0;
  background-size: 17px 11px;
  content: "";
  display: inline-block;
  height: 13px;
  width: 9px;
}
.mg-pagination .mg-pagination-item-next:after {
  background-position: -10px 0;
  width: 8px;
}
.mg-pagination .mg-pagination-item-current {
  background: #508eef;
}
.mgline .image-container {
  display: block !important;
}
',
@styles_tickers_transit = '';