SET @template_composite = '<div class="mgbox">
<!--advertPrefix-->
    {foreach}
        <div class="mgline">
            {if $banner}{$banner}{/if}
            {if $teaser}
                <div class="image-with-text">
                    <div class="mcimg">
                        <a {$target} {$href} >
                            <div class="image-container">
                                <img class="mcimg" {$src} />
	                                <!--intExchangeWagesImagePlace-->
                            </div>
                        </a>
                    </div>
                    <div class="text-elements">
                        <div class="text_on_hover">
                            <div class="mcdomain-top mcdomain"><a {$target} {$href}>{$source}</a></div>
                            <div class="mctitle"><a {$target} {$href}>{$title}</a></div>
                            <div class="mcdesc"><a {$target} {$href}>{$desc}</a></div>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    {/foreach}
</div>
',
@template_composite_adskeeper = '
<div class="mgbox">
<!--advertPrefix-->
    {foreach}
        <div class="mgline">
            {if $banner}{$banner}{/if}
            {if $teaser}
                                <div class="image-with-text">
                                        <div class="mcimg">
                        <a {$target} {$href} >
                            <div class="image-container">
                                <img class="mcimg" {$src} />
                            </div>
                        </a>
                    </div>
                                                {if $price}
    <a {$target} {$href} class="mcprice-wrap {if $priceold}mcprice-wrap_old{/if}">
        <div class="mgarrowbox">
            {if $priceold}
            <div class="mcprice-cover mcprice-cover_old mcpriceouter">
                <div class="mcprice-inner">
                    <div class="mcdiscount">{$discount}</div>
                </div>
                <div class="mcprice-val">
                    <div class="mcprice">{$price}</div>
                    <div class="mcpriceold"><span>{$priceold}</span></div>
                </div>
            </div>
            {/if}
            <div class="mcprice-cover mcpriceouter">
                <div class="mcprice-inner">
                    <div class="mcprice">{$price}</div>
                </div>
            </div>
        </div>
    </a>
    {/if}
<div class="text-elements">
    <div class="text_on_hover">
        <div class="mctitle"><a {$target} {$href}>{$title}</a></div>
                    </div>
</div>                                    </div>
            {/if}
        </div>
    {/foreach}
</div>
',
@template_composite_lenta = '<div class="mgbox">
<!--advertPrefix-->
    {foreach}
        <div class="mgline">
            {if $banner}{$banner}{/if}
            {if $teaser}
                                <div class="image-with-text">
                                        <div class="mcimg">
                        <a {$target} {$href} >
                            <div class="image-container">
                                <img class="mcimg" {$src} />
                                <!--intExchangeWagesImagePlace-->
                            </div>
                        </a>
                    </div>
                                            <div class="text-elements">
    <div class="text_on_hover">
                    <div class="mcdomain-top mcdomain" style="display: none;"><a {$target} {$href}>{$source}</a></div>
                <div class="mctitle">
            <a {$target} {$href}>{$title}</a>
        </div>
                        <div class="mgtobottom">
                            <div class="mcdomain"><a {$target} {$href}>{$source}</a></div>
                    </div>
    </div>
</div>
                                    </div>
            {/if}
        </div>
    {/foreach}
</div>
',
@template_composite_disclaimer = '
<div class="mgbox">
<!--advertPrefix-->
    {foreach}
        <div class="mgline">
            {if $banner}{$banner}{/if}
            {if $teaser}
                                <div class="image-with-text">
                                        <div class="mcimg">
                        <a {$target} {$href} >
                            <div class="image-container">
                                <img class="mcimg" {$src} />
                                                                                                                                </div>
                        </a>
                    </div>
                                            <div class="text-elements">
    <div class="text_on_hover">
                    <div class="mcdomain-top mcdomain" style="display: none;"><a {$target} {$href}><!--intExhangeWagesSourcePlace-->{$source}</a></div>
                <div class="mctitle"><a {$target} {$href}>{$title}</a></div>
                        <div class="fake">
                            <div class="mcdomain"><a {$target} {$href}>{$source}</a></div>
                                </div>
        <div class="mgtobottom">
                            <div class="mcdomain"><a {$target} {$href}><!--intExhangeWagesSourcePlace-->{$source}</a></div>
                            <div class="disclaimer">{$disclaimer}</div>
        </div>
    </div>
</div>
                                    </div>
            {/if}
        </div>
    {/foreach}
</div>
',
@template_transit_mgid = '
<div class="mgbox news">
<!--advertPrefix-->
<div class="news__heading"> You May Like </div>
{foreach}
   {if this.iteration % 10 == 0 || this.iteration % 10 == 6}
   <div class="mgline news__item news__item_big">
   {if $banner}{$banner}{/if}
       <div class="news__item_big__inner">
           <a {$target} {$href} class="news__link news__link_full">
               <div class="blur-image view-desktop">
                   <div class="blur-image__wrap">
                       <div class="blur-image__fade"></div>
                   </div>
                   <div class="blur-image__image">
                       <img class="mcimg" {$src}>
                   </div>
               </div>
               <span class="news__hover"></span>
               <div class="mcimg news__image news__image_big">
                   <img class="mcimg" {$src}>
               </div>
               <div class="mctitle news__title news__title_big">
                   <span class="news__link_full__item">
                       <span class="news__text">
                           {$title}
                       </span>
                       <span class="news__text news__descr">
                           {$desc}
                       </span>
                   </span>
                   <span class="news__domain">
                       {$source}
                   </span>
                   <span class="news__btn">
                       <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-arrow"><g class="fa-group"><path fill="#fff" d="M319.91 292H24a24 24 0 0 1-24-24v-24a24 24 0 0 1 24-24h295.91l35.66 36z" class="svg-arrow__left"></path><path fill="#fff" d="M305.44 103.05L441 238.54l.06.06a25.23 25.23 0 0 1 0 34.84l-.06.06L305.44 409a24 24 0 0 1-33.94 0l-17-17a24 24 0 0 1 0-33.94L355.57 256 254.5 154a24 24 0 0 1 0-33.94l17-17a24 24 0 0 1 33.94-.01z" class="svg-arrow__right"></path></g>
                       </svg>;
                   </span>
               </div>
           </a>
       </div>
   </div>
   {/if}
   {if this.iteration % 10 != 0 && this.iteration % 10 != 6}
   <div class="mgline news__item news__item_small">
       <div class="image-with-text news__item_small__inner">
           <a {$target} {$href} class="news__link news__link_full">
               <span class="news__hover"></span>
               <div class="mcimg news__image">
                   <img class="mcimg" {$src} />
               </div>
               <div class="text-elements news__title">
                   <div class="mctitle news__text">
                       {$title}
                   </div>
                   <div class="news__domain">
                       {$source}
                   </div>
               </div>
           </a>
       </div>
   </div>
   {/if}
   {/foreach}
</div>
',
@template_transit_lenta = '
<div class="mgbox news">
<!--advertPrefix-->
<div class="news__heading"> Вас также заинтересует </div>
{foreach}
   {if this.iteration % 10 == 0 || this.iteration % 10 == 6}
   <div class="mgline news__item news__item_big">
       {if $banner}{$banner}{/if}
       {if $teaser}
       <div class="news__item_big__inner">
           <a {$target} {$href} class="news__link news__link_full">
               <div class="blur-image view-desktop">
                   <div class="blur-image__wrap">
                       <div class="blur-image__fade"></div>
                   </div>
                   <div class="blur-image__image"></div>
               </div>
               <span class="news__hover"></span>
               <div class="mcimg news__image news__image_big">
                   <img class="mcimg" {$src}>
               </div>
               <div class="mctitle news__title news__title_big">
                   <span class="news__link_full__item">
                       <span class="news__text">
                           {$title}
                       </span>
                       <span class="news__text news__descr">
                           {$desc}
                       </span>
                   </span>
                   <span class="news__domain">
                       {$source}
                   </span>
                   <span class="news__btn">
                       <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-arrow"><g class="fa-group"><path fill="#fff" d="M319.91 292H24a24 24 0 0 1-24-24v-24a24 24 0 0 1 24-24h295.91l35.66 36z" class="svg-arrow__left"></path><path fill="#fff" d="M305.44 103.05L441 238.54l.06.06a25.23 25.23 0 0 1 0 34.84l-.06.06L305.44 409a24 24 0 0 1-33.94 0l-17-17a24 24 0 0 1 0-33.94L355.57 256 254.5 154a24 24 0 0 1 0-33.94l17-17a24 24 0 0 1 33.94-.01z" class="svg-arrow__right"></path></g>
                       </svg>;
                   </span>
               </div>
           </a>
       </div>
       {/if}
   </div>
   {/if}
   {if this.iteration % 10 != 0 && this.iteration % 10 != 6}
   <div class="mgline news__item news__item_small">
       {if $banner}{$banner}{/if}
       {if $teaser}
       <div class="image-with-text news__item_small__inner">
           <a {$target} {$href} class="news__link news__link_full">
               <span class="news__hover"></span>
               <div class="mcimg news__image">
                   <img class="mcimg" {$src} />
               </div>
               <div class="text-elements news__title">
                   <div class="mctitle news__text">
                       {$title}
                   </div>
                   <div class="news__domain">
                       {$source}
                   </div>
               </div>
           </a>
       </div>
       {/if}
   </div>
   {/if}
{/foreach}
</div>
';