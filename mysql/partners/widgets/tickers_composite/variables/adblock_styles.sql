SET @adblock_styles = '.mgresponsive {
  display: inherit;
}
.mgbox {
  padding: 0 !important;
  position: relative !important;
  text-align: center;
  vertical-align: top !important;
  margin: 0 auto;
  border-style: solid;
  border-width: 0px;
  border-color: ;
  background-color: ;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  line-height: 100% !important;
  transition: none !important;
  box-sizing: border-box;
}
.mgline {
  position: relative;
}
.mgline .image-with-text {
  position: relative;
}
.mgbox {
  width: 100%;
  max-width: 100%;
}
div.mcimg {
  padding: 0px;
  text-align: center;
}
img.mcimg {
  border-style: solid;
  border-color: #ffffff;
  border-width: 0px;
  width: 100% !important;
  height: auto !important;
  max-width: 492px;
  max-height: 277px;
  box-sizing: border-box;
  display: block;
}
.mctitle {
  margin-top: 10px;
  text-align: center;
}
.mctitle a {
  font-weight: bold;
  font-size: 15px;
  line-height: 15px;
  font-style: normal;
  text-decoration: none;
  color: #333333;
  font-family: Verdana,Geneva,sans-serif;
}
.mcdesc {
  display: block;
  text-align: center;
}
.mcdesc a {
  font-weight: 400;
  font-size: 12px;
  line-height: 12px;
  font-style: normal;
  text-decoration: none !Important;
  color: #666666;
  font-family: Verdana,Geneva,sans-serif;
}
.mcdomain {
  display: block;
  text-align: center;
}
.mcdomain a {
  font-weight: normal;
  font-size: 10px;
  line-height: 10px;
  font-style: normal;
  text-decoration: none;
  color: #bbbbbb;
  font-family: Verdana,Geneva,sans-serif;
  padding: 4px;
  display: block;
  overflow: hidden;
  text-transform: capitalize;
}
.mcdomain a img.mcimgsrc {
  vertical-align: bottom;
  margin-bottom: -3px;
  height: 20px;
  width: 20px;
  display: inline-block;
}
.mgline {
  background: none repeat scroll 0 0;
  background-color: ;
  cursor: pointer;
  display: inline-block;
  overflow: hidden;
  zoom: 1;
  display: inline;
  padding: 0 !important;
  border-style: solid;
  border-color: #ffffff;
  border-width: 0px;
  width: 32.33333333%;
  max-width: 32.33333333%;
  box-sizing: border-box;
  margin: 10px 0.5%;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  word-wrap: break-word;
}
.mgline .image-container {
  position: relative;
}
.mgline .image-container .mcimgad {
  position: absolute;
  right: 0;
  bottom: 0;
  width: 20px;
  height: 20px;
}
.mgline {
  vertical-align: top;
}
.mgline,
.mgbox {
  min-width: 90px;
}
.mgline[max-width~="120px"] .mcdesc {
  display: none !important;
}
@supports not (flex-wrap: wrap) {
  .mgbox {
    display: block !important;
  }
  .mgline {
    display: inline-block !important;
  }
}
.text-elements a {
  text-decoration: none;
}
div.mcprice {
  text-align: left;
}
div.mcprice span {
  font-weight: bold;
  font-size: 14px;
  line-height: 14px;
  font-style: normal;
  text-decoration: none;
  color: #2a3a7b;
  font-family: Verdana,Geneva,sans-serif;
}
div.mgbuybox,
div.mgarrowbox {
  display: false;
}
div.mgbuybox,
div.mgarrowbox,
div.mcprice {
  display: none;
}
span.mcpriceold {
  text-decoration: line-through !important;
}
@media (max-width: 480px) {
  .mgline {
    width: 48% !important;
    margin: 1% !important;
    max-width: 48% !important;
  }
}
@media (max-width: 480px) {
  .mgline {
    width: 98% !important;
    margin: 1% !important;
    max-width: 98% !important;
  }
}
img.mcimg {
  margin: 0;
  opacity: 1 !important;
}
.mgline {
  position: relative;
}
.mgline .fake {
  visibility: hidden;
  position: relative;
  padding-top: 4px;
}
.mgline:hover .mctitle a {
  color: #2b397b;
  text-decoration: underline !important;
}
.mgbuybox {
  text-align: right;
  font-weight: 700;
  font-size: 12px;
  color: #666666;
}
.mctitle {
  margin-top: 2px;
  line-height: 1 !important;
}
.mctitle a {
  line-height: 110% !important;
}
.mcdesc {
  margin-top: 0;
  margin-bottom: 2px;
}
.mcdesc a {
  line-height: 1.5 !important;
}
.mcprice-wrap {
  position: absolute;
  display: block;
  top: 0px;
  left: 0px;
  text-decoration: none;
}
.mcdiscount {
  color: #fff;
  border: none;
  font-size: 1.21em;
}
.mgarrowbox {
  position: relative;
  background: #fff;
  padding-right: 12px;
}
.mgarrowbox:before {
  content: '''';
  position: absolute;
  top: 0;
  right: -10px;
  border-top: 21px solid transparent;
  border-bottom: 21px solid transparent;
  border-left: 10px solid #fff;
}
div.mcprice-cover {
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-wrap: nowrap;
  -ms-flex-wrap: nowrap;
  flex-wrap: nowrap;
  -webkit-align-items: stretch;
  -moz-box-align: stretch;
  -ms-flex-align: stretch;
  align-items: stretch;
  -webkit-justify-content: flex-start;
  justify-content: flex-start;
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
  height: 42px;
  font-size: 14px;
}
div.mcprice-cover .mcprice {
  font-size: 1.29em;
  color: #fff;
}
div.mcprice-cover.mcprice-cover_old .mcprice-inner {
  padding-top: 4px;
  width: auto;
}
div.mcprice-cover.mcprice-cover_old .mcprice-val {
  padding-right: 0;
}
div.mcprice-cover.mcprice-cover_old .mcprice {
  margin-bottom: 4px;
  color: #2a3a7b;
  font-size: 1em;
}
div.mcprice-cover.mcprice-cover_old + .mcprice-cover {
  display: none !important;
}
.mcprice-val,
.mcprice-inner {
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-align-items: center;
  -moz-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-justify-content: flex-start;
  justify-content: flex-start;
  white-space: nowrap;
}
.mcprice-inner {
  background: #ed181d;
  position: relative;
  padding: 0 2px 0 8px;
  text-align: center;
  color: #fff;
  width: 100%;
}
.mcprice-inner:before {
  content: '''';
  position: absolute;
  top: 0;
  right: -10px;
  border-top: 21px solid transparent;
  border-bottom: 21px solid transparent;
  border-left: 10px solid #ed181d;
}
.mcprice-val {
  padding: 2px 10px 0 14px;
  -webkit-flex-direction: column;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-align-items: flex-start;
  -moz-box-align: start;
  -ms-flex-align: start;
  align-items: flex-start;
  font-weight: bold;
  font-style: normal;
  text-align: left;
}
.mcpriceold {
  display: block !important;
  position: relative;
  width: 100%;
  font-size: 0.78em;
  font-weight: inherit;
  color: #ed181d;
}
.mcpriceold span {
  position: relative;
}
.mcpriceold span:before {
  content: '''';
  height: 1px;
  background: #ed181d;
  left: 0;
  right: 0;
  top: 50%;
  position: absolute;
}
div.mgbuybox,
div.mgarrowbox {
  display: none;
}
.fake,
.mgtobottom {
  display: table;
  bottom: 0;
  width: 100%;
  text-align: left;
}
.fake > *,
.mgtobottom > * {
  display: table-cell;
}
.mgtobottom__buy {
  padding-right: 20px;
}
.mgtobottom {
  position: absolute;
}
.mgline .image-with-text,
.mgline .mgtobottom {
  width: 100% !important;
  margin: 0 auto;
}
.mgline .image-with-text,
.mgline .mgtobottom {
  max-width: 492px;
}
.mghead {
  font-family: Arial,Helvetica,sans-serif !important;
  color: #2b397b;
  font-size: 17px !important;
  text-transform: uppercase !important;
}
.mcdomain {
  display: block;
  overflow: hidden;
  padding: 4px;
}
.mcdomain a {
  display: block;
  padding: 0 0 2px;
  padding-top: 5px;
  overflow: hidden;
}
div.mcprice,
div.mcriceold {
  font-weight: bold;
  font-size: 14px;
  line-height: 14px;
  font-style: normal;
  text-decoration: none;
  color: #2a3a7b;
  font-family: Verdana,Geneva,sans-serif;
}
.mgline[max-width~="120px"] .mgarrowbox,
.mgline[max-width~="120px"] .mgbuybox {
  display: none !important;
}
.mgline .image-with-text {
  min-height: 1px;
}
@media (max-width: 480px) {
  .mgline {
    width: 98% !important;
    margin: 1% !important;
    max-width: 98% !important;
  }
}
.mgline {
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  overflow: hidden;
  background: #fff;
}
.mgline .image-container {
  display: inline-block !important;
}
.mgline .mgtobottom {
  left: 0;
  right: 0;
}
.mgline .mctitle {
  margin-top: 5px;
  min-height: 35px;
  overflow: hidden;
  padding: 0 7px;
}
.mgline .mcdesc {
  padding: 0 20px;
}
.mgline .mcdomain {
  padding-top: 4px;
  padding-bottom: 4px;
}
.mcdomain a {
  line-height: 14px;
  margin-top: 0;
  margin-bottom: 5px;
}
.fake > *,
.mgtobottom > * {
  padding-left: 7px;
  padding-right: 7px;
}
.mcdomain a img.mcimgsrc {
  margin-right: 5px;
}
.mg-slider-overlay {
  overflow: hidden;
  position: relative;
}
.mg-slider-overlay .mg-slider-wrap {
  min-height: 200px;
  position: relative;
  width: 100%;
  -webkit-transform: translate3D(0, 0, 0);
  -moz-transform: translate3D(0, 0, 0);
  -ms-transform: translate3D(0, 0, 0);
  -o-transform: translate3D(0, 0, 0);
  transform: translate3D(0, 0, 0);
  -webkit-transition-duration: 200ms;
  -moz-transition-duration: 200ms;
  -ms-transition-duration: 200ms;
  -o-transition-duration: 200ms;
  transition-duration: 200ms;
}
.mg-slider-overlay .mg-slider-wrap .mg-slider-box {
  left: 0;
  top: 0;
  position: absolute;
  width: 100%;
}
.mg-pagination {
  text-align: center;
}
.mg-pagination .mg-pagination-list {
  display: inline-block;
}
.mg-pagination .mg-pagination-item {
  cursor: pointer;
  display: inline-block;
  *display: inline;
  *zoom: 1;
  width: 8px;
  height: 8px;
  margin: 0 2px;
  background: #ddd;
  border-radius: 50%;
}
.mg-pagination .mg-pagination-item-prev,
.mg-pagination .mg-pagination-item-next {
  background: #f7f7f7;
  border: 1px solid #cecece;
  border-radius: 2px;
  cursor: pointer;
  display: inline-block;
  height: 11px;
  margin: 0 10px;
  padding: 5px 0;
  text-align: center;
  vertical-align: middle;
  width: 29px;
}
.mg-pagination .mg-pagination-item-prev:after,
.mg-pagination .mg-pagination-item-next:after {
  background: url(https://images.dable.io/static/i/prevnext2.png?3) no-repeat 0 0;
  background-size: 17px 11px;
  content: "";
  display: inline-block;
  height: 13px;
  width: 9px;
}
.mg-pagination .mg-pagination-item-next:after {
  background-position: -10px 0;
  width: 8px;
}
.mg-pagination .mg-pagination-item-current {
  background: #508eef;
}
.mgline .image-container {
  display: block !important;
}
@media (max-width: 480px) {
  .mgline {
    width: 100% !important;
    margin: 1% 0 !important;
    max-width: 100% !important;
    min-height: 250px;
  }
}
';