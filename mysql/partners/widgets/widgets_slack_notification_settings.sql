insert into partners.widgets_slack_notification_settings(widget_id, alert_type, delta, hour_from, hour_to, period)
    VALUES
        (350, 'revenue_drop',       1, 2, 4, 'hour'),
        (350, 'revenue_increase',   2, 2, 4, 'hour'),
        (350, 'profitability_drop', 3, 2, 4, 'hour'),
        (351, 'revenue_drop',       1, 2, 4, 'hour'),
        (351, 'profitability_drop', 3, 2, 4, 'hour');