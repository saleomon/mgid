UPDATE partners.current_status
set value = DAY(CONVERT_TZ(current_timestamp,'Europe/Kiev','Europe/Kiev'))
where current_status.key IN('g_blocks_rotate_start_date', 'g_blocks_rotate_date',
                            'p_news_rotate_start_date', 'p_news_rotate_date');