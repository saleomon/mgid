const selectors = ['.mctitle a', '.mcdomain a'];

setTimeout(() => {
    init();
}, 1000);

function init() {
    const roots = [...document.getElementsByTagName('div')].filter((el) => el.id.indexOf('ScriptRootC'));

    if (roots.length === 0) {
        return false;
    }

    roots.forEach((el) => {
        el.shadowRoot ?
            findElementsAndChangeStyle(el.shadowRoot) :
            findElementsAndChangeStyle(el);
    });
}

function findElementsAndChangeStyle(root) {
    selectors.forEach((selector) => {
        const arr = [...root.querySelectorAll(selector)];

        arr.forEach((el) => {
            el.style.setProperty("line-height", "unset", 'important');
        });
    });
}